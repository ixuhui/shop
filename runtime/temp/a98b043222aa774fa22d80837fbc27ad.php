<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:75:"D:\phpstudy_pro\WWW\web\public/../application/admin\view\message\index.html";i:1619323284;s:58:"D:\phpstudy_pro\WWW\web\application\admin\view\layout.html";i:1619609317;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>后台管理系统</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="/public/static/admin/css/main.css" rel="stylesheet" type="text/css"/>
    <link href="/public/static/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/public/static/admin/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
<!--    <script src="/public/static/admin/js/jquery-1.8.1.min.js"></script>-->
<!--    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.js"></script>-->
    <script src="/public/static/admin/js/bootstrap.min.js"></script>
<!--    <script src="/public/static/admin/js/highcharts.js"></script>-->
<!--    <script src="/public/static/admin/js/exporting.js"></script>-->
<!--    <script src="/public/static/admin/js/draw.js"></script>-->
<!--    <script src="/public/static/plugins/layer/layer.js"></script>-->

</head>
<body>
<!-- 上 -->
<div class="navbar">
    <div class="navbar-inner">
        <div class="container-fluid">
            <ul class="nav pull-right">
                <li id="fat-menu" class="dropdown">
                    <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-user icon-white"></i> admin
                        <i class="icon-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a tabindex="-1" href="javascript:void(0);">修改密码</a></li>
                        <li class="divider"></li>
                        <li><a tabindex="-1" href="javascript:void(0);">安全退出</a></li>
                    </ul>
                </li>
            </ul>
            <a class="brand" href="index.html"><span class="first">后台管理系统</span></a>
            <ul class="nav">
                <li class="active"><a href="javascript:void(0);">首页</a></li>
                <li><a href="javascript:void(0);">系统管理</a></li>
                <li><a href="javascript:void(0);">权限管理</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- 左 -->
<div class="sidebar-nav">
    <?php if(is_array($authInfoList) || $authInfoList instanceof \think\Collection || $authInfoList instanceof \think\Paginator): $i = 0; $__LIST__ = $authInfoList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$auil): $mod = ($i % 2 );++$i;?>
        <a href="#accounts-menu<?php echo $auil['id']; ?>" class="nav-header" data-toggle="collapse"><i class="icon-exclamation-sign"></i>
            <?php echo $auil['auth_name']; ?></a>
        <ul id="accounts-menu<?php echo $auil['id']; ?>" class="nav nav-list collapse in">

            <?php if(isset($auil['son'])): if(is_array($auil['son']) || $auil['son'] instanceof \think\Collection || $auil['son'] instanceof \think\Paginator): $i = 0; $__LIST__ = $auil['son'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$auilson): $mod = ($i % 2 );++$i;if($auilson['is_nav'] == '1'): ?>
                <li><a href="<?php echo url("$auilson[auth_c]/$auilson[auth_a]"); ?>"><?php echo $auilson['auth_name']; ?></a></li>
            <?php endif; endforeach; endif; else: echo "" ;endif; endif; ?>
        </ul>
    <?php endforeach; endif; else: echo "" ;endif; ?>
</div>
<!-- 右 -->
<link rel="stylesheet" href="/public/static/plugins/layui/css/layui.css">
<script src="/public/static/plugins/layui/layui.js"></script>
<div class="content">
    <div class="header">
        <h1 class="page-title">权限列表</h1>
    </div>

    <div class="well">
        <a class="btn btn-primary" href="javascript:sendAll()">群发消息</a>
        <!-- table -->
        <table class="table table-bordered table-hover table-condensed">
            <thead>
            <tr>
                <th>编号</th>
                <th>昵称</th>
                <th>头像</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <?php if(is_array($fans) || $fans instanceof \think\Collection || $fans instanceof \think\Paginator): $i = 0; $__LIST__ = $fans;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$f): $mod = ($i % 2 );++$i;$color = $i%2==1?'success':'error'; ?>
            <tr class="<?php echo $color; ?>">
                <td><?php echo $i; ?></td>
                <td><?php echo $f['nickname']; ?></td>
                <td><img src="<?php echo $f['headimgurl']; ?>" alt="" width="50"></td>
                <td>
                    <a href="javascript:alertWindow('<?php echo $f['openid']; ?>');"> 发送消息 </a>
                    <a href="javascript:void(0);" onclick="if(confirm('确认删除？')) location.href='#'"> 删除 </a>
                </td>
            </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
        </table>
    </div>
    <!-- footer -->
    <footer>
        <hr>
        <p>© 2017 <a href="javascript:void(0);" target="_blank">ADMIN</a></p>
    </footer>
</div>
<script>
    function alertWindow(openid){

        layui.use('layer', function(){
            var layer = layui.layer;
            layer.open({
                type: 2,
                area: ['300px', '300px'], //宽高
                content: ['<?php echo url("alertWindow","","",false); ?>?openid='+openid,"no"] //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
            });
        });
    }


    function sendAll(){
        $.post('<?php echo url("sendTextAll"); ?>');
    }
</script>
</body>
</html>