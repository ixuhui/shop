/*
Navicat MySQL Data Transfer

Source Server         : 旭辉博客
Source Server Version : 50535
Source Host           : i65ahw3c.2406.dnstoo.com:5506
Source Database       : zhangxuhui

Target Server Type    : MYSQL
Target Server Version : 50535
File Encoding         : 65001

Date: 2021-05-16 16:55:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tpshop_address
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_address`;
CREATE TABLE `tpshop_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `consignee` varchar(64) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  `address` varchar(255) NOT NULL DEFAULT '' COMMENT '收货地址',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '收货人手机号',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL COMMENT '软删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_address
-- ----------------------------

-- ----------------------------
-- Table structure for tpshop_attribute
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_attribute`;
CREATE TABLE `tpshop_attribute` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '属性id',
  `attr_values` varchar(255) NOT NULL DEFAULT '' COMMENT '供选择的属性值',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  `pid` int(11) DEFAULT '0' COMMENT '父级id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_attribute
-- ----------------------------
INSERT INTO `tpshop_attribute` VALUES ('1', '尺寸', null, null, null, '0');
INSERT INTO `tpshop_attribute` VALUES ('2', '5', null, null, null, '1');
INSERT INTO `tpshop_attribute` VALUES ('3', '6', null, null, null, '1');
INSERT INTO `tpshop_attribute` VALUES ('4', '颜色', null, null, null, '0');
INSERT INTO `tpshop_attribute` VALUES ('5', '白色', null, null, null, '4');
INSERT INTO `tpshop_attribute` VALUES ('6', '灰色', null, null, null, '4');
INSERT INTO `tpshop_attribute` VALUES ('7', '紫色', null, null, null, '4');
INSERT INTO `tpshop_attribute` VALUES ('8', '7', '1618380948', '1618380948', null, '1');

-- ----------------------------
-- Table structure for tpshop_auth
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_auth`;
CREATE TABLE `tpshop_auth` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `auth_name` varchar(20) NOT NULL COMMENT '权限名称',
  `pid` smallint(6) unsigned NOT NULL COMMENT '父id',
  `auth_c` varchar(32) NOT NULL DEFAULT '' COMMENT '控制器',
  `auth_a` varchar(32) NOT NULL DEFAULT '' COMMENT '操作方法',
  `is_nav` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否作为菜单显示 1是 0否',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_auth
-- ----------------------------
INSERT INTO `tpshop_auth` VALUES ('15', '系统管理', '0', '', '', '1', '1617865710', '1617865710', null);
INSERT INTO `tpshop_auth` VALUES ('19', '角色管理', '15', 'Role', 'index', '1', '1617866052', '1617866052', null);
INSERT INTO `tpshop_auth` VALUES ('20', '权限管理', '15', 'Auth', 'index', '1', '1617866093', '1617866093', null);
INSERT INTO `tpshop_auth` VALUES ('21', '商品管理', '0', '', '', '1', '1618038427', '1618038427', null);
INSERT INTO `tpshop_auth` VALUES ('22', '商品分类添加', '21', 'Catrgoty', 'cateAdd', '1', '1618456135', '1618456135', null);
INSERT INTO `tpshop_auth` VALUES ('23', '商品属性添加', '21', 'Catrgoty', 'cateAdd', '1', '1618472517', '1618472517', null);
INSERT INTO `tpshop_auth` VALUES ('24', '菜单管理', '0', '', '', '1', '1619147200', '1619147200', null);
INSERT INTO `tpshop_auth` VALUES ('25', '创建自定义菜单', '24', 'Wechat', 'createMenu', '1', '1619147624', '1619147624', null);
INSERT INTO `tpshop_auth` VALUES ('26', '删除自定义菜单', '24', 'Wechat', 'delMenu', '1', '1619147810', '1619147810', null);
INSERT INTO `tpshop_auth` VALUES ('27', '消息管理', '0', '', '', '1', '1619317007', '1619317007', null);
INSERT INTO `tpshop_auth` VALUES ('28', '粉丝列表', '27', 'Message', 'index', '1', '1619320906', '1619320906', null);
INSERT INTO `tpshop_auth` VALUES ('29', '素材管理', '0', '', '', '1', '1619336045', '1619336045', null);
INSERT INTO `tpshop_auth` VALUES ('30', '添加永久素材', '29', 'Media', 'add', '1', '1619336167', '1619336167', null);

-- ----------------------------
-- Table structure for tpshop_brand
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_brand`;
CREATE TABLE `tpshop_brand` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) NOT NULL DEFAULT '' COMMENT '类型名称',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_brand
-- ----------------------------
INSERT INTO `tpshop_brand` VALUES ('2', '小米', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('3', '华为', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('4', 'vivo', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('5', 'OPPO', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('6', '华硕', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('7', '联想', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('8', '戴尔', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('9', '惠普', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('10', '神州', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('11', '西服', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('12', '休闲', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('13', '卫衣', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('14', '汉服', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('15', '足力健', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('16', 'ECCO', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('17', 'Clarks', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('18', 'CAT', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('19', 'GEOX', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('20', '大众', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('21', '别克', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('22', '宝骏', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('23', '宝马', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('24', '奥迪', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('25', '奔驰', null, null, null);
INSERT INTO `tpshop_brand` VALUES ('30', '222', '1618387950', '1618388609', '1618388609');
INSERT INTO `tpshop_brand` VALUES ('31', '1111', '1618388636', '1618388753', '1618388753');
INSERT INTO `tpshop_brand` VALUES ('32', '苹果', null, null, null);

-- ----------------------------
-- Table structure for tpshop_cart
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_cart`;
CREATE TABLE `tpshop_cart` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `goods_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品id',
  `goods_attr_ids` varchar(255) NOT NULL DEFAULT '' COMMENT '商品属性ids（多个id用,分隔）',
  `number` int(11) NOT NULL DEFAULT '0' COMMENT '购买数量',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_cart
-- ----------------------------

-- ----------------------------
-- Table structure for tpshop_cate_attr
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_cate_attr`;
CREATE TABLE `tpshop_cate_attr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) DEFAULT NULL,
  `attr_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_cate_attr
-- ----------------------------
INSERT INTO `tpshop_cate_attr` VALUES ('4', '2', '1');
INSERT INTO `tpshop_cate_attr` VALUES ('5', '2', '4');
INSERT INTO `tpshop_cate_attr` VALUES ('6', '3', '5');
INSERT INTO `tpshop_cate_attr` VALUES ('7', '3', '6');
INSERT INTO `tpshop_cate_attr` VALUES ('9', '3', '3');
INSERT INTO `tpshop_cate_attr` VALUES ('10', '3', '7');
INSERT INTO `tpshop_cate_attr` VALUES ('12', '5', '3');
INSERT INTO `tpshop_cate_attr` VALUES ('13', '5', '5');
INSERT INTO `tpshop_cate_attr` VALUES ('14', '5', '6');
INSERT INTO `tpshop_cate_attr` VALUES ('15', '5', '7');
INSERT INTO `tpshop_cate_attr` VALUES ('33', null, '2');
INSERT INTO `tpshop_cate_attr` VALUES ('32', null, '2');
INSERT INTO `tpshop_cate_attr` VALUES ('31', null, '2');

-- ----------------------------
-- Table structure for tpshop_category
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_category`;
CREATE TABLE `tpshop_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(255) NOT NULL DEFAULT '' COMMENT '分类名称',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父级分类',
  `is_show` tinyint(3) NOT NULL DEFAULT '1' COMMENT '是否显示 0不显示 1显示',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='商品分类表';

-- ----------------------------
-- Records of tpshop_category
-- ----------------------------
INSERT INTO `tpshop_category` VALUES ('1', '手机', '0', '1', '1618313313', '1618313313', null);
INSERT INTO `tpshop_category` VALUES ('2', '智能机', '1', '1', '1618313328', '1618313328', null);
INSERT INTO `tpshop_category` VALUES ('3', '小米', '2', '1', '1618313343', '1618313343', null);
INSERT INTO `tpshop_category` VALUES ('4', '老年机', '1', '1', '1618313379', '1618313379', null);
INSERT INTO `tpshop_category` VALUES ('5', 'vivo', '2', '1', '1618313390', '1618313390', null);
INSERT INTO `tpshop_category` VALUES ('6', '苹果', '2', '1', '1618313424', '1618313424', null);
INSERT INTO `tpshop_category` VALUES ('7', '诺基亚', '4', '1', '1618313459', '1618313459', null);
INSERT INTO `tpshop_category` VALUES ('8', '小灵通', '4', '1', '1618313468', '1618313468', null);
INSERT INTO `tpshop_category` VALUES ('9', '电脑', '0', '1', '1618313480', '1618313480', null);
INSERT INTO `tpshop_category` VALUES ('10', '笔记本', '9', '1', '1618313490', '1618313490', null);
INSERT INTO `tpshop_category` VALUES ('11', '联想', '10', '1', '1618313503', '1618313503', null);
INSERT INTO `tpshop_category` VALUES ('12', '戴尔', '10', '1', '1618313518', '1618313518', null);
INSERT INTO `tpshop_category` VALUES ('13', '华硕', '10', '1', '1618313534', '1618313534', null);
INSERT INTO `tpshop_category` VALUES ('14', '台式机', '9', '1', '1618313546', '1618313546', null);
INSERT INTO `tpshop_category` VALUES ('15', '联想', '14', '1', '1618313559', '1618313559', null);
INSERT INTO `tpshop_category` VALUES ('16', '华硕', '14', '1', '1618313574', '1618313574', null);
INSERT INTO `tpshop_category` VALUES ('17', '戴尔', '14', '1', '1618313589', '1618313589', null);
INSERT INTO `tpshop_category` VALUES ('18', '苹果', '2', '1', '1618368650', '1618368650', null);
INSERT INTO `tpshop_category` VALUES ('19', '华为', '2', '1', '1618368683', '1618368683', null);
INSERT INTO `tpshop_category` VALUES ('20', 'OPPO', '2', '1', '1618368692', '1618368692', null);

-- ----------------------------
-- Table structure for tpshop_fans
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_fans`;
CREATE TABLE `tpshop_fans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xcx_openid` varchar(255) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `sex` tinyint(4) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `headimgurl` varchar(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `unionid` varchar(255) DEFAULT NULL,
  `delete_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `num` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=181 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_fans
-- ----------------------------
INSERT INTO `tpshop_fans` VALUES ('66', null, 'odZTltySBoXUaP9FROa--8InCEHU', '..', '1', '', '', '爱沙尼亚', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lCpTU0ndJACAeoWvDJB4nKt0ekuiaNaptXy9deicqFRV5Rzxq67Y4NHcNFkMicFuQ40mQZI520HP5DdZzBktHbjHuF/132', '1618970400', null, null, '1619169524', '8');
INSERT INTO `tpshop_fans` VALUES ('67', null, 'odZTlt5186rZ5fdNRtXLd8vvnw2U', '冬风', '1', '周口', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lCv9ssNjvByWfa1QTvUO8QUSU3iaZX3EawWNyeraE94SGSDbiaR2EHKibNNaeMukQJQ0YYHDP8MF62S69SRR6pB7Yx/132', '1618970402', null, null, '1619333441', '8');
INSERT INTO `tpshop_fans` VALUES ('68', null, 'odZTlt9psoFHsH2Su5qYDEzW_Ks8', '流光若逝', '1', '开封', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lBJu6Ub8gF4icrEic8GUFQZ25iat2CjPiaFfnxVqY2VbuLQKFcaic7AOiauBibFa98pnSDQpib0JunfVQmY51x3emTanIcu/132', '1618970405', null, null, '1619169511', '5');
INSERT INTO `tpshop_fans` VALUES ('69', null, 'odZTlt--prYR1YUBJN2AXRAiR5DA', '明明如月', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKjBcoiauGGauwEP6MSJn9kicFcEHQkicDftGZ39jpLw0EYTQpoAItTtyI4RtPQRUAI3jts5Z1pN65ADwSMMf28b0X/132', '1618970413', null, null, '1619169517', '5');
INSERT INTO `tpshop_fans` VALUES ('70', null, 'odZTlt4zYYEU-squlXsBKYeHz90M', '初心', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaEJRkgsydxb994pnhzEfCkA6m7FRua2naQwZ9WhkssFLSD4AGoh9lJ68RktGFickBzpPfIsjDwibPWFG1CfDAnQrGHA7LnMZZaQFY/132', '1618972120', null, null, '1619169530', '8');
INSERT INTO `tpshop_fans` VALUES ('71', null, 'odZTltwP1ZcsgnoHHbEa2VpDXhXY', '邂逅千禧', '1', '信阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCJvuhQ1uyOZqicibVAr8zeNFvkWKWjdCtcic1a7snjRgXe9Y7HxRd8CGs5u6EM4qG59NoWHXXafmhhSYvYIpGhwTes/132', '1618972172', null, null, '1619169535', '3');
INSERT INTO `tpshop_fans` VALUES ('72', null, 'odZTlt3oGgk6Ip7u0C4ZBHDOCZtI', 'Y', '1', '信阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicQ7O1sA7tczyMfIkibVghlgeyJAGiaEVTCoFCTojgSxm20iaBv5mmJVIIgvXrAkWqSvNpxvkiaXOUndPgfA5wVVyiaaT/132', '1618972306', null, null, '1619333461', '6');
INSERT INTO `tpshop_fans` VALUES ('73', null, 'odZTltyCcYkrAyfsPkAjhjwR0B9A', '－', '1', '', '', '冰岛', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lDzHOeNCDn3qsuDbGMMCFKx59icBiaB98pReNyb4cRq405ShEqT1tKylYmKBzq2MAqkvvl5ZUlskxADQaFCP6k8Nic/132', '1618978971', null, null, '1619169530', '3');
INSERT INTO `tpshop_fans` VALUES ('74', null, 'odZTlt1E7s9i_3iEAILpV1HBENUY', '阳光总在风雨后', '1', '南阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lAWDzUDHEziahpld7Kib2iaQdjMmKqGDa21vIuQ1gRuoZ9VW2icsfIzuEFu1kTkcnYbXgFUW9GtctUXBmLXeg9RxGVib/132', '1618995141', null, null, '1619169512', '6');
INSERT INTO `tpshop_fans` VALUES ('75', null, 'odZTlt9EkotvF9g8PCkitt21uNDw', 'O_o', '1', '濮阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Q3auHgzwzM7Ejg4GYxeMPpVDib7bGTUzWxL7F18aZujSEqyEAgb0QFM8UHISr5DZGEMRkaYmOSYHrqrNUAGjyUw/132', '1619062762', null, null, '1619169510', '4');
INSERT INTO `tpshop_fans` VALUES ('78', null, 'odZTlt-S8o7I0wBz7twhr3NtWuvU', '懂吗？！', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lCpTU0ndJACAadlh0aGVbheQhHhicIEU2VVNJj0aQibQLSEI3ZtuCwjIB3pMOXmuU2C6pNSGQuDrywFKlEVWyHnPN/132', '1619159915', null, '1619333665', '1619333665', '3');
INSERT INTO `tpshop_fans` VALUES ('76', null, 'o1aFB6ALmIoiwyDdP8fmkZRA3KCc', '。', '1', '常州', '江苏', '中国', 'http://thirdwx.qlogo.cn/mmopen/BsiccKnKy2HZ77wsxhKWe03bAqT9QOicYklbIgGzz3PnbtibqgJ21KDZjbumcCU98djvsYvYAgxTmJ39uBbdwWhjzUNvfzKAncu/132', '1619089107', null, null, '1619089107', '1');
INSERT INTO `tpshop_fans` VALUES ('77', null, 'odZTltwhs_yoXX53p_6ixk6KLEZE', '醉笙', '1', '南阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaEIRtCMcMr71oCE1lPHVhx581gRFK6eVbyh4UIjbova9CiaFmo56VtgtQX3s0QIKicicFe70VsDYQT8kw/132', '1619143114', null, null, '1619333463', '4');
INSERT INTO `tpshop_fans` VALUES ('79', null, 'odZTlt6Z-foLeo5_158lnKh33D1U', '恢复出厂设置', '0', '', '', '', 'http://thirdwx.qlogo.cn/mmopen/kiaXicXJs2M4cAOyb4TSPDUcjVyPic4ZA71YGZqK7OYiaLpG1zsbHR0xQTTP5QdDDRWJ1nATEzjJyEkrlHxy4txcwA/132', '1619159916', null, null, '1619321383', '4');
INSERT INTO `tpshop_fans` VALUES ('80', null, 'odZTltyPWVFkHpvfYHqO2BMCbbY0', 'z@z', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/6ibUkoWicwAWib0C08ibmGFWpxtHLnNRGDk2Mialztyy0biaIgVFjXtzxkqlGnQ0DDsnNE1uFxA0PShmINDuX20Iyppj1Wx880uLcf/132', '1619159937', null, null, '1619169534', '3');
INSERT INTO `tpshop_fans` VALUES ('81', null, 'odZTlt7Di5_HaStsxtUi5d-WVP3A', '　　　　　　　　', '1', '', '', '百慕大', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKhS8JoIv9tgc6SazZ3b3UNygJY3Iia0xyWD4UicWvh9uyCu1jpDWjHSiaGapDygHeqRic7icNQTAeTzVc5bXLmWZjiar/132', '1619159983', null, null, '1619169513', '3');
INSERT INTO `tpshop_fans` VALUES ('82', null, 'odZTlt2k5HEtgtItJFGzuMtALUX0', '饮舟', '1', '许昌', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lB5PibzFNO0cXK0FvnWhFpOibEquSDPoFL0WN1Sw9icHgLfnw8L4hJvWfqTMexA6icF2Jbzfr7IqrK4F3UH0Av4SSqB/132', '1619160024', null, null, '1619169526', '3');
INSERT INTO `tpshop_fans` VALUES ('83', null, 'odZTlt-jBVYOPn-K-aONBuyHPJJM', '囡囡', '2', '', '', '', 'http://thirdwx.qlogo.cn/mmopen/ajNVdqHZLLA4jCpKWXd8SaViajw4TMsIdCM79owYDvHv0pGvLnXtrQicUWszImJ14dU4kz1CEhFHJ4CHiaTCiaUicwQ/132', '1619161879', null, null, '1619169510', '2');
INSERT INTO `tpshop_fans` VALUES ('84', null, 'odZTlt0uDABo3PYB3FgYCPzKxIuk', '嘛呢', '1', '', '', '阿塞拜疆', 'http://thirdwx.qlogo.cn/mmopen/Q3auHgzwzM565MwXBU2CHeucLMgVRianoRVoTt9ibh5dScibZnfyp1xMOdOeXib8NCKh1soib9F496l6RL4n8nsHsgg/132', '1619161879', null, null, '1619169511', '2');
INSERT INTO `tpshop_fans` VALUES ('85', null, 'odZTltxqlWyfZScx5bP0t3c0MMq0', '没名字', '1', '焦作', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicSYfF1aDiajSdDYXPOYJ4yibPQl4iaHu8p2Qhv0ZFoQ6TVXIHVgia0juoJ10oaUe7icwPHEsVNpSTPBia0sP4dewStj18/132', '1619161879', null, null, '1619169511', '2');
INSERT INTO `tpshop_fans` VALUES ('86', null, 'odZTltyO9KaGacA4RTzoAQH04zPs', '刘大大', '1', '', '', '圣文森特和格林纳丁斯', 'http://thirdwx.qlogo.cn/mmopen/ajNVdqHZLLDuOLnjoEIahibn5sRDLxBIHdcSibCtEELtq7EQib2shTBRYhmZNONqoCmAmcWliaAJEs9HD2J2XE7ybA/132', '1619161880', null, null, '1619169512', '2');
INSERT INTO `tpshop_fans` VALUES ('87', null, 'odZTlt31BGzupVYJqoYNEXeOiMeE', '磐石', '0', '朝阳', '北京', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lAWDzUDHEziahjUkGbSxTicUY75SKJHXGPSPuAscwNraIIp5zicDzP1Tt27iaww0EOYKd29Dh2gIBh9FS0eLIByyTPb/132', '1619161880', null, null, '1619169512', '2');
INSERT INTO `tpshop_fans` VALUES ('88', null, 'odZTlt289ucDIsWWZYjing49pDIc', 'Ktyuyuyuyuคิดถึง', '1', '洛杉矶', '加利福尼亚', '美国', 'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaEI0QNMGqXeW29JbkT597P4usJE81t8XApIxwkGdhSSuqbiaTUricr7eT8OarPzVo5a5OXylOia8Eia6ng/132', '1619161881', null, null, '1619169513', '2');
INSERT INTO `tpshop_fans` VALUES ('89', null, 'odZTlt9KiaAfMFTb0LJayYfIs7Lg', 'ᯤ', '1', '', '', '中国', 'http://thirdwx.qlogo.cn/mmopen/kiaXicXJs2M4fJbfIZ69eTjlzsL3scmibfx1pHn5dSXp8AhOfaibcvd8MU52JNvVok7U5DZS8iceyvPB99HNtrowgLQ/132', '1619161881', null, null, '1619169513', '2');
INSERT INTO `tpshop_fans` VALUES ('90', null, 'odZTlt9kDJEnVI7dkSEEiG9Ugzv4', '姬昌', '1', '杭州', '浙江', '中国', 'http://thirdwx.qlogo.cn/mmopen/Q3auHgzwzM7mrrricxI1GAfE6bFlvbFuj3mPJXm81OO76NCPCC6exETZnTyO9m4shKdchvvF30jgFUNPCaESCicQ/132', '1619161881', null, null, '1619169513', '2');
INSERT INTO `tpshop_fans` VALUES ('91', null, 'odZTlt9WpseVEH4yuBukSmJNiIro', '啊胜', '1', '开封', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKhS8JoIv9tgaWicYiaPRp0QwwuVLP8AXnVBb79UJO2KIRO8Rdo8GZ3nFJkAT6dJm1ozv78D4ybDMGVxd6bAexLPj/132', '1619161881', null, null, '1619169514', '2');
INSERT INTO `tpshop_fans` VALUES ('92', null, 'odZTlt5Bb52HzNAbtfH15yfYBHCc', '', '2', '安阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaEJxX1b24V6HpYRxXm3ic3Mgf3WRiaYl4JN8UmFSYsibUWEclHgK9MrZsQSrfOHb1k1tGE7PnvuBcNOicQ/132', '1619161882', null, null, '1619169514', '2');
INSERT INTO `tpshop_fans` VALUES ('93', null, 'odZTlt09A0rCsiaqnF4GrISZDOVo', 'ღ᭄ꦿ 匿名访客ꕥ᭄ꦿ', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/ajNVdqHZLLB1yLHoLyX0rTUncbU5rxmkgFmPYazVWEbr2n9NZ5F00RHGclA9JwR42Lc3iau4e8V8bGfLOib4dvzw/132', '1619161882', null, null, '1619169514', '2');
INSERT INTO `tpshop_fans` VALUES ('94', null, 'odZTltzCbwCr6M6Jd4PU6VSFD5LM', '', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKhS8JoIv9tgRzOvDpdKGuPtuMAksEF0g6ulVn5w6Rz6TKGudEx8veqD2A9DxibpCfCVsBvLN8KeDuwt3ibSS69pW/132', '1619161882', null, null, '1619169515', '2');
INSERT INTO `tpshop_fans` VALUES ('95', null, 'odZTlt4mP8g9hSWY2Nc2rmNDx6j8', '八二年的AD钙奶', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicSYfF1aDiajSdGYZpHAKWYgaIDNXxdUbC4H4pVCwT4aG0VIp4OVDT5dSXoVSVLea1aeNzq8YmkWibYDatkeHAqjse/132', '1619161882', null, null, '1619333374', '3');
INSERT INTO `tpshop_fans` VALUES ('96', null, 'odZTlt-wqk6-kWjqgnjD5-Ssbob4', '。。。', '1', '信阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCInwOhp99mV1ebm7KXtzYPEcsibvwL68qibGGL1uJQw7RjCVsdIsGxoo536BFJI20ltJFbWsM5eFS5Q/132', '1619161883', null, null, '1619169515', '2');
INSERT INTO `tpshop_fans` VALUES ('97', null, 'odZTlt6TZVqcAh5V0Ozw0tdiAoq4', '百事可乐', '1', '焦作', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/kiaXicXJs2M4c8w0BfMkibDXKhiahaduGdJqtu69y1HXYxCwIXY6VQzOVoWZ6u6r8tNUYnoBIBkdrvdb3JO3uEmNXw/132', '1619161883', null, null, '1619169515', '2');
INSERT INTO `tpshop_fans` VALUES ('98', null, 'odZTlt6mcEbhNAxh6Wjcc0nNSJ10', 'Wzf', '1', '周口', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lAWDzUDHEziahqVqwDDdicQeibD9XoEiaqc6EkM40iaBEDic1nc1NiadibN04DSBhzmJQPFq95FiaaKp5EicebaTuothiawJBj/132', '1619161883', null, null, '1619169516', '2');
INSERT INTO `tpshop_fans` VALUES ('99', null, 'odZTlt-0bQTnJUH6lRRjgwOVhPwQ', '良子', '1', '', '', '朝鲜', 'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaELXXP6C8ar7GDIj9YVz3T6H2hEmeg43vtUpPMtC5dlzK4WNEOblXhibNlvBgwR57vzakcO4SAoiccPg/132', '1619161883', null, null, '1619169516', '2');
INSERT INTO `tpshop_fans` VALUES ('100', null, 'odZTlt1J79xADAIT1X5TCdtw2iEY', 'F', '1', '', '', '法国', 'http://thirdwx.qlogo.cn/mmopen/ajNVdqHZLLCRkwbXKictXarGerTLysNM3TsdAhKrajUAtKlRQdYRT7ib8eZOK2QNHkDtqePCsLZUAZC9xArf2K7g/132', '1619161883', null, null, '1619169516', '2');
INSERT INTO `tpshop_fans` VALUES ('101', null, 'odZTlt24sd8NfC5DK7RB85CFceiY', 'WM', '1', '', '上海', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKhS8JoIv9tgX1RIySlrtbvWrJ0kroKKeZeFZ3YHJttwAicicD73GXXY9zaia6mpicDdGicyib3wV0OqoybJGjgRKyVDt/132', '1619161884', null, null, '1619169516', '2');
INSERT INTO `tpshop_fans` VALUES ('102', null, 'odZTlt_E4qG6UOKKtFsWQaBNqPIk', '苏莹', '2', '', '', '阿富汗', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKhS8JoIv9tgdF0GGbKyibGV0qd4hg61xb6Ag8nOZNKTSNKmqThHP80xwqRiaClywnAGr8lkeuJlY1Z3Sr5eNwohF/132', '1619161884', null, null, '1619169517', '2');
INSERT INTO `tpshop_fans` VALUES ('103', null, 'odZTlt4o6mrMDnX3PA3CxnvCeF20', 'Villanelle_', '1', '', '', '', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCLXfhvQy21ibz6ZzbSVmxiaUqPNAUkQ76TkUhuHlWflHyPAAVyABjBibwxibRbgTSWYIEeHOic5I4u8tRN8mWAXia84CQ/132', '1619161884', null, null, '1619169517', '2');
INSERT INTO `tpshop_fans` VALUES ('104', null, 'odZTlt8h0zsOA7YD02iia0kQi8eQ', 'Mmy', '2', '开封', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/kiaXicXJs2M4cFniboI0EgVdZIQlgk4m9OENrbNUz92HdRAxISas24payibweKv2mSjxr85lOvw7icFOR0sFdicRsStWzWGuCicScGM/132', '1619161885', null, null, '1619169518', '2');
INSERT INTO `tpshop_fans` VALUES ('105', null, 'odZTlt0H9nwY4DKvBUkIgF-9-wzk', 'log...', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lCpTU0ndJACAUmPHTGOdOAbwtBzfYh7At7BPJzvojQibS8CWcWtXicT7XC5fibAyPC6thzFic6X6C7KCRB2bvM8yKIx/132', '1619161885', null, null, '1619169518', '2');
INSERT INTO `tpshop_fans` VALUES ('106', null, 'odZTlt9RViUxS4mpqux2UTeTTKkg', '小肉肉的帅baba', '1', '商丘', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Q3auHgzwzM5KkicDaLUaONoXUd6sd4432DFLBLbzhbeyK8LG1d4MC6j1rI2DJgibaWkia5nS1CrqYzeRibxfq0pETbNAj5DFWYuOZoicF4CVBUqk/132', '1619161885', null, null, '1619169518', '2');
INSERT INTO `tpshop_fans` VALUES ('107', null, 'odZTlt38r7e5bU6mrvGfNhIpGPpU', '棱角少年', '1', '', '', '阿尔巴尼亚', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKYfiaJ8emXLuvXibyZoStZlmUSZzsdUwNMhibpFdAcksdicVNbjFZMmEeXzYUnfBN2AdUR3HjfvibIV7oJKtIibIASVs/132', '1619161885', null, null, '1619169519', '2');
INSERT INTO `tpshop_fans` VALUES ('108', null, 'odZTlt5B7pirWAtkaY6KqmiFKOB8', '半暖时光', '1', '驻马店', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCJtW0OeMnJhtPx8AZ2MHDLIdZTKpeicg4hAGiaHvGynmM4vrm1g72ZXFic5OHQHQFPzKtpCRFPcFxdQO5K4Vd6N5M2/132', '1619161886', null, null, '1619169519', '2');
INSERT INTO `tpshop_fans` VALUES ('109', null, 'odZTlt-nWZIWSjR9PGd_mo5Urbuw', 'faye_M', '2', '浦东新区', '上海', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKcw5ueAoPsplBpX0CRyGlu0n5sOMhxkwyuwuzEZ5d6RwiaCBOticRxxu78ibPpSOpLtyqoZUbG5Kb9GA8xriaZ3ayR/132', '1619161886', null, null, '1619169519', '2');
INSERT INTO `tpshop_fans` VALUES ('110', null, 'odZTlt8-EwExBKa981HbVxB4zWTU', '开 ', '1', '周口', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicQff0KErZdEkKokQ8UXtU3iaCFYoRk6mSW5aibzaWzc0lb2GgDDucD1ic4A1a1aicKNoHu80cBliaLMvMg4YA0rfzwlv/132', '1619161886', null, null, '1619169520', '2');
INSERT INTO `tpshop_fans` VALUES ('111', null, 'odZTltxYBgHzbrMT4KWEsl85BwWA', '误入琪途', '1', '咸阳', '陕西', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lCpTU0ndJACAU8iaBahAX3qfGnqkYcc6J3vORxj9HgUGSu9N3rQrbuTmyZ88NbBOJoKiaxOCNNkgSdib1BcKIx5BSD/132', '1619161886', null, null, '1619169520', '2');
INSERT INTO `tpshop_fans` VALUES ('112', null, 'odZTltzroENAY0yvhDTtGcZAD7yg', '杜龙飞', '1', '', '迪拜', '阿拉伯联合酋长国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicRrGoyxqB1HuvM7BBYmrdIfnM7f5EdxvIk5yCP6yMJoTB4s1nQcAiam7BK3ZNqfgWYL9YLcV8Gxb41kiaDhSzBX7ib/132', '1619161887', null, null, '1619169520', '2');
INSERT INTO `tpshop_fans` VALUES ('113', null, 'odZTlt-pWP_n5hH3oJfwE2AI5W3Y', 'Hella', '1', '', '不来梅', '德国', 'http://thirdwx.qlogo.cn/mmopen/kiaXicXJs2M4f3vbUvibVOyOSibGgbJjXaaVAFLSEacKq28jGicttfMn6ibylrlWUTgzye3MsxbAA6QMqCej4IhJLC8B6eic5atwPL8/132', '1619161887', null, null, '1619169520', '2');
INSERT INTO `tpshop_fans` VALUES ('114', null, 'odZTlt81-MpWLzmXSzNOMhLHOTls', '霍江龙', '1', '', '', '', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicSuCZPYmXibdcUazP1GiczQQdWVeaOaGPkaaglFhKGwvclKXF4fXSAMbTPPbFSBwqjrhLRib9ckABcHCVv7hrs28Np/132', '1619161887', null, null, '1619169521', '2');
INSERT INTO `tpshop_fans` VALUES ('115', null, 'odZTlt2OPOJwbDO0odDi8AvU8yyU', '繁华', '0', '', '', '', '', '1619161887', null, null, '1619169521', '2');
INSERT INTO `tpshop_fans` VALUES ('116', null, 'odZTlt0XHcVTJH2ysuor_nG2nqWI', '༒࿈漫༙྇漫༙྇之༙྇途༙྇࿈༒', '1', '驻马店', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKjBcoiauGGau7iaK94b3uGwibrjj6VC3McHkjn1NEsBPJx0Ne69XDEA2WEs2oA2reOPMZdIfW7fxNDKc87sVdicb48/132', '1619161888', null, null, '1619169521', '2');
INSERT INTO `tpshop_fans` VALUES ('117', null, 'odZTlt3NVRnFX24GEQ7dBo_eu_zs', '', '1', '洛阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/icwJWPJasRQJY7tbBEwWNyGEquffPO1K6ArsJRuK2RPBkG0CUa9oMxDWgHiajicWU8KlZRhfVEUolzXhsNLQc0icHjHriawdCHAB2/132', '1619161888', null, null, '1619169521', '2');
INSERT INTO `tpshop_fans` VALUES ('118', null, 'odZTlt8k9WN2d_dx_l2YOy8l749M', 'ggxbsjhsbns', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/kiaXicXJs2M4fdZTa67ciaxjlVibkic8Du89SDgal8qlpsMBpNspnz0WbE0TpyenjTsddl2sKY4a3UWcenY9bLDib0G8tJgnGq1ohV/132', '1619161888', null, null, '1619169522', '2');
INSERT INTO `tpshop_fans` VALUES ('119', null, 'odZTlt4GZjBBlTdG1-muDFd4wBdI', '简述MySQL', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lCPmHDB7pZXHcmjnTPXgD4FtT2lFMcABdNbw7YT7MGtpHZRepnyKL2iciaM0lDgAvdT9STvicHlEOIT4AtReyagBhq/132', '1619161888', null, null, '1619169522', '2');
INSERT INTO `tpshop_fans` VALUES ('120', null, 'odZTlt-1TerYPcjNrys2sxpjilOw', '月色太温柔', '1', '周口', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKwpf2yXT0N9cKhGp6UuicRQYkBxPOByc6ZkCt3OWOax6H5nft3mPTB5mCahqnq0MeIBbHwUnRtjH9mSbxHqQ1hic/132', '1619161888', null, null, '1619169522', '2');
INSERT INTO `tpshop_fans` VALUES ('121', null, 'odZTlt1ijs3M6vv2ZhYCG4dylB20', '左左', '2', '信阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/icwJWPJasRQJY7tbBEwWNyDfGN5l9FXn57R2xlm9ibvI6ic5bsPzlaOPKpRLtgoRUx8AAPib6YwpweM5E48pLl0QIvIdNQib1DXPp/132', '1619161889', null, null, '1619169522', '2');
INSERT INTO `tpshop_fans` VALUES ('122', null, 'odZTlt79T5f-_Gr71kpWN_AqXu_E', '呐\'  卢先生', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKjBcoiauGGau5LlrR9OfaPKmicUvvWtpASHknTkEQk6fJTrV4BMKauLU8siaibpJ06bMyzIj2AHtbfh6CF2aIEn3kI/132', '1619161889', null, null, '1619169523', '2');
INSERT INTO `tpshop_fans` VALUES ('123', null, 'odZTlt9b2x0j5lGlItuz-bnB5pGc', '张小博', '1', '周口', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKjBcoiauGGau79QNHHYAQfiaMjC5ve2QKfekzaZrkPibk6qiaVNEMPPyFeSmHLpOw6kC2XuicwdUicg8J6wefQ6cO6aV/132', '1619161890', null, null, '1619169523', '2');
INSERT INTO `tpshop_fans` VALUES ('124', null, 'odZTlt-Xbfo2Edz_cHg6-IV14fkc', '恢复出厂设置', '1', '焦作', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lDT7jZ39pGjltprDWlo75Ukx4w1gp0tGALw6Y3QlnnFB0QZGERoyiavK8l0ZM7QBq7O80xvIsOyENEUP7PqIrfDM/132', '1619161890', null, '1619333609', '1619333609', '2');
INSERT INTO `tpshop_fans` VALUES ('125', null, 'odZTlt08CMSgORSM_0u9LDWDu2s0', '阝曰', '1', '静安', '上海', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicRlMScJnCZcWPDWRicvTsrwFibuC4msuGO0ylnOR9UfAdnSE5FBp1t0PodfU6NBdYkjSKXd2icCq3E3ATS7AyCqxpw/132', '1619161890', null, null, '1619169524', '2');
INSERT INTO `tpshop_fans` VALUES ('126', null, 'odZTlt63o15j-u_THQizq2JLquHg', '无解', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Q3auHgzwzM4VTiceWq1Yf8JibYkO0FS7PibySXRthAEOQ3ibMYkOvtqBia2TXuD7KA1bicoPwfXR3AOY3AQfWf6yHqf1rKicbHSN0Lpicl1O7ThI0r4/132', '1619161891', null, null, '1619169524', '2');
INSERT INTO `tpshop_fans` VALUES ('127', null, 'odZTlt6Om4efSgC5SSRWCx5JAZH0', '奢望', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKPrW4v7kM0PU0dGI157O1NEowtAdBphvepP71wCMmtqYeQM8nWYh4tATm6ppKPhNgeicRMwKgxUhrSr4icpP2RTm/132', '1619161891', null, null, '1619169524', '2');
INSERT INTO `tpshop_fans` VALUES ('128', null, 'odZTlt1D7cxs_ok3-L-wRWH44RoE', '橙子', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/icwJWPJasRQJY7tbBEwWNyBntNDjvaofNtOEwX3tb4V7WboPGJZZrqWHc2EpmUYtOe5fdBWk6mbzOrgFBcz7d6L4qRjblic0jC/132', '1619161891', null, '1619319862', '1619319862', '2');
INSERT INTO `tpshop_fans` VALUES ('129', null, 'odZTlt87QVHZbnCslw1wopvdrM1E', 'asdfghjkl', '1', '商丘', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicS5hqQMrCJ3SQoDRj2aP4W9mHMgRDaCdk5Oibfn4BTA7QLibo0r7pNEZTqwPvaJLMxbxYQbv0VZJIKRF0tDUgfz83/132', '1619161891', null, null, '1619169525', '2');
INSERT INTO `tpshop_fans` VALUES ('130', null, 'odZTlt0D5Kca5IlRNek0B96KMoec', '遇见你', '1', '', '', '阿塞拜疆', 'http://thirdwx.qlogo.cn/mmopen/EUic3NA1JWF4TUweibUibTDKGdhFJPj3IW0l0GtnwxA6SdA3ecl0gzbgUWCDK1fVf9pYj9xxpyZ8luTx8iaXYMIB3TQ3cVZpNPF1/132', '1619161892', null, null, '1619169525', '2');
INSERT INTO `tpshop_fans` VALUES ('131', null, 'odZTlt0MpybKOOLpZ6MZoEi56M6c', 'Mi Manchi', '1', '', '', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicQEWtUoFBJHxc959rolbM6QNjRRr9b5DdYgD1vHzSOgKDKia5Ak5zwmrNQt6f1QoAukmkj9Aazw8nmoRTGsfWOI8/132', '1619161892', null, null, '1619169526', '2');
INSERT INTO `tpshop_fans` VALUES ('132', null, 'odZTlt-NlMQKO8kpGFtghKPNdX6U', '果围', '1', '成都', '四川', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCJJ9qKFApibH9q7JHk51OjMvTgNlMdrDVqvwhNbTunxiaaEDKibpTQz8hRgD2hGg5RicHfPsAWO9shr9JOAQ9PZWiaVb/132', '1619161892', null, null, '1619169526', '2');
INSERT INTO `tpshop_fans` VALUES ('133', null, 'odZTlt5WyC_SGMWIZQ467KBbJiI8', 'Forever丿四少丶', '1', '杭州', '浙江', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicQVjIZuwAicULreT2hwJWTpUWgrQf0PWJpicLicglBoamcoCvlefZSc9bVml5IBZXibOJ7AibANtUgtWudEsXhkXtS2W/132', '1619161893', null, null, '1619169527', '2');
INSERT INTO `tpshop_fans` VALUES ('134', null, 'odZTlt9Byqrlvw_Ly_sz4Te2ndhg', '刘飞', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicRLibYmA8303mkWH1KTkqYyDFh95t3l2kUiaibNnJcNAL7icWF9HukvxzmuvNWoUUXUEQNeibhm6BFXdOneoCFjp2CA3/132', '1619161893', null, null, '1619169527', '2');
INSERT INTO `tpshop_fans` VALUES ('135', null, 'odZTlt62_62lg-UdgcY-uNZ_bCn4', '你好呀！！！', '1', '信阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/reLZO6gF4KTUHgb320KGOaUgXWx5OYeeZMtEjIwUpfRIPOFx70iadchV09YTtV5TTICKickeYtxMXicNGhXReFIDpGcIuYiaShP1/132', '1619161893', null, null, '1619169527', '2');
INSERT INTO `tpshop_fans` VALUES ('136', null, 'odZTlt2PLtBJx6RzMarugScBzHuc', '可儿', '0', '', '', '', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicQcCjwlCZfBH3L7iaOia0GEt2gB7oWHRqwpyQeEsyzkcRxqrHNbwxaYE6f2duBtHw89MUaSpibqicLtI8CGZJtCJMhq/132', '1619161893', null, null, '1619169527', '2');
INSERT INTO `tpshop_fans` VALUES ('137', null, 'odZTlt_Cu07lhapkPVfTvRpDpLRs', 'A', '1', '浦东新区', '上海', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lBICIVib8c8D7tx6gDf1HDwNImkxQcWIHVbo0nkR7SBhoFR0oNuOVBjwz3jsicek3R1Th57fJzBdKrOrcwKyGZRpE/132', '1619161894', null, null, '1619169528', '2');
INSERT INTO `tpshop_fans` VALUES ('138', null, 'odZTlt-v1t97jTIQIfYvi6rdijqg', '吃雨的云', '1', '厦门', '福建', '中国', 'http://thirdwx.qlogo.cn/mmopen/kiaXicXJs2M4cStWgU3dWMl5x0DKyzj3vyX3laLuBKQ2nYP8Umfso5CrHek5qfcaJVaBaf7HfBKqMtvXrQxYM7qG4fXKibjA3TW/132', '1619161894', null, null, '1619169528', '2');
INSERT INTO `tpshop_fans` VALUES ('139', null, 'odZTltxeMMWGl33FSPMGDy4GITfA', 'See you张', '1', '', '', '卢旺达', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicRLibYmA8303mvkwYxTAy8fgJ8s9MtI0ia22hxhjQLFopESkWeUI0BQsVGr0yAibdzoOX9wurABDUddPeu9h6PwHcc/132', '1619161894', null, null, '1619169529', '2');
INSERT INTO `tpshop_fans` VALUES ('140', null, 'odZTlt1oLAtf6KjKhBJKaRGT9_NI', '谁主沉浮', '1', '深圳', '广东', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lCpTU0ndJACAZxaz18AVTkCCRnsAToUytjUuDrDxhSNmTEsmkm9FTwYftnr8N0QuFR6LkaiaTTNtZgEQWKHezMzL/132', '1619161894', null, null, '1619169529', '2');
INSERT INTO `tpshop_fans` VALUES ('141', null, 'odZTlt_rYcZtUEDKClGsipy8ZPW0', 'Slow fever', '1', '', '', '安道尔', 'http://thirdwx.qlogo.cn/mmopen/kiaXicXJs2M4f3vbUvibVOyOUDNV0F98EyvybQ6WbibZRVyOFcmMg98oEIvfW0AHalicFXSKGvdwXbexyaqwT4bOyUstNONCxdGt7/132', '1619161894', null, null, '1619169529', '2');
INSERT INTO `tpshop_fans` VALUES ('142', null, 'odZTltwgQ7aJNwUeqIadcLmE58m0', '还好=', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCIqibic9cicbGudD7CfYjRMW51lG0Nj2h031uTzPd3icJOAbzyZScTJnrmhaxxeQVnIyTjfHBkmszcharbcK3kiaF5fB/132', '1619161895', null, null, '1619169529', '2');
INSERT INTO `tpshop_fans` VALUES ('143', null, 'odZTlt7ayuLB0twrgucdtUaIDXx8', '杨¹³⁶⁷³⁷⁶⁰⁰⁶²', '1', '信阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lCpTU0ndJACAT6hT80RDGjv0LAtrSuib0BTZ05DaHZxKy0Ib3r6jvErmIeWymPF7GmVIuDpveADrgxtv1Bs2XYwG/132', '1619161895', null, null, '1619169530', '2');
INSERT INTO `tpshop_fans` VALUES ('144', null, 'odZTlty99wsg92GIu2mSJHjgZEqE', '夜半小夜曲', '1', '南阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicSg2yRCEdMklBFiaJxO3PovqeWSuiaW9UJ4iaFNXA2EENwkInUmzibNZOnG2LgoJCaISk5VvhuoN9R6voYnV9XdS2sY/132', '1619161895', null, null, '1619169530', '2');
INSERT INTO `tpshop_fans` VALUES ('145', null, 'odZTlt9Q6R-lGG2y2HCyw2txSFj4', '蓦然', '1', '驻马店', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lCpTU0ndJACAewrUOj3Sqd0pvl7uVVWL2Ly9DPHiczicGeLAAk6RpZOSBj7DX62ADtKLedO9ERqTdcHtLa94Z309ib/132', '1619161896', null, null, '1619169531', '2');
INSERT INTO `tpshop_fans` VALUES ('146', null, 'odZTlt-SpLYrzuzonSNva7aOlkho', '殇啊尚', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicRVLwsiboFVAicYwMicvhNd7yKUEPcreq2ZoX3AZiaOdecKajua5Yp2RLRX7M3aSy6KBBA5cPhhyhyBEUOzqjs7QgHf/132', '1619161896', null, null, '1619169531', '2');
INSERT INTO `tpshop_fans` VALUES ('147', null, 'odZTlt4ZLt3CyenO56M_N5mNX2L4', 'ㅤㅤㅤ', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKjBcoiauGGauzwveCDGfJ8REXjGx37nyCcHoqrb48KZVpdsaMJdbmALtdrOd7d1xSLH2SeEggvyPF1XT5SvChPG/132', '1619161896', null, null, '1619169531', '2');
INSERT INTO `tpshop_fans` VALUES ('148', null, 'odZTlt1uajV_UHqpUoOjI5wjfyWM', '小刘', '1', '浦东新区', '上海', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicThTn4m9icKoicDJGVg6A1m2SibuRvvZ3dxkuWKIAHjWkMw7sCdUN3dtfublqAicQX2VKWt8lZibpv5lRpPjylJ4ZY0A/132', '1619161897', null, null, '1619169531', '2');
INSERT INTO `tpshop_fans` VALUES ('149', null, 'odZTlt7xFGi-XZ8EqPd00z57-2uQ', '十七', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicT1Mb5NdbWsAevjAV1vfCZNJNCFcyAKPuMhoOGAu3Ug4rOJjItmzEe24Ftxw69OwTdJBbicA5Avvia7CvtYFHDyxl/132', '1619161897', null, null, '1619169532', '2');
INSERT INTO `tpshop_fans` VALUES ('150', null, 'odZTlty07bPTw_tL1TorwMxTg-LA', '二月三十', '2', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lBscFBwdmd0BAQRjq4IGiaoJ7uiaMicrEKy5srPoc0CpUAxqAuUvibzV7ibfvK2x5yKrsNKnP4St1CSkGQ6NzibmqdpfB/132', '1619161897', null, null, '1619169532', '2');
INSERT INTO `tpshop_fans` VALUES ('151', null, 'odZTlt2kWZ-iL2zDw1w0D_Kv_QZU', '篮球', '1', '松江', '上海', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lCpTU0ndJACAXEKFYeqHZjGXI0qKEWicvXKNLowgiacheCk9snFhcBVWvKxAlbyviaKteYyBergpNsQ5gQvH1TWCR9/132', '1619161897', null, null, '1619169532', '2');
INSERT INTO `tpshop_fans` VALUES ('152', null, 'odZTltzUHtHaYWo4ZM--Io4XEmNc', '975', '1', '南阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicTDkA2hEYcUM4M3rTBVsKsA2DLECAvAdicOXCd7BhPicuJosicIiaLC5trmVj1nArblPGP5maUZU25c434dZyic3034Q/132', '1619161898', null, null, '1619169532', '2');
INSERT INTO `tpshop_fans` VALUES ('153', null, 'odZTlt1TtCIQIxIGKO8LjM9X0ujA', '追梦之星', '1', '惠州', '广东', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lCaa1PxYKOyl0ahURIF6iavJyfQQPia5JhMO4fxhzN5nic3V1SibpK5JRASydbjdAuAzERw6KJ5h5YOIESc5ljpKE44/132', '1619161898', null, null, '1619169533', '2');
INSERT INTO `tpshop_fans` VALUES ('154', null, 'odZTltwunMrtq1xMOHjAN8T-QTYc', '茶香过半', '1', '驻马店', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lCpTU0ndJACAVhkxJugrhtxdtcd3jaVpyKywjSzYjGEFGbDYQ9a7bYeqcaQAFvTBficrlHichFF4tAbIzgWzGFDJE/132', '1619161898', null, null, '1619169533', '2');
INSERT INTO `tpshop_fans` VALUES ('155', null, 'odZTlt2Q5vS_FtubEThIbItDV_2g', '康康', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Q3auHgzwzM63MricxNZgyxXXYU4NB8Mr3ibwYuwwibicnmVNuY31hGDxSeT7Hc1GFJcDddR8xDjh2M7PsE8aCopicBs5Xibp4sjT7CsSOODFxYIzU/132', '1619161898', null, null, '1619169533', '2');
INSERT INTO `tpshop_fans` VALUES ('156', null, 'odZTlt2BeSJYKVnwGICNBtbO6Hgo', '2013@Star涛', '1', '安阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/1suME9rg8lB8zKAxa7WqQfYpTHa9A7AkAI9TTnfzibXcco6mibceia3mq2Doatib1MsBAibgb9jSq0M7RfibYzYJ2ibdG64nN3Hz2oc/132', '1619161898', null, null, '1619169534', '2');
INSERT INTO `tpshop_fans` VALUES ('157', null, 'odZTltwckT29e2dkbucEgaA_1vF0', 'WYS', '1', '郑州', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKhluQ1ldVaMq9ss8w5779ibI9b2pnbRv7f8gX9wl1htBn8xqYIR8VOZRoNV1xDHeicicibbibTwZXDOUojY9X2UDAs8/132', '1619161899', null, null, '1619169534', '2');
INSERT INTO `tpshop_fans` VALUES ('158', null, 'odZTlt2RvcOIqj57b3GyaTcX4REo', '嘉禾望岗', '1', '', '', '智利', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicRLibYmA8303mvBrxhO11Attc8vbkZ0suHWnibJM7ln5mibib00E1g5AF3IJVn8n6tiadTtWhNUzmNNt5qhgBN4zSM1z/132', '1619161899', null, null, '1619169534', '2');
INSERT INTO `tpshop_fans` VALUES ('159', null, 'odZTlt4WKXti766pNrooCDTE62Dc', '？？？', '1', '南阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/ajNVdqHZLLBTibibtzrd8LHfZbQk1ygicwhQcE2ibxXN8TjxACs2zF7RSW1qdbW4nbXXXABqldkhib2Lbj0nhBTeyemtshgKM4P9BcFkWLRyq6hU/132', '1619161899', null, null, '1619169535', '2');
INSERT INTO `tpshop_fans` VALUES ('160', null, 'odZTlt5fx9DFClGPjsorZeTxvYtE', '。', '1', '常州', '江苏', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKjBcoiauGGauzCesMG1gnLD8syiaicw5UrD3UgicRo4pLvklXShIj2QInVKd1hicaAWiciak0KvxWlSS7Ml8Sy61U9W1F/132', '1619161899', null, null, '1619169535', '2');
INSERT INTO `tpshop_fans` VALUES ('161', null, 'odZTlt8qm5ZcRI9tMWO8xG89gR7I', '尔小鱼', '1', '苏州', '江苏', '中国', 'http://thirdwx.qlogo.cn/mmopen/Y7ffx5VPBCKjBcoiauGGau7or3UBHD60Mm29H78LXsBOwk7ADc9yhOpCNaWIVTpU3lvHfyX24XTibffZ9NhgJdSGacnJicyjexo/132', '1619161900', null, null, '1619169535', '2');
INSERT INTO `tpshop_fans` VALUES ('162', null, 'odZTlt2PWlKIazpA6-7K4gE9yAuI', '烟花易冷', '1', '周口', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicRLibYmA8303mtaicDA5icCnoIsk691BZhVjOqzyUicoQ07mnGte4ZwaZ97RFFAgYz9fq1EJIpj3zjz1j0OIcUGszjJ/132', '1619161900', null, null, '1619169536', '2');
INSERT INTO `tpshop_fans` VALUES ('163', null, 'odZTlt38hXNveTSYJ3P56ZlfCHPE', '王直率', '1', '信阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicRLibYmA8303mvzFACicKRM2BUBGtccP8x9sr0dnqP8eRHenEfMc6H5uR8TibJdst3hkJwicI1DNQQcSWflx9qlLEv7/132', '1619161900', null, null, '1619169536', '2');
INSERT INTO `tpshop_fans` VALUES ('164', null, 'odZTlt2Pz80E3C8GFCoo_CKESgkY', '流鲤飞', '0', '', '', '', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicQcCjwlCZfBH15ic4FdgpibxHhnRkWnzvN2gfXSbp5Qibwyaatbu40b5DPP1e6UH5t8Lia571cXG7HdVZ9ibSeeANWVia/132', '1619161900', null, null, '1619169536', '2');
INSERT INTO `tpshop_fans` VALUES ('165', null, 'odZTltwbHxmQe50R5yQeSrkyA_uU', 'ᖇꫀᧁᖇꫀ', '1', '', '', '中国', 'http://thirdwx.qlogo.cn/mmopen/kiaXicXJs2M4diaPSEEG8aicP0XH48D4cDnCOUavAf7lN5dcBTczDibco3owBoBMqHNfvZCrWtkZnicKCA7ticwRcib0ibwUoRWUQDhY0/132', '1619161901', null, null, '1619169536', '2');
INSERT INTO `tpshop_fans` VALUES ('166', null, 'odZTltxrev6l9_pnin5UCQ8Tx4Yo', 'Adorkable', '1', '洛阳', '河南', '中国', 'http://thirdwx.qlogo.cn/mmopen/DvG61tpFGicSeBv4nICGaLgJYDOkFhSmdUyvJlnVvhky5HjjR8dFPfNmzmIeqO8T9wic6eGCbG57MCmxSUVrdaZibum00Uv8UvW/132', '1619161901', null, null, '1619169537', '2');

-- ----------------------------
-- Table structure for tpshop_goods
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_goods`;
CREATE TABLE `tpshop_goods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `goods_name` varchar(255) NOT NULL DEFAULT '' COMMENT '商品名称',
  `goods_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品价格',
  `goods_number` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '商品数量',
  `goods_introduce` text COMMENT '详细介绍',
  `goods_logo` varchar(255) NOT NULL DEFAULT '' COMMENT '商品logo图',
  `create_time` int(11) unsigned DEFAULT NULL COMMENT '添加时间',
  `update_time` int(11) unsigned DEFAULT NULL COMMENT '修改时间',
  `delete_time` int(11) unsigned DEFAULT NULL COMMENT '软删除时间',
  `brand_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品类型id',
  `cate_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品分类id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_goods
-- ----------------------------
INSERT INTO `tpshop_goods` VALUES ('1', '小米1', '1000.00', '100', '小平米好啊', '20210413/7e27a8730b5da0bde7e26eda5f3be2be.jpg', null, null, null, '1', '3');
INSERT INTO `tpshop_goods` VALUES ('2', '小米2', '1000.00', '100', '小平米好啊', '20210413/07285e815432c0fe88123c298640ee7b.jpg', null, null, null, '1', '1');
INSERT INTO `tpshop_goods` VALUES ('3', '小米3', '222.00', '22', '', '20210413/a54d948ba555071d3fb8ef419c787942.jpg', null, null, null, '1', '1');
INSERT INTO `tpshop_goods` VALUES ('4', '小米4', '333.00', '33', '', '20210413/57de873189e8953446420a086156a393.jpg', null, null, null, '3', '1');
INSERT INTO `tpshop_goods` VALUES ('5', '小米5', '100.00', '100', '', '20210413/19ebd9e622c72d1dd35598f4d163d099.jpg', null, null, null, '1', '1');
INSERT INTO `tpshop_goods` VALUES ('6', '小米6', '222.00', '22', '', '20210413/c6c1d105d872c0ce92d409b429c2393c.jpg', null, null, null, '1', '1');
INSERT INTO `tpshop_goods` VALUES ('7', '诺1', '111.00', '110', '', '20210413/3dcec4834c309cd1eef4872c396c8ce0.jpg', null, null, null, '1', '1');
INSERT INTO `tpshop_goods` VALUES ('8', '诺2', '222.00', '22', '', '20210413/348ed579bb964ed474ff8788755281a0.jpg', null, null, null, '1', '1');
INSERT INTO `tpshop_goods` VALUES ('9', '联想1', '4000.00', '100', '', '20210413/31bec377ed61ef8aa2beddd0a284bcc7.jpg', null, null, null, '1', '9');
INSERT INTO `tpshop_goods` VALUES ('10', '联想2', '4200.00', '200', '', '20210413/b48c742ee0cc92dc406376e09269c1f0.jpg', null, null, null, '1', '9');
INSERT INTO `tpshop_goods` VALUES ('11', '联想台1', '3000.00', '300', '', '20210413/4552d86e148916c8b955c31e18f9de00.jpg', null, null, null, '1', '9');
INSERT INTO `tpshop_goods` VALUES ('12', '联想台2', '4000.00', '42', '', '20210413/20eb467f7f61b9ed507174dac8e1e458.jpg', null, null, null, '1', '9');
INSERT INTO `tpshop_goods` VALUES ('13', '华台1', '2000.00', '100', '', '20210413/ee9ee93920f3c42e3ddb4cc212a565e5.jpg', null, null, null, '1', '9');
INSERT INTO `tpshop_goods` VALUES ('14', '小米11 Ultra 至尊', '6499.00', '1111', '小米11 Ultra 至尊 5G 骁龙888 2K AMOLED四曲面柔性屏 陶瓷工艺 12GB+256GB 黑色 游戏手机  ￥6499.00\r\n', '20210414/348f45884c8c97a41f33193d053b8239.jpg', null, null, null, '2', '1');
INSERT INTO `tpshop_goods` VALUES ('15', 'Redmi Note 9 ', '1299.00', '1000', ' Redmi Note 9 5G 天玑800U 18W快充 4800万超清三摄 云墨灰 6GB+128GB 游戏智能手机 小米 红米', '20210414/4014b99dbe0cd0ffb565494ac4041c02.jpg', null, null, null, '2', '1');
INSERT INTO `tpshop_goods` VALUES ('16', 'Redmi K40 Pro', '2799.00', '500', 'Redmi K40 Pro 旗舰骁龙888 三星E4旗舰120Hz高刷直屏 6400万高清三摄相机 杜比全景音 33W快充 6GB+128GB 墨羽 游戏电竞智能5G手机 小米 红米', '20210414/fbf4c05d9de78c197f2c599a650c6fd8.jpg', null, null, null, '2', '1');
INSERT INTO `tpshop_goods` VALUES ('17', 'Redmi K30S', '2599.00', '600', 'Redmi K30S 至尊纪念版 双模5G 骁龙865 144HzLCD旗舰屏幕 8GB+128GB 月光银 游戏手机 小米 红米', '20210414/0bdd545871b44e722b76eecde92143d0.jpg', null, null, null, '2', '1');
INSERT INTO `tpshop_goods` VALUES ('18', '小米11 Pro ', '5299.00', '1111', '小米11 Pro 5G 骁龙888 2K AMOLED四曲面柔性屏 67W无线闪充 3D玻璃工艺 8GB+256GB 黑色 游戏手机', '20210414/5f27fe6b27f6f798fd0c503235e894c9.jpg', null, null, null, '2', '1');
INSERT INTO `tpshop_goods` VALUES ('19', 'vivo iQOO Neo5', '2699.00', '1122', 'vivo iQOO Neo5 8GB+256GB 夜影黑 骁龙870 独立显示芯片 66W闪充 专业电竞游戏手机 双模5G全网通iqooneo5', '20210414/3bc288c6411f8e099f9aaa7c3211c242.jpg', null, null, null, '4', '1');
INSERT INTO `tpshop_goods` VALUES ('20', 'vivo S9', '3299.00', '2000', 'vivo S9 5G手机 12GB+256GB 印象拾光 前置4400万超清双摄 后置6400万OIS黑光夜视 6nm旗舰芯片 拍照手机', '20210414/2340aa812d48ab1c68f4dcc320be620b.jpg', null, null, null, '4', '1');
INSERT INTO `tpshop_goods` VALUES ('21', 'vivo iQOO 7', '4198.00', '1321', 'vivo iQOO 7 12GB+256GB 传奇版 骁龙888 120W超快闪充 KPL官方赛事电竞手机 双模5G全网通vivoiqoo7', '20210414/f5a5e37c47bc35ac631e08f9356981a2.jpg', null, null, null, '4', '1');
INSERT INTO `tpshop_goods` VALUES ('22', 'vivo iQOO Z3', '1799.00', '2211', 'vivo iQOO Z3 8GB+128GB 云氧 高通骁龙768G 55W闪充 120Hz高刷竞速屏 6400万三摄 双模5G全网通手机iqooz3', '20210414/31277707dfbc2b1f144839ded50029e4.jpg', null, null, null, '4', '1');
INSERT INTO `tpshop_goods` VALUES ('23', 'vivo iQOO Neo3', '2698.00', '1541', 'vivo iQOO Neo3 5G 8GB+128GB 夜幕黑 高通骁龙865 144Hz竞速屏 立体双扬 44W闪充 双模5G全网通手机', '20210414/b614f14d5d67393a1b6c20fe515ea8c5.jpg', null, null, null, '4', '1');
INSERT INTO `tpshop_goods` VALUES ('24', 'Apple iPhone 11 (A2223)', '4908.00', '1231', 'Apple iPhone 11 (A2223) 128GB 紫色 移动联通电信4G手机 双卡双待', '20210414/474e6ae70844c30cef9243a7412d907b.jpg', null, null, null, '2', '1');
INSERT INTO `tpshop_goods` VALUES ('25', 'Apple iPhone 12 (A2404)', '6799.00', '222', 'Apple iPhone 12 (A2404) 128GB 白色 支持移动联通电信5G 双卡双待手机', '20210414/2719829b1ecd7523c83ab655e21b9816.jpg', null, null, null, '2', '1');
INSERT INTO `tpshop_goods` VALUES ('26', 'Apple iPad Pro', '6229.00', '2211', '\r\nApple iPad Pro 11英寸平板电脑 2020年新款(128G WLAN版/全面屏/A12Z/MY252CH/A) 银色', '20210414/7976f4e698cf84d0c40380982bbc6f76.jpg', null, null, null, '3', '1');
INSERT INTO `tpshop_goods` VALUES ('27', 'Pro ', '8499.00', '111', 'Apple iPhone 12 Pro (A2408) 128GB 海蓝色 支持移动联通电信5G 双卡双待手机', '20210414/d7e4a448a62d58dc9731024c7eeb1293.jpg', null, null, null, '4', '1');
INSERT INTO `tpshop_goods` VALUES ('28', 'Apple iPad mini', '2921.00', '221', 'Apple iPad mini 5 2019年新款平板电脑 7.9英寸（64G WLAN版/A12芯片/MUQW2CH/A）深空灰色', '20210414/2392b86f51f425d923883a22138b7655.jpg', null, null, null, '5', '1');
INSERT INTO `tpshop_goods` VALUES ('29', '华为 HUAWEI Mate 40', '6999.00', '1152', '华为 HUAWEI Mate 40 Pro麒麟9000 SoC芯片 超感知徕卡电影影像 有线无线双', '20210414/8c998fff8d887a321930a537ca5adcd3.jpg', null, null, null, '3', '1');
INSERT INTO `tpshop_goods` VALUES ('30', '荣耀V40轻奢版', '2999.00', '542', '荣耀V40轻奢版 5G 超级快充 6400万超清四摄 8GB+128GB幻夜黑 移动联通电信5G 双卡双待手机', '20210414/35dc16972f57263514aac5a9063bd21b.jpg', null, null, null, '3', '1');
INSERT INTO `tpshop_goods` VALUES ('31', '华为 HUAWEI Mate 30E', '5299.00', '2145', '华为 HUAWEI Mate 30E Pro 5G麒麟990E SoC芯片 双4000万徕卡电影影像 8GB+128GB星河银全网通手机', '20210414/ba6fca55ae6233bf7559405587163d5f.jpg', null, null, null, '3', '1');
INSERT INTO `tpshop_goods` VALUES ('32', '华为 HUAWEI Mate 30 ', '10999.00', '2214', '华为 HUAWEI Mate 30 RS 保时捷设计麒麟v990芯片OLED环幕屏双4000万徕卡电影四摄12GB+512GB玄黑5G全网通手机', '20210414/f5f7209005b54b2d9f1c8fe1142eff22.jpg', null, null, null, '3', '1');
INSERT INTO `tpshop_goods` VALUES ('33', 'OPPO Reno5 Pro', '3399.00', '654', 'OPPO Reno5 Pro 5G 6400万人像四摄 65W超级闪充 8+128 星河入梦 全网通手机', '20210414/f13679f8b8f0717a2f45c564bf7d565b.jpg', null, null, null, '5', '1');
INSERT INTO `tpshop_goods` VALUES ('34', 'OPPO A93', '1999.00', '1563', 'OPPO A93 8+256GB 雅银 骁龙双模5G 超大存储 5000mAh大电池 18W疾速快充 90Hz高刷屏 4800万三摄 全面屏手机', '20210414/637e0e4e410a2b2fcd17d3334c87801f.jpg', null, null, null, '5', '1');
INSERT INTO `tpshop_goods` VALUES ('35', 'OPPO K7x', '1399.00', '1254', 'OPPO K7x 双模5G 4800万四摄 5000mAh长续航 90Hz电竞屏 蓝影 6GB+128GB 30W闪充 全网通游戏智能手机', '20210414/864a228608f22879744324ca983f9f56.jpg', null, null, null, '5', '1');
INSERT INTO `tpshop_goods` VALUES ('36', 'PPO A52', '1399.00', '1245', 'OPPO A52 5000mAh超大电池 长续航 18W疾速快充 星阵AI四摄 美颜拍照游戏智能手机 8GB+128GB 星耀白', '20210414/c16c7e98c6bc89cd7203a4c24407faaa.jpg', null, null, null, '5', '1');
INSERT INTO `tpshop_goods` VALUES ('37', 'OPPOA8', '1099.00', '1254', 'OPPOA8 多功能AI三摄 4230mAh大电池 6.5英寸水滴屏 美颜拍照智能手机 4GB+128GB 天青色', '20210414/202aa9d189ec11bd0aab9d6aa69d878d.jpg', null, null, null, '5', '1');

-- ----------------------------
-- Table structure for tpshop_goods_attr
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_goods_attr`;
CREATE TABLE `tpshop_goods_attr` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) NOT NULL COMMENT '商品id',
  `attr_id` int(11) NOT NULL COMMENT '属性id',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  `apid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_goods_attr
-- ----------------------------

-- ----------------------------
-- Table structure for tpshop_goodspics
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_goodspics`;
CREATE TABLE `tpshop_goodspics` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) NOT NULL COMMENT '商品id',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_goodspics
-- ----------------------------

-- ----------------------------
-- Table structure for tpshop_joke
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_joke`;
CREATE TABLE `tpshop_joke` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3301 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tpshop_joke
-- ----------------------------
INSERT INTO `tpshop_joke` VALUES ('1', '老总来分公司开会，带了小三生的二儿子一起。会议结束，孩子找不到了。着急就问身边的秘书，你看到我老二了吗？秘书娇羞的说：我是很想看的，可是您一直都没给我机会啊。');
INSERT INTO `tpshop_joke` VALUES ('2', '饺子馆生意不好，老板急招美女促销。来一外国美女，老板问：会说中国话吗？答：会一点点。老板说：你就站门口喊\"水饺，吃碗水饺，十元一碗，来碗吧！洋妞说我去门口试试喊吧\"出去二小时马上生意火爆，吃完的都不肯离去。老板好奇，出去一看，只听那洋妞高喊着：睡觉，睡觉，吃完睡觉，十元一晚，来玩吧！ ！！！');
INSERT INTO `tpshop_joke` VALUES ('3', '昨天升职，三个损友吵着让我请客，那就去吧，吃饭唱歌，一共花了我2000多。今天早上醒了一想，老子升职以后，工资比以前多了两百，这一晚上我就白升职一年了。。。。');
INSERT INTO `tpshop_joke` VALUES ('4', '几个损友去帮一兄弟表白加油，表白进行高潮时，那兄弟说：亲爱的，我会一辈子对你好，给你做牛做马。这时旁边一哥们顿时接上：嫂子，快答应吧，他做牛做马，你只需要给他 草 就可以了。 无声一分钟 果然担得起损友的名称。');
INSERT INTO `tpshop_joke` VALUES ('5', '刚才惹到宝宝了，人家嚷着不要我这个妈妈了，要换个妈妈。她爸爸听见了，贱贱地跑出来对宝宝说：“你说的是真的，我也觉得应该换个老婆。”宝儿嘟着脸走过去，拉着她爸爸低下身子，对着脸就是一巴掌，嘴里还喊着：“你敢换老婆试试看！”笑死我了');
INSERT INTO `tpshop_joke` VALUES ('6', '我找你帮个忙你会答应吗？ 什么忙？ 只要你答应，我给你500块钱 好啊，什么忙？ 借我一千块钱让我应应急');
INSERT INTO `tpshop_joke` VALUES ('7', '前几天出差，今儿个一回去，我就和老婆合力抓到一个小偷，SB一样的躲床底下。居然还把衣服全部脱掉，挡着外面。真TM逗。');
INSERT INTO `tpshop_joke` VALUES ('8', '有一天，跟二货哥们逛街，前面有个妞，腿又长又直！胸也很大，身材超级棒！就商量着谁去要电话，然后二货输了，就屁颠屁颠的跑过去，他就他看了一眼就跑回来了，说了一句特别经典的话，”从后面看能让你精尽，从前面看能让你人亡！”');
INSERT INTO `tpshop_joke` VALUES ('9', '‍‍‍‍一老光棍去寺庙请教禅师：“请高僧指点我怎么才能找到老婆呢？” 禅师仰天长叹：“唉，我要知道怎么找老婆还出来当什么和尚？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('10', '我觉得现在的人都好自私啊。 计划生育的影响，都是独生子女不懂得分享。 那你是独生子女吗？ 不是的，我还有个弟弟。 你弟弟多大了？？ 18厘米！！！');
INSERT INTO `tpshop_joke` VALUES ('11', '儿子今天来办公室耍，一女同事逗他说：好帅哟。儿子也对女同事说：姐姐好漂亮。说完他马上跑我面前认错：爸爸对不起我又撒谎了。儿子脸红得象苹果，女同事尴尬得也腮红赛苹果，一对儿2苹果呀');
INSERT INTO `tpshop_joke` VALUES ('12', '“妈，我要跟我媳妇儿离婚。”\r\n“因为啥啊？”\r\n“我昨天给她讲个笑话，她没笑。”\r\n“就这个不能离”\r\n“她没笑床底一个男滴笑了”');
INSERT INTO `tpshop_joke` VALUES ('13', 'lz兰州夜市摆地摊的，就刚刚，一个女顾客买衣服，她的一个肉嘟嘟的小狗就在后面跟着，我就使劲瞪着那汪，3秒后它怒了，果断冲上来，吓得我拔腿就跑，跑到马路边的栅栏回头看时，它又疯了一样冲上来，我都没地方跑了，幸亏内女的叫了声“甜甜”，那家伙才回头，被一马路的人笑死了，问啥狗，答曰：斗牛。不作就不会死啊，还好我命大。大婶，处女贴，给过吧。');
INSERT INTO `tpshop_joke` VALUES ('14', '儿子生病，半月没去幼儿园，今早送去，老师在门口看到就打招呼:小宝，还认识我不？亲儿子萌萌的摇摇头:我不认识，我不认识你！田老师，我不认识你！');
INSERT INTO `tpshop_joke` VALUES ('15', '汪峰，为了国足，你就跟章子怡结婚吧，必须今晚发布消息，这样，为了不让你上头条，国足肯定会赢的。然而，国足辜负了球迷和汪哥的一片好意，0：2惜败');
INSERT INTO `tpshop_joke` VALUES ('16', 'LZ男 给大家说个真人真事，初中二年级，我同桌家里开副食批发，那天他带了两瓶大啤来上学，两人在上课时间喝完了，尿急了，看着还有几分钟才下课，实在难憋，两人都尿在瓶子里，下课老师走了 我俩静悄悄把瓶子放身后准备去扔了，走在走廊时，本班的一男同学从我身后一把就将我们的“啤酒”抢了，他说：“有啤酒不给哥爽下”。仰头就猛灌一大口，当时附近超过十人有男有女，随后才弱弱的问我一句：“什么酒啊？咸的？暖的！”他看我不说话 隔两秒就吐了 吐的真是天昏地暗啊！直到现在 我们每年同学聚会 当事人 在的话 第一次举杯后同学们都会同时说一句“咸的，暖的。”');
INSERT INTO `tpshop_joke` VALUES ('17', '！从小家长、老师都在教导我们：出门外面，有事一定要找警察叔叔，而且一定要熟记自己家的地址！今天我在家蹲坑忘带纸了，家里没人在，只好拨通的报警电话，并如实汇报了我家地址，可等了半个多小时了，也没等来警察叔叔给我送纸！哼！再也不相信大人们的话了！');
INSERT INTO `tpshop_joke` VALUES ('18', '昨晚男友送我回家，路上一小哥问我们需不需要房间。我们没理他，就在身后喊：都是常客了，咋装不认识呢？\r\n男友一愣，我已经揪住他耳朵：你你，你说，你是和谁一起去的！\r\n拉客小哥笑着走后，男友一边给我暖手一边夸我反应好快。\r\n我连连点头，幸好刚才反应快啊');
INSERT INTO `tpshop_joke` VALUES ('19', '电梯人很多，一美女紧挨着我，电梯里的灯可能接触不良，突然猛闪，还滋滋做响，美女啊的一声抱住我肩膀，电灯随即恢复正常，美女松开手，竟卑夷的看了我一眼，哼的一声把脸扭了过去。妈蛋的，什么意思？我惹了谁了！');
INSERT INTO `tpshop_joke` VALUES ('20', '嘿，屎壳螂。”\r\n“说了多少次，叫我集翔物！”\r\n“hi，避孕套！”\r\n“跟你说多少遍了？叫我拦精灵！”\r\n“嗨，卫生巾！”\r\n“说了多少次！叫我吸血鬼！’\r\n“hi，书包！”\r\n“说了多少次叫我护书宝！”\r\n“嘿，屎壳郎爸爸、屎壳郎妈妈、小屎壳郎！”\r\n“说过多少遍了，叫我们集翔三宝！”');
INSERT INTO `tpshop_joke` VALUES ('21', '下课铃声响了，女孩起身准备走出教室，身后的男孩，见女孩雪白的裤子上一片殷红，急忙脱下上衣围在女孩腰间，女孩惊慌失措的问：你要干嘛？男孩低声说：你那什么来了吧？女孩有所察觉后，甩手就是一个耳光：臭流氓！怎么这么多人就你往这看！');
INSERT INTO `tpshop_joke` VALUES ('22', '　　一富豪和自己的律师讲：我犯了点小错误，正在派出所里，你如果让他们只判我半年的话，你就可以得到一万元的酬金。富豪如愿以偿了，给律师钱的时候，律师讲：这真是一个棘手的问题，本来法官是判无罪释放的。');
INSERT INTO `tpshop_joke` VALUES ('23', '半个时辰前，老夫夜观天象，发现繁星退散，乌云密布，狂风怒吼，雷声阵阵！隐约听到孤魂痛哭，野鬼哀嚎，好似人间马上要面临大劫，既然老夫已参透这天地异象，岂能做事不理，怎能忍心生灵涂炭，人间变地狱，老夫运起毕生所学，仰天大喝一声：赶紧进屋关门，要特么下雨！');
INSERT INTO `tpshop_joke` VALUES ('24', '一次陪同事相亲，见面刚坐下同事就打了个大喷嚏，正尴尬这招呼打的，只见同事默默的抬起头说：对不起我对漂亮过敏。这机智我佩服的五体投地啊。');
INSERT INTO `tpshop_joke` VALUES ('25', '　　前几天一哥们跟我说他两个多月没回家了，一到家洗了一个澡，洗的的正爽，他妈突然推开浴室门看见了哥们的裸体，说了句我终身难忘的话；儿子，我的天！你咋还没开始发育呢！？');
INSERT INTO `tpshop_joke` VALUES ('26', '最近在网上发现了一同性恋网站，本人极度无聊的打算用最好哥们身份信息注册，结果显示该用户已存在。。。');
INSERT INTO `tpshop_joke` VALUES ('27', '明天女朋友来找我玩，我一兄弟看着比我还激动。。。。。。是不是要发生什么！！！！！！');
INSERT INTO `tpshop_joke` VALUES ('28', '今天出门在路上车子被一个妹子的撞了，那妹子说是新手然后说好下午私了可我等了她三个多小时。她一来就说对不起大兄弟刚刚在来的路上又把别人给撞了。。。。。');
INSERT INTO `tpshop_joke` VALUES ('29', '在地铁上听到一段对话:男生摸着女生的脑袋说:\"我真的不知道如果这个世界少了你会怎样。\"女孩子害羞的低下头，用手拉了拉衣角。男生打了一个响指，一脸机智的说:\"厚！说不定它会转的更快一点哦。\"');
INSERT INTO `tpshop_joke` VALUES ('30', '去眼镜店洗眼镜，洗完之后戴上，不禁说了一句：“啊，我的世界又重见光明了！” 结果，眼镜店小哥邪魅一笑，应和道：“等世界再次被黑暗笼罩时，再来找我吧。” 听得人神魂颠倒……（via 小野妹子学吐槽）');
INSERT INTO `tpshop_joke` VALUES ('31', '前段时间做了阑尾手术，过了几天要把肚子上的塑料管拔掉。医生说没有打麻药，拔的时候可能有点疼你忍着点，还说我数到三了就拔。1，2突然感到一阵钻心的疼，医生我们说好的三呢？');
INSERT INTO `tpshop_joke` VALUES ('32', '某男在女友脖子上吻了个草莓，被七岁的小侄女看到。 侄女说：“阿姨，你脖子怎么回事啊？” 不好意思的回答：“被狗咬的。” 侄女惊讶的说：“啊？那你打针没有，会得狂犬病的。” 某女淡定的答：“打了，当时就打了！”');
INSERT INTO `tpshop_joke` VALUES ('33', '‍‍‍‍以前我问女朋友：“你以前找过几个男朋友？” 前女友：“你别管我找过几个男朋友，我发誓你是我最后一个男朋友。” 听后非常高兴，心里暖暖的…… 只是三个月后的今天，她成为了别人的新娘。 她确实没有骗我，我只是她的最后一个男朋友……‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('34', '‍‍历史老师问：“小明，列举下中国近代史1952--1999年发生的三件大事吧！” 小明说：“1960年我爸出生，1962我妈出生，1990一个神一样的伟人，我诞生了！” 老师生气的说：“谁问你家的事了。” 小明说：“我家不是在中国吗？” 老师彻底晕了！‍‍');
INSERT INTO `tpshop_joke` VALUES ('35', '‍‍‍‍有一位神经病院的医生问病人：“假如我割掉你的一只耳朵，你会怎样？” 病人回答：“会听不到声音。” 医生听了：“很正常。” 医生又问道：“如果在割掉你的另外一只耳朵呢，你会怎样？” 病人回答：“我就看不到东西了。” 医生着急了：“为什么看不到啊？” 病人在说：“因为眼镜掉下来了啊。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('36', '小明：师父当年我放生了一条鱼，它对我说二十年后会报答我的，现在都二十年了，怎么什么也没发生啊？\r\n师父：什么也没有发生？你以为你是怎么活到二十岁的？');
INSERT INTO `tpshop_joke` VALUES ('37', '刚才老公一手搂着我一手搂着女儿躺在床上，我问感觉怎么样？女儿抢着说：一个男人有一个女人是幸福，一个男人有两个女人是痛苦。我和老公顿时哈哈大笑，宝贝你才五岁啊，五岁！ 还能再让你看电视吗？');
INSERT INTO `tpshop_joke` VALUES ('38', '星期六日不上课，然后说好跟社团的人一起去郊游，人到齐了，出发。刚走了十几米，一个男学弟过来跟我搭讪：美女学姐，你看，我新买的山地，不错吧。我说：不错不错，快不快？他说：恩，很快的。说完就一个劲猛蹬。走出了老远还回头跟我们说：快吧。还没说完只见这货兴奋的一不小心就撞到了树上...');
INSERT INTO `tpshop_joke` VALUES ('39', '师父，为何您绰号凋零？是因为性格孤傲，还是剑法无情？” “这是个悲伤的故事。” “师父请讲。” “那年我和师妹即将出师，师父在桃花园中考校我们的功夫，当时考的是剑气摧花。我师妹一剑出鞘，十尺之外，二十朵花瞬间凋谢，因此得名凋二十，而我……” “师父，我能退学费吗。”');
INSERT INTO `tpshop_joke` VALUES ('40', '就昨天，寝室一哥们儿在浴室洗澡，刚好碰上辅导员查寝，辅导员是个大四的女实习生，我开玩笑对洗澡那哥们说：快出来，有妹子来了。辅导员听了对我笑了下，结果没想到那二货室友居然在浴室里大喊一声：谁？老子干死她！');
INSERT INTO `tpshop_joke` VALUES ('41', '　　刚和老公拌嘴，此为背景。—— 　　我:等会不许吃我炒的菜。 　　老公:我才不吃你炒的菜呢。 　　我:那就好。 　　老公:我吃我媳妇炒的菜，你是谁啊。老实交代，你是哪个妖精附了我媳妇的身。 　　好吧，我竟无言以对了');
INSERT INTO `tpshop_joke` VALUES ('42', '昨天去药店买药，看到一对夫妇买燕窝补品，像咱这种屌丝从来没吹过这种东西，可是我在想，第一个发现它是补品的人是怎么想到它能吃的，燕子刚刚筑好的窝，让你拿下了吃了，有没有考虑过燕子的感受，盖好窝，出去找母燕子，可是一回来窝没了?一脸不相信的跟母燕子说：请你相信我，我真的有房，真的有房……');
INSERT INTO `tpshop_joke` VALUES ('43', '我对着工作了一天回来的老公说：“明天我来烧晚饭给你吃吧！”结果，他竟然说：“我做错了什么？？你要毒死我？！”');
INSERT INTO `tpshop_joke` VALUES ('44', '每晚都能在家里听到隔窗传来动听的古筝声，那天突发奇想推开窗喊了一声，你弹得真好！幻想着从此跟一古典型MM爆发一段浪漫的邂逅之恋...顿了顿，一个五大三粗的爷们声响起：谢谢啦....');
INSERT INTO `tpshop_joke` VALUES ('45', '有一次在地铁上跟老妈打电话，我是站着的，旁边两侧都坐满了人，由于我平时出门老喜欢摸自己口袋确认手机和钱包是否健在，跟老妈说到一半又摸上了，发现只剩钱包摸不到手机了，我一下慌了，车里比较吵，所以我大声说：“妈，我手机丢了，先挂了！”然后东翻西翻，最后定神一看，在自己手里握着。全车人都在看我，我顿时石化。');
INSERT INTO `tpshop_joke` VALUES ('46', '去五哥家，他正在教训5岁的小侄子…五哥:“小子，你尿床了，知道吗？”…“知道！”…五哥:“为什么尿床？”…“妈妈难受，好几天没好好睡觉了，我不想麻烦妈妈给我开灯，所以就尿床了！”…生病中的五嫂:“你可真是心疼你老娘啊！亲儿子！！！”…');
INSERT INTO `tpshop_joke` VALUES ('47', '老爸想吃煎饼，给提供送货的小卖部打电话：你家有没有煎饼？售货员：有。老妈忽然插话：咱家冰箱里还有呢，不用订！老爸对着电话：嘿嘿，我们家也有！');
INSERT INTO `tpshop_joke` VALUES ('48', '今天在洗澡池，旁边一老大爷忍不住放了一个屁。。。。。一个小学生看到后哭着对他爸比说；爸快跑啊！水开了！。。。。。。。。我被他的无邪打败了。。。。。');
INSERT INTO `tpshop_joke` VALUES ('49', '今天两岁的儿子拿刀玩，我不就说了两句，打了一下吗？我那婆婆说我发什么脾气，按他们六十年代生了那么多还不是自己带，凭什么我两个都带不了，我想说，那时候你有老公在身边，至少会帮忙吧，还有一些啊婆也帮你看下，我现在有谁帮我看，老公不在身边，天天看两个，还要做饭给你们吃，任我的孩子在那哭，让你帮我看一个，你就到处说你看不了，还跟别人说我怎么回事，连两个孩子都看不了，我嫁来你们家，煮饭带孩子，在我自己家都没这么累过，我是为了谁家！');
INSERT INTO `tpshop_joke` VALUES ('50', '说个大学时的事！我一个安徽的同学，第一次来出远门上学，根本不知道春运的恐怖！放假后玩了几天才打算回家过年，根本就订不到票了，最后可算买到了一个站票了。进车就是人挤人啊！根本不用担心惯性摔倒！站了一会儿觉得一只脚累了，就抬起来缓解一下，但是他放下的时候发现，原来站着的地方被别人的脚占了。他也看不到地面，都是人，就开始摸索着找找别的立脚点，但是怎么也找不到。他就和占了他地的人说，你能不能挪挪，把原来的地方给他！那个人就开始挪，挪了半天也没找到别的地方，只好跟我同学道歉了！之后我同学就一只脚站到了家！期间他尝试把两只脚互换一下，都失败了！他也他怕那只脚一抬起来，人就只能飞着回家了！');
INSERT INTO `tpshop_joke` VALUES ('51', '小王离异，一个人带着儿子艰难度日。最近，他发现刚上一年级的儿子有些早熟，总盯着街上的女人看。“哇塞，那个阿姨的腿又长又直，如果穿上黑丝袜，肯定会好看。“小王有些生气地打了一下儿子后脑勺：“你才多大，懂这么多！”儿子蔑视地看了他一眼：“你懂什么，我在给自己海选后妈呢。”');
INSERT INTO `tpshop_joke` VALUES ('52', '我姓史，想改名字。冷友们能不能帮我想想办法');
INSERT INTO `tpshop_joke` VALUES ('53', '“如何把胖说得好听点？”“这么些年来，我家里添置的最贵重的大件，是你。”');
INSERT INTO `tpshop_joke` VALUES ('54', '今天逛街突然拉肚子，急急忙忙找厕所，被一女的骑电动车蹭到了，一下没忍住，那女的边哭边打电话，爸我撞人啦，把他屎都撞出来啦。我想哭');
INSERT INTO `tpshop_joke` VALUES ('55', '听说有个女生上课时因为脸大觉得很累，然后把脸搁在桌子上被老师发现，问她：“这位同学，你的桌子呢？”');
INSERT INTO `tpshop_joke` VALUES ('56', '我喜欢香蕉，可是你给了我一车苹果，然后你说你被自己感动了，问我为什么不感动。我无言以对，然后你告诉全世界，你花光了所有的钱给我买了一车苹果，可是我却没有一点点感动，我一定是一个铁石心肠的人！我的人品确定是有问题的！我只是喜欢香蕉而已啊。。。——借此言送给所有对爱情盲目执着的人！');
INSERT INTO `tpshop_joke` VALUES ('57', '以前我们班有一个特损的老师骂人不带赃字，有一天她走到两个同学中间说：我的左边是条狗我的右边是只猪。 那么问题来了：请问猪和狗中间的是什么？');
INSERT INTO `tpshop_joke` VALUES ('58', '那天和男盆友去开房，因为好久没做了，所以比较放的开，叫声估计也比较大，正嗨的时候，一个男人过来敲门，大声问，妹子，多少钱一晚上啊，这么卖力，我是609的，一会你自己过来啊！');
INSERT INTO `tpshop_joke` VALUES ('59', '坐公交车，对面坐个穿短裙的美女。邪念一生，假装系鞋带偷看美女的内裤，可看了几眼，没看出颜色，事后才反应过来，美女，你的内裤呢？');
INSERT INTO `tpshop_joke` VALUES ('60', '女同事在电脑前抱怨：“都好几天了，怎么还不来还不来？”新来的实习生突然接道：“你别吓我……你当时说不是安全期吗？”“我在查快递……”“噢……”现在办公室里的空气有些安静。');
INSERT INTO `tpshop_joke` VALUES ('61', '前几天接到一陌生短信“有人给我钱让我做了你，我给你打过去点钱，你吃顿好的，当断头饭吧。”接着卡上多了2000，那个怕啊，想着是得罪谁了，晚上都不敢出门。后来朋友给我打电话，问我钱收到没，说还我的钱，给我开个玩笑。心中轻松的同时仿佛有上亿只神兽奔过，还你妹啊还，都快被吓死了...');
INSERT INTO `tpshop_joke` VALUES ('62', '现在对于帅哥的定义怎么这么让我蛋疼啊！整天没事照镜子挤眉弄眼腿细的跟牙签似的伪娘也叫帅哥？你丫的啊！帅哥应该是在阳光打篮球挥汗如雨不拘小节的呀！拿衣袖擦汗我也不介意的呀！穿个运动短裤俩腿毛我也觉得是男子汉啊！去你妈的刘海美瞳增高鞋，发膜眼线紧身裤啊啊啊！阳刚点呐你丫的！');
INSERT INTO `tpshop_joke` VALUES ('63', '一女同学多年不见，从牙签妹变成一大胖子。今天在街上遇见我和另外一哥们，我们都很吃惊！沉默了几秒钟，那哥们说：“都说时间是把杀猪刀，怎么到你身上就变成猪饲料了？');
INSERT INTO `tpshop_joke` VALUES ('64', '武松去看武大郎，看见武大郎用针刺破避孕套。武松问：“你这是搞破坏呀，避孕不成功就成人。”武大郎说：“西门庆这色狼就是戴着它有恃无恐地侵害我的老婆的。”武松说：“避孕套破了，潘金莲会怀上他的种呀。”武大郎说：“我就是要取证去告他呀，到时我就说这孩子是西门子，铁证如山，看他还抵赖。”');
INSERT INTO `tpshop_joke` VALUES ('65', '小时候过中秋，妈妈总给我讲嫦娥的故事。但我只想着月饼，对嫦娥不感兴趣；谁料想现在过中秋，对月饼是没什么兴趣了，心里老惦记着嫦娥！');
INSERT INTO `tpshop_joke` VALUES ('66', '和一MM相亲第二次见面。MM有意显摆她的‘爱疯’手机，俺装着没玩过爱疯的样子向她请教各种功能。突然发现她通讯录里有个叫“备胎”的人，突然想逗逗她，问她：”你朋友还有姓备的人啊？“MM（很紧张）“快把手机还我”我估意假装不给，她在抢的过程中不心小拨了出去，我的手机就响了[衰]');
INSERT INTO `tpshop_joke` VALUES ('67', '在食堂碰到极品大爷。。。发飙了。。。冲着食堂管理员大骂：瞅你们那粥什么jb玩意，带个口罩都tmd能喝进去。lz当时就喷饭了');
INSERT INTO `tpshop_joke` VALUES ('68', '他爱上了班花，茶饭不思，无时不刻念着她的名字。室友们发现了他的变化，其中一个找到他，教给了他一个追求的方法。果然，当他拿着那份由五十，二十，十块钱折叠成的“521”找到她时成功在她那里留宿了一晚。他回到寝室，对室友感恩戴德。室友不过微微一笑，“呵，我们几个平常都是六十。”');
INSERT INTO `tpshop_joke` VALUES ('69', '某男邋遢惯了，独居了很长一段时间。总算有一日牵着女友来玩，女友站在房门前犹豫半天。某男：“怎么不进?”女友观察良久，道：“无处下脚！”某男：“你跟我身后，踩着我的脚印进来，不会跌跤的！！！”');
INSERT INTO `tpshop_joke` VALUES ('70', '轮船遇险后正在下沉！一位旅客却若无其事地吃着面包。船长问：“先生，船都要沉了，你怎么还在吃东西？”旅客说：“医生嘱咐过我，千万不要空腹喝水！”');
INSERT INTO `tpshop_joke` VALUES ('71', '今天和一个暗恋很久的女神在一起逛街。看到二货朋友 就上去打了个招呼,他看到我们,问我:这是你女朋友啊。我上去就是一巴掌:谁TM让你剧透的');
INSERT INTO `tpshop_joke` VALUES ('72', '人丑就要多读书，不想学习的时候，拿镜子照一照……后来那么一照，提前辍学了。丫的长那么帅读个毛啊');
INSERT INTO `tpshop_joke` VALUES ('73', '‍‍‍‍一人卖跳蚤药，路人问：“怎么用。” 卖药人答：“抓住跳蚤，以药涂其口，即死矣。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('74', '‍‍‍‍在网吧上网，快中午了，有个小伙跑来问：“谁要快餐。” 我就要了一份，结果说：“要先付钱。” 我就给了，结果到两点都没送来。 这是不是又是一种发财路子，网吧让骗了十几个人。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('75', '隔壁囚牢的老大死刑前一天晚上交给我一个锦囊，叮嘱我混得不好就打开。　　多年后，我被迫打开锦囊，里面写着：兄弟，混得不好就来找我。');
INSERT INTO `tpshop_joke` VALUES ('76', '饭馆服务员刚才告诉我，隔壁桌的老夫妇是老顾客了，从很多年前小饭馆刚开业，夫妇二人就经常过来，每次都打包一两个菜带走，数十年如一日从未间断。　　我听了很受触动，没想到一对老夫妇能相濡以沫的恩爱几十年，竟然还是不会做饭。。。');
INSERT INTO `tpshop_joke` VALUES ('77', '一对恋人在通信时附庸风雅，乱用词汇，结果闹出了一个大笑话。　　男的写道：“亲爱的，想我们不久前还素不相识，可如今已经熟视无睹了……”　　女的复道：“亲爱的，你说得太好了，我不仅对你熟视无睹，而且还横眉冷对哩！”');
INSERT INTO `tpshop_joke` VALUES ('78', '读大学时喜欢一个MM，面容甜美，身材姣好，当时没敢追，但关系不错，天天姐姐姐姐的喊。5年后，故地重游，去看望她。去前发了个短信，说：“姐姐，5年没见了，你还好吗？好想念你，你都在我心里上演了无数部电影了。”结果姐姐回了一句：“不是想电影吧？：）”　　我：……（~~~~(>_');
INSERT INTO `tpshop_joke` VALUES ('79', '昨晚，坐最后一班公车回家，两个小鬼黏在门边一根柱子旁，卿卿我我。　　“你爱不爱我？”女的问。　　“爱爱爱，爱你爱到两根香肠两瓶红酒。”男的回答。　　“什么意思嘛！”　　“就是爱妳爱到长长久久（肠肠酒酒）。”　　“那你爱不爱我？”这次轮到男的问。　　“当然，我爱你爱到厕所里面挂闹钟……”女孩顿了一下。　　“什么嘛？”男孩装出**样。　　“就是爱得有始有终（有屎有钟）');
INSERT INTO `tpshop_joke` VALUES ('80', '一对“80后”大龄男女相亲，互相介绍了自己的基本情况之后，女方坐不住了：“你没房没车来相什么亲啊？”　　男的答：“献爱心来了。”');
INSERT INTO `tpshop_joke` VALUES ('81', '小学考填句子，麻雀虽小，（）。憋了半天不知道写啥，感觉空着不好，于是写了个“可以烧烤”。至今难忘啊。');
INSERT INTO `tpshop_joke` VALUES ('82', '‍‍‍‍‍‍有次和同学去商场买东西，我先出来了。 在门口等了半天没见人出来，我就进去了找，他还在货架上翻翻找找的。 我就火了，冲他喊：“搞毛啊，不是买好了吗？” 他往对面指了指，小声地说：“看。” 尼玛，一个美女穿着小短裙叉着双腿对着我们。 然后我也假装翻翻找找，就这样我们弄了半小时直到那女的换了姿势。 想想当年真是纯啊！‍‍‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('83', '突然发现一个特别奇怪的现象。男生普遍在卖鞋，女生普遍在卖化妆品。而我只能卖萌和装疯卖傻。');
INSERT INTO `tpshop_joke` VALUES ('84', '‍‍‍‍‍‍我们这有个老头看孩子，就把手放孩子眼上，等会孩子就睡了。 N年后这孩子找个男朋友，男朋友学电视里的桥段要给她惊喜。 手蒙上她的眼睛废话完一堆甜言蜜语之后说：“321，睁眼。” 你们猜的没错，她睡着了！‍‍‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('85', 'Pol.ice在广场旁边查酒驾，一哥们喝酒开车被警察扣住了。正要测酒精。。。那哥们机智的跑进广场舞人堆里跟着跳了起来。。。\r\nPol.ice过去一下就发现他了，因为他跟不上舞步，然后就把他带走了。。。\r\n给旁边俩老头吓坏了： “妈呀，跳不好得被抓走啊！”');
INSERT INTO `tpshop_joke` VALUES ('86', '这两天宿舍四个人，三个人都在玩电脑，就去一个在认真看书，我都被我这种行为感动了，哎，我还是再打电话催下，我电脑到底什么时候能修好。');
INSERT INTO `tpshop_joke` VALUES ('87', '昨天去面试，和主管聊得挺好。正好打个喷嚏，主管很绅士的从兜中掏出两团纸，我擦完嘴巴又擦了手，一脸的感谢~。只见他突然惊呼到：“哎呀卧槽，我自己擦鼻涕的两团纸呢？？？卧槽~卧槽~卧槽~！');
INSERT INTO `tpshop_joke` VALUES ('88', '本人从小就晕车，身体素质一直没好起来，大学毕业后出来工作，好不容易泡到个美眉，约好一起去公园玩，下车的时候实在是忍不住，找了棵树蹲下就吐了个稀里哗啦，那美眉还远远的看着，我说:你好歹给我拍拍呀！只见那美眉拿出手机对着我PIAPIAPIA的一直拍~~唉，尼玛吐得正难受呢，被她这么一弄，更难受了！');
INSERT INTO `tpshop_joke` VALUES ('89', '就刚刚给附近一女的发信息说我二十五了。\r\n　　她问我是谁?\r\n　　我说雷锋!\r\n　　她来句快了!\r\n　　我问啥快了?\r\n　　尼玛她说雷锋二十八就死了。。。');
INSERT INTO `tpshop_joke` VALUES ('90', '我睡觉流口水，枕头上都好多口水印子。今天拿出来晒晒，感觉厚了好多，就拆开打算洗洗。拆开一看，我的荞麦枕头居然发芽了…');
INSERT INTO `tpshop_joke` VALUES ('91', '　　提示所有开车或坐车的朋友们，安全带一定要弄好，它可能会救你一难啊。 　　周三，和同事开车出去。到了目的地，他开门就下车了。我的安全带绑得太牢靠了，一时没解开。 　　同事刚下车，就被一辆逆行的小车给刮伤了。我坐在座位上，绑着安全带，愣是没能伸手拉他一把！');
INSERT INTO `tpshop_joke` VALUES ('92', '上周本人出差，把心爱的狗狗交给男盆友看管，狗狗已怀孕，此为背景，一周后回来，男盆友说狗狗怀孕了，我本想告诉他狗狗交给你之前就怀了，话还没说出口，只听见他说:“我就和它睡了一晚上，一晚上。。');
INSERT INTO `tpshop_joke` VALUES ('93', '将近30岁才结婚生子。 这天，我约隔壁邻居带他的女儿，一起去抓阄。 人家小女孩儿抓了一只画钢笔，一看以后就是知识分子啊。 我那个羡慕啊。 我家那小子竟然抓着小女孩子手不放， 心里那个感慨啊，确实比他爹强多了。');
INSERT INTO `tpshop_joke` VALUES ('94', '关于用手机，是这样的！你用个诺基亚的棒棒机，开着宝马，别人一样会觉你是土豪，你用个苹果6骑个破电动车，别人一样会觉得你是个吊丝！就像我！我用的棒棒机，但我却没有宝马。。。。');
INSERT INTO `tpshop_joke` VALUES ('95', '病人：大夫啊，我最近总是梦到同样的梦，这到底是怎么了？大夫：你梦到什么啊？病人：我总梦到我走到了一扇门边，于是我就推啊推，就是推不开！大夫：门上有什么？病人：有一个拉字……');
INSERT INTO `tpshop_joke` VALUES ('96', '小时候，如果出门在外，没有地方小便的时候。妈妈会拿一个矿泉水瓶子， 让把伸进去尿。长大后，偶尔想回味一下那份童趣，却发现再也伸不进去了。不禁一阵怅然，时光一去不返，这就是残酷。。。这段话得到了很多人认同，直到出现了一个神回复：ˇ试试脉动？');
INSERT INTO `tpshop_joke` VALUES ('97', '一对男女因为丈夫的外遇问题而争吵起来，男友辩称他不过是逢场作戏。但他的女友毫不放松，厉声道：“可你演的是连续剧呀！”');
INSERT INTO `tpshop_joke` VALUES ('98', '和朋友在一家餐馆聚餐，刚上一盆炖排骨，不够熟。一哥们大喊:“服务员，看你们炖的肉都咬不动，把你们老板喊过来！”服务员急忙问道：“你想让我们老板干嘛？你们也不想想，你们都咬不动，我们老板肯定也咬不动啊！”尼玛，想象这也很有道理啊。');
INSERT INTO `tpshop_joke` VALUES ('99', '和朋友我们俩人去买饮料，想买瓶激活和脉动，结果一激动说成：“老板，来两瓶激动！”老板愣了一下，然后递给我两瓶激活和两瓶脉动说：“小伙子，自己回去家兑去吧！”');
INSERT INTO `tpshop_joke` VALUES ('100', '老爸叫我去街上买桶装鱼饲料。 买完回家，我上了一辆公交车，由于很重，我把桶放在过道上。 一妹子上车，没座位，就坐在桶上。 我到站了，于是我很礼貌地说：姑娘你把屁股抬起来一下，我要桶。 只见那女的脸都绿了，说：看起来斯斯文文的，没想到是流氓。 我要桶怎么了？我哪流氓了？');
INSERT INTO `tpshop_joke` VALUES ('101', '朋友俩人对话。 —A对B说：我看到你女朋友和一个男人走一起。 恩，那是我给她 找的保镖你不知道吗？ 那为什么搂着你女朋友？可能是加强防护。 那为什么去宾馆？ mlgb你不能给我留个台阶吗？');
INSERT INTO `tpshop_joke` VALUES ('102', '问：“如果你在图书馆看书，正看得入迷，对面的女孩用脚碰了你三次，你怎么办？” 答：“踩住！”');
INSERT INTO `tpshop_joke` VALUES ('103', '去驾校学车，　　教练带我们三个学员练大路，　　女学员很紧张不敢加速，　　教练骂她，你考什么驾照啊，　　这速度不如骑电瓶车！妹子斗胆开始加速，　　越快越紧张，　　教练指着前面一横穿马路的行人说，　　“看到没？”，　　“看到了”， 　　“碾过去”， 　　“啊？我不敢！”，　　教练一脚刹车，　　吼道：“不敢还不刹车？”');
INSERT INTO `tpshop_joke` VALUES ('104', '我有一同学非常见义勇为，　　每次别人做错了事，他总是自己承担。　　有一天我不小心踢到了班长，　　班长大怒，喊道：“谁干的！”　　那同学对我点了点头，　　我以为他又要挺身而出，　　没想到他说了一句差点让我吐血的话：　　“刚才那事不是人干的！”');
INSERT INTO `tpshop_joke` VALUES ('105', '学校门口有两个水果摊，　　一女生在一摊买香蕉，　　挑的时候不停用手捏香蕉，　　估计是看熟不熟。　　这时旁边摊的小贩小声地说：　　同学，到我这边买，我这边的香蕉硬。');
INSERT INTO `tpshop_joke` VALUES ('106', '今天跟姐妹去拍写真，　　看到一对情侣打算去拍婚纱。　　两人都长得不错。　　然后听见那女孩问工作人员，　　我两门牙有点暴，　　你们修照片时候可以修掉吗？　　就听见她男友在旁边狂笑，　　女孩又对工作人员说，　　如果缺了颗门牙可以补么？　　工作人员懵了，　　女孩用余光瞄了下她男友说，　　因为等下他会少颗门牙……');
INSERT INTO `tpshop_joke` VALUES ('107', '有一位政治老师特狠，　　他把全班同学的证件　　照拷到了班上的多媒体里。　　然后谁没完成作业没背　　书就马上点开那个人的照片，　　进行无限放大二十秒。　　效果特别显著，　　基本上几次下来大家　　都把作业按时抄好了');
INSERT INTO `tpshop_joke` VALUES ('108', '今天下了鬼魂探测器来玩。　　居然检测到卧室里就　　有个鬼魂，还是复仇之魂。　　女友吓尿了：“这叫人晚上怎么睡觉啊？”　　我对她灿烂一笑：“没事，等你把妆卸了，　　该吓尿的就是它。”');
INSERT INTO `tpshop_joke` VALUES ('109', '一劫匪绑架了一富二代。　　富二代劝劫匪弃暗投明，　　劫匪听着就把当自己经历全说出来，　　声泪俱下，情到深处，　　劫匪用枪指着富二代说：　　“听了这么多，你感动么？”　　富二代使劲摇头：　　“不敢动，不敢动。”　　劫匪：“再问你一遍，你感动么？”　　富二代还是摇头：“不敢动，不敢动。”　　结果劫匪一枪打死富二代，　　道：“有钱人真绝啊！”');
INSERT INTO `tpshop_joke` VALUES ('110', '写论文的苦逼室友趴　　在桌子上说　　“我憋了半天才８１个字啊！”　　我正准备安慰两句，　　她突然开心的说：　　“掐指一算也有十五分之一了呢，　　听起来好多哦！”　　被她的机智深深的折服了！');
INSERT INTO `tpshop_joke` VALUES ('111', '我真是一个天才， 开车一下就都学会了， 马上就出师了。 只剩两样东西不会了， 连严格的教练都夸我着呢。 教练说，行啊，你可真行啊，这也不会那也不会，我没东西可以教你了，你走吧。 看，我都能上路了。');
INSERT INTO `tpshop_joke` VALUES ('112', '‍‍‍‍大学刚毕业时，一同学说：“要找一个好工作，管吃，管住，高工资，高待遇的。” 20多年过去了，今天遇见他，他说：“我的理想实现了一半，只有管吃管住没有高工资高待遇。” 一打听才知道，他毕业后拦路抢劫，重伤人命，被判死缓，今年刚放出来。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('113', '　　老公：假如我出轨了，你会怎么惩罚我呢，老婆！ 　　老婆：我让你卧轨');
INSERT INTO `tpshop_joke` VALUES ('114', '‍‍‍‍月头生活费到了大花特花，结果到了月中就没多少了。 厚着脸皮打电话给妈，想救济救济我点。 结果我刚说完妈就紧跟着说：“缺钱啊，那我挂了，给你省点电话费。” 我：“……” 这是亲妈啊！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('115', '晚上女友在电脑上下象棋玩。我在沙发看电视剧。突然女友喊到：老公，为什么我的相不能过河啊？ 我沉默了一阵子，说：因为对面和你下棋的不是你老公。他不能容忍你这么过分的要求……');
INSERT INTO `tpshop_joke` VALUES ('116', '第一次和男友生米煮成熟饭的时候，因为都是第一次嘛，我们都很紧张和激动，男友说:我们是不是先洗干净啊，水太多了吧，我:不多啦，每次我看我爸妈做都是这么多水。男友:哇，好白啊。我:当然啦，我们家一直买的东北大米。男友:嗯，我一定要吃两碗…');
INSERT INTO `tpshop_joke` VALUES ('117', '‍‍‍‍一天，二笔损友和我聊天，他突然对我说：“我想出了一个绝妙的想法，能使咱们国家富强。” 我很好奇，二笔损友接着说：“把吃穿住医等等物价，上涨一万倍，使穷人饿死冻死病死，剩下都是富人了，国家就富强了。” 我说：“照你这么说，咱俩为了国家富强去死吧。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('118', '‍‍‍‍老婆：“老公我去隆胸吧？” 老公：“好啊！” 老婆：“你果然嫌弃我胸小，哼！” 老公：“……” 老婆：“老公我去隆胸吧？” 老公：“别去了。” 老婆：“你果然不舍得我花钱，哼！” 老公：“………”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('119', '‍‍‍‍小明：“做我女朋友吧！小红。” 小红：“凭什么，你那么丑。” 小明：“我很丑，可是我也很温柔啊！” 小红：“去shi吧你，丑就已经够造孽的了，还是个娘炮。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('120', '520，二货闺蜜和我聊心事，她问：以后想找个什么样的男朋友啊？我满含期待地说：只要长的符合我要求的就行。二货闺蜜一脸坏笑：那，那个“长”字读什么音？我：滚远远的……');
INSERT INTO `tpshop_joke` VALUES ('121', '一天小明哭着对爸爸说：“爸爸有人骂我是傻逼。” 爸爸大吼道：“那他的儿子是天下最大的傻逼。” 爸爸问：“是谁骂你啊。” 小明：“是爷爷！” 爸爸：“……”');
INSERT INTO `tpshop_joke` VALUES ('122', '昨天我去书店买书，问：老板有《时间简史》吗？老板不理我，然后我连续问了N遍，老板生气的对我说：我就是有时间也不去捡屎！');
INSERT INTO `tpshop_joke` VALUES ('123', '‍‍不要和在海边长大的孩子一起去海洋馆，当你在海底长廊指着玻璃里面说：“哇，你看这个，你看那个，哇好可爱！” 他们只会说：“这个太腥，得多腌一会，那个烤着最好吃。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('124', '早上先去门口，一小萝莉问妈妈：“这两狗狗在干嘛啊？” 我回头一看，原来两狗狗在交配，心里想，看看这位妈妈怎么说。 在闹着玩儿呢，这位妈妈说。 小萝莉说：“我也要闹着玩。” 噗，不小心，我笑出声了。 那妈妈看了我一眼，脸都绿了。');
INSERT INTO `tpshop_joke` VALUES ('125', '‍‍厕所有个水龙头漏电，本来贴了一张纸：“带电，禁止触摸！” 我拿验电笔一试，果然带电。 回到办公室打印了一张：“你能尿到这里么？我能！” 下午开始听见惨叫，第二天惨叫依旧。‍‍');
INSERT INTO `tpshop_joke` VALUES ('126', '一性感美女摆着屁股从夜店的卫生间走向吧台。 以一种撩人的姿态坐下，向酒保诱惑勾了勾手指。 酒保连忙过来，美女问：“你们经理在吗？” 酒保：“不在，他出去了。” 美女就把手放在酒保的嘴里，酒保性奋的挨个吸允了十个手指。 美女满意的看着酒保，对酒保说：“告诉你们经理，卫生间没纸了。”');
INSERT INTO `tpshop_joke` VALUES ('127', '有一天，一个哥们到我们这来，看见大门上贴着“小心玻璃”几个字，扭头就走了他。　　指着大门说，“你们这不欢迎我啊~”');
INSERT INTO `tpshop_joke` VALUES ('128', '有个人在医院输液，输着输着就开始狂笑。别人问他笑什么。　　他说：“我笑点滴……”');
INSERT INTO `tpshop_joke` VALUES ('129', '“哥们，又快考试了，我都快发愁死了。”　　“告诉你个秘诀，肯定不会发愁。”　　“什么秘诀？快说。”　　“你把名字一改就可以了。”　　“什么意思？”　　“我的中文名是过儿，英文名是pass，日本名是不挂科子，韩国名要过思密达，印度名是过儿阿三，俄罗斯名字是必过特罗夫斯基”');
INSERT INTO `tpshop_joke` VALUES ('130', '现在向我们走来的是监考人员方队，他们面目铮狞，带着考卷，左手屏蔽器，右手探测器，他们贼眉鼠眼地走向主席台，对我们说：“小样，你抄一个试试。”');
INSERT INTO `tpshop_joke` VALUES ('131', '小姨子来我家拜年，吃完饭，老婆在洗碗。我和小姨子在客厅闲聊，小姨子问我：“姐夫，我同事们都说我好凶，你看我凶不？” 我刚要开口… 、 -老婆在厨房 伸出头喊道：“你要是敢看一眼 老娘打断你的腿…”');
INSERT INTO `tpshop_joke` VALUES ('132', '老公：你摸我的内裤干什么？ 老婆：我想要！ 老公：好吧 连藏在内裤的两百块你都知道 老婆：……');
INSERT INTO `tpshop_joke` VALUES ('133', '一美女坐出租车，刚上车，嘣的一声放了一巨响之屁，司机吓一跳，美女甚是尴尬。 司机点上烟，抽一口，缓缓地说:屁是你所吃食物不屈的亡魂的呐喊。 美女笑了尴尬尽消:师傅，你好文艺范。 司机摇头说:可是这呐喊声太大了，我特么以为爆胎了！！');
INSERT INTO `tpshop_joke` VALUES ('134', '前几天某宝抢了一张十元代金卷，今天提示今天就到期了，我一看这得赶紧用啊，要不然就浪费了，你说我是买737呐还是买747呐，急等，眼看就到点了……');
INSERT INTO `tpshop_joke` VALUES ('135', '问：如果有人打你老师你怎么办? 答：喂那个人吃炫迈！ 问：Why? 答： 因为根本停不下来！');
INSERT INTO `tpshop_joke` VALUES ('136', '刚刚有对母子过来我店里买雪糕，付了钱后，那女的对他儿子说，来我帮你打开，她儿子说我自己开，那女的又说我来吧，她儿子还是说我自己能打开，结果她吼了一句：我想先吃一口行不行！！');
INSERT INTO `tpshop_joke` VALUES ('137', '[学渣，以你文科成绩来看，你适合学理。以你的理科成绩来看，你适合学文。以你的语文成绩来看，你适合出国。以你的英语成绩来看，你适合考古。以你的总成绩来看，你适合去死……求过！！！');
INSERT INTO `tpshop_joke` VALUES ('138', '‍‍最近颈椎不好，医生建议多休息，别干活。 今天看屋子太脏了，简单收拾一下，老公下班回来怒吼：不是说了别管吗？ 我以为下一句是：我来！ 没想到，人家说：脏就脏吧，你好了再收拾……‍‍');
INSERT INTO `tpshop_joke` VALUES ('139', '钱不是万能的,这句话是有钱人说给穷人听的,让穷人不要骚动,以防抢了他们的钱；钱不是万能的,是穷人说给自己听的,对自己无能的安慰，让自己心安理得！');
INSERT INTO `tpshop_joke` VALUES ('140', '有人评论：刘德华1991年唱的是<<我想有个家>>时光飞逝，到2015年唱的<<回家的路>>。说明买房至少得20年。');
INSERT INTO `tpshop_joke` VALUES ('141', '‍‍话说有一个年轻小伙问教父：“情人节没有情人怎么过？” 教父说：“该怎么过就怎么过呗！难道清明节你家里没有死人，你还要弄死几个再过啊！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('142', '　　LZ男，今天中午和同事一起吃饭，说一朋友养一条狗，起名叫兔子，晚上出去遛狗，喊兔子，兔子。旁边一大爷说，孩子好兴致啊，还养兔子当宠物啊。哥们笑而不答。一会牵狗回家，大爷一脸无奈。说到这一同事来一句，狗自己也会想我是狗非叫我兔子啊，然后我大声喊了句，兔子。突然满屋无声，几秒后大家笑的前仰后到的，先说话那同事一脸的无奈。');
INSERT INTO `tpshop_joke` VALUES ('143', '女孩跟男孩在校园里走着，一路上都没什么言语，突然女孩对男孩说：我们去开房吧，今晚我不想回宿舍了！男孩惊讶道：这怎么行！今晚我要复习功课，明天就要考试了。女孩很失望，于是男孩送女孩回到了宿舍，望着女孩远去的背影，紧拽了兜里仅剩的五块钱，默默地留下了眼泪… ………………割……………… 第二天，男孩东拼西凑借到了钱，找到了女孩激动的说：今晚不回宿舍了，我们去开房吧！女孩委婉的说道：那怎么可以！今晚我还要写论文呢，明天早上就要交了！男孩很失望，于是男孩送女孩回宿舍，女孩回到宿舍一头跑进厕所，脱下了裤子看着那鲜红的卫生巾长长的叹了口气…');
INSERT INTO `tpshop_joke` VALUES ('144', '17岁之前我都是个纯洁的孩子。 直到那年我无意打开哥哥的电脑，看到一个吃马赛克的姐姐， 从此踏上一条不归路！');
INSERT INTO `tpshop_joke` VALUES ('145', '‍‍老总：“印泥在什么地方？” 秘书：“印尼在东南亚！” 老总：“滚出去……”‍‍');
INSERT INTO `tpshop_joke` VALUES ('146', '‍‍妻子：“亲爱的，今天你生日，送你一个钱包。” 丈夫：“我就这20元，要钱包干啥。” 妻子：“喔，这里1000，你拿去。” 丈夫：“老婆你太好了。” 妻子：“今天生日，让你高兴高兴，明天记得一张不少的还给我。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('147', '老婆最近在减肥，半夜里饿了把我叫醒，我睡眼朦胧地说：“要不我下面给你吃吧” 老婆盯着我的裆部看了良久，说了句让我终生难忘的话：“我只吃粗粮…” 粗粮………');
INSERT INTO `tpshop_joke` VALUES ('148', '“大师，昨夜我做梦瘦了18斤。” “梦和现实是反的。” “难道我会胖18斤？” “不，你会胖81斤。”');
INSERT INTO `tpshop_joke` VALUES ('149', '‍‍甲：“什么是丑？” 乙：“能容忍街上有屎，都不能容忍有你。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('150', '医院的树阴下，一对情人在拥抱接吻。一个医生看见了，过去对那男的说：“你真糊涂，施行人工呼吸，应该把她平放在地上才行，走开让我来。”');
INSERT INTO `tpshop_joke` VALUES ('151', '以前农村有个家伙，喝农药自杀。他家老太太非常淡定地把他抓到院子里面。 拿出一桶粪，一勺一勺地喂他吃。边喂边说，自杀是吧，喝农药是吧，吃屎吧你。 那个人吐得那个惨烈啊。别说农药，胃都吐出来了。');
INSERT INTO `tpshop_joke` VALUES ('152', '你不知道的一些小常识！！！ 1、在非洲，毎60秒，就有一分钟过去。 2、凡是每天憋尿过多的人，有高几率100年内死去。 3、据研究，毎呼吸60秒，就减少1分钟寿命。 4、当你吃下了20碗米饭，换算下来竟然相当于摄入了20碗米饭的热量。 5、谁能想到，这名16岁少女，4年前只是一位12岁少女。 6、中国人在睡觉时，大多数美国人在工作。 7、当蝴蝶在南半球拍了两下翅膀，他就会稍微飞高一些。 8、据统计：未婚生育的人中有大多数为女性。 9、如果你每天省下来一包烟钱，10天后你就能买10包烟。 10、当你左脸被打，你的左脸就会痛。 11、人被杀，就会死。 12、中国教英语方式有极大错误，在美国就连小学生都能说一口流利的英语。');
INSERT INTO `tpshop_joke` VALUES ('153', '一位胖女人把电动车停在店门口，大声地责问老板：“你说这辆电动车充足电可以跑40公里，我怎么跑了20公里就没电了？” 老板看了看女人，微笑着说：“美女，你这体重，最少得算两个人吧！”');
INSERT INTO `tpshop_joke` VALUES ('154', '‍‍‍‍一名小警察喜欢局里的警花，在某一天，他终于鼓起勇气去表白。 结果看到一个高大的男人在和警花拥抱，很明显，警花有男朋友。 于是小警察回到自己的办公室。 整天闷闷不乐的，于是他的同事问他：“怎么了？” 小警察一脸忧郁滴说：“局花被别人爆了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('155', '‍‍新婚之夜，新郎新娘俩半夜在床上数礼金，竟然有一部分礼金全是一毛的。 新郎边数边说：“一百零一毛、一百零二毛、一百零三毛。” 隔壁房老子听到叫儿子：“你傻小子，毛是数不清的，还不干正事。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('156', '?【一只老鼠娶了蝙蝠做老婆，很高兴。别人笑它没眼光，老鼠说：＂你懂个屁，人家好歹也是个空姐！＂接着骄傲地指着猫说：＂我和蝙蝠将来的孩子生活在空中！再也不用怕你了！＂猫哈哈大笑，指了指树上的猫头鹰说：＂看见没，俺媳妇！');
INSERT INTO `tpshop_joke` VALUES ('157', '甲：“老兄，借我2万块应急吧！” 乙：“我手头也不宽裕。” 甲：“亲爹，帮帮忙！” 乙：“好吧，用完赶紧还我啊！” 甲在心里暗暗发誓：“想让我还钱，到时候你得叫我爷爷。”');
INSERT INTO `tpshop_joke` VALUES ('158', '天津滨海新区爆炸，这是有关报道：从中国地震局波形记录结果看，第一次爆炸发生在8月12日23时34分6秒，近震震级ML约2.3级，相当于3吨TNT，第二次爆炸在30秒种后，近震震级ML约2.9级，相当于21吨TNT。怎么算的？');
INSERT INTO `tpshop_joke` VALUES ('159', '‍‍‍‍朋友去买火车票，买了很久才回来。 我问他：“是不是很多人在排队。” 他说：“其实排队的人不多。” 我问他：“那你怎么去了那么久才回来？” 他说：“因为不排队的人太多了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('160', '‍‍‍‍本人一初中生，有次上体育课，老师测跳绳，一哥们跳着跳着校服裤子开始往下掉。 一直掉到脚边，这货还在不停的跳绳，我们都停下跳绳看他跳。 等到时间结束后，他一脸淡定的把裤子提上。 向我们抛了个媚眼说：“没见过掉裤子啊！” 然后扭着屁股走了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('161', '22-28岁这六年，我们会迅速走过，也是这六年，奠定了三十岁之后的生活质量和以后的人生方向。不管是男生女生，如果在结婚前奠定一个良好的事业和经济基础，无疑是对自己未来一个巨大的信心和保障。总好过把这个重任扔给一个其实实现不了你太多幻想的婚姻。你不成长，没人替你成长。献给朋友们！');
INSERT INTO `tpshop_joke` VALUES ('162', '今天女朋友给我剪指甲，我说：这么漂亮为啥要剪。她说：你用肯定不知道痛了。');
INSERT INTO `tpshop_joke` VALUES ('163', '‍‍‍‍两同事闲聊。 同事甲：“现在房价真高，要是我有块地就发达了。” 同事乙说：“你要真有块地，给你当儿子都巴不得” 正在这时，“前台美眉喊：同事甲，你有快递。” 从那以后，同事乙不怎么和我聊天了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('164', '昨天下午宿舍突然着火了，凭借我多年的消防知识，赶紧拨打了119，并积极的加入了救火行列，事件引起了媒体的关注，我一直说是我应该做的。领导不但没表扬我，还关了我禁闭！说我不应该报火警。严重影响单位形象。瞧我这小爆脾气，不就是消防员吗？！老子不干了！等等。。。。俺是啥职业？领导我错了。');
INSERT INTO `tpshop_joke` VALUES ('165', '去图书馆避暑，没带学生证进不去，只好在楼道里蹭空调。管理员阿姨洞晓了我的企图，要赶我走。我可怜巴巴地望着她：“我就在外面蹭蹭，保证不进去。”然后她恶狠狠地骂我：你们这些臭男人，每次都这样说。。。');
INSERT INTO `tpshop_joke` VALUES ('166', '微博提问:“周末我约了女盆友去宾馆。请问，第一次约女盆友开房，穿什么内裤好？” 热心网友:“我建议你穿开裆裤！超方便！”');
INSERT INTO `tpshop_joke` VALUES ('167', '‍‍刚刚有对母子过来我店里买雪糕。 付了钱后，那女的对他儿子说：“来我帮你打开。” 她儿子说：“我自己开。” 那女的又说：“我来吧！” 她儿子还是说：“我自己能打开。” 结果她吼了一句：“我想先吃一口行不行！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('168', '‍‍晓丽：“男朋友几个月不联系我了，是什么情况？” 甲：“可能变心了或是从来没爱过你。” 乙：“你回答的太阴暗了，我觉得可能是你男朋友死了。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('169', '上课中，一老兄在玩手机，不幸，被班主任在窗外寻查发现了，班主任不想打断课堂，给该同学发了个短信，意在提醒他。 不巧该生没存班主任的电话，遂回复短信：谁啊，上课呢。 班主任回：看窗外！ 老兄回：谢了，班主任盯着呢，下课再说。');
INSERT INTO `tpshop_joke` VALUES ('170', '‍‍一女同事说的，某天同事逗女儿：“囡囡，妈妈给你生个弟弟好不好？” 女儿说：“妈妈，我不喜欢弟弟，给我生个哥哥好不好？” 同事说：“妈妈不能生哥哥，只能生弟弟的。” 女儿说：“我不喜欢弟弟，我喜欢小狗，要不你给我生只小狗吧！” 同事说完大家爆笑。‍‍');
INSERT INTO `tpshop_joke` VALUES ('171', '「为什么从古到今婆媳总是不和？】原因有3：一是婆婆花4~5年时间教会儿子如何穿裤子，可媳妇却用不了5秒钟就能叫儿子把裤子脱了；二是婆婆用乳房哺育了儿子的生命，可媳妇用没奶的乳房欺骗了儿子一生；三是儿子累了一天，妈妈会看着儿子入睡，可媳妇晚上还罚儿子做俯卧撑！！！');
INSERT INTO `tpshop_joke` VALUES ('172', '那天上课俺画了一只猪贴在了前面同学的背上，他很快发现了，把猪撕下来怒视俺．俺很迷惑的问他：“你怎么知道你的后面有一只猪呢？”');
INSERT INTO `tpshop_joke` VALUES ('173', '我躺在检查台上，医生给我体检，她问我：“你的性欲怎么样？”“啊？我的什么？”我没听明白。“性欲，”她说，“就是，你想不想做爱？”“想啊，”我说，“不过咱们抓紧啊，我老婆还在车里等着。”');
INSERT INTO `tpshop_joke` VALUES ('174', '真不敢相信我的好运——我在公园里捡到一个手提箱，打开一看，里面竟然有夹层！满满地堆了十万英镑的崭新钞票。我一辈子都想要个有夹层的手提箱啊！');
INSERT INTO `tpshop_joke` VALUES ('175', '我陪一同志哥们儿Dave去同志酒吧玩儿，我觉得超没意思的，无聊得都打哈欠了。结果他超不爽的，因为我就这么拿到了六个电话号码……');
INSERT INTO `tpshop_joke` VALUES ('176', '说一个真事，lz一大学同学，长得特别像小姑娘，而且说话嗓音也特别像女生，加之个头不算太高，经常被别人当成女生。在他高中的时候，有一次他肚子疼，遂他老爸领他去老中医，结婚中医把了把脉问出了一句令我这苦逼同学一辈子都忘不了的话:“你例假正常么？”正常么…');
INSERT INTO `tpshop_joke` VALUES ('177', '年轻人压力大啊，公司的前台是个21多点的妹纸，刚被查出精神分裂症，父母都从老家来了，心都伤透了。我问一个同事，什么是精神分裂症，丫的回答我说：“精神分裂就是拿把刀砍了你两刀，下一秒又会惊恐万分的问你，艾玛呀，谁砍的你啊。”');
INSERT INTO `tpshop_joke` VALUES ('178', '有一天，老爸接到一个骗子的短信：”爸爸，我手机钱包都丢了，你把钱打到我同学卡上，卡号是……”老爸看完之后很淡定的回了一条：”正要找你，你爸在我手里，想要见他就乖乖按我说的做……”那边回来三个字“我发错了，叔叔”');
INSERT INTO `tpshop_joke` VALUES ('179', '塞班越来越老，一天中的大部分时间都在昏睡。一天，IOS和安卓为了“谁才是最棒的智能平台”吵得不可开交，塞班被吵醒，对紧抱他的诺基亚说：“放手吧，我已经落伍了。”“不要。”“你再跟我绑在一起，你也会被淘汰!”“那就淘汰吧。”诺基亚哑着嗓子说:“你是因我而生的，我绝不抛弃你。”');
INSERT INTO `tpshop_joke` VALUES ('180', '看到一句话：如果有一天，我们因为下载音乐而被投入监狱，我希望能按音乐风格将我们区分开……');
INSERT INTO `tpshop_joke` VALUES ('181', '　　楼主也说个和好基友的糗事！有次和他一起去美宜佳买烟、楼主顺手拿旁边的杜蕾s看了一下，朋友突然来句“要买吗？”楼主脑抽的回了一句：“和你用吖！？”我以为这就结束了，没想到这货伸出兰花指、特妖艳的姿势、特嗲声嗲气的语气说：“人家不喜欢用T嘛、、、”当时递烟给我那大姐的眼神、、、明显的充满恐惧与鄙视吖！');
INSERT INTO `tpshop_joke` VALUES ('182', '一算卦的老大爷对一男子说:“先生，算一卦吧！”答:“不信。”大爷掐了掐手指，又说:“你今年32岁，结婚两年，无子。”男子大笑:“那是你以为，我有一儿一女。”算命的瞪大双眼，又掐指一算，大笑曰:“那是你以为！');
INSERT INTO `tpshop_joke` VALUES ('183', '我愿下辈子化为一座石桥 五百年风吹 五百年日晒 五百年雨大 只愿心爱的女子从石桥走过 快数数你走过桥 那都是真爱 只为看你裙底的风光 冷哥这是我看了完整版的剑雨吐槽，大家看来一起开心开心');
INSERT INTO `tpshop_joke` VALUES ('184', '‍‍‍‍有一天小明拿着试卷回家，爸爸看了看试卷，特么的5分！ 啪啪啪，就是给小明五巴掌，问下次再考多少，小明哭着说，下次一分也不考了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('185', '‍‍‍‍躺在沙发上不经意的唱了一句：“轻轻的一个吻…” 脸上竟然挨了一巴掌！ 还被质问到：“青青是谁？” 我倒是不急着回答这个问题，心里却暗自庆幸，多亏没唱：“青青河边草啊！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('186', '一日，众女生讨论旅途安全问题。不知不觉间，将问话题焦点转移到“女孩出门，为了安全，应该带刀还是带套？”这一敏感问题上。正当大家讨论得春光满面的时候，一个冰冷的声音涌出：“我只带艾滋病报告单……”');
INSERT INTO `tpshop_joke` VALUES ('187', '男人=吃饭 睡觉 赚钱 　　猪=吃饭 睡觉 　　代入：男人=猪 赚钱 　　即：猪=男人-赚钱 　　结论：男人不赚钱等于猪');
INSERT INTO `tpshop_joke` VALUES ('188', '‍‍‍‍谁说胖子没有好处了？ 胖子的唯一好处就是，人多的时候可以坐副驾驶！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('189', '我对老妈说：“我准备节食减肥。” 老妈紧张地说：“不行，节食伤身体。” 我心里顿时暖暖的，说：“我会注意的。” 老妈依旧摇头：“不行，你现在胖点别人以为是你吃得多，万一你节食还胖，别人会说是遗传的！”');
INSERT INTO `tpshop_joke` VALUES ('190', '‍‍超市里，看到两个僧人购物车里装了满满一车,心想“和尚这么有钱啊！”收银员问他们：“现金还是刷卡?”一和尚说：“施主，贫僧是来化缘的！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('191', '小学数学考试，跟班上一个成绩很好的同学做一起。一直抄他的，他也没拒绝。最后两题他不许我看了。 后面成绩下来了，结果居然是：他89，我92。从那次以后我的成绩都2～30分了。 多么痛的领悟……');
INSERT INTO `tpshop_joke` VALUES ('192', '约了一妹子开房，一阵云雨后…… 妹子性满欲足的对我说：“你真是我遇到最棒的男人，你给了我头皮到脚趾甲前所未有的高 潮……” 我笑道：“别急，一会再给你次……” “快走吧，我老公带人两分钟后就到，你是我第一次放走的男人……” “。。。。。”');
INSERT INTO `tpshop_joke` VALUES ('193', '本人特喜欢吃臭豆腐，以此为背景。一次在路边看到卖臭豆腐的，于是乎便买了些。给了钱，就在这时候，城管来了，只听到老板说了声，小伙子请随我来。我想着，钱都给过了，不能不要吧。我就追了上去，跑了几里路，直到看不到城管了，他才停下。事后对我说，你是我见过最有毅力的吃货。。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('194', '这是个真实的小故事！lz有一个好友，五年前因砍伤人，被判了五年，今年释放出狱！打开自己五年前买的股票，瞬间变成百万富翁，买了车和房，谈了个女友！想想自己的五年都干了些嘛？求别喷，让我哭会儿！致我们失去的五年。');
INSERT INTO `tpshop_joke` VALUES ('195', '我隔壁合租的那个男的是变态，天天用手拍自己的肚子，害得我每天晚上都听着啪啪啪的声音硬的睡不着。要不是我在墙上挖了个洞，我还真发现不了这个秘密。');
INSERT INTO `tpshop_joke` VALUES ('196', '初到北京没有找到工作的我，身无分文，心情低落的走在大街上。正当觉得走投无路的时候，一位老者把装满零钱的大碗递到我的面前,。默默的从大碗里拿出几张钞票，感激的对他说了声，谢谢！好人呀。');
INSERT INTO `tpshop_joke` VALUES ('197', '今天去银行把卡里的最后60元取出来，见工作人员是一美女，便想多聊几句，就问：怎么样识别钱的真假啊？妹子撇了我一眼，把60元扔给我，说到：如果我给你的是一张60元面值的，那就是假币！ 我…………');
INSERT INTO `tpshop_joke` VALUES ('198', '浙江高考作文《文章与人品》0分卷已出炉： 文章，1984年生于陕西省西安市，06年毕业于中央戏剧学院表演系。中国内地男演员、导演。出演过电视剧与青春有关的日子，奋斗，蜗居，裸婚时代等，大受好评，其电影失恋33天，西游降魔篇等片更是大红大紫！但是，文章跟马伊琍好了之后劈腿姚笛。我要说：文章人品真TMD不咋地！！！');
INSERT INTO `tpshop_joke` VALUES ('199', '女儿要嫁人了。 父亲说：“宝贝啊，爸爸没什么钱给你当嫁妆，你就把家里的搓衣板带过去吧。” 女儿道：“爸，我才不会自己动手洗衣服。” 父亲 ：“不是洗衣服！” 女儿：“哦，我懂了，可是我有键盘啊。” 父亲哭泣道：“宝贝啊，就当我求求你，带它走吧，让我过点安生日子！” 女儿：“好吧，爸，我懂了。”');
INSERT INTO `tpshop_joke` VALUES ('200', '“ 老师问：“有钱，任性”的下联是？” 小明答：“没钱，认命”。 老师哑然！ “用一句话形容现代男人的婚后生活！” 小明：“娶了个祖宗生了个爹！” 老师再问：“古代女人为什么要裹脚？” 小明大声道：“怕她们逛街”。 老师接着问：“那么为什么现在不裹了” 小明继续回答：“现在有了支付宝，裹脚也没用。” 老师：“来来来…小逼崽子你讲课吧！');
INSERT INTO `tpshop_joke` VALUES ('201', '接一个电话说是我领导，要我打5万块钱救急。我说，马上打钱！睡一觉后，又来电话了，问怎么还没打钱。 我说急着出来，忘带钱了，只带了两张卡，有钱的那张消磁了，另一张卡没钱。重新办卡要五百块钱，你给我先打五百块重办卡行不？ 他听了沉默很久，最后说，咱同行何必为难同行啊。');
INSERT INTO `tpshop_joke` VALUES ('202', '小时候和我妈去裁缝店，我妈指着电熨斗说，这东西很烫，千万不要用手碰！ 我很听话，没用手碰，我舔了一下。 那感觉比冬天舔黑龙江漠河北极村的铁栏杆还带劲！');
INSERT INTO `tpshop_joke` VALUES ('203', '今天我女朋友问了我一个千古谜团:我和你妈同时掉进水里你救谁？ 当时灵机一动，反问到：我和你爸同时喝高了你扶谁？');
INSERT INTO `tpshop_joke` VALUES ('204', '一美女下夜班,被一色男子尾随跟踪,美女很害怕,正路过一片坟地, 色男子正要下手, 美女走到一座坟墓前说:“爸爸,开门吧,我回来了”。吓的色男子狂奔而去。美女为自己的聪明得意地笑了起来，哪知笑声未落，从坟墓里传出一个阴森森的声音说：“闺女，你咋又忘记带钥匙了呢？”吓得美女尖叫著跑了。这时，一个盗墓者从坟墓里爬了出来，说：“影响我工作，吓死你”。突然发现墓碑前有一老者，手拿凿子在刻墓碑，就奇地问：“你在干吗”？老者生气地说：“这些不肖子孙把我的墓碑都刻错了，只自己来改啦”。盗墓者一听，吓得撒腿就跑了。 看著盗墓者的背影，老者冷笑道：“跟老子抢生意，吓死你”。一不小心，凿子掉地上了，老者正要弯腰去拾，却看见从草丛中伸出一只手，同时还有个冷冰冰声音：“ 啊，敢乱改我家的门牌号”。吓得老者连滚带爬地跑了。 一个拾荒者从草丛中爬出来，捡起地上的凿子，感叹道：“这年头，捡块烂铁还得费这么大.');
INSERT INTO `tpshop_joke` VALUES ('205', '今天朋友发消息问我，有人不断给她发信息，而她不想回，问我怎么办，我暴脾气来了你傻啊，直接拉黑不就完了吗？ 于是我就被拉黑了……');
INSERT INTO `tpshop_joke` VALUES ('206', '小明滚出去后，被罚跑操场10圈，老师见小明跑得慢，就骑电动车到屁股后面赶，到拐弯处小明一个劲的加速，老师也跟着加速，小明一个急停了下来，老师被吓一跳猛拐弯开翻了。 结果老师进了医院，小明开除学籍处理！');
INSERT INTO `tpshop_joke` VALUES ('207', '我们系的晚自习一向很松的，有次一个轮滑社的同学带来了她的轮滑鞋。 我就穿着在班里试，当时根本不会，还拿了两个拖把左右支撑着。 可惜根本动不了，一个同学就过来扶我前进。 突然有人报告：“辅导员来了！” 我那同学撒手就跑回了座位，无助的我只能生生在原地看着辅导员开了门。 辅导员看见我扶着两个拖把杵那儿不能动都震惊了。');
INSERT INTO `tpshop_joke` VALUES ('208', '陪闺蜜去做妇科检查（脱了裤子的那种，你们懂的）。闺蜜出来以后表情各种纠结，我很担心是不是出了什么事，就问她怎么了。她说:里面有个助理医师是个男的。我说：男的怕什么啊？他见过的比你见过的还多呢。她快哭了，然后说：他是我高中时候的学长，追了我两年。我一直很排斥他，手都没让他碰过，balabalabala（各种贞烈），我TM今天把他最想看的地方piapia主动送给他看了。');
INSERT INTO `tpshop_joke` VALUES ('209', '小时候和我妈去裁缝店，我妈指着电熨斗说：“这东西很烫，千万不要用手碰！” 我很听话，没用手碰，我舔了一下。 那感觉比冬天舔黑龙江漠河北极村的铁栏杆还带劲！');
INSERT INTO `tpshop_joke` VALUES ('210', '我一朋友姓向，昨天和我逛街。 他正和我说笑的时候，踩了一块香蕉皮摔个狗吃屎。 他站起来从嘴里吐出两颗带血的牙齿。 我大惊道：“大象，你的象牙掉了！” 他站起来不屑的看了我一眼：“二货，我嘴里还能出象牙？” 我：“……”');
INSERT INTO `tpshop_joke` VALUES ('211', '‍‍今天，我问老婆：“为什么你回到家，就只睡觉什么事都不干！” 老婆淡定的说到：“你的妈妈把家务的技能交给了你。而我妈却传给我弟弟，正所谓传男不传女。”');
INSERT INTO `tpshop_joke` VALUES ('212', '早上看报、 福州西湖公园门口，一个五六岁模样的小男孩哭泣不止。原来，小男孩和爸爸在西湖公园荷花池边玩捉迷藏，一人藏一人找，轮到爸爸藏时，小男孩却怎么也找不到了。后来民警陪着男孩找了40分钟才找到男孩父亲。后来才知道爸爸躲在湖里，伸出一根芦苇呼吸~');
INSERT INTO `tpshop_joke` VALUES ('213', '我家住在山东和江苏交界的地方，晚上睡觉的时候我只能朝着一个方向睡觉，每当老子翻身的时候手机就会收到一条短信，江苏移动欢迎你，当老子翻身的时候又收到短信，您已进入山东省！这是什么情况，哥打个电话还要靠墙根，哎呀不说了，没法过了，这日子！');
INSERT INTO `tpshop_joke` VALUES ('214', '突然发现，原来我们在念“喉”“牙”“舌”“齿”“唇”这五个字的时候，发声重点就恰好在这五个发声部位上，汉语真是博大精深啊！');
INSERT INTO `tpshop_joke` VALUES ('215', '我：“医生，我老婆ML的时候老是胡言乱语，这正常麽？”医生：“你好，这属于正常现象！女人GC时，属于昏迷状态！胡言乱语是在表达兴奋程度，也有助于你们的情趣！”我：“哦！我还以为我老婆一直喊着隔壁老王的名字是有问题呢！谢谢你医生。”');
INSERT INTO `tpshop_joke` VALUES ('216', '关于考试，我设想过的最有创意的对策是携带一瓶白磷悬浊液，涂抹在试卷局部，交卷时夹在其他人的试卷中间，应该不会引起老师的注意，最多也只会觉得有个学生不小心将试卷弄湿了。收上去的试卷通常不是立即批阅而是先存放起来，随着水分蒸发，白磷自燃，将所有考卷付之一炬');
INSERT INTO `tpshop_joke` VALUES ('217', '在健身房看到一个胖妹在跑步机上慢悠悠的走，我过去提醒她：“妹子你得调快一点，不然起不到减肥的效果。”妹子听了委屈的回答：“我调得很快的，但是我一踩上去它就慢下来了！”');
INSERT INTO `tpshop_joke` VALUES ('218', '警察在路上拦了一位超速的司机，请他出示驾照，那位司机非常愤怒的大声叫道：我希望你们警察在做事之前，最好能先商量一下，不要总是颠三倒四好不好？你们昨天刚刚没收了我的驾照，今天又要我出示');
INSERT INTO `tpshop_joke` VALUES ('219', '昨晚第一次去男朋友家见家长，他妈妈做的菜很好吃。我吃完一碗饭刚想起身再装一碗的时候，他妈妈把我按住了说：没事，碗放着等下我一起洗……');
INSERT INTO `tpshop_joke` VALUES ('220', '刘备先跟从公孙瓒，后跟陶谦、吕布、曹操、袁绍、刘表，其为人喜怒不形于色，令人做摸不透，后人以歌赞其曰：“刘老跟，刘老跟，你是一个啥样儿人啊？”');
INSERT INTO `tpshop_joke` VALUES ('221', '语文老师讲课讲到一篇文章时，文章里面出现了深薯，有同学问老师：“深薯那么好吃，是长在木上的还是生在地里的啊？”　　老师说：“你生长在深薯之乡都不知道吗？好吧，当然它是长在树上的吧……\"　　其实深薯是长在地下的，和番薯一样,学生被老师误导了。');
INSERT INTO `tpshop_joke` VALUES ('222', '紫薇含情脉脉地看着尔康“尔康，你现在幸福吗？”　　尔康看着紫薇的眼睛说：“傻丫头，我一直就姓福啊！难道你忘了吗？” 　　紫薇：“讨厌，人家说你现在感觉幸福吗？”');
INSERT INTO `tpshop_joke` VALUES ('223', '小明在一个刚认识不久的美女面前吹牛。　　小明：“前天，10多个人和我打架，打了三个小时都没有把我打倒！”　　美女敬佩的问道：“啊？不可能吧，看不出你这么牛？你是如何做到的！”　　小明（哭了）：“他们把我绑在柱子上打的！我想倒也倒不下！”');
INSERT INTO `tpshop_joke` VALUES ('224', '一位将军级病号住进了某家军区医院，他生性怪僻，就是不让女护士给他打屁股针，并声称叫院长给他请个男医生来打针。　　院长不得已只好安排他的得力助手小扬去为将军打针。　　小扬来到将军病号房门前，推了推鼻梁上的深度近视眼镜，一边敲门一边说道：“报告首长，我是专程为您打针来的。男的！”　　声落门开，一张五十开外的军人脸正冲着他笑道：“小同志你真逗，我知道你是个男的。”　　小扬笑笑说道：“让首长见笑了，可您为什么不让女护士为您打针呢？”　　将军：“小伙子，实不相瞒，我曾当过我老婆面发过誓，此屁股只有她一个女人可摸哦！”　　小扬：“哈哈，首长您老真逗，下面我们可以打针了吗！”');
INSERT INTO `tpshop_joke` VALUES ('225', '临下课,老师轻声说：“现在我们来点名,请同学们小声答到,别吵醒了睡觉的同学。”');
INSERT INTO `tpshop_joke` VALUES ('226', '话说一群追星族在学校食堂饭桌上谈论有关天王刘德华八卦新闻。他们正谈到网友怀疑刘天王夫人朱丽倩已怀孕并为起了各种“雷”名，有的说：“网友们都怀疑是男孩儿并说应该叫‘刘得住’。”　　不知什么时候旁边坐上了班主任，“扑哧”捂了一下闷笑起来。同学们很好奇。　　班主任揶揄道：“刘德华生男孩就叫刘得住。那他是生了女孩是不是叫刘不婷，那郭富城如果以后有了孩子是叫郭盖还是郭富国？黎明的孩子叫黎太阳，黎上草？”');
INSERT INTO `tpshop_joke` VALUES ('227', '小偷把发夹往锁孔里一捅，顺时针旋转180度，随着轻微的吧嗒声，门锁开了。　　这已经是小偷本月的第五次光顾别人的房间了，感觉一次比一次容易，小偷不由慨叹：那造锁的王八蛋害人不浅啊，长期以往，偷窃水平怎么可能会有提高?小偷眉头紧皱，忧心忡忡');
INSERT INTO `tpshop_joke` VALUES ('228', '朋友买了条老粗的假金项链，天天戴着上班。　　有一次夜班回来，路过一偏僻的地方，忽然看到两小伙骑着摩托奔他而来！　　看出他们想抢项链，朋友连忙摘了扔进旁边草丛。　　俩劫匪毫不犹豫，跳下摩托就冲去找，然后朋友就镇定的跨上摩托骑走了。　　劫匪傻了，一个问：“哥，咋整？”　　另一个狠了狠心说：“TMD，报警！”');
INSERT INTO `tpshop_joke` VALUES ('229', '皮哈开垦了一小块土地，并且种上豌豆。当他把开发完成后，他的邻居忽然来访。　　“你种什么了？”他问道，眼睛看着皮哈刚刚开掘的一个个深坑。　　“豌豆。”皮哈大声答道。　　“你忘了做一块墓碑。”　　“做墓碑？”皮哈不懂为什么要做墓碑。　　“嗳，”他摇着头说，“你把这些豆子埋到那么深的地下，它们就应当得到一块适当的碑记。”');
INSERT INTO `tpshop_joke` VALUES ('230', '情人节老婆给买了一件颜色鲜艳的冲锋衣，第二天就屁颠屁颠的穿上了。　　结果上班的时候路过门卫处，保安喊道：“那个送快递的，过来登记。”');
INSERT INTO `tpshop_joke` VALUES ('231', '一天和老婆出去逛街，正讨论今天中午吃什么，突然旁边冲出来一个男的，一边拉扯着我老婆的衣服一边说:“叫你不要买这么贵的衣服，你偏要买！”，然后拿着衣服就跑了，我对老婆说:“你特么的小婊砸敢背着我偷人！”，我老婆很委屈的说我根本就不认识他，我愣了三秒突然好像明白了什么……');
INSERT INTO `tpshop_joke` VALUES ('232', '节假日哪里排队的人最多？ 一位大婶回答:当然是女厕所了');
INSERT INTO `tpshop_joke` VALUES ('233', '《小星星》，《字母歌》两首歌的调是一样的，噢，不小心泄露了秘密！');
INSERT INTO `tpshop_joke` VALUES ('234', '　　一同学过生日，我去留言。我：生日快乐！她：谢谢，兄弟干了这碗热翔！我：干！她：干，贱货，给我留点！我：吃点饭不就行了？她：对！对对！我恶心了好长时间！');
INSERT INTO `tpshop_joke` VALUES ('235', '总会有一天，你的床头有我随意翻看的书，洗漱室的剃须刀旁是我的粉底液，更衣室的白衬衫里夹杂着我的白裙，你朋友无一不知我的样子……然后你在前方，我大步靠近并勇敢的握住你的手，听你低头说：我们回家！嗯！我们回家！……会有这么一天的，对吧？？？');
INSERT INTO `tpshop_joke` VALUES ('236', '今天在食堂吃饭，我姐坐我对面喝牛奶。脑子一抽，突然来了一句“干了这瓶大姨妈”…差点喷了。。');
INSERT INTO `tpshop_joke` VALUES ('237', '小时候妈妈常叫我送饭去厂里工作的姐姐那里，一天将近到厂门口，一个叔叔对我说:\"你的饭盒漏了\"，我马上倒过来看，天啊！饭菜全部散落在地上，我立即捡回表面没弄脏的饭菜，到了厂里，姐姐问:\"为何这次饭菜少了一半?\"我回答:“妈妈要你减减肥，不能老吃这么多，还要嫁人呢。\"');
INSERT INTO `tpshop_joke` VALUES ('238', '昨天晚上做恶梦了，醒来后跟男票装可怜，博同情，结果这臭男人来句：“怎么？让人从猪圈挑出来了？吓坏了吧？”');
INSERT INTO `tpshop_joke` VALUES ('239', '妈妈：不要老吃东西，赶紧写作业去。 小明：我就是在做数学题啊。 妈妈：冰箱里面有数学题？ 小明：咱家的冰箱结霜，影响了制冷和保鲜效果，伤害了冰箱的自尊心，给冰箱留下了严重的心理阴影，我打算求一下阴影面积！ 妈妈：滚出去！');
INSERT INTO `tpshop_joke` VALUES ('240', '经常逛贴吧，忽见一人为自己刚买的狼狗征名字，此楼主说获赞最多的名字就是他爱犬的名字。 第二天再去看，获赞最多的名只有一个字：“爹”。');
INSERT INTO `tpshop_joke` VALUES ('241', '一朋友把另一朋友老婆勾搭了，终于露馅被追打，朋友边躲边大声喊叫：“这几年要不是我守着你老婆，早他妈的被多少男人睡过了都不知道！你是希望她跟我一个人睡？还是希望她和天下所有男人睡？” 打人的朋友愣了一下大骂：“你TM还有道理了是吧？当然是希望她跟你一个人睡了！”');
INSERT INTO `tpshop_joke` VALUES ('242', '‍‍‍‍今天天气好。 某男问某女：“你会爱上我吗。” 女方答：“不会啊！” 男：“那我教你好啦。” 女：“好呀！” 于是他们就相爱了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('243', '某士兵回家探亲，由于多年没有回家，见了老婆就想啪啪啪，无奈看看身边七岁的儿子，这时士兵拿出一个盘子给了他儿子说道:去打一盘子酱油，要打满，撒了回家看我不把你的屁股打烂，儿子走后两个人会心一笑');
INSERT INTO `tpshop_joke` VALUES ('244', '某天。 班主任（生气）：为什么我听到有人在说话？！ 小明（思考）：因为…因为你…有耳朵？');
INSERT INTO `tpshop_joke` VALUES ('245', '‍‍‍‍“老公，如果我们2个在沙漠里，我们身上都没有水，忽然看见有一瓶水，你是自己喝还是给我喝？” “我喝。” “你不爱我！” “不是的，我怕水有毒，我先喝了，用我自己身体过滤以后再给你喝，这样安全。” “喝尿都能让你说的这样文艺。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('246', '老婆：“如果有一个狠漂亮的女的勾引你。会不会上勾啊？” 老公：“不用那么漂亮”');
INSERT INTO `tpshop_joke` VALUES ('247', '‍‍‍‍老张是纪委工作人员。 这天他正在忙，书记来了：“老张，你的手机呢？” 老张：“奥，早上孙子拿着玩的，忘记带了，应该在家里。” 书记：“老张啊！你孙子拿着你手机，拨了10多个局长的电话，拨通了又没人说话，搞的现在有几个心脏病突发都进医院了。老张，你是纪委的，你要注意你的身份啊！” 老张：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('248', '1.陪老板打麻将，老板无意中说：我最欣赏蒋介石，宋美龄喜欢梧桐，蒋介石就在南京种满了梧桐。我心领神会，打出一张五筒，老板：胡。 \r\n2.在家看书，老妈问我：\"明天考试准备的如何？\"我说：\"一切都准备好了。\"老妈很开心：\"这次这么自信？\"我点了点头：\"当然，你看我准备的，红药水，绷带，急救包，担架，以及急救车电话和保险。您可以随便打了。\" \r\n3.去外面吃饭，我对服务员说：“一碗牛肉面，汤要淡。”几秒钟后我突然意识到可能造成误会，又补充了一句：“我说的淡是咸淡的淡，不是鸡蛋的蛋。你可不要在我面里加个鸡蛋啊。”服务员笑着说：“明白，是咸淡的淡。”几分钟后面上来了，里面果然有个咸鸭蛋。又要多花钱了……\r\n4.结婚以来，我一直都没怎么看过老婆的手机，今天偶然玩老婆的手机，看到通话记录里一个被老婆存为叫王八蛋的名字和老婆的手机通话频繁，我瞬间来火，就拨了过去，几秒钟后，我的电话响了…\r\n5.一个老人在公园打太极，很有力道，这时候来了个年轻人说：“老大爷功夫这么好，怎么练的啊。” 老人说：“我站到不动，恨用最大的力气打我试试。” 于是年轻人用力的打了老人一拳。结果......被讹了两万六。\r\n6.小侄子上二年级，今天给我背刚学会的乘法口诀：“一八得八，二八自行车，三八妇女节，四八AKB，五八同城…”\r\n7.未婚女孩发现自己怀孕了，第一反应是“完了，我妈非弄死我不可”，殊不知，肚子里的孩子也在想“完了，我妈非弄死我不可”。\r\n8.早上出门上班刚走到路口，迎面过来一个老头，错身过去就听老头在后面喊“哎，你碰倒我就想跑啊？！” 回头一看老家伙在地下躺着呢，碰瓷的!我说：“咱不带讹人的，这里有摄像头。” 老头说“早看了，没有。” 没监控你也敢这么猖狂，上去狠狠踹了两脚，转身就跑……\r\n9.一个富二代在北京遇到了一个农民工，发现与自己长得十分酷似。就一脸轻视的问；“你妈是不是年轻的时候在xx工厂上过班啊”农民工害怕的说：“没有额，我妈身子不太好所以没离开过我，不过我爹的身体就不错，挺好。年轻时在XX工厂给一老总当过司机……”\r\n今天的谜题是：猴子每分钟能掰一个玉米，在果园里一只猴子5分钟能掰出几个玉米？想知道答案的捧友，请关注微信公众号“捧腹笑话”，回复“谜语0519”即可知晓。');
INSERT INTO `tpshop_joke` VALUES ('249', '‍‍‍‍A：“最讨厌开会了，听着领导讲话就难受。” B：“是啊！净说些空话套话假话。” A：“我觉得领导说话就跟放P一样。” B：“这话怎么说？” A：“放不出来自己难受，放出来了吧，别人难受。” B：“有道理。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('250', '‍‍‍‍某君开了家棺材店。 一个人到他店里看货，说：“老板，这口棺材和那口棺材咋差一半呢？可是细看之下，没有一点不同啊！” 某君听了，打开棺材盖对那人说：“你躺进来试试。贵一倍的棺材，睡起来舒服多啦。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('251', '同事借我钱一直不还，我去找他要。没想到我还没开口，他倒先哭上了：“呜呜呜，我奶奶刚去世了，我好难过…”搞得我一直没机会开口，没办法我只好安慰他说：“哎，人死不能复生，还请你节哀顺便把钱还我吧…”');
INSERT INTO `tpshop_joke` VALUES ('252', '老板：为什么迟到？ 职员：捡到一百元还给失主了！老板：还钱要半小时？职员：他们打了我快半小时，我才还给他们！');
INSERT INTO `tpshop_joke` VALUES ('253', '面试的时侯，老板和老板娘告诉我他们信佛，我看他们的打扮也不像信佛的，还以为人不可貌相。上班了，到了食堂，一打饭我信了，真带我们信佛了。');
INSERT INTO `tpshop_joke` VALUES ('254', '单位缺女职员，今年一次性从学校里招了七个女实习生，\r\n有一天几个哥们在一起喝酒，他们说：你这回发财了，七个仙女呀！好好选一个，可以甩掉光棍的帽子了。\r\n我一口喝下一杯酒说：那哪是七仙女，简直就是江南七怪。');
INSERT INTO `tpshop_joke` VALUES ('255', '一个亿万富翁的小儿子把一枚分币吞到肚子里去了，急诊室正在忙乎。富翁：“医生您快！我已经有十个亿，只要您救好我儿子,分给您五个亿。”医生：“哎呀！人生在世，钱不在多少，就看放在什么地方。贪眼看亿万，太少；小肚藏一分，过多！”');
INSERT INTO `tpshop_joke` VALUES ('256', '兽医吩咐助手喂马吃药：先把一根管子插在马嘴中，一半在外面，把药放入管子中，一吹气，药就会进入马的食道。\r\n助手回来后，兽医问：马吃了吗\r\n助手说：没有，马吹得比我快。。。。。');
INSERT INTO `tpshop_joke` VALUES ('257', '在老家，觉得好冷，于是叫妹妹弄点火来，好，妹妹很听话就拿个火铲弄些火来了，杀马特，我眼睁睁的看她持着带火的火铲滑过被子，泥马的，被子着火了，我一杯水过去灭了，最后她把燃着的空铲子给我烤，顿时我心想这么做到的，她信誓旦旦地回答，拿汽油一浇就OK了，然后她又默默地又浇了些汽油，结果打199........');
INSERT INTO `tpshop_joke` VALUES ('258', '室友忽然跟我说，人工呼吸这个词不好听，一点意境都没有，毕竟是KISS了。我就问，那你给想个词呗，室友鄙夷一笑，答道：早就想好了，就叫爱的供氧。');
INSERT INTO `tpshop_joke` VALUES ('259', '宿舍一兄弟喜欢一女汉子，两人的关系也不错，经常一起打球，有一天，他鼓起勇气告白，谁知这女汉子说：“我只把你当兄弟啊！”他万念俱灰的低下头，片刻，他突然抬起头来，收起了之前的失落，试探的问：“兄弟，能搞基吗？”');
INSERT INTO `tpshop_joke` VALUES ('260', '谈恋爱还没几个月就想过一辈子，交个朋友稍微对你好点就想来往一生，难怪你的怨气那么重、悲伤那么多，这都是天真的代价。');
INSERT INTO `tpshop_joke` VALUES ('261', '小区停电，好多业主都聚在物管找说法。物管：只能等等，具体时间我们也不清楚有人就开骂了，物管主任指着我说：你看那姑娘多有素质，多冷静。。我：我从30楼跑下来，我休息会儿再骂。。。');
INSERT INTO `tpshop_joke` VALUES ('262', '‍‍小学时觉得，要是有一台小霸王人生就完美了。 中学时觉得，要是有一台电脑人生就完美了。 大学时觉得，要是有一个女朋友人生就完美了。 现在想想觉得，无论什么时候只要有钱人生就完美了！‍‍');
INSERT INTO `tpshop_joke` VALUES ('263', '万寿寺的和尚彬师有一次同客人谈话，一只猫蹲在旁边，彬师对客人说：“人们都说鸡有五德，我看这只猫也有五德：见了老鼠不捕，这是仁；老鼠抢夺他的食物就让，这是义； 客人来了，一摆上酒席，它就出来了，这是礼；藏的好吃的东西再严密，它也能偷吃，这是智；每年冬天，它都躲在灶边取暖，这是信。” .');
INSERT INTO `tpshop_joke` VALUES ('264', '本人异地恋。今天跟女票问我：我的裤子你给我买了吗？我：你不是说等天气暖了，瘦下来看身材再买的吗？这货给我来了一句：你也可以先买给我，我再根据裤子型号，决定瘦多少。我……');
INSERT INTO `tpshop_joke` VALUES ('265', '‍‍情人节，我爸带我妈出去吃西餐，我也换好衣服了。 “你去干嘛！今晚我和你妈过情人节。” “爸，我是你前世的情人。” “不要提前世，这世有你妈就够了！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('266', '‍‍“这里还有条厚被子，帮你盖。上” “妈，都已经盖了5床被子了，别盖了，我热！” “不行，得听医生的话！” “医生咋说的？” “他说你缺盖！” “妈，我们明天再去一趟医院！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('267', '百年修得同船渡，千年修得共枕眠。这个是不是可以这样理解：我一个妹纸一起做10次船，就可以做一次共枕眠的事了吗？嘿嘿');
INSERT INTO `tpshop_joke` VALUES ('268', '春节，亲戚问:”对象谈好了吗？””已经订婚了，准备五一结婚” 亲戚:”工作怎么样？”“还算稳定，一年能剩七八十万吧” 亲戚:”房子买哪了？”“市区有一套，准备搁乡下再买一套” 亲戚露出了欣慰笑容:“这孩子的病情恢复的不错，去年还咬人呢”');
INSERT INTO `tpshop_joke` VALUES ('269', '“老板，买盒套套” “对不起，我们没有套套” “我就要套套” “对不起，我们真没有套套” 我 生气的走出店门，妈了个蛋。日用品店都没有套套，唉');
INSERT INTO `tpshop_joke` VALUES ('270', '‍‍‍‍一同学在机场过安检的时候，大摇大摆地拎着饮料就过去了。 机场工作人员怒了：“回来，把你手里的饮料喝一口！” 该同学愤愤地拧开瓶盖，咕咚咕咚把一瓶饮料全喝了，工作人员顿时愣住了。 只见该同学擦擦嘴喊道：“你们不就是想要个瓶吗？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('271', '做了新发型，罗丝烫，比较蓬的那种，于是搭配了一双流苏鞋和一个流苏包。审视了一番后认为够范儿，信心百倍地站在老公面前摆了个POSE。老公瞅了一眼淡淡地说：“你的造型完全是按照隔壁那只藏獒设计的。”');
INSERT INTO `tpshop_joke` VALUES ('272', '丈夫跟妻子说：“油，加不起；路，走不起；学，上不起；病，看不起；房，买不起；菜，吃不起。”妻子说：“你知道根本原因吗？”丈夫笑着说：“你知道我活得累的原因了吧？”妻子说：“原因是，你这个阿斗扶不起。”');
INSERT INTO `tpshop_joke` VALUES ('273', '一间房子出租给多名男女，浴室只好共用，所以洗个澡都要排好久。某日夜里小吴从外头回来，想去洗澡，但刚好浴室里有名女子在洗。于是小吴问：“小姐，你下面有人洗吗？”那小姐听完却很生气地回答：“下面我会自己洗啦！无聊……”');
INSERT INTO `tpshop_joke` VALUES ('274', '阿毛在路上遇到一个妓女。妓女：帅哥儿，和我玩玩儿？阿毛：多少钱？妓女：200块。阿毛：太贵啦！20块怎么样？妓女：你还是找别人去吧！这天，阿毛与妻子上街，路上又遇上了那个妓女，阿毛装作没看见，继续与妻子有说有笑地从妓女旁边走过。后面传来了妓女的声音：“哼！20块的就是不怎么样！”');
INSERT INTO `tpshop_joke` VALUES ('275', '老婆：老公，人家说男人手机里没有微信、陌陌的男人是好男人，你连ＱＱ都没有那不就是绝世好男人了呀，人家好爱你的哦～～老公：什么时候给我换个彩屏手机。。。');
INSERT INTO `tpshop_joke` VALUES ('276', '妻子：“对面的那家女的真是太过分了，洗澡居然不拉窗帘。”丈夫：“哪儿，在哪儿？”妻子：“你看，就在那儿。”三秒后，丈夫看到对面的一个女人正在给她的宠物洗澡。');
INSERT INTO `tpshop_joke` VALUES ('277', '媳妇拿着我的信用卡对我说：“老娘今天要在这个商场刷五万块！你敢说一个不字我就刷十万！”“不敢不敢。”二十万没了。');
INSERT INTO `tpshop_joke` VALUES ('278', '老公一身臭汗回家：“老婆，给你一个热情的拥抱吧！”老婆：“别，你现在给我的拥抱可能只有热，没有情。”');
INSERT INTO `tpshop_joke` VALUES ('279', '两只母鸡在聊天，看到一只公鸡无精打彩的走来，母鸡问：“咋地了？没精神？”公鸡说：“做点生意！”母鸡问：“做啥生意累这德性啊？”公鸡不好意思的说：“嗯……卖点鸡精……”');
INSERT INTO `tpshop_joke` VALUES ('280', '我与未婚妻即将迈入婚姻殿堂，我向她誓言：“我要做你的好丈夫，你想花钱的话，尽管向我开口，我是你的取款机；你要我听你的话，我决不忘记，我是你的录音机；你要我让你快乐，我会乐此不彼，我就是你的游戏机；你要飞黄腾达，我就做你的直升飞机。”未婚妻说：“早晨，我爱赖床，起不来呀，怎么办？”我马上说：“我就是你的起重机。”');
INSERT INTO `tpshop_joke` VALUES ('281', '‍‍‍‍早上去药店买感冒药，一2B朋友陪着。 看到货架上摆着避孕套，她看成了口香糖问店员：“这个怎么买。” 店员看看她：“给男朋友买？” 二货朋友：“我自己吃。” 关键买完还没发现是避孕套。 忘不了离店时店员看我们的眼神。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('282', '夜里做梦淋雨没地方躲，一着急醒了，原来是上铺在向下滴水，我TM当时就生气了，什么狗屁宿舍，下雨天还漏水，不是上铺帮我挡着老子不得淋死。-');
INSERT INTO `tpshop_joke` VALUES ('283', '‍‍‍‍刚才中央大街逛街。 刚从一家店里往外走，快要到门口的时候一个美女突然摔倒在我面前，说了句：“刚进门就给你行大礼。” 然后我说：“你真客气。” 瞪我一眼，走了！ 屌丝就是屌丝………‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('284', '到一同事家，无意间发现他电脑上挂着两个QQ，就跟他开玩笑说：没想到呀居然还玩小号！然后他打开那个QQ，说：实话跟你说吧，这是为我儿子挂的，你看啊，我上面加的全是跟她同龄的全国各地的女孩，我现在没事就跟她们聊聊，等他长大了，就可以直接接手了。我顿时给跪了，中国好父亲呀！');
INSERT INTO `tpshop_joke` VALUES ('285', '坐车走高速公路，快到服务区时，司机吼了一嗓子：“要上卫生间的麻利点啊，提前做好准备！”旁边一哥们弱弱的问句：“我们怎么提前准备？是现在把裤子脱了吗？”');
INSERT INTO `tpshop_joke` VALUES ('286', '‍‍闺蜜：“好开森噢……” 我：“捡到钱了？” 闺蜜：“最近好多以前很久都不联系的同学、同事都加我Q跟微信了，是不是证明我还是挺有人缘的？” 我：“他们都在卖面膜刷广告吧？这个月的流量又不够用了吧？” 闺蜜：“还能不能好好的玩耍了！走，充话费去……” 我：“………”‍‍');
INSERT INTO `tpshop_joke` VALUES ('287', '‍‍刚刚看完看完速7我们从5号厅出来，走到2号厅。 正好一服务员关门我朋友问了句：“里面放的什么片。” 服务员说句：“咱们结婚吧！” 我朋友当时愣了！脸一红说了句：“等我回家商量商量吧！” 那女的当时脸就黑了！‍‍');
INSERT INTO `tpshop_joke` VALUES ('288', '“年轻人踏实一些，不要着急，你想要的，岁月都会给你。只要信念还在，一切都还来得及。”\r\n“来不及了，快tm从厕所给我滚出来，老子要尿裤子啦”');
INSERT INTO `tpshop_joke` VALUES ('289', '一天，甲问保险经纪：“为什么这保险合同里，重伤有的赔轻伤没得赔？”保险经纪解释：“因为微伤是不会赔钱的。”甲一听手起刀落，砍掉自己左手！这算不算重伤？保险经纪人说；当然算！可自残不赔，还涉嫌诈保！');
INSERT INTO `tpshop_joke` VALUES ('290', '同事A：老板约我去钓鱼。 同事B：那你走运了。 A：怎么说？ B：你可以独自一人享受钓鱼的乐趣。 A：你什么意思？老板呢？ B：你老板可能只是找个借口，让他老婆相信自己去钓鱼，其实是去跟他的情人约会，叫上你，只是为替他掩护。 A：你怎么知道？ B：因为他上次约的是我。');
INSERT INTO `tpshop_joke` VALUES ('291', '一天跟我老婆去逛街，她指着一家餐厅门口“请不要带宠物入内”的牌对我说：“我进去，你在门口等着。”');
INSERT INTO `tpshop_joke` VALUES ('292', '老师：“还有十多天就高考了，我知道，　　有些同学家里很有钱，不在乎能不能考上大学。　　所以呢？我希望你们那些家里有钱的，　　好好向你们的父母学习，将来也可以赚大钱的啊！　　以后你们赚了钱，我作为你们的老师，　　也可以光顾光顾你家的生意啊！你们说是吧！”　　某同学：“是的老师，我家是卖棺材的，将来您来，一定八五折。”');
INSERT INTO `tpshop_joke` VALUES ('293', '小明一家人正在看电视直播神七升空！　　“10，9，8，7，6，5，4，3，2，1，发射！”　　话音刚落，火箭冒出炽热的火焰升空了！　　小明兴奋道：“爸爸，我想我也能到火箭心射中心工作了！”　　爸爸：“嗯，你要好好学习，将来就可以到那里去工作了！”　　小明：“我不是说将来，我现在就能到那里工作！”　　爸爸：“现在？现在你能干什么？”　　小明：“我会数数啊，不是可以去给火箭发射倒计时嘛！”');
INSERT INTO `tpshop_joke` VALUES ('294', '老师：“你们知道动物园最多的动物是什么吗？”　　小红：“是猴子！”　　小明：“不对！是人！五一我跟爸爸去动物园时，　　那里人山人海，相比起来，猴子就少得多了！”　　老师：“那你们知道哪里的古董最多吗？”　　小红：“我知道，是博物馆！”　　小明：“不对！是敬老院！　　我爸爸说爷爷是个老古董，所以敬老院有很多老古董。”');
INSERT INTO `tpshop_joke` VALUES ('295', '网上买电子书，买家说已经发货了。　　可是我没收到，可他非说发了。　　好吧！我想，可能是网络不好，邮箱更新慢了吧！　　于是，我等了三天。　　三天后，我收到一包快递。　　包裹里一U盘。　　U盘里一txt电子书。　　还有卖家的求好评留言。　　我彻底晕了，买一电子书，有必要装U盘里寄过来吗？');
INSERT INTO `tpshop_joke` VALUES ('296', '男子紧张兮兮地问：“医生，近来我老是梦到捡拾了很多钱。”　　医生连忙地问：“你捡拾的是美元还是欧元？”　　男子一脸的茫然，说：“我又没占为己有，不知道。”　　医生说：“你做了一个拾金不昧的梦，值得表扬。”');
INSERT INTO `tpshop_joke` VALUES ('297', '记得在拉萨上高三那会，有一次发生了地震。　　后来学校决定把每个教室的门拆了换成门帘，就是怕再有类似的情况发生学生好逃。　　结果有一天历史老师在上课，但是下课了他还没讲完，　　别的班的同学就在走廊上追追打打的特别吵。　　因为没有门听的很清楚，历史老师就特别气愤的说：“把门关上！”　　结果全班同学异口同声的说：“没门！”　　历史老师那表情现在回想起来还觉得很好笑。');
INSERT INTO `tpshop_joke` VALUES ('298', '我认识的一个朋友今年七月份结婚！　　我问她“你个女汉子是怎么钓到爷们的？！！！”　　她说她和他老公纯属偶遇，去夜市套圈，　　瞅着扔的是兔子结果圈直接掉在旁边一个男生的身上，　　套圈老板说“套中了就跟着走啊”　　然后他们就到了结婚生子的地步了。');
INSERT INTO `tpshop_joke` VALUES ('299', '有一只狼来到了北极，不小心掉到冰海中，被捞起来时变成了什麽？　　槟榔。。。');
INSERT INTO `tpshop_joke` VALUES ('300', '我一兄弟全家搬到美国去住，一天看新闻说美国某某某小学集体食物中毒。　　一看是哥们孩子上学那学校，打电话问问孩子咋样， 　　哥们说除了他儿子，全校的人都住院了。');
INSERT INTO `tpshop_joke` VALUES ('301', '早上去早点摊买鸡蛋饼，看到摊前一个老人颤巍巍地在数手里的零钱，摊主在一旁不耐烦地等。 我皱了皱眉，上前说：老人家您要吃啥？我给你买吧。老人手一挥：别吵，我在数他这个月交的保护费足不足！');
INSERT INTO `tpshop_joke` VALUES ('302', '‍‍‍‍老师：“中国的那个汉字最霸气。” 小明：“昊。” 老师：“那小明同学说说为什么呢！” 小明：“因为它表达了对老天的不满。” 老师：“滚出去……” 好像没什么不对啊！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('303', '　　我早上都是6点30的闹钟。今天被我室友吵醒了，我就说：“你今天怎么这么早，6点30都还没到”。他平常都是8点才起来的，他说:“唔，还没响的时候我给你按了！”，卧槽，这死基佬害死我！');
INSERT INTO `tpshop_joke` VALUES ('304', '有位木匠砍了一棵树，把它做了三个木桶。一个装粪，就叫粪桶，众人躲着；一个装水，就叫水桶，众人用着；一个装酒，就叫酒桶，众人品着！ 桶是一样的，因装的东西不同命运也就不同。——人生亦如此，有什么样的观念就有什么样的人生， 有什么样的想法就有什么样的生活！');
INSERT INTO `tpshop_joke` VALUES ('305', '‍‍今天一个女同事，抽了几张楼主的抽纸，奔卫生间走去…… 楼主：“又拿哥的纸！” 她猛回头：“一会用完还给你！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('306', '舅舅家有五岁小正太，那天去超市，正太非要买薯片和巧克力，舅舅舅妈拧不过他，就给买了，，在超市俩人对着他一顿狂批啊，各种教育啊。。。结果晚上，舅舅去洗澡。。。只见正太拿着开封的薯片往他麻麻身上一丢说：没有下次了啊，，下次要吃自己买啊，，我不陪你骗爸爸了。。。。。。。我看着舅妈谄媚的脸，，石化了。。');
INSERT INTO `tpshop_joke` VALUES ('307', '‍‍“我给你个忠告。” “我也给你个忠告：请不要随便给人忠告。” “我忠告你不要忠告别人不要随便给人忠告。”');
INSERT INTO `tpshop_joke` VALUES ('308', '‍‍‍‍甲：“我讨厌死那个新来的女同事啦。” 乙：“为什么？人家又漂亮又温柔又没招惹你。” 甲：“我就是讨厌她没招惹我啊……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('309', '今天帮老妈摘豆角，要把豆角的丝都摘出来。老妈跟我说比较嫩的就不用摘丝了。说着老妈拿起一个豆角给我做示范：“你看这个豆角很嫩吧，这样的就没丝”然后她一瞬间拉出了长长的一串豆角丝。老妈愣了愣，脸一红，说：“他装嫩。”');
INSERT INTO `tpshop_joke` VALUES ('310', '“我是一个习惯漂泊的浪子，一直等待着一个，能让我放下背包的人。我想，你就是那个我命中注定人吧。”“少废话，大包小包过安检，赶紧的。”');
INSERT INTO `tpshop_joke` VALUES ('311', '本人孕妇，最近腰疼的厉害，刚刚老公一边给我按腰一边对着肚子语重心长的说：小宝你看你妈妈为了你受多大罪，长大以后一定要好好孝顺爸爸哦。。。');
INSERT INTO `tpshop_joke` VALUES ('312', '老婆因为工作上的事不高兴，我说了句：老婆，别不开心了，这个世界上，除了你的胸，没什么大不了的。。。她就把我赶出卧室了。。。');
INSERT INTO `tpshop_joke` VALUES ('313', '曾经天真的以为指纹很安全、直到有一天我喝醉了！老婆拿着我的手解锁了我的手机，多么痛的领悟。。。');
INSERT INTO `tpshop_joke` VALUES ('314', '老婆今天给我上教育课：“把老婆当公主，你就是王子；把老婆当皇后，你就是皇帝；把老婆当保姆，你就是保安；把老婆当丫鬟，你就是太监。所以，想当皇帝还是想当太监，就看你对待老婆的方式。”我：“这么说要是把老婆当空气，我岂不就是上帝！”老婆！@#！@#！@#');
INSERT INTO `tpshop_joke` VALUES ('315', '老婆：老公，如果我变成怪兽你还会要我吗？老公：你变不了怪兽，要变也是变巫婆！老婆！@#！@#￥%@#￥%');
INSERT INTO `tpshop_joke` VALUES ('316', '老婆连续生了三个都是女孩子，我说：“亲爱的，我们不生了好吗？”这二货说：“我不！我要生！老娘就要看看你上辈子找了几个情人！”');
INSERT INTO `tpshop_joke` VALUES ('317', '老婆：老公，新闻里面刚刚说到马云分分钟就搞进几个亿，你能吗？我：我能。老婆：滚。。。');
INSERT INTO `tpshop_joke` VALUES ('318', '爹妈为了防止你早恋，给你做过哪些丧心病狂的事？同学A：“不给钱。 ”同学B：“翻手机。”。。。同学N：“给了我这张脸。”');
INSERT INTO `tpshop_joke` VALUES ('319', '和室友一块喝酒，室友喝醉，扶他打车回家，看他摇摇晃晃的，让他靠在我肩膀，靠一会他就躲开了。。。难道是不好意思？？？我按着他脑袋让他继续靠。。。直到我换个姿势才发现，我衣服肩膀上都是铆钉。。。');
INSERT INTO `tpshop_joke` VALUES ('320', '你看，窗外有只漂亮的小鸟哦”　　“真的？在哪儿?”为了骗保险金还债，　　8岁的儿子被他推出窗户；　　十年后,他第二个儿子也8岁了，他从来不让他接近窗户。　　一天，窗外飞进一只漂亮小鸟，儿子开心地追了过去。　　他大惊失色，一把抱过儿子。　　儿子扭头呵呵一笑：“爸爸,不要再把我推下去了！”');
INSERT INTO `tpshop_joke` VALUES ('321', '妻：我的驾驶技术已经十分惊人了！\r\n夫：才学了几天就有这样的成绩吗？\r\n妻：当我开车时，路人都要纷纷逃避！');
INSERT INTO `tpshop_joke` VALUES ('322', '一位数学家测定完阿波罗13号从宇宙返回地球轨道的数据后，便开车回家，汽车开到城外时，他迷路了，他问了几次路才回到家里。正在念小学的儿子听他讲完这件事后，说：爸爸，幸好在空间的那些宇航员还不知道这件事。');
INSERT INTO `tpshop_joke` VALUES ('323', '老王带著小儿子上山打猎，并一路吹嘘自己如何神准。突然小儿子发现天上正有只枭盘踞不去，急忙叫著：爸爸！快拉弓箭！\r\n只见老王死命拉弓射箭，但枭依然飞在天上。老王故做惊讶，对小儿子说：没想到，今天看到奇迹，一只被射中的鸟还能飞！');
INSERT INTO `tpshop_joke` VALUES ('324', '两个好胜心强的女人在一座有喷水池的公园里碰上了。\r\n一个说：哎哟，听说你和罗伯特订婚了？罗伯特从前也向我求过婚呢。他没对你说吗？\r\n没有埃他只说过另一件事。他说他有一次遇到一个不知打哪儿来的混帐女人，追了他老半天他也没搭理。');
INSERT INTO `tpshop_joke` VALUES ('325', '一个人走进心理诊所，对医生说：“我家里人都说我有精神玻”医生问：“为什么？”“因为我喜欢丝绸衬衫。”“这有什么，我也喜欢丝绸衬衫。”“太好了，您喜欢蒸了吃，还是煮了吃？”');
INSERT INTO `tpshop_joke` VALUES ('326', '男：“你是我的太阳。”女：“我有那么酷热吗？”男：“你是我的月亮。”女：“我有那么冷淡吗？”男：“那……你是我的星星。”女：“我有那么渺小吗？快滚！');
INSERT INTO `tpshop_joke` VALUES ('327', '一次在妹妹家跟妹妹还有妹妹的同学打扑克，\r\n打到一半没忍住放了个超级响亮的屁，那个尴尬啊，\r\n不过好在他们什么都没说，\r\n为了缓和一下气氛我跟妹妹说:该你了。\r\n结果我妹神回复:我放不出来。');
INSERT INTO `tpshop_joke` VALUES ('328', '小明妈妈在做面膜，家里的门铃响了。她不好去开门，\r\n就把小明叫来，对他说：“妈妈现在见不得人，你去开一下门。” \r\n小明应了一声，就去开门了，原来是自己的叔叔。\r\n小明叔叔看到开门的是小明，就问：“小明，你爸爸妈妈呢？”\r\n小明回答道：“爸爸没在家，妈妈在做见不得人的事。”');
INSERT INTO `tpshop_joke` VALUES ('329', '元旦快到了，身边很多人都开始告白计划了。\r\n我也想，但我怕表白后，连室友都做不成了……');
INSERT INTO `tpshop_joke` VALUES ('330', '今天看电视上播，喝酒导致癌症。\r\n吓死我了，赶紧喝上一杯酒压压惊！\r\n从此我下定决心：以后再不看电视了！');
INSERT INTO `tpshop_joke` VALUES ('331', '去银行帮公司存钱5万整，　　银行：“存5万以上需要身份证！”　　摸摸兜后发现没带。　　我：“忘带了，你通融下吧！给公司存的！”　　银行：“不行！这是规定！5万以上必须要身份证！”　　我：“额，好吧你找我1毛钱吧！”　　银行：“。。。”');
INSERT INTO `tpshop_joke` VALUES ('332', '我从小跟老爸亲，问爸爸俺怎么来的。　　老爸穿了个夹克，拉开拉链说：“那天我拉开拉链你蹦出来了。”　　时隔许久，我还对那件灰不拉几的拉链有着革掵一样的感情！　　后来我对不同性别胸的尺寸产生了怀疑，　　老爸又说：“小朋友都是吃爸爸的奶长大的，你看，爸爸的都给你吃瘪了。”　　于是我跟老爸更亲了。');
INSERT INTO `tpshop_joke` VALUES ('333', '曾经我无可救药的喜欢上了一漂亮的女网友，向她表白却被拒绝了。　　她说是为了我好，我一直以为只是她的借口， 直到我见到了她的素颜。　　网络上还是有很多好人啊。');
INSERT INTO `tpshop_joke` VALUES ('334', '我练车的时候，发现考官抽的都是好烟，　　什么玉溪，中华，不断的抽。　　直到我考科目2，第2次还没过的时候，我终于释然了！');
INSERT INTO `tpshop_joke` VALUES ('335', '大学一室友打呼噜巨响，　　有天早晨忽然问我们：“我昨晚是不是又打呼噜了？”　　我下铺那哥们就说了：“你打没打呼噜我们不知道，反正楼道里声控灯亮了一宿！！”');
INSERT INTO `tpshop_joke` VALUES ('336', '连续下了十多天的雨了，百姓都盼望着。 　　宋仁宗拍着包拯的肩：“朕决定把你悬挂在城门上。”　　包拯吓了一跳回答说：“但微臣额上的不是太阳是月亮啊！”　　宋仁宗：“没事，挂久一点就会升级成太阳。”');
INSERT INTO `tpshop_joke` VALUES ('337', '小时候发高烧，奶奶带我去打完吊瓶回到家里，　　我躺在床上，奶奶问我：“圆圆啊？烧的怎么样了？”　　我记得我当时的回答是：“烧的挺好的。。”');
INSERT INTO `tpshop_joke` VALUES ('338', '朋友：“我们这今天热死了！”　　我：“你们那热？你是没来西安！”　　朋友：“西安今天有多热？”　　我：“我上次买的塑料手机壳，今天才发现这是橡胶的！！！”');
INSERT INTO `tpshop_joke` VALUES ('339', '当幼师的朋友跟我说，上课的时候，　　发现班里有个小姑娘一副心事重重的样子看着她叹气，　　一下课朋友就问她：“宝宝你今天怎么了？”，　　她拉起我朋友的手，摸索着她的手背忧愁的说：“哎，都年底了你还没个对象，全班都为你操碎了心啊……”　　朋友当时就愣住了。');
INSERT INTO `tpshop_joke` VALUES ('340', '我上的煤矿学校，最近我们来自各个煤矿的配电工集体培训考证，今天天比较热，　　教室内有两台大空调，老师只让开一台，开一台根本不起作用。　　老师说空调只能开一台，要不会跳闸的。　　这时底下一同学默默的说道：“骗谁呢，一屋子的电工，制不了他一个跳闸？”');
INSERT INTO `tpshop_joke` VALUES ('341', '今天跟微信妹子约了，吃完夜宵她问我去哪，老子晕死，点着她脑袋：还能去哪？各回各家，各找各妈！骂完哥就回家了 回家摸着口袋唯一的五块钱，哭了个通宵，天亮的时候发现屁股荷包里面还有100块，又哭到下午……');
INSERT INTO `tpshop_joke` VALUES ('342', '一同学学游泳，怎么学都不会，教练很生气。 有天教练忍不住发火了，就骂到：真不知道当时你怎么游到你妈妈肚子里的？！ 我草，我们当时都愣住了，教练这话说的有点醉啊！ 结果我那二货同学来了一句：当时只知道使劲游才有机会活命……');
INSERT INTO `tpshop_joke` VALUES ('343', '单位一小菇凉特逗！经常跟我们勾肩搭背开玩笑！在单位接打电话时如果不是公事经常相互捣乱！有一回我给我妈打电话，我说什么她说什么！最后我说：妈，你儿媳妇要跟你说话！结果这货来了句：妈妈，我是你儿媳妇！！都大半年了，我妈还记着呢！如果过了。绑也要把她绑回家！！');
INSERT INTO `tpshop_joke` VALUES ('344', '进攻就是最好的防守！！！过年回家准备对那些爱八卦的亲戚邻居先发制人，问他们：夫妻关系和睦么？退休金有多少？炒股赚了没有？孩子学习成绩怎么样？有没有给孩子结婚买房？……');
INSERT INTO `tpshop_joke` VALUES ('345', '1、开学后，老师问小明：“你假期作业呢？” 小明：“忘带了。” 老师大怒：“你开学不带作业带啥了。” 小明：“一颗勇敢的心。”');
INSERT INTO `tpshop_joke` VALUES ('346', '一公车急刹，车上一小伙碰到面前的女孩。这女的顿时就火了骂到“流氓,色狼”,小伙碰到人就没说什么,这女孩看到他没还口就骂的更带劲了，“你个流氓，色狼，你妈生你的时候，你都不忘回头看一眼。”小伙被骂的也窝火,憋红着脸说“你…你才流氓，你才色狼，你在你妈肚子里的时候，还天天看你爸的呢。”');
INSERT INTO `tpshop_joke` VALUES ('347', '今天是学校大四最后一天了。 一大四的同学晚上喝多了，跑到女生宿舍底下大喊：“学妹，哥要走了，请不要挂记我放荡的青春”然后想想又补充一句：“记得把我们的孩子养大！”逆天的是，一女生从宿舍走出来，冲着下面喊：“学长，你放心走吧。孩子其实不是你的!!!!”尼玛，当时周围的人全笑鸟了。。。');
INSERT INTO `tpshop_joke` VALUES ('348', '一晚一对夫妻以为大儿子与小儿子都睡着了，便开始xxoo起来，过了一会小儿子爬了起来，这对夫妻尴尬无比，这还不是gc，gc是大儿子站起来给小儿子一巴掌，叫你躺下看！你非要站起来！');
INSERT INTO `tpshop_joke` VALUES ('349', '楼主送纯净水苦逼工一枚，最开心的就是给学校女宿舍送水，每次拎上去叫：哪儿要水？ 然后就见宿舍门陆陆续续的开了，穿着睡衣或是披着浴巾的妹子向我招手：“帅哥来这儿，来这儿！” 场面可壮观了！');
INSERT INTO `tpshop_joke` VALUES ('350', '刚刚玩手机抽奖，上面提示说 ”听说用舌头刮奖成功率会更高哦” 天真无邪的我试了一下 突然想起:手机上的细菌是马桶的10倍之多。。。 我特么是不是吃过屎了！！！ (求过，真事，给原创一点动力吧～)');
INSERT INTO `tpshop_joke` VALUES ('351', '‘某村一农民，天天喂猪吃泔水，结果被“动物保护协会”罚款了一万元……虐待动物。于是农民改喂大米饭，结果被“慈善者协会”罚款了一万元……浪费粮食。农民没办法，改喂猪吃天山雪莲，结果被“环境保护者协会”罚款了一万元……破坏稀有珍奇植物。过些日子领导又去视察，问农民现在喂猪吃的什么？农民答：喂啥呀？喂啥都tm挨罚！现在我每天给它一百块钱，让自己出去吃！');
INSERT INTO `tpshop_joke` VALUES ('352', '‍‍‍‍女友坐在我大腿上看书，我在玩手机。 忽然间女友一边轻咬下唇，一边专注看书，画面很是诱人。 我便轻轻的亲了女友一下。 女友皱了下眉看了看我，然后继续看书。 接着又出现刚那诱人的画面。当我又想亲女友的时候，女友一手捏住我的嘴唇。 怒骂：“滚一边去，老娘牙疼你掺和个啥！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('353', '　　初中时候在班上成绩一直中下游，有一次数学超常发挥，一百五的卷子考了一百三，激动类一b，放学的时候班里一学霸的妈妈过来接她，挥舞着她考了一百四的数学卷子吼道：你才考这么点，在这么下去怎么办啊！我听的一愣一愣的，gc她妈妈转头问我 你考了多少，我结巴的回答一百三，她妈妈转头就是给学霸一耳光：连他都考了一百三！记得 那天晚上回家的路 我整个人都是奔溃的...');
INSERT INTO `tpshop_joke` VALUES ('354', '康师傅茉莉蜜茶是个牛B的饮料, 喝完一半加点水,就变成了茉莉花茶, 再喝完加点水, 变成了茉莉清茶, 喝完以后倒点水, 它就成了农夫山泉有点甜.');
INSERT INTO `tpshop_joke` VALUES ('355', '同学不想上课，给老师写了个请假条，上面写着：“我今天4567，无法上课。” 什么狗屁东西，老师看了后把请假条顺手扔在桌上，刚好过来音乐老师拿起来一看：我今天发烧拉稀，这小子还挺有音乐细胞哈！');
INSERT INTO `tpshop_joke` VALUES ('356', '‍‍‍‍早上起来去拿个东西，回来的时候就听见我妈，对着我的鼓鼓被窝在说话。 其他的没听见，就听见最后一句：“和你说了那么多你怎么还不理我啊！” 然后我在我妈背后说了一句：“妈，我不在被窝里啊！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('357', '‍‍‍‍今天和朋友在河边吹风，她在听歌，然后手一滑，刚买的5S差点掉进河里，幸好耳机绳拉着。 她若有所思的说了一句：“看，这就是音乐的力量！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('358', '老师：今天作文的题目是“假如我是董事长”，下面大家开始写，小明坐在座位上一动不动。“小明，你为什么不写？” “你见过哪个董事长自己写作文？去把我的秘书叫来” “滚出去”');
INSERT INTO `tpshop_joke` VALUES ('359', '‍‍‍‍老公：“你是我的玫瑰你是我的花……” 老婆：“死鬼竟说好听的，真肉麻！” 老公：“可怜天天被你扎……” 老婆：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('360', '听说睡觉的时候，相互拍背可以更快的入睡。 一小伙伴回复说：“都是特么的骗人的！昨晚和媳妇失眠，就相互拍背。。拍到最后TMD打起来了。。”');
INSERT INTO `tpshop_joke` VALUES ('361', '我在宾馆收银台工作，一天一对男女来开房，女的态度很不好，而且骂人，哥不和她计较。 结帐走时等那男的来付钱，我说：“那位女士是我们的老顾客，这次半价”。');
INSERT INTO `tpshop_joke` VALUES ('362', '姑娘，茫茫人海我们本互不相识，今日却在肯德基拼桌用餐也是种缘分，但是我弯下身子去偷看你内裤的时候你却偷吃了我几根薯条，就太不礼貌了吧？！');
INSERT INTO `tpshop_joke` VALUES ('363', '“不好意思啊，踩到你脚了。”“姑娘，你知道吗，前世五百年的回眸才换得今世的擦肩而过，茫茫人海之中，你没有早一步，也没有晚一步，却偏偏踩到了我，那我也没有别的话可说，只有问一句：你TM是不是故意的？”');
INSERT INTO `tpshop_joke` VALUES ('364', '中午下班回寝室，我抱着换洗的衣服准备去洗澡…没一会儿，我气冲冲的走出来，说:唐师，毛老头那个胎神不买日常用品，洗澡用我的洗发液就算了嘛…他居然还用我的香皂…香皂是能打伙用的嘛？？…唐师:你怎么知道他用了你的香皂？？…我:我昨天洗澡用过后是把香皂盒盖着的…可今天是开着的…唐师:这就能证明他用了你香皂？？…我:那香皂上面那根又黑又粗的毛是怎么回事？…唐师什么一下…说:好嘛，那你们打一架嘛…');
INSERT INTO `tpshop_joke` VALUES ('365', '刚出车站 有一大叔 拿着肾6优雅的向我摆手 “500要不要！ 我就问他:是不是假的? 大叔以迅雷不及掩耳盗铃之速度扣下电池:小伙子你看 电池都是原装的！');
INSERT INTO `tpshop_joke` VALUES ('366', '‍‍‍‍中午吃过饭回去，看到三个四五岁的小孩吵架，一个小男孩对俩女孩。 男孩说：“你们敢过来我就拍死你们。” 一个女孩接着说：“你过来我也拍死你。” 边上的女孩马上接了一句：“你先拍死我俩，我再拍死你。” 当时就忍不住笑喷了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('367', '‍‍‍‍今天到幼儿园接儿子，在外面等着。 看见屋里一个小男孩，背对着我举着一堆零食对一个小女孩说：“婷婷我喜欢你。” 小女孩说：“我还没有心理准备。” 小男孩说“聘礼我都拿来了。” 小女孩说：“我已经有喜欢的人了，如果你爱我就别伤害我。” 我去，现在的熊孩子都这么早熟了，还好我儿子不是这样。 我去，那不是我儿子吗？ 不说了我想冷静冷静，别问我冷静是谁，我不想说。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('368', '‍‍‍‍前段时间公司新来一小伙，因对我们主管不满，把我们主管骂了。 大家都以为他完了，没想到这货现在竟然升职了。 我以为我们主管喜欢这种人，今天也学新人跟他吵了一顿。 我们主管竟然要我滚蛋，TMD离职手续都办了。 才知道那小伙是经理的儿子，消息不灵坑死人啊！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('369', '昨晚加班提前结束，回到家老婆神秘兮兮的对我说：“老公，我给你变个魔术好不好？”只见她走到床前大喊一声：“变”，然后一把掀开被子，令人震惊的一幕出现了，只见我哥们一脸茫然的看着我俩，笑死我了，这SB肯定不知道自己是怎么来到这里的！');
INSERT INTO `tpshop_joke` VALUES ('370', '有一次和媳妇去沙滩晒日光浴，媳妇突然对我说：“老公，快点把裤子脱了，万物生长靠太阳。”真不带这样损人的！');
INSERT INTO `tpshop_joke` VALUES ('371', '　　幸福离我们并不遥远，它一直在我们那些长相姣好，家境优越，脑瓜好使，积极乐观的朋友身上。 ');
INSERT INTO `tpshop_joke` VALUES ('372', '今天第一次参加家长会，不免有些紧张，其他家长都在聊天，只有我默默坐着，过了一会儿，旁边一个大哥忍不住问我：“王老师，你让我们来开会，不上去讲两句吗？”');
INSERT INTO `tpshop_joke` VALUES ('373', '一哥们出国回来，带了一大堆礼物，送给老婆娘家人。老婆激动的听老公说：“这是你姥爷的，你姥姥的，你大爷的，你奶奶的，你妈的，你妹的。。。”说着说着，两人就干起来了。。。');
INSERT INTO `tpshop_joke` VALUES ('374', '女孩新买了iPhone6，室友很羡慕的问：“你哪来的钱？” 女孩神气的说：“我群发了一条短信给班里的男生说：我怀孕了，钱打进卡号：××××，我自己解决！第二天我便凑够了！” 室友听了无限感慨：“人多就是力量大啊！”');
INSERT INTO `tpshop_joke` VALUES ('375', '丈夫：“谢谢你在邻居面前夸我有才华。” 妻子说：“你要钱没钱，要貌没貌，要地位没地位，我不说你有才华，别人还不得骂我傻啊。”');
INSERT INTO `tpshop_joke` VALUES ('376', '　　女朋友要和我分手，原因是她看到我喝酸奶时舔瓶盖了。我给她解释说这是勤俭节约，是我们的优良传统。她生气的说:不是因为这个！这么浅的瓶盖你都舔不干净！');
INSERT INTO `tpshop_joke` VALUES ('377', '今天一朋友问我借钱，说女朋友打胎，我摸着口袋心里想着:不借吧，又说我小气。借吧，又舍不得，那毕竟是我的亲生骨肉。');
INSERT INTO `tpshop_joke` VALUES ('378', '今天女同学穿了一件很暴露的衣服，让我给点评价，我一看说：让我有股想犯罪的充动啊。她竟然说那你就行动啊，我不会怪你的。我激动的不行，上去就按住她，把她脖子上的金链子扯下来就走。这真不能怪我，谁让她穿这么少，把金链子都露出来了。');
INSERT INTO `tpshop_joke` VALUES ('379', '唐僧师徒取经路遇一户人家，欲化斋。男女主人正行房事。唐僧见状：“光天化日。。。”话还没说完女主人一鞋底摔在唐僧脸上，大骂：“老娘长这么大还第一次遇到和尚化这个的！”');
INSERT INTO `tpshop_joke` VALUES ('380', '我一室友喜欢打篮球，打篮球一般不带手机，有天他女朋友打电话来，我接着电话 她先来了句s b，我说我不是你的s b 你的s b在操场打篮球。。。。');
INSERT INTO `tpshop_joke` VALUES ('381', '　　一天，一只大象猜到了蚂蚁窝，窝里的蚂蚁爬到了大象身上，大象一抖蚂蚁们掉了下来，只有一只还在大象脖子上，地上的蚂蚁们对那只蚂蚁说：“快！掐死他..........\"');
INSERT INTO `tpshop_joke` VALUES ('382', '几个同事大年初一聚餐，我去订饭店，此为背景，服务员说:还有包间，大年三十。我说大年初一，她说好的、大年三十，我说大年初一，她说是大年三十，我说我订大年初一晚上，服务员:是大年初一晚上，包间名是大年三十。拿过来订餐本一看，包间名从大年三十、初一、正月十五、八月十五、春分夏至什么都有，妹子 你这么调戏我好么。');
INSERT INTO `tpshop_joke` VALUES ('383', '吃完早餐结账时候发现没带钱，只好让侄女来赎我，你能想象一个6岁的小女孩在众目睽睽下拿着2块5帮我结账后，大喊：走吧？败家玩意！');
INSERT INTO `tpshop_joke` VALUES ('384', '　　正在飙高音是邻居过来了。 　　邻居：“刚才是你在唱高音吗？” 　　我：“是啊。” 　　邻居：“真好听！” 　　我：“不好听 不好听” 　　邻居：“太谦虚了，我也想唱 一起吧。” 　　整栋楼里都回荡着俩的天籁之音');
INSERT INTO `tpshop_joke` VALUES ('385', '据意大利科学家研究发现，在家经常被老婆嫌弃、嘲笑、冷言冷语……男人患心脏病和中风的风险是正常男人的2.6倍，在这一点上他跟职业烟民和换肥胖症的患者概率是一样一样一样的，所以说没事别bb……，我们男人是很脆弱的……');
INSERT INTO `tpshop_joke` VALUES ('386', '这年头结个婚不容易。我为了结婚买房 装修把家里积蓄都光光了，女友家还要8万的彩礼，还要买车才嫁，特别是岳母那态度，没钱让我滚！我就不明白了，你嫁过来是跟我过日子的，又不是卖给我做丫鬟，把我弄的一贫如洗对你有什么好处！！！？？？');
INSERT INTO `tpshop_joke` VALUES ('387', '大师沉思片刻，缓缓对我说道：小伙子，你命中有贵人，而且还是女的。我听后激动的给大师塞了个红包，问道：大师，能不能告诉我那女的是谁？大师回过身去，抬头看天，缓慢道：你妈！ 我…………');
INSERT INTO `tpshop_joke` VALUES ('388', '　　一妻子化完妆后问丈夫：“我漂亮吗？”  丈夫闭上眼，说：“漂亮！”妻子说：“漂亮就漂亮，闭眼干嘛啊？”丈夫说：“我怕睁着眼说瞎话。”');
INSERT INTO `tpshop_joke` VALUES ('389', '6岁的小侄女在家不乖，被他爸爸打了，哭着要找妈妈告状，在家里转了一圈愣是没找见她妈妈，回来一把抱住爸爸的大腿，哭着说“爸，妈妈打我~”');
INSERT INTO `tpshop_joke` VALUES ('390', '刚看到的…小明和妈妈去动物园看猴子~小明指着猴子说~妈妈，你看！旁边这叔叔多像猴子啊…嘘，别瞎说，他要生气的…小明说，没事，它（他）又听不懂…');
INSERT INTO `tpshop_joke` VALUES ('391', '一电视剧里真实的桥段：男子去参军，临走时对妻子说：“八年抗战很快过去，带好我们的儿子！”尼马、你是有未卜先知吗！');
INSERT INTO `tpshop_joke` VALUES ('392', '近期长沙市有一市民把移动告上法庭觉得自己一个月150的流量用不完就清零很不公平，为什么流量超了要交费，没用完的不能累计？难道没有用完的流量就没交钱？！');
INSERT INTO `tpshop_joke` VALUES ('393', '今天刚出小区门口，突然冒出一五六岁的小萝莉，抱住我大腿哭着喊，叔叔你娶了我吧！！！我正纳闷时，这时后面传来一声音，你就是结婚了，今天也得给我去上学！！！');
INSERT INTO `tpshop_joke` VALUES ('394', '狗去院子里捉住了一只猫，它呲着牙对猫威胁道：“抓只老鼠回来见我！”猫很害怕，赶紧去巷子里抓了一只老鼠回来。看见猫提着老鼠颤颤巍巍的来到了它的面前，狗忽然咧着嘴一笑，从口袋里喜滋滋的掏出了一副扑克：“来，咱们斗地主！”');
INSERT INTO `tpshop_joke` VALUES ('395', '妈妈给儿子讲故事：”有两个人，一男一女，他们天天并肩而行，却不能交谈一句，甚至扭头看对方一眼都是奢望。。。“孩子打断了妈妈的话，问道：”你说的是新闻联播那俩播音吧！”');
INSERT INTO `tpshop_joke` VALUES ('396', '一哥们走在路上，路旁小姐拉客，道:帅哥，怎么玩都可以哦。哥们冷笑:我的玩法你接受不了！小姐不解说:你怎么玩？哥们回复:白玩。');
INSERT INTO `tpshop_joke` VALUES ('397', '某现场，主持人问一男嘉宾，和女朋友恋爱五年为什么分手了，男嘉宾回答，她妈逼的，见众嘉宾迟疑，于是改口，她母亲逼的。');
INSERT INTO `tpshop_joke` VALUES ('398', '初中时候，，班上一女孩纸暗恋篮球队一帅哥。gc来了，，那哥们打班赛输了，，我们女主角在旁边大喊一句：你不是一个人啊……');
INSERT INTO `tpshop_joke` VALUES ('399', '昨天有女的问我想不想要她做我女朋友，我说想，然后她又说了让我不要有这种想法。。。');
INSERT INTO `tpshop_joke` VALUES ('400', '宿舍里，一胖妹子坐床上玩手机，我眼睛随意一瞄，看见一只蜘蛛爬上她的床。我就对她大喊：哇！！你床上有蜘蛛！有蜘蛛啊！！这时，这货怒了，对我大吼：你床上才有只猪！！！');
INSERT INTO `tpshop_joke` VALUES ('401', '过年老爸杀鸭子，那鸭子也是命大，血都留了一碗还能逃走，老爸都不去追，躺沙发睡觉。都过了两天了鸭子还精神抖擞，照常吃食，我也是醉了。。');
INSERT INTO `tpshop_joke` VALUES ('402', '想起我爸以前真是牛X，上班前把电脑的电源线藏起来。我找了一天都没找到。下班后告诉我，藏在我书包里...知子莫如父啊，这孩子得多不爱学习啊');
INSERT INTO `tpshop_joke` VALUES ('403', '公交车上，一女的抱了一条狗坐在我旁边，我看了那狗一眼，它也看了我一眼，再看，它还看我，我就一直盯着它看，它也盯着我看。 持续一段时间后，那女的看了看她的狗，又看看我，说：“你们认识？”');
INSERT INTO `tpshop_joke` VALUES ('404', '昨天室友淘宝上买一件衣服，卖家说：“上图好评立马返现五元。” 于是，室友穿上衣服,自信美美地拍了五张照片，今天卖家给朋友返现了十元，叫室友把图片给删了…');
INSERT INTO `tpshop_joke` VALUES ('405', '儿子考试完了，父亲问他：“这次考试的成绩怎么样?”\r\n“不是最好的。”\r\n“那是最差的?”\r\n“也不是。”\r\n“中间吗?”\r\n“比以前有进步。” \r\n…\r\n父亲烦了，喝道：“到底第几名?”\r\n儿子：“倒数第二名！”');
INSERT INTO `tpshop_joke` VALUES ('406', '儿子指着书上的一个“鸣”字问父亲：“爸爸这是什么宇?”\r\n“鸟字。”父亲很肯定地回答。\r\n“‘鸟’字怎么有个‘口’字旁呢？”儿于表示怀疑。\r\n“傻子，没有口的鸟还能活吗?有口的鸟是活鸟，没有口的鸟是死鸟。”\r\n父亲解释说。');
INSERT INTO `tpshop_joke` VALUES ('407', '子：“爸爸，我知道什么叫规律。”\r\n父：“是吗?你举个例子。”\r\n子：“到月底你和妈准吵架，这就是规律。”');
INSERT INTO `tpshop_joke` VALUES ('408', '4岁的孩子告诉父亲：“叔叔给我糖果了。”\r\n“你说了 \r\n‘谢谢’没有?”\r\n“忘啦。”\r\n“那么赶快去说。”\r\n“怎么样，说了吗?”儿子回来后，父亲忙问。\r\n“说了，可是没有用了。”\r\n“为什么没有用？”\r\n“叔叔说：‘不用谢’。”');
INSERT INTO `tpshop_joke` VALUES ('409', '爸爸：“听说你正在学反义词，那么，我问你：‘好’的反义词是什么?”\r\n儿子：“不好。”\r\n爸爸：“‘黑’呢?”\r\n儿子：“不黑。”\r\n爸爸：“混帐!”\r\n儿子：“不混帐!” \r\n。');
INSERT INTO `tpshop_joke` VALUES ('410', '本人今年29岁(剧情需要)，每天早上lol，每次都被骂，说我是小学生，突然觉得自己年轻了许多。。于是，每天都撸');
INSERT INTO `tpshop_joke` VALUES ('411', '我太喜欢这段话了 不要去追一匹马， 用追马的时间种草， 待到春暖花开时， 就会有一批骏马任你挑选； 不要去刻意巴结一个人， 用暂时没有朋友的时间， 去提升自己的能力， 待到时机成熟时， 就会有一大批的朋友与你同行。 用人情做出来的朋友只是暂时的， 用人格吸引来的朋友才是长久的。 所以， 丰富自己 比取悦他人更有力量： 种下梧桐树， 引得凤凰来。 你若盛开，蝴蝶自来！你若精彩，天自安排！');
INSERT INTO `tpshop_joke` VALUES ('412', '我老婆是警察，前天吵架，我和她打架，打不过她就算了，特么的还告我袭警啥的把我抓过去拘留了一天。');
INSERT INTO `tpshop_joke` VALUES ('413', '今天和朋友出去吃饭，最后结账，老板：“304，收您300吧”。我说：“吃了304，要是吃了307是不是也可以收我们300呢？”老板比较爽快：“那是当然”，我擦了擦嘴：“服务员，再给我拿一瓶可乐，要冰的。”');
INSERT INTO `tpshop_joke` VALUES ('414', '昨天晚上回寝室 楼道是声控灯，二货同学问我：你信不信我一个屁把灯蹦亮。我说：怎么可能。然后他用尽全力放了了一个巨响的屁，他居然做到了，灯亮了，现在我在上网 他还在洗内裤！！！');
INSERT INTO `tpshop_joke` VALUES ('415', '小学有个同学是考古学家儿子，叫覃奀垚qín ēn yáo.第一天开学老师拿花名册点名都快哭了，一个字都特么不认识啊！最后含着泪问了句，谁叫，西早不大三个土。。');
INSERT INTO `tpshop_joke` VALUES ('416', '14岁做民工的王宝强，1999年被人说是大骗子的马云，74年大家还在笑话的洗车工周润发，曾在校篮球队被体育老师看不起的乔丹，曾被无数白人排斥的黑鬼迈克尓杰克逊，曾被拒绝1000多次才当上演员的史泰龙。人生或多或少都会遇到挫折，除非你的心脏停止跳动了，不然不要轻易放弃梦想，有梦就去追，早晚会实现。-------致所有为梦想奋斗的人！');
INSERT INTO `tpshop_joke` VALUES ('417', '同学聚会，喝酒谈天，有几位身价还可以的吹嘘起来，最后谈到股票了，有个吹嘘自己扔进去五百万套住了，有个说拿了两千万补仓，还有个吹嘘老婆背着他买了六 千万基金赔了，最后有位同学实在看不下去了，拿起手机说：爸，忙着吗？你吃了午饭把沪市大盘拉起来吧，我几位同学投了点钱在里面炒股，赔着呢。');
INSERT INTO `tpshop_joke` VALUES ('418', '跟老婆出去逛街，老婆看中一条裙子，我嫌太露的多，不让她买。 她说：“我就试试，行么？” 看着她可怜巴巴的小眼神，我妥协了。 结果她穿上裙子，风一样的跑了，留下我和售货员在店里。');
INSERT INTO `tpshop_joke` VALUES ('419', '晚饭前突然有人敲门，我打开门一看，一个送外卖的小伙子。 拎了很多大河蟹站在门外。 我说：“你一定是弄错了，我并没有叫外卖。” “这我知道。” 小伙子说。“这是你的微信好友让我拿给你看一眼的， 这是她今天晚上要吃的美食。她手机坏了，发不了朋友圈。快点看，我他妈的还得跑十几家呢。');
INSERT INTO `tpshop_joke` VALUES ('420', '以前见过这么几句话：“我姓刘，却留不住你的心，我姓李，却理不清你的情，我姓张，却张不开嘴说爱你”，现在我感觉这些都弱爆了，那天看见一个：“我姓高，却给不了你高潮”……');
INSERT INTO `tpshop_joke` VALUES ('421', '你们有谁在听梁静茹的《暖暖》的时候把“安全感”听成“鹌鹑蛋”的？我不信就我一个！');
INSERT INTO `tpshop_joke` VALUES ('422', '去券商开户，券商问：“多大了？”“38”“结婚了么”“结了”“怕老婆不”“怕”“住几楼呀”“21楼”“那您别开了，我们现在只给2楼以下的开！”');
INSERT INTO `tpshop_joke` VALUES ('423', '股市就是《西游记》，各路妖魔神怪齐出想发财，最后不是死了就是被庄家出来收得人财皆空。我就想考一下大家，是谁发布了唐僧肉长生不老的利好消息？');
INSERT INTO `tpshop_joke` VALUES ('424', '一句话震慑全世界！俄罗斯：我要炸叙利亚！朝鲜：我要打韩国！希腊：我要退出欧元区！美国：我要加息了！中国：我要开盘了！');
INSERT INTO `tpshop_joke` VALUES ('425', '租住的地方附近有一物流公司，每天车来车往。我减肥，常常去他们院子里练练器械。昨天晚上，守门的大爷走过来问我：装卸工干不干？一个月能挣七八千。我。。。');
INSERT INTO `tpshop_joke` VALUES ('426', '昨天坐公车，发生了吵架的事情，一男的骂售票员：长这么难看不知道怎么艹的。售票员回：我长怎么难看都是爹妈给的，哪像你街坊四邻凑的。。。');
INSERT INTO `tpshop_joke` VALUES ('427', '“这次召回行动已经开始1个月了，但还没有一辆车到我们这里返修。”汔车修理店的老板对维修师傅说。“这次召回的原因是什么？”“刹车失灵！”');
INSERT INTO `tpshop_joke` VALUES ('428', '小王去吃西餐，服务员给他端上来牛排，他奇怪地问：“这牛排怎么有酒味？”服务员向后退了两步说：“现在呢？”');
INSERT INTO `tpshop_joke` VALUES ('429', '我：“老板，你这水果这么多斑能吃吗？”老板：“选水果就像选老婆一样，有讲究的。”我：“怎么说？”老板：“漂亮的不放心！”。。。说的我竟无语反驳。');
INSERT INTO `tpshop_joke` VALUES ('430', '有人说一边充电一边玩手机，像狗被拴着一样。其实这种说法也不对，狗被拴着，肯定会极力挣扎。而手机党，一般稍微牵动一下眼睛就会瞄一下插座。');
INSERT INTO `tpshop_joke` VALUES ('431', '一室友问：你们觉得街上的女人和你们女朋友有啥区别？ 立马有人回答：街上美女有大凶器！ 接着又有人说：街上的美女裙子都很短。 室友幽幽地抽了口烟，说道：你们说的都不是本质，最大区别在于街上的女人都是活的…');
INSERT INTO `tpshop_joke` VALUES ('432', '曾志伟去姚明家玩，敲他家门，一直没开门，以为姚明不在家，就走了。第二天看着姚明无精打采的，就问他昨晚干嘛去了。姚明神秘的说：昨晚我家好像闹鬼了一直有人敲门，可是从猫眼又看不见人，吓得我一整夜没睡。');
INSERT INTO `tpshop_joke` VALUES ('433', '不要去欺负一个已婚女人，因为你不知道她身后男人的质量；更不要去欺负一个未婚女人，因为你不了解她身后男人的数量。so.女人都比较惹不起.....');
INSERT INTO `tpshop_joke` VALUES ('434', '东北散文 -----作者：不鸡到 假如生活 护龙了你 败吱声 败扎乎 败整景 败嘟囔 败苦丧个脸子 你就搁那嘎达趴着 也败起来 一直往前顾勇 像毛毛虫一样 顾勇……顾勇…… 一直顾勇 总有一天 你会变成…… 有翅膀的……… 扑楞蛾子！ 那个时候 哥会带你装逼 带你飞');
INSERT INTO `tpshop_joke` VALUES ('435', '今天碰到一个碰瓷老太太，硬说是我撞的，我急了，拿出手机装B说：爸，给我150万，我要撞死一老太太。 那老太太一下就起来了，打了我一巴掌还说：骑自行车你还装什么富二代，于是趁机我从容的躺下了。 然后我听到老太太拿起电话说：老头砸，把拖拉机开过来。。。');
INSERT INTO `tpshop_joke` VALUES ('436', '老公出差，晚上给老婆电话。聊几句老婆就想挂：“早点睡吧！今天我太累了...” 老公：“我怎么听着屋里还有人呢？” 老婆：“你出差，我叫闺蜜雨晴来陪我，要不，我叫她跟你说两句！” 老公：“不用了！我相信你” 放下电话后，老公看着身边的雨晴，抽了一宿的烟……这个故事告诉我们：吸烟有害健康！');
INSERT INTO `tpshop_joke` VALUES ('437', '【你喝水的杯子有毒吗】 塑料瓶看底部三角形内数字：1号PET：耐热至65℃，耐冷至-20℃。2号HDPE：建议不要循环使用。3号PVC：最好不要购买。4号LDPE：耐热性不强。5号PP：微波炉餐盒、保鲜盒，耐高温120℃。6号PS：又耐热又抗寒，但不能放进微波炉中。7号PC其它类：水壶、水杯、奶瓶。');
INSERT INTO `tpshop_joke` VALUES ('438', '出去吃饭旁边桌子上坐着一对夫妻带着一个小孩。 小孩一直吵吵，他妈妈就吓唬他，说你再吵吵，旁边的叔叔生气吵你啊。 我一嗓子吼过去，别吵吵！整个餐厅都静了，小孩哇一声哭了。 孩子他爹等等...打人不打脸啊......');
INSERT INTO `tpshop_joke` VALUES ('439', '化学课上…老师提问：“钠是什么？” 同学面面相觑，鸦雀无声，这时化学老师有点发怒，便提高音量问道：“钠是什么？” 突然间老师的电话响了：“那是一条神奇的天路诶...” 从此，化学老师再也没有带手机上过课。');
INSERT INTO `tpshop_joke` VALUES ('440', '马上要过所谓的情人节了……下面是某女孩空间的一个心情：“不管是老公，情人，还是追你的男人，在情人节没有送你礼物，可以马上让他光荣下岗了. 原因一：他不知道情人节他是白痴？！ 原因二：他知道是情人节，不送礼物，他是铁公鸡！ 原因三：情人节不送礼物，他根本没拿你当回事？ 原因四：情人节不送给你，他是送给谁了？ [偷笑][偷笑][偷笑][偷笑][偷笑][偷笑]”！我想问问你们女人凭什么这么要求男人，你们扪心自问一下，平时你都做了什么，打电话敷衍了事，别人说了几百句，你一句哦，知道了，我困了，我要忙了，有资格要礼物？发信息也是一到俩个字，这样的态度，你知道他心里是怎么想的，你们却厚着脸皮发那样的心情，你考虑过男的吗？也许他正好这几天经济拮据？你要他借钱给你买礼物，你收到这样的礼物，心安吗？姐友我不是喷女性，只是说我的遭遇而已，谢谢你们能理解，在这里，预祝天下有情人终成眷属，生活美满，身体健康！');
INSERT INTO `tpshop_joke` VALUES ('441', '‍‍三个女子去算命。 因不知算卦先生算的准不准，就对算命先生说：“你看我们三个人，那个是寡妇？” 算命先生掐指算了算，嘴里念念有词：“抬头瞅低头观，寡妇头上冒清烟。” 俩女子闻听，一齐向另一女子头上望去。 算命先生立即说：“这个是寡妇。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('442', '　　食人族父子打猎，见一瘦子，父说太瘦，放了。见一胖子父说太胖，腻，放了。见一美女，父说：“把她带回家，把你妈炖了。”');
INSERT INTO `tpshop_joke` VALUES ('443', '话说我堂姐家有一两岁的淘气小子！有天我去他家他硬要玩我手机，没办法只有给他玩，一下子没注意他把我手机拿到桶里还在那委屈的说舅舅你手机咋不会游泳！我那个眼泪啊,.');
INSERT INTO `tpshop_joke` VALUES ('444', '萝卜小姐拼命减肥，骨瘦如柴，她妈妈不满的说:“瘦成这样谁娶你？”萝卜小姐不屑地说:“白酒天天盯着我呢老想泡我，还管我叫人参。”');
INSERT INTO `tpshop_joke` VALUES ('445', '‍‍文学大师钱钟书最怕被宣传，更不愿意在报刊露面。 一次一位英国女士求见，钱先生执意谢绝。 他回电话说：“假如你吃了个鸡蛋觉得不错，何必要认识那个下蛋的母鸡呢？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('446', '一只老鼠总是愁眉苦脸，因为它非常怕猫。　　天神同情它的遭遇，便施法把他变成一只猫。　　老鼠变成猫后又害怕狗。　　天神就把它变成狗，但它又开始怕老虎，　　天神就让他做老虎，它又担心会遇上猎人。　　最后，天神只好把它又变回老鼠，天神说：　　不论我怎么做都帮不了你，因为你拥有的只是老鼠胆。。');
INSERT INTO `tpshop_joke` VALUES ('447', '我：傻傻的人其实都蛮可爱的。　　朋友：我就很可爱。　　我：你是傻，一点都不可爱。　　朋友：切，我走了。。　　我：别走。。。　　朋友：哼哼，觉得我可爱了是吧。　　我：用滚快一点。　　朋友：你妹！');
INSERT INTO `tpshop_joke` VALUES ('448', '半夜饿了，叫外卖，　　拨过去人家打烊了，但还是答应给我送。　　我很感动，决定照顾他生意，就多点了几样小菜，　　没想到电话那边大叔冷淡地说：“你可真能吃。”　　米粉送来了，他说以后早点打电话，我心虚地问：　　“是不是准备起来很麻烦，对不起。”　　他说：“那倒没有，太晚吃饭对身体不好。”　　感动得我呀……　　然后他接着说：“何况你还吃那么多。”');
INSERT INTO `tpshop_joke` VALUES ('449', '老婆：老公，用一样东西形容我，你会选择什么？　　老公：烟。　　老婆：是因为我能让你上瘾吗？　　老公：你和烟都花了我很多钱。');
INSERT INTO `tpshop_joke` VALUES ('450', '老婆：老公，用一样东西形容我，你会选择什么？　　老公：烟。　　老婆：是因为我能让你上瘾吗？　　老公：你和烟都花了我很多钱。');
INSERT INTO `tpshop_joke` VALUES ('451', '‍‍‍‍媳妇晚上看电视总是指使我干这个干那个。 一会儿让削苹果，一会儿让倒水，我装作没听见，就是不动地方。 媳妇问我：“跟你说话没听见啊？聋啊？” 我说：“我不是声控的？” 媳妇上来给了我一个大嘴巴子，说：“不是声控的是吧，老娘让你变成触屏的。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('452', '‍‍‍‍儿子：“爸爸，你工作一天挣多少钱啊？” 爸爸：“差不多二百多吧！” 儿子：“给，这是我的压岁钱！一共三百块！你可以陪我一天吗？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('453', '常有人说“女人的黄金年龄很短，只有22—26岁，男人就不一样，到了30,40岁照样不着急”。其实男人的黄金年龄更短，只有16—18岁，在这段时期的他们，长得帅会有人喜欢，打球厉害会有人喜欢，学习好会有人喜欢，玩乐器会有人喜欢，但到了30岁以后只要他没钱，就很少有人喜欢了！');
INSERT INTO `tpshop_joke` VALUES ('454', '把十个男人和一个女人放荒岛上，三个月后，见男人们做了一顶轿子抬着那个女人在玩耍，女人娇媚动人、面若桃花！再把十个女人和一个男人放荒岛上，三个月后，见女人们围着一棵椰子树，有往上丢石头的，有拿果子逗的，那个男人瘦得像猴子，抱住树死也不肯下来！');
INSERT INTO `tpshop_joke` VALUES ('455', '‍‍‍‍这天在街上，碰到一男孩一女孩，四五岁左右，在玩游戏。 谁赢了就可以弹对方脑门一下。 女孩赢了，男孩很干脆的让女孩弹了一下。 过后男孩赢了，女孩紧张的闭上了眼睛。 男孩却收起了曲起的手指，在女孩的额头上亲了一下。 我想我知道为什么我会没有女朋友了，不说了，哭会去。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('456', '一天，哥们开着新买的车，后面突然窜出一个开大奔的：嘿，哥们，会开大奔吗？没理他，一会儿，大奔越来越快，主人又路过：嗨，哥们，会开大奔吗？哥们怒了，你他娘的不就车比我贵点吗？这么得瑟？又一会儿，大奔已不知道开多快了：嘿，哥们，会开大奔吗？然后便撞在电线杆上了。哥们立即说道：“让你他娘的得瑟，撞了吧？大奔主人又说了一句：哥们，会开大奔吗？我不知道刹车在哪儿啊');
INSERT INTO `tpshop_joke` VALUES ('457', '‍‍‍‍‍‍今天坐公交车，对面做一妹子，刚坐下就从包包里掏出一个烤地瓜吃了起来。 很快，地瓜吃完了，这货又从包包掏出了一杯热奶茶。 这都不是重点，重点是吃完喝完，她特么又从包包里掏出一只小狗，活的小狗。 当时我就想说：“妹咂，能把包包借哥用用吗？”‍‍‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('458', '‍‍‍‍一对情侣去吃牛肉面，面上了。 上面有两块大肉片，然后女的就加了一块到自己碗里。 正常男的都会对女朋友说：“你想吃都给你。” 结果那男的边把肉往碗里戳边说：“快，快躲进去，她马上又要来了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('459', '‍‍‍‍男：“你晚上睡觉的姿势是？” 女：“大字形，你呢？” 男：“我木字形。” 女：“滚。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('460', '‍‍‍‍第一次去女朋友家，她五岁的弟弟说我：“你长得丑，好丑。” 等我回家后，小家伙用我女朋友电话给我打说：“你好帅，刚才是逗你玩。” 我去，带着哭腔赞美我！你姐到底对你做了什么！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('461', '老师来家访，看见客厅的鱼缸里养了几只透明的虾，老师戴着眼镜看了半天，问我养的是什么  我说：\"虾啊！\"  老师一愣，转身就走出了我家  我也一愣，赶紧大声解释道：\"虾啊老师，老师虾啊，老师真是虾啊，真虾啊！！！\"');
INSERT INTO `tpshop_joke` VALUES ('462', '【太可怕了，最新骗局！】遇到身材好的美女搭讪一定小心，她会说自己父亲是高官经商，待你上钩之后，女子会过户一套别墅和一辆豪车到你名下，再给你一大笔钱，然后要求和你过夜，你逐渐陷入骗局，最后娶了女子，你就得和她过日子！然后，你要啥有啥不用工作，整天玩物丧志最终成为一个社会的废人。');
INSERT INTO `tpshop_joke` VALUES ('463', '男生舔了女朋友的手，导致女朋友的闺蜜意外怀孕，孩子的爹是男生的舍友。');
INSERT INTO `tpshop_joke` VALUES ('464', '早上一觉醒来,习惯性的，挠挠JJ，突然感觉不对，草！我JJ呢？！床上只有一滩血？！是不是我昨晚打飞机打多了然后断了！我那个伤心啊然后我妈来了句，傻丫头起床了.....');
INSERT INTO `tpshop_joke` VALUES ('465', '叫你偷看本主公洗澡，叫你偷看本主公洗澡，叫你偷看本主公洗澡......备注：张飞偷看刘备洗澡，罚扫厕所一个月，凡是把“主公”看成“公主”的都必须赞一个！');
INSERT INTO `tpshop_joke` VALUES ('466', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 有个财主对仆人很吝啬。    　　一天，仆人听到秋蝉叫，便故意问财主：“这个叫的是什么呀？”    　　财主：“秋蝉。”    　　仆人:“它吃什么呀？”    　　财主：“它吃风喝露水。”    　　仆人:““它穿衣服吗？”    　　财主：“不穿。”    　　仆人说：“它跟你在一起最好了。”');
INSERT INTO `tpshop_joke` VALUES ('467', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 有一吝啬的人请朋友吃饭，仆人请示他是否出酒，但不好意思直说就说：门外山上山，酉时在水边。    　　他说：一撇一捺，夕上夕。意识是人多，不出酒。    　　一朋友听懂了他们的暗语，就说：文下口，墙无土。田上玄，牛多一。');
INSERT INTO `tpshop_joke` VALUES ('468', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 晚上跟朋友逛街，看见一个人烧纸，    　　于是我问说：今天不是愚人节吗，你烧纸干嘛。    　　他说：我逗鬼玩    　　我想说：大哥你真有胆量');
INSERT INTO `tpshop_joke` VALUES ('469', '一个富家之子去考试，父亲事先考了他一下，成绩很好，满以为一定能录取了，不料榜上竟没有儿子的名字。父亲赶去找县官评理。县官调来卷查看，只见上面淡淡  一层灰雾，却看不到有什么字。父亲一回家便责骂道：“你的考卷怎么写得叫人看也看不清？”儿子哭道：“考场上没人替我磨墨，我只得用笔在砚上蘸着水写  呀。”');
INSERT INTO `tpshop_joke` VALUES ('470', '有个人向县衙告状道：“小人明天丢失锄头一把，请老爷查究。”县官问：“你这奴才！明天丢失锄头，为啥昨天不来报案？”在旁边的小吏听到后，不觉失笑。县官马上断案道：“偷锄头的一定是你！你到底偷去干什么？”小吏答道：“我要锄除那糊涂虫。”');
INSERT INTO `tpshop_joke` VALUES ('471', '智空大师面对群敌厉声呵斥：“区区魔教竟然敢挑战少林，你以为我是吃素的？” 然后就被方丈逐出了寺门。');
INSERT INTO `tpshop_joke` VALUES ('472', '上午去医院看病，我对医生说我头疼欲裂、喘不上气、心脏偶有撕裂般的疼痛。医生给我做完了检查，说：“奇怪，你身体这些部位都很正常啊。” 我：“哈哈哈哈，我是骗你的，笨蛋！” 医生微微一笑：“我也是。”');
INSERT INTO `tpshop_joke` VALUES ('473', '　　那天食堂电器坏了，正盛饭呢，工作的大哥大妈全没了，都去帮忙了。还差2个人就到我了，可等了五分钟还没人，我生气的从侧门进到打菜的地方，大叫道：盛饭的呢？！这时人群里争先恐后的说道：我盛饭，大哥快点盛~  我顿时呆住了：我...  同学甲：（凶凶的）叫你盛你就盛。这人是学校里一出名混混，我立马帮他盛了，结果。。。我就在那被逼盛了一中午饭，解释的时间都不给。');
INSERT INTO `tpshop_joke` VALUES ('474', '中国人太累的原因、1、贫富不均，有仇富心理。2、一生只做三件事：养孩子，供房子，存钱养老，忽略自己。3、工作时间长压力大。4、互相设防，缺乏信任感。5、治安差，没安全感。6、个人权利和财产没保障。7、对未来充满忧虑。8、应酬多，好面子。9、虚情假意，违心做事。我突然发现我占好几样，尴尬！我也是中国人');
INSERT INTO `tpshop_joke` VALUES ('475', '　　说个悲伤的故事。我跟女友分手了，没错！我甩的她！想听原因么。前天基友跟我耍牌，玩到很晚，吃完夜宵就到我家睡，半夜起来放水发现基友跟女友在卫生间搞在一起，我一气之下拿起针......现在她躺在垃圾桶里！');
INSERT INTO `tpshop_joke` VALUES ('476', '我一表弟在北京工作，一天开车出去办事。 路上遇到一交警拦下他的车，看了下他的车牌对他说：你不知道今天你的车牌位数是限号禁行吗？ 表弟这爆脾气当时就冲着交警吼到：我TMD是昨天开出来的。一直在路上堵到现在！');
INSERT INTO `tpshop_joke` VALUES ('477', '“老板，黄瓜怎么卖？” “三块五。” “帮我挑粗点的，光滑点的，长点的，不要有刺的……？” “五块一斤！” “不是说好三块五吗？” “人家是买菜，你买老公怎么能一个价……！”');
INSERT INTO `tpshop_joke` VALUES ('478', '终于拿到驾照了，临走教练问：知道以后上路最重要的是什么不？答：注意看路上的标志，教练说：回去上网把兰博基尼，法拉利，宾利...等这些车标志记熟悉了比记那些重要！我。。。');
INSERT INTO `tpshop_joke` VALUES ('479', '　　老张在电梯里注视一长发美女，目不转睛，他媳妇非常不爽，突然那美女转过身来，给了老张一耳光说：你个老色狼！当两口子走出电梯，老张委屈的对媳妇说：我没摸她啊！媳妇说：我知道，不过是我摸的！');
INSERT INTO `tpshop_joke` VALUES ('480', '转:某日，两男一女三个小朋友院内玩耍，小女孩提议我们玩猪八戒背媳妇吧。两男大赞。小女孩说“我说预备开始咱们就开始”然后小女孩说开始。。。。然后一个小男孩背起身边另一个小伙伴疯狗一样消失在黄昏的尽头。。');
INSERT INTO `tpshop_joke` VALUES ('481', '找个大妈擦鞋，大妈说：素的2块、荤的5块。我好奇就叫的5块的。然后大妈慢慢接开领口的几个扣子，低下头开始擦了。。。我。。。');
INSERT INTO `tpshop_joke` VALUES ('482', '在医院上班，今天去给病人量体温，有个人40度，吓了我一跳，然后我问她有没有不舒服，然后她说“体温表很冷，我把它放在热水袋上弄热来再量的。”好吧，你说你不是弄手，你弄体温表干嘛呀。真奇葩，这样也想的出来。');
INSERT INTO `tpshop_joke` VALUES ('483', '下午上课又睡着了，老师叫醒我手指着我生气得说不出话来：“你....你说你....”我抬起头诧异得接道：“想....想要逃，偏偏注定要落脚？！”');
INSERT INTO `tpshop_joke` VALUES ('484', 'lz农村滴，娶媳妇嫁姑娘都要钱，听lz妈妈说老家有一家娶媳妇，结婚那天，新郎去新娘家接亲，新娘子非要3000块钱，不给就不上车，闹了好久，新郎家里面才送了3000块钱去，然后才结得了婚的。惯例这不是高潮，第二天回门，到了新娘家门口了，新郎官爬上了一棵树上，说给3000块钱，不给不下来，新娘家妥协拿来了钱。第三天离婚了。。。');
INSERT INTO `tpshop_joke` VALUES ('485', '有一同事失恋，办公室里面各种想不开，哭着喊着说TMD给别人养了三年的媳妇。这时办公室一老同志悠悠的开口说：三年算什么，以后你可能还要给别人养个十几二十年的媳妇，你还不能饿着她，冻着她伤着她，更不舍得伤着她！');
INSERT INTO `tpshop_joke` VALUES ('486', '从前有只羊，一天得干10个小时的活。一天，主人告诉它，多干活有奖励，于是它照做了。接下来，主人每月把它身上的羊毛剪了三分之一 ，年底到了，给它织了件毛衣，然后告诉它：诺，这是你的奖励，恭喜你，明年继续努力吧！羊很生气，把它的故事写成童话，起了个名字叫：“绩效工资”。');
INSERT INTO `tpshop_joke` VALUES ('487', '本人东北一女子，也是一个穷屌丝，狠下心来也只买了一个高防貂大衣，半个月工资啊。 刚才回家的路上遇到一个抢劫的，见面就说：衣服脱了，只劫财。 没办法只能忍心把衣服给他，颤抖的说：大哥，今天零下26度，总不能让我只穿毛衣回家吧。 抢劫的想想，把他的破棉大衣给了我。 到家却发现破大衣里兜里竟然有两万块钱。 明天去佟二堡买真的了......');
INSERT INTO `tpshop_joke` VALUES ('488', '单位有个同事，刚结婚时，别人和她说，男方家的婆婆强势，她说不怕。结婚后，万分精力讨好婆婆，各种积极，带婆婆各种游玩，各种集体户外活动，各种手机社交，各种中年联谊。1年后，婆婆出轨，和公公离婚出门……');
INSERT INTO `tpshop_joke` VALUES ('489', '是在公交车上碰到的，不能用感动来形容。 主人公是一个小学生和一位年长的老爷爷。 小男孩穿着校服和同学嘻嘻哈哈坐在最后一排打打闹闹，看起来年纪很小。 在公交车上总会有一些喜欢涂鸦“做记号”的人。 只见一个小男孩拿着记号笔正准备往车顶上写字的时候， 坐在他旁边的老爷爷把他手温柔的制止住说：“小朋友，在公共场合乱涂乱画是不文明的。” 小男孩懵懂的看着。老爷爷继续说：“你们是祖国的花朵是未来的希望，将来要做一个有用的人讲文明的人”。 小男孩听了觉得有道理于是收起笔点了点头。 我以为这只是一段教育，后来老爷爷的举动差点让我泪奔。 老爷爷从他略显破烂和老旧的袋子里颤抖地拿出一个杯子。 爷爷把不锈钢的杯子递给小男孩说：“喜欢画画就在这上面画吧，爷爷喜欢爱画画的人”。 \r\n他的声音太温柔都要把我融化了。 \r\n于是小男孩接过杯子说了声谢谢。我注视着男孩，只见他安静的在杯子上涂涂写写。 \r\n我站在旁边，瞬时间觉得阳光都明亮了起来。');
INSERT INTO `tpshop_joke` VALUES ('490', '不知道你们一个个的都黑陈妍希干啥，摸着良心想一下，假如陈妍希给你当老婆 你要不要。。');
INSERT INTO `tpshop_joke` VALUES ('491', '征集一句充满正能量的口号鼓励准备高考的学生。网友：高三的小情侣们，距失恋还有33天。');
INSERT INTO `tpshop_joke` VALUES ('492', '原来上学时住校，有一次在宿舍和一男同学搞“暧昧”，其实就是开玩笑，我深情脉脉的对那位同学讲：我不在乎别人怎么说，我只要你。\r\n这时老班忽然出现在我的身后，幽幽的看了我一眼，我脸刷得红了。其实是担心老师误会。结果gc来了，第二天老师就给我换位了，居然让我和班花坐一起了！');
INSERT INTO `tpshop_joke` VALUES ('493', '我一哥们让我陪他去兑彩票，说是中了20万元，路过一算命老头的摊位，哥们就让老头给他算一算，老头说我哥们是花子命（哥们穿的有点邋遢），哥们不服的掏出彩票，我一细看，尼玛都过期了！算命老头说的对，你就是一花子命');
INSERT INTO `tpshop_joke` VALUES ('494', '今天终于鼓足勇气约在网上聊了很久的女孩见面，说好了时间和地点，来到见面地点看见一个很漂亮的女孩，心想要是约会对象是她该多好。\r\n我掏出手机拨打女孩电话，这时那个女孩接了个电话，回头看了我一眼撒腿就跑，我电话那边听那个女孩急促的说了句信号不好听不到然后就挂了，尼玛到现在我都没有弄明白到底是什么情况。');
INSERT INTO `tpshop_joke` VALUES ('495', '今天偶遇大学里最好的哥们，好久不见了，想嘲讽他一下。\r\n我：嗨，哥们，才七八年不见，头发都花白啦！\r\n他：哎，没办法，岁月催人老啊！不能和你比，你真的是一点没变啊，还是当年那未老先衰的样子！');
INSERT INTO `tpshop_joke` VALUES ('496', '你饶了我吧真事。我一同学。让一老头儿算他和她对象怎么样，那老头说:你和你女朋友是最好的，生日属性都是最好的搭配，然后我那二货同学说我昨天和俺对象分手了。\r\n那老头儿愣了一秒，说:最好的也不一定能在一起。\r\n我同学又说:刚刚和好了。\r\n老头儿说:我错了，你饶了我吧！');
INSERT INTO `tpshop_joke` VALUES ('497', '我经常被老婆打我经常被老婆打，有时一打就是连续好几个小时，场面简直惨绝人寰。\r\n有天，我已被连续毒打了近三个小时，我他妈实在是忍不住了，也不想在忍了，就举手问她能不能让我先上个厕所再打，尿真的忍不住了。');
INSERT INTO `tpshop_joke` VALUES ('498', '有一次我妈在做菜，突然和我为了点小事吵起来，我顶了句狠的，她啪一下就抓起个鸡蛋砸我头上，嚷道：“给我滚，不想见到你了！”\r\n我也气爆了，转身就往外走。还没到门口她又说：“回来，擦干净再走，太影响咱家的形象！”\r\n我这才发现蛋壳还沾在头上呢');
INSERT INTO `tpshop_joke` VALUES ('499', '我们小时候老被父母骗自己是垃圾桶里捡回来的，一般人笑笑就算了。我一堂弟，一直被爸妈说是捡来的，问了奶奶爷爷都说是捡来了，大家都觉得逗他很好笑。直到某一天，全家吃饭突然他就跪下了，哭着谢谢爸妈让他过上不用乞讨的好日子。从此以后，家里再没有拿这事开玩笑了。');
INSERT INTO `tpshop_joke` VALUES ('500', '刚才一朋友给我讲了一个他初中时被不良少年勒索二十元钱，他交出五十元，不良少年找给他三十元的动人故事......');
INSERT INTO `tpshop_joke` VALUES ('501', '和男同桌争论高数题，我太女汉子了嗓门儿大，他弱弱地说：你凶什么凶。 我咣一拍桌子吼：我没凶！ 顺着他的目光：靠！没胸，颤抖的难道是布料么？');
INSERT INTO `tpshop_joke` VALUES ('502', '女儿晚上不肯睡，被她爸爸训了一顿，流了不少泪。 上床睡觉时，她和我耳语：“妈妈，我们把爸爸卖掉，再买对面的王叔叔好不好？”');
INSERT INTO `tpshop_joke` VALUES ('503', '问：小白狼长的比灰太狼帅，又比灰太狼能抓羊，为什么红太狼选择了灰太狼而不选小白狼呢? 答：灰太狼虽然没有小白狼帅，又抓不住羊，但是他有豪华城堡，小白狼却没有房子！');
INSERT INTO `tpshop_joke` VALUES ('504', '突然想到昨晚去公司开party的时候、跟着老总去隔壁豪华包和几个老板谈几千万的黄金投资。然后自己屁颠屁颠掏出七块钱的红塔山给老总和几个老板每人发了一根。 MD！我真是太TM勇敢了。');
INSERT INTO `tpshop_joke` VALUES ('505', '朝鲜为什么敢打美国？ 神回复：因为他们钱不在美国，老婆不在美国，孩子也不在美国。 （2）美国为什么不敢打朝鲜？ 神回复：朝鲜太穷，一个导弹好几百万美元，炸哪儿都赔 （3）美国和朝鲜到底咋回事？ 神回复：青春期遇上更年期呗！一个啥事都敢干，一个啥事都要管。 。。。今天上午，金正恩将军在朝外国记者陪同下视察朝鲜航天局时宣布：10年内，要让朝鲜宇航员登陆太阳！ 一位美国记者问道，太阳温度那么高你们怎么登上去呢？ 顿时全场鸦雀无声。大家不知道该怎样回答这个问题。 这时金将军缓缓说道，我们天黑去！顿时全场朝鲜人响起雷鸣般的掌声…… 正在看电视直播的奥巴马，冷笑着对周围的同僚说：这二货，天黑了根本没太阳！ 白宫内也响起了雷鸣般的掌声');
INSERT INTO `tpshop_joke` VALUES ('506', '对于路痴来说，一条路正着走反着走是两条街，白天走晚上走是两条街，春天走冬天走是两条街，平时走周末走是两条街。 你们这些方向感好的人怎能体会到这种横看成岭侧成峰的魔幻特效。');
INSERT INTO `tpshop_joke` VALUES ('507', '问：一句话证明自己是资深光棍？ 答：别家的孩子已经不粘人了，我的孩子还是那么粘手。');
INSERT INTO `tpshop_joke` VALUES ('508', '朋友的身份证、银行卡等被偷了，后来她去报了案。 警察叔叔告诉她：“你不要抱太大的希望，我们和电视里的警察不一样。”');
INSERT INTO `tpshop_joke` VALUES ('509', '昨天和丈母娘一起吃饭，然后丈母娘指着我家两岁半的闺女说：“看你闺女怎么那么傻。” 然后我那二货老婆就急了：“你闺女才傻呢，世界上就没有比你闺女更傻的人。” 老婆啊，你让我说你什么好……');
INSERT INTO `tpshop_joke` VALUES ('510', '今天坐公交，看见地上有两块钱，我很想捡又不好意思，于是我机智的把手机扔到地上，把两块捡了起来，心里暗喜。 谁知手机屏碎了，心也碎了……');
INSERT INTO `tpshop_joke` VALUES ('511', '‍‍‍‍一朋友抱怨她老公，说：“结婚前就跟个哈巴狗一样天天粘着我，现在就是别人家养的藏獒，天天冲我嗷嗷叫。” 我回：“挺稳定的，一直都是狗！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('512', '‍‍‍‍‍‍小王：“小丽，问你个问题！男的自慰叫撸管，那女的呢？” 小丽：“叫柔道。‍‍”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('513', '今早同事的女友到公司里玩，我同事是个烟瘾，他女友一直让他戒烟。而我同事烟瘾过大耐不住跑到吸烟处抽烟，当他刚点燃香烟时，他的女友突然出现在他的面前把烟夺回扔掉，然后狂吻了他的嘴唇说：如果以后我在你身边时，你想抽烟把我带到吸烟处吻我就可以了。。。一旁的我默默地离开现场。');
INSERT INTO `tpshop_joke` VALUES ('514', '西兰花说：“我像一棵小树”； 香菇说：“我像把小伞”； 核桃说：“我像大脑”； 香蕉说：“我们换个话题吧？”');
INSERT INTO `tpshop_joke` VALUES ('515', '小时候有次被老爸揍完后躺床上，刚好牙齿换牙拿手摇摇就出血了，往地上吐了两口血水，老爸进来看见吓呆了，我想起武侠片的情节，呻吟到：我估计不行了，真后悔来这世上……老爸顿时抱着我跑去医院，一路哭……回来的时候，我被揍得真的后悔来这世上!');
INSERT INTO `tpshop_joke` VALUES ('516', 'LZ单身狗一枚，刚在小区溜达，迎面一漂亮妹子冲我高呼：土豪～土豪～并示意过去，正当我一筹莫展该如何依据剧情发展下去时，一只拉布拉多神犬英姿飒爽的终结了我的梦想。我本能的摸了摸兜里乱七八糟的几元大钞：尼玛～幸好劳资不是土豪…如果她叫你一声你敢答应吗？');
INSERT INTO `tpshop_joke` VALUES ('517', '‍‍‍‍别人的女朋友见父母前都是这么说的：“万一你父母不喜欢我怎么办？” 我的二货女友来了句：“万一你爸喜欢我怎么办？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('518', '‍‍‍‍一天小明一伙讨论最讨厌什么样的人。 小刚：“我最讨厌比我帅的人了！” 小华：“我最讨厌比成绩好的人了！” 小黑：“我最讨厌比我高的人了！” 小军：“我最讨厌比富的人了！” 小明：“你们就这么讨厌我吗？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('519', '‍‍‍‍儿子：“爸爸，我们班小明他妈妈说他是买东西送的！我呢？” 爸爸：“你也是啊。” 儿子：“胡说，我怎么可能呢？” 爸爸：“真的，儿子！你是爸爸买冒牌杜蕾斯送的。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('520', '‍‍‍‍霜霜到阿拉伯地区旅游，要过一个边防小站，看到人人都在排队交过关费，有些脸上有大胡子的人不用给钱就过去了。 问旁人怎么回事，旁人说：“有大胡子的都是警察，不用给钱。” 霜霜好像明白了什么。 到她时也不给钱直接过去，士兵拦住问霜霜：“什么意思？” 霜霜掀起裙子说：“我是秘密警察！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('521', '有没有发现和自己喜欢的人，在手机上聊天聊着聊着就会不自觉的笑了起来，我不信只有我一个人有这种感觉。');
INSERT INTO `tpshop_joke` VALUES ('522', '　　两个五六岁的孩子在水潭边上玩耍，有一个不幸落水，万幸被卤煮及时救起，孩子没有大碍，送孩子回家，孩子竟说是卤煮把他推下水。。。 　　哎。。。是小孩变坏了？还是坏人变老后投胎转世了？');
INSERT INTO `tpshop_joke` VALUES ('523', '话说。很久很久以前，盘古开辟了天地，他的头顶着天，脚踩着地，最后他挂了。他的毛发变成了森林，他的血液变成了河流，他的肌肉变成了大地。多么悲惨的结局啊 等等，那珠穆朗玛峰是怎么回事？难道说？你懂得！');
INSERT INTO `tpshop_joke` VALUES ('524', '李世民惹了武媚娘，江山被夺走了；雍正惹了甄嬛，被她用绿帽子给压没气了；咸丰惹了慈禧，清朝灭亡了，所以要好好尊重你身边的女人，不要轻易和她得瑟。切记:天干物燥，小心她闹！哄哄是良药！????');
INSERT INTO `tpshop_joke` VALUES ('525', '男人跟女人最大的不同你们知道是什么吗？ 老公被老婆捉奸在床！ 老婆，你原谅我吧！ 老婆被老公捉奸在床！ 老公相信我，我们真的什么也没做！！！');
INSERT INTO `tpshop_joke` VALUES ('526', '　　老头子给儿子买房子，去现场办理分期付款手续登记。银行业务员说：先生，您是季付，还是月付。老头一听火了，说：我他妈的不是继父，也不是岳父，我是。。。父亲！ 于是业务员就在申请表格上打勾。。。一次性付清！有钱就是任性');
INSERT INTO `tpshop_joke` VALUES ('527', '今天出去看见卖甘蔗，问多少钱一斤，大爷说1.5元，我说太贵，然后给大爷讲价，好不容易说到9毛一斤，忽然想起来没带钱，然后我弱弱的说了一句，大爷能不能送我一根。大爷看着我说，你神经病？哎。丢死人了。');
INSERT INTO `tpshop_joke` VALUES ('528', '和往常一样，我的父亲喝了点酒，一场家庭暴力即将发生。我忍不住问他：“爸！你为什么要喝这么多酒？！” 父亲重重的吸了一口烟，忧郁地说：“麻醉了全身，挨你妈打时不会那么疼。”');
INSERT INTO `tpshop_joke` VALUES ('529', '　　老婆：“亲爱滴，你最佩服滴人是谁？” 　　老公：“俺最佩服滴人唐三藏！” 　　老婆：“为嘛泥？” 　　老公：“他整天啥也不干只管旅游还老有漂亮妹纸争着抢着要吃！” 　　老婆：“难道你想被吃？” 　　老公：“有不想被妹纸‘吃’滴男同胞木有？” 　　（认为木有滴请点赞哈哈！）');
INSERT INTO `tpshop_joke` VALUES ('530', '皮肤一一皮下组织一一脂肪一一肌肉一一前鞘一一壁层腹膜一一脏层腹膜一一子宫。这些都是剖宫产取出婴儿后要缝合的组织，想当年麻醉都快过了，医生还缝合，我能清楚的感觉针穿过去但不痛，可是拉线时就痛心痛肺的。还不能叫，更不能说话。好几次痛得受不了问医生还有多久。都换来责备声。缝合花了1小时45分，这是老公说的，他一直在门口不安的等着。男同胞们，女人生孩子不易，剖腹产更不易，且行且珍惜！');
INSERT INTO `tpshop_joke` VALUES ('531', '老师：“小明，你说一下李时珍的著作是什么”。小明：“不知道，但是我知道他的临死前的遗言”。老师：“你怎么知道？是什么？”小明：“这草……有……毒！”老师：“滚出去！”');
INSERT INTO `tpshop_joke` VALUES ('532', '提醒你穿衣的人，让你吃胖的人，给你端开水的人，给你做饭的的人，你哭时在摸着你的头的人，做你闹钟的人，你埋怨他都对你笑的人，你累时给你鼓励的人，老是一直看你的人，喜欢听你唱歌的人，发信息秒回的人，盼着你平安回来的人，都是你应该去珍惜的人。');
INSERT INTO `tpshop_joke` VALUES ('533', '一富婆拿着100块跟乞丐说：叫我狗狗一声爹，钱就给你！乞丐冲着狗狗叫了声爹。接过富婆的100块说了声：谢谢妈！');
INSERT INTO `tpshop_joke` VALUES ('534', '有个兄弟叫王伟，以前叫他伟哥他就生气，现在岁数大了叫他老王，他哭了说，还是叫伟哥吧，邻居防我跟tm防贼差不多。。。');
INSERT INTO `tpshop_joke` VALUES ('535', '甲：“你知道那里WIFI信号最好吗？” 乙：“夕阳红。” 甲：“为何？” 乙：“夕阳无限好啊！” 丙：“……”');
INSERT INTO `tpshop_joke` VALUES ('536', '“哥们，出车祸了，借点钱。” “咋回事，肇事司机是谁？” “我老婆，购物车。”');
INSERT INTO `tpshop_joke` VALUES ('537', '小龙女看起来又矮又胖，是有原因的：“在首集中，李莫愁为了逃出古墓而不被发现，就让师妹小龙女每天到自己的房间，把她的那份饭菜也通通吃光，以此做掩护。” 每天吃六顿饭，能不胖吗？');
INSERT INTO `tpshop_joke` VALUES ('538', '感谢百思的亲们让我知道自己还会感动还有“心”！我没有朋友，不常回家看爸妈，上班一个人一个办公室下班一个人一间卧室，我以为我不快乐但也不悲伤，直到你们的帖子更新，带给我欢笑带给我感动的泪，谢谢！');
INSERT INTO `tpshop_joke` VALUES ('539', '刚在外面玩完 朋友开车回来 正开着有条狗在路中间跑 看到我们车来不及跑了就准备趴下钻车下面 结果朋友1个急刹车把狗吓个半死 嗷嗷叫 朋友来了句:妈的 这狗想碰瓷！ 瞬间我就凌乱了。。。。');
INSERT INTO `tpshop_joke` VALUES ('540', '今无聊压马路看见一个穿得很单薄长的很丑的妹子大晚上这么冷在路边卷缩着，我顿时心一疼！这个社会是怎么了啊，人丑就注定被人抛弃吗？为什么人都只看外表！不在乎别人的内心呢？ 我走过去搂住妹子轻声安慰，不说了，妹子的奥迪确实不错！');
INSERT INTO `tpshop_joke` VALUES ('541', '老婆：我也不是不讲理的人，你就不能解释几句？ 我：那好，你听我解释…… 老婆：你做的破事儿还有脸解释？ 我。。。');
INSERT INTO `tpshop_joke` VALUES ('542', '刚才在肯德基，隔壁桌埋头猛打消除游戏。他儿子缠着叫“爸爸给我玩”，“你好好学习”。“但是你都在玩”，“我是为了让你羡慕我，从而产生学习的动力”。“不懂，给我玩啊”，“认真念书，二十年后就给你玩！”……');
INSERT INTO `tpshop_joke` VALUES ('543', '　　因为最近脸上长痘痘，我妈就跟我说闺女啊，咱别老玩电脑了，人家都说电脑中病毒中病毒的，你的脸我估计就是中病毒了。。');
INSERT INTO `tpshop_joke` VALUES ('544', '深夜同时和5个美女共处一室，还是躺着，看着她们美丽的脸庞和玲珑的身段，哎…………，淡定的睡了，谁有这么好的定力。：“做个卧铺说的这么清新脱俗”');
INSERT INTO `tpshop_joke` VALUES ('545', '一朋友的女儿是双胞胎，有天姐妹俩兴奋地告诉爸爸：“老爸，今天我们全班同学要选一位最帅的爸爸，结果你当选了。” 爸爸很高兴，问怎么会当选的。 双胞胎姐妹说：“同学们都投自己老爸的票，我们有两票，所以你当选了！”');
INSERT INTO `tpshop_joke` VALUES ('546', '　　我：你知道诸葛亮咋死的吗？ 　　同桌：俺读书少，不知道。大哥告诉我呗？ 　　我：他啊 为了收拾刘禅捅的烂篓子，日理万机，操劳过度而死。 　　同桌：这么惨？ 理万机和劳过度是谁啊？还有那烂篓子是猪哥谁啊？ 　　去死吧你,读的都是什么书？');
INSERT INTO `tpshop_joke` VALUES ('547', '一小伙在火车站上班，一次有个老太太来问路：“小伙子，上海南的车往哪边走？”小伙看老太太行动不太方便，于是搀扶着她从贵宾室里走，并一直把老人送上了去海南的火车。第二天老人的儿女在上海南站等了一天也没接着人…');
INSERT INTO `tpshop_joke` VALUES ('548', '　　导师对实习生说：你们就快要做正式的儿科医生了，小孩子最怕打针了。所以考验我们儿科医生的实力到底好不好，其实就在于他给小孩子打针的时候，孩子哭不哭，闹不闹。如果孩子哭了闹了，那不叫针，如果孩子不哭不闹，孩子很平淡，那才叫针。 毕竟，平平淡淡才是真嘛');
INSERT INTO `tpshop_joke` VALUES ('549', '跟老婆开着车，突然从旁边冲过来一辆保时捷跑车，那妹子太漂亮了，就对老婆说，老婆，看到车上的美女了吗，挺漂亮，老婆说漂不漂亮我不知道，我只知道她258一次，我问为什么，她说让我追上去看看车牌，我一看“浙B2581C”');
INSERT INTO `tpshop_joke` VALUES ('550', '小学的时候，以为男孩子和女孩子牵牵手就能怀孕，上了中学意识到自己懂的太少，可现在，我怎么又觉得自己中学时好傻好天真。');
INSERT INTO `tpshop_joke` VALUES ('551', '冬哥陪太太逛街… 左边走来一位长发披肩、挺亭亭玉立、极上镜的MM，情不自禁地行注目礼，扭过头去看得眼发直；太太不悦：“怎么了？你啊？！”冬哥忙解释：“脖子睡觉时扭着了。”一会，右边又来一位染金色短发、露脐短上装、超短裙的豹妹，冬哥又扭头向右边，太太不满：“又干什么？”冬哥：“正一正脖子也不行吗?”');
INSERT INTO `tpshop_joke` VALUES ('552', '一天小王喝醉了，坐出租车回家，在车上小王是哇哇大哭啊：“师傅，你说，呜呜，男人为什么一喝多就管不住自己下半身呢？能理解我吗师傅！” 司机师傅安慰道：“我说老弟呀，男人嘛，生理需要很正常的，没事的哥理解你！” 小王：“大哥，大哥你这人老好了，大哥，大哥我真没控制在啊！我尿你车上了！”');
INSERT INTO `tpshop_joke` VALUES ('553', '　　a：如何破坏友情？ 　　 　　b：借钱。 　　 　　a：再狠一点？ 　　 　　b：不还。 　　 　　a：再狠一点？ 　　 　　b：再借。 　　 　　a：再狠一点？ 　　 　　b：把钱还给他老婆。');
INSERT INTO `tpshop_joke` VALUES ('554', '‍‍外卖小哥送饭到洗脚城。 外卖小哥：“美女，一共30块。” 美女：“要不你洗个脚吧，就当饭钱了。” 外卖小哥：“不行，我怕熏死你，还要贴医药钱。” 美女：“……”‍‍');
INSERT INTO `tpshop_joke` VALUES ('555', '‍‍儿子10岁了，女儿6岁。 有天，兄妹俩吵架，然后发展成打架。 女儿打不过，就躲在我身后。 我刚准备劝儿子要让着妹妹一点，结果儿子说：“爸，咱们10年的交情了！你居然护着跟你认识6年的她！” 我也是醉了……‍‍');
INSERT INTO `tpshop_joke` VALUES ('556', '‍‍‍‍一美女去买电动小黄瓜，对话如下。 美女：“老板我要个震动棒。” 老板：“你自己挑吧！” 美女：“我就要那个红的。” 老板：“妹子你在挑个吧！那个不卖。” 美女：“不行，我就要那个红的。” 老板：“妹子那个灭火器真的不能卖给你啊！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('557', '跟老婆看电影，里面妹子各种波涛汹涌，我就感叹说个个都是凶器啊。。。老婆不乐意了，说那我是什么？我嘴巴太快，没管住嘴就直接说了句：你那是暗器。。。暗器。。。');
INSERT INTO `tpshop_joke` VALUES ('558', '‍‍由于老婆工作比较忙，所以大都是我做饭，有的时候做多了还会自己带点拿公司去。 不止一次同事说：“哟，爱心便当哦！哟，爱心早餐哦！哟，你媳妇真贴心哦！” 自己做饭怎么了，默默的吃完，泪奔啊！‍‍');
INSERT INTO `tpshop_joke` VALUES ('559', '大学里面校运会一般很少人去的，可是我有个室友很擅长长跑我们便去给他打气。。。。15000米跑完一大半了丢了第二名快一圈的时候 本来稳稳的速度居然开始减速然后直接倒在地上，后来回去之后我问他怎么了 平时不是这样的。这货来句 内裤太紧了 磨射 了。。射 了。。。了。。');
INSERT INTO `tpshop_joke` VALUES ('560', '近日网络上出现了一个女人砸车打自己出轨老公的视频，旁边很多人围观，女人的手都砸的鲜血淋漓，围观的人那么多，就没有一个能、、、给她递块砖头的、、、');
INSERT INTO `tpshop_joke` VALUES ('561', '吃火锅时一直在帮大家涮的人，烤肉时一直在帮大家烤的人，唱K时一直在帮大家点歌的人，旅行时一直在帮大家拍照的人…是天使，要珍惜。');
INSERT INTO `tpshop_joke` VALUES ('562', '刚刚给四年级的弟弟写语文暑假作业，有一个阅读理解，大概说墙缝隙里掉了一粒瓜子，几天之后出了瓜苗，在没有阳光没有泥土的墙缝中冲破外壳不屈向上，茁壮成长，虽然他只活了几天。。底下有个问题说用我们学过的一首家喻户晓的诗赞扬小草顽强生命力，，想了很久很久我提笔写了一句“一枝红杏出墙来”。。。我写的对吗');
INSERT INTO `tpshop_joke` VALUES ('563', '小时候，和弟弟姐姐一起睡觉，家里穷，大夏天的，停电了。姐姐让我和弟弟帮她扇扇子100下，她再帮我们扇100下，喊我们大点力扇，等下她也大点力帮我们扇。结果100下扇完了，她睡着了，擦。。拿脚踹，打，各种就是不醒。');
INSERT INTO `tpshop_joke` VALUES ('564', '看见个招工广告说是不累待遇好，打电话一问水果销售月薪两千五有提成，满一年五险一金，果断奔着五险一金而去。刚满一年我就不干了，朋友问我怎么了。别提了我找他要五险他说当时说的无险。我又要那一金，他给我发了一斤香蕉。文字陷阱擦的～');
INSERT INTO `tpshop_joke` VALUES ('565', '几个女同事聊天，有人说自己老公的哥哥是处长，另一个说自己老公的舅舅是局长，还有人说自己老公的哥哥是厅长_……最后一个女的不屑的说了一句：“我老公的弟弟是麦克风”，瞬间那几个女的都流露出了羡慕的眼神。');
INSERT INTO `tpshop_joke` VALUES ('566', '理发的时候，理发小哥不是会各种推荐各种求办卡么，一句话秒杀他“那个，先剪吧，看效果好就办卡。”……保证小哥会全程全神贯注，甚至拿出蓝翔毕业考核的水平来，完事后办不办卡，主动权就在你手上。不要谢我，我姓雷');
INSERT INTO `tpshop_joke` VALUES ('567', '一直想经商挣点钱，做了好久调查发现只要挣钱的都有人干，突然发现在三亚没有卖羽绒服的，我把所有家当投了进去开了一家波司登专卖店，已经三个月了，现在吃馒头的钱都没了，求大神教怎么才能挣钱。。');
INSERT INTO `tpshop_joke` VALUES ('568', '和男友吃火锅，鸳鸯火锅，他说，咱俩就像这鸳鸯锅两种口味一样，我说，是我像这麻辣的这样火辣而你像清锅这样平淡吗？男友说，不，我像麻辣的，而你像麻辣隔壁的。。。');
INSERT INTO `tpshop_joke` VALUES ('569', '和哥们喝酒，喝高了，我说不喝了，他说再喝点吧，我说:“今天嫂子不在家吗？”他说:“就是在家才多喝点啊！”我说:“是不是多喝点床上有激情啊？”他说:“我就是想喝多了躲过那一劫……”');
INSERT INTO `tpshop_joke` VALUES ('570', '“你觉得这世界上可以触摸的痛苦是什么？” “每当觉得肚子快饿扁了的时候，一摸发现还是有一大堆肉！！！”');
INSERT INTO `tpshop_joke` VALUES ('571', '一个人突然联系你了，正常，他在找备胎。突然不联系你了，也正常.你只是个备胎。有一天又联系你了，更正常.你是一个好备胎。接着又不联系你了，依然正常。比你好的备胎出现了！！');
INSERT INTO `tpshop_joke` VALUES ('572', '我的班主任姓侯，这天上课时，侯老师放了个屁，我们一个个憋住不笑，这时，侯老师电话响了，铃声很震撼，“猴哥猴哥，你真了不得……”诶，当天的作业翻了一倍啊！');
INSERT INTO `tpshop_joke` VALUES ('573', '‍‍丈夫：“我想养狗。” 妻子：“住哪儿啊？” 丈夫：“我那间屋呗！” 妻子：“不适合。” 丈夫：“咋不适合了？” 妻子：“狗能睡猪窝吗？” 丈夫：“……”‍‍');
INSERT INTO `tpshop_joke` VALUES ('574', '‍‍甲：“你老婆对你咋样啊？” 乙：“很好啊！晓得我没钱养她，就一直不出现。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('575', '　　今天表妹来我家玩，这不是重点。重点是我们看了会电视她说天太热就去洗澡。 　　然后我脑子一抽就趴通风口偷窥她了……再然后，是的，我妈回来了。 　　我永远忘不了她看我的那种眼神……不说了我得找个地方吃饭，我现在还没敢回家呢。');
INSERT INTO `tpshop_joke` VALUES ('576', '有一天老王去找大师，说啊：“大师阿我这耳朵越来越听不见了，连放屁的声音都听不见了，这可咋整啊！”大师闭着眼拿出一颗丹药说饭后服下。老王不解的问：“吃了它就能听见了？”大师曰：不是，吃了它放屁就能大点声了');
INSERT INTO `tpshop_joke` VALUES ('577', '　　夫在妻怀孕8个月时同房，儿子出生后就问：谁是爸爸？夫答：我就是。儿子巨愤怒的戳着他的头问：你说，这么戳你痛不痛？痛不痛！ 　　　　 　　续： 　　妻怀孕8个月与夫房事 女儿出生后就问:谁是我爸爸? 夫因上次的教训于是戴了顶头盔答:我是. 女儿张嘴一口吐沫喷到他脸上 怒斥:这样吐你脏不脏?脏不脏? 　　');
INSERT INTO `tpshop_joke` VALUES ('578', '‍‍一女的去拍照，摄影师问：“美女，你是要侧光，还是全光？” 同来的男友一听就火了，大声骂到：“我们是来生活照的，穿衣服的。你小子是不是不想活了，当着我面调戏我女友？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('579', '‍‍我：“怎样才能丰胸啊？” 闺蜜：“天天按摩啊！” 我：“真有用？” 闺蜜：“肯定啊！磨出一层老茧来肯定要大点啊！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('580', '我家熊孩子特皮实，今天在屋里听我妈训他:我说一二三不许说话！一！二！熊孩子马上接话:一二三四五六七八九十…………');
INSERT INTO `tpshop_joke` VALUES ('581', '街边，一对情侣在吵架。女孩对男孩说，“我们分手吧！”男孩沉默半天，开口问道。“我能再说最后一句话吗？”“说吧，婆婆妈妈的。”“我会编程……”“会编程有个屁用啊，现在到处都是会编程的人！”男孩涨红了脸，接着说道，“我会编程……我会变成……童话里，你爱的那个天使……”');
INSERT INTO `tpshop_joke` VALUES ('582', '情人节时。女：亲爱的！我喜欢大大的玩偶！七夕时。女：亲爱的！我喜欢法国香水！生日时。女：亲爱的！钻石代表永远！圣诞节时。女：亲爱的！我………男：等一下，宝贝！你有没有喜欢便宜的东西？女：有啊！我最喜欢你了！');
INSERT INTO `tpshop_joke` VALUES ('583', '男：亲爱的，您好！女：对不起，你是谁？男：您看呢？女：我看不见呀！男：那您听出是谁吧？女：我也听不出。男：再见（放下电话）啊，又聋又瞎。表姨也真是，怎么介绍一个这样的人，幸亏还未相亲。');
INSERT INTO `tpshop_joke` VALUES ('584', '男：“你是我的太阳。”女：“我有那么酷热吗？”男：“你是我的月亮。”女：“我有那么冷淡吗？”男：“那……你是我的星星。”女：“我有那么渺小吗？快滚！”');
INSERT INTO `tpshop_joke` VALUES ('585', '姑娘特别有钱。一天傍晚，一个年轻的小伙子，对她特别温存。“你那么阔。”他吻着她说。“是的”她坦率地承认“我值100万美元。”“你能嫁给我吗？”“不。”“我料到是这样。”“那你又何必问呢？”“我只不过是想体验一下，当一个人失去100万美元的时候，是个啥滋味。”');
INSERT INTO `tpshop_joke` VALUES ('586', '女：“你怎么随便把人家的照片传到网上啊。”男：“这有什么啊，有很多人夸你呢。”女：“夸我什么？”男：“说你是西施。”女：“真的？”男：“你自己去看嘛，他们都说情人眼里出西施。”');
INSERT INTO `tpshop_joke` VALUES ('587', '我和女友周未到一个寺院游玩。处在热恋中的女友，欣喜若狂，看到佛像就拜一番，口中念念不忘祈祷。我问她：“许什么愿？”她微笑着说：“让佛祖保估我越来越漂亮。”我坏笑道：“都长成这样了，佛祖也没有办法。”她不理我，又跑过去拜了又拜，祈祷：“既然我已长成这样了，那就让我恋人周围的漂亮女孩子越来越少吧。”');
INSERT INTO `tpshop_joke` VALUES ('588', '我最近喜欢上一外文系的女生，可是别人说她有男朋友，我该怎么办？百度知道最佳答案：追她男朋友，让她伤心欲绝，然后你就主动过去安慰她。完美的计划啊。');
INSERT INTO `tpshop_joke` VALUES ('589', '一哥们暗恋一妹子，喝酒壮胆去表白，过量。表白，妹子大惊，惊喜中答应，哥们狂喜。第二天酒醒，完全不记得昨天表白，继续暗恋妹子。。。');
INSERT INTO `tpshop_joke` VALUES ('590', '一对情侣坐地铁去世纪公园，出站之后两人因为哪个口近而争执起来。男朋友坚持1号口，女朋友则执意要走2号口，结果无奈之下男朋友只能求助于咨询台的工作人员。阿姨望了男孩子一眼，只说了一句：要去世纪公园就走1号口，要女朋友就走2号口。');
INSERT INTO `tpshop_joke` VALUES ('591', '过年，回老家，前几天到姐夫家坐坐，他和小侄儿在一起看《动物世界》，豹子家族，他对侄子说：你看小豹子2岁就离开妈妈独立生活了，那幺你呢？你今年都7岁了。没想到侄子却说：小豹子4岁就有自己的儿子了，那我呢？。。。直到现在仍忘不了姐夫当时的表情！');
INSERT INTO `tpshop_joke` VALUES ('592', '　　今天坐店里，有个卖鱼的过来问：有人在吗？我果断站起来，他看了我一眼说了句：没人在啊。。卧槽。难道我是牲口？？不是人？');
INSERT INTO `tpshop_joke` VALUES ('593', '老公是柴，老婆是米，火候适中， 米饭自然香甜可口；老公是油， 老婆是盐，不离不弃，生活自然有滋有味；老公是糖，老婆是醋，酸酸甜甜；小日子一定缠缠绵绵；老公是茶，老婆是水，温温润润，一生都要恩恩爱爱；愿天下每对夫妻，都把柴米油盐的生活，过得红红火火！');
INSERT INTO `tpshop_joke` VALUES ('594', '刚才看新闻看到山东一小伙为了给女朋友过情人节，掰亮了5000多根荧光棒在广场马路上摆了直径几十米的大型爱心，女友被感动哭，广大市民皆惊呆了。第二天男子双手瘫痪经检查因为劳累过度，所以说，秀恩爱死得快。');
INSERT INTO `tpshop_joke` VALUES ('595', '朋友一语文老师，一次改到一题，要求写出李白和孟浩然分别时的对话。一学生写到：李白说：“浩然哥，慢走，有空常来玩啊！”孟浩然说：“嗯，你一定要幸福。');
INSERT INTO `tpshop_joke` VALUES ('596', '　　午饭和公婆一起吃，闺女贪玩不吃饭， 　　楼主一生气一着急来了句：快吃饭，你奶奶的猪头肉可香咧…… 　　婆婆，你脸上不要挂辣么多黑线好么？老公，你眼珠表像粘层面粉好么？ 　　公公，你表要笑的辣么开心好么……');
INSERT INTO `tpshop_joke` VALUES ('597', '今天在街上闲逛、 看到一老大爷陪他女儿在路边买东西、摊主是位老大娘、 眼神看起来不太好…… 价格谈好后、 那女子拿出一张一百的、 伸手就想递过去、 这时大爷一皱眉、 对他女儿说道：“ 先把钱给我看看！” 那女子缓缓地把钱递给大爷、 他仔细看了看钱、 脸色逐渐阴沉！然后直接当场撕了！！又从兜里拿了一张新的递给那位卖东西的大娘。为老大爷点个赞！');
INSERT INTO `tpshop_joke` VALUES ('598', '甲：前几天我感冒了，挤了一次地铁，出了一身大汗，感冒好了！ 丙，这算什么，我每次坐地铁上下车从来不用自己走，我把两胳膊一伸，两边的人抬着我就走了。 丁：这有啥稀奇的，我自从坐地铁上早班，都不用自己买早餐了，总有人把豆浆啊，牛奶往我嘴里塞，吃完了，别人还和我道歉呢，对不起，洒你一脸！');
INSERT INTO `tpshop_joke` VALUES ('599', '　　医生走进病房，看到一个病人气色很好：“今天精神不错。” 　　病人：“是呀！是呀！自从得了这精神病，整个人精神多了。”');
INSERT INTO `tpshop_joke` VALUES ('600', '大学时候住宿舍，晚上宿舍里凑齐四个人就打牌，赌脱衣服，衣服全脱光了以后再输就揪下面毛，一张牌一根毛，有一天晚上，班主任忽然趴在我们宿舍窗户上，看到宿舍里我只穿裤衩在那哈哈大笑，另外两个光着屁股在给另一个光屁股的拔毛…… 后来请了我们家长，我爸现在一和我吵架就拿这事说我……');
INSERT INTO `tpshop_joke` VALUES ('601', '　　十八岁生日了 和父亲一起聊天 他说我这么大了 家庭的重任该交给我了 　　听了这话我感觉肩上的责任特别大 陷入了沉思 　　突然 父亲把他手中的锄头神圣的递给了我 他走后 我在风中凌乱');
INSERT INTO `tpshop_joke` VALUES ('602', '晚上寝室熄灯了，我和几个兄弟在打着灯打牌。 我总有种不详的预感“我们打着灯是不是有点嚣张阿” “没事老师这会都睡了” 这是时有人敲门，一兄弟装b大喊“报上狗名”砰！我永远都忘不了班主任黑着脸带着校长领导破门而入将那兄弟带走时的情景。');
INSERT INTO `tpshop_joke` VALUES ('603', '时代在变，钱是越来越不值钱了。哎，10年前我拿5块钱可以从超市带走5根火腿肠，麦丽素两袋，花生米一包，两支钢笔，一瓶墨水，四包方便面，一瓶洗面奶。现在不行了……都安装监控了。');
INSERT INTO `tpshop_joke` VALUES ('604', '说个爸妈的，爸爸是个脾气超级好的人。\r\n说话慢慢吞吞，妈妈相反。\r\n每次吵架妈妈就像机关枪一样，霹雳啪啦说的很快很多，嗓门又大。\r\n爸爸坐在角落，通常我妈骂十句他才回一句。\r\n刚才听他们又在因为小事吵架。\r\n我妈一顿袭击之后，我爸爸弱弱地说了一句：“你慢点，刚才没听清……”\r\n我妈直接翻白眼走人……');
INSERT INTO `tpshop_joke` VALUES ('605', '盲人打灯笼走在路上，人问：你双眼失明打灯笼何用？\r\n盲人答：怕他人看不清路。这是儒家。\r\n盲人答：怕别人撞到我。这是墨家。\r\n盲人答：不是说晚上出门就得打灯笼吗？这是法家。\r\n盲人答：想打就打何必问何用？这是道家。\r\n盲人答：你猜。这是释家。\r\n盲人答：灯笼上不是写着招牌牛肉饭么。这是吉野家。');
INSERT INTO `tpshop_joke` VALUES ('606', '一日，外星人来到地球，对一个卖菜老伯做了一个1的手势，老伯伯以为买一斤菜，摆了个2的手势。\r\n外星人摆出一把枪的手势，老伯伯以为买8斤菜，冲它竖起大拇指。\r\n外星人赶快走了。\r\n回去后，它汇报：“大王，地球人好厉害，我跟他说我打死了一个人，他跟我说他打死了两个人。我说我用枪打死的，他说他用大拇指摁死的。”');
INSERT INTO `tpshop_joke` VALUES ('607', '发一隔壁小萝莉的事情吧，小萝莉今年六岁，各种可爱\r\n一次她爹妈不知为何事吵了起来，我们闻讯赶过去劝架\r\n小萝莉呆在哭得那叫一个煽情，两口子脾气都比较大，拉都拉不住\r\n眼看场面快失控了，只听见小萝莉哭着很不耐烦地喊道:“我说让你们别结婚，你\r\n们偏要结。。。偏要结。。。”');
INSERT INTO `tpshop_joke` VALUES ('608', '小王在10楼人事部门工作，一个月前，被调到9楼行政部门去了。\r\n今天，小王同学打电话到人事部门找他：“小王在么？”\r\n接电话同事说：“小王已不在人事了。”\r\n小王同学：“啊啊！？什么时候的事啊，我怎么不知道啊，还没来得及送他呢？”\r\n“ 没关系，你可以去下面找他啊”');
INSERT INTO `tpshop_joke` VALUES ('609', '一老师对同学们说：“有没有觉得自己很蠢的同学请站起来。”大家沉默几分钟后\r\n一男生缓缓而起老师说：“怎么你觉得自己很蠢吗？”那男生答道：“不，老师，\r\n我是不忍心你一个人站着……”');
INSERT INTO `tpshop_joke` VALUES ('610', '本人到山上写生。看见路边一间小草房蛮漂亮的，在草房对面坐下开始画。\r\n没过多久，俩村民从我面前走过，看了看我画的画\r\n其中一个对另一个说：“这孩子画厕所做什么呢……”');
INSERT INTO `tpshop_joke` VALUES ('611', '女朋友马上就来了，我很是激动，想好了各种姿势，想想就激动，前天就出发了，怎么还没到，真难等，早知道就让店主发顺丰了。');
INSERT INTO `tpshop_joke` VALUES ('612', '‍‍‍‍在公交车上，看到一个哥们搂着老婆秀恩爱，羡慕了一车人。 突然一个电话打来，我清楚的看到他手机上写着老婆。 哥们温柔道：“老婆，等会儿我买菜回去，给你做好吃的。” 真是中国好男人。 等等！他搂着的是谁？‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('613', '‍‍‍‍一天，刚睡醒，就听到客厅父母在聊天。 母亲说：“谁要娶咱闺女，真同情咱姑爷，好可怜。” 父亲说：“那他肯定是上辈子造的孽，不值得同情！没什么可怜的。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('614', '‍‍闲的无聊打几年前的一个手机号。 一拨通马上唱：“在山的那边海的那边有一群蓝精灵。” 然后马上就挂了。 没想到几分钟后手机收到一条短信，一看是以前的那个手机号发过来的。 打开一看，上面写到：“它们活泼又聪明。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('615', '　　那一天骑摩托车赶路，看见前面有一个男的骑自行车载着老婆同向而行，前面横杠上还坐着一个小孩。我一看那个女的：那脸长得是真的叫黑，往前赶上一点再看那男的脸：比女的还黑。心里就想：你俩的脸那么黑，孩子还小总该白一点儿吧？待车子走并头时再一看他们的孩子：比她父母俩人都黑。天呐！你一家子都是从非洲才回来的吗？');
INSERT INTO `tpshop_joke` VALUES ('616', '问：古人是如何装逼的？ 答：有句话，不知当讲不当讲');
INSERT INTO `tpshop_joke` VALUES ('617', '‍‍前几年和老婆到郑州玩住在一个叫龙门大酒店的地方吧！ 晚上正睡的香电话响了，接住里面传来甜美声音：“需要服务吗？包你满意！” 当时脑抽！说：“等下，我和老婆商量下。” 对方立马说：“有病！” 挂了。‍‍');
INSERT INTO `tpshop_joke` VALUES ('618', '二叔家盖房子，一楼和二楼的构造一样，没有封墙，有一次我把二楼当一楼就那么直挺挺的走下去宛如天神一般降临到一群小孩子面前，从此奠定了我在村里孩子头霸主的地位。');
INSERT INTO `tpshop_joke` VALUES ('619', '‍‍‍‍刚刚和闺蜜一起逛街，没钱了。 我拿着手机说：“我现在全身上下最值钱的就是这手机了，干脆卖了买几百杯奶茶吧！” 只见闺蜜翻了个白眼，幽幽地说：“我现在全身上下最值钱的就是那层膜。” 我去，你特么的思想纯洁点啊！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('620', '老师要求同学们用“大象像什么造句”，一孩子回答:“大象像五千五百五十五个面包挤在一起”');
INSERT INTO `tpshop_joke` VALUES ('621', '在街头散步，路边一位阿姨问：“小伙子，擦皮鞋不？”我琢磨着闲着也是闲着，就擦一个吧，好歹还赚三块钱。');
INSERT INTO `tpshop_joke` VALUES ('622', '都说蹦极很刺激，今天就和女朋友一起去 蹦极。没想到女友到了高台又反悔不想跳 了。我生气了:“钱都花了，怎么能不跳呢” 。说完就一把把 她推了下去，然后我看了 看旁边指导员手里的绳子，问:“这是干什么 用的”？？？');
INSERT INTO `tpshop_joke` VALUES ('623', '‍‍老师：“谁来解释一下班师回朝？” 小满：“该是打了败仗回家吧！” 老师：“何以见得是打了败仗呢？” 小满：“都搬运尸体了还不算败吗。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('624', '‍‍一老妇找画家画像，让画家在自己脖子上画上钻石项链，耳朵上画上钻石耳环，手上画上钻石戒指。 画家很奇怪说：“这些首饰你明明是没有啊？” 老妇说：“我死后老头子一定会再娶的，到时候让他的后老婆去找去吧！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('625', '‍‍一天饭后，边看电视边和老婆聊天。 老婆问起：“我和你妈妈究竟哪个好？有啥区别？” 我不好意思地说：“区别吗，有点大了啊！” “啥区别”老婆追问。 我说：“老妈是给我生命的人，而你却是把我累病的人。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('626', '野外急救小知识：如果在外面不小心摔断了腿，打120肯定是首要的，但是在等待救援的时候，你可以就近找一些野草，就是附近生长得最茂盛的那种，用石头细细的碾碎，然后慢慢敷在肿痛的伤处，事实证明，这样做很能打发等候救护车的时间。');
INSERT INTO `tpshop_joke` VALUES ('627', '姐姐的女儿在上初二，姐姐总是给她剪很难看的发型，我问她为什么不给孩子剪个好看的头发，她说怕女儿早恋，头发难看就没人能看上她了，我问，那万一她看上别人怎么办？姐姐说她看上别人，别人看不上她也没用！这可真是亲妈啊！');
INSERT INTO `tpshop_joke` VALUES ('628', '昨晚乘电梯下楼，开始电梯我就自己一人。下到中间的时候。进来一个七八岁的小女孩，进门就问我几点了！我想吓吓他，压低声音说道：你能看见我？女孩满脸呆萌的说道：叔叔你脸那么大谁看不见啊！');
INSERT INTO `tpshop_joke` VALUES ('629', '‍‍和老婆吵架，她骂我是猪，我回骂她是野猪，并且告诉三岁的小女儿说：妈妈是野猪。 小女点头，学了一遍。 不就，幼儿园老师教英语，说：爸爸是dad。 又问：妈妈是什么？ 小女大声回答：妈妈是野猪！ 老师哭笑不得。‍‍');
INSERT INTO `tpshop_joke` VALUES ('630', '‍‍周末带着3岁儿子和老婆一起去逛动物园，看到蟒蛇圈养在铁笼子里。 就问妈妈：“为什么关起来？” 妈妈说“怕蟒蛇伤害人。” “那为什么不用铁皮而用带窟窿的铁丝呢？”儿子又问。 我说：“怕闷死它啊！” 儿子又说了：“那就不怕它减肥后逃跑吗。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('631', '爸爸教育我：“你可不要被男人给骗了，男人的话你别信。” 一时之间我竟不知道该不该听爸爸的话');
INSERT INTO `tpshop_joke` VALUES ('632', '‍‍‍‍小熊猫哭着对妈妈说：“其实我的爸爸是隔层的熊大叔对吗？” 熊猫太太急忙捂住儿子的嘴：“别胡说，让你爸爸听到就坏了。” 小熊猫说：“就是爸爸告诉我的，他说我是熊孩子！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('633', '‍‍甲：“你每月收入5位数，真不错啊。” 乙：“别提了，老婆要拿走两位数。” 甲：“很幸运啊，只拿零头。” 乙：“她拿的是前两位。”');
INSERT INTO `tpshop_joke` VALUES ('634', '　　跟女神表白：我儿子很喜欢你，想你做他妈妈。女神问，你儿子多大了？在哪呢？我说：只要你同意，我会安排你们10个月后见面。结果女神却说能不能六个月见面。');
INSERT INTO `tpshop_joke` VALUES ('635', '　　今天心情不好，追了好久的妹纸兴高采烈的告诉我她暗恋的人对她表白了。我向大师询问我应该如何面对。大师笑了笑，拿出来一张卫生纸。“大师，您是要告诉我擦干眼泪继续前行吗？” “不不不，我只是想给你举个例子，你就算再悲催，也不过追了妹纸几个月，这棵树拼命长了三十年，最后却被做成了卫生纸…”');
INSERT INTO `tpshop_joke` VALUES ('636', '‍‍‍‍两只苍蝇打乒乓球，正打着一只苍蝇倒地不起。 另一只急问：“你怎么了？” 倒地那只说：“拿错拍子了，是电蝇拍。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('637', '有没有懂跑车的朋友，求推荐一款400万到800万的跑车，要求性能好，起步快，马力大，舒适性高的，外观时尚又好看的，我拿来当手机纸壁。');
INSERT INTO `tpshop_joke` VALUES ('638', '有没有懂跑车的朋友，求推荐一款400万到800万的跑车，要求性能好，起步快，马力大，舒适性高的，外观时尚又好看的，我拿来当手机纸壁。');
INSERT INTO `tpshop_joke` VALUES ('639', '有一个女生她第一次到公共浴室里去洗澡，由于方向感太差，走错了浴室，她推门一看，里面全是男的，而且还是裸的。 她顿时尖叫，神志不清的指着那些裸男大吼道：“你们这些臭男人、色狼、流氓、混蛋，洗澡都不穿衣服的吗？”');
INSERT INTO `tpshop_joke` VALUES ('640', '现在的女人真特么会装。 昨天和几个哥们在KTV聚会，玩的很嗨，要走的时候女神告诉我说她喝多了，让我送她回家。 我这暴脾气当时就怒了，你特么喝果汁也能喝多？ 还好我聪明，要不然就耽误我回家玩游戏了。');
INSERT INTO `tpshop_joke` VALUES ('641', '一宿舍的同事在厕所抽烟，怕从后面往马桶里扔烟头会烫着pp，就从前面扔，结果烫的那个惨啊，连裤头都没办法穿，就找了两个创可贴打了个十字绷。今天下午一起去洗桑拿，旁边的一哥们跟他说：“兄弟洗澡够隆重的呀，连老二都打着蝴蝶结。”');
INSERT INTO `tpshop_joke` VALUES ('642', '姐上初一时，班上很多女生来大姨妈了，姐还没有，听到一女生说现在不来大姨妈将来会流很多血，姐怕了，又不懂怎么才会来大姨妈，看到人家垫了卫生纸，自己回家也垫了张纸，以为这样就能来大姨妈了。哎，等了一天也没见血，卫生纸还老是滑落…');
INSERT INTO `tpshop_joke` VALUES ('643', '我想当年白素贞身体不舒服就是因为有蛇精病吧！');
INSERT INTO `tpshop_joke` VALUES ('644', '昨天朋友聚会，一个年龄差不多的新朋友席间问我“咱俩个谁大”，我说“这个，咱俩得掏出来比比”，他愣了一下，脸红着说“我说的是年龄”，我说“我说的是掏出来身份证比比年龄，一样啊，哈哈”。');
INSERT INTO `tpshop_joke` VALUES ('645', '大龙觉得自己的名字太土了，于是到小明那里去改名字。大龙问小明：在我的名字后面加上一个什么字才能使它不土而且还很新颖呢？小明说：我能加一个字，使你的名字变得生龙活虎起来。大龙激动的问：什么字？小明不假思索地说了一句：大龙虾');
INSERT INTO `tpshop_joke` VALUES ('646', '我娘又跟我谈人生，说以后可以找个军人男票，因为军人体质好。我：“军人多基佬。”娘亲：“嗯？基佬是什么？”我：“就是同性恋。”娘亲：“哦～怪不得说军队是个大‘基地’。”……娘亲啊?！！！');
INSERT INTO `tpshop_joke` VALUES ('647', '“你的脑子被驴踢了么？” “那你不知道下脚轻点啊” “……”');
INSERT INTO `tpshop_joke` VALUES ('648', '“医生，傲娇怎么治？” “哼，我才不告诉你。” “……”');
INSERT INTO `tpshop_joke` VALUES ('649', '小学时捡过一次铅笔上交被老师表扬，从此走上不归路，经常去找东西上交，还有好几次拿自己的东西交换表扬。最奇特的一次是上课故意晃桌子，把同桌的笔晃掉，抢先一步捡起来，就去讲台上交。同桌也不示弱，死死拖住我，我就一步步把他拖到讲台对老师说:老师，我捡到一支笔！同桌拖着我哭着说:我的……');
INSERT INTO `tpshop_joke` VALUES ('650', '那天和朋友一起去医院做体检，办健康证。医生给我们一人一个试管和棉签，棉签是那种一个木棒上面有团棉花。让我们去厕所取点便便做化验，我说早上刚拉完的。怎么办。医生说：没事，去厕所用棉签插入菊花里会蹭到一些。结果插进去了，这时一朋友叫我。那声音满走廊都能听见，当时我一紧张，菊花一紧，啵，一拔出来，哎呀我去，棉花呢。');
INSERT INTO `tpshop_joke` VALUES ('651', '两二货在聊天：1：嘿！你叫什么。2：我叫封文龙。1：哦！封兄你好啊！！2：可不可以不要叫丰兄！！！1：哦，那文兄你好。2：。。。。能不能正常点。1：你好龙兄！！2：。。。我爸怎么给我取这个名字！！！');
INSERT INTO `tpshop_joke` VALUES ('652', '一个小女孩向流星许愿：我期待有这么一天。生活中出现这么一个帅哥。然后过上这样的生活:你去保养豪车，我做美味佳肴。还应该有个聪明的喜欢玩具的儿子。很多年以后。帅哥毕业于北方汽修，美女毕业于新东方，他们的儿子就读于蓝翔……忧伤源于领悟。');
INSERT INTO `tpshop_joke` VALUES ('653', '有次陪儿子去吃粉丝，给儿子点了一份，觉得有点渴，对儿子说，乖乖，给爸喝口汤，儿子很乖的把碗推到我旁边，把筷子递给我。我说，爸不饿。就喝口汤，儿子说，我是让你堵着点，就喝汤，别把我的粉丝吃了。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('654', '几个屌男闲的，在一起讨论爱听自己媳妇说的一句什么话。甲说：爱听媳妇叫我“老公”。乙说：爱听媳妇说“我爱你”。丙说”我没你们那么浪漫，但我爱听媳妇说“快睡觉。”');
INSERT INTO `tpshop_joke` VALUES ('655', '　　今天上公厕拉完粑粑才发现没带纸，摸摸口袋，刚好昨天发的工资都在，于是乎我用RMB擦屁股了，用了一千多。这不是高潮，高潮是我完事后把钱放水里冲冲然后到隔壁银行存了');
INSERT INTO `tpshop_joke` VALUES ('656', '别以为分手后去你空间或微博看了两眼就以为人家还忘不了你，还在乎你。网友说的：上完厕所还得回头看一眼马桶呢。有时候只是想知道他过得好不好，没我过得好我就放心了！');
INSERT INTO `tpshop_joke` VALUES ('657', '爸爸买了个测谎机器人，专给说谎的人耳光，刚刚组装完，隔壁王叔叔便来找妈妈，小明便喊:爸爸你看，王叔叔来了！！机器人瞬间给了小明两个耳光。。。两个。。');
INSERT INTO `tpshop_joke` VALUES ('658', '　　妹纸：亲爱的，你说小四最不想见到的人是谁丫？ 　　骚年：嗯···应该是周董吧！ 　　妹纸：为什么丫？ 　　骚年：因为每次见面周董都会唱：“矮哟···”');
INSERT INTO `tpshop_joke` VALUES ('659', '家里有个小霸王游戏机，想打拳皇了，于是跑到碟子店：“老板给我张拳皇的碟子”。老板看了我一眼，找了张碟子用黑袋子装好给了我。我回家打开袋子一看，尼玛还真是全黄的。。。。。');
INSERT INTO `tpshop_joke` VALUES ('660', '这个哥们是个拥有博大胸怀的人，是个悟透红尘的人，是个把“你辛福我就是我最大的快乐\"付于行动的人~~~他的网名\"我给情敌买春药”！');
INSERT INTO `tpshop_joke` VALUES ('661', '今天出去看见卖甘蔗，问多少钱一斤，大爷说1.5元，我说太贵，然后给大爷讲价，好不容易说到9毛一斤，忽然想起来没带钱，然后我弱弱的说了一句，大爷能不能送我一根。大爷看着我说，你神经病？哎。丢死人了。');
INSERT INTO `tpshop_joke` VALUES ('662', '和往常一样，我的父亲喝了点酒，一场家庭暴力即将发生。我忍不住问他：“爸！你为什么要喝这么多酒？！” 父亲重重的吸了一口烟，忧郁地说：“麻醉了全身，挨你妈打时不会那么疼。”');
INSERT INTO `tpshop_joke` VALUES ('663', '　　老婆：“亲爱滴，你最佩服滴人是谁？” 　　老公：“俺最佩服滴人唐三藏！” 　　老婆：“为嘛泥？” 　　老公：“他整天啥也不干只管旅游还老有漂亮妹纸争着抢着要吃！” 　　老婆：“难道你想被吃？” 　　老公：“有不想被妹纸‘吃’滴男同胞木有？” 　　（认为木有滴请点赞哈哈！）');
INSERT INTO `tpshop_joke` VALUES ('664', '皮肤一一皮下组织一一脂肪一一肌肉一一前鞘一一壁层腹膜一一脏层腹膜一一子宫。这些都是剖宫产取出婴儿后要缝合的组织，想当年麻醉都快过了，医生还缝合，我能清楚的感觉针穿过去但不痛，可是拉线时就痛心痛肺的。还不能叫，更不能说话。好几次痛得受不了问医生还有多久。都换来责备声。缝合花了1小时45分，这是老公说的，他一直在门口不安的等着。男同胞们，女人生孩子不易，剖腹产更不易，且行且珍惜！');
INSERT INTO `tpshop_joke` VALUES ('665', '　　今天往新家搬家具，在楼下才搬一茶几下车，没走几步就碰了一老大妈，只见她立刻倒地不起，哼哼唧唧。我就发蒙了，先想：我的工资还的半个月发，我的银行卡中已没有几块钱了，我还欠贷款，我还欠丈母娘钱，倒霉啊，听天由命吧。先扶大妈起来吧，向前扶到一半时大妈讲：咋赔我？您说吧，我彻底奔溃。大娘看了看我，突然大笑：没事的，没事的，这个游戏真的很刺激。我得个闹心呀。');
INSERT INTO `tpshop_joke` VALUES ('666', '　　我和同事（同事未婚）开玩笑说，我们来看一下手心，你经常用哪只手打飞机，哪只手的手心就会发黑，这货马上就去看右手，然后把左手拿过来给我们看，大家那个笑啊！泪的出来了！！！');
INSERT INTO `tpshop_joke` VALUES ('667', '冬日的早上，天异常寒冷，飘着雪花。营房前，连长在训话：弟兄们冷不冷！‘冷’大家异口同声。‘冷就来个五公里越野暖和暖和’，第二天连长又问：弟兄们冷不冷，不冷！不冷那就站两小时军姿！尼玛。。。');
INSERT INTO `tpshop_joke` VALUES ('668', '一哥们长相平平，喜欢和帅哥呆一起。时间长了大家都说你是不是搞基啊？二货说：“不是啊，帅哥身边美女多。看能不能捡个漏！”……捡个漏…？二货带上我！');
INSERT INTO `tpshop_joke` VALUES ('669', '昨天晚上一女孩约我出去玩 我说我没钱 她说她有钱就可以了 看完电影把我骗去宾馆开房 老子36块出去的 今天早晨536回来的');
INSERT INTO `tpshop_joke` VALUES ('670', '昨晚楼主一家子和岳父岳母一起出去吃饭，酒足饭饱，楼主潇洒的手儿一挥，“服务员，埋单！”我正要掏钱买单，岳父大人忙一手把我拦住，“装啥子逼呢！就你那几个零花钱？你还是留着买包烟抽吧！”这时，岳父转面以命令的口吻对我老婆说:“闺女！这单你买！”我顿时眼眶一热，还是岳父理解我，中国好岳父啊！出到饭店门口，岳父偷偷附我耳边问:“刚才帮你省多少钱来着？”“六百多呢！”“那好！待会给我整条玉溪烟，注意保密！”');
INSERT INTO `tpshop_joke` VALUES ('671', '昨天上班的时候，看到前女友了。她坐在台阶上，满脸疲惫，喝着一块钱一瓶的廉价矿泉水，时不时的擦擦额头上的汗。我知道我该去打个招呼，毕竟相爱过。“hi，你好吗？”我对她说道，脸上漏出阳光般的笑容，她有点不知所措，没想到是我，低下头逃离我的视线。我接着说：“后悔吗？当初选择离开我。”她的脸红彤彤的，那么可爱，还用小手捋了捋自己的秀发。我知道该有所行动了，我不想再错过这次机会，我靠近她，对她说道：“这个瓶子可以给我吗？”');
INSERT INTO `tpshop_joke` VALUES ('672', '拉拉在课堂上玩吸铁石，被班主任看到了，班主任走下讲台来没收。她刚一伸手，整个吸铁石就吸在了她的金戒指上……她的金戒指上……她的金戒指上……');
INSERT INTO `tpshop_joke` VALUES ('673', '开车到乡下游秋，经过一个村子时不小心碾了一只小鸭子，出来一位大姐要赔偿，我说一只小鸭子能有多少钱？…她说这鸭子家养的，能长到七八斤，一斤能卖五十块呢……我慌忙丢下五百元就走了，万一她发现那鸭子是母的我连车都保不住了……');
INSERT INTO `tpshop_joke` VALUES ('674', '厂主对被开除的工人说：听说，你要在我死后到坟场上对我的坟墓吐口水？　　工人说：放心吧，我已经改变了主意，我没有排队的耐心。');
INSERT INTO `tpshop_joke` VALUES ('675', '新来的女秘书美丽迷人，两位主管决计亲自指点她工作。　　教导她该怎样不该怎样，是我们的责任，甲说。　　对，乙兴奋的说，你负责教导她该怎样。');
INSERT INTO `tpshop_joke` VALUES ('676', '经理对秘书说：八月二十日的会议十分重要，请你记着提醒我。　　秘书说：这是前天的事了。　　经理说：天啊！我居然忘记了参加会议！　　秘书说：您已经去过了。');
INSERT INTO `tpshop_joke` VALUES ('677', '办公室里老张素以机智著称。　　某日小王特别找了一个题目来刁难他。　　小王说：老张啊！你可知世界上最吃亏的事是什么？　　老张说不知道。　　小王说：就是一个人死了，他的钱还没有花光。　　大家都看着老张，心想他这回可没什么好说的了！　　谁知老张愣了一下，随即说道：小王啊！那你知不知道世界上最惨的是什么事？　　小王说不知道。　　老张说：就是一个人钱花光了，他还没有死。');
INSERT INTO `tpshop_joke` VALUES ('678', '小张：科长，对批评您不介意吧？　　科长：绝不，反而很喜欢。　　小张：是啊，真诚的批评好处很多……　　科长：最重要的是我想知道谁对我不满。');
INSERT INTO `tpshop_joke` VALUES ('679', '一次公司开会，董事长不小心放了一个屁．怕没面子，便小小声告诉甲员工，帮我顶一下，给你加薪。　　甲员工便告诉会议中的人说：对不起，刚才吃太多了，所以放了一个屁。　　又过不久，董事长又放了一个屁，又告诉乙员工：帮我顶一下，给你加薪。　　乙员工也照着做。　　谁知道董事长又放了第三个屁，这时丙员工快速又大声的答道：我的！我的！那是我的屁！！');
INSERT INTO `tpshop_joke` VALUES ('680', '同事在午餐后于办公室闲聊，谈到新同事珍妮自幼丧母，四姐妹长年旅居国外，均由她父亲一手带大，真是父兼母职的好父亲。　　不料在一旁休息，受英文教育而对中文又一知半解的珍妮竟生气的跑过来说：请你们不要骂我父亲是‘福建母猪’好吗？');
INSERT INTO `tpshop_joke` VALUES ('681', '‍‍‍‍朋友对我讲：“如果你的相亲对象，没工作但打扮时尚、身材火辣、一身奢侈品、手拿iphone6！那么恭喜你！” 我忙问：“怎么讲？” 同事一脸坏笑：“她什么姿势都会！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('682', '在公园里散步，突然看到前面有一对情侣喝完水把塑料瓶随地扔下。我快步跑到情侣旁边，捡起塑料瓶就对这对情侣吼道：就是因为你们这样的人，才让别人说我们中国人没有素质。情侣含羞而去，看着远去的情侣，我拿着塑料瓶说：再拣几个就攒够钱坐公交车回家了。');
INSERT INTO `tpshop_joke` VALUES ('683', '‍‍‍‍昨天下班回家买了凉菜，看见旁边还有卖黄瓜的。 嫩嫩的就买了点，回去吃完菜去洗个黄瓜分给朋友吃。 看到她拿着黄瓜闻了闻。 我说：“没毒。” 她说：“我就闻闻，看看是不是用过的。” 我去！还能不能好好玩耍了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('684', '小明费劲千辛万苦终于凑齐龙珠。小明:“神龙出来吧！”顿时乌云密布电闪雷鸣神龙出现！小明万分激动：“可以满足我一个愿望吗？”神龙：“きぅぁぉくぃうずざこお”小明：“呃。可以说中文吗？”神龙：“可以”。然后神龙就消失了……');
INSERT INTO `tpshop_joke` VALUES ('685', '“老公，你鼻子长痘了”“长痘怎么啦？”“鼻子上长痘是不是叫痘鼻（逗比）啊”');
INSERT INTO `tpshop_joke` VALUES ('686', '‍‍‍‍就是昨晚，要睡觉了，一室友说：“哪来的怪味。” 我想是不是谁的脚臭味，就说：“不就是有点臭吗？” 他说：“不是。” 我问：“那是啥。” 他居然说：“是烧开水的味。” 尼玛，我少读书，烧开水还有味？‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('687', '‍‍‍‍同桌失恋了，他趴在桌上有气无力地说：“这世界上还有什么比爱情还复杂啊？呜呜呜！” 我二话不说，抄起桌上的数学书咂向他脑袋。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('688', '‍‍‍‍两个谈恋爱的人在谈话。 妹子:：“亲，你怎么那么黑呢？” 小哥：“嘿嘿，不是有句话说的，黑夜给了我黑眼睛，我想我应该是不小心按了全选！不过这样也好，我可以在黑暗中保护你啊。” 说得妹子乐滋滋的，感动啊！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('689', '一天坐公交！我前面有个美女，上车就跟开车的师傅说：“我没带钱，亲你一下就当投币了可以吗？”师傅说可以。美女亲了司机一口就坐下了。后面一大爷看一清二楚，二话没说上来就一顿猛亲司机，然后说：“我也没带钱。”司机师傅都吓哭了说：尼玛刚才那是我媳妇跟我开玩笑呢 。。。。');
INSERT INTO `tpshop_joke` VALUES ('690', '‍‍‍‍女神发了一条说说：“今天走着走着，突然一阵大风吹来，裙子竟然走光了，哎，真是郁闷死了。” 我回复道：“这么精彩的场景我没有看到，这才是真正的郁闷！” 紧接着她就把我qq删了，而且从此之后就再也没搭理过我。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('691', '刚刚朋友到饭店点菜，叫到，营业员给我来盘白菜炒芹菜，芹菜炒鸡蛋，鸡蛋炒白菜，白菜炒鸡蛋，鸡蛋不要蛋黃，蛋黄不要太黄。老板：滚出去！');
INSERT INTO `tpshop_joke` VALUES ('692', '我和弟弟是双胞胎，我能告诉你我们高考全是我考的，吃自助餐还能花一份钱吃饱倆人吗');
INSERT INTO `tpshop_joke` VALUES ('693', '“别生气，兄弟之间互相叫个傻逼怎么了？开玩笑而已，感情好才不拘小节，你看二狗不也是经常叫你傻逼吗？”“滚你妈逼的，他可不跟你一样说的这么发自肺腑！”');
INSERT INTO `tpshop_joke` VALUES ('694', '‍‍“兄弟，今年圣诞节咋过啊？” “吃饭，睡觉，街上看美女！” “有什么不一样的吗？” “街上看美女，吃饭，睡觉。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('695', '‍‍昨天和闺蜜在公园坐着聊天，旁边两个男的大声在哪里吹嘘自己一晚上几次，有多厉害。 突然过来一老大爷拄着拐杖，轻声细语的对两男人说：小伙子，你要明白一个道理，没有耕坏的田，只有累死的牛！ 仔细想了想，我觉得也有道理。‍‍');
INSERT INTO `tpshop_joke` VALUES ('696', '‍‍少年：“大师，我怎么样才能成为，百万富翁，千万富翁。” 大师默默不语，指了指山间吃草的黄牛！ 少年：“我懂了，大师！你是要我像他一样，吃苦耐劳，辛勤劳做。” 大师：“都春运了，还不去囤票。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('697', '在公交车上，电话突然响了，旁边站了个穿短裙的美女，我从裤兜里掏出手机接听，不小心烟掉了我蹲下去捡，站起来之后发现打火机又掉了，然后再蹲下去的时候，美女就给了我一脚，，，，，我真的不是故意的！');
INSERT INTO `tpshop_joke` VALUES ('698', '今天燕郊下车路上看到一对情侣吵架到分手，大致是女孩子要求男孩子买果6男孩子刚刚工作才3000多，目前支付不起，女孩就以这个理由分手。后来男生一再劝说但最后女孩不同意。男生转头就走了。女孩儿就在原地吼男孩没钱永远别来找我。我听到这话。无语了。说实话，在我看来，女孩子无论漂亮或者丑，都不应该要求男朋友给你买他目前承担不了的，都说女子在外不容易，同样男人在外也不容易，不能仗着男女朋友就提出过分要求，谁的钱也不是大风刮来的，男人最后成什么样子，和他爱的女孩子给他的影响很深。所以如果在一起请珍惜。不要绿这个那个，约这个那个，爱惜自己，也爱惜对方。喷子请轻喷。');
INSERT INTO `tpshop_joke` VALUES ('699', '千万不要跟女人打架，打赢了女人，别人说‘欺负女人算什么男人？’被女人打赢了，别人会说‘女人都能打赢你你算什么男人？’跟女人打成平手，别人会说‘一个男人连女人都打不赢算什么男人？’结论就是，一旦你跟女人打架，最后的结果都只有一个，你不算男人。');
INSERT INTO `tpshop_joke` VALUES ('700', '初中时有个同学非常迷恋玄幻小说，有一天，他在过道摆出道家经典的手印，大喊一声：“破邪！”漂亮的英语老师刚好从校长室出来，愣了一下，破口大骂：“你才破鞋！你才破鞋！你全家都破鞋！！明天叫你家长来！”');
INSERT INTO `tpshop_joke` VALUES ('701', '美女同事今天喊住我“跟你说个事”我很激动啊，今天是520啊，问：“什么事”女同事：“我长的蛮白的”我：“看得出来，怎么了。”女同事“没事呀，今天是表白日，我向你表示一下我很白，就这样”我次奥。。。没人表白已经够悲催了，你TM还来耍我。。。');
INSERT INTO `tpshop_joke` VALUES ('702', '‍‍‍‍以前一直不明白为什么结婚要选个好日子，啥时候结不都一样么？ 直到后来我结了婚才恍然大悟：“原来结了婚就没有好日子了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('703', '‍‍‍‍今天和女友走在大街上，突然一辆保时捷冲来，将我女友撞飞几十米。 老子当时破口大骂：“你他娘的开车不长眼啊？” 要不是老子这次气冲满了，飞出去的就是我了吧？‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('704', '今天下午我和6岁的侄子看电视，我说：长大后你去当兵吧！他说：我不能当兵。我问：为什么？他说：我的头太大了！我想了半天也没想出为什么头大就不能当兵！结果他说：头大容易被瞄准啊！！！原来如此啊！！！');
INSERT INTO `tpshop_joke` VALUES ('705', '‍‍‍‍小时候为了几元钱去玩游戏机，经常顺点废铁、酒瓶。 有一次中午上学路上，刚好看到路边有施工的铁轮。 一阵心喜，顺手拿了两个，由于急着去上学，无法直接卖给废品收购站的老板娘。 刚好看到旁边有几个树坑，便把铁轮放到其中的一个树坑里面，上面盖好土，只等放学拿出来卖俩钱。 谁知，戏剧性的一幕出现了，下午放学到地方一看……树坑已经被载上了树。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('706', '‍‍家有一老，如有一宝。 我外婆九十岁，身体挺好只是耳朵有点背。 今天家里就我和外婆两个人，我去厕所，蹲了个大号。 舒舒服服的释放掉才发现没带纸！ 蹲着在厕所找了几分钟放弃了，就叫外婆。 叫的都声嘶力竭了，现在还蹲着呢。 诶，我准备还是自己去吧。‍‍');
INSERT INTO `tpshop_joke` VALUES ('707', '读初中的时候，我们教室后门上有个小洞，老师就经常从洞里面监视我们，一天我就跟一2B同学开玩笑说老师来了，他马上就规矩的做作业了，我们都笑他，过了几分钟老师真来了，我又说老师来了，那同学直接就朝门洞里吐了帕口水，结果那同学被老师整惨了');
INSERT INTO `tpshop_joke` VALUES ('708', '‍‍‍‍老婆在辛苦的干着家务，老公却目不转睛的盯着泳装走秀节目看。 老婆抱怨的说：“你就忍心看我一个人在这里干活呀！” 老公说：“不忍心看呀！你没看我目不转睛的盯着电视看吗？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('709', '　　男同事瘦子和一胖同事女的搞暧昧，，一天我问他说把你俩放一起打一到菜，你知道叫啥吗，他来了句金针菇炒黑木耳，瞬间，，，哥凌乱了，，');
INSERT INTO `tpshop_joke` VALUES ('710', '‍‍‍‍奶奶一次去医院说：“看看自己的腿总是没有劲，为什么？” 医生告诉她：“大娘！您这是缺钙。” 没等医生说完，奶奶插句嘴：“我晚上盖的不少！还有被子压脚呢！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('711', '春寒料峭，公园的偏僻处传来一男子哀求的叫喊：“大哥，放我一马！” 另一年长男子沉稳回道：“不行！” “我给你车？” “那也不行！”“大哥，不要啊，大哥……”伴随着凄厉的哀喊，只见年长男子缓缓地抬起手，“砰”地一声重重的砸了下去：“将军！”');
INSERT INTO `tpshop_joke` VALUES ('712', '记得小时候和小伙伴们冬天里扔雪球，当时我用石头捏在雪球里边结果把他们全扔哭了，而且每次都是我，从此我在村里有了个称号，雪地制霸，现在想想挺对不起他们的，莫怪老衲！');
INSERT INTO `tpshop_joke` VALUES ('713', '　　小乌龟看到一只蜗牛问：“你在干什么？”蜗牛说：“我在练长跑。”小乌龟鄙视地说：“上来吧，我带你。”蜗牛爬到小乌龟背上，那里有条蚯蚓，蚯蚓说：“坐稳了，老快了。”');
INSERT INTO `tpshop_joke` VALUES ('714', '向男神表白，男神只回了句「why double may」然后就下了线。我盯着这句话百思不得其解，直到旁边的室友读了一遍：「我爱 大波 妹」');
INSERT INTO `tpshop_joke` VALUES ('715', '有两个同学，她们关系很好，女孩经常开玩笑说是他娘，这男孩喜欢这女孩，有一天，他去和她表白，女孩说，你只要去学校大会上当着所有同学表白，我就答应你，男孩真的跑到大会上，大声说，妈妈，我爱你，，，还受到学校领导的表扬。。');
INSERT INTO `tpshop_joke` VALUES ('716', '　　我是一名男学生，我们班级有一女同学，为人活泼、大方、开朗，但是人长得特别瘦，又很爱开玩笑。有一天她坐到我身边和我聊天。我看了她一眼说：“你人太瘦了，全身都是骨头。别减肥太过，导致发育不良啊。”没想到她却说：“就你胖，就你有肉，别忘了，你有一个地方全是肉，一点骨头都没有”。 尼玛，我杂想不明白。到底是哪里 ？？？、、');
INSERT INTO `tpshop_joke` VALUES ('717', '儿子现在经常喝奶喝到一半就累得睡着了，必须把他弄醒继续喝完，今早尝试网上的办法，在他喝睡时用力弹他的脚底，儿子痛的撕心裂肺大哭起来，我妈一下跳起来给了我一巴掌：“他是你亲生的吗？下手这么重！”我正准备哭，老婆也跳起来了，冲我妈吼：“他是你亲生的吗？下手这么狠！”');
INSERT INTO `tpshop_joke` VALUES ('718', '早上一边刷牙，一边看手机，碰巧看见一个新闻 :三个月不换的牙刷比三个月不洗的袜子还脏。 当时就给我吓尿了，立马把我那四个多月没换的牙刷扔了，赶紧用我半年没洗的袜子擦了擦嘴，那酸爽简直不敢相信。');
INSERT INTO `tpshop_joke` VALUES ('719', '　　看到路边有人烧纸钱，就问同桌：清明节到了吗？ 他说：没有啊！那怎么现在就有人烧纸钱？他说：提前寄，清明节寄的人多，等的时间长，亲人上来找就麻烦了……我竟无言以对，想想也是。');
INSERT INTO `tpshop_joke` VALUES ('720', '今天去理发，旁边坐着一个十岁左右的小男孩。（背景）理发师问小男孩说:“你要怎么剪？”小男孩自信的说:“给我剪一个阳光帅气，霸气，非主流而又不失风度的光头！”');
INSERT INTO `tpshop_joke` VALUES ('721', '公交车上，司机最多的话是：“往后走，往后走啊。” 那天坐公交，公交司机也这样喊：“往后走啊，往后走啊，那后边没有人。” 这时候，一屌丝弱弱地问：“师……师傅，你别吓唬我，你真的没看见‘他们’吗？”');
INSERT INTO `tpshop_joke` VALUES ('722', '今天上课，男老师，本人女，下课的时候我去厕所拉臭臭。。。。。然后回去的时候老师点名了，但是已经点过我的名字了，同学说老师给我记了旷课，于是我就去找他解释，我说我刚去厕所了，结果！！！老师问，你去厕所有没有证据，你怎么证明你去厕所了！！！！！去厕所还要证明，我怎么跟你开，傻逼老师，我直接告诉他，老师，你是要跟我去女厕所看看，还是我去取点翔你看看我中午吃的什么……');
INSERT INTO `tpshop_joke` VALUES ('723', '一哥们讲他小时候的事，小时候用水枪枪战，这哥们被其他小朋友追出去好远，是好几个打我哥们自己，结果哥们的枪里没水了，果断窝到草堆后面退下裤子，来了一泡热的。。。遭遇战瞬间变成了歼灭战，据说还有被打到嘴里的，从此哥们在村里当上了孩子头。');
INSERT INTO `tpshop_joke` VALUES ('724', '有个吃货女友是什么体验 我看到什么好吃的都想陪她吃 她看到什么好吃的都想替我吃 每天寸步不离地看着，就怕一没管好连屎都吃 而且遇上一两个脾气爆的，有次吃饭跟她吵架，“嗖”地一声就站起来冲我吼 “你翅膀熟了是吧？？！” 终于忍不住跟她说 “你长点心吧你” “点心？什么点心？？！” 我真tm怀疑当初怎么看上这娘们的……');
INSERT INTO `tpshop_joke` VALUES ('725', '‍‍‍‍第一次相亲成功了，女方留我在她家过夜，女方父母单独给我留了间房。 都晚上十点半了，她还赖在我的房间不肯走。 我当时也不知道怎么想的，就对她说：“你还是快点出去吧，有女人在我睡不踏实。” 然后就没有然后了！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('726', '今天村里一个大爷来找爷爷喝酒，爷爷嘲笑他放羊丢羊。爷爷说人家放一群羊丢了还算正常，你这老头一个人放一只羊还给丢了。然后那大爷憋红了脸来了句，我没它跑得快。。');
INSERT INTO `tpshop_joke` VALUES ('727', '‍‍‍‍女：“我们分手吧？” 男：“好好的，为什么要分手啊！” 女：“哪里好了！我知你长短，但你不知我深浅！我和你在一起是不会幸福的，还是分手吧！” 男：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('728', '　　今天在单位两个同事A 和 B逗着玩，打了起来，经过一番搏斗，A 把 B 按在了地上，直到 B 不再挣扎才放手，突然 B 从地上一跃而起，指着 A大声问道：“服吗？”然后转身大步离开…那声音，那气势，有那么一刹那我都觉得是他打赢了…');
INSERT INTO `tpshop_joke` VALUES ('729', '小学的时候比较讨厌她，和她同桌时在桌子中间画条线，告诉她如果过线就用圆规扎她，后来圆规丢了，也没在意！长大后，她变漂亮了，追求她成功了，新婚晚上，她在床上画了条线，并且拿出了我当年丢的圆规！');
INSERT INTO `tpshop_joke` VALUES ('730', '‍‍‍‍在酒店相亲的女孩问我：“有房么。” 我羞涩的回答到：“有，刚开的情侣套房。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('731', '上课中，老师抽点名，抽点完后，一奇葩同学激动的站起来，‘’老师，为什么没点我。‘’ 全班不淡定了好不好，没有这么积极的好不好，声音洪亮啊，老师灰常淡定的来一句‘’我是抽点名啊，是不是没抽到你就感觉白来了……白来了……老师心里清楚着呢，那同学名叫夏克。。');
INSERT INTO `tpshop_joke` VALUES ('732', '‍‍‍‍语文课，同学们正在互相交流全家人爱看什么书。 一会儿，发言时间到了。 小明抢先发言：“我们全家人都爱看书。妈妈爱看保健书，爷爷爱看笑话书，奶奶爱看做菜书。” 老师提示到：“那爸爸爱看什么书呢？” 小明想了想，说道：“我爸爸是经理，他爱看秘书。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('733', '‍‍一高僧问我：“两个妹纸，一个胸大和长腿的，你选哪个？” 我苦苦思考了一夜。 第二天，我对高僧说：“能不能两个都选。” 高僧点头微笑道：“能啊！你选你的，管我啥事啊！我又没妹纸给你。爱咋选咋选。” 我想说，现在的高僧都特么这么逗么！‍‍');
INSERT INTO `tpshop_joke` VALUES ('734', '昨天到邻居家玩，看到前几天正在开花的那棵花树叶子都落完了。我问咋回事啊？她的小萝莉说，妈妈剪了几枝花插在花瓶里，它气死了。');
INSERT INTO `tpshop_joke` VALUES ('735', '‍‍老师：“小明，昨天你扫地太不干净，我要罚你！” 小明：“老师，不是俺扫的地。” 老师：“那是谁？” 小明：“威风啊！你罚他吧！”（威风扫地啊！）‍‍');
INSERT INTO `tpshop_joke` VALUES ('736', '哥给兄弟打电话说：家里有海鲜！你带两瓶酒、在买俩菜上我这来！弟高兴的拿着东西去了、喝的差不多了、还是不见海鲜、就问哥：咱的海鲜呢？……………………我草！虾米紫菜汤！');
INSERT INTO `tpshop_joke` VALUES ('737', '‍‍高中生不光是紧张的学业，也有同学间的友谊和调侃。 课间，一个女生对同桌闺蜜说：“高考结束了我就去整容。你说我要去韩国整个容，到底整哪里好呢？” 闺蜜还没言声儿，后面的男生说话了：“我看吧！换个脑袋就行了。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('738', '老湿为了向学生证明吸烟的害处。特意把从香烟中提取的尼古丁放在虫子身上，不一会儿虫子就死了。老湿接着问大家:你们看，这是实验说明了什么？ 童鞋们异口同声的说:“抽烟肚子里没虫子！！！”');
INSERT INTO `tpshop_joke` VALUES ('739', '‍‍‍‍老婆：“听说咱小区的小娟傍大款了，对方还给她买了钻石项链呢，也不知道那男人是谁？” 老公：“反正不是俺，俺就给她买过一珍珠的……” 呃？老婆大人饶命哇！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('740', '有天晚上我正走在回家的路上，忽然听到一个女人的叫声，不要啊。我连忙上前看见一个男的堵着一个女的，我立即冲了上去把那个男的打了一顿。那个女的说你干嘛打我老公！');
INSERT INTO `tpshop_joke` VALUES ('741', '一女人抱着婴儿去看病，男医生看了看婴儿，随后摸了摸女人的咪咪。 男医生说道：“奶水不足，婴儿营养不良！” 女人怒骂道：“你TMD不开口先问问就摸？我是孩子他小姨！”');
INSERT INTO `tpshop_joke` VALUES ('742', '上生物课时老师在讲遗传，小明同学举手问老师：是否可以讲得具体点？ 老师：就是你长得像你爸爸就是遗传。 小明：我明白了，但认识我的人都说我长得像我邻居王叔叔。 老师说：那不是遗传是遗憾。');
INSERT INTO `tpshop_joke` VALUES ('743', '某天，同桌上课迟到了，班主任恼怒地问：“为什么会迟到？” 同桌：“因为房间太乱，我没有整理。” 班主任：“所以你宁可不上课，也要在家里收拾房间吗？” 同桌：“不，是我妈说我床上东西堆太多了，她翻了翻没找到我，就以为我已经去上学了！”');
INSERT INTO `tpshop_joke` VALUES ('744', '今天饭店吃饭，一大叔坐我对面，他对我说：小姑娘，你有男朋友吗？ 我：没呢，现在还单身。 过了半分钟，大叔：小姑娘啊，你这不行啊，再丑也要谈恋爱啊。 卧槽。。。尼玛。。。尼玛。。。看招！');
INSERT INTO `tpshop_joke` VALUES ('745', '小明是一个盲人，在按摩房工作，一天，一个漂亮的美眉找他按摩，他按了好一会说：“背面按好了，换正面吧。” 这时美眉怒道：“你刚刚就是在给老娘按正面的。” “额，感情这美眉是个飞机场啊……”');
INSERT INTO `tpshop_joke` VALUES ('746', '被老婆甩了一巴掌，我哭着跑去找我爸。 我爸一看我脸上的巴掌印吓了一跳，说：“儿媳妇手掌挺大的。”');
INSERT INTO `tpshop_joke` VALUES ('747', '今天做公交车，看见一猥琐男摸一女的很久了，那女的一直没反应。 我TM是在看不下去了，于是冲过去对那男的说道：“你TM技术到底行不行啊，不行让我来！”');
INSERT INTO `tpshop_joke` VALUES ('748', '女朋友问我：“亲爱的你会不会家暴打老婆？” “你放心好了。”我笑着对女友说道：“下次打她的话一定叫你。”');
INSERT INTO `tpshop_joke` VALUES ('749', '街头散步，遇到一乞丐，他对我说：“行行好吧！” 然后掏出一台验钞机……');
INSERT INTO `tpshop_joke` VALUES ('750', '“老板，今天游泳池里的水怎么少得这么快呀？” “哦，对面那群女孩子是今天刚来学游泳的。”');
INSERT INTO `tpshop_joke` VALUES ('751', '昨晚打KFC叫外卖。从十二点打到一点，电话里一直在说占线中……朋友实在饿的不行了，拿过电话，按了Englishservice…等待…通了！朋友第一句话就是：Can you speak Chinese？对方一愣，回答：yes！然后这货就模仿老外的口音用中文问：那卧能泳种文电餐吗？回答：可以！于是点餐成功！');
INSERT INTO `tpshop_joke` VALUES ('752', '哎！ 今天进了趟公安局，理由特简单：昨天给我新买的3D自行车换了个繁琐的锁，但是由于我不会弄，今天上班时在小区门口弄了半天都没弄开，最后门口保安打了110，还对电话里说，喂，是110吗？我抓到一个小偷正在偷车，但是好像是个新手，撬半天都没撬开。 我当时这个无语......');
INSERT INTO `tpshop_joke` VALUES ('753', '‍‍老婆与老公去看电影。 一进黑漆漆滴影院，里头尽是搞对象滴男女！ 老婆指着搂在一起滴一对儿，说：“亲爱滴，你也跟人家学着点儿呗！” 老公看着那个，恨不得钻进怀里妹纸滴内衣里头去滴男人，对老婆说：“亲爱滴，原来你想让俺出轨啊！” 老婆：“你啥意思？” 老公：“那男滴是俺单位经理，那女滴反正不是他媳妇儿。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('754', '我上高中时候的女朋友很霸道，平时不许喊她 老婆，不许拉手走路，不许跟同学说我和她的关 系，晚上回她家睡觉也是分开走，当时我一直很 不开心，后来这么多年过去我也慢慢释然了，毕 竟她是我的班主任。');
INSERT INTO `tpshop_joke` VALUES ('755', '室长：街上的美女与自己的女友有什么区别？ 室友A：街上的美女有胸～器 室友B：穿的特短 室长：你们都没有说出本质，街上的T～MD都是活的，活的，活的…知道的了吗？');
INSERT INTO `tpshop_joke` VALUES ('756', '今天读大学的表弟来我家吃饭，问到他有没有交女朋友时候，表弟说还小... 然后我老妈子不知道抽了哪条筋说了一句。 不小了啊，你表哥像你那么大时候有女孩子都在咱家住着不肯走了.. 说完大家都笑了，突然觉得哪里不对劲。 回头看了一下老婆.一股杀气涌在我身上...');
INSERT INTO `tpshop_joke` VALUES ('757', '　　家在东北农村，平房没洗手间，厕所在屋外好远，冬天冷～~～~背景交代完毕～～~一天朋友们坐一起喝酒，有个货想小便又嫌外面冷，我说那就提个饮料瓶子去院子里解决。这货说太小没法用。。。这时候嫂子来句:脉动的就行！。。。脉动的就行，秒懂的有没有');
INSERT INTO `tpshop_joke` VALUES ('758', '表妹被甩了，表哥担心她想不开，就在QQ上开导她。 表哥问：“那男生为什么要和你分手？” 表妹似乎是个忧伤的女孩，又似乎是懒得听表哥唠叨， 半天了才回表哥一句：“莪蔂ㄋ，眞啲蔂ㄋ。” 然后QQ便下线了，深藏仇与恨。 表哥似乎突然醒悟了。。。。');
INSERT INTO `tpshop_joke` VALUES ('759', '最近一女汉子朋友跟一哥们表白了：“那个，那个，我喜欢你。。。”哥们当时就不乐意了：“我一直拿你当兄弟，你在这跟我撒娇卖萌！”女：“老娘想跟你处对象，有意见吗？”男：“没。。。没意见，都听你的。”');
INSERT INTO `tpshop_joke` VALUES ('760', '妹妹在学校调皮捣蛋被要求叫家长来，妹妹硬叫我去冒充老爸，我迟疑了下：“不好吧，老师一眼就能看出我不是。”果真老师一眼就看出我不是老爸：“不是说了叫父母吗？怎么把爷爷叫来了？”');
INSERT INTO `tpshop_joke` VALUES ('761', '车前有个小姑娘，向后跳了一下摔倒在地，用很痛苦的表情看着我。我：“没撞到你啊，怎么年纪轻轻就讹人啊？” 小姑娘：“谁能证明，你有装行车记录仪吗？” 我心头一紧，没装啊。“后悔了吧？” 说着小姑娘从包里掏出了一个盒子，“大哥装一个吧给你优惠价”。');
INSERT INTO `tpshop_joke` VALUES ('762', '群里来新人，大家都起哄让他爆照，小伙子长的挺帅气，我问：“小伙子，还没女朋友吧？” 他：“你怎么知道？” 我：“因为你的名字叫没有B要啊”');
INSERT INTO `tpshop_joke` VALUES ('763', '舍友八万，考试头天晚上，对我说：我今天晚上不打算睡了。 我问：咋了？想临阵磨枪？ 八万：什么啊？我要把那部新下的毛片儿看完！ 我：卧槽！明天都要考试了，特么还急着看那个？ 八万：没法子啊，我爸说了我考不及格他就打断我手，到时候我还咋撸？ 我晕……');
INSERT INTO `tpshop_joke` VALUES ('764', '出差去外地，找个家酒店住下了，已经午夜了 隔壁却依然还是不停的啪 啪 啪… 雅蠛 蝶的声音不绝于耳， 隔壁的小荡妇叫的那个销 魂， 让我羡慕嫉妒恨，我邪 恶的想到了，警察蜀黍，于是我把手机调成最大音量，放了一段警车鸣笛的声音…');
INSERT INTO `tpshop_joke` VALUES ('765', '今天买了三斤芒果，到家之后我姐拿了最大的一个芒果进房间了几分钟之后又拿着芒果洗了一下，估计是刚才拿芒果的时候忘记洗了吧！');
INSERT INTO `tpshop_joke` VALUES ('766', '我们在朋友家里打麻将。\r\n哥几个问他：你媳妇回来真的不会生气?\r\n他说：不是我吹牛，老子在家就是老虎。\r\n突然门口有动静，他赶紧收麻将。\r\n我们说：你不是老虎嘛?\r\n这b说：武松回来了，快………～');
INSERT INTO `tpshop_joke` VALUES ('767', '和男朋友出去夜宵，他去买烟让我先去，到地方点好菜，旁边两个光着膀子的男人一直往我这边看，还小声低估什么，看他俩那猥琐的样子就知道说的不是什么好话，抽的烟头还故意扔到我脚边，我白了他们一眼，气的直跺脚！看我不做声他们说话声音也大了，越说越难听，我刚想和他们理论，旁边一个人影冲过去直接把那俩流氓撂倒，不愧是我男朋友！晚上我躺在他怀里，他说了一句话：“还记得当初我追你的时候说过什么吗？我的拳头和下体只为你硬！”好幸福的感觉！我又何尝不是呢？');
INSERT INTO `tpshop_joke` VALUES ('768', '一天政治课，老师问正在睡觉的小明：什么是所有权，什么是使用权？\r\n小明：我是我妈生的，所以我妈拥有我的所有权。我妈嫁给我爸，我爸拥有我妈的使用权。\r\n老师：好像说的有道理，这次放过你，坐下');
INSERT INTO `tpshop_joke` VALUES ('769', '暑假在叔叔店里帮忙，他让我把门口的牌匾擦一下，结果我一不小心把牌子弄掉下来砸两半了，就在叔叔愤怒的准备揍我的一刹那，我连忙说：叔叔，恭喜恭喜啊，这是要开分店啊。叔叔赞许的摸了摸我的头。太机智。');
INSERT INTO `tpshop_joke` VALUES ('770', '那会儿刚到日本，日语还不利索。有一天在公交车站等车，来了两个水手服妹子问路，我不想在妹子面前露怯，结结巴巴的说怎么走怎么走。妹子点头微笑说大丈夫慢慢说别着急……说到一半，俩妹子的其中一个，忽然对另一个用普通话（他说的口音很象北京的）说了一句： 这鬼子他娘的说啥呢？');
INSERT INTO `tpshop_joke` VALUES ('771', '‍‍‍‍我：“美女，你无论智慧、身材、相貌、品德都已经达到完美的境界了，可惜还差一点。” 美女：“差哪一点？” 我：“就差一个像我这样的男朋友。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('772', '前几天和朋友吃饭时认识一妹子，长得挺漂亮，和她也聊的来，互留了电话后，她每天都给我打电话，我知道她的一片心意，但是鉴于我自身的条件，我不得不拒绝她。。。因为我实在是没有多余的钱买她推.销的保险……');
INSERT INTO `tpshop_joke` VALUES ('773', '分手的那天，女友说：“其实我也想哭，但现实叫我不能哭。”\r\n我当时脑袋一热，微笑着说：“你是怕妆掉了吧！”');
INSERT INTO `tpshop_joke` VALUES ('774', '我：“医生我病了，每天打打杀杀的，伴有严重暴利倾向，而且经常睡了人家姑娘还记不住她是谁，这……还有的治吗？”医生：“说人话。”我：“每天都做梦。。。”医生。。。');
INSERT INTO `tpshop_joke` VALUES ('775', '今天接到一个推.销的电话，对方说：“您好，先生，能打扰你一分钟的时间么？”我说：“不行！至少十分钟，已经很久没人给我打电话了。”');
INSERT INTO `tpshop_joke` VALUES ('776', '据说公司附近晚上偶有女色狼出没！哥今天加班到九点，现在担心死了！要如何做才能让她顺利得逞呢？！');
INSERT INTO `tpshop_joke` VALUES ('777', '一天，我搬了张滕椅在阳台晒太阳，小说里不都是这样写的吗？惬意的阳光洒在我的身上，我慵懒的躺在滕椅上，像王子一般漏出迷人的微笑。于是我让老妈帮我拍张照片，谁知老妈说：有啥好拍的？跟晒咸肉似的！');
INSERT INTO `tpshop_joke` VALUES ('778', '妈妈：“傻闺女，你为什么这么疯狂的迷恋小四！”女儿：“妈妈，你知道的，我一米六，小四才一米四！”妈妈：“你是说，距离产生美！”');
INSERT INTO `tpshop_joke` VALUES ('779', '吃饭吃到一半，老妈把碗一摔冲我骂道：“三十好几的人了，天天不务正业，你说你为这家做过什么贡献！”我低头不语，有泪，从脸庞滑过。。。为了化解这种尴尬的局面，老爸在旁轻轻唱道：“老人不图儿女为家作多大贡献~啊，一辈子……”老妈猛得一巴掌呼到老爸脸上：“说谁老呢！”');
INSERT INTO `tpshop_joke` VALUES ('780', '就是本人打工期间，一同事，晚上经常梦游说梦话，他本人是不知道的，那天和他说完，不信，还老说晚上说梦话的时候录下来。晚上，正准备开启录音，他（睡着的了）突然在床上坐了起来，双手像僵尸那样的掐状，恶狠狠的说道：我TM掐死你，敢说我晚上梦游说梦话。。。我能说，我吓到不敢录了吗，日有所思，夜有所梦，吓到后来直接转工作了!!!');
INSERT INTO `tpshop_joke` VALUES ('781', '大家交朋友的时候一定要慎重，尽量多交一些酒品好的朋友，昨天一个哥们喝多了竟然给我打电话说暗恋我很久了！卧槽，原来他是一个同、性、恋！最可气的是，第二天他把这事给忘了！害我白高兴了一个晚上！！');
INSERT INTO `tpshop_joke` VALUES ('782', '跟喜欢的女孩还有她男朋友一起吃饭。饭后去唱K。喝了几轮下来都有点晕。她男朋友把她叫了出去。我看半天没回来就跟着出去了“为什么？”女孩哭着说。男的说“玩够了OK？以后别再给我打………”话还没说完，我的拳头已经落在他脸上了。那件事后女孩跟我在一起了，一年后我们结婚了。婚礼那天老婆对我说“谢谢你那天那一拳”我没说话摸摸她的头。婚后我们很幸福。半年后老婆怀孕了。做完检查老婆脸上满是幸福。我让她先回去，说自己还有事。然后转头回到医院，走进重症监护室的一张床边坐下。不是别人，正是老婆的前男友，他面色惨白看到我来了冲我笑笑，刚开口，我说“放心吧，她很好”“谢谢……”“是我该谢谢你”“替我好好照顾……”然后就闭眼了。他是我这辈子最好的兄弟。我只想说，一个男人的爱，放弃比坚持更难！');
INSERT INTO `tpshop_joke` VALUES ('783', '气象局通告：由于台风登陆沿海地带，所有移动、联通用户这几天请保持关机状态，以免巨大台风把信号吹到大洋彼岸，造成不必要漫游费用。她md，，怪不得刚充话费又欠了');
INSERT INTO `tpshop_joke` VALUES ('784', '老王与大师行至山脚处，老王问大师怎么才能做到泰山崩于前而面不改色？大师拔腿就跑。老王说：大师你的意思是只要锻炼的跑的快，砸不到，就能无所畏惧的站在山下了吗？大师喊到：你TM瞎啊，山真塌了。');
INSERT INTO `tpshop_joke` VALUES ('785', '新郎新娘在洞房，正打算行房事的时候，由于光线有点暗，新娘就问：“要不要点蜡烛啊。” 新郎被吓到了，说：“才第一天，不要玩那么大吧。');
INSERT INTO `tpshop_joke` VALUES ('786', '　　以前当幼师的时候有一个超级大胸美女同事…… 　　有次跟她一起逛街路过一家内衣店就进去看看，导购很是热情的给我介绍各种漂亮的内衣，给她介绍的都是那种哺乳期穿的超大号内衣（样式都不好看） 　　她悠悠的叹了口气说:这么美丽的大胸却一直找不到漂亮的内衣来装，真是可惜了！！');
INSERT INTO `tpshop_joke` VALUES ('787', '某次坐公交，司机师傅一个急刹。导致某男士没来的急反应一下拥向前面的女士，这下女士恼了转过来就对男士呵斥道，看你这副德行。男士一看旁边人都盯着他看，顺口就这来了句这不是德行。。这是惯性。');
INSERT INTO `tpshop_joke` VALUES ('788', '吕布和貂蝉静静地漫步在公园。貂蝉然后一个踉跄踩到了草坪上，吕布大吃一惊说：“草草，你踩到草草了。”话音刚落，突然白光一闪曹操出现了。曹操暴喝一声：“谁在叫我，还叫的这么恶心。');
INSERT INTO `tpshop_joke` VALUES ('789', '一大爷每天早上都会坐在我家厕所不远处的石头上（厕所在外面的那种，农村的都知道），每天早起上厕所出来，他都会给我打招呼，而且大老远见我都会喊，小伙子吃了没？你说我该怎么回答？');
INSERT INTO `tpshop_joke` VALUES ('790', '我和前任交往了3年，有天他突然提出分手，理由是爱上了别人，我直接气哭了，我爱上别人都没有跟他分手，他怎么可以这样！！！');
INSERT INTO `tpshop_joke` VALUES ('791', '和女朋友异地，上次回家想摸摸胸，这货不肯，想要强上，他妈的竟然给我念起了金刚经，性趣全无。。。');
INSERT INTO `tpshop_joke` VALUES ('792', '‍‍‍‍‍‍我和女票在床上看电视，女票吃着零食弄的床上都是零食末。 我埋怨她说：“能不能别在床上吃东西。” 她白了我一眼：“好吧！我以后绝对不在床上吃东西了。” 我想了会，马上求饶：“老婆，我错了。”‍‍‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('793', '灰太狼的孩子为什么姓小，而红太狼的初恋好像也姓小，仔细想想我好像发现了什么');
INSERT INTO `tpshop_joke` VALUES ('794', '‍‍‍‍今天我买好礼物鲜花，在卡片上写上：“老婆，5201314！” 然后跑到老婆公司门口等她下班送给她！ 伴着她同事们羡慕的眼神，老婆高兴的拥抱我！ 晚上回到家，老婆拿着菜刀架在跪着搓衣板的我的颈上吼道：“说，做了啥对不起老娘的事！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('795', '‍‍‍‍雌鸟泪流满面，雄鸟怒气冲冲地说：“我说了多少次了，我没结婚，这指环是人类给我套上的。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('796', 'A：“为什么你网上购物只上淘宝和天猫，却从不上JD购物？是不是没有账号？”B：“劳资是一个有原则的人！宁愿把钱给马云整容，也不愿给那个京东大叔把妹！”');
INSERT INTO `tpshop_joke` VALUES ('797', '我：“来一份过桥米线，加鹌鹑蛋，金针菇，三克油！”服务员妹子：“帅哥三克豆油还是麻油？豆油是厨师放的我也不知道是几克，麻油的话你自己放。”');
INSERT INTO `tpshop_joke` VALUES ('798', '小偷偷东西，被男友抓到，向我炫耀道：“亲爱的，我厉害不？”我：“贼厉害！”男友一脸黑线。。。');
INSERT INTO `tpshop_joke` VALUES ('799', '一哥们相亲，女的说，看你有点高冷范。哥们说：高没有吧我才一米七，冷倒是有点，今天穿的少。女说：你还有的萌？哥们悦：我不猛，有点瘦。女。。。');
INSERT INTO `tpshop_joke` VALUES ('800', '甲：听说你的签名卖了50元？乙：是啊。甲：你又不是什么名人，怎么签名能卖那么多钱。乙：我在一张100元上签名，然后以50的价格卖出去了。甲：...');
INSERT INTO `tpshop_joke` VALUES ('801', '男同胞的心理是不是都有这样的情节，别人家女孩子无论哪个季节穿的越暴露越好，而自己的女朋友哪怕是夏天也要给我裹的严严实实......');
INSERT INTO `tpshop_joke` VALUES ('802', '王老师外出度假，临走前担心家中被盗，便在客厅桌上放了一百块钱并给小偷留纸条：您别费神了，我们家我都找不到钱，这一百块钱给您零花。隔壁家是铁道部的，有钱而且还不敢报案。等度假回来，王老师发现桌子上钱变成了二十万！在他留给小偷的纸条的反面写着：给您的信息费，请笑纳!');
INSERT INTO `tpshop_joke` VALUES ('803', '‍‍期末考试结束后回家。 爸爸：“考的咋样？” 我：“有人数学好，有人英语好，有人语文好。” 爸爸：“那你呢？” 我：“心态好。” 爸爸：“站那别跑！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('804', '下班回家，刚进门就看到老爸将老妈最心爱的兰花连盆打碎了，刚想好好戏弄威胁一下。老爸突然对我说:儿子你赶快躲起来，趁你妈还没下班，要不然等她回来了，你也遭殃。虽然是我打碎的但是你老妈的脾气你又不是不知道，边说边把我推进我的房间...瞬间觉得老爸好伟大啊！ 片刻之后，就听老妈熟悉的声音，紧接着老爸狠狠地向我房间吼道：小兔崽子快点给劳资滚出来，你自己跟你妈解释解释，逃避是没有任何作用的……');
INSERT INTO `tpshop_joke` VALUES ('805', '‍‍两个妹子聊天。 A妹子愁眉苦展：“情人节就要到了啊！奴家怎么就是收不到巧克力啊？” B妹子说：“你觉得呢？” A怒道：“你就不要补刀了，不就是没有男朋友嘛！” B微微一笑：“NO，是因为狗吃巧克力，会死的！”');
INSERT INTO `tpshop_joke` VALUES ('806', '借了朋友一把伞一直没还，今天下雨。发信息给朋友说，下雨了，快来救你的伞！朋友以为我把她的伞弄坏了，问，咋弄坏的？我说，下雨了，我在打它！');
INSERT INTO `tpshop_joke` VALUES ('807', '儿子回家后兴冲冲地告诉爸爸：”老师说，一个孩子吃河马的奶，一个月内体重增加20磅。“爸爸厉声说：”胡说八道，哪有这回事！是谁的孩子？“”就是河马的孩子呀。“儿子认真的回答');
INSERT INTO `tpshop_joke` VALUES ('808', '‍‍妻子：“为啥要娶我呢？” 丈夫：“你好看啊！” 妻子：“为啥好看啊？” 丈夫：“苗条。” 妻子：“为啥苗条？” 丈夫：“你做菜太难吃，你就吃得少。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('809', '‍‍老外A：“我太傻了，头次吃饺子居然剥皮。” 老外B：“你算好的了，我头吃还吐核呢。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('810', '　　楼主女，和我老公是大学同学，结婚两年多了，因为房贷车贷压力比较重所以一直都还没生宝宝，大学同学大部分都生宝宝了，有一次同学聚会一男同学坏笑的问我老公还不生宝宝，是不是不行啊？我笑着说：不是我老公不行，是我不行，要不把你老婆借我老公使使？我那同学脸都绿了！匿了～');
INSERT INTO `tpshop_joke` VALUES ('811', '雨一直下，站在站台下的我心情和天气一样阴暗低落，独自在城市打工的我看透了人间的冷漠…… 我沉浸在自己的思绪中，任凭一辆辆公交车驶过…… “先生，有没有1元钱？”一个乞讨者出现在我面前，轻声问我。 我没好气的说：“没有！” 他微笑的从瓷碗里拿出1块钱递给我：“看你等了好久没上车，估计忘带钱了，雨一时停不了，赶紧回家吧！” ……');
INSERT INTO `tpshop_joke` VALUES ('812', '　　LZ去一工厂考察，期间工厂美女总栽作陪，中午共进午餐时，大家交谈甚欢。吃好后美女总栽有事要先走，与我们一一握手道别。到我时，我发现她衣服正胸前有很多米饭，我就善意提醒，美女，衣服上有东西。她低头一看胸前说：没吃饱！没吃饱？没吃饱。。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('813', '有次去车站接客户经理，意外发现还带了一个美女来，美女拖一行李箱跟在经理后面，客户经理介绍：财务小张。一路闲聊，到了宾馆，我果断开了一个大套间，无人反对！ 回去路上，司机说：“老板，你应该开2个房间，假如搞错，不是很尴尬？” 我：“笨，2个人出差1个行李箱，那还会错？！”');
INSERT INTO `tpshop_joke` VALUES ('814', '女汉子老婆风风火火回到家，见面就嚷:“老公，刚才坐公交遇上小偷了，从我身边一过我就发现手机没了，丫的下车还想跑，我追出一站地，逮住就是一顿削，那小子死活不承认。翻遍全身也没找到我的诺基亚，小偷求我说:大姐，你随便挑一个吧。我就拿了一个苹果5回来了。”我:“老婆，你出门忘带手机，你那诺基亚在梳妆台上呢”。');
INSERT INTO `tpshop_joke` VALUES ('815', '　　一次上体育课，老师来晚了，体育委员管着我们，他大声喊：“向前看！”劳资赶紧从口袋里掏出钱，然后眼睛就盯着手里的钱，由于站在前面，体育委员一眼就看到了我，然后他说：“某某某，你干嘛呢？”我嬉皮笑脸的说：“你不是让我们向钱看吗？”');
INSERT INTO `tpshop_joke` VALUES ('816', '：妈妈我不要跟爸爸姓程了，：为什么呢？…每次大家叫我程管都笑话我，：那你跟妈妈姓吧！姓鲁，：额，那还是跟爸爸姓吧！');
INSERT INTO `tpshop_joke` VALUES ('817', '老师刚罚完一名不守纪律的学生，然后教导我们：“你们要明白，惩罚不是目的......”后排阴暗角落传来一句：“是手段！”');
INSERT INTO `tpshop_joke` VALUES ('818', '　　今天朋友生日，可能烧烤吃多了，菊花要凸头了，刚好朋友厕所已满，就和几个朋友去竹林里解决。 拉出来了！突然朋友手机响了“我想要怒放的生命……” 感觉不会再听汪峰的歌了。');
INSERT INTO `tpshop_joke` VALUES ('819', '“老婆，我爱你爱的好深。” 　　“哎，老公你别说了，其实里面最少还有一半是你从来没有爱过的。”');
INSERT INTO `tpshop_joke` VALUES ('820', '今天公司女职员三五成群聊天，我路过此为背景。听她们说什么B照，C照的，我无意来了一句女人要什么B照，C照开开小车就开以了，我也C照，尼玛引的哄堂大笑，其中一个女职员扯着另一个女职员的胸罩带子说这个C罩也能开车？');
INSERT INTO `tpshop_joke` VALUES ('821', '　　刚刚朋友来找我:“哥，我要跟我老婆离婚”。 　　“为啥呀？你们不是挺好的吗？”我说。 　　“昨晚我回家给她讲了一个笑话，她没笑。”朋友郁闷的说。 　　“擦，这就是你要离婚的理由？” 　　“不是，她没笑没关系，但是，她床下的男人笑了……” 　　我：“………”');
INSERT INTO `tpshop_joke` VALUES ('822', '老公要去上班，老婆问:下班吃什么？老公笑着说:吃你！下午老公下班后就看到老婆穿的很少的围着餐桌跑步，不解的问:你在干嘛啊？老婆说:我在热菜…热菜');
INSERT INTO `tpshop_joke` VALUES ('823', '都说自己的工资卡等等被老婆没收的，其实这已经算是很不错的了。你们有没有想过自己 和老婆在同一家公司上班，而老婆在财务部工作的？尼玛，工资还没发就知道总共有多少 钱了。。。');
INSERT INTO `tpshop_joke` VALUES ('824', '‍‍‍‍现在的人喜欢苹果的，就叫果粉。 喜欢小米的，叫米粉。 我想知道自己叫什么粉，努力想下，我应该叫奶粉吧。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('825', '说个本人上初中真事，本人考初中，有一个同学做我旁边考试，我本想抄他的答案，却看他在写诗，后来考完试后，老师拿出卷子，卷子上写，孙子出题难，儿子监堂严，老子我不会，白花卷子钱。');
INSERT INTO `tpshop_joke` VALUES ('826', '‍‍新兵：“班长，我觉得不该惩罚我。我手榴弹已经扔出去啦，也爆炸啦，训练任务完成啦！” 班长：“你扔哪里啦！排长现在还在抢救呢！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('827', '合租的两个女孩，上厕所总是喜欢踩在座便器上，每次都留下很黑的鞋印！我跟老公诉苦，老公说：你在座便器上涂点洗洁精就好了。 第二天，发现那两个女孩走路都是岔着走的！');
INSERT INTO `tpshop_joke` VALUES ('828', '‍‍“老王你上厕所用什么那只手擦屁股啊！” “我想想啊！嗯，是左手，你呢？” “我一般用卫生纸！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('829', '‍‍‍‍今天帮老妈摘豆角，要把豆角的丝摘出来。 老妈说：“老的豆角才要摘，嫩的就不用了。” 说着拿起一条来：“你看这条那么嫩，它抽不出丝的。” 但是她用手一摘，摘出了一串丝，呆了。 老妈：“它装嫩。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('830', '我一个大男人，饿了给你煮饭，冷了给你织毛衣，病了彻夜陪你，晚上还任你取暖。我为了你还不结婚，没有女人这十多年都过来了。现在你结婚了，就要搬出去了？就要丢开老子了？你想得美，没门！我后半辈子！你必须负责！”---------“爸，别闹了...”');
INSERT INTO `tpshop_joke` VALUES ('831', '人与狗打架\r\n人赢了：比狗还狠毒\r\n人输了：连狗都不如\r\n打平了：跟狗一样');
INSERT INTO `tpshop_joke` VALUES ('832', '女秘书因工作出色，在老板的撮合下，她和一名能干的职员结了婚。初夜......\r\n新郎：小声点儿，别人听到了多难为情！\r\n新娘：你说话怎么和老板一样呀！');
INSERT INTO `tpshop_joke` VALUES ('833', '结婚三个月后，新娘子向朋友哭诉。\r\n新娘：我真是绝望了，他连吻都不吻我。\r\n朋友：太不象话了，赶快跟你老公离婚。\r\n新娘：不行啊！\r\n朋友：怎么，不能离吗？\r\n新娘：他不是我老公。');
INSERT INTO `tpshop_joke` VALUES ('834', '两个女职员午饭时聊天。\r\nA：新来的董事长真帅，衣服穿得也得体。\r\nB：可不是，衣服穿得还快呢。');
INSERT INTO `tpshop_joke` VALUES ('835', '一同性恋者去公共洗澡堂，洗澡堂里聚集了黑、黄、白种人，好不热闹。\r\n同性恋者感慨地说：“好哇，自助餐！”');
INSERT INTO `tpshop_joke` VALUES ('836', '食人族走进日本男女混浴池时，高兴地道：“好，二米饭有营养又好吃！”');
INSERT INTO `tpshop_joke` VALUES ('837', '儿子：那架飞机也能吃吗？\r\n父亲：飞机跟龙虾差不多，得把皮儿拨掉，吃里面的肉。');
INSERT INTO `tpshop_joke` VALUES ('838', '法国世界杯足球如火如荼之时，食人族来到足球场看到人满为患的情景，\r\n禁不住高呼：“今年大丰收啊！”');
INSERT INTO `tpshop_joke` VALUES ('839', '猎人手持猎 枪对准了头顶的一只鸟，这时，鸟拉了一泡屎正好落在他的\r\n脸上，猎人气愤地骂道：“你出来也不穿裤衩？”鸟讥讽道：“难道猎\r\n人拉屎还穿裤衩？”');
INSERT INTO `tpshop_joke` VALUES ('840', '被猫赶进死胡同的老鼠急中生智，面对着猫突然摇摇晃晃，口中念道：\r\n“哎哟！困死我啦！”猫对老鼠的举动非常惊讶，便问：“怎么了？”\r\n老鼠恳求道：“我肯定是吃了老鼠药，我难受死了，快把我吃了吧！”');
INSERT INTO `tpshop_joke` VALUES ('841', '　　叫兽:“童鞋们，为了更好的让你们了解一下青蛙，下面我们学习一下这两坨浸泡过福尔马琳的已经解剖好的青蛙……” 　　王小贱:“老师，我有一个问题，现在已经变异出了三明治青蛙么？” 　　叫兽:‘’现在还没有发现……” 　　王小贱:“那你特么拿两块面包来，这是在考验我读书少么？虽然我经常旷课……” 　　“额，擦擦擦，我特么刚才吃了啥……”');
INSERT INTO `tpshop_joke` VALUES ('842', '老婆坐在我腿上说:“我是不是很重？”，我:“没感觉到”，老婆高兴的说:“看来我是真的轻了”…………我:“不是，你坐上来的时候，我腿麻了”');
INSERT INTO `tpshop_joke` VALUES ('843', '去吃自助餐，知道吃不完是需要多付钱的，所以吃的饱饱的就要离开，服务员拦住我，说有有饮料没喝完，需要多交十块钱，其实只剩下一口了，我就问他有没有纸和笔，他说有，你写下来投诉我也没用，然后我就拿起饮料喝进嘴里，写下一句话：我含着离开，出去吐出来需要钱吗？');
INSERT INTO `tpshop_joke` VALUES ('844', '一哥们给我打电话，说要离婚。我问咋回事，哥们说他一直怀疑他媳妇和楼下的理发师关系暧昧。今天他去楼下理发，理发师问剪个什么样的发型，哥们说剪个我老婆喜欢的发型，半小时后，哥们一看和理发师的发型一模一样…');
INSERT INTO `tpshop_joke` VALUES ('845', '　　毕业后到360公司去面试，找不到路，打电话给面试官，面试官说：“你用手机地图查找一下。”我立马回道：“我手机好像中毒了，有些软件打不开，你等一下，我用手机管家杀一下毒。”电话那边传来：“我们这边人招满了，你不用来了”');
INSERT INTO `tpshop_joke` VALUES ('846', '有位医科大学学生正愁毕业后做哪个科目，在一次同行的聚餐上学生向他们探讨现在做医生哪个科目比较挣钱！有人说耳科，有人说外科，有人说妇科，这时一位德高望重的老医生站起来狠狠的拍了一下桌子道:屁！眼科！学生晃然大悟，立马报了肛肠科！');
INSERT INTO `tpshop_joke` VALUES ('847', '每天下班回家，都会看见双胞胎姐妹老婆躺在床上等我，心中一阵感动。没办法，哪个男人不希望自己有个三妻四妾，颤颤抖抖的对她们说“老婆，我又爱上了一个人，我把她带回来行么？”看着她们红扑扑的脸蛋，害羞的没有说话，我知道她们已经默认了！可以放心了，好啦，吃完馒头就给你们三妹充气');
INSERT INTO `tpshop_joke` VALUES ('848', '上学的时候总喜欢把同学手机里面的自己的名字改成他爸，然后偷偷的打个电话给他，总会听到一句“爸”。');
INSERT INTO `tpshop_joke` VALUES ('849', '自己动手给手机贴膜，贴完之后气泡有点多，拿给室友看，室友的一句话反映我高超的贴膜水平：拿走拿走，我有秘籍恐惧症！');
INSERT INTO `tpshop_joke` VALUES ('850', '听说女人喜欢男人说“随便刷”于是等到吃完饭，我也对他说“随便刷！”媳妇把破抹布一把刷在我脸上。哄媳妇开心怎么就这么难呢');
INSERT INTO `tpshop_joke` VALUES ('851', '1.昨晚老弟陪我去相亲，对方是个留着点小胡子的帅哥，第一印象挺好的。回到家我弟就拉着我的手跟我说:“姐，这个不行，他有胡子”我不解问到:“有胡子咋啦？挺帅的。”他满是哀怨的语气幽幽说了句:“有胡子扎得大腿好疼，真的。”我现在越想越不对。\r\n2.小时候淘气，把小路两旁的草绑起来，有人走过就会不慎跌倒，有天看到路边有堆牛粪，算好距离大概摔倒脸刚好能摔在牛粪上，把草绑好就回家了，可是。。。我真的没想到。。。那天晚上，我老爸满嘴牛粪的回来了。这就是我隐藏了十八年的秘密。。。\r\n3.朋友圈九大派：①广告刷屏派；②心灵鸡汤励志派；③集满点赞送礼派；④偏方秘方养生派；⑤45度嘟嘴自拍党；⑥新晋父母晒娃派；⑦亲友海外代购派；⑧吃喝拉撒展示派；⑨孤单寂寞文艺派。。。你是什么派？\r\n4.男朋友送我一金手链30多克呢，今天带单位炫耀可上班不许戴首饰就直接放裤兜里，上厕所不小心掉马桶里了，幸亏同事聪明让我用吸铁石栓根绳子把金手链给吸上来了，不然我心疼死\r\n5.昨天打摩的出去：师傅，到那xx多少钱。“七块”“以前不都是六块么，怎么你多要一块”“哎呀小伙子，看你这么帅多收你一块钱怎么了”！ 他说的好有道理，我竟然无言以对！\r\n6.在路上捡到一个神灯，擦了擦后突然灯神出来了：“你有什么愿望，尽管说出来吧。”我想了想激动地说：“我想要永远花不完的钱。”灯神点点头说：“挺不错的愿望，祝你早日能实现，886！” 说完灯神嘭地一声消失了。\r\n7.“男人，脸上一定要保持干爽，别一到了夏天就整天汗哒哒的，只有保持干爽，做事成功率也会提高。我就是这么一个非常注重面部干爽的人。”警察：“这就是你掀人家姑娘裙子擦汗的理由？”\r\n8.有个妹子发表说说：好想有个卧室，睡在窗子旁边，每天阳光照射在脸上，睡到自然醒。下面三屌丝立马回复 “大家好，我叫窗子。” “我叫阳光照，请多多包涵。” “人们都叫我自然醒，你们好。”然后一堆贱人狂赞……\r\n9.念高中时学校的保安大叔对他特别好，每次他跑出去上网深夜回来翻铁门，保安大叔都会及时出现助他两臂之力，托住他屁股不让他下来。\r\n10.今天我走在街上，对面过来一老奶奶，当我跟她擦肩而过的时候，她一把就抓住了我的胳膊，慢慢的往下躺，我当时吓坏了。刹那间我问自己：我的银行卡里还有多少钱？够不够让老奶奶讹我一回？过了二十秒，老奶奶自个又站起来了，拍了拍身上的土说：“小伙子，够刺激吧，人生到处是惊喜！”说完就走了。');
INSERT INTO `tpshop_joke` VALUES ('852', '同事为了减肥于是每天坚持骑马，功夫不负有心人，这一个月下来 马瘦了四十多斤……');
INSERT INTO `tpshop_joke` VALUES ('853', '作为一个文艺工作者，不能再颓废下去，是时候写点东西了！ “中小学寒假作业代笔，价格好商量。” 作文1000字，五元，超过按100字，五毛钱计算。 函数，开根号，多元次方程，一题一元。看题目难度适当加钱。 如果有需要作文辅导得，一个小时十元。 以上情况都有本人得手写发票，小本经营，诚实守信，有需要请拨打13636100437.另外，萝莉，美女8.8折');
INSERT INTO `tpshop_joke` VALUES ('854', '和女朋友一起同居也好久了，那天女朋友洗碗时打碎了一只碗，我赶紧安慰她说：“岁岁平安吗。” 没想到她却埋怨起我来说：“都怪你！” 我就纳闷了，心里很是不平衡。于是我说：“你打碎的，怎么能怪我？” 没想到她能这样说，“哼，这要是你来洗，不就不会打碎了吗？” 真是歪说歪有理吗？简直是岂有此理？');
INSERT INTO `tpshop_joke` VALUES ('855', '二货：为什么我的脸长得这么大，身子长得这么宽？ 二逼：因为你是被父母拉扯大的。');
INSERT INTO `tpshop_joke` VALUES ('856', '结巴的小妖捡了一个神灯，灯神：你有什么愿望吗？小妖：我…我要…我我要。 半个小时后衣冠不整遍体鳞伤的小妖：我…我他妈要钱！');
INSERT INTO `tpshop_joke` VALUES ('857', '报了个驾校我坐在后面一妹子坐前面试车，启动完问怎么车不走了，教练说档啊，妹子楞了半天党在北京啊……下去，我让你在北京');
INSERT INTO `tpshop_joke` VALUES ('858', 'A:我说你这人脾气怎么就这么倔，家务活不会，饭也不做，你说我娶到你这种人真是倒了八辈子霉。 “哎我说哥们你对着你的右手嘀咕什么呢？”');
INSERT INTO `tpshop_joke` VALUES ('859', '小五在火车上看到站台上有卖面包的，就拿出两块钱对火车下的一个小男孩说：“帮叔叔买两个面包，其中一个给你吃。” 不一会儿，小男孩嘴里嚼着面包跑回，递回小王一块钱说：“卖面包那就只有一个面包了。”');
INSERT INTO `tpshop_joke` VALUES ('860', '公司一女同事腿稍微有点短，有一次穿了一双新买的长筒皮靴，她问我：亲，看看好看不？ 我说：不错，皮裤很洋气。');
INSERT INTO `tpshop_joke` VALUES ('861', '儿子考试没及格，被老妈痛骂，完事后，儿子忧怨的对爸爸说:“你为何要娶她？” 老爹也哀怨的说“还不是因为你！”');
INSERT INTO `tpshop_joke` VALUES ('862', '中学生时代谈了个女朋友，那时特么单纯，以为亲亲嘴就会怀孕，也就一直忍着没亲上去，到后来实在忍不住了亲了一口，没想到真的怀孕了，唉，冲动是魔鬼啊！');
INSERT INTO `tpshop_joke` VALUES ('863', '‍‍朋友们聊天，其中一姓李的问：“我老婆生了个女儿，叫啥好呢？” 一人答曰：“就叫李雅梅吧！这样你老婆就可以喊你雅梅爹了。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('864', '新年回家天气突然变冷，又没带厚衣服回来。 妈：“穿我的衣服，又漂亮又暖和” 我：“不要，我们有年龄差距” 妈：“这个你不用担心，那是我年轻时候穿的” 我：“-_-！”无言以对..');
INSERT INTO `tpshop_joke` VALUES ('865', '一年前我和女票闹分手，我们虽然吵的很凶，但我知道我们不能缺失彼此，于是我决定用硬币决定分和，我把硬币抛起，抓在手心，用0.1秒的速度扔向下水道，把她挤在墙上，一场激烈的kiss之后，我说我不能没有你，她低着头嗯了一声，回家的路上突然想起了什么，抓住我领子说:你哪来的硬币？回家睡沙发！');
INSERT INTO `tpshop_joke` VALUES ('866', '‍‍老师：“小明，知道啥叫热带鱼么？” 小明：“知道，不就是煮热了的带鱼？俺不爱吃煮的，俺爱吃油炸的。” 老师：“……”‍‍');
INSERT INTO `tpshop_joke` VALUES ('867', '一考生迟到31分，被监考拦着不让进。场内考生看不下去，喊“现在才9点29”；另一考生附合“对，还没迟到半小时”；众多考生把自己手机调成9：29给老师看。监考老师感动得热泪盈眶，然后哭着把同学的手机都收了，记零分');
INSERT INTO `tpshop_joke` VALUES ('868', '朋友发朋友圈：男人不到30，就没有魅力。 下面一个妹纸评论到：你说的是“秒”还是“分钟”？');
INSERT INTO `tpshop_joke` VALUES ('869', '今天拿着一张五十元假钱去玩具店 看中了一架飞机 掏钱时我就把50元，递给了收银员 收银员说 这钱是假的。当时我就不乐意了 说的跟你飞机是真的似的 不要拉倒');
INSERT INTO `tpshop_joke` VALUES ('870', '‍‍小明：“老师，今天你生日吗？” 老师：“是的，你要送老师礼物吗？” 小明：“对啊！我多想送你一套祖传的染色体！” 老师：“滚…”‍‍');
INSERT INTO `tpshop_joke` VALUES ('871', '看着我妈刚打扫完家里又准备洗碗，我爸不禁心疼的说道:“老婆，你去休息吧，剩下的我来。” 只见他转过身挽起袖子，便对我大喝道:“快去把碗洗了，不然老子揍死你。”');
INSERT INTO `tpshop_joke` VALUES ('872', '‍‍甲：“我终于发现长的丑是种病啊！” 乙：“何出此言？” 甲：“不然整形医院为什么会叫医院。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('873', '‍‍老婆：“亲爱滴，你这辈子有木有当过官儿？” 老公：“当过啊！” 老婆：“当过嘛官儿？” 老公：“副队长！” 老婆：“你们队一共几个银？” 老公：“俩！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('874', '　　走在路上一小孩放鞭吓了我一跳，我看小孩长得可爱就想逗逗他，故意做凶神恶煞状：你个熊孩子，你给老娘过来看老娘不打扁你！突然角落里窜出一男的大声喊：被敌人发现了，宝宝你先撤，我来掩护！说完冲我一个劲儿扔鞭炮，边扔边跑……，-_-#');
INSERT INTO `tpshop_joke` VALUES ('875', '第一天，小白兔去钓鱼，一无所获。第二天，它又去钓鱼，还是如此。第三天它刚到，一条大鱼从河里跳出来，大叫：你要是再敢用胡萝卜当鱼饵，我就扁死你。 你给的都是你自己“想”给的，而不是对方想要的，活在自己世界里的付出，不值钱。');
INSERT INTO `tpshop_joke` VALUES ('876', '老师：为什么我们对父母的称呼是爹妈，可是人家香港人都称父母为爹滴、妈咪呢？ 小明：你懂什么，这说明人家香港人孝顺、感恩、开放呀。 老师：何以见得？ 小明：笨，你想呀，若没有爹的弟，他们会生出来吗！若没有妈的咪！他们能长这么大吗……');
INSERT INTO `tpshop_joke` VALUES ('877', '‍‍早上3岁女儿推门进来，就门就大声喊：“粑粑嘛嘛快起来，我一大早就起来了，你们怎么还不起来？” 宝贝接着又说：“我还有很重要的是要做。” 我说：“什么事啊？” 她说：“我要去看看球球的绳子有没有拴拴好！不然它跑出来就不好啦！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('878', '打开收音机，一个温柔的声音传出：\"…如果肤色分红，脸上的绒毛细嫩柔软，那么说明很健康…\" 听到这里，忍不住摸了自己的脸，对镜顾盼，再笑一笑，样子健康可爱。这时，又听播音员 说道：\"好，听众朋友，这次我们的《养猪知识讲座》就到这里……\"');
INSERT INTO `tpshop_joke` VALUES ('879', '这是个简单明了的事：写作文的时候，第一我都在胡说八道些什么，第二就是我还能胡说八道些什么，，，，，都不知道我这些年的作文怎么编出来的...');
INSERT INTO `tpshop_joke` VALUES ('880', '‍‍儿子：“爸，老师让你去一趟学校。” 爸：“你又在学校惹什么事了？” 儿子：“打…打抱不平来着…” 爸：“不愧是我儿子！打抱不平，儿子，做得好！” 老师：“看看你儿子把鲍不平小朋友打的。” 爸：“鲍….鲍不平？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('881', '前几天踩了钉子，到医院打伤风针，打针的护士长的水灵灵的，\r\n昨天又踩了钉了，又到那家医院另一个护给消完毒，\r\n邪了我一眼，又来看我们家志玲妹妹了，用心良苦啊。\r\n我想说，妹啊，你太了解哥我了……');
INSERT INTO `tpshop_joke` VALUES ('882', '“什么玩意儿，一年四季都在呵呵！”\r\n“逗逼呗！”\r\n“什么玩意儿，一年四季都在咳咳！”\r\n“有人添加你为好友，或者是有消息的时候。”');
INSERT INTO `tpshop_joke` VALUES ('883', '今天觉得生命好脆弱，好绝望。不想活了。养了四年的老母鸡死了。我本来是想把它养成鸡精的，那样我们家炒菜永远都会好鲜的，可能嫌我野心太大，结果它就自杀了。 说好要坚持到最后，人和鸡之间一点信任都没了。');
INSERT INTO `tpshop_joke` VALUES ('884', '晚上晚睡习惯了，每天都十一二点才去睡。回我妈家也这样。几天之后我妈受不了了。她跑我跟前对我说；你都丑成这样了还敢晚睡。不知道睡觉美容吗？我都泪奔了问我妈你这么嫌弃我，我是不是你捡来的？我妈说不是啊，要是别人生了这么丑的孩子早掐死了哪还能忍着抱去扔啊。也就我心软就当猴子养了。妈，你别拉我让我把这瓶农药喝了吧。');
INSERT INTO `tpshop_joke` VALUES ('885', '吃苹果的时候如果你不专心，那么便会有半条虫子出来给你个惊喜。血和泪的教训让我明白不管事情重不重要，一定不能一心二用。不说了我去吐会去。');
INSERT INTO `tpshop_joke` VALUES ('886', '吃苹果的时候如果你不专心，那么便会有半条虫子出来给你个惊喜。血和泪的教训让我明白不管事情重不重要，一定不能一心二用。不说了我去吐会去。');
INSERT INTO `tpshop_joke` VALUES ('887', '生物课上女生说，身体不适，老师问怎么了。旁边一个说可能来大姨妈了。另一个男生也在那里不舒服，旁边的不假思索，他大姨夫来了！');
INSERT INTO `tpshop_joke` VALUES ('888', '1、第一次烧鸡爪，食谱说要放八角，实在找不到零钱，我就放了1块。别问为什么，有钱，就是任性！2、偶现在是喝酸奶只舔盖，吃泡面只喝汤，手机贴完膜就送人，把薯片拿出来扔了舔手指头，去高档消费场所点完菜不吃东西就自拍，益达一瓶只吃最重的两个！没办法，有钱，任性！3、昨天去找算命大师算了一下，大师说我能活83岁甚至更长。算好后，我是骑电瓶车回家的，骑的那叫一个欢快，也不管什么红绿灯，命长，就是这么任性。4、大街上，遇到一个乞丐把钱施舍给另一个乞丐，我很奇怪，于是问他：“作为乞丐，为什么还要帮助他人？”\r\n他说：“不为什么，我有钱就是任性。”5、刚才菜市场买鱼，草鱼39鲈鱼18，老板娘说39+18 47块，\r\n我楞了一下，果断付钱，都不带讲价的，有钱，任性！');
INSERT INTO `tpshop_joke` VALUES ('889', '我家有只大黄狗，特别色一见到漂亮美眉就摇头摆尾。时间一长朋友都知道了这个秘密，一次两个朋友让我带阿黄出去看门女，刚出门不远，就见它摇着尾巴去追赶前面一位女士，我们也紧随其后。结果是位手里拿着香肠的大妈......');
INSERT INTO `tpshop_joke` VALUES ('890', '今天招工来了个高个子帅哥。美中不足的是眼角下有个疤。\r\n这肯定有故事。问起原因说小时候无聊看家里狗在睡觉。\r\n于是它把狗眼扒开使劲吹气。那狗急了上去一口就给他留了这个纪念。\r\n大伙听了都没直起腰！');
INSERT INTO `tpshop_joke` VALUES ('891', '因为长相问题，总被同事取笑： 你…你…你…太丑啦 直到结婚以后，事情才有了转机。 现在同事们都说：你…你…你…太…太…太丑啦。');
INSERT INTO `tpshop_joke` VALUES ('892', '路过广场，看到一男一女两小学生背着书包坐在路边，面前摆了一堆零食。而小男孩不吃，都是喂给小女孩吃的。突然小男孩对小女孩说：班长，我当小组长的事情就拜托你了。小女孩满嘴食物，说不出话来，只是点点头。');
INSERT INTO `tpshop_joke` VALUES ('893', '“老公你想吃柚子还是想吃芒果？”上了无数次当之后我终于可以下意识的把这句话自动翻译成：“老公你是想去剥柚子还是想去切芒果”了。');
INSERT INTO `tpshop_joke` VALUES ('894', '课堂上，老师对学生说:“中国的男生比女生多三千万，所以现在一定要好好努力要不将来只能打光棍。”角落里的小明幽幽的说了一句:“老师你能确定所有的男生都喜欢女生吗？” 教室顿时沸腾起来！');
INSERT INTO `tpshop_joke` VALUES ('895', '某次月考，试卷题目：写出一本名著和该名著中的名言。某同学在冥思苦想了好久后才想到如下答案：《西游记》和名言:嫂嫂，把嘴张开，俺老孙要出来了。');
INSERT INTO `tpshop_joke` VALUES ('896', '记得第一次租房的时候，房东大哥吧啦吧啦半天表示热情。房东说：“家里的东西你可以随便用。”我也为了表示客气随口赞了一句：“嫂子真漂亮。”哎，大哥，我不是！哎，大哥，你别关门呀！');
INSERT INTO `tpshop_joke` VALUES ('897', '小时候玩小霸王游戏机，爸爸出差时怕我上瘾，就把游戏机锁到密码箱里面（那种三个数字组合密码的）。爸爸出去之后我每周六周日就开始套密码，从001开始套到999都没有套开。最后快放暑假了，我打电话问爸爸密码箱密码，爸爸告诉我是000，我狠狠的抽了自己两个嘴巴。');
INSERT INTO `tpshop_joke` VALUES ('898', '当年网吧通宵归来，又饿又累，晃晃悠悠就跑食堂去吃饭。吃到一半便不饿了，但此时我已睡着。我的脸啊，直接砸在饭盘里，什么胡萝卜、土豆丝等，飞得到处是。这不是GC,GC是边上的一个女生吓坏了啊，以为我挂了啊，当即把饭盘直接扔了，大喊大叫：“天哪，死人啦，这饭菜有毒！”');
INSERT INTO `tpshop_joke` VALUES ('899', '我现在一次性可点5个雪花了，我才十岁，各位哥哥姐姐让我过吧！');
INSERT INTO `tpshop_joke` VALUES ('900', '乡亲们，解放军来了，别跑了，咱们都是一家人！相亲们，别跑了，我们是来救你们的！乡亲们，再跑我可就要开抢了！…“砰砰砰！ 砰砰砰！”');
INSERT INTO `tpshop_joke` VALUES ('901', '有一道题：郑和七下西洋，并死在途中，请问他死在了第几次下西洋途中？年少的我竟然去百度了………');
INSERT INTO `tpshop_joke` VALUES ('902', '小米：老师，放暑假的时候我把上学期的旧课本和学习资料捐献给了山区的贫困儿童，您说我做的对吧。 老师：我看你是交不上暑假作业了吧！');
INSERT INTO `tpshop_joke` VALUES ('903', '里根在他的70岁生日庆宴上说，“今天适逢我39岁生日的第31个\r\n周年纪念。无论哪一年我都过得很愉快。如果你们考虑选择其中的一年让\r\n我来参加宴会的话，我想那也挺好。”');
INSERT INTO `tpshop_joke` VALUES ('904', '里根身为美国总统，执政8年，权倾一时，但是他说：“有人说我是\r\n全世界最有权势的人。可我一点也不信。白宫有一位官员，每天早晨把一\r\n张小纸片放在我办公桌上，纸片上写着每一刻钟我该做的事情，他才是全\r\n世界最有权势的人。”');
INSERT INTO `tpshop_joke` VALUES ('905', '第40位美国总统罗纳德?里根（1911年出生）．在一次外出中，他\r\n和随行人员突然遭到来自路边的枪击，好几个人受了伤，幸运的是里根没\r\n被击中要害，事后，他详细地了解了其他人的伤情，当得知他们都无生命\r\n危险时，他大声对身边的人说：“好消息！快给我们弄几个尿盆来，我们\r\n又可重聚在一起了。”');
INSERT INTO `tpshop_joke` VALUES ('906', '在卡特的飞机降临在饱受旱灾之苦的得克萨斯某镇之前，该镇忽然\r\n下起了雨。卡特踏上滑溜溜的机场跑道，向聚集在那里前来欢迎他的农民\r\n发出微笑。“你们或者要钱或者要雨，”他说；“我拿不出钱，所以我只好\r\n带好了雨。”');
INSERT INTO `tpshop_joke` VALUES ('907', '杰拉尔德?R?福特（1913年出生）是美国第38任总统，他说话喜\r\n欢用双关语。有一次，他回答记者提问时说：“我是一辆福待，不是林肯。\r\n众所周知，林肯既是美国很伟大的总统，又是一种最高级的名牌小汽\r\n车；福特则是当时普通、廉价而大众化的汽车。福特说这句话，一是表示\r\n谦虚，一是为了标榜自己是大众喜欢的总统。');
INSERT INTO `tpshop_joke` VALUES ('908', '约翰逊嘲笑共和党政敌说：\r\n“有一位老人需要做心脏移植手术。有三颗心脏可供选择，一颗是18\r\n岁运动员的，第二颗的19岁舞蹈家的。第三颗是75岁银行家的。这位病\r\n人询问银行家的政见，得知他是一位共和党人。了解这一点后，这位病人\r\n选择了银行家的心脏。\r\n“移植手术相当成功。人们问他为什么他宁愿选择75岁老人的心脏。\r\n而不要充满生气的年轻人心脏，他说：‘我需要一颗我知道从未用过的心\r\n脏’。”');
INSERT INTO `tpshop_joke` VALUES ('909', '约翰逊总统向一群商业界头面人物讲了一则轶事，以说明需要大量资\r\n金同俄国人导弹竞赛。故事如下：\r\n1861年，一位得克萨斯州人离家前去参加南军士兵阵营。\r\n他告诉他的邻居他很快就会回来，这场战争不会费力，“因为我们能\r\n用扫帚柄揍这些北方佬。”两年后，他才重返故里，少了一条腿。他的领\r\n居向这位神情悲惨、衣衫褴褛的伤兵询问到底发生了什么事，“你不是说\r\n过战争不费力，你们能用扫帚柄揍这些北方佬吗？”\r\n“我们当然能，”这位南军士兵回答，“但是麻烦在于北方佬不用扫\r\n帚柄打仗。”');
INSERT INTO `tpshop_joke` VALUES ('910', '美国第36位总统林登?贝恩斯?约翰逊（1908--1973年）， \r\n26岁时\r\n被任命为全国青年总署德克萨斯州分署署长。他在任期期间对手下人十\r\n分严格，喜欢讲他们的不是。\r\n一次，他走过一个同事的座位，看到他的办公桌子上堆满了文件，就\r\n故意提高嗓门说：“我希望你的思想不要像这张桌子这样乱七八糟。”这\r\n样，同办公室的人都听得一清二楚。这位同事费了好大的劲，才在约翰逊\r\n第二次巡视办公室前把文件整理好了，并清理了桌面。约翰逊又来到办公\r\n室时，一看原来乱槽槽的桌面变得空空荡荡，于是说：“我希望你的头脑\r\n不要像这张桌子这样空荡荡的。”');
INSERT INTO `tpshop_joke` VALUES ('911', '刚才机场安检遇见一女孩，应是准备和家人去学校报道。原本时间已很紧张，带的洗发水，化妆品严重毫升数超标不愿意托运与安检人员又哭又闹起了冲突。结果安检姐姐让她把包里面的东西全部拿出来过检，当着她父母的面检查出了，香烟，打火机，杜蕾斯，润滑剂，跳蛋。。。。剩下她父母在风中凌乱。');
INSERT INTO `tpshop_joke` VALUES ('912', '今天去相亲，这次的对象不错。“你有房有车吗？”“有啊”“你喜欢小孩吗？”“还行”“先生，你就没什么要问我的吗？”“有啊。你女儿怎么还没来？”我需要A，V（安慰）');
INSERT INTO `tpshop_joke` VALUES ('913', '检察官: 签个字 。 罪犯: 我不识字 。 检察官: 正是因为你不识字才造成今天的后果 。 罪犯: 是啊，不然我也不会抢了人跑去派出所躲，那牌牌上的字一个不认识 。 检察官: ………');
INSERT INTO `tpshop_joke` VALUES ('914', '　　小王：“看那位年轻的房地产大亨，真是年轻有为，事业有成啊！” 　　小明：“他旁边那个是谁？” 　　小王：“是他老婆。” 　　小明：“嗯。。。我明白了！” 　　小王：“明白什么？” 　　小明：“一个长相难看的妻子可以使她丈夫将全部的精力花在工作上。”');
INSERT INTO `tpshop_joke` VALUES ('915', '朝男盆友要零花钱，他不给，我就在床上翻来覆去的折腾，最后他说，我从没看见过一团肉，可以在床上翻滚的这么自如，给你200，别翻了，床受不了……果断打起来了……');
INSERT INTO `tpshop_joke` VALUES ('916', '唐僧：这八戒天天去泡妞，也不好好取经，你去把他举报了！ 悟空：好勒！ 第二天.... 唐僧：怎么样，好了吗？ 悟空：嘿嘿，还不错，就是有点紧。 唐僧：好像没有什么不对的......');
INSERT INTO `tpshop_joke` VALUES ('917', '就刚才,在街口,有一个穿低腰牛仔的姑凉,弯着腰在系鞋带。屁股都漏出半截来,姨妈巾也出来半截,这不是高潮,高潮是,一条大黄狗跑过去一口咬出血淋淋的姨妈巾飞奔而去!留下一群人目瞪口呆的!都忘了去扶那姑凉!');
INSERT INTO `tpshop_joke` VALUES ('918', '今天班里一个学生抄作业被我发现了。我说，你为什么要抄作业？那学生说了一句让我无法反驳的话，因为我自己不会做！');
INSERT INTO `tpshop_joke` VALUES ('919', '某剧组拍戏，找不着群演，拉来一批当地黑社会凑数。因是夏天戏，得光膀子，带头大哥异常彪悍，好生威武。他一脱衣服，副导演差点昏倒，但见大哥两臂皆是纹身，左胳膊：天生我材必有用！右胳膊：世上只有妈妈好。');
INSERT INTO `tpshop_joke` VALUES ('920', '老娘我简直太有钱了，我该给保姆买辆什么车呢？论坛回复：那就要看她跟你老公发展到什么关系了～');
INSERT INTO `tpshop_joke` VALUES ('921', '记得以前上初中的时候。老师在上面讲课！我在无心学习却又不知道干什么！想来想去还是戴上耳机听歌。但是又怕老师发现要爆骂！于是我就看向了同桌！拍了一下他。然后又用手指了我戴上耳机的耳朵和老师。最后是嘴巴！意思就是让他帮我放风。如果老师发现了就告诉我！做完之后看他点了点头我以为他知道了！我便拿书立起来趴在桌子上安逸的听着歌！可是刚趴下就听到同桌发生的说:“老师我同桌在听歌，叫你讲课小点声” ..........尼玛，当场卧了个操 默默的拿出作业本写检讨');
INSERT INTO `tpshop_joke` VALUES ('922', '‍‍‍‍今天，看儿子在沙发上边吃零食边看电视。 我忍不住说：“儿子啊，你说你都这么大了，也该减减肥了。” 儿子说：“我这是要维持我平衡的体重，身高一米八，体重一百八，就是这么平衡，任性。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('923', '‍‍‍‍试卷发回来了，小明看了，忿忿不平地对老师说：“太不公平了，古人可以写光阴似箭都可以，为什么我写光阴似炮弹就不行？” 老师说：“成语懂吗？四个字的。” 小明说：“我写的是升级版的，老师你OUT了，现在这年代，不都流行升级吗？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('924', '‍‍‍‍吕胜在街上看到一和尚摆摊算卦，他前面的桌上除了占卜工具，还摆着不少香艳书籍。 吕胜思忖许久都无法悟出其中玄机，只好向和尚讨教：“大师，请问您桌子上如此摆设，究竟有何说法。” 和尚白了他一眼，悻悻地说：“生意这么淡，不卖点书挣饭钱，我喝西北风去？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('925', '因为他们是农民，他们只靠种地养不活一大家子人，于是他们进城，你们叫他们农民工。 因为他们没有文化，找不到轻松的工作，便只能去干一些又脏又累又危险的被你们看不起的体力活，用他们一双双手、造起你们一幢幢的办公楼。 他们没有你们的那些保险、他们甚至不知道出了事故要找谁去索赔，那些被你们看不起的农民工，他们才是最伟大的人，他们纯朴,没有你们久居社会的狡诈。 当你们在办公室里吹着空调玩着电脑时,或许他们正在工地楼顶顶着太阳加班,心里还在想着多赚点钱给孩子买几本好的辅导资料、给妻子添几件新衣服... 他们的人格岂能任你们那些无知的人去践踏？ 喜欢老者那句“你根本就不是人！”不是因为你没有人证，而是因为你没有人心。一个人如果丢了做人的心,那么也就不配做人了！');
INSERT INTO `tpshop_joke` VALUES ('926', '有人骂你是傻B，我过去就是一巴掌，这不是废话么');
INSERT INTO `tpshop_joke` VALUES ('927', '‍‍‍‍每次吃饭，老婆都嫌我吃饭慢，边催边说：“锅里的饭还要不要？不吃我喂狗了哦！” 我一边扒拉着饭，一边对老婆说：“要！要！我还没吃饱呢！” 二货媳妇，你捂着嘴笑啥啊！男人就是应该多吃才对，多吃才能身体好的。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('928', '　　老公：亲爱的，我考考你，一个老头只剩下一颗牙，吃东西却塞牙了，这是怎么回事？ 　　老婆：因为他带假牙了吧？ 　　老公：不，因为他吃藕套眼里了。');
INSERT INTO `tpshop_joke` VALUES ('929', '看了近20年的西游记，我一直没弄明白猴子闹天宫的时候 ，老君都打不过猴子。为毛五百年后，老君的一头牛都把猴子打的满山跑，修仙不是年龄越大功力越强麽？');
INSERT INTO `tpshop_joke` VALUES ('930', '‍‍‍‍高中夏天的时候，同学晚上睡觉喜欢不穿衣服睡。 一天晚上，宿舍一同学在宿舍来回跑不知道在干什么，然后被教导主任从门口的窗户里看到了。 顿时来了一句：“同学，裸睡可以，但是裸奔就不行了啊！” 随之宿舍传来一阵憋笑……‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('931', '人分三六九等，肉有五花三层 。没有那些鱼鳖虾蟹，哪有这些花花世界。我捧你时你是个玻璃杯子，我松手时你就是个玻璃茬子。不要把自己想的多么多么牛X，再厉害的香水也干不过韭菜盒子。');
INSERT INTO `tpshop_joke` VALUES ('932', '‍‍美女：“老板，这个鳄鱼皮包真漂亮，只是不知道它防不防水？” 老板：“当然防了，你什么时候看过鳄鱼打伞啊？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('933', 'A：小明，最近有没有做什么坏事啊。B：没有啊。做坏事早晚都是要被抓的。 A：嗯嗯。真不错。 B：我都是中午做。 。。。。。。。。。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('934', '‍‍公公婆婆在一起，经常闹不完的笑话，每次都笑的我肚子疼。 就刚刚，公公在桌子上放了一大杯茶，婆婆就喝了一点。 一会公公过来见茶少了，就问：“谁喝我的茶了。” 婆婆说：“大人我喝的。” 公公说：“就你还大人，最多小妇女一个！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('935', '　　喜欢老板很久了，碍于她的身份不敢表白，今天鼓起勇气冒着被辞退的危险趁她不注意亲了她一下，然后很激动的看着她..... 　　只听见她说：“你偷亲我，你脸红个什么劲呀.....”');
INSERT INTO `tpshop_joke` VALUES ('936', '刚同事说的，说他军训的时候，有个哥们儿点名走神了，没喊到。教官怒了，叫他去墙根下，喊一千遍到！那哥们儿就去了，到，到，到，到。。。。。。duang。。，墙塌了，外边倒车的大货车听他指挥，直接倒进来了，小伙伴们都震惊了。。');
INSERT INTO `tpshop_joke` VALUES ('937', '今天，去姐姐家玩，姐姐家有小正太一个。 我刚坐下小正太就跑过来问我：“小姨你说小心肝是不是有腿呀？” 我问：“为什么这么说？” 小正太回答：“那我天天听爸爸在房间说‘小心肝快把腿张开’” 只见姐姐姐夫超级尴尬中。');
INSERT INTO `tpshop_joke` VALUES ('938', '“嗯，天蝎和巨蟹配。” “我是狮子，我和什么配？” “母狮子。”');
INSERT INTO `tpshop_joke` VALUES ('939', '女幼儿园老师带学生去游泳，女老师穿的是泳装。 这时，一小男孩看见老师的泳裤上有一条黑色的线。 就问老师：“这是什么？” 老师一看尴尬地对那小男孩说：“这是线头。” 然后，就自己忍痛把这线头给拔了。');
INSERT INTO `tpshop_joke` VALUES ('940', '一天，吴宇森、张鑫焱（《少林寺》导演）和王晶在讨论五行和取名。 吴宇森：我五行缺木，所以带个“森”字。 张鑫炎：我五行缺金和火，所以带“鑫”和“炎”字。 王 晶：你们聊，我先走了……');
INSERT INTO `tpshop_joke` VALUES ('941', '你们有谁在听梁静茹的《暖暖》的时候把“安全感”听成“鹌鹑蛋”的？我不信就我一个！');
INSERT INTO `tpshop_joke` VALUES ('942', '去券商开户，券商问：“多大了？”“38”“结婚了么”“结了”“怕老婆不”“怕”“住几楼呀”“21楼”“那您别开了，我们现在只给2楼以下的开！”');
INSERT INTO `tpshop_joke` VALUES ('943', '股市就是《西游记》，各路妖魔神怪齐出想发财，最后不是死了就是被庄家出来收得人财皆空。我就想考一下大家，是谁发布了唐僧肉长生不老的利好消息？');
INSERT INTO `tpshop_joke` VALUES ('944', '一句话震慑全世界！俄罗斯：我要炸叙利亚！朝鲜：我要打韩国！希腊：我要退出欧元区！美国：我要加息了！中国：我要开盘了！');
INSERT INTO `tpshop_joke` VALUES ('945', '租住的地方附近有一物流公司，每天车来车往。我减肥，常常去他们院子里练练器械。昨天晚上，守门的大爷走过来问我：装卸工干不干？一个月能挣七八千。我。。。');
INSERT INTO `tpshop_joke` VALUES ('946', '昨天坐公车，发生了吵架的事情，一男的骂售票员：长这么难看不知道怎么艹的。售票员回：我长怎么难看都是爹妈给的，哪像你街坊四邻凑的。。。');
INSERT INTO `tpshop_joke` VALUES ('947', '“这次召回行动已经开始1个月了，但还没有一辆车到我们这里返修。”汔车修理店的老板对维修师傅说。“这次召回的原因是什么？”“刹车失灵！”');
INSERT INTO `tpshop_joke` VALUES ('948', '小王去吃西餐，服务员给他端上来牛排，他奇怪地问：“这牛排怎么有酒味？”服务员向后退了两步说：“现在呢？”');
INSERT INTO `tpshop_joke` VALUES ('949', '我：“老板，你这水果这么多斑能吃吗？”老板：“选水果就像选老婆一样，有讲究的。”我：“怎么说？”老板：“漂亮的不放心！”。。。说的我竟无语反驳。');
INSERT INTO `tpshop_joke` VALUES ('950', '有人说一边充电一边玩手机，像狗被拴着一样。其实这种说法也不对，狗被拴着，肯定会极力挣扎。而手机党，一般稍微牵动一下眼睛就会瞄一下插座。');
INSERT INTO `tpshop_joke` VALUES ('951', '售票员：你提的是什么东西？ 乘客：酒精. 售票员：NTMD装傻是不？！酒精还敢带上车 滚下去灬 乘客：口误灬灬这是乙醇. 售票员：TMD不早说 赶紧的上来 车就要开了');
INSERT INTO `tpshop_joke` VALUES ('952', '男：昨天在街上突然内急，没带纸，附近又没商店，找了半天才找到一个ATM机，于是我就提了500块搞定， 女：土豪啊，我们做朋友吧！不过你这是侮辱人民币你造吗？ 男：可以啊！不过我取了5次，每次都取100，每次都让机器给我打印一张凭条，你以为呢？ 女：……');
INSERT INTO `tpshop_joke` VALUES ('953', '刚才曾经追过的女神发微信说要3块钱买冰激凌，我直接红包5块，告诉她砍砍价买2吃，过了一会她回了我十块说开玩笑的不用我钱，我当时就有小情绪了，我说我的就是你的，就回了她20。后来她给我40。我就回80心里想这样也好能多多交流还能装逼，然后她又160，我就320。然后就被拉黑了。。。');
INSERT INTO `tpshop_joke` VALUES ('954', '有一天，老小驴问老驴说：为什么人家奶牛吃的都是精饲料，我们却只能吃干草？老驴说人家奶牛是靠胸部工作的，我们只能靠两条破腿照不一样。');
INSERT INTO `tpshop_joke` VALUES ('955', '一天跟好友聊天，好友告诉我；本来我儿子不应该这么早的生出来，我好奇的听着好友接着说，“我媳妇知道怀上了，正纠结要不要时，看电视报，有个她喜欢的大明星要演一部电视剧，一算上演日期，正好在坐月子的时候，于是果断要了这个孩子！');
INSERT INTO `tpshop_joke` VALUES ('956', '小日比出差很晚回酒店，发信息给她老婆道平安：“老婆睡了吗？”小日比老婆：“睡着啦”小日比说：“睡着了还能回短信”小日比老婆：“你老婆真睡着了”');
INSERT INTO `tpshop_joke` VALUES ('957', '我拿考卷来问老师问题，“老师这题应该怎么做？”“滚出去！”“唉，现在的老师怎么都这样子。”我只好满带失望的神情出了女厕所');
INSERT INTO `tpshop_joke` VALUES ('958', '媳妇问我：“你能分清奶牛和黄牛的叫声吗？” 我说：“分不清，你学学。” 媳妇：“奶牛的叫声是哞～” 我：“黄牛呢？” 媳妇：“北京去吗？沈阳去吗？广州去吗？”');
INSERT INTO `tpshop_joke` VALUES ('959', '路上听广播，说“李女士去贵州出差，买了一箱茅台酒，总价值8万块。小心翼翼托运结果还是碎了，航空公司只赔偿了几百块。”司机和我正议论这事儿太倒霉了，突然主持人话锋一转，“茅台需要这么贵吗？真的要到贵州才能买到正宗茅台吗？下面我们请到贵州茅台的总经销商…”');
INSERT INTO `tpshop_joke` VALUES ('960', '父亲把做了错事的儿子叫到面前，儿子害怕地低着头。父亲抚摸着他的脑袋说：孩子，你长大了，我不会再像以前那样因为你犯了错误就骂你....儿子感动地抬起了头，父亲接着说：你应该尝尝挨打的滋味了！');
INSERT INTO `tpshop_joke` VALUES ('961', '花间错，食材尽头的羁绊，肛上开花，凳子的拒绝，灵魂的缺口，闸门上的铁绣。---给痔疮起的新名子');
INSERT INTO `tpshop_joke` VALUES ('962', '‍‍‍‍父女俩开着大奔去旅游，半路上遇匪徒拦截。 危机时刻，女儿对父亲说：“爸爸，快，把身上、车内所有值钱的东西都塞进我的小洞洞里面，这样他们就发现不了啦。” 父亲照办，果然匪徒搜身一无所获，只抢得小车一辆。 看着匪徒驾车绝尘而去，父亲懊悔地说：“唉，早知道这样，我们把你妈妈也带上。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('963', '　　数学课上正在做题： 　　旁边妹子问：约？ 　　我立马就不高兴了：约你MB啊，没看到都TM保留两位小数了.');
INSERT INTO `tpshop_joke` VALUES ('964', '上课的时候,同桌在玩手机. 突然手机响了,他连忙把手机扔我桌子上. 顿时老师和全班40多双眼睛盯着我... 我顿时站起来,拿起手机往地上用力一砸：老师，我再也不玩手机了！');
INSERT INTO `tpshop_joke` VALUES ('965', '‍‍‍‍今天很荣幸的被大师接见了，大师头也不抬：“有什么问题快问啊？” 我：“为什么弥勒佛要袒胸露乳呢？” 大师：“因为天气热嘛。” 我：“那他又为什么光着脚呢？” 大师略一思考，很郑重地回答说：“脚痒的话，可以方便抠脚。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('966', '‍‍‍‍今天早上我去超市买东西，正排队结账呢。 突然听前面一大哥自言自语说：“现在的土豆咋越来越贵了，以后吃土豆都吃不起了。” 我喵了一眼大哥手里的东西，他居然拿的是猕猴桃。 我没忍住就告诉他：“你拿的不是土豆，是猕猴桃。” 谁知大哥来了一句：“我去，我还以为是土豆长毛了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('967', '　　相亲时女滴问我：有房么？ 　　我答：没有！ 　　她一脸不乐意。 　　我就指指她胸，反问：你不也没房？凭啥要求我有啊？ 　　她：TM我没房我明天就能隆俩，你倒是明天盖俩试试。。。');
INSERT INTO `tpshop_joke` VALUES ('968', '听说每个人都会渐渐变成自己最讨厌的那种人，我从现在开始仇富不知道还来不来得及？');
INSERT INTO `tpshop_joke` VALUES ('969', '‍‍‍‍本人女，怀胎八月。 一日老公跑过来问我：“以后孩子取什么名字好？” 我说：“要不就叫漂亮吧。” 老公问“为什么？” 我说：“以后别人喊我时都叫我漂亮的妈妈。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('970', '千真万确的真事，昨天上午逛街，遇到老同学（女）和她女儿，母女倆一样高，着装也相似，……她历来小气……都说女的喜欢别人夸她年轻漂亮，于是我说“好久不见了，这是你女儿？乍一看，还以为是倆姐妹呢！呵呵”可是她却把脸一沉“你是说我女儿显老？还是对她有不该的想法？啊？？？”我……我……我……靠，走了……');
INSERT INTO `tpshop_joke` VALUES ('971', '　　有两个酒肉哥们儿，上夜班时，趁当官不注意，想溜出去喝酒。翻墙出去，墙特高，第一个哥们儿跳过去，没动静…咋样儿…半天应了声，快点……第二哥们，嗖…跳下来…掉进大粪坑…在看第一哥们正扒在地上喘息呢。问，你咋不告诉我呢？第一哥们说，告诉你，你会笑话我的……两人无语了……');
INSERT INTO `tpshop_joke` VALUES ('972', '妹妹今年虚6岁，喜欢奇思妙想。今天吃晚饭时，突然当着大家的面，问妈妈：“我长大了后是男的还是女的呀？”');
INSERT INTO `tpshop_joke` VALUES ('973', '昨天，坐公交上学，看到一个很可爱的小女孩上车，手里抓着一张百元大钞，眨着水灵灵的大眼睛对司机说：“叔叔，我木有零钱。”司机大叔慈祥的笑着说：”过来让我捏下脸。“小女孩胆怯地把脸凑过去，司机大叔用力地捏了一下她的脸蛋，然后就让她下车了。');
INSERT INTO `tpshop_joke` VALUES ('974', '　　前段时间养着一只小猫，蛮可爱的，也很粘人。就刚才，睡午觉的时候她把猫压死了都不知道，还特委屈的和我说，开始她是侧着睡的，后来一翻身就把它压死了。说得我不禁想起以后我们有孩子了睡谁哪边好一点！！！');
INSERT INTO `tpshop_joke` VALUES ('975', '那时候爱上一个人不是因为你有车有房，而是那天下午阳光很好，我穿了一件白衬衫，你竟然一眼就认出是Dior，而且知道那是经典款，就这个款式跟我聊了一个小时，我就爱上了你，因为，你懂我。爱情往往就发生在，我低调显摆的东西，竟然被你高调的识破，于是相见恨晚，从此一发不可收拾');
INSERT INTO `tpshop_joke` VALUES ('976', '男朋友总会问我到底喜欢他哪点，我一直不敢诚实告诉他，是因为他们宿舍好几个帅哥，就男朋友长的最难看，我以为可以放长线钓大鱼，没想到男朋友的几个帅气舍友特么没一个看得上我…');
INSERT INTO `tpshop_joke` VALUES ('977', '我二货闺蜜生病了，她打电话来给我说：亲爱的，我好像发烧了，然后我说：去看医生啊，没想到这货和回复了句：神经病我生病为毛看医生，我应该去医院看病。我也是醉了……');
INSERT INTO `tpshop_joke` VALUES ('978', '妹子要我晚上去她家装电脑，我当时心情就激动了！第二天累死了，妈蛋，她家开了个网吧！');
INSERT INTO `tpshop_joke` VALUES ('979', '　　家里刚买摩托车那会个个都要学，男装的，弟弟教我。。。割。。。那会爷爷还在，养了几头牛，牛粪和尿都有地方放，有两个大缸装的尿，我就骑着那车我弟坐在后而，一挂挡一加油，直奔那地方去，就在离得一米左右的地方还好我俩一起刹住了车，不然后果可想而知，当时挂的五挡，再也不敢碰了，五六年过去了，再也没人敢教我开车，我只想说，那只是一次意外！！！！！！');
INSERT INTO `tpshop_joke` VALUES ('980', '男友约女孩吃饭，到楼下后打女孩电话。女孩刚洗完澡，裹着浴巾走到阳台上，冲着楼下男友一边招手一边喊到：上来呀，先上来玩玩呀，快上来啊，这是一路人甲走过小声说到：咦，又开了一家么……');
INSERT INTO `tpshop_joke` VALUES ('981', '女友iPhone不小心摔坏了 我惊呼：“我上个月的工资！艹！”。女友回头瞅我一眼，，，我：“尼玛我下个月工资。。”');
INSERT INTO `tpshop_joke` VALUES ('982', '问：“怎样的情侣才叫恩爱？” 神回复：“他们去开宾馆，一夜之后都不知道宾馆WIFI密码。”');
INSERT INTO `tpshop_joke` VALUES ('983', '媳妇是个十足的女汉子。 lz今天回国，她来接机。 远远的我就看她狂跑过来，快跑到我面前的时候噗通一声摔了个狗吃屎。 正准备拉她起来的时候就见媳妇趴在地上，大声喊道：“唔皇大驾光临，臣妾侯架来迟，请皇上赐罪。” 这时，方圆几十米的视线都聚集过来了。');
INSERT INTO `tpshop_joke` VALUES ('984', '昨天体检去医院医生让我取翔检测，给我和棉棒。我问医生这尼玛怎么取？医生告诉我插里拿出来就可以了。结果我照做后尼玛怎么只有棒了？棉花呢？');
INSERT INTO `tpshop_joke` VALUES ('985', '青年：“有多少人像我这样失眠的呢？” 大师：“发个红包就知道了。”');
INSERT INTO `tpshop_joke` VALUES ('986', '“爸爸，我们学校成立一个乐队，我想去参加，学校还说乐器要自己带。”父亲盯了儿子老半天，递过一根筷子：“孩子，咋家穷，你能不能去争取当指挥？”');
INSERT INTO `tpshop_joke` VALUES ('987', '刚才老公手机10086来短信：“快汇钱来。” 我想现在客服什么态度，语气这么硬。 就回了句，“用完再汇吧！” 那知那边又来了短信，“没钱休想找老娘。” 唉，你说现在这客服怎么了……');
INSERT INTO `tpshop_joke` VALUES ('988', '就刚刚，一女汉子同事看见我在嚼槟榔，问我槟榔还有没有，我说：“有，还是进口的！”同事：“还有进口的槟榔？拿个给我尝尝。”我：“你真的要？”同事：“给我！”我说那你张开嘴，然后我火速把自己嘴里的槟榔放进了她嘴里。不说了，现在脸上还火辣辣的……');
INSERT INTO `tpshop_joke` VALUES ('989', '“导演，我后背的纹身是不是弄错了？” “没有，我们查过了，岳飞后背的刻字不是精忠报国，而是尽忠报国” “那我后背的精尽报国是怎么回事？” “。。。。。”');
INSERT INTO `tpshop_joke` VALUES ('990', '前几年，男友外遇分手了。 今天，我在街上遇见他和他丈母娘在一起。 我：“你这个狗东西，昨晚才贪污了我，今天怎么换了丈母娘了？” 说完我转身就跑了，留下他们在风中凌乱……');
INSERT INTO `tpshop_joke` VALUES ('991', '这十种男人是最可恨的，同胞们见到了可以上去暴打，因为打了不犯法。1 坐出租车，计程表上打出23.5元，他掏出20元，说：“不用找了！”我亲眼见到这种男人被暴打的场面，那叫一个惨呀！2 在女人面前贬低别的男人以抬高自己的男人。这种男人是自大狂，所以在贬低别人的时候就特别不留情面，所以我们见到这种男人时可以毫不犹豫地上去打。3 给乞丐两块钱，然后叫人家找一块钱的男人。这种男人我还能有什么话说？除了打，还有什么可以表达我们的情绪？4 三十岁了还称自己“男孩”或“男生”的男人。你想，一个三十岁的男人恶心兮兮地说：“像我们这样的男孩……”你的皮肤会不会有蛇在上面爬的感觉？碰到这样的男人，你不打他，就是对不起你自己！5 用老婆的钱在外面包小蜜的男人。如果你用老婆的钱在外面胡吃海喝，花天酒地，人家最多说你是个“吃软饭的”，最多很厌恶你，并不会上去打你。因为存在这样的女人，她情愿把钱给自己心爱的男人花，这叫周瑜打黄盖。但是我相信不存在这样的变态女人：把自己的钱拿给男人，让男人去找别的女人。存在这样的男人，是他道德的败坏，人人可打；存在这样的女人，就更该打了！6 穿肉色丝袜的男人。不管什么情况，穿肉色丝袜的男人就该死，这比西装笔挺却穿一双球鞋或者穿黑皮鞋配白袜子的男人更可怕。因为男人穿西装配球鞋或穿黑皮鞋配白袜子只是品味不够，人家在背后最多会说：“丫太不会搭配。”穿肉色丝袜的男人就不同了，这种男人不管出于什么目的，穿上这种袜子总是让人有想吐的感觉，你不打他会憋死的。7 在饭桌上，语出惊人，说：“我想拉屎。”这种男人属于装可爱的类型。男人可以可爱，但是不能“太”可爱。比如偶尔撒撒娇，说：“不干啦！讨厌啦！”这样的话对你女朋友说说，那还不要紧，你女朋友心情好，说不定会说：“哟！蛮可爱的嘛！”如果心情不好，你就准备接一个耳光吧。但是你如果在饭桌上说那么恶心人的话，就不是可爱，无论坐在饭桌上的是什么人，不打你就显得太亏了。8 出国归来，说话时老是夹着外语单词的男人。你跟他讲了，他还振振有词，说：“没办法，改不过来，在外国这么多年了。”把责任推给习惯问题。我简直是TMD扯淡！你在中国生活这么多年，讲了多少年汉语？出国才几年？就把你多年的习惯改成这种德性啦？这叫什么？这叫卖弄！伙计们，上去打丫的！9 从日本留学归来，说话时老是说：“在日本怎么怎么样。”这种男人不用我解释，你一定会上去打的。之所以把日本单独辟出来归为一条，是因为这种事跟普通的出国不同。这关系到民族感情。涉外无小事，更何况是日本？这样的准汉*，你不打，你就是汉*。10 逛完小百科不回复一下的男人。女人不回贴，可以。因为我们照顾女士。男人不回贴，败坏！为什么？你想想，我花这么多时间原创出这么好的和男人有着莫大关系的东西，连版权都不要了，贴出来给男人看，你看了不回贴，还有良心吗？所以，同胞们，举手之劳，多多益善，你好我好大家好嘛！');
INSERT INTO `tpshop_joke` VALUES ('992', '1.我一同学上小学的时候体育课拿了一个袋装的酸奶,没来得及喝就上课了,他当时也不知怎么想的,就顺手放进自己戴的帽子里了,他上课还不怎么老实,体育老师一怒就朝他脑袋拍了一巴掌,顿时酸奶顺脑袋横流,那场面．．．老师当时吓得手都哆嗦2.有一个女孩子平常被妈妈管的很严。有一次被男朋友叫去看电影，临出门时妈妈嘱咐说：“出去要放聪明点不要被男人占了便宜，如果他摸你上面你就说不要，模你下边你就说停。”女孩说记住了，晚上回来她妈问她有没有被占便宜，女孩哭着说：“占了，他上下一起摸我，我就照你教的说：不要停，不要停。”3.鱼说：“我时时刻刻把眼睁开是为了在你身边不舍离开。”水说：“我终日流淌不知疲倦是为了围绕你，好好把你抱紧。”锅说：“都他妈快熟了还这么多废话。”4.钱可以买房子但买不到家，能买到婚姻但买不到爱，可以买到钟表但买不到时间，钱不是一切，反而是痛苦的根源，把你的钱给我，让我一个人承担痛苦吧！5.狮子和熊分别在树旁大便，一个月后，狮子发现自己大便旁的树木比熊的那棵长得粗壮，于是说了一句饱含沧桑的哲理——狮 屎 胜 于 熊 便！6.尊敬的用户，此时我们已从您的话费中扣除20元献给巴勒斯坦民族解放事业，为此巴自治政府决定以全体阿拉伯世界的名义授予您崇高的称号：本.沙勒巴基！7.刚才和朋友聊天，其中有谈到你，知道吗？我和他们吵了起来，还差点动手打起来，因为他们有的说你像猴子，有的说你像猩猩，实在太过分了！根本没有把你当猪看！8.我一直是江湖中不知名的侠客，直到有一天遇到了传说中最神秘的你，竟然叫出了你的名字，从此，我在江湖上也有了响当当的名号：知猪狭！9.父亲：“你怎么这么笨，真是个小猪猡！咳！你知道小猪猡是什么吗？”儿子：“知道，它是猪的儿子。”10.测听力，用一个耳机，发出不同音量和频率的声音，测试你是否听得到。我一同事怎么也听不到，医生（注：年轻女医生）不停的放大音量，可还是听不见。于是女大夫问：“你打过炮吗？”一下子满屋寂静……我同事憋的脸红脖子粗小声说：”打过，可是有什么关系吗？““哦，我是说你是不是退伍兵。” 又晕倒了一片');
INSERT INTO `tpshop_joke` VALUES ('993', '以下内容皆收集整理自网络，欢迎转载。挑选整理每日经典段子，博得大伙一乐。1.我过生日，老婆给我买了一瓶女士香水，当然，香水被她自己拿去享用了。老婆过生日，我给她买了一个我心仪已久的剃须刀。两天后，岳父多了个新剃须刀。2.女:我有一个不情之请。 男:说吧。 女:我男人吧体弱多病，有的是指不上他，你是男人，而我又是女人，你又那么强壮，呵呵…… 男:没事，快开始吧！ 女:能不能把我新买的冰箱搬进来。3.晚上，媳妇衣衫不整的躺在床上玩手机，我随手拿起桌上的香蕉一脸坏笑对媳妇说:“真羡慕香蕉。”谁知她鄙视的看了我一眼，“你是没见过茄子吧……”哎，她已不是当年的她了。4.同桌告诉我，这世上再也没有比爱情更复杂的东西了，我一本数学书摔在他脸上！5.女朋友穿着暴露躺在床上挠我痒痒，我坏笑着说：“你不乖哦～”她咬咬嘴唇，双手环上我的腰：“那……你来惩罚我好吗。”然后我把她缓缓抱起来放在床上，轻轻地把她的手绑在床头，然后蒙住她的眼睛，拿出她手机把壁纸设成了黄子韬。6.女：你是怎么来的？ 男：开车来的。 女开心道：开的什么车啊？ 男：也不是什么好车，几十万，垃圾车…… 女激动：尼玛，几十万的车，还叫垃圾车？真有钱……能不能带我去兜兜风？ 男：好啊，走……说着一指路边的垃圾清运车，上吧……7.最近特别想跳槽。其实现在的待遇还不错，是家世界五百强的大企业，单位给配车，还不用自己掏油钱，主要负责与客户洽谈交易后期的相关事宜，就是太累了经常要出差。至于为什么要跳槽呢，就是觉得。。。kfc外卖员这份工作不太适合我。8.日本所提出的“取消俄罗斯在联合国内的一票否决权”的提议，遭到俄罗斯的一票否决。 好梗，我能笑一年9.“你今天为什么衣冠楚楚的，查理？”“庆祝金婚纪念日。”“你开什么玩笑，你才结婚五年。”“可它对我来说就像整整50年！”10.以前楼主和老婆嘿咻的时候，担心隔墙有耳，习惯开点音乐掩盖一下声响。没想到，这却培养了老婆的音乐细胞，现在她几乎每晚都要听音乐');
INSERT INTO `tpshop_joke` VALUES ('994', '1.一向严厉的班主任要走了， 今天是最后一课， 我们几个女生纷纷送上了礼物。 没想到班主任接到礼物的时候突然怔住了， 过了好久才慢慢说道：“额，这是我当老师以来第一次收到礼物。 我真没想到，我平时对你们那么凶那么差， 你们不但不记恨反而送我礼物。 我只想问一句，你们是不是贱啊？”2.昨晚全公司加班，突然没电了，经检查是保险丝烧了。我懂点电，老板让我去修，一妹子在旁边用手机照亮着。在公司人安静的等待的时候，媳妇来电话了，开始是嘘寒问暖的，后来突然问：怎么那么安静？这个时候妹子说话了：你快点搞了啦，人家手都酸了。。。3.今天感冒了，回家便钻进被窝休息进入免打扰睡眠模式，老婆见我感冒严重，便兴冲冲的去厨房忙活了，半小时后，回来端着姜糖水过来，瞬间，我感动的稀里哗啦。。。然后，然后老婆就端着姜汤水自己喝了起来。。。说怕被我传染，先预防着。。。4.快递刚送过来的TT。。我坏笑道：老公，要不要试试质量如何。。老公：行啊，我来吹个气球给你看看能吹多大！我。。。。5.男：亲爱的你知道吗？在动物界，腿越少的动物越聪明。例如：人比狗聪明，狗比蜈蚣聪明。女：要这么说的话，女的比男的要聪明哦。');
INSERT INTO `tpshop_joke` VALUES ('995', '一辆大巴被劫持了，歹徒上来挑了几个女的就下车了，一车人无动于衷！这时一个妇女突然跳下车和歹徒搏斗起来，于是一车人蜂拥而上，最终将歹徒制服！记者采访，问她是什么勇气让她勇于和歹徒搏斗？她生气的说，一车女的全挑光了，就剩我一人，磕碜谁呢！一个女孩让一个男孩在楼下等他一百天就嫁给他。结果那个男孩在第99天时微笑着离开了。后来，有人问他，你坚持了这么久，眼看就要成功了，为什么在关键时刻离开？男孩解释着说：当时都没想到自己能坚持这么长时间。心里就想到，我有这么大的毅力，为什么不去朋友圈卖面膜呢！“你TM天天扒别人老婆裤子要脸不要脸，我怎么没见着你天天扒我裤子？这日子能不能过了，不能过就离！”“老婆，你有必要把我给女儿换尿不湿说得那么严重吗？”宿舍里一个逗比胖子对我们说：我要减肥，等我减肥成功了再和你们一起玩耍。这时，我对铺来了一句：你TM这是想和我们绝交啊？以前特看不起买彩票，因为觉得那是投机取巧，而且中五百万的几率微乎其微，今天突然想明白一件事，买彩票中五百万的几率要比我工作挣五百万的几率大的多。一天，女朋友在卧室里赤裸着身体照镜子，自己打量了半天后，对我说：我看上去又胖又丑，难看死了，亲爱的你说句好听的话安慰安慰我吧！我说：你眼光真准。新闻:一男子在田里捡到周朝时期的青铜剑，上交政府，得到了800元的奖励。神回复:好不容易打出了神器，结果卖给了npc！今天和一哥们儿去ATM机取钱，一个面相猥琐的男人站在哥们儿的旁边，一直斜着眼盯着他。哥们儿十分害怕，终于忍不住转头对男人大吼：“你总盯着我看干嘛？想打劫啊！”男人也大吼道：“我就是想看看你把身份证塞提款机里能取出多少钱！”(本文系作者个人观点，不代表一点资讯的观点和立场)');
INSERT INTO `tpshop_joke` VALUES ('996', '站在讲台上，我们慷慨激昂：“钱是个什么东西？钱是王八蛋，它能把黑的变成白的，把邪恶变成正义。记住吧，同学们，金钱乃万恶之源啊！”　　下了讲台，我们顾不得擦汗水，急忙找主办方要演讲费。　　攥着那几张钞票，我们心里踏实了许多：“钱是什么？钱真是个好东西，没它我在讲吧上有什么好吼的。”');
INSERT INTO `tpshop_joke` VALUES ('997', '朋友千里迢迢而来，奉上礼品：“真不好意思，一点小礼物，不成敬意。”　　我们马上谦让：“哪里哪里，你来就是我最大的荣耀了。至于礼品，千里送鹅毛，礼轻情意重嘛。”　　朋友走后，我们翻看礼品，气不打一处来：“搞什么嘛，这点小礼品他也拿得出手？真是个铁公鸡！”　　随手将那“鹅毛”扔在一边。');
INSERT INTO `tpshop_joke` VALUES ('998', '赶车时，我们不停看表，心想：这车可千万要开慢点，不然要是误了车可就麻烦了；　　上车后，我们不停看表，心想：这车怎么这么慢？照这速度，何时才能赶到目的地啊？');
INSERT INTO `tpshop_joke` VALUES ('999', '那天晚上，在酒桌上，几个老爷们儿聊起婚姻与围城的关系。　　老许说：“这婚姻就是围城，我就是那城墙，天天围着老婆转。”　　小刘说：“别看我刚结婚两年，但自打进了这围城，我就感觉像是找到了根据地，老婆把旗帜在城楼上这么一竖，我的思想顿时就跟着老婆走了。”　　话音刚落，老梁站起身来，披上外套，我们劝他再坐会儿，老梁却摇了摇头说：“我得赶紧撤了，我家城规十点准时宵禁。”');
INSERT INTO `tpshop_joke` VALUES ('1000', '大家有同样的感受么，家里的镜子和外面的不一样啊……　　　　理发店的镜子更不一样啊~隔了我的飘飘长发……　　　　我就哭~~昨天剪了个短发，在理发店的镜子照的那个清爽，那个干净利落。　　回家后再照镜子，怎么感觉有点傻……　　　　今早出门看到商场镜子，尼玛，这个二货是谁……');
INSERT INTO `tpshop_joke` VALUES ('1001', '停车没入位 小心被大妈推走啦[doge]2015-10-27 13:05 法制晚报 显示图片四川简阳跳坝坝舞的大爷大妈们把车推到路上的照片火了。作为有车一族的小伙伴，咱们#停车入位别添堵#，给跳舞的大爷大妈腾个地儿，不给他们徒手推车的机会，把车推到路上实在是太危险啦！#文明行车##文明停车#从身边做起，从#停车入位别添堵#做起。@@北京市社会办');
INSERT INTO `tpshop_joke` VALUES ('1002', '昨晚跟老公闲扯，老公嫌弃我啥也做不好，我就说：是的，我除了长得好看 一无是处。gc来了，老公说：人家长得好看怎么说也是个花瓶，你tmd充其量就是个花盆，花盆，花盆…!-_-#');
INSERT INTO `tpshop_joke` VALUES ('1003', '每年的10月31日 是“万圣节之夜”。这一天，人们都会购置各种服装道具，细心打扮、华丽变装，小朋友们则提上糖果篮子，玩起了大家都熟悉的“Trick or Treat”(不给糖果就捣乱)游戏。这个变相的角色扮演派对总是能催生出众多奇思妙想，甚至国际巨星、性感大咖们也都为这天拼尽全力，丢掉偶像包袱，各种搞怪形象全然释放……不要心急，您也可以!接下来就带你玩转奇趣魅力的万圣节秀场。颐堤港精心打造全城极具摩登氛围的万圣节“奇趣变装秀”! 10月24日至11月1日，来颐堤港可以享受时尚、趣味、互动的万圣节变装造梦空间，你有可能寻找到另一个与众不同的自己。传统游戏玩不够，您还可以拥有更丰富想象力和创意，打造属于自己的风格和时尚态度。活动中，“奇趣变装间”、“时尚变装秀”、“奇趣糖果机”、“定制变装明信片”，诱人的美味、甜品、狂欢派对……每一个环节都能令人感受到惊喜!在“奇趣变装间”画上OPI万圣节美甲，炫出闪耀光辉;搭配上丝芙兰面部彩绘，增加节日趣味;再为心爱的人亲手做一颗爬爬步步万圣节棒棒糖，一起甜甜蜜蜜;去DIY 3D蝴蝶结和万圣节神秘饰品，与孩子尽情玩乐，共同彩绘一个南瓜带回家……太多的工作坊活动，嗨爆全场!在“时尚变装秀”领略潮流大牌的时尚魔力，必须做个日常时装精!参与微信摇一摇，还能赢取品牌万圣节礼包。在“奇趣糖果机”前面，与情侣或孩子共同感受抓取糖果的幸运和快乐，共享甜蜜与趣味，其乐无穷。而这一切刻最重要的是：发现你的与众不同，记录下美好时光!不用担心，颐堤港早已贴心准备好一切。在“定制变装明信片”就可以把照片打印成专属明信片，将您变装的美丽瞬间定格，留下美好回忆。与此同时，众多颐堤港餐饮店铺也将推出多重万圣节系列的菜式、甜品，并举办狂欢派对。令您全方位吃喝玩乐，享用不尽。除此之外，颐堤港还将推出缤纷万圣节奇趣礼品，消费满赠即可参与抓取糖果和手工活动!关注颐堤港微信，并将现场照片发送至朋友圈，就能马上领取一个“头上长南瓜”可爱的饰品。还等什么?颐堤港万圣节“奇趣变装秀”，怎能错过。关于颐堤港颐堤港位于北京市朝阳区酒桥仙路18号，直通地铁14号线将台站，占地约为5.9万平方米，总楼面面积约为17.6万平方米，是一处以零售为主导的综合商业项目。该项目建有一座时尚购物商场、高25层的甲级办公楼颐堤港一座、拥有共369间客房与套间的休闲式商务酒店北京东隅及正在建设中逾17公顷的开放式公园。颐堤港氛围时尚灵动、汇集众多国际品牌，世界各地美食齐聚、健康美味应有尽有，更有丰富多彩的休闲娱乐活动，可以尽享文艺风情。颐堤港商场及北京东隅都荣获了美国绿色建筑协会的能源与环境设计先锋评级(LEED)金奖认证，颐堤港一座荣获该评级白金奖认证。');
INSERT INTO `tpshop_joke` VALUES ('1004', '1、今天是个很特别的日子，你还记得么？好好想想，去年的今天你还哭了呢，哈哈，今天是万圣节，还记得我装鬼吓唬你么。2、万圣节“捉鬼”游戏：此鬼人模鬼样，张牙舞爪鬼头鬼脑，行事鬼鬼祟祟，你若捉到他定能驱魔辟邪，万圣节祝你开心快乐！3、万圣节，点亮一只南瓜灯，给孤魂野鬼留一点照路的光；放一些糖果供品，给小鬼们一个饱餐的机会。万圣节祝你的善行，感动鬼鬼们，带给你用不完的快乐幸福。4、万圣佳节，风吹叶落，小鬼出没，今日记得，发发信息，表表祝愿，记得要转，不然难堪，神马过客，均是夜神，心别寒胆莫颤，有我短信保你平安！5、传说万圣节里许愿特别灵验，你不妨实践一番：深夜下两点，你戴上南瓜帽，吃完南瓜饭，举着南瓜灯，披上南瓜蔓，掐一朵南瓜花，走到墓地对天三声喊，你的愿望准实现，最起码人们也得叫你南瓜大仙！6、天空一片黑暗，火焰仍在肆虐。你的祈祷毫无意义，命运之轮已经开始旋转。让蝙蝠狂舞于暗月，让妖魔聚集于眼前，你要是敢不回我短信，万圣节就等着看好戏吧！7、万圣节，为你送去一份祝福，祝你身材像魔鬼一样迷人，眼神像魔鬼一样诱人，本领像魔鬼一样超人。哈哈，万圣节快乐。8、为了表示我对你真诚的祝福，三十六路妖魔，七十二路鬼怪，听我号令，立刻给正在看短信的好朋友送一个巧克力蛋糕，送去我的祝福，万圣节快乐！9、如果你怕鬼，万圣节你来吓鬼，如果你信鬼，万圣节你来扮鬼，如果你爱鬼，万圣节你来追鬼，如果你见鬼，那么万圣节真的到了，抓紧时间去欢笑吧！10、忙忙碌碌的日子里，或许遗忘了联系，身心疲惫的日子里，或许忘记了送祝福，但是今天是你的节日，我绝不会忘记，送上最真挚的问候，万圣节快乐！11、知道你是个鬼机灵，快点想个鬼点子，咱俩搞个鬼把戏，万圣节里去捣鬼。约会地点鬼门关，手里提盏鬼吹灯，接头暗号鬼画符，谁要不去鬼脑壳。不见不散哟。12、装扮巫师，预知未来；装扮小鸟，愤怒时代；装扮柯南，看破真相；装扮丸子，天真可爱；装扮僵尸，夺人魂魄；装扮天鹅，美丽夺目。万圣节到了，想好你的角色了吗？13、来吧，一起装扮；期待，新的形象；放开，心中担忧；扮演，梦中印象；放肆，开怀大笑；幻想，天马行空；逍遥，激情荡漾。祝万圣节快乐！14、你装扮猫咪汤姆，我就装扮淘气的杰瑞；你装扮善良的白天鹅，我就装扮邪恶的黑天鹅；你装扮愤怒的小鸟，我就装扮那只气你的肥猪。快乐万圣节，逗你没商量！15、万圣节，你开门见鬼，鬼躲；抬脚踩鬼，鬼溜；举手招鬼，鬼逃；转角遇见鬼，鬼求饶。你以为你是钟馗呀。原来你扮得比鬼更可怕。祝万圣节玩鬼快乐。');
INSERT INTO `tpshop_joke` VALUES ('1005', '曾在贴吧上看到这一幕：\r\n 1L：4L是我小弟\r\n 2L：4l是我儿子\r\n 3l：4L是我孙子\r\n 4L：汪汪汪！！');
INSERT INTO `tpshop_joke` VALUES ('1006', '内心神揣测：Chapter1二哈：伸出手好想要触碰你，却又不敢，怕踏出了那一步，就连朋友也做不成，那一声呜咽，你可曾听懂？\r\n我喵：你丫是不是有病！俺都做好了备战状态，你丫居然躺下了又！\r\nChapter2二哈：（最后一声叹气才是亮点）你这凡夫俗子简直不懂本王！！！你们喵界的都是我玩弄于利爪间的小毛球，嘚瑟啥！！我喵：瞧瞧你那二货的蠢样，拉拉小手怕都不敢！？\r\nChapter3我喵：你特么到底会不会击掌？\r\nChapter4二哈：人家好想跟你握手，可是人家好害羞嗄~~我喵：你TM的左手右手都是慢动作，到底要干什么？有本事落喵爷头上试试，挠不死你个大块头！\r\nChapter5我喵：二货，到底要干啥？我都杵半天了，若即若离的，要打又不打，要摸又不摸的。二哈：人家撒娇在呢，没看出来么？小编：脑弄还能开得再大点么？各位看官自行脑补！');
INSERT INTO `tpshop_joke` VALUES ('1007', '昨日(3日)，宿迁一位李先生在网上发布了自己读三年级的儿子的一道数学题目。题目很简单，但目测众多老师看了该哭晕在厕所了。11月1日晚上，李先生照例检查儿子的家庭作业，当看到这么一道数学题时，题目答案让他哭笑不得：“小明今年8岁，爸爸的年龄是明明的3倍，爷爷的年龄是爸爸年龄的6倍多3岁，爸爸、爷爷今年各多少岁?”众位的脑海中想必已经有了答案，爸爸的年龄是24，而爷爷已经147岁了!这就表示，爷爷在123岁高龄时才生下小明的爸爸，而小明的爸爸15岁就当爹了。帖子一出，立刻引起了众多网友的吐槽，不少网友道：“生物老师看了该哭了。”然而小编觉得此话不妥，岂止是生物老师，众多老师都要一齐“哭晕在厕所了好吗!”不但无辜躺枪的生物老师没法跟孩子解释为何有人能活到如此高龄、无法解释为何123岁了还能生下小明的爸爸，咱亲爱的政治老师也无法解释的清为何小明爸爸15岁就能结婚生子了。难道人家不按法律规定来?这不又把法律老师牵扯进来了嘛。唉，看来一道三年级数学题，要“牵连”的人可真是不少呀。在这里，小编就想建议一下那位数学老师了，为了不让一道数学题将众位老师“逼到厕所集体痛哭”，也为了不误导孩子，让孩子具有正确的常识，老师还是应以大局为重，不要再出这种吓坏人小心脏的“奇葩”题了。');
INSERT INTO `tpshop_joke` VALUES ('1008', '昨天我们的话题是：说说有一个不会聊天的朋友是种什么体验~看完评论，充分感受到了大家的无奈~↓↓↓@思聪的小甜甜一见面就 你怎么好像老了 你比去年胖了点@你看我这是啥玩楞？分分钟想拉黑举报他顺便再拎着果篮去看看他父母@‰￡我姓石 √铁石心肠的石一句话说完全场气压直逼北极冰洲南极冻土 让人恨不得棉被羽绒服统统披起来@想念成诗就是和ta讲话分分钟想要暴打ta800次都没法解气就像我同桌我真心想吃了ta不过我们还是挺好的@刘晗不管你说什么都以“然后呢？”结尾。“刚才吃的好饱。”“然后呢？”……“你看那边的美女！”“然后呢？”……“帮你介绍个对象？”“然后呢？”，好想呵呵，但是就怕他继续回我“然后呢？”@言希我说：我要减肥！她说：减肥成功的人都不值得做好朋友！@温婷婷导师带我们三个人，春节后返校开小会，导师说“你看A同学给我带来河南特产烧饼，B同学给我带来内蒙老家的牛肉干。。。”当时脑子一抽筋，我转向那俩同学质问道“为啥不给我带？！”@WADE-3“我跟你讲，我刚刚心情本来超不好的后面遇到男神瞬间世界都美好了，你知道我现在什么心情吗！”“画个k为正的正比例函数图像一目了然”“…你走”@迟哥下次请你吃饭。我今天就有空@～除了给他发红包会领走，其他时候没有任何反应～@?? 凉。每分钟原谅他八百回，分分钟想拿菜刀砍死她@??懒喵说了好笑的段子，对方.....。话题瞬间终结了。@过敏原他说的每句话都是结束语………@Deslre感觉这就是上天派来考验你忍耐力的生命中的一个坎@小七与阿陆。刚开始聊天就告诉我，我不会聊天@&#214;不会聊天的朋友？这种人早就打死了啊，哪还有什么体验个个都是话题终结小王子，最后太狠23333！');
INSERT INTO `tpshop_joke` VALUES ('1009', '进入了大学，可是小明却为情所困静不下心来好好安排光阴，他很烦、很乱。他问智者，将心静下来要多久？　　智者答，一个呼吸。　　小明摇了摇头，去问痴情人。　　痴情人泪流轻语，一辈子。　　小明迷糊中似有所懂，回家路上分了心出了严重车祸被送往医院。　　进入病房的那刻小明从昏迷中醒来，他听见肇事司机向院长问了他正在思考的问题：“将他的心静下来要多久？”　　院长隐晦的声音传过：“一刀。”');
INSERT INTO `tpshop_joke` VALUES ('1010', '乘电梯下楼，遇见一时尚美女，喷着浓浓的香水下至五楼。　　上来一老太太，闻这股气味，大声评论道：“怎么在电梯里也喷杀虫剂？”　　满电梯的人掩嘴而笑……');
INSERT INTO `tpshop_joke` VALUES ('1011', '爷爷回到家，看见爸爸在疯狂的打我，连忙制止，问缘故，“兔崽子看见有人死了，家人在扔纸钱，觉得花花绿绿的，就跑去捡。”“那也不至于打孩子啊？”“可是，爹，我问他为什么捡的时候，他说，以后爷爷死了，就不买了，直接撒。”“爹，爹，您别冲动先把刀放下……');
INSERT INTO `tpshop_joke` VALUES ('1012', '出差一个月回来了，老婆做了几个菜犒劳我，我一看有凉拌黄瓜、炒黄瓜、黄瓜蛋花汤……，问：怎么都是黄瓜啊？老婆：你回来了，留着也没什么用了……');
INSERT INTO `tpshop_joke` VALUES ('1013', '「微信」是什么？ 答：扯犊子的工具。 . 上午： 基本没动静。 . 中午： 各种晒，晒胸晒大腿，晒幸福，晒方向盘，晒飞机票，应有尽有。 . 傍晚： 开始各种饭局，酒店，ktv，求陪同，求偶遇。 午夜 各种饿，各种吃，各种再也不吃宵夜了，明天开始戒酒。 . 凌晨： 各种哭，各种失眠，各种感悟，各种胡言乱语。 . 总结：「微信」是腾讯开的精神病院。 只能微信！不能全信！');
INSERT INTO `tpshop_joke` VALUES ('1014', '　　冯绍峰对着倪妮发誓说：‘’如果有一天我离开了你，我就把名字倒着念‘’。倪妮说：‘我也是’’！——尼玛，看着我也是醉了！ 　　神回复：林志玲表示表示不服。');
INSERT INTO `tpshop_joke` VALUES ('1015', '狼若回头，必有缘由，不是报恩，就是报仇。事不三思必有败，人能百忍则无忧。越牛逼的人越谦虚，越没本事的人越装逼。拼你想要的，争你没有的。要想人前显贵，就得背后遭罪。最穷不过要饭，不死终会出头！ ———献给一直在打拼的朋友们');
INSERT INTO `tpshop_joke` VALUES ('1016', '年底了公司发工资了，一到家我就很自觉地把钱交给老婆，老婆亲了我一口对我说：“老公累了吧，今天我来帮你洗脚吧”， 此刻我终于感觉到了一点男人的尊严，苦日子总算熬到头了，立刻脱下鞋：“给老子洗干净点。” 老婆刚拿起鞋，就把鞋垫抽出来，然后给了我一巴掌：“你胆子可真够大的啊，竟敢在鞋底偷留私房钱！” 我TM也是醉了。。。');
INSERT INTO `tpshop_joke` VALUES ('1017', '　　昨晚和老婆啪啪啪，儿子睡着了，突然儿子动了一下。继续啪啪啪，儿子又动一下，继续啪啪啪。儿子:要我动几次你们才知道我是醒的！');
INSERT INTO `tpshop_joke` VALUES ('1018', '内涵段子:以前高中老师常说 高中紧些 大学就松了 而现在大学了 我终于明白了老师想要传达的意思 我受教了 啊 多么痛的领悟');
INSERT INTO `tpshop_joke` VALUES ('1019', '一性感美女摆着屁股从夜店的卫生间走向吧台。以一种撩人的姿态坐下。向酒保诱惑勾了勾手指。酒保连忙过来，美女问：你们经理在吗？酒保：不在，他出去了。美女就把手放在酒保的嘴里，酒保性奋的挨个吸允了十个手指。美女满意的看着酒保，对酒保说：告诉你们经理，卫生间没纸了！');
INSERT INTO `tpshop_joke` VALUES ('1020', '一直以来啊总有人批评80后90后如何思想堕落如何离经叛道 如何的扶不起来 还有人说他们是道德毁掉的一代人 是“垮掉的一代” 我特别的不理解 自顾自跳广场舞扰民的不是他们 不让座就拳脚相加的也不是他们 碰瓷耍赖的不是他们 炒高房价的不是他们 开黑心食品加工坊的不是他们 建小工厂乱排污的更不是他们 是谁自己道德缺失 反过来教育下一代人说这就是现实呢? 是谁自己跌倒了还要反讹把自己扶起来的年轻人这到底是谁扶不起来?  当然那些值得尊敬的长辈们给八零后九零后树立了榜样让他们不断的成长不忘自我反省 于是我们能看到身边大多的80、90后都能够遵守公序良俗 他们会主动的让座 会羞于插队 最坏的行为也就是在电梯里面按亮所有的楼层  如果要给绝大多数80后90后一个整体的印象 我可以这么说 他们是刚一就业就被延迟退休 他们是干20年都买不起房 结不起婚 却每天努力工作的一代人 他们是吃着各种黑心食品长大却每天茁壮成长的一代人 他们是顶着恶劣的就业环境 自然环境 顶着高高的房价 住在北上广出租屋里却没有怨天尤人 每天还在追 求自己理想追求自己想要生活的一代人 如果谁硬要说他们是“垮掉的一代人”那么是被现实');
INSERT INTO `tpshop_joke` VALUES ('1021', '阿凡提突然病了，病得还不轻，卧床不起多日了。他的邻里好友来探望他。可这些人一来便向阿凡提问这问那，没完没了，完全忘了阿凡提是个病人。在疾病的折磨下阿凡提哪儿有心思与他们瞎聊呢？但又不便对他们说什么。　　临走时，这些人又你一言我一语地问阿凡提：阿凡提，您还有什么事要我们做吗？您还有什么意愿吗？　　阿凡提听了，立刻回答道：谢谢你们的好意，我只有一个意愿，那就是请你们今后探视病人的时候，一定要少说话！');
INSERT INTO `tpshop_joke` VALUES ('1022', '阿凡提已年过七旬，一天，他不服老，企图把院子里的一块大石头搬动一下，这一搬坏了他的事，腰也扭了，气也不顺了。从此，他卧床不起。　　许多亲朋好友前来探望他。他对安慰他的人说：请你们别难过，我身体和年轻时一样，力气一点没减少。　　何以见得呢？人们问道。　　我们家院子里的那块大石头，我年轻时搬过它，怎么搬也没搬动，几天前我试了试，仍然没搬动，你们看我的力气不是和年轻时一样大吗？阿凡提说。');
INSERT INTO `tpshop_joke` VALUES ('1023', '希特勒来到一个精神病医院视察，他问一个病人：是否知道我是谁。病人摇摇头。于是希特勒大声宣布：我是阿道夫·希特勒，你们的领袖。我的力量之大，可与上帝相比！　　病人们微笑着，同情地望着他，其中一个人拍拍希特勒的肩膀说道：是啊，是啊，我们开始得病时，也像你这个样子！');
INSERT INTO `tpshop_joke` VALUES ('1024', '精神病院的病人对新来的医生说：医生，我们都很喜欢你，觉得你比以前那位医生好多了。　　医生：谢谢，为什么呢？　　病人：你看上去和我们的样子差不多。');
INSERT INTO `tpshop_joke` VALUES ('1025', '病人：谢谢你，医生。谢谢你昨天把增强记忆的办法教给了我。　　医生：噢，有这回事么？');
INSERT INTO `tpshop_joke` VALUES ('1026', '为什么你不把自己的脑袋忘了。　　如果忘掉脑袋，那我的帽子往哪儿戴？&nbsp;');
INSERT INTO `tpshop_joke` VALUES ('1027', '某老人读完一本关于如何增强记忆力的书，便大肆吹嘘他的记忆力提高了一大截，还要老妻试试他。妻子说：明天咱们外出旅行，你把应带的东西背一遍。　　老人精心抄了一份清单，认真地背起来。　　第二天，两人上路了。在汽车里，妻子问他：你能背下咱们带的东西了吗？　　老人一字一句地背得滚瓜烂熟，一件不少。　　妻子很高兴，问他东西放在哪儿？　　老人一听，瞠目结舌。他懊丧地说：亲爱的，东西都忘在家里了！');
INSERT INTO `tpshop_joke` VALUES ('1028', '二位男子在万圣节化妆舞会後走路回家。当他们经过一个墓园时。一时兴起要穿过此墓园。当他们走到一半时便被一声声叩-叩-叩的声音给吓住了。这声音是从某个阴暗处传出，他们被吓得浑身发抖，接着他们发现有位老年人手执凿子正在凿一块墓碑。　　其中一位男子便说：我的天啊，先生，我们以为你是鬼耶，这么晚了，你在这做什么呢？　　老人骂道:这些傻瓜,他们把我的名字拼错了。');
INSERT INTO `tpshop_joke` VALUES ('1029', '有位经常丢三落四的科学家乘火车时，正赶上列车员查票。他找遍了自己的所有口袋也没有找到车票，急得满头大汗。　　这时，列车员认出了他是大科学家，说：不要紧，你不必着急，回来时给我们看看就行了。　　不，我要将它找出来的。　　你太认真了，其实……　　不是认真，我必须找到这该死的车票，要不然，我怎么知道我该上哪儿去？');
INSERT INTO `tpshop_joke` VALUES ('1030', '史密斯是个年轻的律师，业务上很能干，但十分健忘。一次，他被派往圣路易斯去会见一位重要的诉讼委托人，以解决一件疑难案件。第二天，他那个事务所的老板收到他从圣路易斯发来的一份电报：　　忘记诉讼委托人的姓名，请即电复。　　老板复电：　　委托人的名字叫霍布金斯，你的名字叫史密斯。');
INSERT INTO `tpshop_joke` VALUES ('1031', '一和尚来到森林里，站在大树下参悟佛法。有只大灰熊从他背后走过来，闷声就是一掌，和尚当即晕死过去。大灰熊把和尚翻过来一看，摸着后脑勺说:”哎呀，俺认错人了，还以为是光头强。”');
INSERT INTO `tpshop_joke` VALUES ('1032', '听说喝骨头汤能补钙…… 猪都笑了， ..猪..说:我才三、四个月就被迫出栏了，我比你还缺钙呢！你吃我，你补什么？ 激素！抗生素！ 现在的猪和20年前的猪完全不一样喽！20年前的猪吃的是野菜，玉米面，小麦麸子，豆饼……纯植物，无任何添加剂！ 现在的猪吃的是调配的饲料，三月肥，三月肥，三月不肥包你赔。 如果还想通过吃猪骨头补钙？想的美0 你知道我想什么吗？？？？ 他妈的我也想啊补钙！');
INSERT INTO `tpshop_joke` VALUES ('1033', '老师：你说你！啊！这么不争气！你能给你们家！你爸你妈做什么贡献啊！学生：老人不图儿女为家做多大贡献啊！一辈子不容易就图个平平安安！');
INSERT INTO `tpshop_joke` VALUES ('1034', '老师：今天我也让你们当一回老师，让你们体会一下做老师的辛苦。小明：上面的那位同学，既然你那么喜欢站着，就TM站到走廊上去。老师：滚出去……');
INSERT INTO `tpshop_joke` VALUES ('1035', '老师：小明，你又上课睡觉！给我站起来！小明：老师，我的上眼皮和我的下眼皮在生病！老师：哦？什么病？小明：自闭症！');
INSERT INTO `tpshop_joke` VALUES ('1036', '七夕之歌…久久不爱爱,皮肤就变坏,要想皮肤好,早晚必须搞,要想感情真,全靠搞得深,如果舍得干,弄烂不遗憾,只要高潮到,其它都不要。 早晨起来做回爱,锻炼身体又补钙,中午时分搞一搞,增加感情效果好,晚上睡前叫叫床,办起事来更疯狂,半夜别忘补一炮,增强免疫防感冒。');
INSERT INTO `tpshop_joke` VALUES ('1037', '跟一朋友聊天，聊到老婆的时候，朋友深深的吸了口烟，意味深长的说道：“结婚前，我老婆喜欢撒娇，结婚后就喜欢撒野了。”');
INSERT INTO `tpshop_joke` VALUES ('1038', '朋友和我们打麻将他老婆来电话叫我们别说话，朋友对电话说：“路上堵车，估计很晚才能回来。”他老婆在电话那头说：“你注意安全，慢慢开车，你按个喇叭我听听！”');
INSERT INTO `tpshop_joke` VALUES ('1039', '和老婆逛街，遇到一个卖花的小女孩，她上来就抱住腿说：“大哥哥，买朵花送给女朋友吧。”“放手！别抱着我腿！”老婆凶狠狠地说。');
INSERT INTO `tpshop_joke` VALUES ('1040', '老婆：“老公，我想买这个？”老公看都不看价格，把卡扔过去：“想买什么？自己充钱！”');
INSERT INTO `tpshop_joke` VALUES ('1041', '电焊工开了个电焊铺，取了一个高大上的铺名:焊武帝，大家都夸这铺名起得好，他也很得意，逢人就炫耀……\r\n这天他去隔壁的糖果店炫耀，糖果店老板拉着他看了看自己的店名:糖太宗，电焊工沉默了……糖果店老板又指了指不远处切糕店的店名:汉糕祖，二人一起沉默……\r\n这时，一个掏粪工骑着拉粪车从两人面前经过，二人仔细一看拉粪车上，瞬间就羞愧的面红耳赤，拉粪车上赫然写着三个大字:擒屎皇……');
INSERT INTO `tpshop_joke` VALUES ('1042', '女“我是你的什么？” 男“你是我的优乐美。” 女“你有见过180斤的优乐美？”');
INSERT INTO `tpshop_joke` VALUES ('1043', '对女人来说胸小可以用加厚的罩，眼小可以用眼影假睫毛，个矮可以穿高跟鞋恨天高，腿粗可以用长裙掩盖以显得较小。对男人来说，长得矮黑丑挫只有一个办法，就是用钱来掩盖。');
INSERT INTO `tpshop_joke` VALUES ('1044', '今天去相亲了，朋友:怎么样？ 我:像雪碧，朋友:哦，怎么说？ —我:去时心飞扬，见识偷心凉，〒_〒');
INSERT INTO `tpshop_joke` VALUES ('1045', '有一次我偷偷带着老爸，从网上淘到的古铜币去班里炫耀，结果搞不见了。 老爸问我时，我战战兢兢地说：“不是教育我，要想办法跟同学打成一片吗？” 老爸愤怒地说：“我现在就想想办法，把你打成一片（揍扁）！”');
INSERT INTO `tpshop_joke` VALUES ('1046', '我：“你怎么整天除了玩电脑就是玩手机，也不知道休息下。” 儿子：“我耍电脑是为了让手机休息，耍手机是为了让电脑休息。” 看我不打屎你！');
INSERT INTO `tpshop_joke` VALUES ('1047', '考试时，我正在做卷子，由于做的太认真了，露出了半截内裤都浑然不知，于是监考老师把我露出的半截姨妈巾当作小抄抽出来时，整个世界都安静了……');
INSERT INTO `tpshop_joke` VALUES ('1048', '教师家长群，有人提议大家相互熟悉一下，群内遂开始诸如以下内容的话：“我做建材”“我做连锁美容院”“我做点小生意，翡翠玉石”“我做游轮旅游主要是 欧洲和南极”……随后又有人说咱们聚聚联谊一下吃个饭吧。有一位一直没发言的家长说：我拿5000块够。”片刻寂静后，有人回应：“我觉得5000够了。');
INSERT INTO `tpshop_joke` VALUES ('1049', '有一天，情人节时，老公问老婆：“你喜欢什么花？我可以给你。” 老婆高兴的说：“真的吗？那我说了！有钱花、可以花、随便花。” 老公听了顿时晕了过去。');
INSERT INTO `tpshop_joke` VALUES ('1050', '前女友结婚了，死活要我去祝福她，谁知那男的知道我是她前男友，很不屑的说：就你这样的，没车没房，不知道为什么当初会看上你。秉着段子精神，我头上扬45度，望着天空，忧郁的说了一句：可能是活好吧。。。');
INSERT INTO `tpshop_joke` VALUES ('1051', '一个好的老公钱包里面应该要有老婆的照片，对不对？ 神回复：这是错的，一个好老公就不该有自己的钱包。感觉好有道理。');
INSERT INTO `tpshop_joke` VALUES ('1052', '‍‍‍‍今天应酬完回家，小区楼下转了一圈没见停车位。 对着楼上喊了一嗓子：“你老公回来啦！” 嘿嘿，三分钟不到，十几台车开走了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1053', '话说，送鞋给女朋友就是送走的意思，不懂这个是谁说的。 最近有个小伙给他女朋友买了双鞋，拿到面前的时候，女朋友大吵大闹，说小伙不爱她了，要送走她，小伙子灵光一闪，往里面塞了一块钱，说哪里送你走啊，这是一块走的意思啊。 女朋友破涕而笑。');
INSERT INTO `tpshop_joke` VALUES ('1054', '两个朋友，一个在深圳一个在丽江。一个年薪十万，买不起房，租房住，朝九晚五每天挤公交，呼吸着汽车尾气，想着出人头地。一个无固定收入，住在湖边一个破旧四合院，每天睡到自然醒。以摄影为生，到处溜达。没事喝茶晒太阳，看雪山浮云。一个说对方不求上进，一个说对方不懂生活');
INSERT INTO `tpshop_joke` VALUES ('1055', '‍‍‍‍一同事住宿舍，那天带女朋友去宿舍看没人就各种缠绵发出各种声音。 正当完事休息时，角落那床传来句：“哎呀我的妈呀，你们总算完了，害的我呼吸都不敢大声。” 剩下他两独自凌乱。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1056', '刚坐公交车，坐对面的俩女的很小声的说“对面那男的好丑。” 我第一反应转过去看我旁边的，没想到我旁边的也在看我，我俩相视一笑。 “对面俩女的好丑”我们异口同声的说到。');
INSERT INTO `tpshop_joke` VALUES ('1057', '‍‍‍‍老王头儿越看孙子越不像自己亲生的，于是悄悄带着孙子做了亲子鉴定。 果然孙子和自己没有一点儿血缘关系，为了能说服儿子。 老王强迫儿子领着孙子去做了亲子鉴定，结果孙子是儿子亲生的！ 老王头一病再也没起来！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1058', '今天听一哥们给他妈打电话：‘妈，干吗呢？’ 他妈说：‘干活，给你攒钱娶媳妇’。 这哥们那眼睛就开始湿了…… 此时……此时那边传来一声音‘别动，是三条不？！擦，胡了！');
INSERT INTO `tpshop_joke` VALUES ('1059', '美院正在上人体课。一女生画着画着，突然把笔扔在地上！女生对男模怒斥道：“一会大一会小的，还TM让不让人画了！”');
INSERT INTO `tpshop_joke` VALUES ('1060', '记得我们小时候的顺口溜都是“大头大头，下雨不愁，人有雨伞，你有大头”。 今天在楼下听一小破孩高歌“是他……是他……就是他……抗日英雄大姨妈。” 突然感觉整个人都不好了。');
INSERT INTO `tpshop_joke` VALUES ('1061', '小表弟学习成绩不好，只念过高中的小姨下了很大决心全程陪读。 陪读的效果真的很不一样，高考成绩出来后，小姨上了重点本科线，小表弟只上了专科线。');
INSERT INTO `tpshop_joke` VALUES ('1062', '‍‍老两口看电视，突然转播选美比赛。 老头子一看，脸骤红，起身入屋去。 老太太笑说：“老头子还挺封建呢！” 片刻，老头子出屋，端正坐好，脸上多了一副老花眼镜……‍‍');
INSERT INTO `tpshop_joke` VALUES ('1063', '　　期中考试，儿子被狠训了一顿——成绩很不合爸爸的心意。 　　过了几天，老师的短信来了：补考的结果，成绩还是不及格。爸爸看了，怒从心头起：“你这次是补考，补考还不及格，你太给我丢人了！” 　　儿子一听，连忙辩解说：“这能怨我吗？老师用的卷子还是上次那张。上次不会，这次肯定还是不会啊。”');
INSERT INTO `tpshop_joke` VALUES ('1064', '单位大哥，向我请教结婚购房以及装修的经验，然后我告诉他：“房子要买一楼，窗户不要装有护栏，这样着火了也不会被困；卧室衣柜一定要够大，这样才够你媳妇衣服的容量；床一定要有高度，由其是床底，空间一定要够用，这样就可以多放一些杂物；大哥按照我的建议，买了房，装了修，结果结婚不到半年就离婚了...');
INSERT INTO `tpshop_joke` VALUES ('1065', '今天和朋友去吃饭，看见他鼻子下面有一颗鼻屎，又不好意思说，就说他鼻子下面有一颗米饭，结果他笑笑，就用舌头舔进了嘴里.........');
INSERT INTO `tpshop_joke` VALUES ('1066', '‍‍有次公司同事一起聚餐，吃饭的时候点了一些啤酒。 有个同事突然问：“有没有人喝白的？” 大家都摇摇头。 他说：“你们都不喝白的啊？那我喝吧！” 大家都觉得挺爷们，谁料那货居然要了瓶营养快线……‍‍');
INSERT INTO `tpshop_joke` VALUES ('1067', '以前爷爷喜欢下棋，下不过邻居大爷，很苦闷，我就想替他报仇。私下找邻居大爷，他说让我，我说不要，打开手机里“象棋大师”，调到专家难度。大爷下一步我摁一步，机器人下一步我学一步。大爷都傻了，连输4局，然后我就逃了。后来他居然找我拜师，说我是天才，一边玩手机都这么厉害，都不带想棋步的');
INSERT INTO `tpshop_joke` VALUES ('1068', '‍‍一少妇在给小闺女喂奶，大闺女说她妹妹：“你吃的是二手的你知道么。” 爸爸在旁边不乐意了，对大闺女说：“你吃的也是二手的，你知道么。” 这时，少妇斜眼望向爸爸：“你确定你吃的不是二手的？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1069', '‍‍“你把我的头发理成这个样子，让我怎么出去？” “没关系，本店代售帽子。‍‍”');
INSERT INTO `tpshop_joke` VALUES ('1070', '马上就要和女朋友分手了，为了挽救这场爱情，我就把她的素颜照发到朋友圈，结果她不仅主动联系了我，还信誓旦旦的说，我跟你没完......');
INSERT INTO `tpshop_joke` VALUES ('1071', '刚从打麻将时放了个屁，感觉声音有点大就问其他三位：你们听到什么声音没有？我对面那哥们说屁都放了还说个屁，只见另外两位牌友异口同声的说道：我这么小声放个屁你也听得到？。。。我瞬间石化！这算一屁三响么');
INSERT INTO `tpshop_joke` VALUES ('1072', '跟我读: (•̀ㅂ•́)و✧ “枯藤、老树、昏鸦 (◉ω◉ ) 晚饭、有鱼、有虾 (´･ω･`) 空调、WiFi、西瓜٩(*´︶`*)۶҉ 夕阳、西下(´,,•∀•,,`) 你丑、没事、我瞎 ”*罒▽罒*');
INSERT INTO `tpshop_joke` VALUES ('1073', '‍‍‍‍想想高中老师还是挺牛逼的存在。 物理老师说：“同学们看好啊，我要射了！” 数学老师说：“同学们看好啊，我要变了！” 化学老师说：“这种试剂对你们人类是有害的！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1074', '　　小伙子还在加班吗? 是啊！刚来业务不熟加班多多熟悉业务。 嗯，不错啊，挺积极啊，有前途。你让我想起一个同事他以前和你一样天天加班，最后!最后怎么啦？加薪了?升职了？ 嗯，不是，最后他老婆和别人好上 ，哈哈');
INSERT INTO `tpshop_joke` VALUES ('1075', '同桌问我GC是什么，我想我不能带坏纯洁的好孩子。我就问她，“高”的拼音第一个字母是什么？她说G “冷”的英文的第一个字母是什么？她说C 她恍然大悟原来是高冷的意思啊。 后来她就到处对别人说，你又GC了啊（你又高冷了啊）');
INSERT INTO `tpshop_joke` VALUES ('1076', '‍‍‍‍我：“老板，我的手表不走了，你帮我修下。” 老板：“好的，你明天过来取吧。” 我：“好。” 第二天，我去取表：“老板，我来拿手表。” 老板：“你的手表走了啊？” 我：“走了？” 老板：“我晚上把它修好了，然后它就走了。” 老板你过来，我保证不打死你！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1077', '‍‍‍‍今天上街看到人体秤，于是我就上去称了下。 对着男票高呼：“我又瘦了。” 男票不经意说了句：“胸都没有能不瘦吗。\' 我：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1078', '这段话真漂亮！ 婚姻其实就这么真实！男人虽爱美人，但不一定会娶她，到最后……男人娶的都是适合做老婆的。女人虽然爱钱，但不一定会嫁富人，到最后……嫁的都是待她好的。所以，做人到最后，拼的不是富和美，而是责任和人品。其实，心地善良的女人最美丽，有责任感的男人最富有！');
INSERT INTO `tpshop_joke` VALUES ('1079', '‍‍‍‍小姨子是很宅的那种女孩子，正好暑假了，她姐让我带她去驾校报名。 报完名，跟着教练先看下学车场地，正巧一辆车不怎么怎么烧着了。 有一名教练跟学院正拿着灭火器扑救。 小姨子来了一句：“姐夫，这驾校可以，学了救火都用真车烧！” 然后就看到带我们来的那教练一脸黑线！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1080', '你碰到你爱人出轨了你会怎么办呢？（扭头就走啊）真宽容！然后呢？（把头喂狗啊……）');
INSERT INTO `tpshop_joke` VALUES ('1081', '夫妇俩逗儿子：“爸妈谁好？”儿说：“妈好！”爸问：“爸怎么不好？”儿说：“逛公园你抱阿姨不抱我！”妻大怒！儿接着说：“还是妈妈好！你出差妈怕我害怕，还找个叔叔一起睡。');
INSERT INTO `tpshop_joke` VALUES ('1082', '　　你们千万听我一句劝哈，想让别人停止吸烟过去跟人家直接言语沟通就好了，我已经碰见走到人家桌边，神情凝重不说话连续做各种控烟手势，跟火影忍者结印一样，最后让对方以为是聋哑人乞讨，给了10块钱。');
INSERT INTO `tpshop_joke` VALUES ('1083', '五岁的时候，你可以只为捕捉一只蝴蝶，而跑到一公里外的田野。十岁的时候，你可以只为一个冰淇凌，而跑遍大街小巷的商店。十七岁的时候，你可以为喜欢的人，一个人去陌生的城市。二十七岁的时候，你可以只为了生活，而随便就找了人，过一辈子。你说，你越来越懒了，懒得去爱，也懒得被爱。晚安');
INSERT INTO `tpshop_joke` VALUES ('1084', '记者采访农民工：假如中国爆发战争，你愿意上前线吗？ 农民工：上前线有户籍限制吗？农村户口可以吗?是不是北京、上海户口的人优先上战场？要暂住证吗？要上岗证、健康证吗？需要有一年以上完税证明么?还有，牺牲了大家赔钱都一个价吗？真打的时候，分单双号吗?冲锋前要摇号吗？多久能摇上号？');
INSERT INTO `tpshop_joke` VALUES ('1085', '一次去商店买洗脸盆，问老板，这个盆怎么卖，老板说5块一个，我问老板质量怎么样，老板抓起盆就往地上摔，啪！摔碎了，正当我惊恐的看着地上的盆，突然老板说：这质量，我能卖给你吗？来看看这个8块钱的！');
INSERT INTO `tpshop_joke` VALUES ('1086', '　　室友半夜起来撸，我对他说，我们还是学生，你怎么能这样做！室友说，这怎么了，我都已经是成年人了，这是我的自由，我说，那你撸自己的啊，撸我的干嘛！');
INSERT INTO `tpshop_joke` VALUES ('1087', '每个人心底都有那么一个人，已不是恋人，也成不了朋友。时间过去，无关乎喜不喜欢，总会很习惯的想起你，然后希望你一切都好……');
INSERT INTO `tpshop_joke` VALUES ('1088', '咱坐不起飞机，咱没有失联，咱坐不起豪轮，咱没有沉船。别总抱怨自己没钱，别总羡慕人家这儿那儿地潇洒游玩，一不小心，上天入地，天堂阎王殿，有去无回。每天睁开眼能听到父母声音看得到孩子的笑脸，吃的粗茶淡饭，挣点可以养家糊口的小钱，和朋友喝点小酒、上网扯扯咸淡，看着新闻想着那些撕心裂肺寻亲不见的哭声，感叹：原来平安才是幸福！！！');
INSERT INTO `tpshop_joke` VALUES ('1089', '　　说个老师的事，这不前两天我们学校派出一位搞计生工作的老教师去别学校监考初三生化实验操作考试。也许是工作的关系，监考时，尼玛，第一句话就暴漏了职业病。“同学们，请把你的准生证放在桌子上，没有的不准考啊。”学生大惊，一生弱弱问道：“我们又不生孩子，只是考个试，有准考试证还不行吗？”这老师立刻醒悟道：“有证就行。”');
INSERT INTO `tpshop_joke` VALUES ('1090', '今天加班很晚才回家，街上基本没人了，经过一条小路的时候，发现后面几个男的悄悄的跟着我，有个还在淫笑说:这小妞身材真不错耶...我当时非常害怕，甚至不敢回头，万一我回头把他们给吓跑了怎么办 ？');
INSERT INTO `tpshop_joke` VALUES ('1091', '你们有谁在听梁静茹的《暖暖》的时候把“安全感”听成“鹌鹑蛋”的？我不信就我一个！');
INSERT INTO `tpshop_joke` VALUES ('1092', '去券商开户，券商问：“多大了？”“38”“结婚了么”“结了”“怕老婆不”“怕”“住几楼呀”“21楼”“那您别开了，我们现在只给2楼以下的开！”');
INSERT INTO `tpshop_joke` VALUES ('1093', '股市就是《西游记》，各路妖魔神怪齐出想发财，最后不是死了就是被庄家出来收得人财皆空。我就想考一下大家，是谁发布了唐僧肉长生不老的利好消息？');
INSERT INTO `tpshop_joke` VALUES ('1094', '一句话震慑全世界！俄罗斯：我要炸叙利亚！朝鲜：我要打韩国！希腊：我要退出欧元区！美国：我要加息了！中国：我要开盘了！');
INSERT INTO `tpshop_joke` VALUES ('1095', '租住的地方附近有一物流公司，每天车来车往。我减肥，常常去他们院子里练练器械。昨天晚上，守门的大爷走过来问我：装卸工干不干？一个月能挣七八千。我。。。');
INSERT INTO `tpshop_joke` VALUES ('1096', '昨天坐公车，发生了吵架的事情，一男的骂售票员：长这么难看不知道怎么艹的。售票员回：我长怎么难看都是爹妈给的，哪像你街坊四邻凑的。。。');
INSERT INTO `tpshop_joke` VALUES ('1097', '“这次召回行动已经开始1个月了，但还没有一辆车到我们这里返修。”汔车修理店的老板对维修师傅说。“这次召回的原因是什么？”“刹车失灵！”');
INSERT INTO `tpshop_joke` VALUES ('1098', '小王去吃西餐，服务员给他端上来牛排，他奇怪地问：“这牛排怎么有酒味？”服务员向后退了两步说：“现在呢？”');
INSERT INTO `tpshop_joke` VALUES ('1099', '我：“老板，你这水果这么多斑能吃吗？”老板：“选水果就像选老婆一样，有讲究的。”我：“怎么说？”老板：“漂亮的不放心！”。。。说的我竟无语反驳。');
INSERT INTO `tpshop_joke` VALUES ('1100', '有人说一边充电一边玩手机，像狗被拴着一样。其实这种说法也不对，狗被拴着，肯定会极力挣扎。而手机党，一般稍微牵动一下眼睛就会瞄一下插座。');
INSERT INTO `tpshop_joke` VALUES ('1101', '‍‍‍‍老婆生日。 老公：“亲爱滴，祝你年年有今日岁岁有今朝！” 老婆：“尼玛你今年忘了给我买礼物明年还不想送。” 老公：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1102', '‍‍‍‍跟同事去买水果。 到了同事来了一句：“老板，给我杀两个菠萝，再杀个柚子。” 老板直接愣了：“客官，怎么个杀法？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1103', '‍‍‍‍二货老婆去她远方舅舅家喝喜酒，几天未回。 深夜孤枕难眠，老婆打电话来问我：“想我吗？” 我说：“想得硬睡不着。” 老婆说：“死鬼，你等会，我弟媳妇在洗澡。” 一会老婆发来彩信，并附言：“趁我新婚弟媳妇冼澡偷拍的，你也沾沾喜气，撸撸睡吧。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1104', '眼见范冰冰与李晨在一起了，孩子会取名李冰冰；李冰冰唯有嫁给范伟，生个孩子叫范冰冰，方能扳回一成????');
INSERT INTO `tpshop_joke` VALUES ('1105', '‍‍‍‍记得刚毕业的时候，几乎所有亲戚都在问：“哪儿工作啊？” 疲于回答的我统一回复：“在比尔吉沃特当ADC呢。” 长辈们一听这名字这职位，都以为是世界五百强的跨国公司，也就不再追问其他的了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1106', '‍‍‍‍昨天去骑马，带老婆和她闺蜜。 跑了不到两圈，老婆闺蜜掉下来了，瘫倒在地上。 当时大家都吓坏了，过去看了还好没事。 问她：“怎么掉下来的。” 她说：“不知道。” 晚上老婆笑着和我说：“她高潮了，夹不住了。” 我：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1107', '‍‍‍‍我走进一家店铺，冲着老板喊：“我爷爷没钱了，给我拿1000万来！” 老板瞥了我一眼：“不就是上坟烧纸钱吗？有必要说的那么拽吗？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1108', '‍‍‍‍朋友爱喝酒，车里经常放着一瓶125ml的老村长！ 五一朋友聚会喝酒都喝了不少，散后我陪他去见客户，结果遇到交警查酒驾。 只见这货悠悠把车放在路边，拿起那瓶老村村当着交警面一口气把它给喝了。 然后说：“蜀黍我是刚喝的酒我这不开了！” 然后我们下车叫了的士走了！俩交警在那儿凌乱中！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1109', '　　我：妈，我今天遇到个神经病！ 　　妈：啊？你又照镜子啦？ 　　麻麻，请你不要这样……');
INSERT INTO `tpshop_joke` VALUES ('1110', '我有个朋友姓敖，名武。每次去他家楼下叫他，都感觉自己好奇怪，知道今天。我又在他家楼下喊，：“敖武。嗷武。嗷武”。 一楼的大妈从窗户探出头：发情了啊！！！');
INSERT INTO `tpshop_joke` VALUES ('1111', '　　转看到的，广州一男子怕漂亮女友被抢，每天带其吃美食，从1000斤养到180斤，近日他终于求婚成功了。看到这我想说，这就是偶像啊！');
INSERT INTO `tpshop_joke` VALUES ('1112', '昨天晚上收到死党发来的短信：“哈哈，刚刚亲热完，女朋友她妈回来了！我现在拿着衣服躲在她的衣柜里，像演电影，好刺激…”看完这条短信后，我默默的拨通了他的号码……就让电影朝更刺激的方向前进吧！');
INSERT INTO `tpshop_joke` VALUES ('1113', '一南方妹子，比较娇小，才158cm，有一男性朋友，195cm来着。某天，男生突然很认真对她告白，妹子瞬间黑线，随口就回了一句：“你是想找根拐杖吧？”');
INSERT INTO `tpshop_joke` VALUES ('1114', '　　今天逛街，突然内急，好不容易找到一公厕。直接往里闯，被门口的大爷拦住，要交费。我急着说先解决了出来再给。出来后一搜口袋，居然没零钱了。幸亏哥机智，看到大爷叼着烟，遂掏出口袋烟递出一根:大爷，您看这行不？大爷鄙视地看了看我那七块的黄山，没吭声。我咬了咬牙，又掏出一根:上厕所一次五毛，我这七毛了，不用找了！赶紧放到大爷怀里，跑了。永远不会忘了大爷那凌乱的眼神。');
INSERT INTO `tpshop_joke` VALUES ('1115', '我是一名银行柜员，清明节那天为一位客户办业务，客户说：哟，今天看来蛮空的，外面都没几个人排队。我很严肃地说：没有啊，你看外面凳子上坐了好多人排队呢。那客户回头看了一眼，立马办完业务跑了。');
INSERT INTO `tpshop_joke` VALUES ('1116', '一女生跟男朋友分手了，旁边她同学安慰她：“那男的有什么好，土木工程的，一听就知道又“土”又“木”的！”旁边软件工程的男同学一听，心都凉了半截…');
INSERT INTO `tpshop_joke` VALUES ('1117', '　　以前读书打饭回寝室吃都说“打包”，最近却流行“带走”……刚打球回来，去食堂窗口:“带走！阿姨！”阿姨刚要给我打饭，这时后边一个工友师傅不乐意了:“你可不能带走阿姨，这是我老婆～”');
INSERT INTO `tpshop_joke` VALUES ('1118', '　　今天哥们捡着一手机，放心没人丢失手机，是工作完成后上车前换工作服就把手机放车顶，换完就上车走人～走了好几里路都快上省道了…等红灯一刹车…咣当一声，神马东西，擦，手机…停车！尽然还在，中间还经过修高速的土路，竟然没丢…真捡着了！');
INSERT INTO `tpshop_joke` VALUES ('1119', '王警官经过对凶案现场的盘查后说：“你们有没有发现，这个案子与两个月前朝阳区ktv包厢密室杀人案和一周前东城区电子厂跳楼案以及三天前的海淀区出租房碎尸案有个惊人的共同点？” 年轻警员恍然大悟：“您的意思是说这是一个连环杀人案？” 王警官说：“不是，我的意思是说我都破不了。”');
INSERT INTO `tpshop_joke` VALUES ('1120', ' 十年没见的初中同学聚会，一女同学说男友搞房地产的，各种炫富，还说今晚所有消费全算她的。埋单时，她男友来接她，看上去有50岁了。高潮是我身边一哥们儿淡定地喊了声：“爸……”');
INSERT INTO `tpshop_joke` VALUES ('1121', '‍‍今天小姨子来我家，对老婆哭诉道：“我被甩了，那个王八蛋脚踩两只船。” 老婆安慰道：“别哭了，有什么好伤心的！谁年轻时没爱过两个人渣啊！” TM我现在就想静静。‍‍');
INSERT INTO `tpshop_joke` VALUES ('1122', '　　LZ农村的，每回想起夏天一大群赤裸裸的小盆友跳到村子旁边的小水库游泳，打水仗，时不时又跑上岸互相抓粘在jj上粗黑发亮的大蚂蝗，那个酸爽劲，想想也是醉了。');
INSERT INTO `tpshop_joke` VALUES ('1123', '‍‍宿舍有个哥们个子才160cm，经常遭到同寝的人嘲笑。 今天这货被逼急了，大声吼到：“我矮怎么了，浓缩就是精华，我特么至少捡钱速度比你们快。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1124', '真正的放下大概是，你不会删除他的聊天记录，也不会把他拉入黑名单，只是任由他躺在通讯录里，再也懒得去点开。他像你掉进床底的笔，扔在地铁站的水，决定不要了，就再不会想起。他留下的那些痕迹就像家中沙发缝的灰，油烟机上的渍，你不会为它特意来次大扫除，只是心情好时，顺手一擦。');
INSERT INTO `tpshop_joke` VALUES ('1125', '一个地主给自己十几岁的孩子找了个媳妇，由于小新郎年龄太小，不懂人事，一切都由做父亲的代办了。 　　这让媳妇很难堪，于是她向婆婆哭诉，婆婆听完恨恨的说：“这个该死的，跟他爹一个德性。”');
INSERT INTO `tpshop_joke` VALUES ('1126', '‍‍‍‍晚上同事聚会，打电话给二货老公请示。 “臭老头子，奴家今晚同事聚会，你自己吃饭哈。” “嗯，不许喝酒，晚上不安全！” “哎哟，担心我啊！嘿嘿……” “滚犊子，我是担心别人！你别伤着别人！” 不聚了，回家削丫的！');
INSERT INTO `tpshop_joke` VALUES ('1127', '我小时候话特多，有天晚上睡觉时又那喋喋不休，我妈忍不了了就威胁道:再说话就把你扔了，我再重捡一个回来。我幽幽看了我妈一眼道:你捡回来的小孩也一样说话，因为他是他妈扔的。');
INSERT INTO `tpshop_joke` VALUES ('1128', '今天刚换教室，我如往常一样扣着手机，边玩边上厕所。就在我脱裤的那一瞬间，几个妹子进来了，然后就啊的一声尖叫。我一愣，纳尼？小便池哪里去了？然后我点根烟，淡定的走出厕所，对那几个女生说：“我是修厕所的”。');
INSERT INTO `tpshop_joke` VALUES ('1129', '‍‍在高中的时候妈妈对我说：“以学习为重不要搞对象。” 在大学的时候妈妈对我说：“以身体为重不要搞对象。” 毕业后妈妈对我说：“以事业为重不要搞对象。” 说的好像有哪家的丫头能看上我似的。‍‍');
INSERT INTO `tpshop_joke` VALUES ('1130', '‍‍‍‍昨天坐火车去北京。 对面有一伙计从一上车就开始吃，一会拿出一只扒鸡吃，想把头拽下来丢掉，结果头拽下来了顺手把身子丢掉了！ 看他盯着鸡头愣了一会，埋头接着吃！朕实在憋不住了！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1131', '‍‍‍‍有一天，两个情侣坐在公园的长椅上谈话。 女：“明明喜欢我，却不跟我说。” 男：“我想静静。” 女：“你跟我说清楚，静静是谁。” 男：“你先别说这个，你TM先告诉我明明是谁。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1132', '有一天某人去喝豆浆，发现豆浆里有一只苍蝇在豆浆里游来游去！喊道：服务员过来，它在干什么？服务员：他在游泳！客人：你不知道这里禁止游泳吗？服务员：对不起，我帮你换一碗吧！服务员把那碗拿走又拿来了一碗豆浆！客人吼道：碗里怎么还有苍蝇？服务员面不改色心不跳的回答道：不可能，我端来之前，已经让它们全部上岸了！客人........');
INSERT INTO `tpshop_joke` VALUES ('1133', '在洗衣服的外婆和弟弟的对话 弟弟：外婆，帮我把这个内裤扔了吧。 外婆：你看这图案多好看，扔了怪可惜的，我给你改个口罩，你还可以用。 弟弟。。。。。');
INSERT INTO `tpshop_joke` VALUES ('1134', '‍‍‍‍一个人准备上山拜师学艺对大师说：“大师，求您收我为徒吧！” 大师说：“施主啊，你六根未净，情丝未断，心中那份执念还没放开，老衲怎么能收你为徒啊！” 这位施主说：“这是我路上遇到隔壁师太托我给你捎的袈裟，这是你昨晚拉下的！” 大师：“那，施主，进屋说话！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1135', '　　楼主未婚夫在部队，打算年底回来领证……于是去向领导问问请假的相关事宜…… 　　领导:什么？？你要结婚？？不行！我都还没结婚你不准结！ 　　他:……你特么找不到女朋友关我什么事！');
INSERT INTO `tpshop_joke` VALUES ('1136', '‍‍今天我和一哥们看见前面有个女的在走，那屁股扭的特大。 当时就没控制住声音说了一句：“看那女的扭屁股走的带劲了。 然后就被那女的听到了，就说：”你tm不扭屁股走路我看看。 然后那哥们一下子就愣住了，我在那一个劲的笑。‍‍');
INSERT INTO `tpshop_joke` VALUES ('1137', '粘苍蝇的纸放一天会粘满苍蝇，二货朋友说为什么都那么傻，没看到同伴都死了吗！ 我不假思索的来了句：“路上如果围一群人你能忍住不上去看看吗？”');
INSERT INTO `tpshop_joke` VALUES ('1138', '地铁里大家都捂住鼻子 肯定是哪位没素质的又在地铁里吃韭菜包子！\r\n要不是我现在大便蹲着不方便 不然肯定去教训一番！');
INSERT INTO `tpshop_joke` VALUES ('1139', '在家里的花园里给儿子把嘘嘘！他爷爷走过说“嘘嘘不要把在草上，草碰到嘘嘘马上死”我愣啊，原来儿子尿的不是尿是除草剂啊');
INSERT INTO `tpshop_joke` VALUES ('1140', '‍‍‍‍‍‍哥们昨天刚结婚，今天一大早给我打电话说：“出事了！” 我问他：“怎么了？虚脱啦？” 他说：“不是的！昨晚洞房后习惯的给了她两百块钱！” 我：“那你们吵架啦？” 他：“没有，TMD找了我五十！” 找了五十！‍‍‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1141', '飞机上，空姐让乘客们系好安全带，并且用生动的例子来劝说:“上次飞机破降时，没系安全带的都摔的血肉模糊。” 有乘客问:“那系了安全带的呢？” 空姐回答:“没事，都坐的好好的，和活人一样。”');
INSERT INTO `tpshop_joke` VALUES ('1142', '本人农村姑娘，村里年轻人大多出去打工了，所以年轻人不多，所以周围只有我家有Wi-Fi，没设密码最近我家墙外有很多小伙蹭网，于是村里有了这样的传言：嘿，你知道吗？那xxx家姑娘可多人喜欢啦，墙根下蹲的都是人，抢着打电话呐！');
INSERT INTO `tpshop_joke` VALUES ('1143', '今天我问老公，你有五块钱你会给我多少 老公说如果我有五块我就给你四块，我心里想 老公还是挺疼我的，我再问 如果你有十万你会给我多少，老公说 如果我有十万 那我一定把五块全给你…… 给我过 今天让他下不了床！！');
INSERT INTO `tpshop_joke` VALUES ('1144', '三岁的浩浩在外婆家玩耍，一不小心把外婆家的电视机弄翻在地上摔坏了！他外婆听到声响进到屋子里问他“你咋把电视弄点地上摔坏了？”他赶紧说“我没有！那是在演武打片，电视里那俩人打的太厉害了把电视震掉了！”');
INSERT INTO `tpshop_joke` VALUES ('1145', '本人比较矮，老公比较高，这是背景。家里有3岁的宝宝一个，出去就喜欢我抱，我愤怒的问：为什么非要我抱不要爸爸抱？ 老公搭腔到：你底盘比较低，她可能有坐跑车的感觉...........');
INSERT INTO `tpshop_joke` VALUES ('1146', '金子和泥巴相遇，金子不屑的对泥巴说：“ 你看你，灰不溜秋的，你有我闪亮的光芒？你有我高贵吗？”泥巴摇摇头说：“我能生出花、生出果、生出草、生出树木、生出庄稼、生出万物。”金子无言以对。 悟：生命的意义，不在于自己值多少钱，而是自身创造了多少价值。');
INSERT INTO `tpshop_joke` VALUES ('1147', '从前，数字王国和字母王国打仗。 数 字王国派出一队精英去字母王国做间 谍。一个月后，1和3伤痕累累的回来 了：报告国王，我们装B被发现了……');
INSERT INTO `tpshop_joke` VALUES ('1148', '　　宿舍里一舍友特节俭，内裤破了个洞就自己拿针线缝，缝好还给我们展示其手艺如何，然后穿上一提立马哭天叫地喊爹娘，尼玛针忘记取下来了，扎蛋上了...');
INSERT INTO `tpshop_joke` VALUES ('1149', '　　公司业务部的晓丽姑娘对镜化妆，突然来了一句：今天去和客户谈，他要是不同意签合同，我就洗脸吓死他。众人惊了。');
INSERT INTO `tpshop_joke` VALUES ('1150', '　　我平时就内向，不爱说话，和人在一起说话，就很拘谨。有一次 ，表哥给我介绍了一个女朋友，相约在茶馆见面，表哥介绍完，借故离开， 我们俩聊了没几句，交谈陷入困境。我为了打破僵局，就找了个热门话题。不小心把楼市说成了房市于是就问她：“你是怎么看待房市的？”尼玛，就见她害羞地小声说：“还是，还是不要过于频繁的好。”卧槽，是我把人家带进茄子颗里去的。我难为情极了。。。');
INSERT INTO `tpshop_joke` VALUES ('1151', '我有一从小玩到大的哥们，不是亲兄弟胜似亲兄弟。五年前我们同时爱上一个叫兰兰的女孩，哥们忍痛退出，兰兰成为了我的女友。。 哥们对我们说了声祝福从此离开了这个城市，与我断绝了一切联系方式。。 去年结婚的时候哥们封了个大红包给兰兰，酒也没喝，就离开了。红包上写道:“新婚快乐！帮我照顾好他一辈子。” 。。。。');
INSERT INTO `tpshop_joke` VALUES ('1152', '一个人误把假眼吞入口中，最后卡到肛门，送去医院检查，医生看了当场晕倒。 后来医生醒来说:我看了这么多年屁.眼，居然被屁_眼看了一眼。');
INSERT INTO `tpshop_joke` VALUES ('1153', '‍‍‍‍一天我对我老公说：“老公，你快抱抱我！” 老公就过来抱我，然后我问：“老公，我重了吗？” 老公说：“嗯，重了呢！” 我说：“当然重了，因为你现在是抱得两个人！” 老公忙追问道：“老婆，难道你怀孕了！” 我说：“不是，你抱的是我和大姨妈！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1154', '　　老赵和老王俩好友喝酒， 　　老赵喝多了，老王送老赵回家， 　　快到家的时候，老张不愿意， 　　又把老王送回，老王看他喝的多又送老张， 　　来回了几次，最后终于到了老张的家里。 　　媳妇看到丈夫的朋友把老张送回，碍于面子，倒水递烟好不客气，聊了半小时有余，老张始终认为在别人家里，直夸女人漂亮识大体，弟妹弟妹地叫着。 　　这时家里的小狗跑出来， 　　老张猛的一惊： 　　“这不是我家的小狗吗？” 　　老王说： 　　“你再看看媳妇是谁的？” 　　老张：……');
INSERT INTO `tpshop_joke` VALUES ('1155', '一次我们英语课老师坐公交车，一小偷正准备把老师的电话装口袋，　　就在这时，电话响了，老师回头看着小偷和在他手上的电话，　　这时GC来了，小偷说，那你先接，先接....');
INSERT INTO `tpshop_joke` VALUES ('1156', '对着学生楼喊：“还撸呢”，顺间灯灭了很多。　　今天哥们也喊了一声，不过是在小区里喊的拿起喊话器大喊：“你老婆来了。”　　结果三分钟后，外来停放车辆开走20多台，终于有地方停车了。');
INSERT INTO `tpshop_joke` VALUES ('1157', '求你，不要减肥，更不要离开我。　　你难道不知道吗，我从来都没有嫌你胖，甚至祈求你越胖越好。　　我喜欢你胖胖的样子，不，是爱，发自肺腑的爱。　　可是，最近你是怎么了呢？你怎么瘦了？钱包，你醒醒啊...');
INSERT INTO `tpshop_joke` VALUES ('1158', '女屌:为什么我没人要？\r\n小贱:因为你的身材是这样的——月佥，月要，月退，胸..');
INSERT INTO `tpshop_joke` VALUES ('1159', '小胖不听话，母亲要揍他，小胖：“你敢打我吗？”　　“怎么不敢打”？　　“老师说了，虎是一级保护动物，我属虎，谁打谁犯法。”');
INSERT INTO `tpshop_joke` VALUES ('1160', '“亲爱的，未来的路很长，充满了未知，可能随时有危险，所以乖乖躲在我身后，让我保护你好吗？”　　“你谁啊？插队还那么多废话！滚后面去！”　　“哦。”');
INSERT INTO `tpshop_joke` VALUES ('1161', '刚来到新厂就喜欢上了一姑娘，于是哥天天藏她东西，等她着急找的时候忽然拿出来并大声说“看！我找到的，厉害吧”直到今天她看到我正在藏本子，脸红着走开了（这情况……有戏啊），晚饭后我敲开了她的房门，然后……和她老公喝了杯茶');
INSERT INTO `tpshop_joke` VALUES ('1162', '一天小红，小蓝和小绿在街上走，突然小绿踩到小红，小红生气的喊：“你赔我鞋，赔我鞋赔我鞋”，小绿也喊：“吵吵啥，让驴给踢了是咋滴？”，小蓝在那边忍不住了：“刚刚不是小绿踢了小红一脚吗”～顿时一片撕心裂肺的笑声');
INSERT INTO `tpshop_joke` VALUES ('1163', '‍‍‍‍妹纸：“哎，孤独的生，孤独的死，孤独的刷围脖。” 骚年：“换个说法，会更押韵！” 妹纸：“怎么说？” 骚年：“孤独的活，孤独的死，孤独的丑逼刷围脖。” 骚年猝，享年18岁！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1164', '‍‍我们村有一个老头无亲无故靠吃低保过日子，人还特封建迷信。 住的房子也是以前用泥土沏的，时间长了到处漏水。 有一天晚上下很大的雨把老头的房子下垮了，人也埋里面了。 第二天清早被邻居救出来的时候，老头说话了：“别动，帮我把黄历拿来，我看今天能不能动土！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1165', '一天两音乐人在聊天，甲：我的梦想是有一天让我的音乐火遍全国，今天终于实现了，我录制了一首单曲红遍了大江南北，所有人听到了都回头。乙：那你录制的是什么乐曲。甲：《倒车——请注意》');
INSERT INTO `tpshop_joke` VALUES ('1166', '说个朋友糗事 我农村的 有一个朋友去上学到半路了突然想便便 找了地方没有手纸 身上有几块钱 就用钱解决了 另一个朋友知道了 然后钱不见了');
INSERT INTO `tpshop_joke` VALUES ('1167', '‍‍‍‍昨天看到一个段子，说恋爱的最好境界就是把她当女儿养。 我问男友：“你把我当啥养。” 男友淡淡的说：“我把你当儿子养。” 尼玛！劳资有那么汉子吗？‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1168', '昨天爬九华山，半山腰看见一个美女从包里不断地掏出苹果，香蕉橘子啥的往一个不知名的小庙菩萨面前摆，嘴里还叽叽咕咕的，我靠近点想听听她在做什么仪式，只听到:菩萨帮帮忙，老娘实在是背不动了。');
INSERT INTO `tpshop_joke` VALUES ('1169', '今天晚上回到宿舍都十点多了，我室友说：“等到十一点叫我起来，我要和妹子聊天。” 结果十一点到了，我叫他起来没叫醒。 我扫脸乎了两耳巴没醒，我又扫脸揣了几脚，结果他说了句：“别闹正和妹子亲嘴呢！” 当时我一脚口水！');
INSERT INTO `tpshop_joke` VALUES ('1170', '有人问了上帝，“喜欢”与“爱”有什么区别呢？ 上帝指了指一个孩子，只见孩子站在花前，久久不肯离开，最后，孩子被花的美丽迷醉，不由的伸出手把花摘下来。 上帝说，这就是喜欢。 接着，上帝指了指另一个孩子，只见孩子满头大汗的在给花浇水，又担心花被烈日晒着，自己站在花前。 上帝说：这就是爱。 喜欢是为了得到，而爱却是为了付出，这就是最本质的区别！ 花若盛开，蝴蝶自来！ 你若精彩，天自安排！');
INSERT INTO `tpshop_joke` VALUES ('1171', '‍‍女儿：“麻麻，作业做完了。我眼皮打架，想睡觉。” 妈妈：“去睡吧！明天还要早起。” 过会儿，麻麻去看女儿睡着没，发现女儿竟躺在床上耍手机。 吼道：“眼皮子打架还耍手机？” 女儿：“刚才是在打架，我用手机劝他们，就没打了。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1172', '‍‍女儿：“妈妈，为什么暑假比寒假长啊？” 妈妈：“因为天气原因吧！” 女儿：“是热胀冷缩吗？我们今天上课学的一个现象。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1173', '记者：“小伙子，我是电视台的，能采访一下今年回家你妈逼你结婚了吗？” 二逼：“我妈没逼我，呃，不对，我妈逼我了，呃也不对。”');
INSERT INTO `tpshop_joke` VALUES ('1174', '　　老公放了个屁，臭不可闻。 　　老婆皱眉：“不许放屁！” 　　老公：“哦，知道了！” 　　刚说完就又放了个更臭滴…… 　　老婆大怒：“说了不许放屁你还放！” 　　老公委屈：“这屁又不是俺的，你干嘛冲俺发脾气？” 　　老婆：“放你娘的屁，你放的屁不是你的是谁的？” 　　老公：“你刚不是说了，是俺娘的啊！”');
INSERT INTO `tpshop_joke` VALUES ('1175', '个矮是硬伤。以前有个男友身高186，姑娘我身高150。。。某天，这货带我去爬山，山上滑，我不心向后滑，这货拿胳臂拦着，结果我从他腋下溜下去，摔了大跟头。从此以后，我没有一个对象超过175。');
INSERT INTO `tpshop_joke` VALUES ('1176', '‍‍我是开水果店的，一天一妹纸问：“桃子和苹果咋卖？” 我：“桃子4元，苹果5元。” 妹纸：“我都要买，4元5可以么？” 我：“那好吧！” 妹纸选好后，称重。 “一大袋苹果下面就一个小桃子，尼在玩我吗？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1177', '‍‍妹纸A：“怎么办？我老妈要把我给卖了。” 妹纸B：“那你老妈失策了，你卖不了几个钱。” 妹纸A：“这都不重要。她要把我卖到四川去，关键是我不吃辣啊！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1178', '甲：“你为啥当喜剧演员呢？” 乙：“很多人嘲笑我，所以干脆收点他们的钱。”');
INSERT INTO `tpshop_joke` VALUES ('1179', '‍‍妹纸：“亲爱的，如果我是你的尾巴，那你是什么？” 骚年：“那我就是壁虎。” 妹纸：“那你更甩不掉我了。因为你尾巴掉了的话，我还能再长出来。” 骚年：“……”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1180', '‍‍骚年：“宝贝，做女生真好，就像你卸完妆就能过鬼节了。” 妹纸：“哼！那也总好过你，脱掉裤子能直接过儿童节了。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1181', '早上，小明看见他二姨，说：二菜，买姨啊。这孩子，长这么大话了连人都不会说');
INSERT INTO `tpshop_joke` VALUES ('1182', '‍‍‍‍我在住处附近开了家私人小诊所，一般中午老妈都会给我送饭。 这天老妈送饭来后刚坐下，正好有个脸色苍白的人来看病。 我检查后给病人开了些口服药，并叮嘱说：“你要多补充些胡萝卜素、维生素Ｂ和Ｃ、糖类和芦丁，再就是ＤＨＡ、卵磷脂等，对你身体有好处！” 病人走后，我从老妈那里看到一脸的骄傲和崇拜，我淡淡一笑：“我不过是叫他多吃点蕃茄炒鸡蛋。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1183', '顾客：“老板这鱼怎么卖啊？” 老板：“12块钱一斤！” 顾客：“ 太贵了，不要！” 老板手一指旁边：“那条鱼刚死，8块一斤！” 顾客：“那...它是怎么死的？” 老板白了他一眼：“没人买，气死的！” 哈哈哈！ 不可以再生气了！ 生气就不值钱了哦！');
INSERT INTO `tpshop_joke` VALUES ('1184', '‍‍‍‍中午在公交车站，看见一妹子，穿个短裙，她正在路边玩儿手机。 我便来了句：“妹子前面有沟很深啊！” 妹子捂着自己的胸口说：“流氓！” 然后看着我往前走，扑通，掉下水沟了！ 都跟你说沟很深的了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1185', '老爸在客厅边抽烟边看电视，看到我回来了顺手递给我一支，我刚点上烟老爸突然拍照发微博：儿子在家抽烟搞的满屋子都是烟味，生气。然后含着泪对我说：\"忘记你妈快下班了，家里烟味一时半刻散不去，你快跑吧。\"');
INSERT INTO `tpshop_joke` VALUES ('1186', '‍‍‍‍一辆汔车撞倒了一个路人，司机说：“这不是我的错，我开车一向很小心，我已经开了5年车了。” 路人说：“什么？难道是我的错？你开了5年有什么稀奇？我走路都走了50年了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1187', '‍‍‍‍记得一次吃中饭，桌子上有几份素菜，包括黄瓜。 我们几个同事就在说：“怎么还不上硬菜。”（荤菜） 有个女同事特别单纯说了句：“那黄瓜不就是硬菜么。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1188', '高考前，我遇到了刚毕业的大师兄，我问他上大学的感受，他茫然的拍了拍我的肩膀说小兄弟，当四年的欢乐和激情过后，大学提起裤子，冷冷的对我说，你走吧，把金钱和青春留下，我才发现不是我上了大学，而是大学上了我。这就是我的感受…。而我一个农村学生，怎么办啊考还是不考…。');
INSERT INTO `tpshop_joke` VALUES ('1189', '‍‍‍‍前几天看见贝克汉姆的纹身蛮好看的，就学着也纹了一个大花臂。 今天痂都还没掉，回家的时候就被我爸打了：“学什么不好学这个。” 我反驳道：“史进不也纹着九条龙嘛？燕青不也纹了一身大花绣吗？我一身白肉为什么就不能纹？” “史进燕青的结果你不知道吗？打不死你。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1190', '‍‍‍‍教授穿着衣服坐在澡盆里，妻子奇怪地问他：“你怎么穿着衣服洗澡？” 教授从澡盆地跳了起来，突然他一拍脑袋说：“幸亏我忘记往澡盆里放水了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1191', '双胞胎在妈妈肚子里聊天。 :“经常进来的那家伙是谁？” “没看清，戴个头罩” “等下次给他扯下来” “没时间，他溜得太快了”');
INSERT INTO `tpshop_joke` VALUES ('1192', '本人在一私人老板工厂打工，面试时候财务让交两个银行卡帐号，当时很不解，但还是交了，今天发工资了，两张卡，一张进账五百，剩下的全进另外一张卡。好奇问同事。同事说了句：“小伙子，一看你就还没结婚！好好跟这老板干，这年头这样的老板不好找啊！”');
INSERT INTO `tpshop_joke` VALUES ('1193', '　　小时候抽烟，就躲开一边抽免得被老爸发现，抽到不到一半见老爸来了就赶紧把烟灭了。老爸过来就一巴掌，接着吼道:‘个败家玩意儿，一半不到你就把烟灭了！’');
INSERT INTO `tpshop_joke` VALUES ('1194', '朋友有一空间相册有问题密码，问题是：”你是不是傻逼？”我回答是，系统告诉我密码不正确，我一气之下又接着换了，“我是傻逼”，“恩”，“我真的是傻逼”，“我要怎么说你才相信”等字样，最后实在坚持不下去了，第二天问朋友，他特么居然告诉我密码是“不是”。。。。。。。kao。。');
INSERT INTO `tpshop_joke` VALUES ('1195', '今天去狗市买狗，刚进去狗贩子就说大哥买狗吧，看我这狗咋样3800。太贵了了，我再看看。别啊大哥，我这狗聪明，起来给大哥学个瘸腿走路。小狗爬起来学的真像，当时就掏钱买了，现在都到家半天了这狗咋还一直学瘸腿走路呢。。');
INSERT INTO `tpshop_joke` VALUES ('1196', '今天买早点，发现早点摊位旁的墙上贴着一张租房的告示，上面写着，本人有一套3室居房出租，精装全配，1元/月.带小孩的不租，有老人的不租，夫妻不租，小情侣不租，男孩不租。然后是电话号码和联系人。 我去这是不是房东的泡妞新技能');
INSERT INTO `tpshop_joke` VALUES ('1197', '话说我叫樊木青 一天，网友和我聊天： 饭母亲在不？ 樊木请 情 我去 请 草！ 青 返拇心 樊木青 你名字真难打出来');
INSERT INTO `tpshop_joke` VALUES ('1198', '老婆生病了，吃不下东西，眼看要瘦了，我很着急。照这样下去，又得花钱买衣服了。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('1199', '人，最大的破产是信用破产 ! 哪怕一无所有， 只要信用还在， 就有翻身的本金， 保护好信用， 珍惜别人给的每一次信任， 因为有时候机会只有一次。不是朋友不给力 ，而是你的信用度已经不支撑你的行为或要求！ 朋友有时候就像钞票， 有真也有假，');
INSERT INTO `tpshop_joke` VALUES ('1200', '老妈问我：你对花的了解怎么样？我：还行，怎么了？老妈：藤、泽、苍、本是什么花？为什么好多人在网上求种子？');
INSERT INTO `tpshop_joke` VALUES ('1201', '　　商业街上人声鼎沸的，做买的做卖得摩肩接踵好不热闹。各种广告促销的声音夹杂在其中，有些吵人了。一个美眉在街上走着，被一个小伙子拦下了：“小姐，你好。请进来看一看，化妆品全场打折……”“不用了。我不缺那些……”“没事儿进来看看吧，女生嘛，就应该好好保养自己的。”美眉不为所动：“我有最好的化妆品……”小伙子不死心：“进来看一下，说不定有你用的那款呢？”美眉：“你们这里肯定没有我用的。”小伙子最后的挣扎：“你没看怎么知道没有你用的那款，看一下吧。对了，你用的是什么？”美眉回答：“美图秀秀。”');
INSERT INTO `tpshop_joke` VALUES ('1202', '五年级作文——“三十年后的我” 一孩子写道：“今天天气不错，开着老公送的劳斯莱斯，戴着3克拉大钻戒，上挂着红宝石项链，带孩子到大森林公园去玩。突然路上冲出个浑身恶臭、满脸污秽、无家可归的老太太。天啊！她竟是我语文老师!” 老师评语：这星期你站着上课！！');
INSERT INTO `tpshop_joke` VALUES ('1203', '“奶奶，你出去买瓶醋怎么这么久才回来啊”“今天学雷锋的人太多了，就门前那个红绿灯我就走了十八个来回，刚到对面就又被拉回来了。”“那您是怎么回来的丫”“因为走了太久，我最后体力不支摔倒了，那排队准备扶我过马路的十几个人就全都散了，我才一路狂奔回来~”');
INSERT INTO `tpshop_joke` VALUES ('1204', '‍‍处长：“机关实行首问责任制了，如果有人来办事，你的第一句话应该怎么说？” 小王：“不知道。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1205', '‍‍老婆：“亲爱滴，三八节都过了，你为啥没送俺礼物啊？” 老公：“亲爱滴，俺送你花了，你没瞅见吗？” 老婆：“哪有花？” 老公：“俺就放厨房桌子上啊！” 老婆：“韭菜花啊！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1206', '又降温了！丢不丢人。晒不晒脸！让辽宁怎么看吉林！让黑龙江怎么看吉林！让吉林以后在东北三省怎么混！和谁赛脸呢？今天20度明天3度！冬天你走不走了，夏天你来不来了？你俩处对象呢？恋恋不舍的，磨磨唧唧的，整的这天忽冷忽热的。上午穿半袖，中午光膀子，下午穿棉袄，明天不知道还穿啥！我让你们折腾稀碎，鼻涕拉瞎的，又感冒又发烧的，你俩赶紧确定关系，给个痛快话，明天我应该穿啥？ ？？');
INSERT INTO `tpshop_joke` VALUES ('1207', '一个老师跟我们说：这个学期有16周 过了4周，还剩多少周?还剩两个月！…… 16周-4周=12周 12周=3个月⊙_⊙两个月?！ 这个老师，她教我数学(￣∇￣)');
INSERT INTO `tpshop_joke` VALUES ('1208', '‍‍老公：“亲爱滴，咱甭看言情剧了，行不？” 老婆：“为啥？” 老公：“每次你都哭滴稀里哗啦，忒浪费！” 老婆：“俺浪费俺滴泪，关你毛事？” 老公：“俺说滴是面巾纸！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1209', '‍‍老婆：“亲爱滴，你总去那一家菜摊买菜，买菜滴时候还价不？” 老公：“不，俺有时候还，有时候不还！” 老婆：“不是同一家菜摊子？你这是为啥？” 老公：“同一菜摊但不是同一卖菜滴啊！俺碰到卖菜滴是男滴俺就跟他还价，碰见是女滴俺就不还。” 老婆：“你是看人漂亮动心了吧！” 老公：“动啥心啊？那女滴生滴猪圆玉润跟你特像，俺看见她就像看见你，俺哪还敢还价？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1210', '刚刚有个傻冒告诉我鲁迅姓周，真逗啊！周迅是个演员好吗？笑死我了！真想一板砖呼死他！我记得鲁迅原名李大钊，浙江周树人，是著名的反法西斯音乐家，一生有2000多项发明，被称为太空步的创始人。他拥有一个好嗓子，小学时就凭借着90分钟跑100米的优异成绩考上了新东方烹饪学校！毕业后成功进入富士康苦心练习勃鸡， 他擅长110米栏，左手反打技术高超，拿手全垒打，大灌篮，“后空翻180度右旋体360度后蹬地翻转720度”是他的经典动作，更难得可贵的是他落地没有水花。他还是恶魔果实能力者，传说中的三忍之一，曾大闹天宫，后改邪归正，统一三国，传说他有107个弟兄，个个铜头铁臂，面目狰狞，这便是羊村的起源，她生平淡泊名利，后遇到高人阿凡达的指点，打死了白雪公主，与七个小矮人快乐的生活在一起。！并写了名侦探柯南的故事。名侦探柯南讲述的是要成为海贼王的八神太一收服了皮卡丘并登上创界山启动光能使者打败了鲨鱼辣椒，然后跟多啦A梦一起通过黄金十二宫收集七个葫芦娃召唤神龙复活二代火影，但最终为了保卫M78星云而成为了羊村村长，同蓝精灵们一起抵抗光头强的入侵的故事 。');
INSERT INTO `tpshop_joke` VALUES ('1211', '以前高中的时候我们宿舍一哥们儿的老爸退休在家，有点迷信吧，每天干什么都要看黄历某天此哥们儿没钱了，给老头打电话要钱，电话通了，这边喂了半天，那边也没反应，然后就挂了，等了一会儿过来一条短信：“今日忌说话，有啥子事”，我们集体黑线');
INSERT INTO `tpshop_joke` VALUES ('1212', '某人饲养多年的宠物狗走丢了,于是在网上悬赏百万房产寻狗。没几天,爱犬却自己回来了，还带了一只母狗。主人大喜,但爱犬无论如何却不肯进家门。主人正大惑不解,:狗冷笑道:\"房子呢?没房子我回来干嘛?\".');
INSERT INTO `tpshop_joke` VALUES ('1213', '老婆1米6,lz182，经常说老婆矮，今天早上又说了一次，那二货婆娘直接来一句，老娘就算1米3，你还不是要跟老娘中间对齐，，，这次我真的是醉了，，');
INSERT INTO `tpshop_joke` VALUES ('1214', '从前有个国王很爱美，有俩裁缝给国王做了件世上最美的衣服，说只有聪明的人才能看见这衣服，愚蠢的人看不到，国王为了证明自己不是愚蠢的人，说了谎，第二天游街后国王宣布，给全国漂亮的姑娘没人做一件……');
INSERT INTO `tpshop_joke` VALUES ('1215', '今天我们老师批评我们，说我们整天吵闹，没有个好的学习环境，就无法学习好，还给我们讲起了孟母三迁的故事。\r\n老师讲到孟母最后搬家到学校附近后，孟子终于开始好好学习了。\r\n这时候我反驳道：“那是孟子没活在现在啊，不然孟子他妈可哭惨了啊！”\r\n老师愣愣的说道：“为什么啊！”\r\n我笑着说道：“因为现在学校附近都是打KISS，谈恋爱，拜金的啊，即便孟子长得丑不早恋，他也肯定会学会打DOTA的啊！”');
INSERT INTO `tpshop_joke` VALUES ('1216', '刚上高一没几天.和同学去上网.有个不认识的女孩和我搭讪.说:美女把你Q Q号给我呗！我的一句话给她惊呆了…\r\n我说:给你 那我用什么？');
INSERT INTO `tpshop_joke` VALUES ('1217', '楼主今天坐在宿舍休息，进来一同事进来对我说:走，哥们！俺今天刚发现一家全国都有分店的饭馆。请你吃饭。我心想平常这个人看着挺小气的想不到还不错.就问到:饭馆叫什么名字？那哥们的答案雷到我了…………沙县小吃');
INSERT INTO `tpshop_joke` VALUES ('1218', '1、等红灯时，一女子不顾红灯径直往前走，这时男朋友说：没看到红灯嘛，别闯！女子愤怒的回到：老娘我红灯的时候你少闯了吗？\r\n2、跟女朋友热恋当中，一天我问：我平时应酬多，要是喝醉了回家你会让我进去吗？女朋友果断地说：想的美！亲嘴都不让……\r\n3、邻居一小盆友回家哭着对妈妈说“妈妈，我不想跟爸爸姓程，我不要叫程管，同学每天都笑话我”“那好吧，傻孩子别哭，你就跟我姓鲁吧”……');
INSERT INTO `tpshop_joke` VALUES ('1219', '4、今天中午在食堂吃饭，边吃边玩手机。这时，一个找不到空桌的美女坐在了我对面。顿时我紧张起来，但为了不失态，强装镇定，本想文雅地吃一口饭，可一激动，把手机放嘴里了……\r\n5、一日和一女神在网上聊天，女神问：“为什么喜欢我？”我回复：“人生难得一只鸡。”女神的头像马上就灰了。我想说的是人生难得一知己，尼玛，万恶的输入法……');
INSERT INTO `tpshop_joke` VALUES ('1220', '老婆拽着我衣服撒娇道：“老公，我想穿貂。老公，我那高跟鞋也坏了哎！我想买双AJ！包包也裂了哎，换新的好不好！”“媳妇我给你冲个红钻吧？');
INSERT INTO `tpshop_joke` VALUES ('1221', '今天和女朋友在商场，她说要买包包我说上个月刚买不买了。然后她不高兴的大声给我说话，人挺多的，弄得我很没面子。 然后我也不舒服了 我说: 你再凶，你再凶我不要你了！ 她说：我没凶。 结果路人幽幽的来了句:没胸就更不能要了！');
INSERT INTO `tpshop_joke` VALUES ('1222', '一位旅客从后面拍了一下司机喊：就在这里停车。司机尖叫一声，差点撞上前面的公交车，冲过人行道才停下来。旅客讲：对不起，这一拍吓着你了。司机讲；是你吓着我了，我今天第一天开出租车，以前我开的是灵车，现在改行了。');
INSERT INTO `tpshop_joke` VALUES ('1223', '‍‍‍‍唐僧：“这八戒天天去泡妞，也不好好取经，你去把他举报了！” 悟空：“好勒！” 第二天…… 唐僧：“怎么样，好了吗？” 悟空：“嘿嘿，还不错，就是有点紧。” 唐僧：“好像没有什么不对的……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1224', '‍‍一天晚上，我躺在女友胸上，跟他说：“老婆，我躺在你胸上，打一词语。” 女友不知。 我说：“平板电（垫）脑。” 然后，晚上我就睡沙发了。‍‍');
INSERT INTO `tpshop_joke` VALUES ('1225', '应该把结婚证调整一下，像驾驶证一样共12分 出轨搞外遇扣6分罚十万 去KTV泡妞扣2分罚一万 在外面有不三不四的关系扣3分罚五万 打老婆扣6分罚十万，发现两次外遇，直接吊销结婚证，没收个人财产，终生不得再娶。');
INSERT INTO `tpshop_joke` VALUES ('1226', '一对夫妇避孕失败后，生了一个小男孩 一出生就握紧拳头，哈哈哈的笑个不停，当医生把拳头掰开，发现里面有一把避孕药，这时小男孩开口说话了，你们两个想弄死我，没那么容易，哈哈哈。。。。。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('1227', '‍‍‍‍昨天，女友说：“你脸好大。” 我说：“你脸才大，要不比一比。” 她问：“怎么比？” 我说：“把脸浸水盆里，看谁溢出的多。” 她说：“不行，你会喝。” 我：“…………”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1228', '上课时看电子书，过了一会老师来了，把我手机没收了，她看见我的通讯录里有个备注叫畜生，于是老师疑惑的拨了过去，这时，老师的手机响了。。。。。');
INSERT INTO `tpshop_joke` VALUES ('1229', '‍‍‍‍在公交车上，一个萝莉对着站在一边的男子说道：“校长，我想吃哈根达斯。” 话音刚落，周围的人一起看向男子，有的拿出手机要报警。 这时，男子尴尬地说：“你这熊孩子，在学校叫校长，出了学校应该叫爸爸！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1230', '我带男朋友回家，在家吃了顿饭，我偷偷和我妈说他有点胖，你要是觉得不合适你就和他说，我们认识也不久，没事的。我妈说：你凭什么说我女婿胖？我。。。');
INSERT INTO `tpshop_joke` VALUES ('1231', '贤弟，此次西行取经路上，怕是妖魔横行，朕送你一件宝物，可以收妖。 说完，太宗从身后的侍卫处取出一件衣物披在唐僧身上，对他说道： 这件小西服，收腰的');
INSERT INTO `tpshop_joke` VALUES ('1232', '一位妹子在空间发说说内容如下：肉肉！离开我的腰！有本事冲胸来！好有志气');
INSERT INTO `tpshop_joke` VALUES ('1233', '‍‍‍‍农场里面一只小鸡从鸡舍里走出来小鸡问道：“主人呢？” 猪：“去市场了。” 鸡：“主人去市场干嘛。” 猪：“买蘑菇去了。” 鸡撒腿就跑。 猪：“你跑什么？” 鸡：“主人去买粉条你跑的比我还快。” 牛：“咱俩一块跑吧，听说主人还带回来一个专家呢！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1234', '小朋友A：你再欺负我，我就让我爸揍你！小朋友B：那我就让我爸揍你爸！小朋友A：那我就让我妈揍你爸！我爸说了，天下男人都怕老婆！小朋友A：那我就我妈揍你妈！我爸说了要对付38还得要38。。。?');
INSERT INTO `tpshop_joke` VALUES ('1235', '‍‍‍‍乞丐找到一个小贩，他说：“哎，我要向您提一个最难的问题，如果一个人饿了，他又身无分文，那该怎么办呢？” 小贩想了想，给他几个硬币。 第二天，乞丐又找到小贩：“我有个最难最难的问题。” 小贩：“是不是昨天的那个问题？” 乞丐：“是的，是的。” 小贩：“我不是已经告诉你答案了吗？” 乞丐笑了笑说：“是的，不过，当我回家后，我本想好好地领会您的答案，可您的答案已经不在了，现在想请您再给我一个昨天的答案……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1236', '一天，我偷偷传给我旁边男同学一张纸条，问他，你知道10到13岁的男生不能对10到13女生说什么吗？他说，是我爱你吗？我说不是，他要我告诉他答案，我旁边的同学默默地说，笨蛋！不就是你发育了吗？嘛！笨死了');
INSERT INTO `tpshop_joke` VALUES ('1237', '‍‍‍‍“是什么让你们坚持了十年爱情长跑最终步入婚姻殿堂？” “读初中的时候，大家都喜欢吃辣条，但是只有他吃完从来不舔手指，就直接用纸巾擦掉，这种土豪行为让我爱上他了……”');
INSERT INTO `tpshop_joke` VALUES ('1238', '这问题有过多少女朋友就被问过多少次。新交一女友也问我：“我和你妈掉水里了，你先救谁。” 我说：“我让我妈救你，她小时候能在长江两岸游两个来回。” 女友有些不高兴，说：“你为什么不下来救我?” 我说：“我要是下来了，你就没救了，因为我不会游泳，我妈肯定先救我。”');
INSERT INTO `tpshop_joke` VALUES ('1239', '高中时，学校规定男女之间晚上在操场的距离不能小于20cm，否则抓到当作早恋直接通知双方父母。前阵子回母校，和以前老师聊天，谈笑中提起那20cm制度，老师说：“现在社会进步，学校思想也改进了。”我点了点头，刚想说早就该取消了，老师又接着说：“以前是男女距离不小于20cm，现在不限定性别。”');
INSERT INTO `tpshop_joke` VALUES ('1240', '有匹小马要过河，老水牛见了对他说“别怕，水很浅，只到了我的膝盖” 小松鼠立刻跑了过来喊“别听他的，水很深，我的朋友就是淹死的” 小马不知道该听谁的，旁边的马妈妈告诉他“孩子，别理那俩神经病，咱们走桥！');
INSERT INTO `tpshop_joke` VALUES ('1241', '　　小明被欺负了，然后他去找他的好兄弟 　　好兄弟来报仇了，看来气势 很庞大。 　　没想到，开始了，兄弟竟然和他一起石头剪刀布。 　　我当场就晕倒了');
INSERT INTO `tpshop_joke` VALUES ('1242', '今儿，和朋友一起出去玩。朋友扯了我一下衣服，我问她干嘛，他说扯犊子，我特么就怒了，踹了她一脚，差点摔着，说了一句滚犊子，她男朋友，就赶紧过来扶了他一下。旁边一损友说了一句，护犊子是么。。。护犊子。。。于是，一场关于犊子的战争就这样爆发了。。');
INSERT INTO `tpshop_joke` VALUES ('1243', '家有六岁萝莉一枚。妹妹，前两天迁坟，LZ农村的，原来是直接埋山上，土地征用迁到公墓，迁完以后。妹妹直接跪下，边磕头边说。爷爷您搬家了，终于不住土房子了。搬小区房了。家里人彻底凌乱在风中。。。。大婶给过吧。');
INSERT INTO `tpshop_joke` VALUES ('1244', '同事聚餐，上了一盘红烧猪脸，肉基本吃完了，因为点的菜多，服务员来收空盘子。高潮来了。。。一同事想把盛猪脸的盘子留在自己跟前方便夹菜，就跟服务员说:剩下的猪脸倒掉，盘子留给我用。二货服务员想都没想就接了句:脸都不要了还要盘子干啥?顿时全桌人都笑喷了。');
INSERT INTO `tpshop_joke` VALUES ('1245', '　　一朋友升职了。请我们吃饭，朋友在公司表现一直很普通。就问他是不是被潜规则了。他说上司是个男人能升职全靠他那张嘴。次奥！嘴？有些不对劲。还是我想多了');
INSERT INTO `tpshop_joke` VALUES ('1246', '上大学的时候跟女朋友一起去吃麻辣烫，遇一女，点完菜，装碗的时候说，师傅，我不要葱，姜，蒜，不要辣椒，当时师傅就傻了。完了还神补刀，你还是拿白开水给我装吧。我一想，美女你只是来吃个烫的嘛？');
INSERT INTO `tpshop_joke` VALUES ('1247', '　　每个年纪的三大欲望：小学生：去玩、吃点心、小裤裤。初中生：拯救世界、被黑暗组织追杀、解除右手的封印。高中生：啪啪啪 啪啪啪 啪啪啪。大学生：要学分、找工作、想回家。社会人：想回家 想回家 想回家。');
INSERT INTO `tpshop_joke` VALUES ('1248', '　　拿到公司的体检报告，报告里有一项：膀胱未见充盈，当时吓坏了，赶紧百度又电话咨询医生朋友，最后总结原因是早晨体检时候没有憋尿，回想也的确，尿检时尿都差点不够。');
INSERT INTO `tpshop_joke` VALUES ('1249', '在淘宝上看到一神评价～～从心底默默问候店主母亲很多次了，出差去酒店住了一晚后，枕头上，被子上，床单上都被染了黑色，买了你一身保暖内衣才50多块钱，我特么赔了酒店两百多块钱！');
INSERT INTO `tpshop_joke` VALUES ('1250', '昨晚跟一美女一夜情，早上醒来发现上班要迟到了，三两下穿起衣服就走了，到公司感觉下身奇痒，跑厕所一看发现穿错内裤，随即打电话给那美女问她是否有病，电话那头“你TM才有病，变态，钱不给钱还偷内裤…”');
INSERT INTO `tpshop_joke` VALUES ('1251', '史蒂夫在酒吧的一个角落里，手里拿着一瓶酒，看上去很失落，老板见了，走上去安慰他，说：“怎么了？史蒂夫？”　　史蒂夫长叹一声：“我上次和我那凶八婆老婆吵了一架，她发誓一个月不理我。”　　“那不是好事吗？？你还丧气。”老板疑惑不解。　　他又长叹一声，说：“那是四星期以前的事，今天是最后一天。”');
INSERT INTO `tpshop_joke` VALUES ('1252', '今夜雨蒙蒙，伸手不见五指，路经垃圾堆，只见具皮肤苍白孩童静静躺之，惊呼，又定眼一看，失声骂之，哪个杀千刀的把布娃娃乱扔…');
INSERT INTO `tpshop_joke` VALUES ('1253', '那天闲来无事，往男友怀里钻。 　　正在看球赛的男友问：“你这是干什么呢？”　　我说：“小鸟依人你不懂啊？”　　男友：“就你这人高马大的还小鸟依人，鸵鸟还差不多。”　　我……');
INSERT INTO `tpshop_joke` VALUES ('1254', '某人打电话给路灯管理所，他说，“这里一盏路灯坏了，修理它不会很麻烦，因为我只要一踢灯柱，灯就亮了。”　　管理所职员：“很难确定什么时候派人去修理，不过，如果你能每晚把灯踢亮，我们可以让你在管理所兼职，并免费提供一双皮鞋。”');
INSERT INTO `tpshop_joke` VALUES ('1255', '一艘帆船返回了港口。船长怒冲冲地对气象员说：“按照你的气象预报，我们满以为是北风，但实际上吹的是南风！”　　气象员平静地说，“这是因为北风又转回来了。”');
INSERT INTO `tpshop_joke` VALUES ('1256', '朋友是幼教，我偶尔去她们幼儿园玩。　　前两天园里来了个特别帅的小男孩，我很喜欢，抱着就亲亲。　　小帅哥说：“姐姐我都被你亲过了，你跟我结婚吧？”　　“可以啊。可是你这么小，等你长大姐姐就老了，到时候你会嫌弃姐姐的。”　　小帅哥语出惊人：“没事，到时候我就把你给我爸爸，他会要的！”');
INSERT INTO `tpshop_joke` VALUES ('1257', '公交司机：你坐的是老幼病残孕专座！　　男青年：我知道。　　公交司机仔细打量男青年，一头雾水地问：你身上哪儿都不缺怎么残疾了？ 　　男青年：我没阑尾！');
INSERT INTO `tpshop_joke` VALUES ('1258', '大二有次考数学，学了半年也没学明白的东西。卷子前面2张勉强涂了40分，卷子第三张的题目我都没看懂。快交卷了也没抄到，一咬牙，把卷子第三张撕了下来，交了前面2张，出考场。　　等成绩公布，60分。估计老师没找到第三张卷子，又怕担弄丢卷子的责任。我就这样过了。');
INSERT INTO `tpshop_joke` VALUES ('1259', '话说现在很多人都把子说成纸，一女同学经常这样卖萌。　　一天该女感冒了在图书馆纸用完了，于是发短信给室友，说：过来的时候带两包纸。不一会儿室友过来了，手里拿着两个包子。');
INSERT INTO `tpshop_joke` VALUES ('1260', '某日去ATM取钱，是那种银行旁边房间里的ATM机器，那个房间门我用尽全身力气，无论是推还是拉就是打不开。愤怒的我对着门使劲踹了一脚，还大喊一声FXXK！　　正欲离去，旁边走来一美女，用看SB的眼神上下打量了我一番，然后华丽的掏出银行卡，在旁边的机器上刷了一下，于是门就开了。');
INSERT INTO `tpshop_joke` VALUES ('1261', '和五岁宝宝站公路旁等公交，这时一张越野猎豹车使过。宝宝说：爸爸，那车后怎么背着一个包呀？我：宝贝儿，那是备胎！宝宝：我妈妈和王叔叔也是这样说你，那妈妈怎么背着你！我：走，我们不去爷爷奶奶家了，买把砍刀，回家砍现用胎去！');
INSERT INTO `tpshop_joke` VALUES ('1262', '有哪些被当做灵异事件，后来用科学否定的？ “初中时男生们传言女厕所闹鬼，据说全是血。。”');
INSERT INTO `tpshop_joke` VALUES ('1263', '一天一日叫旧，一天二日叫昌，一天三日叫晶，一天日九回叫旭，日一整天叫昊，日一个月叫明，站着日叫音，关了门日叫间，日不进去叫白，日弯了叫电，日软了叫巴，从上往下日叫由，从下往上日叫甲，日穿日叫申，男女都不日叫双休日，说要日却不日叫假日。');
INSERT INTO `tpshop_joke` VALUES ('1264', '真事不割！上几天大学同学聚会，我们男生喝了好多酒，我喝断片了。第二天醒来发现我在旅店，脱的一丝不挂，旁边睡着全班最丑的女生。。。。。。什么情况这是！');
INSERT INTO `tpshop_joke` VALUES ('1265', '　　爸爸给3岁的女儿洗澡，刚把女儿放进水盆，女儿就大叫：“妈妈快看，爸爸泡妞啦。”');
INSERT INTO `tpshop_joke` VALUES ('1266', '都说曾经初恋的青涩,LZ也说说自己的青涩，割了吧，，，话说初恋是初中同学，高一的时候星期五下晚自习，几个初中同学就在马路上压马路，聊天聊的比较晚，初恋房东关门回不去了，艾玛我激动半天说去我那，结果也关门了，最后特么在体育场的草地上抱着初恋看了一晚上星星，，，那憋的，，，走的时候我真切的感觉到了蛋疼');
INSERT INTO `tpshop_joke` VALUES ('1267', '我很喜欢这段话！ “钱”离开人，废纸一张；人离开“钱”，废物一个。鹰，不需鼓掌，也在飞翔。小草，没人心疼，也在成长。深山的野花，没人欣赏，也在芬芳。做事不需人人都理解，只需尽心尽力，做人不需人人都喜欢，只需坦坦荡荡。坚持，注定有孤独彷徨，质疑嘲笑，也都无妨。就算遍体鳞伤，也要撑起坚强，其实一世并不长，既然来了，就要活得漂亮！ ——致自己');
INSERT INTO `tpshop_joke` VALUES ('1268', '今天我去买猪肉，这块肉摸摸，那块肉摸摸，一块都没买，最后赶快跑回家，在我家水缸里洗了个手，太开心了，一个星期都能喝到猪肉汤了。');
INSERT INTO `tpshop_joke` VALUES ('1269', '一帅哥学长朋友圈发了条：以后要找一个替我照顾孩子，给我孩子洗衣做饭，每天在家里做家务的人！一暗恋已久的妹子看到后立刻评论说：我愿意！学长：好的，以后你做我家保姆！');
INSERT INTO `tpshop_joke` VALUES ('1270', '初中的时候帮哥们送情书给隔壁班女生，女生拿过去之后看都没看一眼，就扔地上了。。。我连忙说不是我写的，她才把信捡起来。。。');
INSERT INTO `tpshop_joke` VALUES ('1271', '同事问我，在网吧上网遇到了流氓怎么办。我说，喊人报警。她说，大半夜周围没有人，报警的手机也被抢走了。我说，别穿的暴露性感就不会招惹流氓了。她说不行我喜欢露肉的打扮。我说，那你应该掏出身份证去开房。你穿的暴露性感，然后还要去一个人没有的地方。目的到底是什么，是不是去找去流氓了？');
INSERT INTO `tpshop_joke` VALUES ('1272', '‍‍‍‍读书的时候，每逢考试，总能遇到一个奇葩老师……… 一次期末考，一个老师进来后在黑板上写了仨大字：“快！静！齐！” 然后转过身对我们说：“抄的快一点，抄的安静一点，最重要的是抄的整齐点！我去给你们放风。” 说完，他就找了一张凳子到课室门口坐着了………‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1273', '　　姐夫:大吗？ 小姨子:嗯…大！ 姐夫:喜欢吗？ 小姨子:嗯…嗯…我好喜欢！ 姐夫:感觉怎么样？ 小姨子:好好吃！我想天天要。 姐夫:把腿分开点，别把衣服弄脏了！ 小姨子:嗯，嗯…嗯… 姐夫:今天这事，别让别人知道。 小姨子:我又不是外人，你怕什么？ 姐夫:要让你姐知道了，我就死定了！ 小姨子:有那么严重吗？ 姐夫:你姐对我管的很严，我是用私房钱给你买的雪糕！');
INSERT INTO `tpshop_joke` VALUES ('1274', '“你好，是送水的吗？” “是呀！” “能给我送桶水吗？” “女士，你看看现在是几点！你有见过大半夜1点多送水的吗？” “哎呀，我急呀！拜托，拜托！” “明白了！有认识的送水工吗？” “4号大哥...”');
INSERT INTO `tpshop_joke` VALUES ('1275', '‍‍今天去理发店，工作人员不停地向我介绍，要我办张会员卡。 我火了就骂了他一句。 他对我说了句话，让我气乐了。 他说：“我们店里只有会员才能骂人呢！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1276', '‍‍朋友一边抽烟，一边咳。 我就劝他：“你就不能把烟戒掉？” 他说：“不能戒啊！戒了要出人命的。” 我：“你就那么嗜烟如命吗？” 他说：“不是，你嫂子说了，如果我这辈子能把烟戒了，她就去死。” 我：“……”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1277', '今天听见办公室里那帮穷屌丝在讨论女人变心的话题，他们认为如果女人变了心，哪怕用铁链子都拴不住。结果领导走过，笑了笑说：你换根金链子试试。集体沉默……');
INSERT INTO `tpshop_joke` VALUES ('1278', '‍‍“唉，昨天到北京探亲，雾霾太大，闯了几个红灯！这下惨了！” 朋友劝道：“没事的，电子眼也拍不清楚车牌的。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1279', '外甥女摸着自己的肚子说：“舅舅，我还想吃。” 我笑着说：“想吃就吃，你舅妈这手艺一点儿也不比大饭店的差。” 老婆看外甥女这么爱吃，高兴地说：“能吃是好事。” 儿子开口了，道：“等你吃成我妈这样的大胖子，嫁不出去就该愁了。” 老婆听了这个气啊。 外甥女却不以为然地说：“怎么嫁不出去，舅妈不是嫁给舅舅了吗？” “就是。”老婆给外甥女又端了碗饭说，“别听你哥胡说，该吃吃。” 可饭摆在她面前，她犹豫了一下，问：“那我要是吃胖了，是不是只有像舅舅这样的才肯娶啊？要是那样，我还是不吃了。”');
INSERT INTO `tpshop_joke` VALUES ('1280', '‍‍‍‍今天，一同事带小孩到公司玩，非要叫我爸爸。 我逗她：“叫爸爸有什么好处？” 她说：“爸爸可以跟妈妈睡觉！” 叔叔真没白疼你，这幸福也来得太突然了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1281', '退休工人李大妈坐公交，上车就有人让座。她坐下，又站起，把座位让给了一个小朋友。马上又有人让座，大妈毫不犹豫，让一个比她还老的老人坐了。大家投来激赏目光，纷纷让座。大妈说上班族不易，把一男一女两个疲惫的年轻人按到座位上，最后才安心坐定。这样，李大妈一家五口，都找到了座位');
INSERT INTO `tpshop_joke` VALUES ('1282', '闺蜜刚才跟我说：“等我结婚后，我一定要问问我老公是有多饥不择食，连我这样缺心眼的都要。。。”');
INSERT INTO `tpshop_joke` VALUES ('1283', '昨天和媳妇还有她同事吃饭，饭局间我说：美女拍个照，媳妇害羞挡住了脸，我淡淡的说，你让开我给美女拍个照。。。');
INSERT INTO `tpshop_joke` VALUES ('1284', '关于哄老婆开心，有人讲笑话，有人买钻戒，而我两者皆有：“我给你买了个钻戒。哈哈当然是开玩笑的啦。”');
INSERT INTO `tpshop_joke` VALUES ('1285', '今天去相亲，女孩问我：你是做什么工作的？我：研究员。女孩：哦？研究什么的？我：各种常见与非常见材料在高温下的分子重新排列及组合。女孩：具体点，听不太懂。我：厨师，炒菜的。女孩：……');
INSERT INTO `tpshop_joke` VALUES ('1286', '正月，一屋几代人同堂，一叔辈拿着一叠十元钞票戏小一辈八岁正太“叫一声爸给十块”。小正太一连十几声爸，钱都给完了，根本停不下来，这时亲爸盯了小正太一眼，示意差不多了。小正太可能感觉到有点过了说道:你叫我一声爸我退你十块......一阵笑声过后接着亲爸一顿胖揍。');
INSERT INTO `tpshop_joke` VALUES ('1287', '我如当年讨厌老师拖堂一样的烦着公司加班。但是再也没有了站起来说不愿意的勇气。我草，多么痛的领悟。。。。');
INSERT INTO `tpshop_joke` VALUES ('1288', '丈夫：“谢谢你在邻居面前夸我有才华。”\r\n妻子说：“你要钱没钱，要貌没貌，要地位没地位，我要不说你有才华，别人还不得骂我傻啊。”');
INSERT INTO `tpshop_joke` VALUES ('1289', '随着年龄的增长，能看淡的和无所谓的事儿就会越来越多。这是个好现象，不再因为别人说你两句就气得跳脚，也不会因为丢了钱包和手机就惊慌失措，哪怕是失去一个很爱的人，也就那样了。毕竟这些都是曾经许多个丢脸的时刻换来的，在人前保全体面和自尊，太重要了。');
INSERT INTO `tpshop_joke` VALUES ('1290', '看到哈密瓜十五块一个，好大一个，就去挑了一个递给老板娘，老板娘居然说：快来挑快来选啊！我无语就打断老板娘的话：我说阿姨，我都挑了你就不用喊口号了吧！然后旁边卖袜子的帅哥都笑了，老板娘说这是卖袜子的口号');
INSERT INTO `tpshop_joke` VALUES ('1291', '本人男，又高又瘦，所以一群朋友都叫我竹竿，我每次听到都很郁闷。最近他们叫我宰相了，让我小小窃喜了一把，他们终于发现我不喜欢这个外号了。有一次我忍不住问一个朋友，怎么改口叫我宰相了，这时朋友很淡定的说，他觉得竹竿太难听了，刚想谢谢他，接下来他说的一句话让我更不淡定了，他接着说，反正都是撑船的，宰相文雅多了。。。我去，感情我的船还得撑下去。。。');
INSERT INTO `tpshop_joke` VALUES ('1292', '老师：请问闺房三宝是哪三宝，小明，你来回答！ 小明：黄瓜，茄子，胡萝卜！ 老师：知道该怎么做了吧？ 小明：我这就滚！');
INSERT INTO `tpshop_joke` VALUES ('1293', '‍‍昨天午休时，同桌睡不着觉，就喊我帮其掏耳朵屎。 我小心翼翼的帮他挖着，我看到挖出来的都是湿的。 就嘴贱的问了一句：“为啥你耳屎过去是干的，现在是湿的。” 同桌不无揶揄的说：“还不是我脑子进水了才和你同桌造成的。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1294', '老公老远出差回来，累极了躺在床上，老婆含情脉脉地对老公说:“听说空置地不开发要交闲置费，除非找人合作开发。”，老公:“就算累死了也不让共同耕种呀!”');
INSERT INTO `tpshop_joke` VALUES ('1295', '老师说：“要把学校当自己的家”一学生说：“老师这是我家，请您出去”');
INSERT INTO `tpshop_joke` VALUES ('1296', '‍‍老婆：“亲爱滴，咱们买只鹦鹉吧！等俺出差时可以让它陪你说说话儿！” 老公：“亲爱滴，千万甭买！” 老婆：“为嘛呢？” 老公：“鹦鹉净瞎叫。俺男同事家就有一只鹦鹉，某天他正跟人办事儿，那鹦鹉猛地来了一嗓子‘老公，俺回来了！’结果俺那同事吓得从此不举了！” 老婆：“就冲这一嗓子，俺非买不可了！” 老公：“可怜俺滴……”');
INSERT INTO `tpshop_joke` VALUES ('1297', '‍‍蚂蚁在看电影，结果突然来了一只大象，坐在了他前面挡住了银幕。 蚂蚁生气地跑到大象前面的座位，扭头对大象说：“被我挡住了银幕，你现在很不爽吧！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1298', '　　冬天，在路上看见可爱的小萝莉不要调戏。。。大姐你听我说，我真没拿针扎你女儿脸，我只是摸了下。是静电！真的是静电。唉....');
INSERT INTO `tpshop_joke` VALUES ('1299', '精神病院院长在台上放了一只兔子第一个病人上去：“驾！驾！ ”第二个病人：“驾，快去追他！ ”院长看见第三个病人在那一直摸那只兔子，满意地点了点头。没想到那个病人却说：“小样，先让你们300米，等我擦完车再说。”');
INSERT INTO `tpshop_joke` VALUES ('1300', '由于本人怕冷，经常赖床，这几天为了能早起，闹钟一响果断一脚把被子踢开。 就这样坚持了3天，果然有效 ，不说了，我得去找医生了，打了2天点滴，感冒还没好。');
INSERT INTO `tpshop_joke` VALUES ('1301', '甲：你为什么买那么多的火锅底料，难道不知道里面含有石蜡吗？　　乙：我买火锅底料就是为了提炼里面的石蜡　　甲：你干脆直接买石蜡不得了！　　乙：你不知道，买的石蜡都是假冒伪劣，还不如提炼合算');
INSERT INTO `tpshop_joke` VALUES ('1302', '新婚夫妻在洞房花烛夜聊天，丈夫说：“今天，在妻子面前我庄严宣誓，海枯石烂不变心。”　　妻子说：“我们这里隔海很远，我哪里知道海里的事。”　　丈夫说：“我爱你到天老地荒。”　　妻子说：“天老了，地荒了，你就没有吃的，怎么有力气爱我？”　　丈夫说：“我加入妻子的丈夫，永不叛妻，为了妻子，随时随地牺牲自己。”&nbsp;');
INSERT INTO `tpshop_joke` VALUES ('1303', '儿子：爸，你知道李白被我们后人尊称为什么？　　爸爸：儿子，这我真不知道。　　儿子：诗仙。爸，你知道杜甫被我们后人尊称为什么？　　爸爸：儿子，这我也不知道。　　儿子：诗圣。爸，那你知道你被我“尊称”为什么？　　爸爸：尊称为什么？　　儿子：诗盲！');
INSERT INTO `tpshop_joke` VALUES ('1304', '古时一富二代仗势欺人、无恶不作，那日又在欺负穷人，秀才前去制止。富二代恶人先告状，说穷人惹他了。　　秀才责备穷人说：“你们怎么这么不知天高地厚，怎么敢惹‘二代’呀，你们知道他是谁吗？他就是孙悟空、三太子、二朗神。”　　富二代问秀才：“我怎么会是他们呢？”　　秀才说：“他们是神啊！他们多有能耐！”　　富二代一听挺高兴就回家了，穷人问秀才：“他欺负人你还把他当神？”　　秀才说：“说他是神，那他可就不是人了！”');
INSERT INTO `tpshop_joke` VALUES ('1305', '长裤跟内裤说：“我最爱护主人的面子，绝不能让主人春光泄露。”　　内裤说：“我最能体贴主人，始终守护主人的秘密。”　　长裤说：“主人不要你，照样安全。”　　内裤说：“主人不要你，更加迷人。”');
INSERT INTO `tpshop_joke` VALUES ('1306', '一小伙子总是自我否认，缺乏自信，成天闷闷不乐。　　一天小伙子来到寺庙拜见方丈高僧指点迷津。　　方丈问道：\"你来所谓何事？”　　小伙子说：“我相貌不行，智商也觉得比不上别人，我很痛苦.....”　　这时正面来了一个非主流男子，方丈一点那人，说道男即是女，女即是男。　　小伙子顿时明悟，一股强大的自信心涌上。');
INSERT INTO `tpshop_joke` VALUES ('1307', '我单位一同事非常吝啬，基本都是别人的东西让他用。　　有天他带了包饼干来，每天吃几片，我们开玩笑吃了一片，没想到他的饼干是数好的，结果我们赔了他一包。　　还有一次，一同事随手在他的牙签盒里拿了根牙签用，只见他自己拿了根牙签剔了牙齿后又放回去了，还说着“明天还能用”，后来再也没人敢用他的了。');
INSERT INTO `tpshop_joke` VALUES ('1308', '70年代，姑娘，叔叔请你吃棒棒糖-------------叔叔你真好！　　80年代，妹子，叔叔请你吃棒棒糖-------------你个骗子!　　90年代，美女，叔叔请你吃棒棒糖------------切，一晚上500，少了不干啊！');
INSERT INTO `tpshop_joke` VALUES ('1309', '一个漂亮姑娘对她的朋友说：“昨晚我男友说要给我画祼画，可真够刺激的。”　　“你又不是专业的模特儿，会摆姿势吗？！”　　姑娘说：“可他也不是专业的画家啊。”');
INSERT INTO `tpshop_joke` VALUES ('1310', '妻子对半夜溜下床的丈夫说：今晚你不要再患梦游症了，因为那个女人出国旅行去了。');
INSERT INTO `tpshop_joke` VALUES ('1311', 'LZ男，胖纸一枚体重一百八有余，有天正在做饭媳妇在后面放洗澡水，不知道怎么想的突然决定用屁股顶我一下。。。。。她才九十不到。结果，媳妇华丽丽的被反弹进了浴缸。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('1312', '前两天，老师打电话说我儿子考试语文98分、数学17分，孩子偏科太严重了。人无压力轻飘飘，我给他好一顿胖揍。还别说，这次考试就好多了，语文数学都17分。');
INSERT INTO `tpshop_joke` VALUES ('1313', '男:我快死了，你可以收养我的孩子么？ 女:什么？你快死了？你孩子多大了？ 男:他还小，还是颗细胞，你肯定能养活他！ 女:滚！');
INSERT INTO `tpshop_joke` VALUES ('1314', '　　一同事中午买了很多蛋糕，都去抓。。另一同事冒了句：再拿就给别个拿完了。。 　　同事说：没事，吃吧。反正也是给儿子买的。。。');
INSERT INTO `tpshop_joke` VALUES ('1315', '去男友家，客客气气地吃了顿饭以后男友妈妈悄悄把男友拉到一边说：“儿子啊，你眼光到底行不行啊，怎么比上次的那个还丑？”我顿时欲语泪千行，阿姨啊，其实，上次的那个也是我……');
INSERT INTO `tpshop_joke` VALUES ('1316', '原来最喜欢吃路边的米线，有天在家看新闻说吃一碗米线相当于吃三个塑料袋。正感到揪心呢，老妈旁边飘出一句：“没事的，你平常吃那么多垃圾食品，吃几个塑料袋装一下也好”这。。。。');
INSERT INTO `tpshop_joke` VALUES ('1317', '一天，一逗比舍友问他女朋友，如果给你一根棒棒冰你是喜欢舔着吃，还是含着吃？这个时候我们要求他打开免提，好奇的想知道他女朋友的回答，结果淡淡的从那边传来了“我喜欢掰开吃”舍友们。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('1318', '记得小时候喜欢村里一菇凉、 怎奈她看不上我、 不过我没有放弃、 一直追求着。困惑之际、 猛然看到电视上说：绑住女人心最好的办法就是让她怀上你的孩纸， 因此、 有一回趁她晚上回家途中、 我突然从草丛中钻了出来、 一把抱住她、 对着她的脸蛋上去就是一口、 然后快速逃离现场、 接着在家中计算着当爸的日子……');
INSERT INTO `tpshop_joke` VALUES ('1319', '一妹子为情所困，刎颈自杀，急救室医生救治后对她说：姑娘，人家自杀都是割动脉，你到好，把气管割了，怎么样，是不是觉得通气多了？');
INSERT INTO `tpshop_joke` VALUES ('1320', '这几天老婆生孩子花了不少钱，想找个亲戚借钱的都没有，还是隔壁的王大哥热心肠，又送奶粉又送营养品，嘘寒问暖的，唉！远亲真不如近邻，等儿子再大点，我让儿子认他做干爹。');
INSERT INTO `tpshop_joke` VALUES ('1321', '　　现在很多女孩子没有公主的命，却得了公主的病。 　　现在很多男孩子没有富二代的命，却得了富二代的病。 　　前者娇生惯养，后者装逼。除了吃喝拉撒,脑袋一片空白。');
INSERT INTO `tpshop_joke` VALUES ('1322', '一个男子看到一个修女很喜欢，就上前问约吗，修女害羞的跑了，只是路边扫地的大妈说小伙子你可以扮成神父就可以约她了，小伙子觉得不错就在月黑风高的晚上，带上面具扮成神父约修女，激情过后，小伙子摘下面具说哈哈我就是早上约你的人，修女摘下面具说哈哈我就是早上给你提建议的大妈。');
INSERT INTO `tpshop_joke` VALUES ('1323', '哥们我早恋，初一谈的女友 7年！后3年是同居的 感情不可谓不深！两年前遭遇富二代插足 短短两月就被分手！同年他们结婚了…至今不法自拔！今天是他们结婚2周年！这滋味谁懂？段友们教教我 我该怎么办？');
INSERT INTO `tpshop_joke` VALUES ('1324', '　　有一天表弟对我说：“哥哥我发现我最近一直偏科，语文六十多分。。。。”我说：“那要加吧劲学语文了呀，你数学多少分呢？”我弟唯唯诺诺地说：“才二十多。。。。”');
INSERT INTO `tpshop_joke` VALUES ('1325', '某日，蝴蝶看到了蚂蚁，告知，你这么宅，这么闷，谁会看上你啊，有没有考虑过以后啊？，蚂蚁答，有考虑过啊，可蚁后它看不上我啊。');
INSERT INTO `tpshop_joke` VALUES ('1326', '真事.一同学相亲那天晚上就在女方家睡了，第二天问他搞定没有.他说别提了，那女的死死抓住秋裤不放，老子拼了全身力气拔了她的秋裤.TM的里面还有一条秋裤...');
INSERT INTO `tpshop_joke` VALUES ('1327', '刚和老公谈恋爱那会儿，我们出去约会吃完饭，找个公园坐着，依偎在一起。有时他在我身上嗅啊嗅：亲爱的你好香哦。 我心里有鬼，轻轻的推他:那里会？ 他认真的说：有时候，我觉得你身上好香啊！嗯！今天就好香。 我无语:真是尴尬得要死，让我怎么告诉你，今天我来大 姨妈啊！');
INSERT INTO `tpshop_joke` VALUES ('1328', '　　还是lz，今天我们一家人团圆，姥姥包了饺子，我尝了一个，感觉还可以这是小姨叫我拿点醋，我想让弟弟拿不知怎的脑子一抽来了句，弟，饺子让你拿醋吃小姨，顿时，我感受到了来自小姨的杀气，家人们笑喷了');
INSERT INTO `tpshop_joke` VALUES ('1329', '“想我时候就打给我吧，这是我的号”。“这么长，是联通还是移动啊？”“工行”');
INSERT INTO `tpshop_joke` VALUES ('1330', '妈，我打工回来了，今年又没赚到钱，我一无所有…孩子，你并不是一无所有啊，你还带回来一身肉啊。你还有脸回来啊！你还有脸带着一身肉回来啊。');
INSERT INTO `tpshop_joke` VALUES ('1331', '‍‍‍‍一女近视，看东西模糊，但是从不愿戴眼镜。 谈了个男朋友，非要给她配副眼镜。 戴上眼镜，感叹到这个世界真清晰，然后对着男的说：“咱们分手吧！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1332', '老王在小区遛弯。突然一个避孕套砸在他的头上。还是刚用完的，里面还有不少液体。他抬头看看。就四楼窗户是开着的，他就去那家敲门。开门的是个中年人。老王问你家都谁在家呀。中年人说就他儿子和儿子女朋友在家。问老王有事吗？老王就把避孕套给了中年人。说：我把你孙子送来了');
INSERT INTO `tpshop_joke` VALUES ('1333', '‍‍‍‍小伙子开车，一手扶着方向盘，一手摸着副驾驶的妹纸大腿。 路边交警看到了，大喊：“喂，用双手！” 小伙子：“大哥你别逗了，我用双手的话，谁特么来扶方向盘？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1334', '在公园看大爷写字。大爷提笔就在纸上写了个“滚”字。我一看知道是大爷嫌我靠太近了，暗示我离远点呢。心中虽感不快，但毕竟是自己打扰人家，就后退了几步观看。谁知大爷又写了个“滚”字，我这火爆脾气上来了，抬腿一脚把他踹翻了。这下惊动了警察蜀黍，大爷说：我正要写“滚滚长江东逝水”，不知怎么就被这小伙子踹倒了。看着大伙询问的表情，警察蜀黍，我还是去你们那住几天吧。');
INSERT INTO `tpshop_joke` VALUES ('1335', '‍‍听说过街天桥那边有一个算命特别好的先生。 一天我终于碰到了他：“大师，您看看我的手相，啥时候能找到男朋友！” 大师瞅了我一眼，默默的底下头：“姑凉，看手相没用，这是个看脸的时代！‍‍”');
INSERT INTO `tpshop_joke` VALUES ('1336', '‍‍‍‍初中时生物是体育老师兼着，讲到两栖纲青蛙是体外受精。 班上一二货大声问老师：“我看到过青蛙抱对，宅怎么还是体外受精？” 老师解释不清，一下子就急眼了，一拍桌子，喊道：“那是极个别，不自觉的！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1337', '‍‍‍‍傻子：“大家都叫我傻子，没人和我同姓同名。” 小王：“别说了。” 傻子：“怎么了？” 小王：“学校不知道哪个傻比和我名字一样，天天惹是生非。我都被街上的小混混揍了好几次了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1338', '小红：“小东，你的完美情人的标准是什么？” 小东：“我的完美情人要忠贞不二。” 小红：“那我是你的完美情人吗？” 小东：“不是。” 小红：“什么？我的不够忠贞吗？” 小东：“我的完美情人要忠贞，不二。” 小红：“。。。。”');
INSERT INTO `tpshop_joke` VALUES ('1339', '‍‍‍‍小王：“如何评价恋爱，越恶搞越好。” 傻子：“泡妞就是做完了丧权辱国的事。” 小王：“那结婚该如何评论？” 傻子：“然后结婚就可以做丧尽天良的事了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1340', '今天发现女友怀孕了。我：怎么搞的，上次你没吃药吗。女友：吃了啊，吃了两片呢。我： 那怎么还怀孕了，吃的什么啊。女友：吃的抗生素啊。我：卧艹！！！');
INSERT INTO `tpshop_joke` VALUES ('1341', '“老板，你家的百货超市什么都有吗？”“应该是吧，看你要什么了”“那我要个男朋友”“.........”“哎老板，你翻箱倒柜找什么呢，不是真有男朋友卖吧！”“我在收拾东西呀，不然怎么跟你走？”');
INSERT INTO `tpshop_joke` VALUES ('1342', '“姑姑，我的衣服最近洗得很干净哦”，小龙女：“呵呵，而且不伤手”“奇怪了，姑姑，我们的雕去哪了？”');
INSERT INTO `tpshop_joke` VALUES ('1343', '食堂菜是我国第九大菜系、广泛分布于全国各地、主要烹饪方法有瞎特么炒、 乱特么炖……主要特色以不放肉、不放油而闻名全国');
INSERT INTO `tpshop_joke` VALUES ('1344', '每次只有在拧不开瓶盖的时候我才会意识到自己是个女人，当我满怀期待的把瓶子递给身边的男性，却发现他也拧不开时，才会意识到有句老话说得好，生男生女都一样');
INSERT INTO `tpshop_joke` VALUES ('1345', '许多男性朋友打电话来咨询，说老婆怀孕了，自己又想过性生活，究竟可不可以，我的建议是可以，但一定要小心，次数不宜频繁，否则很容易被老婆发现');
INSERT INTO `tpshop_joke` VALUES ('1346', '每次坐地铁我都会十分的心痛，几乎都要感叹国人素质何在？\r\n每次都有人大声地打电话丝毫不遮掩，还有人在公放流行歌曲且陶醉在其中。\r\n最可恶的是还有情侣旁若无人的亲热！\r\n看着这些，我哪里还有心情吃我的臭豆腐！');
INSERT INTO `tpshop_joke` VALUES ('1347', '丈夫得了急病，妻子急忙打急救电话：喂！你是120吗？对方答：我是119！妻子快速地说：那麻烦你到隔壁告诉你的邻居，让他开救护车到我家救人！');
INSERT INTO `tpshop_joke` VALUES ('1348', '一个年轻商人牵着骆驼在沙漠里行走，走了很久。小伙子春心萌动，但沙漠里荒无人烟，于是他就想对那头母骆驼采取行动。当他尝试了很多办法，终于把骆驼放倒以后，开始了…\r\n可是，他来一次，骆驼就站起来往前走一下，再来一次骆驼又起…\r\n如此反复很久，小伙子很不爽，内心极其纠结。后来一架灰机坠落在沙漠里，所有的乘客都死掉了，只有一位美女奄奄一息，小伙子给美女水喝给美女干粮吃，美女活了过来，说“你有什么要求我都答应你”，小伙子很激动，说：“那你快去帮我把骆驼按住”');
INSERT INTO `tpshop_joke` VALUES ('1349', '有天中午去吃羊肉泡馍，吃了很多蒜，嘴里的味道你懂的，于是回去后猛刷牙很多遍，下午去学校，跟同桌聊天，一张嘴，同桌眉头紧皱，我心想不会吧，这也能闻出来？紧接着他一句话我就晕了“你中午吃牙膏了？”');
INSERT INTO `tpshop_joke` VALUES ('1350', '相信我，真正的美女不会因 为流氓向她吹口哨而生气的， 她们从小就习惯了');
INSERT INTO `tpshop_joke` VALUES ('1351', '‍‍‍‍小明：“我的运气可真够好的，出门就捡到了五元钱！” 小芳：“这算什么，我还捡到过三万呢！” 小明：“什么，你捡到过三万！” 小芳：“我还同时捡到了五饼和一筒呢！” 小明：“切！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1352', '老婆是个吃货，一节甘蔗，我拿刀打算砍成两节，一人一半，就叫老婆拿另一端。 本来一刀宰中间的，老婆突然往她那边一拉，结果砍到我手上了，事后缝了10多针。 吃货老婆事后说：“我只是想多吃一点...”');
INSERT INTO `tpshop_joke` VALUES ('1353', '‍‍‍‍一天，我在学校拍了一个同学的屁股，结果被老师发现了，老师告诉了妈妈。 回家后，妈妈问我：“为什么打人家的屁股？” 我说：“我在拍马屁。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1354', '‍‍‍‍遇到一个妈妈带着一个小萝莉路过一个酸奶摊。 小萝莉看着摊上的各种奶说了句：“妈妈，买奶。” 本以为她妈妈会去买。 但是我听见的话是：“买奶？我给你买爷呢，还买奶？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1355', '　　监狱长对一个死刑犯说：“在你死之前，我可以答应你一个要求。” 　　死刑犯：“我想吃荔枝。” 　　监狱长说：“这个季节没有。” 　　死刑犯说：“没关系，我可以等。”');
INSERT INTO `tpshop_joke` VALUES ('1356', '男：哪个。。。我喜欢你，你可以当我女朋友吗？我：抱歉，不可以。。。男跟激动的说为什么？女：老公不要闹！要上我赶紧上！');
INSERT INTO `tpshop_joke` VALUES ('1357', '‍‍‍‍别指望减肥了，八戒走了十万八千里也没见瘦下来。 而且，他还吃素！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1358', '‍‍‍‍儿子：“爸爸，我交的女朋友跟我妈很像！” 爸爸：“你为什么要找跟你妈像的女人呢？” 儿子：“因为我妈对我最好哇！” 爸爸：“傻儿子！老婆跟老妈不一样哇！老公跟儿子也不一样哇！” 儿子：“听不懂哇。” 爸爸：“有了老婆就懂啦。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1359', '一家三口一块看动物世界，电视里正在播放一群狼撕分猎物的情景。女儿说：“爸爸，我怕。”爸爸说：“女儿不怕，有妈妈呢。”女儿纳闷地问：“要是真有狼来了，爸爸不是比妈妈更有力量赶走狼吗？”爸爸接着说：“你妈妈胖，她一个人就够狼吃的了，到时候爸爸就抱着你逃跑了。”');
INSERT INTO `tpshop_joke` VALUES ('1360', '‍‍‍‍有次小草给我传答案，结果掉在了地上，被老师捡起来了。 老师指着校花说：“居然敢作弊？” 校花死不承认（校花坐我旁边），老师指着我喊：“难道是她吗？！” 当我傻呀！ 那时候突然觉得长得丑真好！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1361', '期末考试，小明考的很不好，怕爸爸伤心，就给哥哥发了个信息，让爸爸有点心里准备信息如下“哥，我考完了，让爸爸做好准备”。过了一会哥哥回信息“爸爸让我转告你，他已经做好准备了，你也准备准备！”');
INSERT INTO `tpshop_joke` VALUES ('1362', '用你的评论把故事接下去，我开个头：我在树林里看见一坨屎。 接下来有情神回复');
INSERT INTO `tpshop_joke` VALUES ('1363', '别说，5s就是好用，老婆按了下home键就把我哥们手机打开了');
INSERT INTO `tpshop_joke` VALUES ('1364', '到火车站去，问一美女“请问火车站网上取票点在哪？”美女看了我一眼“口渴，买瓶水来告诉你。”边上大爷突然说：“小伙子，我带你去，渴死她”,大爷，你能不能不那么多事...');
INSERT INTO `tpshop_joke` VALUES ('1365', '马上要和女朋友结婚了,小姨子打电话说请我吃饭,饭后,小姨子幽怨的说到,最喜欢的人要结婚了,新娘确不是我,我想把我第一次给我最爱的人.我一听,顿时外面走.到门口,发现老丈人一家都在外面.说到,女婿啊,你过关啦!我当然心里就捏了一把冷汗,说到,老子太机智了!幸好把避孕套放车上!');
INSERT INTO `tpshop_joke` VALUES ('1366', '‍‍和老婆逛超市，突然老婆深情地对我说：“老公，谢谢你一路陪我走来。” 我：“这么近没必要打车啊！” 哎，沙发不好睡啊！‍‍');
INSERT INTO `tpshop_joke` VALUES ('1367', '‍‍出租车里大刘用对讲机跟另一个的哥说：“我最近肾不行，要不就不起来，起来也很快就完事。” 那头说：“这得用中药调理，我以前也这样，现在没半个点下不来。” 大刘说：“那你给我弄一副。” 这时一旁的乘客听着心动，“也给我来一副吧！我留下您的电话。” 说完下车，大刘拿起对讲机：“又上钩一个。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1368', '今天打排位在和蜘蛛对撸的时候 他要 死了 马上变成蜘蛛飞到天上然后变成 斯巴达来打我 我已经举报了 麻痹怎么 有这么变态的外挂');
INSERT INTO `tpshop_joke` VALUES ('1369', '本人性别男，悄悄欠佳一些阳刚之气，但也是男的！！有一次坐地铁，车厢人不是很多，正好肩膀有些痒，我就掀开外套随手抓了几下，没想到后边有个女的走过来：“是不是肩带掉了，我来帮你整吧”！我………');
INSERT INTO `tpshop_joke` VALUES ('1370', '‍‍早上遇见一卷发小女孩。 我问：“小妹妹，你的头发是在哪里烫的？” 小女孩说：“妈妈说是在她肚子里，她喝开水烫的！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1371', '自拥有手机以来，短信一直没删，昨日得出不完全统计：累计中奖137次，资金共计7260万元，另有各种iphone手机68部，笔记本电脑36台，轿车27辆，中过芒果卫视一等奖56次，被大学录取15次，儿子被拐卖23次，被法庭传唤31次，银行卡有异常31次，儿子嫖娼在外地被抓103次。请告诉我，我这一生是否算传奇...');
INSERT INTO `tpshop_joke` VALUES ('1372', '　　王小姐自作多情地以为某男士暗恋着她，只是不敢表白而已。 　　“你要勇敢地对我说三个字。”王小姐对那男士说。 　　“王八蛋！”那男士说道。');
INSERT INTO `tpshop_joke` VALUES ('1373', '初中时，常去校园池塘里钓鱼。同学妒忌，便吓唬:”你偷鱼，我要揭发你。”我只好把两条大点的送到班主任面前:”老师这鱼不行了，浮上来，我就抓来送给你了。”后来，班主任笑着批评我:”以后别违反纪律了。”');
INSERT INTO `tpshop_joke` VALUES ('1374', '　　老师：‘恰巧’一词怎么解释？ 　　学生：是凑巧同时发生的意思。 　　老师：举一个例子来说明。 　　学生：爸爸和妈妈恰巧在同一天结婚。');
INSERT INTO `tpshop_joke` VALUES ('1375', '在火车上，突然听到一段熟悉的步步高手机铃声，看到对面的彪形大汉从口袋里拿出来了一个粉红色的步步高音乐翻盖手机，接完电话后冲我们憨憨一笑说：我老婆的，她刚刚换了个苹果，就把旧手机给我用了，我用啥手机都行，只要有声....');
INSERT INTO `tpshop_joke` VALUES ('1376', '（1）朝鲜为什么敢打美国？神回复：因为他们钱不在美国，老婆不在美国，孩子也不在美国。（2）美国为什么不敢打朝鲜？神回复：朝鲜太穷，一个导弹好几百万美元，炸哪儿都赔（3）美国和朝鲜到底咋回事？神回复：青春期遇上更年期呗！一个啥事都敢干，一个啥事都要管。');
INSERT INTO `tpshop_joke` VALUES ('1377', '一炒股的大哥彩票中了500万，在领奖台上表情很从容， 记者上台采访：你准备怎么花这些钱呢？ 大哥很淡定的说：先把买中车融资的钱还了。 记者又问：那剩下的呢？ 大哥没有马上回答，而是从兜里慢慢的掏出盒烟，45度角仰望天空，点上一根，悠悠地说：剩下的…剩下的慢慢还呗……');
INSERT INTO `tpshop_joke` VALUES ('1378', '　　一位新来的守夜人去一家天文观察台上班。他目不转睛地盯着一位天文观察员把一架庞大的天文望远镜瞄准着寥廓的天空。 　　突然，一颗流星划破黑空，陨落天际。 　　守夜人大为惊讶，赞叹道：“先生，您这一炮打得可真准！ ”');
INSERT INTO `tpshop_joke` VALUES ('1379', '刚刚处对象那会...晚饭过后她说:乖！去把碗筷涮了，本小姐今晚让你好好爽爽。我兴奋地去把碗筷给涮...婚后...吃完晚饭她说:快去把碗筷涮了，今晚老娘放你一马。于是我又兴奋地去把碗筷给涮了...');
INSERT INTO `tpshop_joke` VALUES ('1380', '小时候在外面买了东西回家总是把价钱给爸妈往高了报；现在买了东西回家总是把价钱给爸妈说得很低。小时候在外面受了委屈回家总是在爸妈面前哭着说；现在受了委屈回家要想着办法在爸妈面前保持笑。其实我们都长大了。大家是不是都有同样的感受。');
INSERT INTO `tpshop_joke` VALUES ('1381', '今天物理课上偷偷吃东西。　　突然老师说：“服务员，给这位同学上杯水！”　　我：“……”');
INSERT INTO `tpshop_joke` VALUES ('1382', '今天下班的时候，下起了大雨，来到楼下，一群人在等人送雨具来，　　其中有个美女孤零零地站一旁，我当时就没犹豫把伞塞给她，准备冒雨回去。　　她：“哎，你的伞。”　　我：“是你的伞。”　　然后我跑进雨中，留给她一个背影，当时感觉自己真帅。　　跑了一段回头看时，她上了一辆宝马，把伞扔了。');
INSERT INTO `tpshop_joke` VALUES ('1383', '监狱长对一个死刑犯说：“在你死之前，我可以答应你一个要求。”　　死刑犯：“我想吃荔枝。”　　监狱长说：“这个季节没有。”　　死刑犯说：“没关系，我可以等。”');
INSERT INTO `tpshop_joke` VALUES ('1384', '一次小红问小明：“小明，孔融让梨的故事让你知道了什么？”　　小明：“让我知道了孔融有当皇帝的潜质！”　　小红：“当时他还那么小，你怎么看出来的？”　　小明：“皇帝吃东西时，第一口必须都让太监吃，没毒了才吃呀！傻X。”　　小红：“滚！”');
INSERT INTO `tpshop_joke` VALUES ('1385', '在路上遇到一个老大爷在拎东西，本想上去帮他拎的。　　一着急就说成了：“老东西，大爷我帮你拎。”');
INSERT INTO `tpshop_joke` VALUES ('1386', '去朋友家吃饭，吃完饭，她老婆说出门办点事。我一时大脑短路，嘴抽抽地来了一句：“有空常来吃吃饭啊！”　　她的眼神我现在还忘不了。。。');
INSERT INTO `tpshop_joke` VALUES ('1387', '一次空姐面试，面试官对面试的女的说：“如果你从高空坠落下去，你的第一反应是什么？”　　女的说：“我会捂住自己的裙子。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1388', '‍‍‍‍　　甲女：“你去整容两块钱就够了。”　　乙女暗喜：“真的，这么简单？”　　甲女：“你坐公交去整形医院一块钱，到了医院人家告诉你整不了，你再坐公交一块钱回来，两块钱就够了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1389', '下课时看见几个同学围在一起聊天。　　我好奇也便凑过去：“你们在聊啥？让我听听。”　　结果他们说：“我们人类的世界你不必懂。”　　绝交！‍‍');
INSERT INTO `tpshop_joke` VALUES ('1390', '和老婆吵架，我怒吼说：“别以为你长的漂亮我就不敢骂你！”本以为她会高兴的不再吵了，谁知道，二货老婆说：“别以为你说的好像很有道理的样子，我就不会打你了。”');
INSERT INTO `tpshop_joke` VALUES ('1391', '有次火车排队上厕所，一女的出来手上的水甩我一脸，等我上完厕所后，发现水管根本没水……');
INSERT INTO `tpshop_joke` VALUES ('1392', '给广大男同胞一个建议，如果有女生主动加你和你聊天，并且主动问你约吗，你赶紧把她删除，百分之百是你老婆（女盆友）测试你的。不说了，搽药去了 ～');
INSERT INTO `tpshop_joke` VALUES ('1393', '今天跟女神一起看电影。想到有可能会kiss。怕眼镜耽误事就换了隐形戴。还好我机智，不然这一大嘴巴肯定把我眼镜煽坏咯。');
INSERT INTO `tpshop_joke` VALUES ('1394', '刚才在院子门口碰见一熊孩子放炮，专门往人脚下扔，我走过去很慈爱的问他：小朋友，你的家长呢？熊孩子一脸熊样的挑衅看我：我一个人出来玩的！靠！我一听这话就放心了，当场把他收拾了一顿，现在好舒坦。');
INSERT INTO `tpshop_joke` VALUES ('1395', '他拿出秘籍，师傅的话又在耳边响起：你考虑好了，此乃魔功，一旦修炼，你便会变成魔。他脸上流露出坚毅与果敢，打开秘籍练起来。他一遍练毕，“嘭”的一声，变成了馍。');
INSERT INTO `tpshop_joke` VALUES ('1396', '有人骂你“禽兽不如”怎么办？答:马上反驳“我哪里不如你了”。如果有人很嚣张的跟你说：不服咬我啊！你可以回他：虎毒不食子！又学会一招有木有~~');
INSERT INTO `tpshop_joke` VALUES ('1397', '骆宾王七岁随父出游，路遇鹅游水上，骆父命其作诗，骆宾王张口就来：鹅鹅鹅，曲项向天歌，白毛浮绿水，红掌拨清波。骆父拍手叫绝。又遇荷花，骆父令其再作一首，骆宾王张口就来：荷荷荷……啪！骆父一个耳光甩过来：叫你作诗，笑你麻痹啊！');
INSERT INTO `tpshop_joke` VALUES ('1398', '提前放年假，今天回来快到镇上了打老爸电话。我：爸，我回来了，马上到镇上了来接我咯。爸：两个人回来的还是三个啊。我：就我一个啊。爸：反正走路也只要个把小时，锻炼锻炼身体吧，挂了……大龄单身男青年的悲哀。');
INSERT INTO `tpshop_joke` VALUES ('1399', '本人男，二十，长期锻炼，长得还行，家里一套独立别墅，有一台吉普牧马人，月入三万左右，一直忍受着这个年龄不该拥有的帅、高大、成熟、稳重、大方…………不行了，编不下去了，太恶心了。。。');
INSERT INTO `tpshop_joke` VALUES ('1400', '今天我看见外甥光着屁股在厨房门口哭。我抱他坐我腿上玩一会后问为什么哭？他说：刚便便了没人擦……');
INSERT INTO `tpshop_joke` VALUES ('1401', '今天上课，我的女神坐我后面，上课的时候，女神没带书， 老师说没带书的站起来，我突然想起了这不和“那些年”里面的情节一样的。 于是我把书给了女神我站起来，老师说：“站起来还这么得瑟， 出去做500个蛙跳。” 大概过了5分钟女神也出来，我说：“你咋出来了？” 女神说：“上数学课，你给本英语书干嘛？”');
INSERT INTO `tpshop_joke` VALUES ('1402', '如果女人不舍得花男朋友的钱；男朋友请客时点最便宜的；跟男朋友吃饭时会偷偷付账；会这样做的女生不是嫌你穷，不是可怜你，更不是她有钱，而是她真心爱你。不舍得你为她破费，她宁愿自己节俭一些也不乱花你的钱。男人你睁大眼睛看清楚啦！身边这样的女生才是真正爱你的！！！');
INSERT INTO `tpshop_joke` VALUES ('1403', '今天小明被老师叫去了办公室，老师：为什么你每次都跟我对着干？小明：从后面干也可以啊！老师：滚！以后别来上学了！！！');
INSERT INTO `tpshop_joke` VALUES ('1404', '还记得高二那年的6月9号上课，我同桌正在睡觉，班主任突然进来了，第一句话就是：“你们现在已经高三了！”同桌被吓得坐了起来，瞪大了狗眼望着我道：“我他妈睡了多久？！！！！！！！！”');
INSERT INTO `tpshop_joke` VALUES ('1405', '老婆：“这哪队踢哪队？” 　　老公：“法国踢尼日利亚。” 　　老婆：“这是中超联赛么？” 　　老公：“…世界杯。” 　　老婆：“中国队在哪？” 　　老公：“跟你一样在看电视。” 　　老婆：“为什么不上去踢？” 　　老公：“国际足联不让。” 　　老婆：“是因为钓鱼岛么？” 　　老公：“…因为水平不行。” 　　老婆：“不是有姚明吗？” 　　老公：“…滚……”');
INSERT INTO `tpshop_joke` VALUES ('1406', '一千金小姐与一书生相约黄昏后。 小姐道：“郎君待我，可否如那唐朝怀素一般？” 书生道：“怀素是个和尚，小姐可是要我跟和尚一样持礼守节？” 小姐笑道：“不是！你可知怀素是个书法家，他最擅长什么字体？”书生愣了一下，大喜道：“你早说嘛！”');
INSERT INTO `tpshop_joke` VALUES ('1407', '几天家里的狗狗生小狗狗了，狗狗还很小，眼睛都没有睁开呢。然后把手指放到小狗狗的嘴边，让它吸。小狗狗以为是乳tou就一直吸。我正在那乐呢,我妈从我身后悠悠地说了句：你小的时候我也这样玩过你！我到底是不是她亲生的？？？');
INSERT INTO `tpshop_joke` VALUES ('1408', '男人，二十年的少爷，一天的王子，十个月的小时工，一辈子的赚钱工具，女人，二十年的公主，一天的王后，十个月的贵妃，一辈子保姆。其实，男人和女人都不容易。相扶相携，同甘共苦，才是真正的夫妻！');
INSERT INTO `tpshop_joke` VALUES ('1409', '有时候 不小心知道了一些事 才发现那些自己所在乎的事情 在别人眼里是那么可笑 有时候 不经意发现了一些人 才明白你把他们放心里 而他们却可以转身就把你遗忘 有时候 要等到看透了一些物 才懂得世事变迁人心冷暖 没有什么东西是永恒的 那些你以为放不下的 终究还是要放下 那些你以为忘不了的 最后还是要忘掉 鱼离开了水会死 而水没有了鱼 却会更加清澈 最后才明白 谁也不是谁的谁 何必高估了自己 把一些人、一些事看得那么重要 所有的烦恼都是源于不够狠心 你要是总是顾及别人 那谁来顾及你？');
INSERT INTO `tpshop_joke` VALUES ('1410', '上高中的时候一个男生和一个女生拌嘴，女生说道：你抄我作业可以，但是你要管我喊声妈。只见该男生冲女生大声喊：妈，那啥，我不抄作业了，我想吃奶。。。');
INSERT INTO `tpshop_joke` VALUES ('1411', '昨天我们村沸腾了，村里有个干部喝醉了酒，跑到村委大院里对着给全村播报的大喇叭大声哭诉，说自己的老婆搞外遇跟别人跑了！足足半个小时啊！！##全村人头一次对村广播听得那么认真，全部搬出凳子来坐在自家大门口听，就差拿爆米花了……');
INSERT INTO `tpshop_joke` VALUES ('1412', '和男朋友说家里的事情，弟弟不听话，爹地太没有威信，管不住他，然后他说，没有微 信申请一个吧');
INSERT INTO `tpshop_joke` VALUES ('1413', '今天喝多了找代驾，第一次找挺紧张的。代驾来了立刻就拒绝了我。挺生气的，尼玛电动车不是车？');
INSERT INTO `tpshop_joke` VALUES ('1414', '世界上没有无缘无故的爱，也没有无缘无故的恨，却TM偏偏有无缘无故的胖！');
INSERT INTO `tpshop_joke` VALUES ('1415', '三岁儿子很粘人，有时候我半夜起来上厕所他睁开眼看不到我都会哭着找我… 昨晚他睡着后，老公想啪啪啪，怕吵醒他，我们转移到客房，正进行到一半，儿子揉着惺忪的睡眼进来了，一句话没说爬上床就躺在我旁边了，唉！跟屁虫，爸爸妈妈办点事咋就这么难啦！');
INSERT INTO `tpshop_joke` VALUES ('1416', '一些专家多年研究发现，说人的手指头呀，其实直接反应了一个人的能力。比如说，食指比较长的人语言能力较强。无名指较长的人，数学能力较强。小拇指较长的人……就是挖鼻屎比较方便些！');
INSERT INTO `tpshop_joke` VALUES ('1417', '黑客：哇哈哈！小渣渣，你的电脑被我黑了！ 小白：万岁！ 黑客：你怎么不害怕？还高兴？ 小白；我求求你入侵我的账单表 黑客：然后呢？ 小白：看到电费没有 黑客：嗯 小白：请把它改成0⃣️ 黑客：为什么？ 小白：我不想交电费。 黑客：去死');
INSERT INTO `tpshop_joke` VALUES ('1418', '　　从前有一位美人鱼爱上了人类的王子，她找到了巫婆，说：”无论如何都要见到他。“巫婆给了她一瓶魔药， 　　说：“喝了它就会见到王子，但是你会变成泡沫。“美人鱼喝下了魔药，慢慢的她周围升起了泡沫。” 　　她抬起头看见王子正深情的看着她，说：”哎！水开了，这鱼再给我加点酸菜呗。“');
INSERT INTO `tpshop_joke` VALUES ('1419', '今天和女朋友去鬼屋玩 感觉太恐怖 女朋友胆子小 被吓哭了 妆都哭花了 …我连拉着她的手跑了出来，刚出门口 就听见一个小朋友在喊：“妈妈快看 那个叔叔把鬼牵出来了…”');
INSERT INTO `tpshop_joke` VALUES ('1420', '早上去上班，一美女看见一男的拉链没拉，于是偷偷地给拉上了，结果，对，你们没猜错…全车的男人都偷偷的把拉链拉开了……');
INSERT INTO `tpshop_joke` VALUES ('1421', '甲:今天和女神聊天，我只说了一句话她就把衣服脱了！乙:真的假的？你说了什么？甲:晚上给她发了句 在么？她回复我说:“我去洗澡了！”');
INSERT INTO `tpshop_joke` VALUES ('1422', '最近很热，下班回家，热的要命，经常撩起裙子直接对着风扇。我家猥琐的老公每次都刻意地走到我身边，把裙子再撩高一点，然后深吸一口气，“咳咳咳，好大的阴风啊！老婆注意空气污染啊！”我就想直接给他一巴掌，这死老公。。。');
INSERT INTO `tpshop_joke` VALUES ('1423', '邻居给老唐儿子介绍了个对象，并一再夸奖女孩知书达理。老唐和儿子都很是满意，谁知结婚后，却发现她酷爱打麻将并且是输多赢少。有一天，邻居碰到老唐问：“儿媳表现还好吧？”老唐苦笑了一下：“你说的没错！‘知书达理’，打麻将就知道输，家务活从不打理！”');
INSERT INTO `tpshop_joke` VALUES ('1424', '18岁不做尝试，被骄傲毁了，走着父母铺好的路。25岁不肯改变，被观念毁了，干着每月固定工资的工作。30岁不愿拼搏，被懒惰毁了，日复一日的观望着别人的成与败。40岁如临大敌，被面子毁了，羡慕着别人的荣华，嫉妒着别人的身家。50岁追悔莫及，被顾虑毁了！上有老下有小，只能小心翼翼的活着');
INSERT INTO `tpshop_joke` VALUES ('1425', '绝对真事，你知道最奇葩的离婚是因为什么？听村里人说三十年前，我们村里有五个哥们打赌离婚后谁能再娶到黄花大闺女。你没看错，因为一句话，五个全部离婚了，而且最后都娶到了黄花大闺女。好醉好醉。。。');
INSERT INTO `tpshop_joke` VALUES ('1426', '朋友的真事，当牛肉面三块五的时候，他去吃牛肉面，吃出了一只苍蝇，叫来老板问怎么有苍蝇？老板鄙夷地说：吃个三块五的牛肉面，不吃出苍蝇能吃出牛啊？他气不过举报了老板，老板被罚款一千二百块。');
INSERT INTO `tpshop_joke` VALUES ('1427', '父亲节那天！交通广播出了一个和父亲有关的互动题。。。是先有的父亲还是先有的儿子！结果一个几岁的小朋友的回答雷倒一片！没有我时他当了三十多年的儿子！这几年有了我他才当了爸爸！');
INSERT INTO `tpshop_joke` VALUES ('1428', '割……一位中国老人在海关翻找护照，韩国女警不耐烦的问：“你第一次来首尔？” 老头儿点头：“第二次。” 女警一瞪眼：“既然第二次，怎么不知道提前拿护照？” 老头儿说：“我第一次来没用护照。” 女警不屑：“不可能！中国人来韩国都要护照！你上次来是什么时候？” 老头儿平静地说：“1951年，开坦克来的，当时叫汉城。”');
INSERT INTO `tpshop_joke` VALUES ('1429', '最直观感受到知识就是财富的时候就是毕业的时候楼下收垃圾的大妈说:书6块钱一斤');
INSERT INTO `tpshop_joke` VALUES ('1430', '高考的时候，监考老师拿那个金属探测器，扫我前面那个女生，本来一切正常，可是扫到那个女生后背的时候，突然响了，监考老师疯了一样的问，背后藏了什么？我都憋出内伤了。');
INSERT INTO `tpshop_joke` VALUES ('1431', '工作单位生活枯燥乏味，一工友晨练跑步，气喘吁吁的回来后，我给他倒了一杯水，还没等我说完话，他就着急的就喝下去一大口，烫的那个真叫鬼哭狼嚎的，至今忘不了那烫坏的舌头，以及工友的狼狈样。据医生说温度再高一点，舌头都能烫熟吃了。尼玛，这该怨谁呢？我只想说，哥们，你到底有多渴？');
INSERT INTO `tpshop_joke` VALUES ('1432', '同学出差回来立马离婚了，骇的我们都大吃一惊。于是我问同学：“你们离婚是怎么回事？”没想到，朋友竟然说：“我出差一趟回来后，我就发现妻子有了外遇；”我说：“你又不在家，杂看到的。别冤枉好人啊？”那知道同学却说：“我在说的仔细点，我回到家当晚看到妻子穿的内裤竟然是人家‘男小三’的内裤啊。”');
INSERT INTO `tpshop_joke` VALUES ('1433', '今天去食堂打饭的时候，一女同事打了两份饭菜，\r\n路过我身边的时候地上有水打滑，然后就一个大马趴倒地上了，\r\n她倒下的时候我没敢扶她，因为我清晰的感觉到了整个食堂都在晃动，\r\n以她的体重，我若扶她，铁定被压扁…别怪我，我没那实力…');
INSERT INTO `tpshop_joke` VALUES ('1434', '就刚刚，在电梯里遇一大姐，一直在按1，\r\n我诧异的望着她说：“大姐，您到几楼。”大姐回到：“11楼啊！”\r\n我就连忙按了7和11，大神啊！谁家电梯是按两个1就是11的，\r\n想想这大姐应该醉的不行……');
INSERT INTO `tpshop_joke` VALUES ('1435', '孙悟空在取经前吊打各路妖怪神仙，大闹天宫，好不威风。。。\r\n为何在取经后一遇妖魔鬼怪就到处找救兵？\r\n神回复：多了一个猪队友。。。');
INSERT INTO `tpshop_joke` VALUES ('1436', '就刚刚在公交车上，一位大妈莫名奇妙在那里发出了很奇怪的声音随后就掏出手机给人打电话，剧情是这样的（免提）\r\n大妈：喂，在哪呢，你儿子在家没\r\nXX：我儿子放学后回来又出去了\r\n大妈：昂，就说呢。刚刚看见你儿子和一个女生在一起很亲密个子看起来低低的各种吐槽 各种嫌弃最后还来了一句你看着办\r\n我看到这一幕也是醉了 真的是 防火防盗防阿姨');
INSERT INTO `tpshop_joke` VALUES ('1437', '跟一妹子在聊天，要我用蓝牙传点东西给她，结果她的名字是姐姐的菊花。\r\n。。连接时，姐姐的菊花要求和你配对。我是不是该做点什么。。');
INSERT INTO `tpshop_joke` VALUES ('1438', '同学在操场跑步一千米后累得要命，作为同学的我，立马递过来一瓶矿泉水，他接过来狠狠的喝了一大口，呛得要命。咽到肚子里才发现原来居然是白酒，气得他要打我，体育老师望着他逐渐变红的脸色。还一脸关切的说：“歇歇就会没事的。”气的那同学干瞪眼。');
INSERT INTO `tpshop_joke` VALUES ('1439', '记得楼主在学校上初三时，由于家里原因不能继续读了，\r\n课间收拾书本时隔壁班一个混混说“那孩子过来给我买包烟去”\r\n然后给我了张二十的，于是乎我接过钱到小卖铺买包辣条吃着回家了，估计那孩子会很郁闷……');
INSERT INTO `tpshop_joke` VALUES ('1440', '甲（男）：你老婆脾气好不啊\r\n乙（男）：我老婆，一来气就摔家里的东西\r\n甲（男）：我老婆爱惜东西，气往我脸上撒');
INSERT INTO `tpshop_joke` VALUES ('1441', '　　一哥们上厕所后满身是水的回来，我问他咋啦。哥们一脸悲愤的说：她奶奶的厕所灯坏了，我正蹲坑呢。一家伙进来后二话不说照我的坑就撒，我都不敢说话怕撒我一嘴，只好低头默默忍受了。我：………');
INSERT INTO `tpshop_joke` VALUES ('1442', '　　公司年会，大酒店聚餐，同事们边吃边表演节目。轮到我了，二胡独奏：二泉映月。一曲完毕，下面掌声雷动。我大喊：“我拉的好不好？！”同事们异口同声：“好！”我又问：“你们吃的香不香？！”又是异口同声：“香！”。事后我被暴打一顿。');
INSERT INTO `tpshop_joke` VALUES ('1443', '　　话唠问禅师：“我总是很热情的和别人讲话，可是他们为什么都不喜欢我。” 　　禅师打断了话唠的话，说：“我觉得你应该去玩玩微博！” 　　话唠：“哦！我明白了，禅师，你是说我应该把我的思想传播给更多的人知道吗？” 　　禅师：“不，我是说，那样你就只能说140个字了！”');
INSERT INTO `tpshop_joke` VALUES ('1444', '　　王教练也教美女开车，美女开着开着，王教练：“把前面那人撞死！” 　　美女：“杀人犯法！” 　　教练：“那你还不踩刹车！” 　　美女：“收到。” 　　一脚油门下去，咚，王教练教唆杀人被捕。');
INSERT INTO `tpshop_joke` VALUES ('1445', '　　我爸从小教导我：“钱要放好，掉了会被捡到的人骂的。” 　　当时不明白，在我我捡到五十块钱的时候明白了！ 　　卧槽！哈哈哈！不知道哪个SB掉的！');
INSERT INTO `tpshop_joke` VALUES ('1446', '　　路人A：“你在桥下干什么？” 　　路人B：“找东西！” 　　路人A：“找到了吗？” 　　路人B：“找到个屁。” 　　路人A：“那是我放的。”');
INSERT INTO `tpshop_joke` VALUES ('1447', '　　跟180斤胖表姐去逛街，表姐看中一件衣服，有最大号，穿着正合适。 　　就跟营业员杀价，说了半天，营业员不为所动，一口咬定不能再便宜。 　　表姐用求助的目光看着我，我只好上前帮忙：“你看这衣服，码数这么大，除了卖给我姐，你还能卖给谁？” 　　营业员犹豫片刻：“好吧，卖给你了。” 　　哎，表姐，你别走啊，还没付钱呢。');
INSERT INTO `tpshop_joke` VALUES ('1448', '　　同事喜欢公司里一妹子，昨天买了玫瑰花向妹子表白，结果遭到拒绝。 　　随手把花丢进垃圾桶了，我告诉他软的不行来硬的吧。 　　结果下午上班扛来一捆甘蔗，表白成没成功我不知道。 　　但是下班的时候看到妹子高高兴兴的把甘蔗扛回家了。');
INSERT INTO `tpshop_joke` VALUES ('1449', '　　恋爱纪念日，我对女友说：“咱们结婚不买钻戒好不好？” 　　女友对我摆了个剪刀手的手势。 　　我说：“难得你明事理，那就不买了。” 　　女友说：“不买你妹，两克拉。”');
INSERT INTO `tpshop_joke` VALUES ('1450', '　　我：“你相亲的女孩怎么样？” 　　哥们：“像是青春怀旧电影里走出的女主角。” 　　我：“哇，清纯又美丽？” 　　哥们：“打过胎。”');
INSERT INTO `tpshop_joke` VALUES ('1451', '有人发错短信给明明，明明回他说：你发错了。　　他不听，一直给明明发短信，　　在明明一个电话打过去喷了他一通以后，　　他又发来一条短信：你是露露吗？　　明明说：我不是露露，我是六个核桃…');
INSERT INTO `tpshop_joke` VALUES ('1452', '大军中午吃饭，服务员端上菜，　　大军问这是水煮白菜嘛？服务员说是干锅白菜。　　待两分钟后，厨师长出来：“我做水煮肉片，　　谁把我白菜端走了？”　　瞬间感觉整个饭店都是奇葩……');
INSERT INTO `tpshop_joke` VALUES ('1453', '胖胖开车捡起交警偷放在树后的流动测速摄像头，放在车上拉走了，　　警车紧追其后，开了一个多小时，　　那货带着摄像头进了交警队：“大哥，您看看这玩意是你们的不？我在路边捡的！”');
INSERT INTO `tpshop_joke` VALUES ('1454', '一日，副校长弓长校长去一老师家串门，　　看到该师家的家具放置的乱七八糟，　　物品更是随处乱放，　　于是惊呼：“你家是不是让炮崩过了！”');
INSERT INTO `tpshop_joke` VALUES ('1455', 'A：“你知道为什么很多人不怕辣吗？”　　B：“不知道。”　　A：“因为他们吃辣之前都要看一种小动物，然后就不怕辣了。”　　B：“是什么小动物？”　　A：“蟑螂。”　　B：“为什么？”　　A：“因为———看见蟑螂，我不怕不怕辣，　　我神经比较大，不怕不怕不怕辣。”');
INSERT INTO `tpshop_joke` VALUES ('1456', '罗罗刚刚去超市买香烟，顺手买个个随便冰棒。　　这时，旁边有个小萝莉一直看着罗罗手里的冰棒。　　罗罗顿时问：“小妹妹想不想吃呀？”　　小萝莉说了声想。　　罗罗说：“那你叫一声好听的。”　　那个小萝莉毫不犹豫的说了声 老公 ！！！！！　　我了个擦！');
INSERT INTO `tpshop_joke` VALUES ('1457', '一哥们在IT公司工作。　　IT人大家都懂的，天天加班，熬夜常态，更无双休可能。　　一个周末，大家一起忙了一上午，十一点多，　　某同事忽然起身，丢下一句话就冲出去了。　　他说：你们忙着，我出去结个婚就回来。　　此人现已有一个可爱的儿子，也离开公司自己创业了，　　但这个事迹成为永久的传说！');
INSERT INTO `tpshop_joke` VALUES ('1458', '昨天参加朋友婚礼，主持人说新郎准备了惊喜，　　谁喝完十杯啤酒，新郎给一条中华，参加婚礼的都要喝，　　最后一个朋友说喝20杯，没人竞拍，他赢了，　　拼死喝完20杯，要中华的时候，新郎拿出了一条中华牙膏。。。');
INSERT INTO `tpshop_joke` VALUES ('1459', '昨天美女同事去驾照练车，赶上同事胃肠感冒，　　教练长得有点难看，同事不分左右，让往左拐她往右拐，　　教练无奈说：“你看着我！”　　同事看了一眼突然胃里一阵翻腾开车门就吐了，　　教练脸都绿了，说我是长得难看点，　　但我教了几千个学员你是第一个没忍住的！');
INSERT INTO `tpshop_joke` VALUES ('1460', '一哥们在IT公司工作。　　IT人大家都懂的，天天加班，熬夜常态，更无双休可能。　　一个周末，大家一起忙了一上午，十一点多，　　某同事忽然起身，丢下一句话就冲出去了。　　他说：你们忙着，我出去结个婚就回来。　　此人现已有一个可爱的儿子，也离开公司自己创业了，　　但这个事迹成为永久的传说！');
INSERT INTO `tpshop_joke` VALUES ('1461', '有一次我回学校的路上突然拉肚子，着急找了家饭店借厕所，服务员让我去二楼，我上去找到厕所，但厕所写着正在维修，小心使用。我哪管那么多，脱了裤子就拉。爽完以后下楼，吃饭的人全不见了。服务员从柜台爬出来对我叫着:卧槽！刚才楼上往下掉大便，正好落在吊扇上。。。');
INSERT INTO `tpshop_joke` VALUES ('1462', '有一个人在饭店吃饭 发现菜里有个苍蝇 就很生气的叫服务员 “来来来你看看 这菜里有个苍蝇 ”“诶吗我心思多大事 呢 一个苍蝇能吃你多少菜！能不能大度点”');
INSERT INTO `tpshop_joke` VALUES ('1463', '警察抓住了小偷，对小偷说：我都抓住你几十次了，你怎么就不知悔改呢！小偷对警察说：其实我也厌倦了这个行业，早想金盆洗手了。警察问：那为什么还偷？！小偷无奈的说：买金盆的钱，还没凑够呢……');
INSERT INTO `tpshop_joke` VALUES ('1464', '语文老师给女友的信：你苗条的身材像破折号，丰满的乳肪像冒号，凹凹的肚脐像句号，不尽激起了我的感叹号，我多想穿过你的小括号，在里面留下一串省略号！');
INSERT INTO `tpshop_joke` VALUES ('1465', '四岁的女儿和十岁的儿子吵架，儿子扬言要打女儿，女儿赶紧跑到我身后求报护。我刚要开口劝儿子不要跟妹妹计较，儿子就气乎乎的坐到了一旁说：爸，我和你是十年的老交情了，你和她才认识四年，你自己看着办吧！');
INSERT INTO `tpshop_joke` VALUES ('1466', '周末叫女同桌一起去游泳，玩得太晚她就在我家住下了，她睡客房。结果这可怜的孩子半夜腿抽筋，而且还是两条腿一起抽，疼的她滚下床往我房间爬，刚爬到客厅，我爸听到声响就起床一探究竟！于是，我爸看到一个披头散发，面目狰狞的少女在地上爬行，心脏病差点犯了。。。');
INSERT INTO `tpshop_joke` VALUES ('1467', '大半夜醒来又在微博看人家撕逼，一博主发了张照片，钢琴上放个iPhone，有人骂她拍iPhone炫富，博主说我钢琴五十万你看不见，骂我用iPhone炫富，笑死我了。');
INSERT INTO `tpshop_joke` VALUES ('1468', '小红:你说，谁是世界上最可怜的人？ 小明:炮兵连炊事班战士！ 小红:为什么？ 小明:戴绿帽，背黑锅，看别人打炮…… 小红:……');
INSERT INTO `tpshop_joke` VALUES ('1469', '‍‍小明问爸爸：“爸爸，什么才是男子汉啊？” 爸爸说：“就是能自己养活一家人，顶天立地得，一家之主！” 小明说：“啊，好厉害啊！那我以后也要做像妈妈一样的人！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1470', '一女穿超短裙坐地铁 , 左腿根纹了的个克林顿 , 右腿根纹了个小布什 , 对面男 子猛盯 , 女怒 : 看啥 ? 男答 : 我就想，看看两个总统中间的那个大胡子是不是本拉登 !');
INSERT INTO `tpshop_joke` VALUES ('1471', '女人不要动不动就和男人生气，学学潘金莲，不喜欢毒死就好了，多简单的事。武则天说过，男人听话就留着，不然就直接弄死，能动手的尽量别吵吵，磕磣！怪不得妖精们都抢着嫁唐僧，能过过，不能过吃肉！?');
INSERT INTO `tpshop_joke` VALUES ('1472', '早晨，我对妻子说“亲爱的，你给我裤子熨了没”“熨了”“不对吧，昨天我兜里还剩十三块五怎么还在”');
INSERT INTO `tpshop_joke` VALUES ('1473', '刚才在路上， 见一男人摸一个女人的胸部，女的大叫: 啊 变态 ！男的不乐意了: 老子纯爷们 摸你一个女人怎么叫变态呢？老子要是摸男人才是变态，艹！说完转身就走，留下女人凌乱在风中。忽然觉得好像没什么不对～');
INSERT INTO `tpshop_joke` VALUES ('1474', '我的小姨只比我大几岁，所以我们走在一起就很像一对。有一次我和小姨手牵手的去逛街，被我班主任看到了。于是我班主任就给我妈说了这事。回到家之后我妈就把我拦住问我：“今天你和哪个女孩子手牵手逛街呀。”我直接来了一句：“你妹呀！”于是我妈一巴掌扇了过来……');
INSERT INTO `tpshop_joke` VALUES ('1475', '记得高中的时候，我们班上有个同学名叫明柛剑，新来的班主任老师点名时喊道：日月神剑，当时全班同学都给笑喷了。');
INSERT INTO `tpshop_joke` VALUES ('1476', '　　我做梦梦见一老头拿一根绳子要捆我，我直接把他暴揍一顿，这么大年纪了喜欢搞基就算了还玩SM，临走时我问他叫什么，他说：“别人都叫我月老”');
INSERT INTO `tpshop_joke` VALUES ('1477', '一觉醒来，仿佛又回到了十年前。加内特还在森林狼，詹姆斯还在骑士，科比还在湖人，易建联还在广东，托雷斯还在马竞，德罗巴和穆里尼奥还在切尔西，王菲和谢霆锋还在谈恋爱，10年时间，我是不是做了一个很长的梦？');
INSERT INTO `tpshop_joke` VALUES ('1478', '一真事，初九，未来的丈母娘家里拜年会，女朋友家很多亲戚都会来，席间我和她家亲戚们喝酒，感觉有点醉就让我女朋友带我去回房睡一觉，女朋友把床铺好后叫我去睡，谁知我一屁股把床弦坐断了，冷友们自己脑补那些七大姑八大婆的惊讶表情，目前丈母娘大人正在考虑要不要我的问题。');
INSERT INTO `tpshop_joke` VALUES ('1479', '我搜了一下“把醋当酱油放了怎么办”，百度知道说可以放白糖。后来我又不得不搜了一下“把盐当白糖放了怎么办”');
INSERT INTO `tpshop_joke` VALUES ('1480', '酒店客房里，一男一女在床上坐着，两人不言不语，女方一直红着脸，男的开始还好，后来时便沉不住气，于是他对女的说:你信不信我能把你全身连衣带内一下子拔下来？女方大吃一惊，问道:不可能吧，我都没见过手速这么快的人。男的终于怒了:那你TM不赶紧脱！');
INSERT INTO `tpshop_joke` VALUES ('1481', '‍‍‍‍朋友聚会，妻子也想参加。 丈夫拒绝道：“不行，大家说好的都不得带配偶去的。” 妻子出主意到：“你说我是你情人不就行了。” 丈夫瞥了妻子一眼：“切，就你这模样，就算打死了谁也不相信的。” 然后天都快亮了，这厮还在屋外求老婆开门。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1482', '‍‍‍‍唐僧在白龙马头上安了一个GPS导航，从此躲避了很多灾难！ 导航提示：“前方八百米处狮驼岭，请绕行！” 唐僧：“悟空，我们绕路走吧，不想你们那么辛苦的打妖怪！” 导航提示：“前方一千米处盘丝洞，请绕行！” 唐僧：“悟空，我们绕路走吧，免得妄造杀孽！” 导航提示：“前方六百米处女儿国。” 唐僧啪的一下直接把导航关了，说：“我们不能每次遇到困难险阻就逃避，这次不绕行了，这次我们要勇敢面对……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1483', '‍‍‍‍和老婆关系有点紧张，于是两人坐下来谈心。 老婆说：“我觉得这也不是一个人的问题，肯定是两个人的原因，也不能全怪我是吧？” 我说：“嗯嗯没错，还得怪你的闺蜜小美。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1484', '“姐夫，在吗？”“在呢，有事么？”“我姐加班去了是吗？”“是的！”“那我今晚和你一起睡行吗？”“你都18了，已经成年了!”“可是我害怕一个人睡觉！”“滚一边去，一大小伙怕个毛！”');
INSERT INTO `tpshop_joke` VALUES ('1485', '某时髦女前来就诊，医生让他先去挂号，她不紧不慢：“没事，少不了你的，你先看，到时一块算。”医生：“那你至少买本门诊病历让我写啊。”她又来了：“没事，你写你的，到时一块算！”');
INSERT INTO `tpshop_joke` VALUES ('1486', '一个孩子很贪玩，考试成绩总是很差。父亲警告他再这样下去，就不带他出去玩。这次考完试后他对老师说：“这次请您给我打100分好吗？”老师说：“这怎么行，你只能得20分。”他想了想说：“这样吧，这次给100分，以后每次扣10分，扣满80分为止。”');
INSERT INTO `tpshop_joke` VALUES ('1487', '考试成绩很差，妈妈很生气，怒斥道：“你说说你，除了吃还会什么？”儿子：“还会饿。”');
INSERT INTO `tpshop_joke` VALUES ('1488', '父亲教导儿子：“人的生命只有一次，所以要珍惜。”儿子说：“不是失去才懂得珍惜吗？”');
INSERT INTO `tpshop_joke` VALUES ('1489', '女儿：妈，这是考试卷子，90分。妈妈：你老实交代，后面那个0是不是你加上的，你如果老实交代妈妈给你100块。女儿：好吧，妈妈，我说实话，其实那个9才是我加上的。妈妈。。。');
INSERT INTO `tpshop_joke` VALUES ('1490', '今天下午幼儿园去接女儿，走出校门看见幼儿园老师和他男朋友一起，手牵手下班回家。女儿和老师打了招呼说：“老师今天你爸爸也来接你的呀！”');
INSERT INTO `tpshop_joke` VALUES ('1491', '　　今天奶奶过生日，小侄子喊着问：过生日为什么要吃这个啊，跟屁股一样的寿包 　　大家脸色大变啊... 　　接着，小侄子打开寿包，看到里面的豆沙馅儿，说：快看快看，里面还有便便... 　　众人狂晕...狂吐....狂.....');
INSERT INTO `tpshop_joke` VALUES ('1492', '想到小学的时候学抽烟，烟没了又没钱买！走过玉米地时看到玉米长出来那须跟卷烟丝差不多，于是就抓了一把用作业本的纸卷了一支，点火猛嘣两口，直接咳到泪奔！！');
INSERT INTO `tpshop_joke` VALUES ('1493', '早上起床，打开电脑，发现女朋友还在睡觉，于是忍不住上床嘿咻…… 完事之后发现电脑刚好打开，360提示：开机时间35秒，恭喜你！打败了全国75%的用户！ 我好像发现了什么……');
INSERT INTO `tpshop_joke` VALUES ('1494', '中国人和韩国人发生战争我支持中国，因为我是中国人。韩国人和欧洲发生战争我支持韩国，因为我是亚洲人。欧洲和外星人发生战争我支持欧洲，因为我是地球人。日本美国和外星人发生战争我支持外星人，因为我还是人。');
INSERT INTO `tpshop_joke` VALUES ('1495', '‍‍老婆：“亲爱滴，你最得意滴事是嘛事儿？” 老公：“是俺把俺滴第N任女朋友甩了，那可是俺生平唯一一次甩人不是被人甩。” 老婆：“你为嘛要甩了她？” 老公：“因为她对俺说‘俺看上你是俺瞎眼，要么俺走人，要么你滚蛋！’所以俺就走了，把她甩了！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1496', '朋友问我：“为什么我有时候大便到一半时，感觉还没有拉完，但怎么也拉不出来，擦完后穿起裤子却又想拉了，蹲下后又能接着拉出来?” 我想了想说：“这就跟电脑死机一样，重启后就能继续运行了。”');
INSERT INTO `tpshop_joke` VALUES ('1497', '今天看到网上专家说自己的手机比自家的马桶脏十倍不止，于是哥淡淡的回复到:我敢用舌头舔自己的手机你敢舔你家马桶么...');
INSERT INTO `tpshop_joke` VALUES ('1498', '认字需要两个一起念才能认出来，深圳的圳离了深字我压根不认识……');
INSERT INTO `tpshop_joke` VALUES ('1499', '一男骑摩托车，后座上带着一个四五岁的小孩。 那男的骑术太差，小孩摇摇晃晃，终于摩托车一颠，小孩掉下来了。 那个男的浑然不知，我停下车把孩子抱上来，加大油门追上了他。 埋怨道：“你怎么骑摩托的，孩子掉了你都不知道?” 那个男的瞪着眼睛看了看孩子，大叫到：“你妈呢?”');
INSERT INTO `tpshop_joke` VALUES ('1500', '数学课，小军不认真听课在下面偷看小说《小时代》。突然老师提问：“小军，你来回答一下，这个圆柱的高是多少？” 小军一愣，说：“这个……原著郭敬明的高可能是1米47。”');
INSERT INTO `tpshop_joke` VALUES ('1501', '1．情深深雨蒙蒙里，书桓曾表情沉重地说：“八年抗战就要开始了！”　　　——天嘞，书桓兄，都还没有开始抗战，您就知道是八年了？对于这种未卜先知的能力，只能表示佩服。　　2．紫薇：尔康……你好过分哦～～　　（……三　……秒……钟　……过去了……）　　接着羞涩道：但是我好喜欢你的过分哦……　　　——。。。大家。。。。请自由地。。。［泪奔而去］　　3．《水云间》里芊芊跳楼受伤，马景涛抱住芊芊如拨浪鼓般狂摇，　　“芊芊，你怎么样？啊？怎么样？”　　　医生：“病人需静养，不能受刺激。”　　　于是马某放开芊芊，又开始狂摇医生：“医生，你救她呀！你救她呀！！”　　　——我，大哥，您脑子没事儿吧……　　4．尔康，一个破碎的我，怎么帮助一个破碎的你……　　　——内啥，琼瑶大妈,俺啥也不说了……　　5．女：你说！你说！到底为什么！　　男：你听我解释　　女：我不听我不听我不听！　　——我＝＝＃　大姐，您不听还问啥子啊……　　6．紫薇以婢女的身份随乾隆等人出游，途中某晚与尔康幽会，紫薇娇羞说道：“拜托你不要总是盯着我看，这样会被皇上发现的……”　　　尔康脸涨得通红，偶好奇得看着他，不知他运气要干吗？良久，尔康爆出一句：“谈何容易～～”　　　——“咣当”一50T的天雷砸在我头上！！好吧，接上面，我继续晕！！！！！　　7．那个什么龙梅的在草地上OOXX完事后，康熙一脸满足的说：龙梅啊，是你强暴了朕啊！　　——当时看到那一幕的时候对陈道明万分的佩服。竟然能用如此冷静的口气说出如此彪悍的语句！　　8．还珠2里面香妃病了～蒙丹改装混进宫去看她。。。　　原来的台词俺记不住了～～　　大概就是这个意思－－－－后面半句是原话～～　　蒙丹，你为何总皱着眉头，有时候，我真的很想拿一把熨斗把你的眉头熨平。。。。　　——穿越，这绝对是穿越！　　9．康熙秘史里皇后死时,夏雨说到：爱后，爱后。。。。。。　　——　只听说过爱妃，啥叫爱后。。。。　　10．琼瑶阿姨一生作品无数，最最XX的是《情深深雨蒙蒙》中的这一段：　　女：你无情，你冷酷，你无理取闹！　　男：你才无情，冷酷，无理取闹！　　女：我哪里无情，哪里冷酷，哪里无理取闹！　　男：你哪里不无情，哪里不冷酷，哪里不无理取闹！　　女：好～～～就算我无情，冷酷，无理取闹！　　男：你本来就无情，冷酷，无理取闹！　　女：我要是无情，冷酷，无理取闹！也不会比你更无情，冷酷，无理取闹！　　男：哼！你最无情，冷酷，无理取闹！　　——听罢，我瞬间对这无情冷酷无理取闹的世界绝望了。。。　　11．书桓，你不要过来，让我飞奔过去！　　——　＝＿＝＃　　12．《又见一帘幽梦》传说中的十三晕！　　紫菱：云帆，我晕车耶！　　云帆：怎么会晕车呢，这只是马车呀。是不是中暑了？有没有发烧？　　紫菱：我不是那种晕车！　　我是坐着这样的马车，　　走在这样的林荫大道上，　　我开心得晕了，　　陶醉得晕了，　　享受得晕了，　　所以，我就晕车了。　　其实，我自从来到普罗旺斯，就一路晕。　　我进了梦园，我晕。　　我看到了有珠帘的新房，我晕。　　看到古堡，我晕。　　看到种熏衣草的花田，我还是晕。　　看到山城，我更晕。　　反正，我就是晕。　　云帆：好，你晕吧。　　——好吧，你不晕我也要晕了！！　　13．琼瑶阿姨＊＊台词远远不止这些,除了著名的13晕,又见一帘恶梦里还有很多BH的例如　　云帆对紫菱说:噢,你这个折磨人的小东西　　云帆对紫菱说:你滑得像条鱼　　云帆对汪展鹏说:你知道,她（紫菱）想要回来,那是一分钟也不能耽搁的,所以我们就像箭一样射回来了。　　——同志们，你们都学会比喻了么。　　14．琼瑶拍摄多年，总有那么一个场景是经年不变的：　　女主：“告诉我这不是真的，你骗我的！”然后双手捂着脸嚎啕大哭OR双手捂嘴转身就跑……　　　——话说，大姐您就是不听别人解释对吧？？？！！！　　15．　当然，有了女主角必然要有男主啦！！！　　于是：　　男主：　“你真的好残忍好残忍……”然后一副欲哭无泪OR泪流满面～～～　　——囧RZ……所以我说你□啊！！！　　16．《我是一片云》　　友岚索吻被拒，追问宛露“为什么”，可是又很快捂住人家的嘴：不要告诉我为什么，让我保留一点希望吧。　　——我X的～～～你要不想知道还问个啥子劲啊～～～～　　17．新版西游记，孙悟空对着众人说：“就算我只有畜生的智慧……你们为什么不教我？”　　　——猴哥儿！不带这样的！第一，对自己的定位太残忍！第二，那个历史时期都用上“智慧”这词儿了．．．．＃＃＃＠＠＃￥￥％　　18．《笑傲江湖》李亚鹏版　　里面有一句：“令狐大哥，你贵姓？”　　　——我⊙﹏⊙　　19．紫薇对尔康无限深情的说：“求求你，不要这么帅好吗？”　　　尔康回一句：“我也求求你不要这么温柔。”　　　两人深情相拥．．．．　　——我！才真的要求求你们了．．．');
INSERT INTO `tpshop_joke` VALUES ('1502', '1.周杰伦：温柔的让我蛋疼的可爱女人. 2.光良：哦第一次我，说爱你的时候，呼吸难过蛋不停地颤抖. 3.任贤齐：你总是蛋太软，蛋太软，把所有问题都自己扛. 4.刘德华：你就像一个刽子手把我出卖，我的蛋仿佛被刺刀狠狠的宰. 5.中国移动：沟通从蛋开始。');
INSERT INTO `tpshop_joke` VALUES ('1503', '开学第一天 刚入学的时候，全班自我介绍。一男同学走上讲台：“我叫尤勇，来自北京，我爱下棋！”说完就下去了， 下一位是个女生，该女娇羞地走上讲台，忐忑不安地自我介绍：“我……我叫夏琪…我喜欢游泳…#棋坛小龙女#');
INSERT INTO `tpshop_joke` VALUES ('1504', '告诫各位剩女，男生就像大学食堂里的菜，虽然不中意吃，……但是……但是…………去的晚了还是会没有的！！！');
INSERT INTO `tpshop_joke` VALUES ('1505', '研究发现：女人穿得过少会加快全球变暖的速度——男人看到穿着暴露的美女，血流加速，体温升高，向周围环境排放的热量增加，直接导致环境变暖。男人看到穿着暴露的美女，呼吸变急促，呼出温室气体二氧化碳的量增加，间接导致环境变暖……');
INSERT INTO `tpshop_joke` VALUES ('1506', '比尔盖茨的新别墅落成后,发现设计师给他开了个玩笑:1按门的开关后先播放音乐,大门两分钟才能启动;2不能兼容旧家的电器,需全部使用新电器;3电灯开关要双击;4一次只能打开一扇窗子;5每晚睡眠前需选择\"休眠\"、\"睡眠\"、\"待机\";6冲马桶时,马桶会提示:\"你的大便要保留吗?你确定要冲马桶吗?\"');
INSERT INTO `tpshop_joke` VALUES ('1507', '一个小男孩拿着一张假钱走进了玩具店，准备买一架玩具飞机。 服务员阿姨说：“小朋友，你的钱不是真的。 ”小男孩反问道：“阿姨，难道你的飞机是真的？”');
INSERT INTO `tpshop_joke` VALUES ('1508', '女生每个月都会来月经。又把来的那个称做\"好朋友\"，但知道为何要这样称呼呢？ 把好朋友这三字拆开不就很传神了吗? \"女子月月有\"!');
INSERT INTO `tpshop_joke` VALUES ('1509', '语文好歹能增长你的文学知识！英语能让你与鬼佬交流！历史能让你不背叛啊！地理能让你不至于迷路啊！政治能让你知道怎样维权啊！可是数学除了毁掉整个人生还能做什么啊！泥马！！你用函数买菜啊！你去黄鹤楼还去算长江里的船距离你多远啊！你看到一排电话号码要想想它们之间有没有通项公式啊！');
INSERT INTO `tpshop_joke` VALUES ('1510', '一年前,学校广场上献血.200CC送一副修指甲的用具,400CC送个手表. 邻班一MM听说了感觉很幸福,跑过去问护士:\"1000CC送什么?\" 护士淡定的说:\"送个棺材……\"');
INSERT INTO `tpshop_joke` VALUES ('1511', '我和一哥们去玩，遇到几个混混挑衅我们。我特么一来气。上去就是一口老黄痰。就动起手来。我刚想叫我哥们帮个忙。谁知道他却说了一句:我警告你们，打我朋友可以，但千万别打我。结果我哥们被我和几个混混揍了一顿。');
INSERT INTO `tpshop_joke` VALUES ('1512', '　　想当年一个妹纸半夜跑到网吧来找我，问她睡不着吗？不想睡，上网吗？不想玩！然后我竟然就脱下我衣服给她披着在网吧睡了一夜……不说了……我想静静……');
INSERT INTO `tpshop_joke` VALUES ('1513', '老婆比我小十天，今天我两抬东西，我这头抬起来，她那头抬不动…我：同样吃了二十多年饭，你丢不丢人。 老婆：你比我多吃十天饭呢，有本事你十天别吃饭再试试！！尼玛我竟无言以对');
INSERT INTO `tpshop_joke` VALUES ('1514', '养儿子和养女儿的区别：养个儿子……跟玩游戏差不多，建个帐号，起个名字，然后开始升级，不停的砸钱……砸钱……砸钱……一年升一级，等以后等级起来了，装备也神了，却被一个叫儿媳妇的盗号了！养个女儿……就象种一盆稀世名花，小心翼翼，百般呵护，晴天怕晒，雨天怕淋，夏畏酷暑冬畏严寒，操碎了心，盼酸了眼，好不容易一朝花开，惊艳四座，却被一个叫女婿的瘪犊子连盆端走了！');
INSERT INTO `tpshop_joke` VALUES ('1515', '　　朋友的老妈，住六楼出门忘带钥匙，灶上炖的菜，沒办法打119，消防员来了后从楼顶用绳顺到凉台进去把门打开了，大妈感激不尽，一直说谢，送出门外，高潮来了一阵风吹来门随即又被关上。消防员再次爬上楼顶……打开门后， 　　消防员不停的叮嘱：千万别送了，别送了……');
INSERT INTO `tpshop_joke` VALUES ('1516', '姐夫是初中语文老师。一次跟姐夫去钓鱼。。钓的都是还不到一两的小鲤鱼。姐夫把鱼扔河里了说“太小了。。回家叫家长。”叫家长。。。听着也醉了。');
INSERT INTO `tpshop_joke` VALUES ('1517', 'lz女，闺蜜半夜三更当街和他老公吵架，我老公拉开他老公，我去劝闺蜜，谁知那丫头不理我，开始往前跑，我163，她148小萝莉一枚，追不上啊，我喊她别跑了，你想要在小镇出名吗？你再跑前面就是坟墓地，你想要我跑步追你减肥吗？闺蜜还是不停下，跑着她就拐进一个弄，接着是狗叫声，再接着就是两个我们女人尖叫声。。。。。狗开始追我们，然后我一边骂狗，一边骂她半夜发疯瞎跑跑什么！然后第二天，镇上都在议论昨晚有两个女的瞎跑跑被狗追的事');
INSERT INTO `tpshop_joke` VALUES ('1518', '老张的律师对他说：“有好消息也有坏消息。”老张说：“先说说好消息吧。”律师说：“好消息你太太找到了一张价值20万的照片。”老张问：“那么坏消息呢？”律师说：“那是一张和你的女秘书在一起的照片。”');
INSERT INTO `tpshop_joke` VALUES ('1519', '同事给介绍个男的，第一次约会，走在路上看到卖糖炒栗子的，我买了十块钱的，男的赶紧说没零钱了，我不知声，他说真的，我从兜里掏出十块给阿姨了…gc是当我站在烤冷面的摊位前，这位仁兄掏出7张一百的摆到我面前说了一句，你看我真的没零钱了不骗你的，我能说我满脸的黑线还有周围人的眼神吗…三十了还没结婚的你，活该你单身啊');
INSERT INTO `tpshop_joke` VALUES ('1520', '　　半夜迷迷糊糊醒来，发现自己抓着一只手。捏了捏，没有感觉，卧了个槽，明明是一个人睡的！!!瞬间就清醒了!! 原来是左手被压麻了，右手正抓着左手......');
INSERT INTO `tpshop_joke` VALUES ('1521', '最近买了电子烟，使用感觉还不错。昨天出门坐公交随手就塞到牛仔裤兜里了，可能是牛仔裤太紧，压到开关了，整个车厢的人都看我胯下部位一直在冒烟，我还专心看手机根本没发觉，直到一个好心的哥们拍拍我，哥们你好像屌炸了。。。。');
INSERT INTO `tpshop_joke` VALUES ('1522', '　　同事老婆再要个把月就生了，请假回农村老家待产。现在农村信号还不怎么好，同事玩手机老是卡机死机，所以同事总说:“怎么又卡机死机了！”这话被他有点耳聋的奶奶听成‘杀鸡吃鸡’！终于在第五天奶奶找他谈话:“孙子！今年家里也就养了30来只鸡，几天都听到你说‘杀鸡吃鸡’！照这种吃法，等你老婆生孩子时可就没土鸡蛋吃了！咱改吃鱼行吗！……');
INSERT INTO `tpshop_joke` VALUES ('1523', '有次放学没回家，去打电游，被老爸逮住了。到家后老爸慈祥的说：“先去洗澡吧，以后先回家再出去玩。”幸福来的好突然，让人难以置信。迅速进入卫生间，脱衣，开热水器。这是老爸突然一脚把卫生间门踹开，拿着竹条进来怒吼：“兔崽子！平时打你叫你脱衣服你不脱，这会给你长个记性”');
INSERT INTO `tpshop_joke` VALUES ('1524', '一个有钱女子，对大师说：“大师，我觉得活着真没意思！钱吧，我随便花有的是；男人嘛，我天天换；至于山珍海味，吃得我都恶心。大师，你说我到底咋整啊？！”大师回头，拿蜡烛把女子的衣服点着了。女子急忙吹灭，想了想说：“大师，我知道了，你是说再多的钱也没有生命重要！”大师说：“没有。我是说：你不吹，能死！！！”');
INSERT INTO `tpshop_joke` VALUES ('1525', '　　老李是一个好赌之人，于是在自己胸前纹了一张扑克。一日上街见一人更厉害，竟然在头上炆了一个骰子。于是上前打招呼：“兄弟，你也喜欢赌？” 　　那人回道：“阿弥陀佛”');
INSERT INTO `tpshop_joke` VALUES ('1526', '今天我在网吧玩游戏。旁边坐在一个小学生在玩LOL。这时来了一个怒气冲冲的家长，应该是他爸爸。准备打的时候，那小孩说了一句叼炸天的话。：“要打回家打，你不要脸我还要脸呢！还有，等我玩好这一局，回家武器随你眩”瞬间感觉这孩子见不得明天的太阳了。');
INSERT INTO `tpshop_joke` VALUES ('1527', '多少黑名单，是曾经的特别关注。多少的互道晚安，如今却变成了呵呵与再见。没有一段感情是始终如一永恒不变的，也不会有多少朋友会一直守在你旁边。就像听一首曲子，调子喜欢就围在一起，曲终人散，就好好道别，从此不再打扰。可惜很多人只会享受甜蜜，不会处理告别，于是怨恨纠缠，最终彼此两厌。');
INSERT INTO `tpshop_joke` VALUES ('1528', '　　本人坐在门口休息，突然过来一个人，满脸热情地跟我打招呼:大哥，最近在忙些什么啊？ 　　看到他这么熟络，我还在疑惑是不是哪个好久不见的朋友时，就见他从背包拿出来一些东西，再次热情地对我说:大哥，买几把牙刷吧～～');
INSERT INTO `tpshop_joke` VALUES ('1529', '别再抱怨自己在十三亿人中都找不到一个对的人了，你连选择题只有四个选项也找不出一个对的。。。别说找对象');
INSERT INTO `tpshop_joke` VALUES ('1530', '今天一个小孩来看病，老师:这是个小孩的常见病～你来说说～～～我:白大褂恐惧综合症？');
INSERT INTO `tpshop_joke` VALUES ('1531', '小时候我家养了一条土 狗，叫大黄；这天带着我家的大黄在村里溜达！一条恶犬狂吠着从一家院子冲了出来！护主心切的大黄冲上去和恶犬战作一团…眼看大黄实力处于劣势，我急忙从别人家大门上掰下来一块板子，瞄准对方狗头…一板子下去…“成功”将大黄击晕！！后来，大黄三天没理我！');
INSERT INTO `tpshop_joke` VALUES ('1532', '刚处对象的时候，媳妇儿就是lz女神，娇小温柔~连说个话都细声细气的。结婚以后啊，连放屁都不遮掩，故意用力嘣出个动静，还不忘做个手 枪的手势指着LZ！苍天啊，LZ上辈子造了什么孽啊！！');
INSERT INTO `tpshop_joke` VALUES ('1533', '不知是09还是08年注册的QQ，突然之间密码丢了，无奈之下只好回答密保问题找回密码。最爱的人是谁？小学班主任是谁？最喜欢去哪？明明是很简单的问题，可输了十几次都不正确，怎么也回想不起来答案是什么，逝去的终究不在了。');
INSERT INTO `tpshop_joke` VALUES ('1534', '今天村里一爷们领证了，请客吃饭，讨论到吃田鸡， 新娘子说那玩意不干净，少吃， 这时一直默默无语吃东西的新娘突然说到， 有撒恶心的，那么多蝌蚪都吃了，吃个青蛙有撒， 瞬间大家沸腾了，信息量好大。');
INSERT INTO `tpshop_joke` VALUES ('1535', '过节这几天，村里一姑娘家里出了很多事，先是她爹病了，哥手不小心被刀砍了，她又摔骨折了。她向我们抱怨时哭了，说不知遭了什么倒霉运。我安慰她：“没事的，清明节时给你爷爷多烧点钱，保佑你家改改运气。”谁知她哭得更凶了，边哭边骂：“滚，我爷爷还没死。”尴尬万分，唉，少小离家老大回，家乡之事该问谁？');
INSERT INTO `tpshop_joke` VALUES ('1536', '我都这么大了，每次回到家见到我爸就会不自主地问：“爸，我妈呢？”见到我妈就：“妈，我爸呢？”不知道有多少人会是这样子的。');
INSERT INTO `tpshop_joke` VALUES ('1537', '村姑偷瓜被看瓜的小伙子逮到。村姑想色诱小伙子，直往他身上蹭：大哥，那边有片高粱地。小伙子：还想偷高粱？高粱地特么也是我家的……');
INSERT INTO `tpshop_joke` VALUES ('1538', '天凉了，媳妇有点感冒，擦鼻涕的纸乱扔。。。我就和她开玩笑说:晚上给你打一针就好了。媳妇:好。你打针要是超不过半个小时，针管给你掰折。有本事就来，没本事就别逼逼。 我还是闭嘴吧。');
INSERT INTO `tpshop_joke` VALUES ('1539', '朋友买了个肾6，出去玩儿就被偷了。 。。发现后打那电话。。。居然通了！然后朋友一通解释让对方还手机，对方问他怎么证明手机是他自己的，朋友想了想说，密码是6688。。。然后电话挂了。。。再打。。。您拨打的电话已关机。。');
INSERT INTO `tpshop_joke` VALUES ('1540', '今天本来想亲一下媳妇的。突然一咳嗽，一口痰吐在她脸上……然后,到现在还没有理我。。抽烟人痰多伤不起啊');
INSERT INTO `tpshop_joke` VALUES ('1541', '女神说她无聊，问我要不要一起吃饭，我心灰意冷的说：“算了吧，我知道你其实并不喜欢我。”女神立刻激动的反驳：“谁说的？”我心里一喜：“这么说你喜欢我？！”“不是，我就想知道是谁把这个秘密告诉你的。”');
INSERT INTO `tpshop_joke` VALUES ('1542', '今天我侄子给我说： 压岁钱这种东西，就是大人给大人的，中途给我们看看..');
INSERT INTO `tpshop_joke` VALUES ('1543', '多和那些看起来嫁不出去或者娶不到媳妇的人做朋友吧，这样，比较省份子钱。');
INSERT INTO `tpshop_joke` VALUES ('1544', '今天终于鼓足勇气约在网上聊了很久的女孩见面，说好了时间和地点，来到见面地点看见一个很漂亮的女孩心想要是约会对象是她该多好，我掏出手机拨打女孩电话，这时那个女孩接了个电话，回头看了我一眼撒腿就跑，我电话那边听那个女孩急促的说了句信号不好听不到然后就挂了，尼玛到现在我都没有弄明白到底是什么情况。');
INSERT INTO `tpshop_joke` VALUES ('1545', '某留学生在英国打官司，让英国律师给法官送礼，律师说：“万万不可,送了你肯定输！”最后留学生还是向法官送了礼，但官司却赢了。律师百思不得其解，留学生说：“我给法官邮寄了包裹，但写的是对方当事人的名字！”哼，有什么能难得住我中国人民的智慧！');
INSERT INTO `tpshop_joke` VALUES ('1546', '人到了某个人生阶段，最好还是能接受点孤独。酒肉朋友随喝随有，但一个人安安静静的时候却很难得。有多久没一个人待会儿了？读书、散步，又或者是坐在某个地方什么也不干呢。外头的天空其实挺好，冷风吹在脸上的感觉也不是想象里那么糟糕，什么也不用去想，什么也不必对抗，大概才是生活原本的模样吧。');
INSERT INTO `tpshop_joke` VALUES ('1547', '以前的高中同学，整天在朋友圈炫富，我告诉男朋友说这个同学老炫富真讨厌,男朋友说：“你管她借钱她就不炫了”，后来我故意管她借两千块钱她竟然真的借给我了…… 后来我男朋友就成她男朋友了…');
INSERT INTO `tpshop_joke` VALUES ('1548', '人都说 每逢佳节胖三斤，昨天一上电子秤，它M的直接死机了是几个意思');
INSERT INTO `tpshop_joke` VALUES ('1549', '读书那时喜欢一个女孩子很久了，每次早读前碰见她都会跟我打招呼早上好，我也很礼貌的回她早上好。如今她已为人母，偶尔想起我都会狠狠的往脸上抽一巴掌，呵呵，早～上～好！原谅青春年少的我，我真的不懂暗号。');
INSERT INTO `tpshop_joke` VALUES ('1550', '多年以后，窦唯、谢霆锋、李亚鹏三人走在路上，遇见带着娃娃们出来散步的王菲，王菲停住脚步，深情地对孩子们说:三人行，必有你爹。');
INSERT INTO `tpshop_joke` VALUES ('1551', '张无忌深知要练成独孤九剑，并非是一朝一夕的事情，于是他带着林黛玉在断肠崖下一蹲就是十六年，最后得了老寒腿，后来名医华佗为他刮骨疗伤，痊愈之后又跟随着李世民征战天下，大展身手，携手建立了大唐，成名后不甘为臣，便在梁山集结了一百单八条好汉再某大业，无奈未能如愿以偿，从此踏上取经之路。');
INSERT INTO `tpshop_joke` VALUES ('1552', '和女票去吃烧烤，吃完想擦擦嘴，就问女票包里有没有纸，女票曰：包里没有，下面有。哎呦我去，一口啤酒差点没喷出来，来，有本事掏出来！');
INSERT INTO `tpshop_joke` VALUES ('1553', '‍‍‍‍看到家里公鸡一只脚站立，我就问5岁的儿子说：“为啥那只公鸡能用一只脚站立啊？” 没想到儿子却说：“就你这智商，也能当爸爸。你想啊，要是另一只脚也抬起来，准得趴窝。” 想想也是啊。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1554', '考试不让提前交卷，考完化学是我提前半小时做完，没事干，就用草稿纸写叫一个纸条:“傻瓜，你上当了，费那麽大劲打开以为是答案吗？”然后用纸一层一层的包裹起来，刚包好，被监考老师看到，过来看我的试卷已经做完了，以为我要给别人传答案，连同卷子一起收走了，然后再讲台一层一层打开，最后……他那眼神……我永远也忘不了！！！');
INSERT INTO `tpshop_joke` VALUES ('1555', '‍‍我的儿子3岁了，活泼可爱，老婆溺爱有加。 一天他在沙发上玩，我在打电脑。 谁知道他一不小心，跌下了沙发。 我本想他会哭鼻子的，可是没想到，他却走过来打了我一下说：“就你这样看儿子的。” 这都是跟老婆学的……‍‍');
INSERT INTO `tpshop_joke` VALUES ('1556', '有些事发生一次可能毁掉一生，我是卖保险的，我有个女同事，一次去一个比较偏僻的公司给投保人签发保单…坐电梯时，遇到三个混混青年，电梯故障，困了他们半个小时，女同事说到这里泣不成声：妈的，那三个贱人，只看了我一眼就围在一起打了半个钟头的斗地主……');
INSERT INTO `tpshop_joke` VALUES ('1557', '两岁半的小宝贝很乖，总是时不时的用玩具杯子端水给爸爸，每次，爸爸都很得意的望着孩子妈妈一饮而尽！妈妈在旁边看着，一直默不作声... 有一天妈妈突然开口问爸爸：“你有没有想过？”“想过什么？”“整个屋子里，他唯一能拿到水的地方 ...只有马桶…”');
INSERT INTO `tpshop_joke` VALUES ('1558', '‍‍一天，一个大爷在公交上拿着手机登qq，登了好多次都没成功。 我忍不住问：“大爷，你qq是刚申请的么？” 他说：“没有，我自己想的。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1559', '中午跟舍友去食堂吃饭，舍友对食堂阿姨说： 阿姨,我要一份炒饭，要半份的，最近减肥，吃的少。 食堂阿姨愣了一下，很严肃的说：姑娘呀，我都在食堂干了五六年了，别人都拌酱拌辣椒的，你居然要拌粪的，你的减肥方式，我们真的不懂呀。');
INSERT INTO `tpshop_joke` VALUES ('1560', '今天去上班老板和老板娘正在吃饭，我就问他：昨天没约到考场。他说没，老板娘就说他笨，不会送两盒烟啊，老板说：她是女的。老板娘突然犯二说：那就送两包卫生巾。老板说：还送她避孕套呢。我真服了他们');
INSERT INTO `tpshop_joke` VALUES ('1561', '“说起我男朋友的优点，那可太多了，　　他阳光帅气、聪明细心、懂幽默、懂浪漫、有生活又会赚钱…”　　“那缺点呢？”　　“缺点当然也有，主要是懒散拖沓、好玩神秘，还有就是等了25年还不出现！”');
INSERT INTO `tpshop_joke` VALUES ('1562', '给手机下载了个两个小孩亲嘴就可以解锁的卡通解锁功能，　　今天同事借我手机打电话，问我怎么解锁，　　我说亲一下，只见她拿起我的手机放在她嘴上轻轻地亲了一下！');
INSERT INTO `tpshop_joke` VALUES ('1563', '去给我高中弟弟去买衣服，还有姑姑三人行。　　在商场门口弟弟碰到同学，我挎住弟弟胳膊，并依偎他肩膀。　　装作情侣恶搞老弟，老弟正在囧笑时，　　他同学来了句：“我靠！xxx你什么时候找了个这么挫的女友？！”　　旁边姑姑笑的更疯了。。');
INSERT INTO `tpshop_joke` VALUES ('1564', '也许你对现状不满，觉得自己胖、丑、挫、穷、笨、衰……　　但请相信一切都会好起来的，　　慢慢你就会觉得自己好胖、好丑、好挫、好穷、好笨、好衰……');
INSERT INTO `tpshop_joke` VALUES ('1565', '我家楼下新开了家饭馆，老板是一个光头。　　街上的地痞见新开了家饭馆，天天来寻事，老板都忍了。　　一天半夜这些地痞又来寻事，结果听到饭馆里一阵哀嚎。　　接下来听到救护车的鸣笛声。。。　　之后很久没见到那些地痞。　　有一次我路过饭馆见几个少林僧人对老板行礼：“大师兄，最近可好？”');
INSERT INTO `tpshop_joke` VALUES ('1566', '前几天，有5000名英国最顶尖的马拉松选手参加了全英北马拉松大赛。　　可是！最后只有一个人跑完全程。（所以自然拿到冠军）　　因为。。其他人因第一名跑太快。。　　而跑在第二名的人又跑错了岔路。。　　所以带着后面4998人全部跑错了。');
INSERT INTO `tpshop_joke` VALUES ('1567', '某男晚上失眠，于是打电话骚扰好友。　　好友不耐烦地说：“你到底想干嘛！”　　他说：“那你告诉我失眠怎么办。”　　好友无奈地说：“你数羊吧。”　　他吼道：“我属羊怎么了，属羊就该睡不着吗？”');
INSERT INTO `tpshop_joke` VALUES ('1568', '一对O型血夫妻诞下第二个孩子，　　妻子说我们一家O在一起就是一辆奥迪，　　丈夫说再生一个就成奥运会了。');
INSERT INTO `tpshop_joke` VALUES ('1569', '今天强哥接了个电话，突然和电话里的对方骂起来了。　　突然强哥站起来，把手机贴屁股上，脸憋的通红。　　正在我纳闷的时候，“噗”的一声，　　伟大的强哥对电话那头放了个屁，嘴里还说：“熏死你！”');
INSERT INTO `tpshop_joke` VALUES ('1570', '下午听到俩初中生在说：“凯迪拉克和宝马哪个好…等工作就买一辆。”　　我好想说：“叔叔以前也想过……后来还是觉得捷安特比较好…”');
INSERT INTO `tpshop_joke` VALUES ('1571', '甲：咱们厂长讲起成绩没个完，对问题怎么一句也不谈啊？　　乙：这就叫扬长避短。');
INSERT INTO `tpshop_joke` VALUES ('1572', '两个人对话。　　甲问乙：女孩身上有个部位，爸爸可以碰到两次，妈妈也可以碰到两次，男朋友只能碰到一次，而老公一次也碰不到，你说这是哪个部位呢？　　乙想了半天也想不出来，最后甲告诉乙说：嘴唇啊，叫爸爸的时候嘴唇碰2下，妈妈也是2下，叫男朋友就只碰了一下，而老公……就一下也没碰着……');
INSERT INTO `tpshop_joke` VALUES ('1573', '文革时期，某大学一历史系教授以批林孔发了家。一天，他主持系里教师讨论一历史事件。讨论会沉默许久，在主持人的再三催逼下，只有一个教师发言，他说：我就不讲了，因为会议主任讲得很透彻，他是修改历史的专家。');
INSERT INTO `tpshop_joke` VALUES ('1574', '猫和奶牛打招呼。奶牛却取笑猫说：你这么小就长胡子了!猫很生气，说：你怎么这么大了也不戴胸罩。');
INSERT INTO `tpshop_joke` VALUES ('1575', '一位杂货商、一位银行家和一个政客在林子里迷了路。终于他们找到了一家农户，要求借宿过夜。 农民说：可以。但是我只有两个人的地方，另外一个人只能睡到牲口棚里，那里气味不好。 我睡牲口棚。银行家自告奋勇。 半个小时后，有人敲门，门口站着银行家，他喘息着说：我快要被熏死了。 好吧，杂货商说，我去牲口棚睡。 过了一会儿，又是一阵敲门声。我能忍受腐烂食物发出的臭气，可你的牲口棚臭味更甚。杂货商抱怨说。 政客说：你们两个真娇气，我去。 半个小时后又响起了敲门声。农民打开门，门口站着牲口棚里所有的牲畜。');
INSERT INTO `tpshop_joke` VALUES ('1576', '一名男子被判刑12年，在狱中颇为无聊。一天，他发现有一只蚂蚁竟然听得懂他的话，于是便开始训练它。几年之后，这只蚂蚁不但会倒立，还会翻筋斗，令他颇为得意。 终于他出狱了，第一件事便是跑去酒吧，准备炫耀他那只神奇的蚂蚁。他先向酒保点了一杯啤酒，然后把蚂蚁从口袋里掏出来放在桌上，向酒保说：看看这只蚂蚁……那酒保过来，马上一把将蚂蚁拍死，然后很抱歉地对他说：对不起，我马上换一杯新的给你！');
INSERT INTO `tpshop_joke` VALUES ('1577', '某日某国，某渡船缓缓离开码头。船上的乘客来自多个国家。船至河中船长发现由于超载,船就要倾覆了，既回不去又到不了，唯一的办法就是甩掉几个乘客。船长先找到一个日本人，告诉他船要沉了，唯一可以逃生的办法就是游过去，赖在船上就是死路一条。怕死的日本人逃生技术高超，立刻跳入水中。船长又找到一个德国人，告述他这是服从纪律的需要。德国人也跳下去了。船长又告诉法国人，你别怕，你跳水的姿势一定非常漂亮。虚荣心得到满足的法国人也跳下去了。到了苏联人，船长告诉他这是革命的需要。苏联人二话没说跳了下去。船长又找到一个美国人，美国人不干。船长说：放心！你的船票中我们已经给你投了100万美元的保险。美国人也跳了下去。最后，到台湾人，船长一踢他的屁股，喊到：你给我跳下去？台湾人问：为什么？凭什么？船长呵斥：美国人都跳下去了你还等什么！台湾人立刻跳入水中。');
INSERT INTO `tpshop_joke` VALUES ('1578', '有3只老鼠：美国老鼠、英国老鼠及香港老鼠在香港碰上了。　　美国老鼠说：我们那的老鼠，特有本事，看到老鼠药就拿来都吃了。那玩意经饿。　　英国老鼠说：我们那的老鼠，也没什么，吃完老鼠药，还拉一下老鼠夹子锻炼一下，活动活动以帮助消化。　　香港老鼠说：我们那的老鼠，吃饱了没事，就上街泡两母猫。');
INSERT INTO `tpshop_joke` VALUES ('1579', '我家附近有一条比较深的胡同，是我每天回家的必经之路。前天下班回家，拐进胡同走到一半，忽然闯出一个白乎乎的东西，由于光线较暗，吓了我一跳。仔细一看，原来是条小狗。它耀武扬威地向我奔来，看它还没我家那只老猫大，我放下心来。可小狗似乎并不满意这个结果，竟狂吠着想来咬我。我一乐，嗬！就你那小样也想来咬我？ 我吓也要吓死你！便也冲着它狠狠地嚎了两下：汪！汪！小狗立马顿住脚，盯着我看了一下，忽地转身跑远了。　　我正得意，却听见胡同那头传来一个老太太的声音：叫你好好在家门口玩儿，你不听，又遇见那只大狗了吧？');
INSERT INTO `tpshop_joke` VALUES ('1580', 'A: 我想驯我的狗，让它想吃东西时就叫。　　B：这应该是很容易的事嘛！　　A：我已经教了它足有100次了。　　B：怎么样，它会叫了吗？　　A：不叫，但如果我不学狗叫，它就不吃东西。');
INSERT INTO `tpshop_joke` VALUES ('1581', '老师:“小明，你能说说生不如死是什么意思吗？” 小明:“就是活着还不如死了。” 老师:“请举个例子。” 小明:“停尸房有空调而我们寝室没有！”');
INSERT INTO `tpshop_joke` VALUES ('1582', '今天和女友聊天。 女友：知道我脸为什么这么大吗？ 我：因为你胸小肉转移了？ 女友：不是。 我：脑积水？ 女友：不是！是特么老子长得太漂亮！所以放大了给你们看！ 我竟无言以对……');
INSERT INTO `tpshop_joke` VALUES ('1583', '一美女坐出租车刚上车，嘣的一声放了巨响的一个屁，美女甚是尴尬。 这时司机抽了一口烟，缓缓的说：“屁是你所吃的食物不屈的亡魂的吶喊。” 美女一笑尴尬尽消，说：“师傅，你好文艺范啊！” 师傅摇头说：“可是这呐喊声也太大了，我特么以为爆胎了！”');
INSERT INTO `tpshop_joke` VALUES ('1584', '今天惹老婆生气了，百般哄，她扔一盒套套在我面前说，除非今晚用完…老婆去洗澡了，我在天花板上布置了一堆气球，是不是很浪漫…');
INSERT INTO `tpshop_joke` VALUES ('1585', '一个女性闺蜜跟我抱怨她的丈夫实在太敏感了。 “敏感到什么程度？”我问她。 她说：“反正只要我叫几声，嗯……哦……啊……不要停……他就射.了。” 我打断了她的话：“不好意思打断一下，你有纸吗？”');
INSERT INTO `tpshop_joke` VALUES ('1586', '今天收到QQ信息，如下：\r\n陌生人：你好，在吗？\r\n我：在。\r\n陌生人：有男朋友了吗？\r\n我：有啊，怎么了。\r\n陌生人：周末该见面谈谈了。\r\n我：你谁啊\r\n陌生人：你老娘');
INSERT INTO `tpshop_joke` VALUES ('1587', '老师：谁能出一道关于时间的问题？\r\n小明：我第一次可以坚持30分钟，休息10分钟可以来第二次，第2次的时间是第一次得一倍，请问我做两次一共用了多长时间！\r\n老师：非常好，放学来我办公室！');
INSERT INTO `tpshop_joke` VALUES ('1588', '在网上看到一个视频，说的是一个女的看起来挺瘦弱，可是一口气能吃几十个汉堡，我把这个视频的网址转发给老公。晚上我问老公：“有没有看到视频，这女的太能吃了，我都替她老公担心，养她不容易啊！”老公说：“你别瞎操心了，我看你的饭量也不小，还不是一样过日子。”我瞪大眼不服道：“我可没那么能吃，完全没可比性。”老公瞟我一眼，慢悠悠地道：“确实没可比性。人家拍这样的视频赚点击率能挣钱，你吃是纯属浪费钱。”');
INSERT INTO `tpshop_joke` VALUES ('1589', '期末考试，历史考试的时候，小明交了一份白卷。 老师问：“你为什么交白卷？” 小明回答：“因为我不想纂改历史！”');
INSERT INTO `tpshop_joke` VALUES ('1590', '和女朋友相亲时，刚开始聊的还挺不错，突然她盯着我的脸看了大概十秒钟，然后眼神变得飘忽不定，几次欲言又止，显得很局促… 我心里犯嘀咕:这是嫌我长的丑？不好意思拒绝？ 于是对她说:“没事，该说什么就说吧，做不成恋人还可以做朋友嘛。” 她想了一下，羞怯的指着我的脑门说:“那，那个痘痘鼓出头了，我可以帮你挤了吗？” 你这是病！得治…');
INSERT INTO `tpshop_joke` VALUES ('1591', '刚看了下统计，内裤一天不洗就会有0.1克的屎，这些屎里面包括了1000万个病毒，100万个细菌和1000个寄生虫的蛋，原来老子也是有生化武器的人，别惹我，一裤衩弄死你。');
INSERT INTO `tpshop_joke` VALUES ('1592', '某企业引进一条香皂包装生产线，结果发现经常有空盒流过。厂长请一个博士后花了两百万设计出了自动分检系统。一乡镇企业遇到同样问题，农民工花五十买了一大电扇放在生产线旁，有空盒经过便被吹走。启示：文凭不代表能力；知识不一定能转化为生产力；能吹很重要！');
INSERT INTO `tpshop_joke` VALUES ('1593', '我一大学同学学的是水产专业，那天他和几个同学去吃鱼，吃完无聊，拿起鱼骨头拼了起来，结果怎么都拼不完整，发现少了一节，于是找老板理论，最后老板答应免单');
INSERT INTO `tpshop_joke` VALUES ('1594', '　　员工：老板，我要加薪！ 　　老板：这个…… 　　员工：我还要带薪休假！ 　　老板：那个…… 　　员工：怎么说？ 　　老板：这样子吧，我们各退一步！ 　　员工：怎么退？ 　　老板：我不给你加工资，你也不要带薪休假！');
INSERT INTO `tpshop_joke` VALUES ('1595', '　　向一个女汉子表白。 　　她竟然说：老娘拿你当兄弟，你竟然想上我！ 　　靠，妹子你不按套路出牌啊。活该你单身这么久。');
INSERT INTO `tpshop_joke` VALUES ('1596', '队长拿着刚打印出来的通缉犯照片，给警犬闻了闻，道：“闻一闻！就是他！抓住给我往死咬！”当天下午，警局负责打印通缉犯照片的李打印员被警犬咬死。');
INSERT INTO `tpshop_joke` VALUES ('1597', '. 刚在家算了一下，…甄嬛是雍正的妃子，她的儿子当了皇帝成了乾隆，那么还珠哥哥里老是跟小燕子紫薇过不去的老佛爷就是甄嬛，老佛爷身边的桂嬷嬷就是槿夕姑姑，那晴儿是某王爷战死沙场福晋自杀留下的遗孤，那不就是果郡王的孩子咯，怪不得老佛爷那么喜欢晴儿～全都连上了呢～我真棒');
INSERT INTO `tpshop_joke` VALUES ('1598', '　　有一只黄鼠狼，他在养鸡场附近立了个木牌，写着：“不要以为你是鸡，你们或者是一只老鹰，你们就应该学飞翔。”以后，黄鼠狼每天都能吃到鸡。');
INSERT INTO `tpshop_joke` VALUES ('1599', '和老婆发生了矛盾，就打了起来，结果我被打进了医院。 朋友和一位大爷在安慰我，大爷说：“小伙子，你这都算好的了， 上回我看见一个小伙子被他老婆打的跟木乃伊似的。” 我只想说∶“大爷，上回那个也是我。”');
INSERT INTO `tpshop_joke` VALUES ('1600', '有一只老狮子病了，躲在洞穴中大声呻吟，附近的一些动物听到狮子的呻吟声，纷纷进洞探视。 狐狸听到了这消息，也前往探视，走到洞穴前，只听到老狮子呻吟声越来越大，可怜极了，这时原本打算进去的狐狸，忽然竖起耳朵，收回已经跨进洞穴的前脚，在洞穴四周来回踱步。 洞里的老狮子眼见狐狸迟迟没有进洞，忍不住问狐狸说：“狐狸啊！你既然来了，为什么不进来呢？” 狐狸回答：“我只见一些往里走的动物脚印，却看不到往外走出来的脚印，我怎么敢进去呢！?” 启示：凡事“进易退难”,因此平时应该训练自己对环境的观察力，提高对社会的敏锐度，谋定而后动，才能让自己立于不败之地；率性莽撞的作为，则很可能会将自己处于万劫不复的不利险境。');
INSERT INTO `tpshop_joke` VALUES ('1601', '今天在大街上看见一美女似乎在找什么东西，我吹着口哨走上去色迷迷的问：美女，你找啥呢？ 美女白了我一眼：“流氓” 我赶紧说到：“不用找啦，我就是啊。”');
INSERT INTO `tpshop_joke` VALUES ('1602', '老婆：老公，今天我在外面算命你猜说我啥？ 老公：还用猜？他一定说你是个旺夫相对吧？ 老婆：啊，你怎么知道？ 老公：就你这长相，我一看没有性趣，就你这脾气，我在外啥也不敢干，除了赚钱还是赚钱，能不旺夫吗？ 老婆：啪！！！');
INSERT INTO `tpshop_joke` VALUES ('1603', '老师上课叫小明起来回答问题，想锻炼他的胆子。小明弱弱的说到：......老师我.....我不会...老师便说：就不能像个男子汉一样?小明若有所思....终于，小明怒拍桌子，大吼一声，“老子不会！”');
INSERT INTO `tpshop_joke` VALUES ('1604', 'A：史上谁的性功能最强？B：许仙吧，蛇一次交配需要4个小时。C：斗破苍穹里的萧炎不错，他为取地莲青火与美杜莎做了三年。D：你们弱爆了，史上性功能最强的是葫芦娃他爹，抱着颗树都能草出七个娃来。E：孙悟空他爹表示不服。');
INSERT INTO `tpshop_joke` VALUES ('1605', '邻居问刘先生：“我今早听到从你家里发出了女人尖叫声，这是怎么回事呢？” 刘先生：“这是我老婆发出的，因为我当时心血来潮就偷偷亲了下她。” 邻居：“哎呀！都是老夫老妻了亲一下干嘛还大惊小怪的？！” 刘先生：“不巧的是我忘了嘴里还叼着根烟。”');
INSERT INTO `tpshop_joke` VALUES ('1606', '今天考试，刚发卷两分钟，室友就交卷了 ，我就纳了闷了，这抄也得抄二十分钟吧 ，回来我问他，你怎么交这么快啊？室友 说：“我看了一遍，都会，肯定过了，我就 交卷走人了～”');
INSERT INTO `tpshop_joke` VALUES ('1607', '在路上偶遇学生时代暗恋过的那个女神，长得比以前还要漂亮了，只是少了点活泼可爱，多了份成熟性感。 她对我笑笑，开口说：“老同学，好久不见啊~”我静静看着她，鼻子一酸，流下两行鼻血！');
INSERT INTO `tpshop_joke` VALUES ('1608', '下楼取快递，看见快递员穿的羽绒服和我网购的是同一款，我说：这么巧，我也买了一件这个。 他边说边脱：这件就是你的，车子装不下了，只好穿着了。');
INSERT INTO `tpshop_joke` VALUES ('1609', '两个女人在天堂聊天问对方是怎么死的： 女A：我是被冻死的! 女B：我是心脏病突然发作而死的，因为我怀疑我先生有外遇而提早回家，看他自己一個人在看电视，不合常理。 女A：后来呢？ 女B：我直觉确定有个女人藏在我家里，我找遍了全屋子，上至屋顶，下至地下室，看床底，搜衣柜，都翻遍了，最后累得心脏病突然发作死了。 女A：你真傻啊，你要是打开冰箱看一看。咱俩就都死不了了。。。');
INSERT INTO `tpshop_joke` VALUES ('1610', '提问:为什么我每次做爱买最大号的TT都还是太小了，我的JJ真的有那么大吗？难道国内就没有更大的TT吗？ 神回复:“兄弟！蛋不用放进去的。”');
INSERT INTO `tpshop_joke` VALUES ('1611', '一、根据数学家研究，男人的双手伸开之时所形成的圆弧跟女人的胸部凸起的圆弧极相吻合。 \r\n二、根据医学家研究，男人身上水份最少的地方是双手，而女人身上水份最多的地方则是胸部。 \r\n三、根据按摩家研究，女人用手给男人按摩最好的地方就是男人下面的有底裤裆，而男人用手给女人按摩最佳的去处就是胸部。 \r\n四、根据美学家研究，男人最酷的姿势就是将要与人打架挥手的那一刹那，而女人最美的姿态就是胸部略微挺起的那一时刻。 \r\n五、根据文学家研究，男人的双手是女人写文章灵感最多的地方，而女人的胸部则是男人写文章灵感最多的位置。 \r\n六、根据地理学家研究，男人身上韧性最强的部份就是双手，而女人身上韧性最弱也就是最柔软的地方就是胸部。 \r\n七、根据心理学家研究，男人在****女人的时候，第一欲望就是摸胸，第二才是向无底裤裆进攻。 \r\n八、根据生物学家研究，男人和女人的身体生来就是相互配合的，比如男人的有底裤裆以及女人的无底裤裆，而男人的手以及女人的胸部也是如此。 \r\n九、根据历史学家研究，史上众多太监中，他们的手最后基本上都变得软弱无力，而没有被皇上宠幸过的妃子的胸部，则全部也变得松松软软的。 \r\n十、根据运动学家研究，男人经常用手抚摸女人的胸部可以增强韧性，而女人经常被男人抚摸胸部也可以增加水份，对于下一代孩子吃奶是颇有好处的。');
INSERT INTO `tpshop_joke` VALUES ('1612', '什么是权利无监管？举个真实例子。上星期单位领导开始限网速，单位管理阶层才能上网，其余人员只能上QQ，网页80端口几乎全封闭，系统更新也封了。于是广大需要网络的人员干瞪眼，管理阶层上班就上网打打牌，工会什么的屁都不放一个。你以为会有爆动？扯吧，一伙人已经去楼下办4g移动卡了。');
INSERT INTO `tpshop_joke` VALUES ('1613', '主持人问：“您还经常去看医生？” “是的，常去看。”“为什么？”“因为病人常看医生，医生才能活下去。”主持人接着问：“您常去药店吗？”“是的，常去。因为药店老板也得活下去。”台下又是一阵掌声。“您常吃药吗？”“不，因为我也要活下去。”台下大笑。');
INSERT INTO `tpshop_joke` VALUES ('1614', '裸男跑到停车场，看门的大爷目不转睛盯看着裸男，\r\n裸男害羞道：你别这样看我呀，大家都是男人！\r\n看门的大爷：哦，我眼睛不好，看了半天没看出你是男是女！');
INSERT INTO `tpshop_joke` VALUES ('1615', '走在大街上，突然一人急忙忙地向我跑来。\r\n问：“你好，请问110电话多少号啊？”\r\n我：““你傻啊？110多少号你不会打114问呀？”\r\n说着那人就掏出了手机，拨打了114。');
INSERT INTO `tpshop_joke` VALUES ('1616', '朋友去河里钓鱼，中午去的晚上回来的！我问钓到多少？\r\n他做了一个八的手势，我说八斤？\r\n他摇摇头，我又问那是八条？\r\n他也摇摇头！\r\n我说那是多少？\r\n他突然对我吼到就钓到这么长一条。');
INSERT INTO `tpshop_joke` VALUES ('1617', '我家有一只狐狸犬。有天我出差半个月刚回到家它就冲我跑过来。\r\n而且貌似跳起来想让我抱它，我突然发现家门口有一个一元硬币。\r\n随即蹲下去捡，结果狗狗华丽的从我头上飞过去撞大门上了。\r\n从此我进门就不搭理我了！');
INSERT INTO `tpshop_joke` VALUES ('1618', '临沂这两天热屎了、蚊子什么的都出来了，\r\n昨天老弟在做作业，我在旁边看着他，突然飞来一直讨厌的蚊纸，\r\n扑哧扑哧飞到老弟的胳膊上了，我便提醒他赶紧拍死，\r\n谁知道这货幽幽地说：“慢着，不急，等他喝饱了我再拍，\r\n那样拍下去鲜血直溅比较有快感。”');
INSERT INTO `tpshop_joke` VALUES ('1619', '小店的老板半夜里被一个强盗从床上拎了起来！\r\n强盗手持利刃，恶狠狠地威胁道：“把钱都交出来！”\r\n小老板委屈地说：“实在没有办法，昨晚您的同行已经来把钱都拿走了。”\r\n强盗气愤地吼道：“你为什么不把门锁好！”\r\n小店老板：“……”');
INSERT INTO `tpshop_joke` VALUES ('1620', '小时候和铁哥们一起睡，他喜欢用手摸着我的耳朵才能睡着。昨晚陪老婆睡觉，突然想起他，我就摸着媳妇的耳朵，她居然说：“你睡觉怎么和某某一样啊!”我居然笑着说：“那么多年他还是没变!”安然睡去，早上起来我一直在找结婚证。');
INSERT INTO `tpshop_joke` VALUES ('1621', '在淘 宝上看到一件衣服，评论都是差评，唯独一个好评，好评这样写，“给闺蜜买的，闺蜜穿起来很丑，我太高兴了。”');
INSERT INTO `tpshop_joke` VALUES ('1622', '朋友聚会一哥们儿想对女神表白有不知怎么开口。在朋友们的提议下。我大义凛然地把女友借给他。让他现场练习表白。之后就是一段深情的表白。表白过后所有人都为其鼓掌。当时我脑子抽风大声喊:“在一起，在一起……”然后就没有然后了。女友变嫂子。你们婚礼我送点啥好呢？');
INSERT INTO `tpshop_joke` VALUES ('1623', '看到一则说说:右手陪伴了我八年，终于要 结束了！ 神评论1:换左手了？ 神评论2:右八年，左八年，双手搭档又八 年');
INSERT INTO `tpshop_joke` VALUES ('1624', '隔壁家老王被叫到了学校。“你儿子太不听话了，他把土钉放在女同学的凳子上。”隔壁家老王：“他还小，不懂事嘛，不就是个恶作剧么，让他道个歉就行了，干嘛还把我叫到学校？早知道大爷分分钟几百万上下。”老师：“问题是他还非说钉子上有情花剧毒，他把人家女孩子的裤子扒了，要帮人家吸毒…”');
INSERT INTO `tpshop_joke` VALUES ('1625', '上次充话费，结果充错了，给别人充了100，想来想去给那人打了个电话，结果是个妹子，便约出来要她陪我聊会，见面了是个美女，从此以后就一直帮她充话费，后来终于成我女朋友了。爸妈问我怎么谈的，我回答说：货真价实的充话费送的。。');
INSERT INTO `tpshop_joke` VALUES ('1626', '读初中的时候，我们班有一个人叫卷毛。因为他的头发很卷很卷，有一天他的同坐从他的头上拔了一根毛下来，给后面的美女看，没想到这美女立刻打了报警电话**中学性骚扰');
INSERT INTO `tpshop_joke` VALUES ('1627', '1，上海一男子跳河自杀，嫌河脏又爬上来了。 2，沈阳一男子近视2100度，结婚14年没看清老婆的模样。 3，美国一女子开枪自杀，中弹十八发而没死，于是自己开车去找医生. 4，俄罗斯一醉汗私闯民宅，被一老汉用黄瓜拍死。 5，山东一小偷入室盗窃，误将鞭炮当蜡烛，被炸成重伤。 6，重庆市一派出所，110电话不交电话费，被停机二十天。');
INSERT INTO `tpshop_joke` VALUES ('1628', '昨天无聊看了一次黄日华演的《天龙八部》，突然发现这电视剧其实应该叫《爸爸去哪儿》，萧峰找到爹了，虚竹找到爹了，段誉找到爹了，王语嫣找到爹了，慕容复找到爹了，突然发现导演这是在逗人玩～');
INSERT INTO `tpshop_joke` VALUES ('1629', '像往常一样去买早餐，排队中，突然，听到一个妹子急声喊到：“老板，不要这样”，心想，有人要耍流氓，刚要出手相救，早餐老板说到，“不要酱啊，好的”');
INSERT INTO `tpshop_joke` VALUES ('1630', '调皮捣蛋的他为何端庄正坐？吵闹寒暄的教室为何肃然安静？网瘾如毒的网虫为何放下了手机拿起课本？嚣张跋扈的她为何放开了同学拿起了纸笔？种种谜团为何突发而起？静请关注下期的走进科学‖窗户后面的一张脸‖');
INSERT INTO `tpshop_joke` VALUES ('1631', '听着昔日的同学讲述我的初恋--他的近况：结婚两年多了，老婆长的很漂亮，给老婆买了一台911，给自己买了一台卡宴。听到这里，我难过的说不出话来，早知道这样！就应该要他来我老公开的4S店来买车啊！');
INSERT INTO `tpshop_joke` VALUES ('1632', '阿呆最近发了笔小财，于是开了一个养鸡场，养了几百只小鸡和一群母鸡，结果过了几天后那群小鸡全都死光了。朋友问他道：“你喂那些小鸡吃啥了呀？”阿呆回答道：“我没喂东西给小鸡吃呀。”朋友惊讶道：“你不喂它们东西吃，小鸡当然会死呀。”阿呆不解道：“小鸡不是应该由母鸡来喂吗？”');
INSERT INTO `tpshop_joke` VALUES ('1633', '办公室里，与女同事闹着玩，女同事十分生气。 恶狠狠地对我说：“不要惹我了，再惹我我可就发毛了！” 坐在角落里的一个声音飘过来：“元宵节发毛？一人一根吗？”');
INSERT INTO `tpshop_joke` VALUES ('1634', '我新时代大学生一枚，喜欢上我们班一漂亮妹子，于是每次上课我都坐她旁边，可是最近我们班的班花总是坐我们两个中间，还总和我搭讪，你说，你说，她到底是嘛意思吗？嘛意思……');
INSERT INTO `tpshop_joke` VALUES ('1635', '‍‍老婆：“老公，你觉得伦家的身材咋样？” 老公：“嗯。可以用一首歌名来形容。” 老婆：“哪首歌？” 老公：“十五的月亮。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1636', '今天骑摩托车带同事去跑业务，路遇前方五六个交警查车，（本市禁摩）我准备趁交警不注意冲过去。于是对后面同事说：抱紧、抱紧！结果那个S B 拿出手机果断拨了110。');
INSERT INTO `tpshop_joke` VALUES ('1637', '‍‍昨天到隔壁串门，邻居是个妹纸。 聊了会儿天，临走时，妹纸问我：“你看过西游记，孙悟空拜师学艺吗？” 我说：“看过。” 然后，她就在我头上敲了3下，把我送出门了。 一晚上没睡着，真不明白她为什么敲我头！‍‍');
INSERT INTO `tpshop_joke` VALUES ('1638', '　　突然想起一次跟几个哥们在夜宵摊吃宵夜… 才刚上酒就跟隔壁桌为点小事闹起来了，越闹越凶，隔壁桌一人摔了个酒瓶对着我们，我们也不甘示弱，纷纷都摔个酒瓶针锋相对比气势…… 我们这边一哥们拍了下桌子，大吼一声：有你们这么浪费酒的吗？ 只见他开了瓶酒，咕隆咕隆喝完了，瓶子一摔就开干…… 我和我的小伙伴们都惊呆了');
INSERT INTO `tpshop_joke` VALUES ('1639', '昨天老公去参加单位里面的自助餐活动，是很高级的那种自助餐，吃完临走的时候每一个人发一盒哈根达斯，刚吃一口就听见同事说一盒要六十几块呢，老公听后给同事们告别 匆匆回家。那是我第一次吃哈根达斯 不....是喝哈根达斯。');
INSERT INTO `tpshop_joke` VALUES ('1640', '‍‍老师：“小明，你为森马要勒索同学的钱？” 小明：“老师，俺没有勒索，俺那是给随地乱扔垃圾的同学开罚单呢！他们心眼真小，你看那些警察给大人开罚单，人家那些大人从来不去告警察的老师。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1641', '还能相信感情么，以前的积蓄，工资都给她花，洗衣做饭端茶送水任何事情都是我做，她朋友找她想去哪玩就去哪玩，每次我都还给她当司机接送，自己什么都舍不得买，上下班为了省油钱都是坐公交，还放弃了自己的世界投入到她的世界里，业余的时间哪哪都陪着她，可还是嫌弃我房子两室两厅小了，大城市挤了，离开我回老家小县城去了，刚回去就找了个条件比我好的，彻底凌乱了，从一个风流倜傥被女孩子围到转的改变成了家庭好煮男，反而会是这种结果，看来确实是男人不坏女人不爱。');
INSERT INTO `tpshop_joke` VALUES ('1642', '‍‍今天做公车，半路靠站后，一个老头先下了车，回头向车上喊：“老婆子，下车啦！到站啦！回家啦！” 喊了两次没人搭理。 突然他脸色凝固，嘴里碎碎念，一仔细听：“老婆子已经不在了啊！不在了！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1643', '很多女人都喜欢这种好男人：早上6点起床，晚上9点上床睡觉。不抽烟不酗酒，不逛夜店不被诱惑，更没绯闻。生活中没秘密可言，不藏私房钱，不玩微博微 信，连暧昧短信都没有。稳重随和，平时不是静静呆着思考未来，就是读书学习，非常听话，衣着整洁常新。这样的男人在这很多…——省监狱管理局 宣');
INSERT INTO `tpshop_joke` VALUES ('1644', '‍‍哥哥：“我离婚了。” 弟弟：“为啥啊？” 哥哥：“我讲了个笑话她没乐。” 弟弟：“一个笑话，不至于啊！” 哥哥：“她是没乐，衣柜里传来哈哈声。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1645', '今天早晨跟媳妇说:“媳妇，我要带你远走高飞，去一个大的城市旅游一年半载。”说完媳妇给我一个大嘴巴，骂道:“说人话！”…………媳妇咱们外出打工吧');
INSERT INTO `tpshop_joke` VALUES ('1646', '我：我去买了包烟，没带火，所以说老板娘借个火。 老板娘：没有。 我：借个火。 老板娘：没有，买烟还借个火，我这还卖避孕套，是不是要腾地方打一炮，滚蛋！ 我：......');
INSERT INTO `tpshop_joke` VALUES ('1647', '有一天，一位外国的妙龄女子来到单身已久老板身边，用外语和老板搭讪。 老板担心自己外语不佳，便在纸上用画来交谈，两人在纸上谈得很尽兴，最后女子在纸上画了一张床，老板大喜过望，不顾外语不佳说：“你怎么知道我卖家具？” 说完便用熟练的语气介绍起了自己的家具。');
INSERT INTO `tpshop_joke` VALUES ('1648', '如果以后我当贼的，我就过年去偷小学生的房间，有压岁钱啊，多啊！顺手也把他的寒假作业也带走！你儿子跟你说小偷偷了你的钱和寒假作业你信么？？？');
INSERT INTO `tpshop_joke` VALUES ('1649', '老公抽烟时放了个屁！此为背景！ 二货老婆：咦！绿色通道，这边进那边出！ 老公：咳！咳！咳！（烟从嘴和鼻孔里冒了出来） 二货老婆：哎呦喂！兵分两路又回来了！ 老公：你过来，我保证打不死你！');
INSERT INTO `tpshop_joke` VALUES ('1650', '早上座公交，好不容易有个座位刚坐下就上来一老大妈，无奈只好起身让座。 老大妈很是健谈：“小伙子不错，长的也好看，多大了啊？” “23了，奶奶。” “诶呦，我有一孙女，也23了。” 听到这里我瞬间就来了精神，“这么巧阿，看您这样孙女肯定也挺漂亮阿。” “是阿，同样23，人都已经买车了。”');
INSERT INTO `tpshop_joke` VALUES ('1651', '昨夜回家，看到楼下一小车里有一对被男女好像锁车里了。看样子很热，衣服都脱了，女的表情特难受，好像缺氧要死了，男的正嘴对着嘴做人工呼吸，人命关天，不容迟疑，我赶紧找个砖头把车玻璃砸碎，男女马上跳出来获救了，嘴里还一直喊着“是谁”!是谁!可是我已走远，，传播正能量，让世界充满爱～ 别问我是谁，我是我雷峰他弟，雷管!!');
INSERT INTO `tpshop_joke` VALUES ('1652', '邻居家的孩子出生了，越长越不像他爸，不像他爸也就算了，你TM也别像我呀！');
INSERT INTO `tpshop_joke` VALUES ('1653', '这世间最大的悲剧莫过于此了！转的：住在隔壁的一个男的，经常看岛国爱情片，声音还放的大，有次实在无法忍受了，就叫上两个人，把他抓住，当着他的面，把他电脑BCDE硬盘中大概六百多G的片子，全部格式化了，到现在始终记得他那杀猪似的惨叫……');
INSERT INTO `tpshop_joke` VALUES ('1654', '女人饿了，男人说：要不给你买点吃的？女人一般不会太开心，如果男人说：走，带你去吃饭。一般结局都很完美。比如一对恋人，女人要下火车，男人发短信，要不我去接你？女人多半会说，不用了。如果说，放心吧，我在车站接你。一般彼此都会很愉悦。女人不知道吃什么，男人说：没事儿，想吃什么你就说吧，不如说，那跟我走吧，带你去吃什么什么。一些事情上，肯定的态度，要比征求女人意见会更能夺得女人欢心。女人可能更愿意听你说，走，带你去吃什么什么，走，周末带你去哪里哪里玩，走，我买好票了，咱们去看电影，没事，回来晚了我接你……同感的请转发， 一个人，一辈子最重要的事，其实就是选对身边的人——炊烟起了，我在门口等你。夕阳下了，我在山边等你。叶子黄了，我在树下等你。月儿弯了，我在十五等你。细雨来了，我在伞下等你。流水冻了，我在河畔等你。生命累了，我在天堂等你。我们老了，我在来生等你！');
INSERT INTO `tpshop_joke` VALUES ('1655', '10岁弟弟不好好学习，还和他妈顶嘴，把他妈气坏了。他爸帮腔说：“这是我媳妇你凭什么惹她生气，等你将来娶媳妇了，我也这么气你媳妇你乐意啊？”谁知弟弟上去搂她妈亲一口说：“那我敢亲你媳妇，你将来敢亲我媳妇吗？”');
INSERT INTO `tpshop_joke` VALUES ('1656', '晚上，天很热…舍友一：卧槽，在宿舍怎么也睡不着，怎么找不到在教室睡觉的感觉呢？我要去教室睡觉，有一块的吗？舍友二：走，一块去，老师上课的录音我都准备好了。');
INSERT INTO `tpshop_joke` VALUES ('1657', '三、傍晚，一只羊独自在山坡上玩。 突然从树木中窜出一只狼来，要吃羊，羊跳起来，拼命用角抵抗，并大声向朋友们求救。 牛在树丛中向这个地方望了一眼，发现是狼，跑走了； 马低头一看，发现是狼，一溜烟跑了； 驴停下脚步，发现是狼，悄悄溜下山坡； 猪经过这里，发现是狼，冲下山坡； 兔子一听，更是箭一般离去。 山下的狗听见羊的呼喊，急忙奔上坡来，从草丛中闪出，一下咬住了狼的脖子， 狼疼得直叫唤，趁狗换气时，怆惶逃走了。 回到家，朋友都来了， 牛说：你怎么不告诉我？我的角可以剜出狼的肠子。 马说：你怎么不告诉我？我的蹄子能踢碎狼的脑袋。 驴说：你怎么不告诉我？我一声吼叫，吓破狼的胆。 猪说：你怎么不告诉我？我用嘴一拱，就让它摔下山去。 兔子说：你怎么不告诉我？我跑得快，可以传信呀。 在这闹嚷嚷的一群中，唯独没有狗。 真正的友谊，不是花言巧语，而是关键时候拉你的那只手。 那些整日围在你身边，让你有些许小欢喜的朋友，不一定是真正的朋友。');
INSERT INTO `tpshop_joke` VALUES ('1658', '今天在大街上逛街，看见一个长得清纯的妹子，于是就上去想打一个招呼，我还没有开口就听见她她:帅哥，你好啊，请问你要干什么。她把那个“干”字说的很重。我一想有戏，于是我盯着她的胸部说:请问你这的奶多少钱。她说:五元。我又看着她的身体说:全部多少钱，她说:不贵，就要200元。我心头大喜，说我赚了，我说:可以送到家吗？她说可以然后我就在家喝了一个月的牛奶。');
INSERT INTO `tpshop_joke` VALUES ('1659', '‍‍‍‍八戒说：“猴哥呀，你知道当年为什么我调戏嫦娥的时候会被发现吗。” 猴哥：“为什么呢？” 八戒：“如今都爱玩车震，那算个屁呀，当初我干那事的时候，月球震动太明显了，被玉帝看到了！其实，嫦娥妹子才不会揭发我呢，因为她也寂寞呀！玉帝那家伙，嫉妒我，所以把我变成猪，这是做给嫦娥看的。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1660', 'A：你爸爸给你取名叫雨婷有什么含义吗？B：我妈说，怀我那会老是下雨');
INSERT INTO `tpshop_joke` VALUES ('1661', '‍‍小时候有次感冒了去打针，本来胳膊细细的血管不好找，天一冷更不明显了。 护士阿姨找关半天没找到，就搓我手背。 搓了一会，直接搓出好多泥，胳膊瞬间变白了……‍‍');
INSERT INTO `tpshop_joke` VALUES ('1662', '高三的时候，女同桌趴在桌子上睡觉，股沟露出来了，我从拿出了一瓶风油精滴了几滴进去，只见她没多久脸红通通的坐起来，赏了我一个大耳光......');
INSERT INTO `tpshop_joke` VALUES ('1663', '‍‍“老板，我能不能请求您考虑一下，给我增加工资的问题呢？我老婆生孩子了……”我问到。 “是意外怀孕吧？”老板问到。 “是的，老板你怎么知道？” “我就知道你想讹公司保险，告诉你，意外怀孕不在保险范畴。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1664', '我跟我朋友炫耀，我家的狗狗很喜欢我做的饭菜，每次都把我喂它的饭菜吃完，朋友听完谈谈的说了一句：狗连屎都吃，还在乎你做的那玩意？');
INSERT INTO `tpshop_joke` VALUES ('1665', '我：老板，我觉得员工偷吃也并非坏事，这至少证明了我们的食品绿色安全啊。 老板：这tm就是你每天吃我半斤鸡爪的理由？。我精评给占了');
INSERT INTO `tpshop_joke` VALUES ('1666', '‍‍记得小时候妈妈问过我一个问题，“假如有5个苹果，妈妈拿走2个，爸爸拿走3个，你还有几个？” 我叹了声气：“这还是人干的事儿么！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1667', '某人在围脖上发：最近脑子不好使，有点健忘，吃过晚饭，打算遛狗，结果到了路边，发现手里提个狗链子，狗好像忘家里了。 不一会就有人回复：号外，号外，一导盲犬正四处疯狂寻找主人！');
INSERT INTO `tpshop_joke` VALUES ('1668', '‍‍她一本正经对我说：“你现在对我好，是因为你没钱，你只能对我好。要是将来有一天你有钱了，肯定会变的！” 我连连摇头：“你放心，不会有那么一天的。” 她一拍桌子：“什么？你一辈子没钱？你敢！” 你就不能按套路出牌吗？‍‍');
INSERT INTO `tpshop_joke` VALUES ('1669', '‍‍对我老婆。我真的忍无可忍了。 于是,今天去我丈母娘家谈离婚的事，我丈母娘极力反对. 问我：“为什么？” 我说：“为什么？你自己打电话问你女儿。” 丈母娘手机停机了，就用我的打过去。 还没说话呢！电话里传来我老婆的声音：“老公，我今晚在我妈家住，不回家了。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1670', '　　昨天晚上领着老婆孩子去超市买东西。媳妇儿让我抱三岁的女儿，我就跟媳妇儿开玩笑，还是你抱吧，我年轻英俊，等会还得找个大姑娘呢。最后买完东西要回家，女儿死活不走，边哭边说给爸爸买大姑娘，给爸爸买大姑娘……亲妮啊！！');
INSERT INTO `tpshop_joke` VALUES ('1671', '段子不完整？点击本帖标题即可跳转到完全版！\r\n1.一个孩子的5.28日记：上午10:30，爸爸说，儿童节出国旅行计划没了，下午1:30，爸爸说，儿童节礼物也没了，下午3:30，妈妈说，爸爸没了… \r\n2.前几天劝一个想入市的朋友别炒股，昨天股市大跌，我打电话给他邀功：“看吧，我的预测是对的，现在跌了吧？”他说：“你说什么？天台风太大我听不清楚！” \r\n3. “在干嘛呢？”“看楼。”“哟，准备买房了？”“不是。” \r\n4.\r\n5.结婚以后就是大人了，请祝福二位新人：黄大明和angelaadult。\r\n6.我拉着范冰冰的手大吼：“我有什么比不上李晨？”她吓得拿起手袋就猛砸我，楞给我砸晕了，妈的这么小个手袋，里面好像装了石头一样沉！\r\n7.本人快递哥，遇到个坑爹收件人，名字叫“爸爸”。然后打电话：喂，你好，你是136xxxxxxxx的机主吗？回：是的，你哪位？我：我是快递员，有个快递，电话号码能看清，收件人有点看不清，您留的什么名称？回：“爸爸”我：哎_我在你家楼下，下来拿快递吧！完成逆转，就是这么机智。\r\n8.终于意识到了，要找个女朋友的重要性了，至少可以在我拆被套，洗被套，装被套的时候帮我捉住另外两个角，一个人在那抖啊抖啊的，抖的手都酸了。\r\n9.我有个玩跑酷的朋友，非常擅长在楼顶之间跳来跳去像武侠片一样，厉害吧？现在每年清明我都很想他。\r\n今天的谜题是：一个人从飞机上掉下来，为什么没摔死呢？（脑筋急转弯）请关注捧腹微信公众号“捧腹笑话”，然后在该公众号的微信文本框输入“谜语0529”，即可获得答案！');
INSERT INTO `tpshop_joke` VALUES ('1672', '妈妈给六岁的儿子出算术题做。你一共有六个苹果，爸爸拿走两个，妈妈拿走四个，你还剩几个苹果？儿子听后很激动:这是人干的事儿吗？');
INSERT INTO `tpshop_joke` VALUES ('1673', '【午间段子】 一次去见领导办事，提了几瓶好酒，手里捏着刚找的零钱。谁知道一推开领导门，领导和上司吃饭呢，我一愣，接着说了句：X总，这是你要的酒，找的零钱给您放桌子上，您忙，我先回去了。第二天。电话响了：小X啊，过来签合同…');
INSERT INTO `tpshop_joke` VALUES ('1674', '‍‍‍‍老婆老公吃完饭后一起散步，遇到小区门口摆地摊的。 老婆：“唉，这个怎么卖？” 摊主：“30。” 老婆：“能便宜点不，出门走得急，只带了20。” 老公：“唉，我这有啊！” 老婆无语，狠狠瞪了老公一眼，愤怒离去…… 老公窃喜，嘿嘿，又省了20。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1675', '‍‍‍‍我的爸妈借了别人的钱之后都会承诺：“欠你的钱，我们通通都会还给你的。” 然后他们生下了我，取名叫通通。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1676', '儿子问：“惊蛰是什么意思？” 妈妈答：“就是气温回升了，冬眠的动物开始醒过来了。” 儿子继续问：“那我怎么还没看到青蛙和蛇呢？” 妈妈幽默道：“你早上醒来不是也得赖会儿床，伸个懒腰，穿衣服再出来找饭吃啊？”');
INSERT INTO `tpshop_joke` VALUES ('1677', '‍‍‍‍老婆的一班死党闺蜜经常逗我玩。 今天一闺蜜带着她三岁儿子来我家玩，我拍了拍手把他抱起来。 回头就逗闺蜜说：“辛苦你了，咱儿子都这么大了。” 我看见在一旁的老婆头发一下子竖起来，眼睛瞪的比灯泡还要大！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1678', '‍‍‍‍老婆：“老公，你觉得幸福是什么？” 老公：“我觉得幸福就是我下班去超市买个泡泡糖，回家咱一家三口能嚼一晚上。” 老婆：“你太恶心了！” 老公：“不恶心，我先嚼！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1679', '十年前上初一的时候，老师让我们写一篇标题是“十年后的我”的小作文，全班同学各种幻想啊，好像写的都是十年后每个人都是有大钱，有豪车，而且男的都风流倜傥，女的都妖娆动人。可是十年后，一部分人才大学毕业，还在为找工作焦头烂额，一部分人还像无业游民样到处晃荡，还有一部分人为家庭生计在工地干苦力。现在想想那时候的幻想多美啊，不过说来说去也怪老师教的不好，让我们连这么简单的计算题都不会算，我们全部乱幻想也不阻止哈，唉！');
INSERT INTO `tpshop_joke` VALUES ('1680', '想当初我还在读书时候，每次上体育都坐在篮球架下老同学们打篮球…当时班上有个胖妹，她有一奇怪的嗜好，就是把整袋尿不湿放桌子上…有一次上体育课，一个同学就使坏，拿了一包走…当时我还是依旧一副肾虚的样坐在篮球架下看他们打篮球，他就在我后面，拆开了尿不湿的包装袋，把尿不湿丢在我屁股后面，然后大声的说:真想不到你居然还用这个啊？！…真是兴趣独特呵…');
INSERT INTO `tpshop_joke` VALUES ('1681', '　　晚上，我和5岁的小侄儿看电视，电视里正播放知识竞赛。主持人问：“万有引力是谁发现的？” 我随口答道：“牛顿。”小侄子在一边提醒：“小声点，别让他们听到了。”');
INSERT INTO `tpshop_joke` VALUES ('1682', '从前宰羊时放完血，屠夫会在羊的腿上割开一个小口，把嘴凑上去使劲往里吹气，直到羊全身都膨胀起来，用刀轻轻一拉，皮就会自己裂开。这叫吹猪或吹羊。如果谁要说可以把牛皮吹起来，那就是说大话了，因为牛皮很大，而且非常坚韧，根本吹不起来。所以\"吹牛\"就是说大话的代名词！');
INSERT INTO `tpshop_joke` VALUES ('1683', '老婆是语文老师，今天在家她突然问我“老吾老以及人之老”下一句是什么，我脑子一抽，回了句“妻吾妻以及人之妻”，哎，不说了，门外面下雨呢，好冷…');
INSERT INTO `tpshop_joke` VALUES ('1684', '我做为一名冷友我也想逗逗比我工资高数倍的丐帮人员，我换了10块钱一毛的到街上，见一个丢一张，后来我回来的时候他们看到我眼神都不一样了，我想他们心里很感激我吧');
INSERT INTO `tpshop_joke` VALUES ('1685', '今天上信息课上好好的，突然卡住了，顿时十分生气。大拍了一下桌子：网管，换机子。');
INSERT INTO `tpshop_joke` VALUES ('1686', '　　我去图书馆借书，我想借老舍的名著《离婚》。借书部里两个工作人员正在聊天，我说：“同志，我要借本书。”那人冷冷地说：“你要什么？”我说：“我要《离婚》。”两个人异口同声地说：“你要离婚到这干什么？去民政局！”');
INSERT INTO `tpshop_joke` VALUES ('1687', '本人男、24岁，和一女孩互相喜欢很长时间，但是她比我大四岁，所以我们一直不敢过线。前几天单位聚餐，我们喝的都有点多，就摊牌了，现在在一起，身边的人有的支持，有的带着世俗的眼光不理解，她的压力比我还大，但是我们真的很想在一起，我不知道如何说服家里人同意，家里的意思是让我找一个老家近一点，比我小，家里独生子女的家庭，但是不在乎的不是这些，我在乎的是我喜欢的人，不想让自己后悔。可能，和自己喜欢的人对抗整个世界也是一种幸福吧。');
INSERT INTO `tpshop_joke` VALUES ('1688', '一天听广播，有个微信互动的节目，让大家说说这辈子说过最多违心的话，我回复给电台的是：这辈子说过最多违心的话“老婆，我错了！”，结果我得了一份奖品，奖品！');
INSERT INTO `tpshop_joke` VALUES ('1689', '心血来潮想考考老婆的智商，找了个魔术视频，说，去，找出其中的奥妙来！二货老婆一开始还是饶有兴致的，后面开始托腮，最后看到我在旁边偷笑就怒了，咣咣咣跑出去又跑回来，提了两袋洗衣粉一袋奥妙一袋立白，各撒了一把混在一起，说，去，找出其中的奥妙来！老婆我错了。。。');
INSERT INTO `tpshop_joke` VALUES ('1690', '逛街，忽肚子疼，去WC但沒带手纸，就在路边捡了块瓦片打算应付。完事，可旁边有人不好意思擦，等了半小时，那哥们还不走。管他笑不笑话呢，刚拿起瓦片，那哥们说话了：兄弟，可以磕一半给我么？');
INSERT INTO `tpshop_joke` VALUES ('1691', '和朋友吃饭，突然旁边两桌打了起来，我和朋友看热闹忽然发现老板也在看热闹，我问老板你为什么不拦着呢！只见老板严肃的说顾客就是上帝，诸神之战尔等凡人怎敢参与，我竟然无以言对。');
INSERT INTO `tpshop_joke` VALUES ('1692', '‍‍‍‍今天遇到好久不见的大学二货室友，在饭店喝酒叙旧。 “你现在年薪多少？” “300万。” “那一个月也有二三十万哦？” “那是，这是基本工资。” “各种羡慕嫉妒恨啊！你做什么的？” “做梦的。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1693', '‍‍问：“师父和师傅有什么区别？” 答：“师父，前方有妖孽。” “师傅，前方右拐。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1694', '现在的年轻人啊，生活一点都没有规律，就说我对面三楼那个姑娘吧，昨天晚上7点多就洗澡了，今天都快十点了，还在客厅看电视玩手机，还不去洗澡。不说了，望远镜拿的时间有点长了，手酸！');
INSERT INTO `tpshop_joke` VALUES ('1695', '‍‍‍‍公交车上屁股被摸了一下，回头一看一美女羞涩地向我笑，我心里暗喜。 这时，美女旁边一大姐说：“小妹，我说你这乱抺鼻涕的坏习惯，可要改改了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1696', '‍‍‍‍班级有一学霸，成绩很好，就是有点骄傲。 今天他在老师公布月考成绩后，得意洋洋地说：“哈哈，没有我不会做的题目！” 我走过去问：“问你道数学题，今天是零度，明天比今天热一倍，请问明天是多少度？” 学霸顿时就呆住了，哈哈，你傻了吧！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1697', '‍‍‍‍今天美女同事穿身白连衣裙。 我开玩笑道：“呦，好漂亮啊，跟个公主似的。” 美女脸上顿时笑开了花：“真的吗，像白雪公主吗？” 我嘴贱地回到：“不，像太平公主！” 瞬间美女变猛兽，就算我腿快，还是挨了她两巴掌。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1698', '歹徒：快吧一百万打到我的卡上，不然你一辈子都见不得你的孩子！楼主：我的孩子两分钟前已经去世，请不要打击我！歹徒：对不起。。请节哀。楼主：客气了，我洗手去了。');
INSERT INTO `tpshop_joke` VALUES ('1699', '‍‍‍‍一屠夫拿着刀对猪说：“你想选择怎样的死法，你可以百花齐放，畅所欲言。” 猪说：“我……我不想死。” 屠夫：“对不起，你跑题了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1700', '‍‍‍‍两个工人在食堂打饭时碰到一块。 甲：“你喜欢一脸麻子的女人吗？” 乙：“不喜欢。” 甲：“你喜欢胸小的女人吗？” 乙：“不喜欢。” 甲：“你喜欢斜眼塌鼻的女人吗？” 乙：“不喜欢。” 甲听后一把揪住乙衣领，骂到：“那你TM还要勾引我老婆？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1701', '想起了自己的初吻，那时我们读高三，一起回老家，手拉手走在野地里，胆子一大就拉到怀里啃了。一次没吻够，再吻第二次，直到我的鼻涕都挂到女友脸上了才作罢……妈蛋，冬日里就这点不好');
INSERT INTO `tpshop_joke` VALUES ('1702', '‍‍‍‍一日小明在客厅看电视，突然正在浴室里洗澡的妈妈大叫起来。 跑出来对着小明大喊：“小明为什么浴缸里有五条泥鳅？” 小明淡淡的到：“妈妈你数错了，是六条。” 然后，就从房间里传出小明那撕心裂肺的惨叫。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1703', '‍‍‍‍开车去停车场，准备进车位的时候，不小心把旁边一辆车划了一道浅痕。 于是本着敦厚老实的性子，就在人家车上留了张纸条，上面书：“不好意思，把您车划了，这是我电话139xxxxxxxx。” 再去开车的时候，旁边车已经走了。 我的车上多了张纸条.上面写着：“没关系，哥们儿，下次划狠点！公家的车。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1704', '别人都说我丑，今天中午我问电扇我丑吗？ 它摇头摇了一中午');
INSERT INTO `tpshop_joke` VALUES ('1705', '‍‍‍‍一男孩考了90，回到家给妈妈看。 妈妈很骄傲，过了一会儿，妈妈说：“我滴儿，我咋觉着这0是添上去的啊！” 男孩说道：“妈咪，这9才是我添上去的。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1706', '‍‍‍‍两人相遇，说起可怜。 一位说：“我的钱包最可怜，自从跟了我，就没见过钱！” 另一位冷冷地说：“我房间里的镜子更可怜，它就从来没见过人！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1707', '‍‍‍‍小白兔钓鱼，钓了三天都没钓上来！ 后来鱼急了，从水里跳出来对小白兔说：“你在放胡萝卜劳资打死你！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1708', '　　排队去食堂打饭，食堂大妈给我盛青椒炒肉时，却连点肉都不给盛。我忍不住说：“麻烦您能再给我盛些肉行不？”大妈说：“不行，同学，你不能这么自私，就这么点肉，要都给你盛了，你后面的同学还能观赏的到吗？”尼玛，感情这肉不是吃的，居然是看着玩的！！！');
INSERT INTO `tpshop_joke` VALUES ('1709', '有天晚上和闺蜜喝茶晚了，又下着倾盆大雨，我们俩瓜兮兮的招了无数的士都是满的，这时来了个黑色的小车，停在了我们面前，闺蜜很犹豫，因为黑车司机太吓人了，我二话不说拉她上了车，先送她到家，车上只剩我和司机的时候，司机幽幽的来句你不怕我是黑车司机啊？！我只说了一句话：我上车之前看过了，我打得过你！……司机接下来一直沉默直到我下车。哈哈，我是想说我是有多汉子？！');
INSERT INTO `tpshop_joke` VALUES ('1710', '‍‍‍‍一节课上很吵，老师对班长说：“如果你是老师你现在想说什么？” 班长：“同学们，下课！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1711', '我非常瞧不起那些与男人约会蹭吃蹭喝之后，不仅一毛不拔，而且还拒绝与男人开房上床的女性朋友！对于这种很不道德的行为，表示强烈的谴责！有的女性朋友，自以为千娇百媚，对男人具有无限的诱惑力，对于男士的邀请约会是来者不拒，跟着吃，跟着玩。但是，之后，当男士要求开房上床的时候，就开始装了，一本正经地强调俺可是正经淫啊！尼玛的！不上床你出来干什么啊？看流星吗？一个大男人，整天奋不顾身地工作着，起早贪黑，没日没夜，有多么忙你知道吗？人家在百忙之中，邀请你出来吃饭喝酒，难道就真是为了请你吃饭喝酒啊？人家上辈子欠你的啊？酒足饭饱之后，请你开个房，睡个觉，过分吗？饭你吃了，酒你喝了，开房上床的正事你不干了，你说这叫神马玩意啊！还有天理吗？还有王法吗？还自我辩解，说什么：出来只是说说话，散散心，喝喝咖啡！你以为找你出来是开人大会议的啊？我也不是说女性朋友跟男人一起吃个饭，就非得要上床，作为女性朋友你可以拒绝。但是在男人向你发出约会之时，其目的应该是明确的，上床是约会的一项重要内容，作为女人你要是没这个心理和生理上的准备，你就应该一口拒绝，给出最明确的信号！否则就请随身携带安全套从容赴约，为和谐社会做贡献！');
INSERT INTO `tpshop_joke` VALUES ('1712', '今天一女生告诉我,生命科学院院长演讲一场,彻底改变了她。院长痛心疾首地说:我就不明白你们女生为什么喜欢敷面膜。不知道胶原蛋白大分子不能被皮肤吸收也就算了。那厚厚一层玩意,不就是在脸上抹了层培养基么?还一敷敷半小时,皮表各种菌各种虫高兴坏了,等你敷完都四世同堂了。');
INSERT INTO `tpshop_joke` VALUES ('1713', '有人问日语和英语的区别在哪里？神回复：当你被一个男的强吻时，你说一句stop他也许会停下来，但是如果你说亚麻諜，可能就不止强吻这么简单了！');
INSERT INTO `tpshop_joke` VALUES ('1714', '1. 公司的前台美女哭着要辞职，同事们很奇怪问：“做得好好地，干嘛辞职啊？”美女大怒道：“我也不想辞职，但是公司有个王八蛋叫高潮，总是迟到！”经理劝道：“那你也不至于辞职啊！” 美女继续解释道：“他迟到不要紧，问题是每天都有人问我高潮来了没有？我真受不了了”. 。');
INSERT INTO `tpshop_joke` VALUES ('1715', '一教授，演讲时拿出20美元，他问学生谁要这20美元，台下的人纷纷举手。教授将钱扔到地上用脚碾过，又问谁要，台下依旧有人举手。教授说：“我如此对待这张钱币，你们依旧想要，是因为它没有因为我的践踏而贬值，人生亦是如此。人生的价值不在于他人的赞赏或批评，而是取决于我们自身。');
INSERT INTO `tpshop_joke` VALUES ('1716', '同事上大学的女儿带着闺蜜回家过节，他女儿说她们俩好的就是姐妹。。咯咯咯。。同事说，那好呀，我又有一个干女儿了。他闺女嘴一撇：爸爸，别闹了，人家有男朋友了，我才不让你祸祸人家呢。。。');
INSERT INTO `tpshop_joke` VALUES ('1717', '詹姆斯总决赛后问禅师：为什么人们都说我是nba第一人，到最后却还是输给了勇士？禅师微笑不语，脱下裤子露出下体，詹皇若有所思：大师是说我独木难支队友太差所以并没有什么卵用？禅师微笑到：裤里就是屌！詹姆斯气急败坏一刀割了禅师的下体 ，咆哮到：还屌不屌？禅师吐血笑到：一割还是屌！');
INSERT INTO `tpshop_joke` VALUES ('1718', '老婆有点胖，我对她说：别人穿丝袜能够看出美丽，你穿丝袜完全是证明丝袜弹性好！老婆也反驳我说：别人吃伟哥能够金枪不倒，你吃伟哥完全是来证明它的药效不好！');
INSERT INTO `tpshop_joke` VALUES ('1719', '甲:今天和女神聊天，我只说了一句话她就把衣服脱了！乙:真的假的？你说了什么？甲:晚上给她发了句 在么？她回复我说:“我去洗澡了！”');
INSERT INTO `tpshop_joke` VALUES ('1720', '最近很热，下班回家，热的要命，经常撩起裙子直接对着风扇。我家猥琐的老公每次都刻意地走到我身边，把裙子再撩高一点，然后深吸一口气，“咳咳咳，好大的阴风啊！老婆注意空气污染啊！”我就想直接给他一巴掌，这死老公。。。');
INSERT INTO `tpshop_joke` VALUES ('1721', '今天看书才知道，长期喝酒会导致高血压，心脏病，吓坏我了， 回到家赶紧喝了杯酒压压惊。并暗暗下定决心：以后再也不看书了，太特么吓人了！.. ……');
INSERT INTO `tpshop_joke` VALUES ('1722', '前几天我带一千块钱在身上。在路上走总感觉钱掉了。一回去尼玛还有300。由于不相信自己。钱都放袜子里。昨天去买烟。小妹还说我的钱辣眼。。。');
INSERT INTO `tpshop_joke` VALUES ('1723', '‍‍电梯里，我问站在前面的大叔：“大叔吃饭了吗？” 大叔笑着说：“吃了。” “吃了啥呀？” “就啃了两个馒头。” 我笑着点了点头：“大叔要注意营养均衡氨。” 转身又问身边的哥们：“大哥你呢？” 那哥们看了我一眼：“韭菜炒蛋，怎么了？” 我打量了下他：“那刚刚那个屁是你放的吧！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1724', '　　听我叔说，他小时候和隔壁小伙伴吵架，两人在那对骂，我叔骂了句:草泥马！结果那小孩他妈就站门口就回了句:我在这，你来啊～～～');
INSERT INTO `tpshop_joke` VALUES ('1725', '小学时捡过一次铅笔上交被老师表扬，从此走上不归路，经常去找东西上交，还有好几次拿自己的东西交换表扬。最奇特的一次是上课故意晃桌子，把同桌的笔晃掉，抢先一步捡起来，就去讲台上交。同桌也不示弱，死死拖住我，我就一步步把他拖到讲台对老师说:老师，我捡到一支笔！同桌拖着我哭着说:我的……');
INSERT INTO `tpshop_joke` VALUES ('1726', '哎呦我滴妈，你们女人都这么生猛么？割。啥也不说今晚被个女的打了，还没敢还手，也怪我自己手贱，看个美女背影像女票上去就一巴掌拍在屁股上，那女的转过身看完不认识上来就干啊，卧槽，眼镜都打成碎片了。');
INSERT INTO `tpshop_joke` VALUES ('1727', '就在刚刚，店里来了一群人，貌似想买我们的家具，姑娘我赶忙出去招呼。\r\n谁知他们嘀哩呱啦说了一通，我半句也没听懂。原来是一帮韩国人啊，\r\n可是他们没带翻译，况且我也不懂韩文啊，记得我不知所措，\r\n对着他们来了一句：撒浪嘿呦思密达！！！他们哈哈大笑起来！\r\n糗死了，走了之后我才想起来，其实我想说的是阿宁哈塞吆，就是你好的意思，\r\n竟然冒出句我爱你来，没文化真可怕啊，呜呜');
INSERT INTO `tpshop_joke` VALUES ('1728', '王东来，如果你看到这句话时，就说明你犯了很严重的错误，如果你不承认，我们连朋友都做不成，我们是兄弟啊，你怎能趁我不在家就对我做出那样的事来，我很心痛，你TMD吃我的，穿我的，喝我的没啥，但你TMD，拿我充气老婆玩滴蜡是几个意思，烫成那样你让我怎么补。你自己看着办吧。');
INSERT INTO `tpshop_joke` VALUES ('1729', '我一朋友他嘴巴长溃疡了，就喝凉茶(罐装)他把凉茶放阳台上，\r\n飞了一只蜜蜂进去 ，没错 他没看到他喝了，\r\n那蜜蜂刺了他 费了好大劲才把蜜蜂和刺拔出来……\r\n嘴肿了好大 打电话给我的时候声音都不对了 把我笑惨了');
INSERT INTO `tpshop_joke` VALUES ('1730', '经理：“小伙子啊！今天你们将要和世界顶尖球队比赛，希望你们规规矩矩，老老实实的比赛，争取胜利！”\r\n队友：“那个，我有个问题！”\r\n经理：“什么问题？”\r\n队友：“到底是老老实实比赛，还是争取胜利？”');
INSERT INTO `tpshop_joke` VALUES ('1731', '‍‍准备和女友领证。 我：“想清楚了么？” 女友：“想清楚了。” 我：“那以后，吵架可不要随便说分手了啊！” 女友：“知道，要说离婚。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1732', '‍‍‍‍小明：“老师，俺早上没吃饭。” 老师：“为森马？” 小明：“俺要把饭钱省下来。” 老师：“你省钱干啥用？” 小明：“俺怕俺将来和俺粑粑一样，衣裳兜比脸蛋还干净。” 老师：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1733', '　　新技能大家快来学，今天早上正在停车发现一个女神正在锁车门，等她走后默默的把车开过去堵住她的去路，在我的车上留下我的电话，锁车扬长而去，坐等美女给我打电话，哈哈，现在电话号码已记下，微信已加，晚上请她吃饭赔罪，我太TM机智了');
INSERT INTO `tpshop_joke` VALUES ('1734', '小明问禅师：我为什么在道上混了二十多年没有被砍死，禅师：全靠你身上两只老虎纹身啊。小明有所领悟说：难道你说像老虎一样凶猛？禅师：你TMD懂个屁，两只老虎跑的快，跑的快，的快，快......');
INSERT INTO `tpshop_joke` VALUES ('1735', '单位大哥，向我请教结婚购房以及装修的经验，然后我告诉他：“房子要买一楼，窗户不要装有护栏，这样着火了也不会被困；卧室衣柜一定要够大，这样才够你媳妇衣服的容量；床一定要有高度，由其是床底，空间一定要够用，这样就可以多放一些杂物；大哥按照我的建议，买了房，装了修，结果结婚不到半年就离婚了...');
INSERT INTO `tpshop_joke` VALUES ('1736', '‍‍‍‍记得我一同事，曾经说过他的交友条件！ 他说，和他做朋友，最起码的标准是：“得是人！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1737', '妹：哥哥你给我本有画的杂志我要照着画画。 我：这本？ 妹：有没有穿了衣服的？ 我：这本呢？ 妹：不要只穿袜子的！ 我：那这本呢？ 妹：我要单人的！这女屁股的后面还有个男的！ 我：草泥马你怎么这么挑啊！');
INSERT INTO `tpshop_joke` VALUES ('1738', '以前宿舍有个娃，太过老实，而且有时候傻得可爱。有一次晚上熄灯后，大家又在聊天，他就讲：“等老子有钱了，就找三个女生。”　我们口味都被他调起来了，问他说然后呢，只见他镇定的说：“打麻将…”');
INSERT INTO `tpshop_joke` VALUES ('1739', '‍‍‍‍老师：“黑板上的这道题，小明你来回答一下吧！” 小明起身：“老师，我不会。” 老师：“你坐下，那么请学号是16的同学回答一下。” 小明再次起身：“老师，我真不会。” 老师：“你坐下，那么请生理课代表回答一下。” 小明拍案而起：“老师，我还是出去吧！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1740', '同事的女儿在重点高中，学习好，长得漂亮，总有男生骚扰。于是同事就给女儿买了个新手机号，他自己用换下来的旧号，某天收到短信“我是高三某班的***，很喜欢你，咱们聊聊吧！”同事回复“我是她父亲，咱们聊聊吧！”本以为打击到那小子，结果收到回复“叔叔，您是过来人，都理解吧？”');
INSERT INTO `tpshop_joke` VALUES ('1741', '　　话说上初中的时候，那个时候还兴送什么千纸鹤，心啊之类的，初二那年圣诞，隔壁班里一个男生送了我一个风铃，托我同桌转交给我。然后我就给了5块钱给我同桌，说算是我买他的。。然后第二天我同桌告诉我，那个男生说还差5毛，那个风铃要5.5。想想也还是很尴尬，今天初中同学聚会还碰上了。。。');
INSERT INTO `tpshop_joke` VALUES ('1742', '对一个男人来说，最无能为力的事儿就是“在最没有能力的年纪，碰见了，最想照顾一生的姑娘。”');
INSERT INTO `tpshop_joke` VALUES ('1743', '　　今天，一爸爸带孩子买水，爸爸给孩子选了瓶1.5元的农夫山泉，孩子指着4.5元的昆仑山想要，爸爸说，那个是辣的。孩子便不再坚持...');
INSERT INTO `tpshop_joke` VALUES ('1744', '＜终于知道为什么结婚女的都要房子了 ＞ 有一对情侣，男的和女的说我们结婚吧，女的说：你有房吗？ 男的说：为什么你们女的结婚一定要有房呢？ 女的说：结婚不是要洞房吗？既然我出了洞那你为什么不出房？');
INSERT INTO `tpshop_joke` VALUES ('1745', '今天坐公交，有人拍我肩膀，说你手机掉了，捡起来一看，真是我的手机，看见他已下车，急忙趴着车窗冲着他喊，谢谢啊谢谢了！然后自言自语的说，还是好人多啊！这时旁边的一位大姐哼了一声说，他是好人？他真是好人，他从你兜里掏出手机看了看，就扔地下了！');
INSERT INTO `tpshop_joke` VALUES ('1746', '　　某日同学发说说“你硬了，她醉了，然后你俩就睡了”下面神回复“你射了，她怀了，然后你俩就完了”我也是醉了～');
INSERT INTO `tpshop_joke` VALUES ('1747', '今天无意间翻了翻小学和初中的作文本，突然发现，我终于知道我为什么不是富二代，家里没钱了，，，因为，小学到初中，我扶了几十个老爷爷和老奶奶过马路。。。。。');
INSERT INTO `tpshop_joke` VALUES ('1748', '一个人土著人不喜欢穿内裤，另一个人就跟他说穿内裤的好处。干净，暖和。结果土著人就穿上试试，突然就想拉屎，就拉了。完事回头一看啥都没有嘿还真干净，然后一屁股就坐下了。嘿还真暖和。');
INSERT INTO `tpshop_joke` VALUES ('1749', '二货老婆在做红烧鱼，先油炸，一会鱼在跳动，二货老婆尖叫，过一会，鱼又跳动，二货老婆又尖叫，这时高潮来了，她一边用锅铲按住鱼，一边说：一会就不疼了，你忍忍！');
INSERT INTO `tpshop_joke` VALUES ('1750', '　　女生之间关系好：来亲亲，想你了亲爱的，老公，，，，老婆，，， 　　男生之间关系好：来儿子，叫爸爸，乖，滚，傻逼..');
INSERT INTO `tpshop_joke` VALUES ('1751', '老爸让我跟他一起去买东西，我骑电动车，老爸坐后边。买完了，准备回家，我爸说你骑的太快，还是我骑吧。然后我就下来了，我爸骑上车就走了，我还没反应过来，我爸就没影了。我手机也没带，口袋也没钱，一个人傻乎乎的搁那等。。。直到吃晚饭时，我爸才来接我，说是吃晚饭叫我洗碗，找不到我，才想起来。。。');
INSERT INTO `tpshop_joke` VALUES ('1752', '‍‍‍‍今天小侄子问我：“叔叔你知道金的妈妈是谁么？” 我说：“不知道。是什么？” 侄子一脸鄙视的对我说：“金的妈妈不就是土么！” 我瞬间无语了。 侄子又来了一句：“苹果都是金子吊泥巴里面了，所以才叫土豪金！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1753', '‍‍‍‍儿子一岁多，刚刚在淘宝上看小女孩的裙子。 儿子看到了，立马抢过手机对着屏幕亲了几口。 果然是亲生的，跟他妈一样流氓…‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1754', '哥几个喝酒。完了我和女朋友开房。六点钟的时候，有个兄弟打电话叫我开门。我就让女朋友去开了，回来到床上躺着，我和我兄弟哈，都睡着了。女朋友就在哪里看电视。她无聊的时候就过来亲亲我，几次过后，我闭着眼睛。这时候她又过来了，亲了我一下。然后 然后啊！就到里面亲我哥们去了。完了我就看着他。世界安静了 真事。第一次');
INSERT INTO `tpshop_joke` VALUES ('1755', '‍‍‍‍我和男票去逛街，今天521。 看见一男的给一女的跪下说：“我爱你，做我女朋友吧！” 女的说：“神经病，就走了。” 很快男的对另一个女的说了同样的话。 原来是单身狗趁着大好日子找女盆友啊。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1756', '‍‍‍‍两女士在办公室闲聊。 甲女：“想不到30多万的车减震这么差！” 乙女：“不能吧，什么车？” 甲女：“公交车啊！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1757', '‍‍考试过后。 小金：“我考了98分！老爸要请我吃肯德基！” 小张：“我考了99分！老爸要带我去吃西餐！” 小李：“我考了100分！老爸要带我去吃皇家御用餐！” 小明：“我考了0分！我老爸让我吃巴掌！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1758', '2007年我一哥哥学车，哥们儿脾气略爆，教练说话略难听偶带脏字为背景…学着学着教练把我哥们儿骂急了，哥们儿指着教练说：老丫挺得你给我下来，爷今天不抽你我谢字倒着写！教练老头也流弊：我教车四十几年你小子是第二个敢跟我动手的！后来进派出所了，我那哥们他爸来捞他，没错朋友们，教练认出他爸就是当年第一个……');
INSERT INTO `tpshop_joke` VALUES ('1759', '‍‍‍‍一天下课了，小明问小刚问题：“蒸一个馒头，需要几分钟？” 小刚说：“5分钟。” 小明说：“那五个呢？” 小刚自豪的说：“25分钟。” 小明说：“你傻啊，你难道不能一起蒸吗？” 小刚想想说：“小明，你吃一个苹果需要几分钟？” 小明说：“两分钟。” 小刚说：“那五个苹果呢？” 小明笑着说：“当然10分钟哦。” 小刚就拿出了五个大苹果，让小明10分钟吃完。 小明瞬间就傻掉了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1760', '女友临时有事，我独自送女友闺蜜回家。眼看到她家了，她突然扑上来，眼神迷离举止轻浮，在我耳旁轻轻吹气:“我想要你。”我良心过意不去最后婉言拒绝了，回家时，在街角见到女友:“亲爱的，刚刚是测试你，你合格啦！”听完我大松一口气，还好女友够傻找了个丑货来试探我。');
INSERT INTO `tpshop_joke` VALUES ('1761', '有—天，甲乙丙三人想申请吉尼斯世界纪录。甲说:我们把一棵树削尖，申请世界上最大的牙签吧。乙说：不，我们还是挖个坑，装满水，申请世界上最小的湖吧。丙说：你们的都弱暴了，在地上挖三个洞，把手指插进去，申请世界上最大的保龄球好了。');
INSERT INTO `tpshop_joke` VALUES ('1762', '老公出差，好久没有爱爱了，今天晚上老公回来，激情过后，老公说饿了，去吃饭，到饭店后老公问我：“吃什么？”我本着段子的精神说：“能点一个代表咱俩结合的菜吗？”老公菜单一合，大声说：“老板，来一个山药炒木耳！”我：“…………”');
INSERT INTO `tpshop_joke` VALUES ('1763', '　　你真的觉得有钱人跟你想象的一样快乐吗？ 　　你错了，有钱人并没有和你想象的一样快乐，他们的快乐你根本就想象不到。');
INSERT INTO `tpshop_joke` VALUES ('1764', '我们班有个女神叫魏声，我是班长，有一天由于有些人调皮不扫地，我放学大叫了一句:要搞卫生的都留下来，结果全班男生都留下来了。。。');
INSERT INTO `tpshop_joke` VALUES ('1765', '女朋友嚷着让我给她买苹果六，我说：“亲爱的我确实没钱，你非要买就是逼我去卖肾啊！要不……你问问你爸去？”她懂事的点点头，然后给她爸打电话：“爸，你知道哪里可以卖肾吗？”');
INSERT INTO `tpshop_joke` VALUES ('1766', '初中上课比较喜欢睡觉，有次梦到自己是老孙正和金角大王决斗呢，它喊了一声:我叫你一声你敢答应吗？突然，我被同桌戳醒，听到有人叫我名字，我脑袋一热，直接说:爷爷在此！老班一脸黑线，随后就…');
INSERT INTO `tpshop_joke` VALUES ('1767', '　　上天待我不薄，人生在世二十年，我就有了马云一半的成就！我已经有了可以媲美他的容貌，就差和他一样有钱了…');
INSERT INTO `tpshop_joke` VALUES ('1768', '我不知道接下来还会遇见怎样的人，但我能肯定的是，无论对方是怎样的人，他同样也渴望着我优秀，从容，美好。所以我不需要把大把的时间拿来幻想未来应当如何，而应该把所有的等待都用来武装自己。只是为了当有一天遇见你时，能够理直气壮的说，我知道你很好，但是我也不差。');
INSERT INTO `tpshop_joke` VALUES ('1769', '跟着旅游团，住酒店一夜的那种所以衣服啥都没洗，洗澡时内裤顺手装兜里了。第二天忘记了，旁边的妹子问我有纸不？我摸口袋鼓鼓的说有！让后我就在众目睽睽之下从兜里掏出了一条皱巴巴的内裤。。。');
INSERT INTO `tpshop_joke` VALUES ('1770', '　　跟朋友一起吃饭，菜刚上全，来一人，打了个招呼:你也在这啊，就坐一起吃。我以为是朋友遇到熟人就没管，三人聊的不错，待人走后，我对朋友说:你这哥们不错啊。朋友反过来问我:不是你朋友吗？我俩瞬间石化！');
INSERT INTO `tpshop_joke` VALUES ('1771', '‍‍‍‍一哥们晚上逛街。 买花小姑娘拉住他的衣服：“大哥哥，买束玫瑰花送给姐姐吧！不然她会生气的。” 哥们说：“哥哥不怕姐姐生气。可你这玫瑰花有刺，我怕姐姐漏气呀！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1772', '昨天上洗手间遇到了公司的李总管，本来想跟他打声招呼的，结果口快说了一句：李总管，你工作那么忙，还亲自上厕所啊。');
INSERT INTO `tpshop_joke` VALUES ('1773', '‍‍“孙悟空，我叫你一声你敢答应吗？” “俺老孙天不怕地不怕你叫啊！” “老公。” “……”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1774', '　　我老公早起时说宝贝昨晚你吃饱了吗？我说还没起床你说梦话呢。儿子说一句给我们震惊了，儿子说妈妈妈我也要吃夜夜，爸爸那种的。');
INSERT INTO `tpshop_joke` VALUES ('1775', '一次出差在长途大巴上，旁边的妹子靠在我肩膀上睡着了，正在我期待着一场艳遇的时候发现肩膀湿了，妹子睡觉竟然流口水，量还很大，最关键妹子醒来第一件事竟然是在我肩膀上蹭了一下口水，这回不光是口水了，还多了一长条的口红印……妹子能不能再让我更凌乱一点……');
INSERT INTO `tpshop_joke` VALUES ('1776', '领导第一时间赶往灾区，眼见一片一片的废墟和群众的哀哭凄凉，还有群群赶来增援的武警战士，领导伤心的眼泪夺眶而出……，这时秘书凑到领导耳边说：领导，灾区还没到呢，这儿正在拆迁！');
INSERT INTO `tpshop_joke` VALUES ('1777', '情人节晚上，在漆黑的夜路上走着，只见前面一个只有初中年龄的少女，应该是给一个男生打着电话：“在我心里，友情第二，爱情第三。。”尼玛，我竟被震惊的无言以对。 竟然在我面前如此秀恩爱，当场我就暴揍她一顿，然后拾起电话，说了句：等她醒来之后转告他：安全第一。。');
INSERT INTO `tpshop_joke` VALUES ('1778', '‍‍甲：“公交里真暖和。” 乙：“就是，像钻被窝里一样。” 甲：“你被窝里有这么多人呀？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1779', '‍‍今天陪媳妇儿逛街，她看上了一双鞋，问我：“怎么样？” 我拿过来看了一下价格：“这个不好看。” 媳妇儿说：“为什么？我觉得好看啊！” 我：“你不相信我眼光？当年可是我看上你的！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1780', '一天我去医院看病，正看着呢。来俩小朋友，其中一个小朋友说：医生，我把玻璃球咽下去了，怎么办啊？医生说：别着急啊，一会我给你开点泻药，然后就能拉出来了。只听旁边另一个小朋友说：那医生你快点的吧。医生说：哎，你着什么急？你也吃了？这时候这个小朋友说了：那玻璃球是我的，我还等着拿去玩呢。。。。。。。！这谁家熊孩子！');
INSERT INTO `tpshop_joke` VALUES ('1781', '今天跟兄弟一起坐车回家，凌晨五点多车子发生故障，整个车子一片漆黑无法启动。过了一会我问兄弟“冻手不”。尼玛刚一说完，我们哥俩差点让一车人打死！');
INSERT INTO `tpshop_joke` VALUES ('1782', '太无聊了 就打开附近筛选了一下 只显示20左右的女生 只有两个人 第一个签名是：情人节去找情人的男的女的都是傻B！情感状态是：已婚 ……第二个我天刚看昵称就下了一跳：再好的内裤也只能装B………');
INSERT INTO `tpshop_joke` VALUES ('1783', '大爷：年轻时只想着挣钱不注意健康，还没老，身体就垮了，值得吗？ 年轻人：大爷，你以为你年轻时不挣钱，身体就不会垮吗？ 说得有道理耶！');
INSERT INTO `tpshop_joke` VALUES ('1784', '‍‍去吃饭，刚坐下，一美女过来问我：“先生你是一个人么？” 我激动的答道：“就是！” 于是，美女默默地把多余的碗筷收走了。‍‍');
INSERT INTO `tpshop_joke` VALUES ('1785', '　　媳妇问我：有天你当了皇上怎么办啊！？ 　　我想了想，说：就你一个就招架不住了，当了皇上岂不是要死？千千万万个你这样的泼妇！！ 　　媳妇黑脸说：好了，没有如果了，别瞎想了！！');
INSERT INTO `tpshop_joke` VALUES ('1786', '有一天，玉帝对众天神说：“谁来给朕形容一下那孙猴子？”太上老君说：“孙猴子一身黄毛，很是能打，总之一句话，很黄很暴力！！”');
INSERT INTO `tpshop_joke` VALUES ('1787', '妻子不让丈夫在室内吸烟，将他逐出门外。朋友得知后，高兴地拉着他说，不必跟女人一般见识，以后要吸烟,就到我家去。你爱人不反对。朋友说，我老婆说，只有客人来了才可以吸。转。');
INSERT INTO `tpshop_joke` VALUES ('1788', '‍‍电影导演准备拍摄，一个人与老虎在一块嬉戏的镜头，可演员由于害怕拒绝拍摄。 导演劝说：“这只老虎是在动物园里，叼着橡皮奶头喝牛奶长大的。” 演员说：“这能说明什么？我也是在妇产医院里出生，叨着橡皮奶头喝牛奶长大的，可我照样爱吃肉。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1789', '记得读初中时，走在街上，一个大叔骑着他的摩托车从对面走来，看见前面有交警了，立马下车推着走，交警就过去问他。交警：驾照拿出来。 大叔：什么驾照？？ 交警：摩托车驾照。 大叔：你没见我推着吗？有驾照我还不骑啊？ 交警……………');
INSERT INTO `tpshop_joke` VALUES ('1790', '还有比这更霸气侧漏的老妈么！晚上回家看到我妈正跟一位不认识的年轻女孩子聊天，看我回来了，我妈一把把我扯过去说：来，这就是你未来的媳妇，你们认识下！！');
INSERT INTO `tpshop_joke` VALUES ('1791', '京东的刘夫人，万达的王少夫人，周董的周夫人，都是九三年的，我查了下属象，果然，九三年是鸡年！我太TM机智了！！');
INSERT INTO `tpshop_joke` VALUES ('1792', '今天接到一个诈骗电话，说我得罪人了，要卸我一条腿，要是不想见血就拿钱摆平，我听了之后淡淡的说，对不起，我是残疾人，没有腿，，然后那边挂了');
INSERT INTO `tpshop_joke` VALUES ('1793', 'LZ女,马上22了。 晚上做在沙发上看电视，老妈突然问：“圣诞节有人约么？有苹果收没？” 我弱弱的说：“没有。” 老妈立刻一脸鄙视：“咋混的，20多了楞没男生约！” 我只想说：“您是多想把我处理出去啊～不就多吃您两年饭么？”');
INSERT INTO `tpshop_joke` VALUES ('1794', '单位有暖气，电梯里没有。 下班，和同事一起走进电梯，电梯门关上，一同事说：冰箱里好冷啊！ 当时没反应过来，反应过来后大家都笑的直不起腰了！');
INSERT INTO `tpshop_joke` VALUES ('1795', '高血压和糖尿病离婚没几天，高血压又狂热地去追求心脏病姑娘了。\r\n心脏病是个老姑娘，想都没想，就答应了高血压的求婚。\r\n心脏病喜欢高血压人实诚，说犯病就犯玻\r\n高血压和糖尿病离婚，是糖尿病水性杨花，经常瞒着自己偷偷去找胰岛素。\r\n高血压和心脏病郎才女貌，说他们俩是一对金童玉女，一点也不过份！\r\n我期待着他们早日修成正果，生下一男半女。\r\n名字我都帮他们取好了。生男孩叫：高压电。生女孩就叫：高压锅。');
INSERT INTO `tpshop_joke` VALUES ('1796', '一直幻想着有一天，能在试衣服时对卖衣服的妹子说：这太大了，给我换件小号！\r\n现在如愿以偿了！每次试内衣都会说：给我换小个码的！');
INSERT INTO `tpshop_joke` VALUES ('1797', '有一位教授博学多才，据说执教几十年都没有被学生问到过另一位教授前来指教。“如果在野外实习时，学生问的植物你恰巧不认识，你怎么回答呀？”\r\n“这太简单啦。野外实习时，我通常都走在最前面，看到不认识的植物统统踩死。”');
INSERT INTO `tpshop_joke` VALUES ('1798', '今天看见一个朋友在更新心情：自从用了触屏手机后，再也不能闭着眼睛发短信了……');
INSERT INTO `tpshop_joke` VALUES ('1799', '在武侠连续剧里面，行走江湖的人不用工作都会有银子花。\r\n一般说了“要杀便杀，哪来那么多废话”，接着会引来更多的废话。\r\n一般说了“有种就出来，躲躲藏藏算什么英雄好汉”，八成接下来会给偷袭。\r\n正派的快死了还有三分钟的台词寄托遗愿，而反派的快死了台词不到一分钟。');
INSERT INTO `tpshop_joke` VALUES ('1800', '光棍节，请女神出来吃饭。\r\n结账时女神说我今天的眼神很忧郁。\r\n我QNMLGB，你埋1200的单试试。');
INSERT INTO `tpshop_joke` VALUES ('1801', '‍‍炎热的草原上，狐狸遇上了老鹰。 羡慕地说：“你的翅膀真大啊！” 老鹰骄傲地说：“是啊！哪像你，这么丑陋！” 狐狸说：“翅膀大有什么用，关键是看扇动的速度快不快？” 老鹰说：“当然很快，不信就让你见识一下。” 老鹰说完，就急速地扇动起翅膀来…… 狐狸笑着说：“真的很快，我也凉快多了！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1802', '‍‍男朋友昨晚睡觉睡得好好，突然一下把我紧紧的抱着。 呢喃了一句：“还好，你还在。” 瞬间哭的稀里哗啦的。 结果这货来了句，“都说了，这头猪我不卖。” 真有一巴掌拍死他的冲动。‍‍');
INSERT INTO `tpshop_joke` VALUES ('1803', '如果昨天是明天的的话就好了，那么今天就是周五了！Q：问题中的今天是周几？A：周三B：周四C：周五D：周日 上面的问题已经让三个QQ群吵起来了....');
INSERT INTO `tpshop_joke` VALUES ('1804', '去浴室洗澡，管理员大哥硬是要我去男澡堂，瞬间好想哭啊，解释了好久了，连胸罩都给你看了，大哥到底怎样你才相信我是女的啊');
INSERT INTO `tpshop_joke` VALUES ('1805', '我被老公揍了，于是我回家跟老爸诉苦，老爸问道：“他打你哪了！”我捂着左脸说道：“你看看，这脸都打红了！”老爸看后，又给我右脸一个大嘴巴子！然后我一愣，哭着说道：“爸！你打我干嘛！”老爸愤怒的说道：“回去告诉他，他打我女儿，我就打他媳妇！”');
INSERT INTO `tpshop_joke` VALUES ('1806', '我三岁时吧，小舅十岁。他很坏，在我拇指和食指涂502捏起来。想象一下，一个哭着的女孩手还掐个兰花指。。。。。');
INSERT INTO `tpshop_joke` VALUES ('1807', '　　一天晚上跟同学去舞厅蹦迪，发现一个妹子穿的很透，让人流鼻血的那种。 　　我对同学喊道：那女的屁股好性感！好想上她...” 　　说的时候音乐很响，同学没听清楚，示意我再说一遍。于是我扯着嗓子喊：“我说那女屁股好性感，我想上她！” 　　刚张嘴，DJ换碟，音乐停了……');
INSERT INTO `tpshop_joke` VALUES ('1808', '刚去药店买藿香正气水，到了药店一下子竟然忘记要买什么，于是挠挠头自言自语的说：那个…那个…什么来着？结果导购阿姨头也不回的去拿了盒套套给我，，，我…不是的阿姨……');
INSERT INTO `tpshop_joke` VALUES ('1809', '给老公说，我讨厌比我白、比我瘦、比我高的人，二货悠悠来了句:看来你是要与全人类为敌呀！！！不说了，太伤自尊了。');
INSERT INTO `tpshop_joke` VALUES ('1810', '炼金玩多了现在走路都会习惯性地看看后面的人，老感觉后面跟着的人在持续掉血');
INSERT INTO `tpshop_joke` VALUES ('1811', '一在天津的学长说的：“今天打车11.9毛钱他给司机12块钱。”　　司机看他半天没动弹给他打了个小票，　　打完小票他还是没动弹说了句：“还差一毛。。。”　　司机愣了，司机边找一毛钱边说：“我纵横天津二十多年头一次见到一毛也要找的。”');
INSERT INTO `tpshop_joke` VALUES ('1812', '一朋友家有悍妻，决定和老婆摊牌。　　双腿一跪说：“老婆大人，在家里你可以随便打我骂我，　　但是能不能在外面给我留些面子？”　　他老婆很爽快地答应了。几日后，逛街之时其老婆又对他拳脚相加，　　他一边逃，一边求情：“咱不是说好只在家打骂我吗？”　　老婆淡淡的说：“男子汉，四海为家。”');
INSERT INTO `tpshop_joke` VALUES ('1813', '高中时候晚上宿舍熄灯了，可是我不想睡。　　想等老师查完房看小说就没摘眼镜，瞪着眼躺床上等老师快走。　　结果老师一个高亮度大手电筒照过来，　　叫着我的名字说：“XXX你晚上睡觉不摘眼镜是为了做梦看的远？？？”');
INSERT INTO `tpshop_joke` VALUES ('1814', '今天在公司门口，见到一美女拖着一个很大很沉的箱子要装在车上，　　她求助的看向我，当然我要义不容辞的过去帮忙。　　弯腰，沉胯，双臂腰部一起用力。然后腰带开了。　　我都忘却了是怎么离开现场的了！！！！');
INSERT INTO `tpshop_joke` VALUES ('1815', '昨天去逛吉之岛，后面有一家三口，夫妻两很悠闲的逛，　　小朋友突然很激动的抱着饼干说：这个好吃，我要这个。　　妈妈边唱边说：“我看不见，看不见。”　　爸爸一边扭头看，一边说：“我也看不见，也看不见。”　　留下小朋友在风中凌乱……');
INSERT INTO `tpshop_joke` VALUES ('1816', '昨天朋友的儿子玩出了新花样，午睡时把他爸妈的头发系了个死扣。　　然后等他们醒，结果总不醒，他7岁的儿子在卧室里放了一个小鞭炮，　　混合双打呀，跟我讲述的时候还气的不行。');
INSERT INTO `tpshop_joke` VALUES ('1817', '朋友去河里钓鱼，中午去的晚上回来的！我问钓到多少？　　他做了一个八的手势，我说八斤？　　他摇摇头，我又问那是八条？　　他也摇摇头！　　我说那是多少？　　他突然对我吼到就钓到这么长一条。');
INSERT INTO `tpshop_joke` VALUES ('1818', '高中同学上课的时候在下面玩手机，　　由于动作太明显，被老师发现了，老师走过来“把你手机给我”，　　同学抬头看了老师一眼，淡定的说道“138********”　　老师一脸黑线。。。');
INSERT INTO `tpshop_joke` VALUES ('1819', '深夜，高速一服务区，一男乘客下车后挥刀对多名群众乱砍，连伤4人。　　民警向其喷射整瓶“辣椒水”，周围的人都闻到了刺鼻气味感觉不适。　　但持刀者却毫无反应，还调侃：“你喷啊，你喷啊，我是四川人！”');
INSERT INTO `tpshop_joke` VALUES ('1820', '关于游戏打到一半妹子来了的问题，　　正确的处理方法难道不是说：“等我打完这场战场/这个boss就陪你，　　你先拿我网银逛会儿淘宝吧？”。。。');
INSERT INTO `tpshop_joke` VALUES ('1821', '　　家有8岁萝莉一枚，，就刚才我逗她说你是小狗，谁知道这货追着我家养的狗直叫爸爸爸爸，狗跑到哪追到哪喊，，自做孽不可活啊，，，闺女你回来，，我错了。。。。');
INSERT INTO `tpshop_joke` VALUES ('1822', '我单身汉一枚。周日，爸妈一起到我租的房子来看我，我买菜回来，宿舍已经被两个老人整理过了。妈妈看了看我的衣柜，语重心长的对我说：一定要注意补充营养啊！我相信他们应该已经见过我的女朋友了……');
INSERT INTO `tpshop_joke` VALUES ('1823', '一哥们跟我说和老婆每次吵完架，她老婆都会躲在柜子里。一次刚和老婆吵完架，又在柜子里找到了她，哥们说你就不能换个地方躲。他老婆说，我换个地方躲你找不到怎么办！哥们说他当时特感动，我只想说这哥们太天真了，你再仔细找找，柜子里肯定有个老王在里面。');
INSERT INTO `tpshop_joke` VALUES ('1824', '大过年的回家过年去。刚进家门就遇我表姐家那熊孩子。只听见他大吼一声：红包拿来，不然我就丢擦炮了。 瞬间一万只黑蜘蛛爬过。 赶紧给了红包，不然马上甩炮又来了。');
INSERT INTO `tpshop_joke` VALUES ('1825', '我:晚上可以约你出来吃饭么？ 女神:晚上我有事，没空，额，怎么你换号码了？ 我:不是，是我朋友的电话，我手机按坏了 女神:还可以按坏？ 我:我看到你发的段子，听说长按三秒可以点三个赞，我太激动，就把屏幕按破了 女神:那你过来接我吧！');
INSERT INTO `tpshop_joke` VALUES ('1826', '近日在老外眼中，八成以上中国人不能上班，大量居民酗酒赌博，剩下的部分人只能看电视度日。中国人只能一家人聚在一起，彻夜不眠地熬到第二天的日出。还有，学校全部停课，工业全面停滞，百分之八十店铺关门，股市全部停顿。无数年轻人更是为了那几毛钱几分钱而丧失理性！抗议的人群竞相走上街头，四处投掷各式各样的爆炸物，空气中弥漫着浓浓的硝烟味儿。。。更有一群低龄儿童以装萌傻笑收敛大量钱财，不劳而获，让人嫉妒眼红，太糟糕了。');
INSERT INTO `tpshop_joke` VALUES ('1827', '坐普通火车，上车后看见一老太太坐在了我的位置上，本着尊老爱幼的精神，我没出声让老太太起来，默默的站在她座位旁边。过了一会站累了，就靠在了她的座位上，可能是妨碍到她了，她转头就大声喝到:你这样靠着让我很不舒服知道吗？没钱就别坐火车，还买站票！我直接叫来列车员把她叫了起来！');
INSERT INTO `tpshop_joke` VALUES ('1828', '　　新年了，我也发一个。前天走亲戚，在我三叔家睡觉，我和我三叔还有堂弟睡一个床，晚上我三叔的呼噜声真是惊天动地，第二天早上却起的很早。于是我弟调侃道：你看你叔呼噜声好大啊，把自己都吵醒了……');
INSERT INTO `tpshop_joke` VALUES ('1829', '小时候是短发，特别羡慕别人的长发，我就把黑色的秋裤套在头上走出家门，别人问我我头上是什么。我还特别甩了甩自己的秋裤，骄傲的说：这是我的头发！现在想想也是醉了');
INSERT INTO `tpshop_joke` VALUES ('1830', '大过年的，身上难免有几个红包不是。和朋友在逛街，尿来了，去公共厕所小便。心血来潮开始拆红包，一会儿就拆完了。就是这个时候高潮来了：拆完了肯定只留下钱，红包丢了不是。你大爷的，我一顺手把钱丢厕所里了，红包在手上。。。。。。。尼玛，一堆翔啊，你们说我是该捡还是该捡喃。 算了，捡吧。 那酸爽啊。你们是体会不到了。');
INSERT INTO `tpshop_joke` VALUES ('1831', '‍‍骚年：“你为什么要拒绝我？到底是为什么？” 妹纸：“我太丑了，觉得自己配不上你！” 骚年：“你以为我会是那样的人嘛？” 妹纸：“哎！关键是你比我更丑……”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1832', '老婆:老公，给我削个苹果去，洗干净了给我吃。 老公：我不去。 老婆：你好不听话老娘的话。 老公：老子他妈又不是声控的。 老婆：啪一巴掌，你他妈是个触屏的。 老公：.....');
INSERT INTO `tpshop_joke` VALUES ('1833', '前几天看有段子说，古时青楼女子样样精通。还知书达理。现在的小姐怎样怎样的，其实我就是想说一句话。知道古时去一次青楼要多少钱吗？你知道古时一文可以买几个包子吗？参考（1两黄金=10两白银=10贯铜钱=10000文铜钱）！有点跑题，但是我的意思是，别人做那么好，因为她贵啊。不过古时青楼女现在都改名了。我们不用专门跑过去看花魁了。因为我们的时代，这群人叫“明星”。');
INSERT INTO `tpshop_joke` VALUES ('1834', '‍‍小时候问我爸：“什么是父债子还？” 爸爸说：“就是父亲欠下的债，由儿子来还。” 我：“那你有欠债吗？” 爸爸：“比如买房时我贷了100多万。” 我：“爸爸我们不适合，我们还是做朋友吧！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1835', '西门庆与奴婢欣雯尽享鱼水之欢，只见他花样百出，忽焉在上，忽焉在后，一会荡起秋千，一会儿又火车便当。。。玩得不亦乐乎，金莲见了，心疼他身体，劝他悠着点，西门大官人摆手一笑: “不妨事，欣雯自由日，想怎么日，就怎么日～”');
INSERT INTO `tpshop_joke` VALUES ('1836', '一位女子因为老爸欠钱还不上，无奈只好嫁给了债主。新婚第一天晚上，女子对得意洋洋的新郎说：我嫁给你，是因为我老爸欠你的钱，你别太得意!第二天凌晨，女子睁开眼睛，摇醒熟睡的新郎，说：我爸到底欠咱们多少钱?可不能就这么算了。');
INSERT INTO `tpshop_joke` VALUES ('1837', '某旅游团安排不周，使一对陌生男女同住一室，当夜无话。早晨，女人对窗梳妆，怪风将她的丝巾吹到树上，眼看就要出发，男人卖力爬到树上取丝巾送女人，不料女人一记重重耳光扇到脸上，骂道：“笨蛋，树这么高爬上去了，床那么低你爬不上来。”');
INSERT INTO `tpshop_joke` VALUES ('1838', '老艺术家说：现在的文艺青年，四成是穷，三成是GAY。 记者问：那不是还有三成吗？ 老艺术家：你以为那三成和谁GAY？');
INSERT INTO `tpshop_joke` VALUES ('1839', '爸爸妈妈带着阿呆去加利福尼亚的海滩度假。海滩上的老外们都一丝不挂的裸泳。阿呆：爸爸，你的鸡鸡怎么没有那些叔叔的大？爸爸：......因为......那些叔叔比爸爸有钱。 过了一会儿。阿呆想喝可乐，爸爸一人去上店里买。阿呆和妈妈留在海滩，可是爸爸回来后发现妈妈不见了。 爸爸：你妈妈呢？ 阿呆：爸爸，你刚走后，来了一个很有钱的叔叔，他看着我妈妈，钱是越来越多越来越多，后来我妈妈就跟他走了。');
INSERT INTO `tpshop_joke` VALUES ('1840', '在部队里某一个班上，只见得其中有一个班兵一直在发抖，班长看到了就大声地问说：「xxx你在干什麽？」那个班兵忍不住了只好说：「报告班长，我要上一号」 只见那班长听了後，不慌不忙地大声叫道：「那边那个一号过来，他要上你。」');
INSERT INTO `tpshop_joke` VALUES ('1841', '　　前几天朋友给我介绍对象，那女的问我你有房吗！那时我脑子一糊涂就说了你想要吗现在可以和你一齐去开！那女气走了，我朋友就更我说别那么焦急啊！');
INSERT INTO `tpshop_joke` VALUES ('1842', '你是否有四个模样？一个是在朋友面前疯癫的样子，一个是在恋人面前完美的样子，一个是只身一人时脆弱的样子，还有一个，是在陌生的人群中安安静静的样子。');
INSERT INTO `tpshop_joke` VALUES ('1843', '上小学时，班上的男生个个躲着老师爬老式篮球架。我们个子矮，爬到分叉处就爬不上去了，但双腿仍拼命地缠着篮球架往上挣扎，那种感觉莫名奇妙地舒服。后来，大个子能爬上去也来学我们；再后来，偶而也有女生去爬；多年以后，拥妻入眠时仍怀念那种感觉……');
INSERT INTO `tpshop_joke` VALUES ('1844', '　　表弟养了条狗把狗叫自己儿子～哥哥哥有天和表弟在家楼下公园散步牵着他儿子不远处的长凳上坐了一个美女表弟儿子不知道怎么了跑去在美女边上抬腿就尿上了美女看见了骂了句草你大爷的往哪尿～我走到美女面前非常严肃的问：美女说话算数吗？？美女：怎么了？我红着脸说我就是它大爷！');
INSERT INTO `tpshop_joke` VALUES ('1845', '能表白就别再错过了. 男：在我高三毕业的时候，我喜欢很久的女生突然莫名其妙的亲了我一口，正当我欣喜若狂准备跪下表白时候她却亲了饭桌上的所有人。 女：高三毕业的时候，为了亲我喜欢的男生，就假装喝醉了，然后亲了饭桌上所有人，只为了亲他一口。');
INSERT INTO `tpshop_joke` VALUES ('1846', '一大早出去吃早点，要了份豆浆油条。吃到一半，看到一男的吃完后浑身上下翻了个遍也没找到一毛钱。于是他走向老板，脸红红的跟老板搭讪:老板，你看见过马惊吗？老板一边忙着炸油条一边一脸疑惑的说:没有！这小子又说了:你见过牛惊吗？老板又答了:没有！这小子又嘻嘻一笑问:那你更没见过人惊吧？老板:嗯！这小子一听乐了:那好今天我就给你表演一下人惊！说完撒腿就跑！');
INSERT INTO `tpshop_joke` VALUES ('1847', '每次打仗前，张飞都要把家中的笔找出来仔仔细细数上一遍，家仆不解，问其何故，“丞相说啦，知几只笔百战百胜！”张飞解释道。');
INSERT INTO `tpshop_joke` VALUES ('1848', '　　今天，我那逗比同学又上课看小说被发现，老师这次没有啰嗦，直接让她把所有小说交出来，她倒也听话，一下十几本的拿。于是下课后我问她“这么多小说你也舍得啊”“反正都看过了，拿走就拿走了呗”我去，我只想对你说：土豪我们做朋友吧！');
INSERT INTO `tpshop_joke` VALUES ('1849', '昨天回家路上，下的地铁楼梯，有个男青年背了个女青年，旁边一个5,6岁的正太跟在旁边一起下楼。开始我以为女青年腿不好，结果楼梯走完了，那个女青年就下来自己走了，估计就是撒娇。然后正太就说，“粑粑，你也背我一会儿吧。”他爸说，“你自己走，我只背我的宝贝儿。”“就是我。”他妈补充说道……');
INSERT INTO `tpshop_joke` VALUES ('1850', '500年前我头戴金箍罩身穿虎皮袄脚踏七彩祥云看见妖怪大喊一声妖怪看俺老孙收了你。500年后我头戴塑料袋儿身穿破皮袄看见人类大喊一声收电视机。洗衣机旧空调。旧电脑。旧手机。');
INSERT INTO `tpshop_joke` VALUES ('1851', '一美女发了张自拍，下面一群人赞赏，有说漂亮的、有说性感的，唯独一货说道：“不好意思，我去上趟厕所。”我感觉这才是最好的夸奖。。。');
INSERT INTO `tpshop_joke` VALUES ('1852', '室友约了一个女孩过夜，临出门我提醒他：“要采取安全措施啊，你要没有我借你。 ” “不用不用，我自己有。”说完他打开抽屉，翻出一把刀带着出门了。');
INSERT INTO `tpshop_joke` VALUES ('1853', '男：我喜欢你，做我女朋友吧！女：不要，你目的不纯！男：木有的事，我就是单纯的喜欢上你！');
INSERT INTO `tpshop_joke` VALUES ('1854', '老婆觉得我在外面不检点，回家我很认真的想说一句：你真的是多疑了！结果口误，说成了：你真的是多余了！你们先点赞，我老丈人带人来了。。。');
INSERT INTO `tpshop_joke` VALUES ('1855', '老婆失踪后，老公第一时间到警局报案，Pol.ice对老公说：请你先冷静冷静！你这样一直笑没办法录笔录。。。');
INSERT INTO `tpshop_joke` VALUES ('1856', '老婆：老公，你说奇怪不奇怪，我竟然可以用眼睛吃饭！我：说的新鲜，用眼怎么吃？老婆：看你一眼就饱了！我。。。');
INSERT INTO `tpshop_joke` VALUES ('1857', '早上起床想喝杯蜂蜜水，老公说：这蜜蜂采了蜜就回巢，那岂不是吃喝拉撒睡都在巢里了。。。我去，还能不能好好的喝个蜂蜜水了。。。');
INSERT INTO `tpshop_joke` VALUES ('1858', '经过我刻苦认真的学习，终于拿上了驾照，问教练，我适合买什么车时，教练抽了口烟对我说：买什么车我没建议，但是真心建议你保险买全保！');
INSERT INTO `tpshop_joke` VALUES ('1859', '今天俩男同事吵架，眼看要动手了。。。突然那个瘦弱的说：“谁怕谁呀，大不了我进医院，你进派出所。”顿时我们就憋不住笑了。。。');
INSERT INTO `tpshop_joke` VALUES ('1860', '老刘朋友聚会的时候，吹牛说敢当着媳妇和丈母娘的面抱小姨子，帮小姨子洗澡。。。老赵幽幽的说：吹啥吹，谁不知道你丈母娘前几天刚生了二胎！');
INSERT INTO `tpshop_joke` VALUES ('1861', '我有一个苹果分你一半这是友情，我只吃一口，剩下的全给你，这是爱情，我一口没吃，直接给了你，那是你父母。我把苹果藏起来了，对别人说我也饿了，这就是社会。');
INSERT INTO `tpshop_joke` VALUES ('1862', '瘦子蹲，瘦子蹲，瘦子蹲完胖子蹲，胖子～蹲，胖子～蹲，胖子蹲完～哎妈呀，扶我一把，起不来了！');
INSERT INTO `tpshop_joke` VALUES ('1863', '‍‍‍‍宝宝家煮包子，宝宝总是把包子叫做包包，爸爸告诉宝宝要叫包子。 晚上，爸爸被蚊子咬了一口，宝宝说：“看包子。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1864', '‍‍学校晚上十点熄灯。 奇葩舍友在晚上十点，还差一分钟的时候大声喊道：“大家好！我是2号男嘉宾，我叫xxx（宿舍另一人），喜欢我的请留灯！” 然后女生宿舍的灯嘭嘭嘭都灭了。‍‍');
INSERT INTO `tpshop_joke` VALUES ('1865', '初次带男友回家，吃饭时爸爸拿起酒瓶给男友倒酒，男友说：叔叔我不会喝。爸爸说：少喝点没事！男友说：我真不会喝酒。爸爸说：别见外，就像在自己家一样，现在年轻人哪有不会喝酒的。这下男友着急的说：我真不会，王八犊子才会喝酒呢！');
INSERT INTO `tpshop_joke` VALUES ('1866', '那天，我跟同事在饭店吃饭，旁边桌一汉子过来问他借个火，我那同事抬头一看不认识，赶紧把桌上的打火机往兜里一揣，一脸严肃道：“我的打火机气儿不多了。。。”');
INSERT INTO `tpshop_joke` VALUES ('1867', '顾客满怀期待地问售货员：“这个体重秤真的可以满100减20吗？”售货员愣了愣：“额，小姐，说的是这台秤的价格不是秤的功能。。。”');
INSERT INTO `tpshop_joke` VALUES ('1868', '‍‍“哥们，你这车咋地了？前擦后碰的。” “啥也别说了，我有个老婆和车库。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1869', '疯人院新任院长走到一个病人面前，问他为什么进了疯人院？病人问答：＂医生，是这样的。我娶了一个已有成年女儿的寡妇，而我的父亲则娶了她的女儿为妻。所以我太太成了她公公的岳母，她女儿成了我的继女和继母。继母生了个儿子，这个孩子成了我的弟弟和我太太的外孙。我也有了一个儿子，这个儿子成了他祖父的内弟和他自己叔父的叔父。另一方面，我父亲提到他孙子的时候，说是他的内弟，我的儿子叫他的姐姐做祖母。我现在认为我是我母亲的父亲，我外孙的哥哥，我太太是他女婿的儿媳，他外孙的姐姐。现在我不知道我是自己的祖父、我弟弟的父亲，还是我儿子的侄子，因为我的儿子是我父亲的内弟。院长，这就是我来这里的原因。我觉得在这里比在家里清净些。”');
INSERT INTO `tpshop_joke` VALUES ('1870', '一位年迈但仍然精力旺盛的高尔夫爱好者前去找巫师，询问天堂上是否有高尔夫球场，巫师说要查一下，第二天给他答复。　　次日，老人又来了。巫师说：“我得到的既有好消息又有坏消息。”老人说：“先告诉我好消息。”“天堂上有很宽阔的高尔夫球场”巫师说。“球场上铺着碧绿的草坪，并备有最好的器械。”老人接着问：“现在告诉我坏消息吧。”巫师说：“下星期日上午十点就该你发球了！”');
INSERT INTO `tpshop_joke` VALUES ('1871', '“喂，是警察吗？” “你好，请问有什么需要帮助您的吗？” “我有四个朋友被杀了，他们还要杀我。” “您好，请不要慌张，告诉我您在什么地方，我们立刻派出警力支援。” “我在魔兽DoTA天梯一区，河道北边野区附近那个有一个大龙两个小龙的草丛里。”');
INSERT INTO `tpshop_joke` VALUES ('1872', '‍‍‍‍晚上坐公交车稀稀拉拉几个人，上车坐下，把买的梨放在脚边，结果忘了系袋子。 到站师傅一个急刹车，你没猜错，梨全部滚了，我从后面追到梨追到了前面。 刚想捡，师傅一启动，是的，梨又齐刷刷的往后面滚了。 我又只有追回来，摇摇晃晃的，站都站不稳，我还要追梨满车跑，也是醉了！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1873', '今天和老婆吵架，吵到激烈的时候，我突然觉得我一个大男人为什么要和一个女人一般见识呢？何况还是自己的老婆！当时我就跟老婆道了歉，老婆挺高兴的。道完歉，她哥哥把菜刀放下了，她弟弟把铁锹也放下了，她妹妹拽着我头发的手也松开了，妹夫手里的擀面杖也扔地下了，老丈人也把砖头丢开了，一家人于是乎又愉快地坐在一起，聊着天，喝着茶，吹着牛，有说有笑… 多好??');
INSERT INTO `tpshop_joke` VALUES ('1874', '‍‍‍‍我们大学有一个湖，湖里有小船和鸭子，记得学校明文禁止去划船和抓鸭子，违反的就直接处分。 其实主要还是为了大家安全，但是有一次几天我们发现食堂多了一道新菜：“孜然鸭片。” 从那以后就再也没见过湖边的鸭子。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1875', '昨天晚上睡觉，梦到在和女友啪啪啪，生平第一次那么长时间，感觉不是一般的爽。早上醒来发现自己躺在宿舍，而舍友已经买好了早餐。 舍友一脸娇羞，“特意为你准备的鸡蛋，吃几个吧。” 我特么有点蒙，让我缓缓！');
INSERT INTO `tpshop_joke` VALUES ('1876', '‍‍‍‍跟同事一起去公司食堂吃中午饭。 吃完饭一起回办公室，走到人事部所在的楼时候，一个年轻人拍了拍同事的肩膀。 问道：“请问这是人事部吗？” 二货同事可能是被拍的疼了，略生气的说：“不是，这是我肩膀！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1877', '‍‍‍‍春天是养花的好季节，看别人买花养，心也怦然动。 于是我昨天也去买了一株含羞草，满以为高养活。 却没想到用手去碰它也没见它有啥反应，于是就问卖花老板。 没想到，人家老板竟解释说：“这株含羞草有点特别，它是吊死鬼卖XX—死不要脸的。” 尼玛，我还就没话说了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1878', '1.朋友告诉我，她瘦到80斤是因为每天只吃苹果黄瓜等蔬菜水果。于是，立志要减肥的我，决定效仿，然后我每天就只吃苹果派、樱桃蛋糕、炸香蕉以及黄瓜味的薯片等等，相信我也会从130斤瘦到80斤。\r\n2.“老公，我胖么？”“你不胖，你标准体重”“那老公，我跟你说个事”“啥？”“我饿了，你抱我去冰箱那取点儿吃的呗”“抱你？？你可拉到吧……你等着，我把冰箱给你抱过来 \"\r\n3.什么是心有灵犀？就是你正想着他时，他就打了电话过来跟你说：“我们分手吧。” \r\n4.一位胖妞来到整容医院，向大夫哭诉：“他们都笑话我太胖，不能反手摸到肚脐……”大夫说：“所以你想我们帮你减肥吗？”胖妞摇摇头说：“不，我想你帮我在后背安个肚脐眼。” \r\n5.读书时一直暗恋她，但没勇气表白，更没胆量碰她。毕业后，她已经结婚了，老公跟我是同学。那天下午，她抱着娃娃在喂奶，我找了个很好的借口。我走到她面前，一把两手按住她的咪咪，然后跟她娃娃说，喊叔叔，不喊不给吃!\r\n6.早上坐公交，我旁边一个美女在吃韭菜馅的包子，弄的整个车里都是韭菜味，我受不了了，就对她说：“能不能下车再吃啊？”那个女的竟然说：“你管的着吗？又不是你家的车，我做什么和你有什么关系啊！有钱自己去包专车啊！没钱臭毛病还不少！”气死我了，我听了以后默默的把鞋脱了，那女的吃着吃着就吐了......\r\n7.我记得前几年单身还被说是贵族的，怎么近几年就特么变成狗了。\r\n8.一个男生想向喜欢的女神表白，他先借了个扩音喇叭，然后在女神宿舍楼下用蜡烛摆成心形。 这时，看热闹的人越来越多，男生觉得时机到了，他激动的拿出扩音喇叭想说话，喇叭发出响亮的声音：“收购旧彩电，旧冰箱，旧电脑，旧洗衣机，旧热水器...”\r\n9.告诉大家一个脸部美容的秘方，番茄蜂蜜美白：将番茄捣成番茄汁加入适量纯净水搅至糊状均匀涂于脸部像洗面奶一样的手法进行轻轻的按揉待约十五分钟洗去，每周1—2次。坚持数月你会惊奇的发现你的丑是没法被改变的。\r\n今天的谜题是：一人真用心(打一字)请关注捧腹微信公众号“捧腹笑话”，然后在该公众号的微信文本框输入“谜语0611”，即可获得答案！');
INSERT INTO `tpshop_joke` VALUES ('1879', '昨天急性肠胃炎,半夜肚子疼上厕所,刚坐马桶上胃里一阵翻腾,转身爬马桶上吐,结果吐了一马桶拉了一地,当时难受的没感觉到。等吐完艰难的再次坐在马桶上才发现拉地上了,被屎一熏又吐了。这时卫生间的门被老公打开了,他看了我一眼又看了地上一眼,扑过来惊恐的叫到,你怎么了。这屎是你吐的?');
INSERT INTO `tpshop_joke` VALUES ('1880', '‍‍‍‍和同事坐公交车，人多，他有座，我站他旁边聊天。 期间上来一位老人，我问他：“哎，你也不让个座？” 他冷冷地说：“不让！没意思！” 我说他：“没素质，不知道尊老爱幼！” 他轻蔑的冲我笑了笑，这才说：“如果这个老人这会昏倒在这个车上了，你去敢扶吗？” 我：“……” 瞬间仿佛一记狠狠的耳光打在了脸上，火辣辣的疼，让我闭嘴。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1881', '越野车看见大卡车倒在路边，便上去询问，大卡车说：“今天碰到一个可爱的小跑车。 。。”“撞上了？”“不，被她萌翻了。 ”');
INSERT INTO `tpshop_joke` VALUES ('1882', '‍‍‍‍去姐姐吃晚饭，推开门，发现姐姐和姐夫正在吵架。 小外甥冷冷地道：“一切不以离婚为目的的吵架都是秀恩爱！” 他们立马停止了吵架，都惊异地看着那五岁的娃。 我说大外甥啊，这是谁教你的啊？‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1883', '‍‍‍‍小孙子去奶奶那里告状。 小孙子：“奶奶，妈妈我自杀！” 奶奶：“别胡说！” 小孙子：“真的，昨天我听到妈妈在床上，对粑粑说，你弄死老娘吧……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1884', '，婚姻就像“跑男”本质是合作。 若队友是李晨，你就轻松些，心里踏实。若是郑恺则无论结局如何，过程中不缺情趣。若是祖蓝他也会绞尽脑汁，拼劲全力。但如果是遇见了节目里的陈赫那你还是算了吧，');
INSERT INTO `tpshop_joke` VALUES ('1885', '‍‍‍‍“老板，这对耳环怎么卖？” “2元一对，3元两对。” “包邮吗？” “不包，同城快递都要6元呢。” “那我就给6元，你给我寄一对过来吧！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1886', '一哥们上班赶公交车就在路口买了个烤红薯放在屁股口袋里，上车后一屁股就坐在座椅上，听见噗的一声。哥们赶紧起身，望着黄黄的屁股口袋对全车人说这是红薯，怕车上的人不信就拿手抹了一下放进嘴里，结果全车人都吐了。司机边吐边说小伙子这是刚才一婴儿拉的便便，你的红薯掉地上了！');
INSERT INTO `tpshop_joke` VALUES ('1887', '‍‍‍‍今天和老公逛街，看见一个年轻辣妈带着个萌萌的小女孩。 于是我对老公说：“快看，好可爱，我也想要一个。”（撒娇状） 老公立马，一脸坚定的对我说：“嗯！你去把小女孩拐走，我去把她妈妈拐走。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1888', '　　那天我去便餐部吃饭，点了一份面条，一边吃一边看店里电视演的电影《泰窘》，吃完了电影没演完，我就坐在那看，老板见我吃完了不走，就每过五分钟看我一眼。终于演完了，我起身要走，老板说：“再坐会儿，还有字幕没演完呢！”');
INSERT INTO `tpshop_joke` VALUES ('1889', '　　有一天，小星在考试，有一道题不会，问旁边的也不会，这时，同桌指了指自己的罗圈腿，小星瞬间明白了原来选C啊，谢谢同桌。');
INSERT INTO `tpshop_joke` VALUES ('1890', '‍‍‍‍‍‍今天早上4岁女儿又想赖床，不肯上学！ 被我半催半骂的叫去了刷牙，她哭哭啼啼就拿着牙刷杯子站在那死活不刷。 我气不过打了她几下！ 出门后，我问她：“知道妈妈为什么打了你吗！明天还记得吗？！” 她说：“记得。” 我：“记得什么？” 她哭着说：“记得起床起来给妈妈打” 我：“……”‍‍‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1891', '感冒，去药店买药，回来的路上遇到同事从超市回来，然后打招呼，并且拿出刚买的给我水果，我脑感冒，去药店买药，回来的路上遇到同事从超市回来，然后打招呼，并且拿出刚买的给我水果，我脑抽的拿出刚买的药问人家要不要吃，，，， T_TT_TT_T');
INSERT INTO `tpshop_joke` VALUES ('1892', '、一天几个兄弟在一起喝酒神侃。其中一个说：“我兄弟见到美女就想吐！”\r\n另外几个有人就问“你兄弟谁啊？还不明白说出来？是我们几个中的哪一个？”\r\n就见这哥们：“嘿嘿嘿、、、、、不已” 瞬间，我秒懂了这其中奥秘。');
INSERT INTO `tpshop_joke` VALUES ('1893', '一个馋嘴的人在酒席上将盘子里的四只麻雀，一口气吃了三只。将最后一只让给朋友吃，朋友说：“还是你吃了吧，我不忍心拆散它们！”');
INSERT INTO `tpshop_joke` VALUES ('1894', '晚上，儿子对我说：“妈妈，我想吃一只苹果。”我说：“太晚了，苹果也睡了。”儿子说：“小的也许睡了，大的一定没睡！”');
INSERT INTO `tpshop_joke` VALUES ('1895', '我97上大学那会儿 网络游戏有一个中国象棋 我那是人机对战 高.潮来了：我第一步走了上帅 电脑想了半个多小时破解我的招 而且每回都是这样子 我每次都乐的对着电脑傻笑 我想知道有多少人干过这事');
INSERT INTO `tpshop_joke` VALUES ('1896', '老婆：什么是吊丝？\r\n老公：吊丝就是没钱，没车，没房，还TM张的丑的人！\r\n老婆：真TM罗嗦，你直接说是你不就得了！');
INSERT INTO `tpshop_joke` VALUES ('1897', '我一同学，在去厕所的时候被一美女撞飞，美女娇羞地说了句对不起，我那同学一眼没看，淡定地量了量飞出去的距离，说他能算出那女生的体重。');
INSERT INTO `tpshop_joke` VALUES ('1898', '上课时，我的铅笔找不到了，对身旁的女同桌说：“借你个东西用下呗”，结果这二货来一句，除了我的处吻初夜不能拿，其余自便。。我滴个天呐~ &#160;真不亏是个人才啊！！');
INSERT INTO `tpshop_joke` VALUES ('1899', '一个顾客到饭店吃饭点了一只南京板鸭，服务员端上来后，他舔了一下鸭子的嘴，说：“这只是浙江的。”服务员又端上来一只，他舔了一下鸭子的嘴，说：“这只是无锡的。”这时，饭店老板激动地跑来：“你能舔了一下我的嘴，我从小就被家人抛弃了，我想知道我是哪里的人！”');
INSERT INTO `tpshop_joke` VALUES ('1900', '今天上街买菜碰到表嫂，正在犹豫在叫表嫂好还是叫嫂子&#160;\r\n表嫂：“哎呦，。，妹妹，买菜呢？”&#160;\r\n我：“哎呀，婊子你也买菜呢？”');
INSERT INTO `tpshop_joke` VALUES ('1901', '小明骑车路过一个桥上，看到一个陌生人要跳河小明立即上前游说：姑娘为什么想不开，要跳河?姑娘：我就是想死啊，反正世上都没有人喜欢我没有说完，女孩就要跳，小明一把抓住了姑娘然后，吻了姑娘说道：我喜欢你！接着又说道：为什么长得这么好看，还要去死呢？女孩：因为我爸妈不喜欢我总穿女孩的衣服！');
INSERT INTO `tpshop_joke` VALUES ('1902', '一群恐怖分子冲进一个国际会议的会场，将参加会议的一百多名政客扣为人质。随后，恐怖分子向政府提出了他们的条件，并宣称如果不答应，他们将会每过一个小时释放一名政客。');
INSERT INTO `tpshop_joke` VALUES ('1903', '极左的时代，到处都在批判今不如昔论。批判会一直开到某生产大队，会场上无人发言，农民们以种田为本，不想理论问题。 大队长只好带头，琢磨半晌，终于想出批判今不如昔的理论，他 说：大家伙想想，金子，多少钱一厅?锡，多少钱一斤?说金不如锡不是混帐话吗? 大家点头称是。');
INSERT INTO `tpshop_joke` VALUES ('1904', '前东欧某国公安部门在审查一个申请移居国外的公民时问：你为甚么要移居西方呢？你在这里有稳定的工作，稳定的收入，有免费劳保，你还有什么可埋怨的呢？　　我没有什么可埋怨的。　　那你为什么要移居西方？？　　因为在那里我可以自由埋怨。');
INSERT INTO `tpshop_joke` VALUES ('1905', '浙江某市一领导在会议上外地客人介绍经验，说其市经济得到迅速的发展，是一靠警察，二靠妓女。众人不解，领导继续阐释：警察，就是改革开放的警察，妓女，就是百年不遇的妓女。众人更加愕然，后经看书面资料，才知道是一靠政策，二靠机遇。');
INSERT INTO `tpshop_joke` VALUES ('1906', '某考选部长王先生，数年前谣传将接任陈履安先生为某监察院长。　　王先生戏称，只有猴子元年才有可能（盖猴子当皇帝时，才有猴子元年，猴子不可能。故他也将没机会）。　　然而近日上层关爱的眼神眷顾，报派王先生为某监察院长之迹象甚大。　　难道，真有猴子元年。　　我可要去查查李先生的生肖了…。　　还是某报故意…。');
INSERT INTO `tpshop_joke` VALUES ('1907', '克格勃的一位干部去拜访自己的好友，那个好友是一位老师。见到他时，好友正在唉声叹气。于是干部问：什么是让你心烦？　　好友有气无力的说：唉，这个问题连你的部门也解决不了。今天我问学生彼德若夫：‘《叶普盖尼。奥涅金》是谁写的？’他居然说不是他写的。后来我又问了季里连科和艾若娃，他们也说不是他们写的。这怎么办？　　两天后，干部给老师打了电话，说：你放心，他们三个都招了。他们说《叶普盖尼。奥涅金》是他们写的。');
INSERT INTO `tpshop_joke` VALUES ('1908', '克林顿访英期间，与撒切尔夫人,杰弗里.豪等共进晚餐。为活跃气氛，撒切尔夫人问杰弗里.豪：你的父母有个孩子，既不是你的兄弟，也不是你的姐妹，他( 她 )是谁？豪大笑着答道：就是我，豪呀。克林顿感到很有趣，回到白宫后，问克里斯托弗到：你的父母有个孩子，既不是你的兄弟，也不是你的姐妹，是谁？克里斯托弗回答不出。克林顿得意地大笑到：是豪呀。');
INSERT INTO `tpshop_joke` VALUES ('1909', '苏联看德国因盛产啤酒而每年赚进大笔外汇，决定仿效，开始派人研究制造啤酒的技术。第一批啤酒制造出来后，苏联送了一些样品给德国鉴定品质。一个月后，德国回函给苏联：恭喜，贵国的马很健康！');
INSERT INTO `tpshop_joke` VALUES ('1910', '甲歌手在模仿某歌星时，总有几句唱不像，心里很苦闷。于是，请教乙歌手。乙歌手倒也爽气，实话实说：上台演唱中，估计下一句有困难时，不必惊慌，只要大喝一声：‘来，大家一起唱！’接着把话筒面向观众，这不，问题就解决了吗？甲歌手听后，茅塞顿开，欣喜万分。');
INSERT INTO `tpshop_joke` VALUES ('1911', '某人谈恋爱被女友甩了，便对朋友说：“难怪人家说天下最毒女人心，我现在完全认为女人是毒药，今后要离得远远的！”&nbsp; 可是过了不久，这人又谈起了恋爱，而且谈得很起劲。于是他的朋友们问他道：“你不是说女人是毒药吗？怎么又谈起来了？”这人回答说：“我也不知怎么搞的，自从上次失恋以后，我老是想服毒自杀！');
INSERT INTO `tpshop_joke` VALUES ('1912', '1、热恋的男女有十分钟不微信聊了，说明什么？神回复：领导在旁边。2、我：天才都是逼出来的！神回复：我是剖腹产来的！3、什么叫成熟？神回复：我麻麻没逼我，我就穿上了秋裤！4、在淘宝上买了几件衣服瘦了。我问妹妹要不要。妹妹神回复：要要要，省的我去买孕妇装了...卧槽！5、如果你跟一妹纸喝咖啡的时候，妹纸用腿碰了有几下，你会咋样？神回复：踩住！');
INSERT INTO `tpshop_joke` VALUES ('1913', '1、打针技术差的护士MM没事就找人练习扎针，一次，遇见一位熟睡病人，一针又一针，   不知不觉从头打到脚…就在MM香汗淋漓想再找位置打针时，病人一跃而起，大 声呼骂：   你特么真当我睡死了?从头打到脚!”MM落荒而逃。第二天上班，院长握着MM手说：“ 医院唯一的睡了8年的植物人被你打针给治好了!      　　2、青年问禅师：“王菲、李亚鹏、周迅、张柏芝、谢霆锋都单身了，明星们个体很耀眼，为何在一起就不行?”禅师拿出一碟芝麻、一碟花生、一碟瓜子、一碟核桃、一碟杏仁让青年分别品尝，问他：“好吃吗?”青年点了点头。禅师又拿出一块五仁月饼给青年品尝，青年吐完恍然大悟。      　　3、江湖险恶，这天，奔雷手易向天带着弟子逃亡，眼看就要被追上，徒弟问：“师傅，我看我们是逃不掉了，你告诉我追我们的是什么人啊?”易向天见逃不过，也放弃了逃亡：“万皖德和随从田尚莱。”徒弟一愣：“就…就像那万紫千红一片海?”听罢，四个人一起跳起了广场舞。      　　4、暗恋班上一练散打的帅哥，怎么暗示都不明白。某天鼓起勇气，把“xx月xx日晚xx时，操场第三棵树下，不见不散。”的字条夹在他课本里。羞涩的我没署名。那天，我很飘逸的等在树下。那帅哥来了，身后还跟一群人影。月光皎洁，那帅哥看到树下的人影，大喝：“是你下的挑战书?!      　　5、书记喝大了，搂住身边的女子说：“要啥名牌包随便挑。”手摸向女子大腿，女子推开，书记：“不要包，给你车行吧，什么牌子随便挑。”手又开始滑动，女子一脸厌恶，书记：“挺倔啊，想当干部吧，说吧，哪个部门”。女：“爸，别把你的工作习惯带家里来行吗?”      　　6、我喜欢陈小春的。里面一句“忘记分开后的第几天起，喜欢一个人看下雨”，就很像我现在的处境。尤其是像今天这样的下雨天，我就喜欢站在窗户旁听着这首歌，看着窗外打伞的人群，合着节拍，大声的唱：“呦，打伞的子孙呦”。      　　7、一外商到一个山村考察，村主任指示文书说：考察完后要给县广播站写篇稿，规格要高。   于是文书在考察结束后就这样写道：“……村党政首脑、会计、文书和其他高级官员，都参加了会见，宾主进行了亲切友好的谈话，同时就双边关系交换了意见，最后，双方还就当前国际形式发表了看法……”      　　8、“我才毕业一年，我的所有朋友见面都装作不认识我，这个社会是怎么了，难道就没有友谊了吗，难道曾经愉快的玩耍都已经忘了吗”“你他妈一年胖了五十斤谁他妈能认识你”      　　9、我在加油站旁边转悠几个小时，加油站的工作人员实在忍不住了，一起几个人走过来问我：“先生，你在这里半天了，有什么事吗?”我不好意思的说：“对不起，我在戒烟。”      　　10、我们宿舍在讨论班上的美女，我说：要是班主任把xx调到我旁边，就好了。万一能日久生情呢!你说是吧。室友回了一句：日久生不生情我不知道，但我知道日久一定腰疼。不说了，带室友去医院先。不要问我为什么!      　　11、一朋友是地铁司机，下了班去相亲，见了面就闲聊起来，女孩问，怎么来的，坐地铁还是开车?哥们觉得这是在试探他经济条件，也是爽快人，认真的说，我没车，我开地铁来的。      　　12、小时候冬天特别能流鼻涕，有一天奶奶说：“告诉你，你流的鼻涕就是你的脑子，流光了你就成了傻子了。”我竟然相信了，一流鼻涕就拼命捂住。');
INSERT INTO `tpshop_joke` VALUES ('1914', '年底了，本人统计了一下，今年共饮酒266场，出勤率100%，其中喝醉有3场，头晕7场，吐了36场，灌醉放倒155人，啤酒喝了926瓶，洋酒135瓶，红酒110瓶，白酒193瓶，扎壶136扎，，同比去年增张47%，983次摇骰子秒杀对方，反被杀51次，掉骰子15次！ 令无数人闻风丧胆！不管是百威，金星，还是雪花勇闯天涯，倒满必干！但现在我为何退出江湖，归隐深山？一瓶啤酒就倒，二瓶啤酒就断片！是什么让我改变如此之大！？是仇恨？还是爱情？ 拿起你的手机，编辑“晚上我要请你吃饭”发送到本人手机，与昔日酒神面对面交流，倾听我背后的故事，感受我曾经的辉煌，揭开酒神背后的心酸。');
INSERT INTO `tpshop_joke` VALUES ('1915', '小时候父母只疼哥哥，对我不冷不热，慢慢我对自己身世产生了怀疑。  无意翻箱倒柜，看见了户口本，翻开的刹那，果然只有哥哥的名字，没我的！哭得天旋地暗，撕心裂肺！  无力地爬起来，手持户口本，找到一瓶农药，准备离开这个没有爱的世界！  千钧一发之际，干完农活回来的老娘，从后面给我一闷掌。  第二天，老爹到处求关系办二胎户口，那年头饭都没得吃，居然有人主动拼命交二胎罚款，村里火速送来一面锦旗！');
INSERT INTO `tpshop_joke` VALUES ('1916', '一美国老太找到一张200年前手写存单，老祖宗在瑞士银行存了100美元。  老太去取钱，即报总行，总行查到该存款底账。  总行找到老太，举行仪式，给老太兑现50万并奖励100万美元。  行长说：“钱存在我们银行，只要地球在，你的钱就在。”  如果在中国，银行会说：对不起，我们有规定，得您祖宗本人来领……');
INSERT INTO `tpshop_joke` VALUES ('1917', '去小超市买水，给了收银员10块让他给我拿瓶绿茶。  收银员给我找了7块还给我一个瓶盖，说：“没有绿茶了，你拿这个再来一瓶的盖子可以去对面商店换一瓶。');
INSERT INTO `tpshop_joke` VALUES ('1918', '古时候的打劫：“此山是我开，此树是我栽，要想从此过，留下买路财。”这语言是多么的粗鲁。  经过上千年的文明洗礼，到了当今社会，语言变得多么文明贴心：  “前方500米收费站，请减速慢行...”');
INSERT INTO `tpshop_joke` VALUES ('1919', '(o )( o) 完美的奶(o)(o) 一般人的奶(,人,) 太久没戴胸罩而变型的奶( + )( + ) 假奶(* )( *) 挺奶( o)(o.) 有颗痣的奶@@ 大头奶 oo A罩杯级奶( O )( O ) D罩杯级奶( @ 人 @ ) 一切都大的奶(. 人 .) 健美小姐的奶(o 人 o) 木瓜型(Q)(O) 穿孔奶(p)(p) 大乳 晕的奶( - )( - ) 贴在玻璃上的奶(#人#) 有马赛克的奶');
INSERT INTO `tpshop_joke` VALUES ('1920', '今天上班，领导对我说：“世界那么大，你就不想去看看？”心里一惊！我擦，这是炒鱿鱼的新词么？我稳稳的回答：领导，您就是我的世界！领导笑着说“你哪里都不去么？”我坚决表态“领导放心，公司是我家，我哪儿都不去”。领导说“可惜，这次组织大家到马尔代夫玩，既然你这样说，那么你就留在公司值班吧！”');
INSERT INTO `tpshop_joke` VALUES ('1921', '　　想当年一个妹纸半夜跑到网吧来找我，问她睡不着吗？不想睡，上网吗？不想玩！然后我竟然就脱下我衣服给她披着在网吧睡了一夜……不说了……我想静静……');
INSERT INTO `tpshop_joke` VALUES ('1922', '老婆比我小十天，今天我两抬东西，我这头抬起来，她那头抬不动…我：同样吃了二十多年饭，你丢不丢人。 老婆：你比我多吃十天饭呢，有本事你十天别吃饭再试试！！尼玛我竟无言以对');
INSERT INTO `tpshop_joke` VALUES ('1923', '养儿子和养女儿的区别：养个儿子……跟玩游戏差不多，建个帐号，起个名字，然后开始升级，不停的砸钱……砸钱……砸钱……一年升一级，等以后等级起来了，装备也神了，却被一个叫儿媳妇的盗号了！养个女儿……就象种一盆稀世名花，小心翼翼，百般呵护，晴天怕晒，雨天怕淋，夏畏酷暑冬畏严寒，操碎了心，盼酸了眼，好不容易一朝花开，惊艳四座，却被一个叫女婿的瘪犊子连盆端走了！');
INSERT INTO `tpshop_joke` VALUES ('1924', '　　朋友的老妈，住六楼出门忘带钥匙，灶上炖的菜，沒办法打119，消防员来了后从楼顶用绳顺到凉台进去把门打开了，大妈感激不尽，一直说谢，送出门外，高潮来了一阵风吹来门随即又被关上。消防员再次爬上楼顶……打开门后， 　　消防员不停的叮嘱：千万别送了，别送了……');
INSERT INTO `tpshop_joke` VALUES ('1925', '姐夫是初中语文老师。一次跟姐夫去钓鱼。。钓的都是还不到一两的小鲤鱼。姐夫把鱼扔河里了说“太小了。。回家叫家长。”叫家长。。。听着也醉了。');
INSERT INTO `tpshop_joke` VALUES ('1926', 'lz女，闺蜜半夜三更当街和他老公吵架，我老公拉开他老公，我去劝闺蜜，谁知那丫头不理我，开始往前跑，我163，她148小萝莉一枚，追不上啊，我喊她别跑了，你想要在小镇出名吗？你再跑前面就是坟墓地，你想要我跑步追你减肥吗？闺蜜还是不停下，跑着她就拐进一个弄，接着是狗叫声，再接着就是两个我们女人尖叫声。。。。。狗开始追我们，然后我一边骂狗，一边骂她半夜发疯瞎跑跑什么！然后第二天，镇上都在议论昨晚有两个女的瞎跑跑被狗追的事');
INSERT INTO `tpshop_joke` VALUES ('1927', '老张的律师对他说：“有好消息也有坏消息。”老张说：“先说说好消息吧。”律师说：“好消息你太太找到了一张价值20万的照片。”老张问：“那么坏消息呢？”律师说：“那是一张和你的女秘书在一起的照片。”');
INSERT INTO `tpshop_joke` VALUES ('1928', '同事给介绍个男的，第一次约会，走在路上看到卖糖炒栗子的，我买了十块钱的，男的赶紧说没零钱了，我不知声，他说真的，我从兜里掏出十块给阿姨了…gc是当我站在烤冷面的摊位前，这位仁兄掏出7张一百的摆到我面前说了一句，你看我真的没零钱了不骗你的，我能说我满脸的黑线还有周围人的眼神吗…三十了还没结婚的你，活该你单身啊');
INSERT INTO `tpshop_joke` VALUES ('1929', '　　半夜迷迷糊糊醒来，发现自己抓着一只手。捏了捏，没有感觉，卧了个槽，明明是一个人睡的！!!瞬间就清醒了!! 原来是左手被压麻了，右手正抓着左手......');
INSERT INTO `tpshop_joke` VALUES ('1930', '老公又开始埋怨我太胖了，吧啦吧啦没完没了的说我，我一个生气打了他一巴掌，他也火了，说我太过分了，我说：“谁过分了，是你先说我胖的。。” 他说：“是你先胖我才说的！”');
INSERT INTO `tpshop_joke` VALUES ('1931', '有一对正在相亲的男女。女：你有房么？男的说，有啊，我家房子可大了。女：有多大啊？男：我家是复式，厕所就有10平米，一层一个，餐厅有50多平米，卧室就有好几十间，浴室20多平米，车库就更大了停了上百辆车呢。。。。。。女：哇太棒了，你到底住哪啊？ 男：呵呵，我住单位宿舍。');
INSERT INTO `tpshop_joke` VALUES ('1932', '本人家开超市，今天送货时突发奇想，问一哥们“我送货时看见货主家里有一刚洗过澡穿着长袍睡衣的男人，有一穿着长袍的女人，又发现有一穿着内裤的男人”问他们什么关系？哥们回答：穿着睡衣的男女是夫妻，穿着裤衩的是他们大儿子，他是不是很纯洁');
INSERT INTO `tpshop_joke` VALUES ('1933', '给女同事吹了一下，眼睛进虫子了，好巧不巧被女朋友看见了，瞬间变脸。哭闹说我背着她乱来，说我早就动歪脑筋了，这些莫须有罪名都算了，最可气是她说是我指使小虫子飞同事眼睛里的。。。。。。指使。。。。。。飞。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('1934', '老师:明明你为什么穿那么厚啊！ 教室里有暖气啊？？？？ 小明:你特么什么时候让劳资在教室呆过啊！>____<');
INSERT INTO `tpshop_joke` VALUES ('1935', '有一次和朋友刚喝了点小酒回家，没喝醉，我们迎面走来一对父女。男的喝的醉醺醺的，他女儿挺漂亮身材好长得好，那男的从我旁边过去的时候突然抓住我的手说小兄弟我女儿就送给你了，本屌各种高兴。然后。。。。他女儿给她爸头上一拳。。。那男的竟然晕了！！！然后女生边说对不起边把男的扛肩膀上走了。。扛走了。。');
INSERT INTO `tpshop_joke` VALUES ('1936', '一自习课上，教室里一片寂静，突然莹莹放了个屁，声音很大，她尴尬的脸都烫了，这时他的爱慕者刘刚站起来道:不好意思，刚才那个屁是我放的，仗义的为莹莹解了围，莹莹羞涩的对刘刚微笑了一下，可是刚消停没几分钟莹莹又没忍住，放了个声音很大的屁，只见莹莹的另一个爱慕者于文广噌一下站起身说，对不起，这是我放的，再一次为她解了围。也不知道莹莹中午是吃了什么东西了，肚子咕噜咕噜个不停，终于没忍住，又放了个屁，正在尴尬时，班里的钱大壮突然站起来，指着莹莹说:“以后，她放的屁，我包了！！！”');
INSERT INTO `tpshop_joke` VALUES ('1937', '如果你喜欢的人醉倒在你床上,你会选怎么做： A守护在她身边；B帮她盖好被子；C轻轻的吻她一下；D把她... 我选的是D, 我以为我的答案会是最龌龊的了,没想到下面有人回复：CDDDDBA……');
INSERT INTO `tpshop_joke` VALUES ('1938', '一丑女看完超人电影，影院出来后感慨“我要是能嫁给超人就好了”我看了她一眼说“你一定能如愿””真的吗？”她惊喜的问。”“是的，你老公在决定娶你的时候，一定要有超人的勇气”');
INSERT INTO `tpshop_joke` VALUES ('1939', '昨晚路过一酒吧门口，一个妹子突然拉住我，问我要服务吗，我说不要，妹子又说很便宜的，我还是不要，妹子最后说我活很好的，为了摆脱她，我跟她说我有女朋友了，然后她气愤的走了，嘴里还说着：现在的女孩真随便，搞得老娘生意都不好做了！我了个擦，你这是什么意思啊，妹子？？？');
INSERT INTO `tpshop_joke` VALUES ('1940', '班任为王，导员为皇，讲师为妃，课代表为相，支书为将，班级为城，学霸为太子，院领导垂帘听政。” 我琢磨了半天，那班长是什么？班长必须是一位能同时接触到王、皇、妃、相、将、太子、城，还要应付垂帘听政的人物。 经过潜心研究，最后终于得出了结论：班长是太监');
INSERT INTO `tpshop_joke` VALUES ('1941', '前天晚上10点30分，突然一个来电：“是王刚吗？”我说不是。 11点36分，又来电：“王刚睡了？”我说你打错了啊！ 12点21分，电话又响：“王刚在干嘛呢？”我迷迷糊糊就火了：“我TM不叫王刚！滚！” 凌晨四点醒了怎么也睡不着，想起那个电话，于是拨了过去。对面迷迷糊糊说：“谁呀？什么事？” 我冷冷说：“没什么事，就是想问你找到王刚没？”');
INSERT INTO `tpshop_joke` VALUES ('1942', '‍‍中午上食堂抢饭，饿得不行了大喊一声：“大妈，我的饭快点。” 大妈朝里面喊了一声：“饭快一点，要饭的着急了。” 全食堂的眼光都聚集过来了。‍‍');
INSERT INTO `tpshop_joke` VALUES ('1943', '小时候有一次放学，下雨天我尿急，打算在学校上个厕所再回家，结果一个女生跑过来说没伞，想跟我一起撑伞回家。我害羞，然后没去上厕所直接送她，一路憋着尿。刚好路过一条小溪，实在是憋不住了，于是在尿出来瞬间，假装滑倒掉倒水里……从小就这么机智啊！');
INSERT INTO `tpshop_joke` VALUES ('1944', '‍‍‍‍平时上班，早餐经常到公司旁边的一个小吃店，那里的素包子不贵还很好吃。 前几天去吃包子，发现包子涨价了。 就问老板：“老板，你这包子怎么涨价了？” 老板回答说：“因为肉涨价了！” 我没明白过来，接着问道：“你这是素包子，肉涨不涨价跟它应该没关系吧！” 老板一脸不屑地说：“表面看是的。可是做包子的大师傅他要吃肉啊！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1945', '　　朋友圈一美女感叹： 　　“早知道那么痛，当初就不下凡了……” 　　一二货回复： 　　“怎么了？脸先着地了？”');
INSERT INTO `tpshop_joke` VALUES ('1946', '　　昨天我妈说到新年了找个风水大师，大师到了我的房间说：“不对，这屋有死人。”我说：“胡说，哪有？”大师指着墙角的垃圾桶对我笑，我急忙拉住他：“千万别告诉我妈。”');
INSERT INTO `tpshop_joke` VALUES ('1947', '和女同事一起出差，我开着她的车。看到后座有个水桶，于是问她：这水桶是干嘛用的？她红着脸说：有时候在高速上内急，就靠边直接解决了……于是，我默默地开过了两个服务区。');
INSERT INTO `tpshop_joke` VALUES ('1948', '‍‍今天早上凌晨3点去偷菜。 见前女友的苹果成熟了，毫不客气地偷光。 第二天发现前女友留言：“如果你以前象现在晚上这么精神，我们也不会分手了。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('1949', '‍‍‍‍一次我和我老婆去许愿池许愿。 我就问她：“老婆，你刚才许的什么愿啊？” 我以为他会许我俩白头到老永不分离一类的愿望。 谁知道她说：“我许的是你这辈子只有我一个人喜欢你，再也没有别的女人喜欢你的愿望！” 我听完也是醉了……‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1950', '爱情是什么，不适合就是穷，没感觉就是丑，一见钟情就是好看，深思熟虑就是有钱。');
INSERT INTO `tpshop_joke` VALUES ('1951', '去小店买西瓜，顺手买了个冰棒，这时旁边一个小女孩一直盯着我手里的冰棒。 我就笑着问：小妹妹你想吃冰棒吗？ 她：想。 我：那你叫句好听的，就给你。 她：老公。 我勒个去。。。');
INSERT INTO `tpshop_joke` VALUES ('1952', '　　晚饭后爸妈在看电视剧，我无聊在一旁弹吉他，老爸不耐烦的说，别弹了行不？让我们好好看会电视。我马上反驳到，当年北漂卖唱的时候听一首歌二十块钱呢。老爸慢慢的从钱包里拿出50块放在桌子上，幽幽的说，我给你50，买个清净，你今晚别弹了。。。老爸，你真是我亲爸！');
INSERT INTO `tpshop_joke` VALUES ('1953', '妻子要外出培训一个月，为了让丈夫和儿子能吃上正常的热饭热菜，她就叫丈夫每天带儿子回娘家蹭饭。培训结束后，她来到娘家，看见妹妹睡在沙发上，想吓唬吓唬她，便蹑手蹑脚过去突然将手放到妹妹两腿之间。妹妹眼睛也没睁开，用手拨开了妻子的手，说：“姐夫，别闹啦，听妈妈说我姐姐就回来了。”');
INSERT INTO `tpshop_joke` VALUES ('1954', '　　听说你看上个美女，泡到了吗？别提 了，哥们天天上她家店去，结果让人骂出来了：你个死秃头，天天到我家理发店干什么？');
INSERT INTO `tpshop_joke` VALUES ('1955', '农村一小孩儿结巴，一天他家养的猪掉粪坑里去了，急忙跑回去跟他爹说， 结果一急说不出话来了，他爹说“你不会唱啊”你的吉祥三宝歌不是挺好的吗 孩子就开始了 “阿爸” “哎” “咱家的猪掉到粪坑里啦” “我操” 俺妈为了救猪也掉里啦？ “啊” “你再不去他俩就完啦” “没事” “我在给你换个新妈” 咱们又是幸福的一家[呲牙] 我去? ?');
INSERT INTO `tpshop_joke` VALUES ('1956', '一日本人在接受记者采访，记者：“如果你母亲和你老婆同时掉进水里，你先救哪一个？”日本人：“我先救我母亲，老婆没有了我可以跟我母亲再生一个”观众被深深的感动了，掌声久久不息…');
INSERT INTO `tpshop_joke` VALUES ('1957', '网络上，你不知道屏幕的背后我是人还是狗。一旦你关注我，你会轻易的发现我是一只单身狗。\r\n —— 薛定谔的狗');
INSERT INTO `tpshop_joke` VALUES ('1958', '经过一个配钥匙的小摊，听见一MM问老板：“老板，可以配钥匙么 ？”老板答：“可以啊。”然后，MM呆站在摊前，老板望着她，长时间的沉默…沉默 ……最后老板忍不住了说：“钥匙呢？”MM：“钥匙？我有钥匙干嘛还来找你配？”');
INSERT INTO `tpshop_joke` VALUES ('1959', '三种妹子对于洗澡的态度： x感妹子：又可以秀身材了 普通妹子：好烦啊，又要洗澡了 女汉子：哈哈哈，又可以站着尿尿了');
INSERT INTO `tpshop_joke` VALUES ('1960', '老公，我想吃西瓜，等着，我去给你买……老公家里没雪糕了，行，我去买……老公，我想吃果冻了，行，我还去买，老婆下次想要啥，一次说清楚……老公，我想吃葡萄了，我艹尼玛的！来来来起来，别睡了，起来！走，我给你送超市去睡！');
INSERT INTO `tpshop_joke` VALUES ('1961', '我去朋友家，看见他媳妇在给孩子喂奶，出于礼貌上前摸摸孩子的脸，说：宝宝好乖啊，白白胖胖的，真漂亮！他媳妇红着脸低声说，孩子睡着了，脸在那边呢。。。啊？！哎。。。');
INSERT INTO `tpshop_joke` VALUES ('1962', '段友们，一句话证明你看过西游记____________________');
INSERT INTO `tpshop_joke` VALUES ('1963', '在推特看到的一个真事。 某男去医院对医生说“我肛门入口那里疼。”医生冰冷的回答“…那是出口。” 某男“……”');
INSERT INTO `tpshop_joke` VALUES ('1964', '　　上初中二年级时候，好像是期末数学考试，最后一题特难，草稿运算验算了好久，确定答案正确的。正在这时下课铃声响了，那个急，这一急就坏事了，精关失守，还伴随着爽爽的感觉，裤子里粘糊糊的，那个糗呀！只听说过急尿的，我TMD急射了。。。');
INSERT INTO `tpshop_joke` VALUES ('1965', '老师:今天怎么迟到了小明:因为今天去吃早餐了老师:吃早餐吃一个小时？小明:小二叫我慢用老师:滚出去第二天老师:今天怎么又迟到小明:因为今天去吃早餐了老师:又说了慢用？小明:不，他说慢走');
INSERT INTO `tpshop_joke` VALUES ('1966', '我不明白胖子为什麽会被嘲笑。一个把吃完的饭转化成肉的人！凭什麽要被那群把吃完的饭转化成屎的人嘲笑。');
INSERT INTO `tpshop_joke` VALUES ('1967', '　　初中时候，乡下的学校人特多。上课的时候除了前面的认真听课后面的该干吗干吗。一次老师讲完课让我们自己复习就出去了。最后面靠门边那几个逗比竟然打起了扑克，正起劲呢老师回来拍拍一位仁兄的肩膀、那位老兄竟然把老师手一挥来了句别烦我！抬头看到对面异样的眼神，此仁兄才反应过来、后来……后来被罚打扫一个礼拜厕所。');
INSERT INTO `tpshop_joke` VALUES ('1968', '在家排行老二，有个别名叫“二伟”。小时候家里人见我长得可爱都叫我“二宝”，过了几年后变得开始调皮了，爸妈改口叫我“二蛋”，上初中的时候跟人打架，额头被人开了个口子，留了一个很深的伤疤，大家又给我起了个外号叫“二郎神”……二十多年过来了，我特么才发现原来我二到现在……我决定给自己取一个大气响彻的艺名。。球大家帮忙！');
INSERT INTO `tpshop_joke` VALUES ('1969', '我永远忘不了，同桌翻译：王侯将相宁有种乎？这句话的意思，他居然翻译成：王侯，蒋相你们两有种吗？有。');
INSERT INTO `tpshop_joke` VALUES ('1970', '　　昨天捡了个手机，想还给失主，等了一天也没个电话打进来，想还给人家也没办法。晚上回家试着给我的手机打电话才发现，他的手机欠费了。我就说：失主也真是的，充上20元钱，不就行了吗！老公悠悠的说：谁会给丢了的手机充话费啊。。。');
INSERT INTO `tpshop_joke` VALUES ('1971', '有一天，小明考试被一道题难住了，题目是：写一个不……不式词语。 小明冥思苦想，终于想出个不孕不育，他高高兴兴地交了题。 后来，结果下来了，这道题没有对也没有错。 小明问同桌：为什么我这道题没有判呢！ 同桌说：可能你戳到老师的痛处了吧！');
INSERT INTO `tpshop_joke` VALUES ('1972', '...... 买了一条裤子101块，我对老板说：＂一块钱算了吧。＂老板说好的。于是我放下一块钱就走了，老板死命的追着我说我没给够钱，妈的现在.社会一点人与人间的承诺都没了，心塞啊. 买了一件衣服210块，我付老板说:“你这裤子这么贵，质量行不行啊”老板说绝对没问题。于是我说:“那我可以试试下蹲吗”？老板说:“可以”，蹲完之后我又说:“老板，可以跑吗”？老板说:“当然可以”。然后我转身就跑了，老板死命的追着我说我没给钱，妈的现在社会一点人与人之间的承诺都没了，心堵啊。买了一双鞋子102块，我跟老板说抹个零吧！老板答应了，我扔下12块钱就要走，老板拦住我说做买卖好几十年还真没遇见过从中间抹零的！唉～人与人之间连最基本的信任都没有，..好累..感觉不会再爱了..心凉啊！');
INSERT INTO `tpshop_joke` VALUES ('1973', '...... 买了一条裤子101块，我对老板说：＂一块钱算了吧。＂老板说好的。于是我放下一块钱就走了，老板死命的追着我说我没给够钱，妈的现在.社会一点人与人间的承诺都没了，心塞啊. 买了一件衣服210块，我付老板说:“你这裤子这么贵，质量行不行啊”老板说绝对没问题。于是我说:“那我可以试试下蹲吗”？老板说:“可以”，蹲完之后我又说:“老板，可以跑吗”？老板说:“当然可以”。然后我转身就跑了，老板死命的追着我说我没给钱，妈的现在社会一点人与人之间的承诺都没了，心堵啊。买了一双鞋子102块，我跟老板说抹个零吧！老板答应了，我扔下12块钱就要走，老板拦住我说做买卖好几十年还真没遇见过从中间抹零的！唉～人与人之间连最基本的信任都没有，..好累..感觉不会再爱了..心凉啊！');
INSERT INTO `tpshop_joke` VALUES ('1974', '今天下午，我翻看表哥的QQ聊天记录，发现，表哥叫我是妹子，叫其他女的都是妹纸。 我问表哥为什么，他说:纸是可以捅破的，我竟无言以对。');
INSERT INTO `tpshop_joke` VALUES ('1975', '女同事：我能从背后摸到我的肚脐，你行么？ 楼主：我也能。 女同事：不信，你来下我看看。 女同事：干嘛呀你，臭流氓！ 哼哼，别说肚脐了，从背后摸到你胸都没问题，你自己行么。');
INSERT INTO `tpshop_joke` VALUES ('1976', '皇后问魔镜：魔镜呀 魔镜 你说谁是世界上最漂亮的女人。 魔镜：又是你… 皇后：我就知道是我. 魔镜：又是你个臭不要脸的。 （以前的感觉好玩在发遍）');
INSERT INTO `tpshop_joke` VALUES ('1977', '嫂子刚生了小孩，奶水不是很多！ 今天吃饭，我妈妈给她单独做了一碗荷包蛋，和颜悦色的端到嫂子面前说：来来来，多吃点奶，好下蛋！');
INSERT INTO `tpshop_joke` VALUES ('1978', '手机上存了前男友的电话，分手几年了，也没打过电话。 今天，手机上突然来了这货的电话，我一个惊讶，心情很复杂，不知道接还是不接好。 接吧，不知道这货是找我叙旧呢，还是通知我他结婚了，更或者借钱呢，我心里挺矛盾的。 还是硬着皮头接了，还没等我开口，电话那边就传来了他的声音：你好，你的快递，请到楼下来拿。真是的，猜中了开头没猜中结尾。');
INSERT INTO `tpshop_joke` VALUES ('1979', '一把大锁插挂在门上，金钥匙自告奋勇的插了进去，却无法将锁打开。不起眼的铁钥匙来了，他钻进锁孔，轻轻一转，锁便轻松打开。铁钥匙傲慢的说：你知道为什么平凡的我能打开这道锁么？因为我是她的原配，我最懂她的心。金钥匙一脸的不屑：我TM有必要去了解你媳妇的心？我就是插着玩玩....……');
INSERT INTO `tpshop_joke` VALUES ('1980', '昨天一二货朋友相亲，非要拉着我去，朋友家条件挺好的有车有房，我客串司机。 到了饭店，刚坐下那女的说你们开什么过来的，有房吗？一年收入多少？你爸是干嘛的？ 朋友说:有车有房，车是三轮车，房子有，家里四室同堂一百平，一年收入几千块吧，我爸是捡废品的，我那个杯茶啊喷的。');
INSERT INTO `tpshop_joke` VALUES ('1981', '班干部竞选，老师说大家想当什么就上去演讲。这时候，一个逗比同学说“老师，我想当班主任可以吗？”');
INSERT INTO `tpshop_joke` VALUES ('1982', '‍‍‍‍一家三口一起在电视上看一群狮子争吃猎物。 女儿说：“爸爸，我怕。” 爸爸说：“不用怕，有你妈妈呢。” 妈妈很纳闷，爸爸接着说：“你妈妈胖，她一个人就够狮子吃的了，到时候我抱着你跑。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1983', '　　男：现在这个点没公交了，我可不可以在你家过夜 　　女：不行，我家就一张床 　　男：我可以睡地板 　　女：睡地板会不会硬啊 　　男：我尽量克制 　　女：。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('1984', '‍‍‍‍千金三岁了，刚开始自己大便千百个不愿意。 大便完对我说：“麻麻我我病了，你看我大便有长的，有圆的，还有三角型的呢。” 我就问千金：“咋弄的？” 千金：“用手捏的……” 狂吐中！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1985', '非工作时间不愿穿工作服… 今天因开会下班晚了，没换装直接回家了。 女儿看到我先是一愣，然后跑到我跟儿悄悄给我说：“爸爸，你们的校服真好看。”');
INSERT INTO `tpshop_joke` VALUES ('1986', '一网友想在家里自己做足疗，边发贴问：泡脚都放什么啊？神回复：当然放脚，难道放屁啊？');
INSERT INTO `tpshop_joke` VALUES ('1987', '‍‍‍‍上完厕所出来，室友进去说了一句：“你厕所冲的好干净。” 然后我说：“怪我罗？怪我没留给你吃喽？” 于是我遭来了一顿暴打。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1988', '今天是家驹53岁的冥诞懷念一下他一世的精彩歌曲吧！我坐着《早班火车》，踏着《旧日的足迹》，《无声的告别》了《温暖的家乡》。试问在这个《大时代》里有《谁伴我闯荡》?我想《我早应该习惯》这《九十年代的忧伤》.每当《追忆》起《逝去的日子》,便有《无泪的遗憾》，想起从前《抗战二十年》时《牺牲》的《我的知己》.无奈的《叹息》《岁月无声》。《爸爸妈妈》曾经对我说《坚持信念》,《活着便精彩》！而我也《无悔这一生》,继续《勇闯新世界》.即使是《撒旦的诅咒》,我也《仍然是要闯》.我《无语问苍天》问到底《谁是勇敢》的《金属狂人》？问我《可否冲破》那束缚我的《玻璃箱》?《愿我能》《战胜心魔》,然后《冲开一切》,登上《现代舞台》并《随意飘荡》在这个《太空》，去追寻那《和平与爱》的世界。《我是愤怒》的问《大地》这个《不可一世》的《巨人》,怎样找到《长城》脚下的《东方宝藏》?《再见理想》,我在《冷雨夜》里《遥望》，遥望《遥远的PARADISE（天堂）》.让我这个《糊涂的人》在《午夜流浪》《心内心外》的《无尽空虚》都徘徊在这《午夜怨曲》中.我《继续沉醉》在这《漆黑的空间》里《怀念你》,我的《情人》.《原谅我今天》');
INSERT INTO `tpshop_joke` VALUES ('1989', '‍‍‍‍暗恋一女孩，很长时间都没成功，于是乎每次网购都写她的名字。 当快递小哥打电话时我都会回答：“我女友出去了，你给我就行了！” 顿时，在精神上有几许安慰，精神倍增地继续单相思的日子了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('1990', '【非原创】 老公：媳妇儿你和你前男友是怎么认识的？ 老婆：微博 老公：那他收入高吗？ 老婆：微薄 老公：你们就是因为这个分手的？ 老婆：不是 老公：那是因为什么？ 老婆：微勃【坏笑');
INSERT INTO `tpshop_joke` VALUES ('1991', '主持人：“张指导您好，请问您对这场比赛有什么看法？”\r\n张指导：“呵呵。”\r\n主持人：“谢谢张指导带来的精彩点评。”');
INSERT INTO `tpshop_joke` VALUES ('1992', '在餐厅吃饭，等了40分钟还不上菜，我把服务员骂了一顿，这还不算过份的，真正过份的是，我根本还没点菜。');
INSERT INTO `tpshop_joke` VALUES ('1993', '足彩的朋友们，天台来相会，拿好自己票，干了这一杯，三喵队斗牛队，踢的像棒槌，老子的票全部都做了废，啊年轻的朋友们，剁掉贱手来后悔，三串一，都是泪，向前一步加入死人堆。。。');
INSERT INTO `tpshop_joke` VALUES ('1994', '老师：“狗是怎么叫的？”\r\n学生：“汪汪汪。”\r\n老师：“那猫呢？”\r\n学生：“喵～”\r\n老师：“牛怎么叫？”\r\n学生：“哞～～”\r\n老师：“鸡怎么叫呢？”\r\n学生：“大爷，进来玩玩儿呗～”\r\n老师：“你给我过来。”');
INSERT INTO `tpshop_joke` VALUES ('1995', 'A：和女朋友下象棋输了。\r\nB：不会吧！你可是班里的象棋高手啊。\r\nA：她学会之后完全不按套路，她的兵可以走两步说是特种兵。她的炮可以翻两座山，说是现在技术发达了又增加距离了。她的车可以拐弯，还问我说你见过不会拐弯的车吗。她的马可以走目，说是千里马。她的象可以过河，还问我你见过怕水的象吗？');
INSERT INTO `tpshop_joke` VALUES ('1996', '晚上，宝宝在平板电脑上玩切水果，食指飞舞。突然，他把游戏暂停，食指在衣服上蹭来蹭去。\r\n我问：“宝宝，你在干吗？”\r\n他抬起头举起食指说：“磨刀！”');
INSERT INTO `tpshop_joke` VALUES ('1997', '跟一个女博士相亲，我问她：“你学历这么高，嫁的出去吗？”\r\n她说：“这个问题，等我回去写篇论文回答你。”');
INSERT INTO `tpshop_joke` VALUES ('1998', '“你是怎么让你老公戒掉烟的？”\r\n我跟他讲：“如果要是他的鼻孔冒烟的话，我们家的烟囱就没有必要冒烟了。”');
INSERT INTO `tpshop_joke` VALUES ('1999', '上英语课，我跟我哥正聊着微信呢，不知怎么回事，听筒突然改成外扬了，他说的话就放出来了，但奇葩的是呆逼老师以为是我的同桌，于是老师就走过去要没收手机。\r\n谁知道这呆逼同桌乞求道：“老师你信我，我从来不和男的聊微信更不会和男的讨论晚上去哪睡的问题，真的，你信我！！！”');
INSERT INTO `tpshop_joke` VALUES ('2000', '可能是因为遗传原因，我数学很差，有一次我考了58分，我爸鼓励我说：“加油，差3分就及格了。”');
INSERT INTO `tpshop_joke` VALUES ('2001', '朋友：咦？你头发怎么总是那么乱啊..?　　我：噢 我的发型是随着风向改变的...');
INSERT INTO `tpshop_joke` VALUES ('2002', '甲：您可以首付1000购买2015年12月21日的船票，可以月供还贷。　　乙：要是我到那天还不完怎么办？　　甲：没关系，可以帮您改签到其它日期。');
INSERT INTO `tpshop_joke` VALUES ('2003', '甲：听说苏泊尔出问题了。　　乙：是的。　　甲：到底出了什么问题。　　乙：卖锰');
INSERT INTO `tpshop_joke` VALUES ('2004', '有一天开车带着小外甥路过乡村公路，突然路旁飞来一只鸡，小外甥高声叫道：“舅舅，舅舅，快、块把那只鸡压死，我们拿回家吃狗肉。”');
INSERT INTO `tpshop_joke` VALUES ('2005', '小王家装修房子。邻居来帮忙，这时邻居看到旁边小王的儿子小明在玩，于是想哄哄他，问道：“小明，你家装修房子干嘛啊？”　　小明很高兴很天真的回答说：“给我爷爷娶新娘。”　　众人无语，旁边的奶奶差点晕过去');
INSERT INTO `tpshop_joke` VALUES ('2006', '女友：“亲爱的，你会不会为我而死呀？”　　男友很尴尬，一时不作声。　　女友急，步步紧逼：“到底会不会，会不会呀？”　　男友环顾四周，见无人，迅速抠出一块耳屎，塞进女友嘴里。');
INSERT INTO `tpshop_joke` VALUES ('2007', '我自小养成习惯，从不喝饮料，这事寝室人都知道。　　一天和寝室长还有隔壁寝的同学出去打球回来，都要去买饮料，我说我不喝，从来不喝，隔壁寝的感觉很诧异，我寝室长告诉他们，我真不喝，然后又补充一句：所以他能比咱们多活几年。');
INSERT INTO `tpshop_joke` VALUES ('2008', '新婚宴毕,亲朋散场，小两口手拉着手进得洞房，正欲行事。但听有人敲门，新郎不悦，只听丈母娘在门外说：“姑爷，你先出来一会！”　　“真扫兴！”新郎生气地打开闩紧的门。　　只见丈母娘手里拿着一团毛线正在打毛衣。　　新郎问：“妈什么事，这么急？”　　丈母娘说：“我在帮我小孙孙打毛衣，想让你看看，喜不喜欢这个色？要是不喜欢我明天就去换。”');
INSERT INTO `tpshop_joke` VALUES ('2009', '面试官：你最喜欢说的三个字是什么？　　求职者：不知道。　　面试官：那你最喜欢说的两个字是什么？　　求职者：没有。　　面试官：那你想来这工作吗？　　求职者：想。　　面试官：那你现在最想说的一句话是什么？　　求职者：不知道，没有想。　　面试官：思维很严谨，录取了。');
INSERT INTO `tpshop_joke` VALUES ('2010', '上课，哥们在手机上玩切水果，切切切。突然他把游戏暂停，因为手上有汗，在衣服上蹭来蹭去。　　我就问：“你干嘛呢？”　　他抬起头举着手，对我说：“磨刀！”');
INSERT INTO `tpshop_joke` VALUES ('2011', '　　刚刚上高中那时来到课室，都是新同学第一次见，想友好点。见一短发妹子 长的很帅 有点像男，我走过去咯了咯她肩打了个招呼没想到她上来就是一巴掌说：我是女的不知道男女有别吗！ 我个暴脾气回了一巴掌：老子也是女的。');
INSERT INTO `tpshop_joke` VALUES ('2012', '大学的时候交了一宿舍损友！一个比一个懒，每天早晨不起床，等我带包子回来，一个比一个起的快！关键是好多次，我还没吃，洗完脸就被瓜分了，lz忍无可忍，第二天包子打回来，当着他们的面一个一个舔一遍！然后去洗漱！回来之后我就震惊了，留了一桌子包子皮，馅儿都没了！没了！');
INSERT INTO `tpshop_joke` VALUES ('2013', '鲜为什么敢打美国？神回复：因为他们钱不在美国，老婆不在美国，孩子也不在美国；（2）美国为什么不敢打朝鲜？神回复：朝鲜太贫穷，一个导弹好几百万美元，炸哪都赔。（3）美国和朝鲜咋回事啊？神回复：青春期遇上更年期呗！一个是啥事都要管，一个是啥事都敢惹！');
INSERT INTO `tpshop_joke` VALUES ('2014', '有个吃货女朋友真好，别人哄女朋友都要说带你买衣服买首饰买包包......而你只需要说，走，带你吃好吃的去~立马就活蹦乱跳有木有。省钱省力省时间，你值得拥有。。。');
INSERT INTO `tpshop_joke` VALUES ('2015', '　　一女子夜晚收到一条短信，内容称暗恋你很久了，女子有些好奇就和他聊了起来，长此以往相互倾诉，两人产生了感情，就在持续这样几个月快要约定见面之际。男孩打来电话开始一边笑一边骂她大傻逼，女孩哭着问亲爱的为什么骂我。男孩说你是不是前面网购给了一家店差评？女孩说是啊。男孩说我就是店主！');
INSERT INTO `tpshop_joke` VALUES ('2016', '老婆:从结婚到现在，你一共就碰过我九次，你是不是tm在外面有人了？老公:媳妇，不行咱俩就离了吧，从昨天下午结婚到现在我一直都没睡过，求你让我睡会儿吧！');
INSERT INTO `tpshop_joke` VALUES ('2017', '当年我就是为了避免清华和北大为了争我而吵架，我一咬牙一跺脚，就考了100多分，后来蓝翔和新东方就为我打了起来！');
INSERT INTO `tpshop_joke` VALUES ('2018', '　　昨晚喝了点酒，晕乎乎的，没人送我，想着离家也不远，就走回去吧。半路，实在走不动了，就在旁边的长椅上趴了下去。结果早上醒来的时候，身上盖着不少报纸，旁边一个乞丐就蹲在我身边，黑眼圈很重，好像没睡觉，他看着我醒了笑着对我说\"姑娘，以后睡觉回家睡，别跟我抢老窝。');
INSERT INTO `tpshop_joke` VALUES ('2019', '一对同年同月同日生的老夫妇过60大寿，宴席期间，上帝降临，说可以满足夫妻二人两个愿望。老妇人说：“我的梦想是周游全世界。”上帝将手中的魔术棒一挥，哗，变出了一大摞机票。老头说：“我想和小自己30岁的女人生活在一起。”上帝又把手中的魔术棒一挥，哗，把老头变成了90岁。');
INSERT INTO `tpshop_joke` VALUES ('2020', '看着别人轻而易举就拿到了驾照，可到了我这里比登天还难。我实在是太笨，科目三考了好几次都没过关？昨天，教练又要我去上路练车。开车途中，我突然发现前面有一棵树，顿时手忙脚乱地使劲按喇叭，教练说如果今天这棵树能给你让道，我拜你为师！');
INSERT INTO `tpshop_joke` VALUES ('2021', '小提琴家参加宴会，女主人邀请他演奏一曲，他婉拒：“太晚了，会影响邻居休息的。” 女主人激动地说：“那样就太好了，谁让他家的狗昨晚在我家窗下叫了一夜呢！”');
INSERT INTO `tpshop_joke` VALUES ('2022', '一个人很吝啬朋友来做客，他只用青菜招待，还假装的热情地说：“没有什么好招待的，没有什么菜。” 朋友说：“这些不是菜，难道还是肉？”');
INSERT INTO `tpshop_joke` VALUES ('2023', '问：“如果你的情敌掉在河里了，他不会游泳，而你会，你该怎么办？”\r\n神回复：“众生平等，不能因为自己偏见而在他人最需要你的时候让他绝望，生命是珍贵的，每个人都有自己的世界，即使是情敌他也有父母亲戚朋友，不能自私自利，所以，我会奋不顾身跳下水，在他面前游，让他看着学习。”');
INSERT INTO `tpshop_joke` VALUES ('2024', '老公家里是农村的，结婚前几天就开始杀猪杀鱼杀鸡，等着宴请宾客……结婚前两天老公来我家找我，我问他这几天在家干嘛了，他说为了娶你把我们家能杀的都杀了……');
INSERT INTO `tpshop_joke` VALUES ('2025', '网络真是一个神奇的地方啊，单身二十五年，终于在网上找到女朋友！马上她就来我家了！！！好了，不说了，快递到了，我去签收！');
INSERT INTO `tpshop_joke` VALUES ('2026', '姐姐第一次杀鱼，犹豫了半天也不敢下手。过了一会，再去看时，只见她两手握着鱼，把鱼按在水底，我问她要干嘛？她说：“等把它淹死了再杀……淹死了再杀！');
INSERT INTO `tpshop_joke` VALUES ('2027', '一个女同事坚持穿露背装上班，我感到很好奇，就问她：“为何总穿得这么少？”\r\n“切，才不是呢，你别瞎说，”她羞涩地答道，“我是为了张总，不是何总……”');
INSERT INTO `tpshop_joke` VALUES ('2028', '这天突然发现，我有大姨，二姨，四姨，五姨，却没有三姨。于是就去问我爸：为什么我没有三姨？心里还想了一下：难道三姨在小的时候就死了？我爸说：你三姨就是你妈！');
INSERT INTO `tpshop_joke` VALUES ('2029', '刚刚一个人在火车上感慨，现在这时候火车上的人除了吃饭的，都在玩手机，这是一种可悲的社会现象，另外一个在吃饭的还附和着说，是啊，人与人之间都缺乏交流了，过了不到5分钟，那两个几乎同一时间拿出手机在那开始把玩起来了。');
INSERT INTO `tpshop_joke` VALUES ('2030', '就是饭馆吃饭点了两个菜，\r\n吃第一个：“世上还有比这更难吃的吗？！ ”\r\n吃第二个：“靠！还真有０');
INSERT INTO `tpshop_joke` VALUES ('2031', '昨天晚上喝多了，咋上的楼都不知道，早上在楼道冻醒了。于是敲门和老婆解释昨晚是睡在楼道了。老婆说：下次喝多了也要敲门知道吗？我愧疚的点点头，这时发现茶几上放着我的钱包、钥匙和手机。');
INSERT INTO `tpshop_joke` VALUES ('2032', '昨天晚上喝多了，咋上的楼都不知道，早上在楼道冻醒了。于是敲门和老婆解释昨晚是睡在楼道了。老婆说：下次喝多了也要敲门知道吗？我愧疚的点点头，这时发现茶几上放着我的钱包、钥匙和手机。');
INSERT INTO `tpshop_joke` VALUES ('2033', '以前我特别自卑，都不敢和女的一起玩，于是我每天激励自己：“你行的！你是最棒的！”。。。一个月下来很有效果，现在的我已经觉得她们不配跟我玩了。');
INSERT INTO `tpshop_joke` VALUES ('2034', '和老婆一起七年了。昨天第一次动手打老婆，老婆一直没说话也没哭。没求饶。我非常生气摔门就走了出去，我很痛苦，又很担心老婆，于是跑回家想道歉，却发现她被我打破了。也没气了。');
INSERT INTO `tpshop_joke` VALUES ('2035', '初中时，我英语一窍不通，又赶上老师评职称，要看成绩，考试时他看我一直在咬笔，就来到我跟前，敲着我的卷子说:“让你平时不好好学，现在不会了吧，说过你多少次，就是不开窍……”说着敲着，我那个惭愧啊，等他走后，我发现每个选择题的其中一个答案下面都有一个指甲印，那是我英语第一次考及格。');
INSERT INTO `tpshop_joke` VALUES ('2036', '我坏笑着对老妈说：我给你讲个笑话吧，从前有个傻子，很喜欢说没有，别人问他什么他都说没有，你听过这个笑话吗？老妈说：你以前有跟我讲过吗？我摇了摇头：没有。转念一想，艹，姜还是老的辣。');
INSERT INTO `tpshop_joke` VALUES ('2037', '厕所出来，洗手。旁边一个小孩在玩烘手机。我洗完也去烘，小孩很不满我打扰他：你干嘛不用那边那个。我给他个白眼：我就喜欢用这个，怎么样！你管我哦！小孩瞪我：幼稚！然后气乎乎走了。第一次战胜熊孩子！');
INSERT INTO `tpshop_joke` VALUES ('2038', '老爸酒足饭饱之后，坐在沙发上看报纸，看着看着，他忽然“扑哧”一笑，说：“报纸上净胡说!”儿子问：“说啥了?”老爸答：“说喝剩的啤酒能浇花。”儿子追问：“不对吗?”老爸反问道：“酒还能剩?”');
INSERT INTO `tpshop_joke` VALUES ('2039', '自己经常睡觉的时候突然身体抖一下就醒了。于是去搜了下“睡觉时身体突然抖一下的原因”，然后一个医生给我的答案是“睡觉状态下忽然的抖动是神经系统发现你忽然陷入睡眠，很久没有活动，它以为你死了，所以它就动动，想试试你死了没有…”');
INSERT INTO `tpshop_joke` VALUES ('2040', '我:“历史上的孔子是个圣人。”  朋友:“根本不是。也就是个男同志而已。”  我:“何以见得？”  朋友:“你看啊，史书上明白的说他，孔子东游，见两小儿便日。”');
INSERT INTO `tpshop_joke` VALUES ('2041', '放假回家，不知怎么的，就走到了高中时暗恋了3年的女孩家的楼下，想想上次来还是3年前悄悄跟着她回家。只是希望看着她的背影...这时从楼上下来一女孩，这背影好熟悉，于是快步走过去，一看，还是她，她的背影还是那么亲切，和她寒暄几句就走了。。。。。。。心里思绪万千啊，怎么越长越难看了呢。。。');
INSERT INTO `tpshop_joke` VALUES ('2042', '老板对甲说：尼玛，我看这小子很不顺眼，你给我把他灭了，我给你一万……\r\n甲：这个……\r\n老板：怎么？我养着你，好吃好喝，现在这点事，你都不能帮我办？再说我还给你报酬……\r\n甲：……好吧……\r\n……\r\n甲对乙：兄弟，只要你把那小子给我灭了，我给你五千……我们关系不错吧，就当帮兄弟一忙……\r\n……\r\n乙对丙：我的老大看那小子不顺眼，你去给我把他灭了……\r\n丙：这不好吧？\r\n乙：怎么不好，我要看看你的胆量，看看你还不是忠心……这样吧，干成了奖励你两千……\r\n……\r\n丙对丁：小子，就看你的表现了，如果你表现好，就可以和老大说说，到时候，就可以吃香的喝辣的……\r\n丁：那，那我怎么表现啊？\r\n丙：拿刀去把那小子给捅了……\r\n……\r\n丙：你的表现不错，奖给你一百元，以后……');
INSERT INTO `tpshop_joke` VALUES ('2043', '朋友问我为什么你从来不上当，不管是传销啊还是商家打折啊从来都不上当。我很淡定的告诉他智慧。\r\n其实就原因一个字 穷.........................................');
INSERT INTO `tpshop_joke` VALUES ('2044', '妈妈问我今天还去不去健身房，我说去的，我要坚持，要练出好身材，像欧美的美女一样，有性感的肌肉。然后妈妈说“人家有肌肉，你也不看看人家欧美的女人有多高，你有肌肉就是矮壮”……');
INSERT INTO `tpshop_joke` VALUES ('2045', '昨天我们班的小朋友说：蔡老师，我爸爸和我妈妈打架了！我八卦性质又出来了，然后我就问：真的呀？是早上打架的还是晚上打架的？小朋友说：晚上打架的！我就问：为啥呀？小朋友就说：因为爸爸抢妈妈的棉毛裤，妈妈不让！我：……');
INSERT INTO `tpshop_joke` VALUES ('2046', '我有哥们特逗，人长的有点着急，三十不到，此为背景，gc是一次在街上遇到两大学生模样姑娘甜甜地问路，“叔叔，会展中心怎么走”，逗B哥们不知怎么抽风了，来了句“嫂嫂，你在问我吗”，那场面大家可以自己往下想了');
INSERT INTO `tpshop_joke` VALUES ('2047', '端午节将近，听说后所街的粽子不错，于是买了两个尝尝鲜，有4元一个，还有8元一个，我买了两个4元一个的，跟妻子一块分享。一尝，还真是名不虚传。第二天，妻子说晚饭吃稀饭，要我买点东西垫巴一下，我觉得粽子还不错，就又上后所街再去买两个粽子，谁知那儿4元一个的卖完了，只剩下8元一个的，无奈，我只好买了两个8元一个的，也想尝尝它与4元一个的到底有什么不同。饭间，我问妻子：粽子好吃不好吃？妻子：好吃！我：与上次有什么区别？妻子：没有什么区别！一样啊？我：这是8元一个的！妻子：啊？我只觉得只是价格不同罢了！我正在纳闷，是不是那个男人（上次是一个妇女）搞错了？把4元一个当成8元一个的卖给我？如果是涨价也不至于这么离谱？突然妻子叫了一声：我找到了！我：找到什么了？大惊小怪的！妻子：多了这样东西！我仔细一看，原来粽子馅里面多了一小块板栗！而且只是一粒板栗的四分之一！我感慨万分：好珍贵的板栗啊！');
INSERT INTO `tpshop_joke` VALUES ('2048', '护士给患者扎屁股针，针扎下去患者一紧张没控制住肛门放了个响屁，一个护理员赶紧跑过来惊讶的问：怎么了，是不把屁股扎爆了？');
INSERT INTO `tpshop_joke` VALUES ('2049', '奥巴马到印度访问，每天使用的经费是2亿美元，消息一出，舆论哗然。当总统即将回国，专机飞过国界线时，奥巴马收到美国平民发给他的一条短信：“总统出国访问一天，穷汉饿死十万三千！”奥巴马马上回复：“我亲爱的子民们，别哭穷好吗？你们穷，难道我就不穷吗？我出国访问，口袋里却空空的，一美分也没有！我所用的一切经费，包括吃的和穿的，连同内裤一起，都是借外国的债开支的嘛。这些钱，可不能白用，我的任期一到，外国人就会要我们连本带利一起归还的，所以我在这短短的几年任期之内忙于秉烛夜游完全是出于不得已啊。你们为自己挣钱忙，我为美利坚花钱忙，我们都是结在这同一根忙藤上的苦瓜嘛！”');
INSERT INTO `tpshop_joke` VALUES ('2050', '本人比较喜欢说粗话，有一天给老公打电话，一接通就说：“你妈逼你在干嘛呢。”\r\n结果是婆婆接的电话，婆婆说：“我在逼他做菜呢。”\r\n艾玛，当时吓死了，好幸运的躲过了一劫。');
INSERT INTO `tpshop_joke` VALUES ('2051', '刚上大学时，每天早上都在别人还在睡觉的时候，起床化妆，晚上在别人睡着的时候，卸妆睡觉；就这样，我有一次起来晚了，还没来得及洗漱化妆，就被室友当男的打了出去...');
INSERT INTO `tpshop_joke` VALUES ('2052', '　　一同事，平时就特2。大家都喜欢和他一起玩。这一天上班，眉飞色舞的进了办公室，迫不及待地和大家炫耀刚买的新手机。这个嘚瑟别提了。第二天上班，蔫头耷脑的进了办公室，主任就问他：怎么了，看你这状态不对啊？2B说：昨天新买的手机，晚上没事下载了一个称重软件，下载成功安装完毕，就把手机放在地上，然后一只脚踏了上去……一个办公室的人都没有憋住笑。');
INSERT INTO `tpshop_joke` VALUES ('2053', '我特么就想问问幼儿园能不能多找几个男老师？儿子放学回家上厕所，居然蹲着嘘嘘，完事还特么拿纸擦擦！');
INSERT INTO `tpshop_joke` VALUES ('2054', '我跟我妹睡，此为背景，今早妹的男票来找她，好不容易把她拉得坐起来，没叫他去把内衣取过来，男票很忧伤的说\"我哪知道哪是你的啊，等下取到姐的怎么办，\"我默默地说了一句，\"你看着小的取\"');
INSERT INTO `tpshop_joke` VALUES ('2055', '小明问他妈妈：“为什么你总不告诉谁是我爸爸啊？我爸爸到底是谁啊？”妈妈回答说：“你爸爸做好事不留名，我也不知道他是谁………”');
INSERT INTO `tpshop_joke` VALUES ('2056', '　　去年夏天去野三坡玩，事先联系好了一家旅馆。进了房间发现香烟的味道特别大。打电话给前台服务员。前台回复：“请稍等，马上派人为您的房间做无烟处理”。好高大上啊，无烟处理，什么高科技。一会儿，来了一位服务员，打开了我房间的所有窗户和门。科技太发达了，果然，一会儿烟消云散。赞一个！');
INSERT INTO `tpshop_joke` VALUES ('2057', '一个朋友（女）因为家庭暴力离婚了，随后她碰到了一个阳光帅气的男孩，他对她非常好，说不在乎她的过去，不在乎她有个孩子，只要能娶她，能跟他在一起就知足了，于是，朋友接受了他的诚恳，两人很快办了手续。可是好景不长，前段时间这个曾经说什么都不在乎的人突然就什么都在乎了，对我朋友各种嫌弃，原来这个男孩又找了一位小姑娘，逼着我朋友离婚，大家能不能帮帮她，她以后的路该怎么走啊？！');
INSERT INTO `tpshop_joke` VALUES ('2058', 'LZ男，胖纸一枚体重一百八有余，有天正在做饭媳妇在后面放洗澡水，不知道怎么想的突然决定用屁股顶我一下。。。。。她才九十不到。结果，媳妇华丽丽的被反弹进了浴缸。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('2059', '前两天，老师打电话说我儿子考试语文98分、数学17分，孩子偏科太严重了。人无压力轻飘飘，我给他好一顿胖揍。还别说，这次考试就好多了，语文数学都17分。');
INSERT INTO `tpshop_joke` VALUES ('2060', '男:我快死了，你可以收养我的孩子么？ 女:什么？你快死了？你孩子多大了？ 男:他还小，还是颗细胞，你肯定能养活他！ 女:滚！');
INSERT INTO `tpshop_joke` VALUES ('2061', '今天跟同事到公司楼下的奶茶店喝奶茶！奶茶店新来了个妹子。奶茶一共16块，买单的时候我摸摸手袋只有15块零钱！于是对着哪妹子说：万水千山总是情，少收一块行不行？哪妹子哪个害羞啊。没想到老板娘随口接道：世间哪有真情在，多赚一块是一块。我：。。。。');
INSERT INTO `tpshop_joke` VALUES ('2062', '‍‍老师把小明家长叫到办公室。 说：“小明成绩下降，是因为在和小红谈恋爱。” 他爸突然一巴掌扇过来：“你和小红谈，小丽怎么办？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2063', '　　说一个大学发生的事情。中午下课，我和一群基友往寝室狂奔，边走边聊中午吃什么，忽然，鹏哥突然冒出一句：*哥，咱俩结婚吧！我顿时呆在原地。其他好基友全坏笑的注视着我，我磕磕巴巴崩了半天说了几个字：我...男...的...啊...啊。鹏哥抽口烟淡定的说：没事，我有葵花宝典！我们全傻了！过了很大一会，鹏哥才反应过来把烟撇地上，用脚蹍了蹍，淡定的说到：我说的是游戏！！！');
INSERT INTO `tpshop_joke` VALUES ('2064', '今天约会迟到惹女朋友生气了，为了提醒自己，也为了下定决心，我愤然拔刀，在她背上刻了一个“早”字。');
INSERT INTO `tpshop_joke` VALUES ('2065', '媽，咱家买了一只鹦鹉吗？”看清楚，那是一只鹰”！哦，会说人话吗？”什么说人话，又不是鹦鹉，哪会学舌！”不是有个鹰语速成班吗”！！！！');
INSERT INTO `tpshop_joke` VALUES ('2066', '‍‍小明老师：“小明妈妈你的身材怎么保持的这么好？” 小明妈妈：“靠跑步！” 小明老师：“那么怎样才能保持跑步的习惯呢！” 小明妈妈：“靠欠债！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2067', '中华文明史，哪个皇帝最缺心眼，李世民呗。千载难逢的机会被他浪费了，如果当时他把唐僧吃了，而不是让他去取经 现在是不是还是大唐盛世呢。');
INSERT INTO `tpshop_joke` VALUES ('2068', '最近便秘去医院就诊，排队人很多就玩儿会儿手机，不一会儿，医生叫我：你，那谁，你是治便秘吗？ 我不好意思的点头算是答应 医生：你别排队了，赶紧回去换个手机，连个智能手机都不是，怎么有助排泄..');
INSERT INTO `tpshop_joke` VALUES ('2069', '‍‍最近因为工作的原因觉得累，就含泪给妈妈发短信说：“我真的很想放弃。” 结果打成了：“我真的很想放屁。” 我妈妈过了很久才给我回了一条：“放了吗？不用向我汇报的。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2070', '‍‍“119吗，我家里着火了。” “在哪里？” “在我家。” “具体点！” “在我家的厨房里。” “我说你现在的位置！” “我在厨房下面趴着。” “请问我怎么到你家呢？” “你不是有消防车嘛？” “我去，烧死你活该。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2071', '　　又一次我在公厕，不禁唱起了“一生要强的爸爸，我能为你做些什么？”这时一个坑里传来了一句“哥们我没纸了，借我点纸行不”');
INSERT INTO `tpshop_joke` VALUES ('2072', '妻子和一岁半的女儿一起洗澡，丈夫看到后，说下次还是不要和女儿一起了。妻子问为什么，丈夫说女儿还在认知阶段，别让他以为男女上半身都长一样…');
INSERT INTO `tpshop_joke` VALUES ('2073', '‍‍男：“长这样还好意思出门，你家没镜子么？要我撒一泡尿给你照下么？” 女：“附近又没电线杆，你撒得出来么？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2074', '‍‍有一次和同事一起去旅游。 同事一泡尿憋得难受，到处寻找公共厕所，最后终于找到了。 冲进去没多久，正好有几个黑人也进去上厕所，同事马上出来了。 “拉完了？”我问到。 “没有。” “那为什么这么快就出来了。” “实在拿不出手啊！”同事脸红着回答。‍‍');
INSERT INTO `tpshop_joke` VALUES ('2075', '昨晚用女友手机给她闺蜜发了条短信：“我怀孕了。”结果，回复了条：“是你男友的还是XXX的？，你先别和你男友说。”昨天一晚没睡，麻木的手到现在还在发抖……');
INSERT INTO `tpshop_joke` VALUES ('2076', '朋友经过一个小树林，突然遇到一个大汉。大汉问：“这位先生附近可有警察？”朋友说：“没有。”“那能在短时间内找到警察吗？”朋友说：“应该不能吧”。随后大汉拿出一把西瓜刀说：“双手放头上，抢劫！”');
INSERT INTO `tpshop_joke` VALUES ('2077', '牛魔王大喝一声，将面前的山劈成两半，然后转过头问孙悟空，“贤弟，你看我牛逼不？”孙悟空:“不看。”');
INSERT INTO `tpshop_joke` VALUES ('2078', '某女子求助：爱上一个黑人了怎么办？黑人很可怕吗？楼下神回复：楼主会从。变成0直到O');
INSERT INTO `tpshop_joke` VALUES ('2079', '一哥们谈恋爱时把网名改成蓝色 ，我一直不解。最近才知道蓝色的英文是blue..');
INSERT INTO `tpshop_joke` VALUES ('2080', '一神人发帖说自己的前任是左手，现任是右手。 一楼神回复：孩子，你幸好没练过瑜伽！');
INSERT INTO `tpshop_joke` VALUES ('2081', '不论哪个姑娘的名字后头加 个.rmvb或者.avi看上去顿时就有了一种别样的风情！');
INSERT INTO `tpshop_joke` VALUES ('2082', '‍‍有一天小明迟到了。 老师怒：“为什么来这么晚？” 小明：“睡过了。” 老师：“怎么回事啊？” 小明：“做梦梦见老师讲课，就想多听会。” 全班顿时鸦雀无声。‍‍');
INSERT INTO `tpshop_joke` VALUES ('2083', '‍‍‍‍老师问小明：“如果你开车遇到了碰瓷的，你会怎么办？” “先看看有没有行人和摄像头。” “假如这些都没有呢！” “那我就直接压过去。” 老师：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2084', '　　就在今天早上做梦梦到一老奶奶问我有对象没，我说没有，然后说她孙女和我一样大，也没对象，就问我要电话，我刚说了个135...就被我妈一嗓子给叫醒了。我的老妈啊！你敢不敢别这么坑');
INSERT INTO `tpshop_joke` VALUES ('2085', '小明：老师，你知道谁是中国历史上第一个快捷酒店的代言人吗？老师：不知道，你说说。小明：孔子啊。老师：为什么是孔子呢？小明：不是有句话说得好，孔子是儒家（如家）的代表人物！老师：滚出去！！');
INSERT INTO `tpshop_joke` VALUES ('2086', '看了东北一哥们在监狱约炮的新闻，心里久久不能平静。 一个囚犯，身陷囹圄，即使电网高墙也不能阻挡人家对美好生活的墙裂渴望。问题是，人哥们不仅渴望，而且还行动了；不仅行动，而且成功了；不仅成功，短短几个月竟成了七个；不仅约了，人家还财色双收！并且，更为励志的是，一个囚犯，竟能在警察的值班室睡了警察的老婆！尤其难能可贵的是，人家根本没编高富帅的谎言，而是坦诚地以囚犯的身份，让情人带钱到监狱赴约。 我们还有什么理由说困难，还有什么理由抱怨业绩难做？我们不要为困难找理由，只为成功找方法。加油…');
INSERT INTO `tpshop_joke` VALUES ('2087', '去同学家里玩，到门口了听见他大喊芝麻开门，我就笑他幼稚，肯定没人鸟你，但是门开了，他小妹开的，我进去后跟他小妹打招呼说你真好，你哥这么幼稚你还陪他玩，结果她瞪我一眼，说:我就叫芝麻。。。。');
INSERT INTO `tpshop_joke` VALUES ('2088', '女友问我：“为何上帝要让女人在经期饱受经痛之苦，却没有给男性任何折磨？” 我大笑说：“别傻了，上帝给了我们女人啊！”');
INSERT INTO `tpshop_joke` VALUES ('2089', '‍‍‍‍昨天晚上和媳妇闹矛盾。 以前老哄着她，跟她道歉！这次确实她做的不对，所以就没搭理。 今天早上一起床，直接从抽屉拿出一片姨妈巾，狠狠摔我脸上。 我问她：“到底什么意思？” 她来句：“老娘‘付巾请罪’。” 然后一摔门出去上班了……‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2090', '‍‍别人问我：“如果我情敌掉水里了，恰好我又会游泳，问我会怎么做？” 我答：“我会在他旁边游来游去！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2091', '　　有一头小马要过河 ，但是不知道水有多深 碰见一头老黄牛。 老黄牛说:水很浅， 刚好没过你的腿。 这时又来了一只小松鼠。 小松鼠说:别听他的， 水很深，会淹死你的！！！ 小马犹豫了 ， 不知道该相信谁好。 这时小马的妈妈过来说:别理那两个神经病， 咱们走桥！！！！');
INSERT INTO `tpshop_joke` VALUES ('2092', '释迦牟尼的一句话：“无论你遇见谁，他都是你生命里该出现的人，都有原因，都有使命，绝非偶然，他一定会教会你一些什么”。 喜欢你的人给了你温暖和勇气；你喜欢的人让你学会了爱和自持；你不喜欢的人教会了你宽容和尊重；不喜欢你的人让你知道了自省和成长。没有人是无缘无故出现在你生命里的，每一个人的出现都是缘份，都值得感恩。 –––若无相欠，怎会相见！（送给那些心灵受到创伤的人们。）');
INSERT INTO `tpshop_joke` VALUES ('2093', '　　今天在街上走，听见一家店前传来淫荡的一声“小帅哥，快点儿来玩啊”，我看了一眼，笑了笑，“小骚货，等着，看我今天不整死你”。 果断走上前去，给了她一块钱，不一会儿，伴随着动感的音乐，我们晃动了起来……“喜羊羊，暖羊羊，红太狼，灰太狼……”');
INSERT INTO `tpshop_joke` VALUES ('2094', '一哥们相亲，女方问：“你有车吗？” 哥们：“宝马X6！” 女方：“有房吗？” 哥们：“高档小区三层复式！” 女方：“恩，条件还可以，先谈谈吧！” 哥们：“你怎么不问问我能不能看上你啊！” 女方：“……”');
INSERT INTO `tpshop_joke` VALUES ('2095', '高贵的女人看她的鞋。精致的女人看她的指甲。性感的女人看她的香水。气质的女人看她的手表。拜金的女人看她的包包。感性的女人看她的文章。贤惠的女人看她的拿手菜。浪漫的女人看她的睡衣。小资的女人看她的化妆包。我看完发现我好像不是女人！赶紧掏出身份证一看，性别：女～心里才踏实了些。。。。。。艾玛。吓死我了，一样没占。。。。 ');
INSERT INTO `tpshop_joke` VALUES ('2096', '　　以前认识一个女的，她说她最喜欢的电影是郭敬明的小时代，因为小时代拍的比较真实自然，反映了生活中的现实。最讨厌周星驰，因为周星驰演技太浮夸太夸张了不真实。我也只能瞠目结舌呀。 　　有人就反驳我说这个就是萝卜青菜各有所爱，没有对错，你也不必较真。 　　我说这就像有人喜欢吃青咖喱，有人喜欢吃黄咖喱，这叫萝卜青菜各有所爱，可是有人说喜欢吃屎，那别人当然要瞠目结舌了');
INSERT INTO `tpshop_joke` VALUES ('2097', '刚在飞机上关机了，问旁边老奶奶几点了，问了三遍她才说“我不能告诉你”。我问为什么啊！老奶奶说告诉你了，你就要谢谢我；你谢谢我，我就得和你唠嗑；唠熟了，你肯定要帮我拿行李；你帮我拿行李，我就要请你去我家吃饭；万一我孙女看到你爱上你，而你特么连手表都买不起...所以不能告诉你几点了...');
INSERT INTO `tpshop_joke` VALUES ('2098', '新兵外出拉练，一新兵瞅见路边一个穿制服的肩膀上六颗星，心中一惊：六颗星啊！“啪”的一下一个立正军礼，以示敬意。排长跑过来就是一耳光：“敬你妈的礼，那是物业！”');
INSERT INTO `tpshop_joke` VALUES ('2099', '今天和老婆吵架，吵到激烈的时候，我突然觉得我一个大男人为什么要和一个女人一般见识呢？何况还是自己的老婆！当时我就跟老婆道了歉，老婆挺高兴的。道完歉，她哥哥把菜刀放下了，她弟弟把铁锹也放下了，她妹妹拽着我头发的手也松开了，妹夫手里的擀面杖也扔地下了，老丈人也把砖头丢开了，一家人于是乎又愉快地坐在一起，聊着天，喝着茶，吹着牛，有说有笑… 多好 ');
INSERT INTO `tpshop_joke` VALUES ('2100', '爸妈吵架吵的很凶，突然老妈把她手机摔了。然后我爸就不服了，“你吵不过就吵不过”摔什么东西。！我妈豪不示弱，我的东西我爱怎么样就怎么样，突然我爸给我一巴掌说道：我儿子，我随便打怎么样！我妈把我拉过去一边说，儿子也是我的，说完又给我一巴掌！');
INSERT INTO `tpshop_joke` VALUES ('2101', '一警察巡逻，发现银行门口一车违规停靠。便上前准备开罚单时，从银行里跑出一蒙面人冲他喊叫：“喂，先别开罚单，我们抢完银行马上就走。”');
INSERT INTO `tpshop_joke` VALUES ('2102', '‍‍‍‍老王查出得了艾滋，顿时医院体检部人员爆满。 “啊，这不是晓丽吗？身体不舒服啊？” “是啊！刘大妈，您也查身体啊？” “李叔叔，你上周不是查过了吗？” “多检查身体更健康！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2103', '小姨子得了肺炎住院，我和媳妇去看望，刚到病房，就听见岳父在吼:我说你拍个X光，还摆什么剪刀手。。。');
INSERT INTO `tpshop_joke` VALUES ('2104', '有次历史考试，我交了白卷。老师很生气，问我：为什么交白卷？难道一题都不会吗？我：我怕不小心篡改历史！');
INSERT INTO `tpshop_joke` VALUES ('2105', '大学里一个室友嘴特别贱，是一个逗比，记得刚来的时候他曾说：“我在文学上特别有造诣，比如据我研究，不管什么诗词，最后都能以“晚上回家玩老婆”做结尾，你们随意感受下。”');
INSERT INTO `tpshop_joke` VALUES ('2106', '‍‍约女朋友去看速7，结束后…… 楼主：“你有没有男朋友？” 她：“没有！” 楼主上去亲一口：“现在有了！” 她低头说：“恩，前男友！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2107', '‍‍‍‍逗比比同学去面试。 考官问：“中美最大的差别是什么？” 逗比同学（深思熟虑）：“中国比较自我，美国更注重他人感受！” 考官：“为什么？” 逗比同学：“啊？中国人的口头禅是卧槽！美国人的口头禅是fuckyou！” 十秒钟之后，考官：“请你圆润的滚出去！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2108', '‍‍老婆怀孕8个月了。 今天在我耳边轻轻的说：“再过两个月就会有一个小人在我这喝奶了，到时你看到了回事什么感受啊？有什么想法啊？” 我过了一会默默的回答：“我会跟他商量一下，能不能给我留点呢！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2109', '　　记得上学那时候，，看别人找四叶草，我无聊也找，竟然被lz找到了一株，对，没错，是一株，有三四十个吧，然后我用树叶把它盖上了，妈的，有个女孩说让我给她找一个四叶草，我那时特别自豪的说，一个 我给你找十一个 找到了做我女朋友 她说好，我直接去拔了十一个，她没话说了 反正她现在是我老婆了');
INSERT INTO `tpshop_joke` VALUES ('2110', '我厌倦了别人再喊我“矮子”，于是我发誓努力多吃饭长高。功夫不负有心人，在我不懈的努力下，我终于成了别人口中的“矮胖子”。');
INSERT INTO `tpshop_joke` VALUES ('2111', '一女性朋友。。样貌中等偏上，被一屌丝穷追猛打好久，朋友受不了屌丝男的骚扰便对其说：你要是一年能赚到100万我就要你。。。屌丝男回复：我要是能赚到一百万还要你啊。。。！！！');
INSERT INTO `tpshop_joke` VALUES ('2112', '‍‍室友甲：“老大，你的苹果再不吃就烂了。” 室友乙：“那你吃吧！” 室友甲：“我已经吃过了。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2113', '我经历的最奇葩的婆媳大战：有一天我媳妇给我做饭，做的确实很好吃，然后吃晚饭没事就在QQ上发了一个我媳妇做的饭是天下最好吃的。然后妈妈看到了，回了个比我做的还好？然后我媳妇也看到了，然后两个人在我的动态里吵了一个多小时，我还不敢插话。');
INSERT INTO `tpshop_joke` VALUES ('2114', '就今天早上天气好冷，上班起来晚了，漱口洗脸，围上围巾出门，在公交车上都望着我，哥还以为自己变帅了，到了公司把围巾拿下下来的时候，尼玛居然围的是洗脸手巾。一路居然没发现。');
INSERT INTO `tpshop_joke` VALUES ('2115', '‍‍小明哭着对妈妈说：“我的小乌龟死了。” 妈妈说：“明明不哭，妈妈给你买玩具，还给你买好吃的。” 小明转悲为喜。 这时，妈妈看到小乌龟动了一下，说：“小乌龟没死。” 小明一下哭了，说：“妈妈，我可以把它杀了吗？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2116', '曲艺很苦恼问大师：大师，我长得这么漂亮，每天出门都被一群男人死缠着 告白，我又很难拒绝别人，我该怎么办呢？ 大师默默的从池塘里鞠起一瓢水，泼在女子的头上。 曲艺恍然大悟的说：我懂了，您是要我心静如水，对待世间万物都以清澈的 心态面对……是吗？ 大师答 : 没这么复杂… 你只要把妆卸了，世界就安静了。');
INSERT INTO `tpshop_joke` VALUES ('2117', '‍‍那天我和一女子相亲。 我看到对方高大臃肿，不是心中的理想款型，就要买单起身。 没想到女方问了话：“你对我有何感受？” 我说：“和怀孕初期差不多。” 女子说：“你是嫌弃我胖吗？” 我说：“不是的，等你怀孕就会明白了。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2118', '‍‍‍‍公交上特挤！ 我受不了，于是喊：“再挤。都要把我手挤别人的包里了！” 好多人瞬间和我保持了距离，这下舒服了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2119', '男人再穷不能穷父母，再苦不能苦媳妇，再坑不能坑兄弟，在饿不能饿孩子！ 男人最怕什么？兄弟的误解，媳妇的眼泪，父母的委屈。累了，伤了，哭了，痛了，躺下来抽根烟，喝口酒，默默的承受着，生活还要继续。');
INSERT INTO `tpshop_joke` VALUES ('2120', '某明星出租房内，有个光头男人脱光了衣服撅起屁股，##『轻点，怕疼』『好的』这时警察推门而入，##『都不许动，穿好衣服跟我们走一趟，我们接到举报说有人在此吸毒』##『警察同志我朋友，屁股被毒蛇咬了，我，帮他把毒吸出来』##『额，不好意思打扰了，你们继续，收队』');
INSERT INTO `tpshop_joke` VALUES ('2121', '我：“我给你变个魔术。” 基友：“哎呦你变啊。” 我：“看着我的眼，我能让你一秒钟忘记你是狗。” 基友：“啥玩意，我不是狗啊。” 我：“看，你忘了吧。”');
INSERT INTO `tpshop_joke` VALUES ('2122', '前女友突然给我打电话，不断的向我倾诉她老公时常背叛她还经常打她。 听的我真的好担心，担心她男朋友下手太轻。');
INSERT INTO `tpshop_joke` VALUES ('2123', '我是来给快高考的孩子们打气来的。 作为一个大三的过来人，我只想说：“高考没什么可紧张的，它只不过是决定你将来在哪座城市打lol而已，所以根本不用紧张。”');
INSERT INTO `tpshop_joke` VALUES ('2124', '小明去网吧，看到一个人桌子上放的苹果6，拿起就跑。 结果被追了两条街还被暴打了一顿，小聪得知后让小明跟着他学。 二人走进网吧，看到一人桌上放着苹果6。 小聪走到背后看了一会然后拿起手机不慌不忙的走了，那人只是叫骂却没有追。 小明很是费解，小聪说：“那人正在LOL四杀了，我是等他准备五杀时拿的他手机。”');
INSERT INTO `tpshop_joke` VALUES ('2125', '今天是情人节，老婆又打扮的漂漂亮亮的上街给我买礼物去了。 去年就给我带回来小牙刷、小牙膏、小梳子，居然还有一包玉溪。 虽然只有18根烟在里面，但心意是满满的。 现在老婆又出门了，好期待。');
INSERT INTO `tpshop_joke` VALUES ('2126', '云雨过后，男友抱着我说：“想想咱俩还真是有缘。” 我：“可不是吗，咋俩小学同校，初中同班，高中同桌，大学同寝。”');
INSERT INTO `tpshop_joke` VALUES ('2127', '小明妈带着幼儿园的小明到女澡堂洗澡，进去看到了老师也在洗。 老师装做不认识小明，快步走进淋浴间。 小明大喊：“老师！老师！你脱光衣服我照样认识你。” 老师：“……”');
INSERT INTO `tpshop_joke` VALUES ('2128', '今天和老公下地去收玉米，由于天热老公脱了短袖光着膀子。 我看到后说：“老公我也热我也想脱衣服。” 老公不耐烦的说：“一个妇女家家的脱衣服有点女人的矜持吗？我这脱衣服可是为了劳动干活。” 我也没趣的说道：“男人脱衣服是为了干活，老娘脱衣服是为了快活。”');
INSERT INTO `tpshop_joke` VALUES ('2129', '总统在监狱里训话：“今天会举行一百米比赛，第一名和最后一名会被枪毙，其他人会被释放。” 犯人：“哦耶！” 总统：“好，下面第一对选手请准备。” 犯人：“我勒个去。”');
INSERT INTO `tpshop_joke` VALUES ('2130', '这个世界太神奇了，浙江富豪包养小三生下双胞胎大儿子不是自己的。 英国女子生下一黑一白双胞胎。 砖家说：“必须半个小时内跟两人发生关系才会有此种情况。” 这些女的也太疯狂了吧？');
INSERT INTO `tpshop_joke` VALUES ('2131', '刚从楼下听俩口子打架。。女的说。。。我真懒得骂你，你小我十六辈。。。男的说。。哪的话这是？？女的说 。。。你积了八辈子德才娶到我啊。。靠！那也才八辈啊！？？我倒了八辈子血霉嫁给你。。。');
INSERT INTO `tpshop_joke` VALUES ('2132', '‍‍昨晚跨年，同学点孔明灯祈福，上面写着“逢考必过”。 但一飞起来就卡在了大树上。 有人就说：“不吉利啊！今年必挂在高数上！”');
INSERT INTO `tpshop_joke` VALUES ('2133', '我们教室在四楼，今天在楼道内看风景，忽然看见化学老师在楼下经过，我想都没想直接吐了口唾沫，直接命中老师，只听老师一声残叫，我连忙把头缩了回去。就这样安静的过了一天。第二天，化学老师气冲冲的找到我，我怀疑有人告状，后来我同桌跟我说，化学老师把我的唾液采取了DAN.......唉！不说了！都是泪啊');
INSERT INTO `tpshop_joke` VALUES ('2134', '‍‍记得高中时住宿，有个同寝的女的一脸嫌弃的说：“你的胸怎么这么大，好恶心啊！” 我那时候听她说完还蛮自卑的。 可如今，我真想去她面前挺胸抬头的走一圈。 记忆中她是平胸…‍‍');
INSERT INTO `tpshop_joke` VALUES ('2135', '昨天和妈妈还有男友一起逛商场，老妈看上一件外套，决定要买导购员告诉老妈到那边刷卡，接着老妈看了下男友说那边刷卡，二货男友迷糊半天，噢的一声就跑去刷卡了，这时我想都没想就说，你怎么让我老公花钱啊，这是我老妈就吼，你花我老公钱的时候我说啥了，花你老公一次就不行啊，我...');
INSERT INTO `tpshop_joke` VALUES ('2136', '冬天接吻时要牢牢握住对方的手，防止他把冰冰的手塞到你的领子里。');
INSERT INTO `tpshop_joke` VALUES ('2137', '‍‍和一个女汉子处兄弟。 那天去她家玩，她去洗澡了，我就在浴室门口窗户抽烟。 然后她裸着就出来了，我们对望一眼。 她说：“没事吧！还是不是兄弟。” 我说：“没事，都是兄弟看一下没事吧！” 她说：“那你下面的小帐篷是几个意思？” ‍‍');
INSERT INTO `tpshop_joke` VALUES ('2138', '梦见我变成婴儿回到了妈妈的肚子里，突然脐带缠脖三圈越挣扎缠得越紧，即将窒息死亡之际。。。。妈个鸡！以后再也不插耳机听歌睡觉了！');
INSERT INTO `tpshop_joke` VALUES ('2139', '舍友不知从哪儿得来的偏方，洗头发时用蛋清当护发素结果水太热冲了一头蛋花。于是整个宿舍的人中午都没睡，一起帮她择蛋花~');
INSERT INTO `tpshop_joke` VALUES ('2140', '‍‍晚上，老公准点坐在电视机旁，等着看武媚娘传奇。 于是我看着老公说道：“你也喜欢看范冰冰呀！” 老公淡定的说道：“我不光看范冰冰，我连宫女都没放过！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2141', '一情侣散步，走到一条比较黑的小路，女友故意撒娇说：“会不会有坏人啊，我怕！”男友马上挺起胸膛说：“别怕，有哥在。”女友说：“哥都有些什么本事？”只听那货说：“哥跑的快。”');
INSERT INTO `tpshop_joke` VALUES ('2142', '一天，小明找大师算卦，大师看完手掌后说：小伙子，你一定很孤单吧！ 小明惊讶说：你怎么知道的？ 大师说：因为你手掌大！ 小明又说：为什么手掌大的人要孤单？ 大师回答道：因为越掌大越孤单。');
INSERT INTO `tpshop_joke` VALUES ('2143', '惬意的咖啡馆里，进来一个身穿西装，戴着墨镜，夹着公文包的人，一看就是个高管范儿！他走到我们旁边桌坐了下来，点了一杯咖啡，然后利索的拿出笔记本电脑，噼噼啪啪开始敲打键盘，两眼不离屏幕！老婆两眼放光的看着他：“这人一定是成功人士，你看多敬业，走到哪都工作！” 我心里那个不爽啊，突然从那货电脑里传出一声熟悉的声音：“抢地主。。。！');
INSERT INTO `tpshop_joke` VALUES ('2144', '那天去买内裤，问老板：“有没有四角的？”老板吼了一句：“我去，最便宜的进货价也要十块。”');
INSERT INTO `tpshop_joke` VALUES ('2145', '老公非常抠门，他经常出差，却从来没给我买过礼物，但这次他从北京出差回来后却说要送我点北京的礼物！我还以为他开窍了，正充满期待时，只见二货神秘兮兮的拿出他的充电宝，拿过我的手机插上，我正疑惑时，二货说要送我点北京的电！');
INSERT INTO `tpshop_joke` VALUES ('2146', '大慈大悲的活菩萨，我有罪！我今天在公交车门口手残把别人递过来让帮忙刷的公交卡给投进投币箱里了。。。');
INSERT INTO `tpshop_joke` VALUES ('2147', '刚女朋友给我打电话，问我：“在做什么？”不敢说我在打游戏，她不准我玩。没说几分钟，游戏就要开团了，我赶紧说：“亲爱滴，我的电话要没血了，过会儿冲了电给你打。”');
INSERT INTO `tpshop_joke` VALUES ('2148', '有一个姑娘，不小心被坏人骗到了山区当媳妇，姑娘给她的丈夫讲了一大堆道理，最后，姑娘说强扭的瓜不甜。。。丈夫想了想说：你说的都对，但本人就喜欢吃苦瓜！！！之后姑娘崩溃了。。。');
INSERT INTO `tpshop_joke` VALUES ('2149', '给女友打电话：“亲，发工资了晚上一起去嗨皮嗨皮吧？”女友：“好啊！刚好今天大姨妈要来，你太有口福了！”“对了，今晚要加班，过几天再约你。”“好吧，大姨妈从老家带来好多好吃的，我去找别人一起分享。”“逗你玩呢！今晚不加班……”“我也逗你玩呢，真的大姨妈来了！”');
INSERT INTO `tpshop_joke` VALUES ('2150', '刚捡到个5s，正想还给机主，结果。。。来条短信：我故意丢的手机，你千万别给老娘送回来，否则我男友要不给我买个6。你要是敢送回来，我TM告你偷我手机！');
INSERT INTO `tpshop_joke` VALUES ('2151', '发一个同事家熊孩子的事。有一天，小侄放学回家书包一扔，说不写作业，奶奶听了说不写打你，小侄只好乖乖就范，谁知刚写一会，笔一扔，刷的站起来自言自语道：不写了，让奶奶打去！');
INSERT INTO `tpshop_joke` VALUES ('2152', '公司出门口两边“通道尽头”都有洗手间，都是先经过女洗手间再到男的，今天有点急就匆匆忙忙地进去了，爽完之后才发现方向有点不对，打开门发现男士专用的尿兜不见了，然后我默默地退回去把门关了，现在正在找机会出来！');
INSERT INTO `tpshop_joke` VALUES ('2153', '人生得意须尽欢，过了山海关都是赵本山。问君能有几多愁，树上骑个猴，地下一个猴。众里寻他千百度，没病你就走两步。天苍苍，野茫茫，我十分想见赵宗祥。红酥手，黄藤酒，大爷，这个真没有。书中自有黄金屋，不是大款就火夫。安能摧眉折腰事权贵，反正十块钱儿的，都是你消费。');
INSERT INTO `tpshop_joke` VALUES ('2154', '我今年23了，出去拜年居然还得了压岁钱。我就说我都这么大了就不要了，结果我姑姑跟我说，没工作没结婚都是孩子，都有压岁钱。哎，亲姑啊，要不要当着这么多人的面揭露我失业单身屌丝狗的形象啊！');
INSERT INTO `tpshop_joke` VALUES ('2155', '　　昨晚闲着无聊，打电话给华莱士叫鸡。我:喂，华莱士吗，给我送一只鸡过来。华莱士:先生，请问你要可服务的还是不可服务的?我想这鸡还分可服务不可服务的了?就问:有区别吗?华莱士:可服务的100块，不可服务的10块。我:来只不可服务的吧这么多年以为我白活了似的，可服务的鸡难道还会自己走过来给你吃不成，还那么贵。别以为我没读过书就好骗，想骗我，没门。');
INSERT INTO `tpshop_joke` VALUES ('2156', '抢红包已经完全摧毁了我的价值观，现在超过1元我就觉得是一笔巨款，超过5元我就眼眶就湿了，超过10元我就窒息了，私信收个100的，跪的心都有了，2015勤俭节约之年，1分钱都来之不易啊...');
INSERT INTO `tpshop_joke` VALUES ('2157', '老妈一直嫌我懒，我每次都和她争论，最后她说一句，别人不知道我还不知道？我怀你的时候，你从来也不踢我，我还以为你死了！！');
INSERT INTO `tpshop_joke` VALUES ('2158', '再来一发，初中一个晚自习，全班安静的学习中。 突然后面“噗”的一声传来了一个响屁，大家顿时笑了，老师拍拍桌子，说“安静点！有什么好笑的啊，不就是放个屁嘛！” 老师话音未落，只听后面“噗~~~~~”传来了一声持续了20秒左右的屁，过程中全班诡异的安静，只听见屁在班里回荡的声音，完事的那一刻，全班爆发出哄笑。老师才回过神，默默地去开了门和讲台边上的窗户。。，');
INSERT INTO `tpshop_joke` VALUES ('2159', '昨天晚上把媳妇打了，打的很惨，打得她哭爹喊娘的。这么多年来，被压抑的情绪终于得到释放。痛快！这梦做的，都没敢跟媳妇说。');
INSERT INTO `tpshop_joke` VALUES ('2160', '转，法官正在审理一件夫妻出轨案件，听了双方的供词后，法官说,太太，照这样说来确实是你对你丈夫不忠。太太愤怒地喊道，不，是丈夫对我不诚实，他说出差一周，可是才三天就回来了。');
INSERT INTO `tpshop_joke` VALUES ('2161', '有一对夫妻，妻子负责买菜做饭。　　有一天，妻子单位有事，打电话让丈夫买菜。　　回家后，妻子嫌丈夫买的菜不好。　　她说：“你看看你买的菜什么样。”　　丈夫说：“我全是照着你的样买的。”');
INSERT INTO `tpshop_joke` VALUES ('2162', '在公交车上，一泼妇无理取闹，爸爸据理力争，在气势上压倒泼妇，最后她偃旗息鼓，不说话了。　　下车后，女儿说：爸爸，没想到你今天表现的这么勇敢！　　爸爸还沉浸在刚才的胜利之中，笑着对女儿说：知道爸爸厉害了吧？　　女儿问：可是，为什么在家里你面对妈妈，连一个屁也不敢放？　　被女儿质问之后，爸爸有点不自在，笑容僵住了，直直的瞪着她，反驳道：我放了！声小，你没听见！');
INSERT INTO `tpshop_joke` VALUES ('2163', '甲乙丙仨哥正谈自己买来一桶时的那些糗事。　　甲：我有一次买来一桶居然没有调料包，最终没办法只能就这么泡着吃了，没味啊！　　乙说：你这算啥，我那次买来一桶还没叉子呢，我只能干嚼了。　　丙说：你们俩那算啥，哥有一次买来一桶，调料包有，叉子也有，所以哥就大胆的泡啊，悲催的是他妈的桶漏水，面还没泡熟，开水漏完了，最后只有吃这种半生不熟的面了，你们有木有？');
INSERT INTO `tpshop_joke` VALUES ('2164', '天蓬元帅，风流倜傥，与七仙女打得火热，有望成为驸马。　　嫦娥妒，酒宴元帅，三十巡后，要借兴起舞，叫元帅帮取下外衣。　　该过程被吴刚录下视频，编辑后交给玉帝。　　玉帝大怒，扁其下凡。');
INSERT INTO `tpshop_joke` VALUES ('2165', '跳蚤找到一份新工作，去周扒皮家当女佣。双休不说，还包二餐饭。　　周末休息跳蚤回到家里，她老公发现跳蚤失常了。　　早上六点天还没有亮，跳蚤就学起了鸡叫。　　吃中午饭的时候，跳蚤居然饭也不吃使劲在那跳。　　她老公心疼地说：“老婆你这是做什么啊！是不是周扒皮欺负你了 ？”　　跳蚤说：“没什么，我学鸡叫是当闹钟。周扒皮说了吃饭前多跳跳，可以省粮食，跳累了，就去睡觉！”　　到了晚上，跳蚤还在那跳，她老公说：“老婆睡觉时间到了，咱睡觉去了好吗？”　　跳蚤说：“老公你先睡吧，再让我跳一会，我还不累。再说肚子饿想睡也睡不着啊！”');
INSERT INTO `tpshop_joke` VALUES ('2166', '很久很久以前，人们都不知道有小鸟这东西，除了一个人，那个人是谁？　　惊弓，因为“惊弓知鸟”');
INSERT INTO `tpshop_joke` VALUES ('2167', '十只小鸡在参军期间练习射击，猜猜有多少只打中靶子了？　　一只，因为“一鸡击中”');
INSERT INTO `tpshop_joke` VALUES ('2168', '高中管理森严，某个晚上深夜时分，一女生因不详原因跳楼自杀，尸体掉落校外。　　第二天整个学校处处谈论此事，经查，该女生违反校规如下：　　1.夜不归宿；　　2.未经允许私自出入学校；　　3.出入学校未配戴通行证；　　4.半夜不睡有扰他人休息；　　5.乱扔垃圾。');
INSERT INTO `tpshop_joke` VALUES ('2169', '一次，老师问小明：什么能发电？　　小明说：羊肉。　　老师说：为啥。　　小明说：那几天俺家没电，俺爸提了几斤羊肉去电力公司，俺家立刻就来电了。');
INSERT INTO `tpshop_joke` VALUES ('2170', '在课堂上，老师说：“女人有了孩子，女和子是好字，人人喜欢。良好的女人，女和良是娘字，要受尊敬。为了儿女辛苦一辈子的女人是婆婆，波和女是婆，脸上带波纹的女人......所以我们要爱护女性，尊重女性”　　突然一同学站起来说：“老师，女人干活怎么解释？”　　老师：“女和干是，是，啊......”老师说不出话来。　　这时笨嘴笑道：“女和干是奸字，所以我们更要爱护女人，不能让她们干活，因为女人一干活就是干坏事！”');
INSERT INTO `tpshop_joke` VALUES ('2171', '2007年我一哥哥学车，哥们儿脾气略爆，教练说话略难听偶带脏字为背景…学着学着教练把我哥们儿骂急了，哥们儿指着教练说：老丫挺得你给我下来，爷今天不抽你我谢字倒着写！教练老头也流弊：我教车四十几年你小子是第二个敢跟我动手的！后来进派出所了，我那哥们他爸来捞他，没错朋友们，教练认出他爸就是当年第一个……');
INSERT INTO `tpshop_joke` VALUES ('2172', '‍‍‍‍一天下课了，小明问小刚问题：“蒸一个馒头，需要几分钟？” 小刚说：“5分钟。” 小明说：“那五个呢？” 小刚自豪的说：“25分钟。” 小明说：“你傻啊，你难道不能一起蒸吗？” 小刚想想说：“小明，你吃一个苹果需要几分钟？” 小明说：“两分钟。” 小刚说：“那五个苹果呢？” 小明笑着说：“当然10分钟哦。” 小刚就拿出了五个大苹果，让小明10分钟吃完。 小明瞬间就傻掉了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2173', '女友临时有事，我独自送女友闺蜜回家。眼看到她家了，她突然扑上来，眼神迷离举止轻浮，在我耳旁轻轻吹气:“我想要你。”我良心过意不去最后婉言拒绝了，回家时，在街角见到女友:“亲爱的，刚刚是测试你，你合格啦！”听完我大松一口气，还好女友够傻找了个丑货来试探我。');
INSERT INTO `tpshop_joke` VALUES ('2174', '‍‍‍‍5岁的小明向妈妈告状：“我们的小狗把我的皮鞋咬破了。” 妈妈：“得好好惩罚它一下。” 小明：“我正是这样做的，我把它的狗粮都吃了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2175', '‍‍‍‍室友对我说：“你的这些歌太难听了，都能让人感到窒息，我快喘不过气来了，算了不特么听了。” 我说：“拉倒吧！这跟我的歌可没关系，谁让你你特么把耳机线一圈一圈的缠在脖子上，能不感到窒息吗，没勒死你算是好的。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2176', '‍‍‍‍今天去买菜。 我：“老板，这菜多少钱一斤？” 老板：“三块钱一斤，都是自己种的。” 我：“两块一斤卖不？” 老板：“两块我进都进不到。” 老板，我们能不能好好的交易了！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2177', '三个男人去天堂，色鬼、财鬼、同性恋。 天使对他们说，“去天堂路上，不管看到什么，能不能做有邪念之事，不然马上掉到地狱。” 三人刚走了几步，色鬼看到一个绝色美女勾引他，他实在忍不住去抓其胸，还没抓到，一下掉入地狱。 财鬼一看赶快往前走，突然前面路上出现一颗绝世钻石，财鬼实在忍不住弯腰去捡，还没碰到时立马反应过来，这是个考验。他马上起身，抵住诱惑向前走。 刚走几步，他转身一看，“咦，同性恋不见了…….”');
INSERT INTO `tpshop_joke` VALUES ('2178', '一女同学在社保大厅工作……割。一天一大妈来办退休，对我同学颇为礼貌地说：阿姨，给我办退休。可怜我那三十多岁的同学，红着脸接过了她的资料，一旁的同事抿着嘴尽量不笑出来。这还不是GC，GC的是，几分钟后，我同学办理完，把退休资料给大妈，说：小姑娘，你的退休手续办完了……同事们再忍不住了…………');
INSERT INTO `tpshop_joke` VALUES ('2179', '一次出去买东西，迎面看见了几年不见的同学和一女的抱一小孩，我:操什么时候结婚的，小孩都这么大了，长的真像你小子，两人脸色有点尴尬，很慌张，同学就干笑了几声，留了电话号码，我说你们忙去吧，我也有点事，同学:好的，电话联系，我刚转身走了几步就听见那女的说:姐夫现在怎么办！');
INSERT INTO `tpshop_joke` VALUES ('2180', '无意中捡到了阿拉丁神灯。灯神对我说，我可以满足你一个愿望，任何愿望。我说，那让我变成世界上最帅的男人。灯神看着我的脸，愣了。沉思良久，咬着牙默念了一段咒语。然后对我说：好了，你的愿望达成了，现在世界上只剩下你一个男人了。');
INSERT INTO `tpshop_joke` VALUES ('2181', '呆着没事在群里聊天，朋友甲说刚起来饿了去吃饭，朋友乙说去哪吃要去蹭饭，朋友甲说放马过来吧，朋友乙说这句俗语用的贴切正好我姓马，我嘴欠的说了句因为没有放狗过来的俗语。朋友乙当时就急了，要不是在群里非得打起来不可。');
INSERT INTO `tpshop_joke` VALUES ('2182', '都说男人只有穷一次，才知道哪个是真正的朋友，我特么的都不敢穷，因为我一穷，连一个假朋友都没了');
INSERT INTO `tpshop_joke` VALUES ('2183', '一朋友的奶奶从楼梯上摔下来了，摔住了腿，于是他带着他奶奶去医院看腿。医生说:你奶奶的腿啊，我那朋友说到:你骂谁呢。医生说:你妹，我还没说完呢， 我是说你奶奶的腿没事，。');
INSERT INTO `tpshop_joke` VALUES ('2184', 'lz小女子，昨天老爹从外地回家奖励我两千块钱，原因就是给我打电话的时候看到来电显示上是：宇宙无敌超级帅粑粑！要知道，在他回家的前一天还是：宇宙无敌超级臭粑粑呢，一字之差，，赚到了哎～么么哒么么哒');
INSERT INTO `tpshop_joke` VALUES ('2185', '下午汇款中，我写上我的汇款人名字，望宁，那小妹妹职员非要看我身份证，说不行有姓望的，好吧，我们姓望的确实不多，，');
INSERT INTO `tpshop_joke` VALUES ('2186', '那年俺遭遇入室抢劫，被劫走28块5毛钱不说，劫匪还用日语骂俺。从那以后俺就立下鸿鹄之志，刻苦学习日语，希望有朝一日再遇劫匪能扳回一局。主任：这就是你上班看岛国片的原因？');
INSERT INTO `tpshop_joke` VALUES ('2187', '在一个校门口看到一个标语在那带尖的大门上焊着：男生翻，男生变女生，女生翻，女生变女人。 我敢说这学校的学生再也不敢翻墙去网吧了！');
INSERT INTO `tpshop_joke` VALUES ('2188', '今天去饭堂吃饭遇到好人了!因为去的比较迟，所以菜剩下的不多了，我就点了个丝瓜煮瘦肉，阿姨很好人地说：“这个菜凉了，换个别的吧！”于是我就乖乖的换了。等我进嘴才发现，这菜还是特么凉的，终究我相信饭堂里的都是骗人的!');
INSERT INTO `tpshop_joke` VALUES ('2189', '今天上午做一空调公交车，我前面有一大妈排在我前面，刚上去司机说就说2块，大妈来了一句是凉快，就往后面走了，司机看着大妈说:钱投2块，大妈来了一句前头没有后头凉快，司机火了大声说道:做公交车钱投两块，大妈又来一句我做国家的车你管我做前头凉块还是做后头凉快，我舒服就行，结果全车人员忍不住哄堂大笑。');
INSERT INTO `tpshop_joke` VALUES ('2190', '邻居一岁半的孩子自己玩儿，我跟他妈妈在旁边摘菜聊天，孩子摔倒了，疼得哇哇大哭，他妈妈哄不好，一直哭。我过来了大惊小怪的喊：“哎呀，你把地砸了一个大坑。”孩子一骨碌爬起来，转着圈找他砸的坑，连哭都忘了。');
INSERT INTO `tpshop_joke` VALUES ('2191', '医生，我多吃胡萝卜，视力真的会有改善吗？　　当然，您什么时候看到过兔子有戴眼镜的？');
INSERT INTO `tpshop_joke` VALUES ('2192', '一位美国人到南美洲某国旅行，感觉肚子很痛，就问当地的人有没有一间可靠的诊疗所。　　当地人回答他：这很难说。但在这儿有政府规定，当某间诊疗所的医生医死一位病人时，那间诊疗所外一定要高挂一个汽球。　　美国人听后就寻找整个城里的诊疗所。　　他看到第一间外高挂二十个汽球就找下一间；第二间外挂有十个汽球，他摇摇头再找。终于他找到一间o挂有五个汽球，就走进诊疗所要救诊断。　　这时有一名医生从后面出来跟他说：先生，您需要等很长时间，我这间诊疗所昨天才开张，忙得很，人手不够！');
INSERT INTO `tpshop_joke` VALUES ('2193', '某男拿女医生所开处方转了半天回来问：13超到底在哪？　　女医生笑曰：不是13超，是B超。　　男大怒曰：靠！你的B分得也太开了！');
INSERT INTO `tpshop_joke` VALUES ('2194', '一位兽医有事要外出，出门前交代他的助手，记得喂诊所内一匹受伤的马服药，他说：你只要拿根管子放入马的嘴巴，再将药丸放入管内，然后对着管子吹气就可以了。说完便放心的离开。不久兽医回来竟然发现助手病奄奄得躺在地上。医师问：发生了什么事？助手回答说：我没料想到那马吹气吹的比我还快！&nbsp;');
INSERT INTO `tpshop_joke` VALUES ('2195', '病人去做体检，大夫用他常人难辩的字迹开了张处方，病人把处方揣进袋里，忘了去拿药。有两年的时间，他每天早晨把处方当作铁路通行证出示给检票员；还用它进了两次电影院，一次棒球场和一次交响音乐会；用它冒充老板的字条得到一次提升。一天，这个人把处方弄丢了，他的女儿捡到后，在钢琴上照其演奏，结果获得了进公立音乐学院的机会。');
INSERT INTO `tpshop_joke` VALUES ('2196', '一只蜗牛正在路上行进，结果後面来了一只乌龟从他身上辗了过去。後来蜗牛被送医急救。当蜗牛神智恢复清醒後警察人员问他当时情况。蜗牛回答说：我不记得了，一切都太快了……');
INSERT INTO `tpshop_joke` VALUES ('2197', '一个人把他的狗带到兽医处说：把这只狗的尾巴切掉。兽医检查了狗的尾巴后说：它什么事也没有，你为什么要这样做？那个人回答说：我的岳母要来我家，我不想家里有什么东西。让她觉得自已是受欢迎的。');
INSERT INTO `tpshop_joke` VALUES ('2198', '医生告诉一个男人，他还有六个月的生命，于是他决定搬去和他的岳母住。因为和岳母一起住六个月，就像是几生几世一样。');
INSERT INTO `tpshop_joke` VALUES ('2199', '脱衣女郎：干我们这一行，看来病的时候最不划算！　　妇科医生：怎么说？　　脱衣女郎：平常我们脱给客人看，还有钱好拿，在这脱给你看，不但没有钱拿，还得给你付钱。　　妇科医生：……');
INSERT INTO `tpshop_joke` VALUES ('2200', '楚阳向生病住院，室友与他聊了起来。　　室友：你因为什么住院？　　楚阳向：扁桃腺手术，明天就要做了，真有些害怕。　　室友：没什么可怕的，我一年级时做的扁桃腺手术，第二天就照样儿吃冰激凌了。　　楚阳向：是吗？那你因为什么住院？　　室友：做包皮切除手术。　　楚阳向：噢！那太可怕啦！我刚一出生就做了包皮切除手术，直到一年后我才能走路！');
INSERT INTO `tpshop_joke` VALUES ('2201', '如果你的手机号最后3位数是你以后的住房面积。你的是__________。007的我已经哭晕在厕所。。。');
INSERT INTO `tpshop_joke` VALUES ('2202', '早上我妈妈给宝贝弟弟换尿片，没有小号的尿片了，结果我那二货老爸就拿我们用的卫生棉说这个，这个小号的，我当场笑喷了');
INSERT INTO `tpshop_joke` VALUES ('2203', '女人:上帝啊，请赐给我一个疼我爱我，懂得照顾 我，体谅我，会关心我，懂得我的感情，一生 一世对我不离不弃，永远不会变心的帅哥吧！男人:上帝啊，请赐给我一个大波妹子吧！上帝:唉，还是男人知足啊！');
INSERT INTO `tpshop_joke` VALUES ('2204', '一次和老公散步，经过一家药店，门口放了一案例秤，就站了上去，发现重了一斤，就怀疑是我的鞋子太重了，于是我把鞋脱了再秤，可是案例秤显示的数字还是和没脱鞋时一摸一样。我就冲店里说：老板，你们店的秤不准 老公一听，赶忙阻止我：别喊了，还不嫌丢人啊？你把鞋提在手里和穿在脚上，有区别吗？!');
INSERT INTO `tpshop_joke` VALUES ('2205', '一天,一个老尼姑觉得身体不适,于是就叫个小尼姑拿着她的尿液样本去医院检查。不巧半路被个妇女给撞了下，尿全部洒在了地上。小尼姑不知道怎么办。那个妇人说：“不就是尿嘛，我赔你点就是了。”小尼姑一想也是，爽快的说：“好”！等到检验报告出来的时候，竟验出老尼姑怀孕了！于是老尼姑仰天长叹：“这年头动物靠不住，连青菜都靠不住了吗？”');
INSERT INTO `tpshop_joke` VALUES ('2206', '今天见一宝马在一路口拐弯，一乞丐见机立即躺车下。乞丐心想，这下要发了。只见从车上下来一高富帅，看了一眼躺地上的人。拿出包软黄鹤楼，抽出一根点上火，吧嗒吧嗒抽了一口。对地上的乞丐说，看你风里来雨里去的也不容易，一日三餐都没着落。我帮你一把吧。只见他拿出电话拨了个号码，一接通就说，喂老爸，给我卡里打50万。 我特麽要碾死躺地上的这王八蛋，看特麽要钱还是要命。');
INSERT INTO `tpshop_joke` VALUES ('2207', '有一晚上回家看到媳妇和一个裸着身的男人在床上，当时我就脑子充气了，立马过去一巴掌把媳妇拍醒：丫的天气这么冷也不知道给儿子盖个被子！');
INSERT INTO `tpshop_joke` VALUES ('2208', '老爸喝多了，老妈准备了解酒汤，但是老爸死活不喝。看他大醉的样子我就端起水杯来说:大哥，小妹敬你一杯！老爸:妹子，哥先干为敬。一口气喝完了解酒汤。。。。');
INSERT INTO `tpshop_joke` VALUES ('2209', '　　以前不知道什么叫缘分，直到有一天，缘分找上了我，我才体会到什么是缘分。（本人男，单身，当年在学校是出了名的花心，但是我自己并不认为我是一个花心的人，耍朋友本来都有分有合，毕业后自己忙于找工作，所以就没交女朋友，家人都很着急，到处托人帮我介绍一个，突然有一天，我表哥打电话给我说要给我介绍一个，我就觉得见一面也无妨，直到见面的一刻我才追悔莫及，那女的竟然是我们高中时的同学，我们男生私下喊的女汉子内型的人，我准备转身离开，假装不认识，没想到，她喊了我，没办法，我只好坐了下来，见她傍边坐着一个虎头虎脑的小子，这时她开口了，这一句话改变了我的整个人生：“这是你的儿子.\"”你们玩笑开大了吧，呵呵。”“他真是你的儿子，毕业那年晚会上，你喝多了，碰见了我，我就把你搀扶回寝室，趁着酒劲我向你表达了爱意，你接受了我，然后我就怀上了他，这些年见你一直奔波，忙于工作，我不想让你多负担，所以没有找你。家人一直逼我，我还是坚持把他养大了，他真是 你的儿子。”我当时脑袋都炸掉了，这么多年我是真不知道有没有那么一段历史，我该怎么办？接受？不接受？不认账？认账?...)');
INSERT INTO `tpshop_joke` VALUES ('2210', '苦逼售楼员一枚……今天在上班正常站位接待客户……咯咯咯咯咯………突然来了一个客户进门后直接无视我，用巨大的声音说了一句:我要找售楼小姐。当时我心情正不爽时我们一个土肥圆妹子同时站起来说:我就是售楼小姐！客户看了看她，又看了看我…说我还是找售楼先生吧……妹子的小脸都绿了……');
INSERT INTO `tpshop_joke` VALUES ('2211', '‍‍‍‍还记得每次大扫除，班主任都会亲切的说道：“学校是你家，清洁靠大家。” 可是当我们睡觉的时候，老师手上的武功秘籍立马砸过来：“你给我站起来，你以为学校是你家啊！” 哎，不说了，现在还是站着的！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2212', '‍‍‍‍一天，晚上，面对女友做的饭菜一点食欲都没有。 我小心翼翼地说：“不行到外面吃去吧？” 还是女友爽快，这不我就从走廊吃上了！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2213', '晚上蚊子老喝我的血，我就把风扇开到最大，虽然我把风扇开到最大蚊子还是要喝我的血，但老子要让它们抗着大风飞过来喝，累死它们，让它们知道知道不付出就没回报。');
INSERT INTO `tpshop_joke` VALUES ('2214', '‍‍‍‍昨晚和老婆在办事，突然有一只苍蝇飞过来。 只听着二货说：“悟空，是你吗？快想办法就为师出去，被他折磨的没有力气了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2215', '回想这几年，尝尽社会的辛酸艰难，从一开始什么都没有到30万，从30万到200万，从200万到800万，从800万到1000多万，最后从1000万到现在的1300万！不是炫耀，我只是想通过我自己的经历告诉我的朋们……… 手机像素越高，拍照越清晰！');
INSERT INTO `tpshop_joke` VALUES ('2216', '‍‍‍‍姐姐带外甥女去动物园。 姐姐说：“看猴子的屁股是红色的。” 小家伙担心的看了一看说：“是小猴子不听话，让它妈妈给打的吗？” 姐姐：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2217', '一女人整容过后焕然一新，她口渴去超市买了一瓶水，问收银员你猜我多大了？收银员说32，女人高兴坏了，说到我今年47啦。然后又去肯德基买全家桶，忍不住问前面排队的人，不好意思，请问一下，你猜我今年多大了，排队人说到，我猜35，女人说到我今年47啦，买完来一桶，在路上边吃边唱歌，看见一老头，便又忍不住问道，不好意思，你能猜猜我今年多大了吗？，老头说到任何女人，我只要摸一摸胸就知道她多大，女人觉得好神奇，想到旁边没什么人，就说你摸摸看猜猜我多大，老头左摸摸，右摸摸，还揉了几下，说到，你今年47，女人说，真神奇，你怎么知道我47了？老头说，因为肯德基店，我排你后面！');
INSERT INTO `tpshop_joke` VALUES ('2218', '‍‍‍‍一哥们翘课，来晚了被老师抓到了。 老师：“你怎么来这么晚，是不是翘课了。” 哥们：“我去上厕所了，纸掉进坑里了，所以来晚了。” 老师：“那你怎么出来了？” 哥们：“我刚好碰到一个老师，问老师借了点纸。” 老师：“男的女的？” 哥们：“女的。” 老师：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2219', '大家都知道有名的 t f boys，但你知道这个名称的由来吗。 曾经有一天，王俊凯在做英语判断题时，写完后发现答案是 t f t f,读起来很顺口。他灵机一动，就想到了一个团队名称 ，于是乎，他就和两个小伙伴一起组成了');
INSERT INTO `tpshop_joke` VALUES ('2220', '我1万本金起家，抓住2009年大涨的机会，资产干到5万，2010年到2011年没怎么赚到钱，5万勉强干到12万，2011年开始爆发，先是重仓华谊兄弟，第一只10倍股，12万变成120万然后在2013年6月份8块钱又重仓上海钢联，到春节涨到76的时候抛出，这个时候资产变成1000万，然后接连买了粤传媒，大富科技，全通教育三只翻倍股，资产接近8000万，去年券商那波行情也抓住了，翻了3倍，资产干到接近2亿，在创业板去年回调的时候2亿全部买入东方财富，之后又是3倍，现在接近6个亿，妈的，现在炒股完全没有成就感，买啥都赚钱！哎呀~ 不说了，医生喊吃饭了。精神病院的饭越吃越精神~');
INSERT INTO `tpshop_joke` VALUES ('2221', '静静：吃饱想去散个步，没男人陪都不好意思去了。 丝丝：一个人去等弓虽女干啊，很刺激的。 静静：为了等弓虽女干，我特么走了多少黑暗小巷，楞是没成功，包倒被抢走几个。 丝丝：我特么更惨，裤子都脱了，抬起头来劫匪又帮我穿上！');
INSERT INTO `tpshop_joke` VALUES ('2222', '一天老师怒气冲冲的去找校长，老师：我们班的小明我实在是受不了了，我要开除他。 校长：毕竟是个孩子吗，给他个机会吧，你开除了他，他家长会很难过的。 老师：我要是他家长，我早就被他气死了。 校长：那你的意思是，我现在站在这里是个奇迹？');
INSERT INTO `tpshop_joke` VALUES ('2223', '老师：男生和女生有什么不同？ 小明：比上不足比下有余。 老师：滚出去！！');
INSERT INTO `tpshop_joke` VALUES ('2224', '我一直不明白男友为什么要跟我分手，今天终于忍不住了打电话过去问他，他沉默了一会儿，说：“我怀了你的孩子！” 我听了很震惊，冷静思考了一下，问他：“可是你是男的啊！” 他歇斯底里地嚎叫道：“你看你，还是这么不信任我！这就是我们分手的原因！”');
INSERT INTO `tpshop_joke` VALUES ('2225', '“到底是吃鱼呢，还是吃鸡呢？还是吃鸡吧。”女孩娇嗔着自言自语。 食堂阿姨斜了女孩一眼，给她打了一根香肠。');
INSERT INTO `tpshop_joke` VALUES ('2226', '晚上和老婆躺床上，老婆：老公，好久没有听你说过情话了，今天说一句艺术点的的情话哄哄我。 老公：你就是艺术，而我是专门搞艺术的。');
INSERT INTO `tpshop_joke` VALUES ('2227', '今天陪老婆去妇幼检查身体，独自来到收费处拿出自己的医保卡递给对方：“挂个妇科！” 收银员看了看回了一句：“泰国回来的？” 我：......');
INSERT INTO `tpshop_joke` VALUES ('2228', '一个班里新来了两个童鞋，男同童鞋先做自我介绍：我叫尤勇，我喜欢下棋。 接下来是女童鞋，她羞涩地走上讲台说：我叫夏琪，我喜欢游泳。 顿时全班轰动……');
INSERT INTO `tpshop_joke` VALUES ('2229', '工友们大澡堂洗澡，互相取笑。 “明哥，吃什么吃的？本钱有点大喔，哈哈。” “唉，憋了几十年了，憋大的。” “可惜长了一副对不起它的脸。” “滚。”');
INSERT INTO `tpshop_joke` VALUES ('2230', '每天吃晚饭，老妈给我勺的饭，我经常说：“妈，少盛点！” 每次照旧我妈回答说：“好。” 然后，我在背后眼睁睁的看着她把一大碗饭压到半碗的体积！');
INSERT INTO `tpshop_joke` VALUES ('2231', '上午一个小姑娘来面试。我问她，怎么称呼。她颤颤巍巍地说：我姓白，红橙黄绿青蓝紫的白。我苦笑着应道：欢迎你，我姓马，生龙活虎的马。');
INSERT INTO `tpshop_joke` VALUES ('2232', '‍‍‍‍高三时候，有一次晚自习，英语老师在讲课。 一同学带着耳机听音乐，听着听着睡着了，还打呼噜。 老师看见了，一声不响，也不让我们叫醒他。 我们心想：“老师真是体谅学生啊！” 下课了，老师给我们打手势，示意我们快点走，小点声，不要吵醒那位同学。 然后把灯关了，就让他一个人待在空荡荡、黑漆漆的教室里睡觉。 据说那位同学醒来后，第一反应是：“我靠，我TM穿越了！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2233', '经常听说有孕妇老人坐公交没人给让座 于是我心里一直在想怎么惩罚他们一下 割～～一次和朋友一起坐公交车 坐到我们要到的站 我假装慌张的喊了一声快跑 结果一车人不管三七二十一都跑下了车 下来以后都不知道发生了什么事 司机师傅更是一脸的茫然');
INSERT INTO `tpshop_joke` VALUES ('2234', '刚看有人说看左耳激情戏，有人叹气的，我也来个，12年在无锡，跟那时的女票去看泰坦尼克号3D，中间不是杰克帮露丝画画，露丝衣服刚脱到一帮转镜头了，只听见一个男的很惊讶的“诶”了一声，紧接着他对象就来个诶你妹吖，怪我们笑点太低，当时全大厅的人都笑了……');
INSERT INTO `tpshop_joke` VALUES ('2235', '昨天老公过生日喝多了，走进小区就说我要尿尿，对着小区的路灯边说边尿：我要把你浇灭。刚尿完路灯到了熄灯的时间自动灭了。于是老公就挨个给他朋友说我尿尿把路灯浇灭了……唉，不说了……我接个电话先……');
INSERT INTO `tpshop_joke` VALUES ('2236', '午间休息，同事聚在一起吹牛逼，话题是自己坐过最贵的车。a说自己开过八十多万的宝马，b说坐过百八十万的房车，c说坐过两百多万的宾利，然后看向做学徒的老公问：小吕，你呢？老公说：我坐的那车多少钱我不清楚，大概18吨，咱部队叫它重型装甲……然后大家开始讨论中午的菜好不好吃！都不能和他愉快的装逼了~~~真替他的交际担心！');
INSERT INTO `tpshop_joke` VALUES ('2237', 'lz苦逼打工狗一枚，在外租房子，平房，厕所公用的那种，。半夜，被尿憋醒了，由于上厕所还得跑出去一趟，lz比较懒，索性拿着半瓶没喝完的康师傅冰红茶尿了进去。。。今早上一哥们来我家玩，玩着玩着口渴了要喝水，平时我和这哥们关系特铁，也不爱讲究那些，我喝过他喝的水吃过他吃过的东西，他也喝过我喝过的水也吃过我吃过的东西，也就没那么多讲究，想都没想直接拿着昨晚的冰红茶咕噜咕噜猛灌了半瓶，等我洗了衣服后走进卧室，这货玩着反恐精英跟我说冰红茶味儿怎么不对劲，是不是放久了，握勒割草。。。我敢跟他说这是昨晚我那啥了的么？不匿，他又不看这坨。。。');
INSERT INTO `tpshop_joke` VALUES ('2238', '本人有个逗逼表弟，他刚当学徒时，师傅给他一百块让他去帮忙买东西，老板找回来七十多块。\r\n回来后把东西拿给师傅，师傅一直用奇怪的眼神看着他，不过没说什么，他也觉得有点疑惑。\r\n到了晚上，他才突然发现：妈蛋！那七十多块还在口袋里！～');
INSERT INTO `tpshop_joke` VALUES ('2239', '一天俺家那位对我说，你知道么其实我们学校有韩国人哎，我说，正常啊交换生吧，女神又说了，那天我看到校园里有两个人带着口罩，我一眼就看出了她俩与众不同的气质，和他们一起进去超市听见她们说的话都听不懂.那我就说，那有可能是外地人啊，二货妞又说了，不是一定是韩国的，我很清楚的听到有个女生说了句（呐尼）...哦，那也许是韩国的吧，哎，等等，这好像是日语吧，说完后我俩相视一笑，二货真是伤不起啊');
INSERT INTO `tpshop_joke` VALUES ('2240', '今早上班的路上，看到一群小屁孩。稍微大一点女孩在用花洒浇花，这时一个小男孩奶声奶气的说：“姐姐，你撒我身上了！”但他姐姐估计没有看听到，就没吭声。小男孩又说了一遍，他姐姐说：“哦，对不起”！小男孩又奶生奶气说：“没关系”。让在旁边经过的我瞬间感觉萌萌哒！');
INSERT INTO `tpshop_joke` VALUES ('2241', '昨晚一个同事喝醉了，我们几个一起去送，到了他家，他媳妇一开门，他:“这位小姐好面善，好像哪见过，对了你是宾馆的小翠吧”。不好他媳妇脸绿了。我们敢紧把他拉到客厅。他说不要栏我，我去打个电话，说着就进了卫生间，一会出来给我们说:没事今天放心在这玩，我刚打电话给家里说了，今天单位加班。奥对了刚才那位小姐呢，...');
INSERT INTO `tpshop_joke` VALUES ('2242', '‍‍过年了，喝酒就别骑摩托了。 昨天喝多了，刚坐上摩托，就被一个交警拽了下来！ 于是我就急了，骂道：“怎么地！我不开，坐会还不行呀！” 交警激动的说道：“坐你自己的去！别坐我摩托！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2243', '甲：如今这天气，真像是到了世界末日，让人透不过气来！你有什么高招？\r\n　　乙：方法之一，在房间内洒点水降温。\r\n　　方法之二：开空调。\r\n　　方法之三：在房间内，不穿衣服。\r\n　　方法之四：穿黄裤，戴绿帽，都混到这步田地，心还不凉了半截？心凉，自然凉。\r\n　　方法之五：戴绿帽、穿破鞋，从头凉到脚。');
INSERT INTO `tpshop_joke` VALUES ('2244', '在伦敦奥运会上，有外媒记者质问中国代表团：你们的选手能游那么快，肯定有服用兴奋剂嫌疑！\r\n　　正在被询质人员不知如何回答时，只见一人慢慢站起来慢条斯理地回答：如果中国真有那么牛叉的兴奋药，那我早就吃了。\r\n　　众人定晴一看，原来他是中国男足队员。');
INSERT INTO `tpshop_joke` VALUES ('2245', '只要你发的笑话好笑过了百，或者进了热门，都会感到自己要出名了吧。\r\n　　但其实……我们根本不看作者。');
INSERT INTO `tpshop_joke` VALUES ('2246', '刚才不小心看到了某条“不转死全家”的贴子……犹豫了一下还是坐在电脑椅上转了一圈……挺方便的……还缓解了疲劳……');
INSERT INTO `tpshop_joke` VALUES ('2247', '中午吃饭，忽然一领导冒一句：“女人生完孩子就没意义了。”\r\n　　接着旁边新入职女同事悠悠的来一句：“那你妈还活着吗？”…');
INSERT INTO `tpshop_joke` VALUES ('2248', '上届奥运会，王楠送给福原爱一块玉佩，辟邪的。\r\n　　小爱特开心的问：楠姐我带着这个是不是抽签就不会抽到你了啊？\r\n　　王楠一脸黑线：是呀，记者在旁边都笑翻了。\r\n　　然后……小爱带着玉佩，抽到了张怡宁……');
INSERT INTO `tpshop_joke` VALUES ('2249', '厂长在和外商谈判时，外商鼻子发痒，打了个喷嚏，恰巧身边的翻译也鼻子痒，跟着也打了个。\r\n　　厂长不高兴地说：这个不用翻译，我听的懂！');
INSERT INTO `tpshop_joke` VALUES ('2250', '本人男，家中独子，刚结婚。饭后我们一家人讨论生孩子的事情。\r\n　　爸爸说：生男孩好。\r\n　　老婆拉着我的手：恩，那我们争取生男孩。\r\n　　妈妈说：生女孩好。\r\n　　老婆：好，我们争取生女孩。\r\n　　我说：好什么好，生两个才不孤单，你看我，连个亲妹妹都没有。\r\n　　老婆深情地摸摸我的头，说：老公乖，那我争取给你生个亲妹妹昂……　　\r\n　　全家对视五秒钟后，彻底凌乱了！');
INSERT INTO `tpshop_joke` VALUES ('2251', '　　有一年我儿子得了大病，当时情况十分危急，心急如焚啊，岂知当天晚上做了一个梦，梦里面天使对我说：“如果想让这个小孩活下去，可以用他父亲的命来换，你愿意吗？”我回答说：“当然了~”第二天我儿子的病真的就好了，我也什么事情都没有，结果，当天晚上，隔壁老王就死了，我突然意识到什么。。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('2252', '刚刚去亲戚家熊孩子要我讲故事，我顺手抄起一本儿童问答图册就问：“东汉末年的三国是那三个？”熊孩子很快回答：“烽火、连天和不休。”然后我在那愣了四秒。。。');
INSERT INTO `tpshop_joke` VALUES ('2253', '闺蜜：你男友厉害不？女 ：莔。你得呢？闺蜜：昔。女 ：额。。。 没点文化连段子都看不懂');
INSERT INTO `tpshop_joke` VALUES ('2254', '同学手机丢了，舍不得内存卡存储的东西，就让我发短信:手机给你了，能把内存卡还给我吗？本来想着对方不会回，谁知道一会回过来了:你的手机也就内存卡里的东西还值点钱……');
INSERT INTO `tpshop_joke` VALUES ('2255', '一个科普贴:不一定每个鸡蛋都可以孵出小鸡，而不能孵出小鸡的鸡蛋其实是未受精的蛋，也就是母鸡的大姨妈，所以我们吃了很多年的大姨妈啊。。');
INSERT INTO `tpshop_joke` VALUES ('2256', '　　一次放长假去女朋友家见家长，我当时很紧张，女朋友为了缓解我的压力，于是对我说就当作回到自己的家里一样，没什么区别~~~到了女朋友家里之后，刚见到他爸妈，我脱口而出就是一句：“爸妈，我回来了，这是我的女朋友~~~”');
INSERT INTO `tpshop_joke` VALUES ('2257', '朋友们在我家玩斗地主到了凌晨02：28，老婆睡醒了迷迷糊糊的看了下手机，急忙跑过来跟我说：“老公，我手机不知道怎了，0跑到2前面了！”说着，拿起我手机看了看，特别迷茫地说：“怎么你的也是啊!!!” 我该告诉她真相呢，还是该告诉朋友她只是睡蒙呢？');
INSERT INTO `tpshop_joke` VALUES ('2258', '请朋友们到家里吃饭。自己烧的。吃后都说难吃。。。我对着菜谱使劲的研究一番后。果断再请他们前来。。这次大家都吃的开心。并且都吃完了。。临走时一朋友说。你大爷的。五。六个人你他吗的就烧一个菜');
INSERT INTO `tpshop_joke` VALUES ('2259', '开学第一堂课，老师：“请用峰回路转造句。” 小明：“我最大的梦想，就是发一条微博后，峰回路转！” 老师不解：“什么意思？” 小明：“就是李易峰回复我鹿晗转发我啊！” 老师：“你给我滚出去！！！”');
INSERT INTO `tpshop_joke` VALUES ('2260', '放学后几个同学一起出去玩儿，放松一下，一哥们儿说：“上课太无聊了，你们是什么状态？”两个哥们分别说道：“就像看日剧没有字幕。”“老师一上来就对我们发动了眩晕效果。”这时候另一个哥们儿说道：“我整整盯了那只在教室飞来飞去的蚊子一节课~~”');
INSERT INTO `tpshop_joke` VALUES ('2261', '一对夫妇吵架。 女（怒）：你不走，我走！ 男（吼）：你要是再敢迈一步，wifi可就断了！ 那女的愣愣地站在原地。');
INSERT INTO `tpshop_joke` VALUES ('2262', '‍‍‍‍话说长训，都是第一次上路的新手比较紧张。 教练让一个女学员换挡，结果那大姐摸档杆，在教练的腿上摸了好几把。 教练都急了，大骂：“让你换档，你摸我干啥，摸我能好使啊！” 哥就在车上坐着，一车人都笑尿了！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2263', '早上，我还没起来，看到媳妇在一边乐滋滋的在化妆，问她今天为啥这么开心，答曰：瘦了，牛仔裤穿上去很轻松了！等我起床一看，我的牛仔裤不见了。');
INSERT INTO `tpshop_joke` VALUES ('2264', '盆友‍‍‍‍：“既然你们关系那么好，为什么不结婚呢？” 女友：“唉，别提了，每次他喝醉了，我不想嫁给他，可每次他酒醒了，就又不肯娶我了！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2265', '小时候，我去欺负一个小朋友。那个小朋友，哭了。 大声喊着：“你等着，我叫我哥来。”我说：“好。我等着。” 五分钟后，小朋友哭着过来了：“我哥不在家。” 我被小朋友的认真感动了！于是我又打了他一顿！');
INSERT INTO `tpshop_joke` VALUES ('2266', '‍‍‍‍办事途中，见一大爷在路边酷酷的侧卧着，安静、慈祥、大气…… 忍不住问大爷：“大爷您在干嘛？” 大爷：“我在沉思。” 我：“大爷，您在思考什么哲理？能不能跟我分享一下？” 大爷：“我在想，这么好的天气，你个破孩子不去泡姑娘，跟我逗什么闷子。” 我：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2267', '母亲节，妈说儿女是爸妈的财富，爸说那是自然，卖儿卖女能挣不少钱呢。。。这爸。。。');
INSERT INTO `tpshop_joke` VALUES ('2268', '‍‍‍‍和男票散步。 男票问我：“你喜欢猫还是狗？” 我：“我都喜欢。” 男票：“只能选一个！” 我犹豫了一会，说：“那我选狗吧！” 男：“汪！汪！汪汪汪……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2269', '‍‍‍‍‍‍‍‍昨天在超市，来到一堆奶糖的货架旁，准备买几斤奶糖。 这时一大娘过来问我：“这奶糖甜吗？” 我回答：“你剥一个尝尝不就知道了。” 大娘就剥了一枚放在嘴里尝。 这时售货员看见了就喊：“这不能尝的，的罚款。” 大娘着急了，忙从嘴里吐出这粒糖，用糖纸包好说：“我只是尝一下，还没吃完，还给你们。” 说着就将那粒糖，扔进了糖堆。 我和售货员凌乱了。‍‍‍‍‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2270', '　　家有3岁小萝莉一枚，如今在上幼儿园，每天都是我接送，虽然辛苦，但是我知道这是一个男人的职责，只要家人好好的，辛苦点又能算什么。 　　今天我一往既如的送小崽来小学，尽然有种恋恋不舍的感觉，就忍不住回头多忘了几眼，还别说那新来的老师还真性感！！！');
INSERT INTO `tpshop_joke` VALUES ('2271', '昨天在楼道里听到一个女生打电话:”刚开始你把我当氧气，后来当空气，再后来当二氧化碳，现在当一氧化碳，你什么意思？');
INSERT INTO `tpshop_joke` VALUES ('2272', '从前有个和尚临死时嘴里一直念着莎士比亚，当时还没有此人，后来一位学者一直在研究，终于知道和尚临死时念的是什么。');
INSERT INTO `tpshop_joke` VALUES ('2273', '李逵问道：哥哥，你因何唤做宋江？宋江道：我爹姓江，我娘姓宋，他二人未曾读过私塾，胸无点墨，只得把二人姓氏做一块儿权当我的名字。我爹是招赘到我母亲家的上门女婿，因此我随我娘亲的姓。话说，我娘亲天生一副好嗓子，唱起小曲堪比李师师呢~~（转）看半天才明白，很内涵很隐晦');
INSERT INTO `tpshop_joke` VALUES ('2274', '因为部分线路的车辆新增装有空调，所以票价是两块，结果大妈上来投了一块。\r\n司机：两块啊。\r\n大妈：凉快！\r\n司机无奈又说：投两块！\r\n大妈笑着说：不光头凉快，浑身都凉快。\r\n说完大妈往车厢后头走，司机说：钱投两块。\r\n大妈说：后头人少，更凉快。\r\n司机竟无言以对。。。');
INSERT INTO `tpshop_joke` VALUES ('2275', '深夜，某男来到小商店：“老板，来瓶二锅头。”\r\n老板：“20”\r\n男子：“上次来还是10块，怎么？”\r\n老板：“那是白天，三更半夜的，都卖20，那10块是辛苦费。”\r\n男子不情愿地掏出20递给老板，老板在酒柜上找了找，然后找了他10块。\r\n男子：“怎么又找了10块？”\r\n老板：“酒卖光了。”');
INSERT INTO `tpshop_joke` VALUES ('2276', '阿炮有个女朋友名字叫WENDY，阿炮非常爱他的女朋友，于是就将女朋友的名字刺青在他的JJ上面，平时垂软时，只看得到“WY”两个字。\r\n有一天，阿炮去美国旅行在公共澡堂里遇着一个黑人，黑人看到他JJ上有“WY”两个字，就问他那是什么意思，阿炮说：“这是我女朋友的名字。”并且将JJ弄翘起来，让“WY”变成WENDY给他看。\r\n这时，他看到黑人的JJ上也有“WY”两个字，他很好奇的问他：“你的女朋友也叫WENDY吗?”\r\n黑人说：“不是。WELCOME TO THE UNITED STATE OF AMERICA，HAVE ANICE DAY。“');
INSERT INTO `tpshop_joke` VALUES ('2277', '有一次和几个好基友在网吧L0L，旁边几个小学生也在撸。然后就大声争吵起来，就听见其中一个说，你TM不要说话，你说话就把你的智商暴露出来了。当时整个网吧里都笑喷了。现在小朋友太有才了。');
INSERT INTO `tpshop_joke` VALUES ('2278', '第一版的看到小龙女被糟蹋的时候感觉好难过，第二版的时候在欣赏，第三版的时候瞬间感觉尹志平太亏了，强奸谁不好啊。。。');
INSERT INTO `tpshop_joke` VALUES ('2279', '一个男屌丝在家闲得无聊。突然，一个电话过来:“喂，小伙子，我是广场舞领舞。你妈叫你来跳一下。” 那屌丝下楼了。他说:“大妈，我是哪个位置?”“你进来吧”那屌丝刚进去，一群大妈迅速倒下，连忙说:“诶诶，你撞我干嘛。赔钱。” 这世上还有没有信任了? (｡ì _ í｡)');
INSERT INTO `tpshop_joke` VALUES ('2280', '哪天傍晚去广场，一群大妈在跳广场舞。 这时一个五六岁的小孩戴着孙悟空面具手里拿一根金箍棒一蹦一跳的走到方阵前排，站定然后大喊了一声：“孩儿们！练起来！” 如此几遍之后，人开始散了……');
INSERT INTO `tpshop_joke` VALUES ('2281', '一拉煤司机在路上遇一老头搭车，因驾驶室没空位，就让老头坐在后斗的煤堆上，到了目的地卸煤时，司机把老头给忘了，操作自动翻斗就把煤卸了下来，这时司机忽然想起老头，立刻跑到车后边找人，只见老头从煤堆中爬出来，直朝司机冲来，老头抓住司机的手说: 对不起小伙子，下车没注意把车给你踩翻了。');
INSERT INTO `tpshop_joke` VALUES ('2282', '‍‍‍‍看到一妹子发表说说：“不管等待多久，总会遇到我的伯乐的。” 哥回复：“即使你是一匹千里之马，也是要骑试试看的…”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2283', '早晨洗头之后头皮很痒，仔细一看：是瓢柔！先泡上一桶康帅傅方便面，抽一支中萃香烟解闷。早餐后穿上报喜乌的外套，含一块大白免奶糖下楼！到中围石油加油。走进了超市，商品琳琅满目：司口司乐、娃娃哈、下好佳、脉劫、橙多多…… 哈还有丑粮液呢，生活多么美好呀真逗！');
INSERT INTO `tpshop_joke` VALUES ('2284', '‍‍‍‍‍‍老师：“请大家解释一下环境因素和遗传因素。” 小明：“长得像爸爸是遗传因素，长得像邻居是环境因素。” 老师：“小明，滚出去！！”‍‍‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2285', '‍‍有一天，小侄放学回家书包一扔，说：“不写作业。” 奶奶听了说：“不写打你。” 小侄只好乖乖就范，谁知刚写一会，笔一扔，刷的站起来自言自语道：“不写了，让奶奶打去！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2286', '　　和一个二货朋友出去取钱办事。到了银行旁边，一看没地方停车，就往前面路边停车了，我下车取钱时，告朋友;如果有交警来查违章停车，你就喊我一声。我正在排队就听得朋友慌里慌张的大喊：大哥，警察来了，快跑。营业厅顿时乱成一团。');
INSERT INTO `tpshop_joke` VALUES ('2287', '妈妈带着三岁的儿子去银行取钱，儿子看营业员把钱递给妈妈就奶声奶气的问：妈妈，这个人是不是欠咱钱啊？亮点来了，妈妈回答：是啊，这个人欠了很多人钱，所以大家就把他关进了铁笼子里。');
INSERT INTO `tpshop_joke` VALUES ('2288', '‍‍‍‍老师在讲课，小明在发呆。 老师：“小明，你起来说说这篇作文写的怎么样？” 小明：“写的很好。” 老师：“我读的是一篇零分作文，你说它好，好在哪里？” 小明：“好在不是我写的。。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2289', '‍‍在饭店吃饭，我看碗有点脏就问服务员：“你们的碗用过后都是怎么处理的？” 服务员说：“放心吧！都是矿泉水处理的。” 于是我放心地吃了起来。 吃饱喝足走到门口，一条大狗流着口水友好地蹭我。 只听服务员大喊：“矿泉水！不许调皮！” 怎么感觉有点不太对劲？‍‍');
INSERT INTO `tpshop_joke` VALUES ('2290', '老公给3岁的女儿洗澡，刚把女儿放进水盆，女儿就大叫：“妈妈快看，爸爸泡妞啦。”');
INSERT INTO `tpshop_joke` VALUES ('2291', '跟老婆商量想换个新手机，老婆爽快的答应了，我心里那个高兴啊！老婆接着说：“要换就一步到位，包给我了！”我上来就是一阵猛亲，还是老婆对我最好，我寻思那不就是爱疯6plus了？今天是个好日子……第二天老婆给了个全新的老年机……一步到位……');
INSERT INTO `tpshop_joke` VALUES ('2292', '那次是谁发了一个冷笑话说：吃菠萝往嘴里吸气舌头会出泡。 我现在在吃菠萝我就嘴贱试了一下感觉没感觉又试了一下，现在停不下来，舌尖好痛(┯_┯)');
INSERT INTO `tpshop_joke` VALUES ('2293', '‍‍今天我出差刚回来没告诉老婆，想给她一个惊喜来着。 刚到家门口，打算逗逗老婆，我一边敲门一边捏着鼻子说：“你好，有快递。” 只听里面传来一句：“我老公又不在家，还对啥暗号。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2294', '盆友圈里有的好姐妹合照真心脸盲，感觉她们当年义结金兰的誓言应该是「不求同年同月同日生，只求同鼻同脸同医生」');
INSERT INTO `tpshop_joke` VALUES ('2295', '在酒店吃饭,我席间内急,服务员热情地说;我们酒店没有卫生间,你可以去对面公厕,我们和他们有约定,到那你就说你是’吃饭的’!');
INSERT INTO `tpshop_joke` VALUES ('2296', '‍‍妹纸：“亲爱的，动词后面跟的是什么呀？” 骚年：“动次打次动次打次……苍茫的天涯是我的爱……” 妹纸：“……”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2297', '教授生物学的教授被里德拦住了。“教授，”里德说道：“向您请教一个问题——遗传与环境有什么不同呢？” 教授看了看里德，说：“那要看从社会学还是纯生物学哪个角度看这个问题了。举个例子：从社会学的角度看，如果孩子生下来像父亲，那就是遗传问题。” 里德安静地听着。教授继续说道：“如果孩子生下来像邻居，那就是环境问题了。”');
INSERT INTO `tpshop_joke` VALUES ('2298', '夫妻俩吵架，丈夫理亏，就把怒气发泄到儿子身上，给了孩子一巴掌。 妻子怒不可遏：“什么？你敢打我儿子？” 边说边把儿子拉过来，也打了一巴掌，还气冲冲地说：“哼！你打我儿子，我也打你儿子，别想占便宜！”');
INSERT INTO `tpshop_joke` VALUES ('2299', '　　听一出租车师傅讲的。 　　一天一老年乘客，车费10元，老人拿出个100的，司机说，大爷，您没有小票（小面值）啊？ 　　大爷说，我们家机器不印小票。 　　司机给找零钱，几个10元的，后来又凑了几个5元的。 　　大爷一看说，你没有50的啊？ 　　司机说，我们家机器不印大票。');
INSERT INTO `tpshop_joke` VALUES ('2300', '1个女孩出国了，出国前答应男友修完硕士就回国成婚。男孩隔日一信，每周一通电话，不曾间断。当女孩子发高烧时，他心焦如焚。女孩一年多就修到了硕士，但嫁给了国外研究所的同学。女孩子对朋友说：“男孩的爱情是让我感动！但当我在大雪天走出教室，冻得浑身颤抖时，是我丈夫的车，及时停在眼前。” 这个故事告诉我们，就算你有多爱一个人，如果不付出行动她也永远不会是你的。 “说一万遍我爱你，不如一句我在楼下等你！”');
INSERT INTO `tpshop_joke` VALUES ('2301', '体育课陪闺密去上厕所，她进去了，我在外边等她，没一会儿，她出来了，一脸严肃的问我：哎你看我嘴角有东西没？我看了她一眼，突然想到了什么，就问她：你吃屎了？');
INSERT INTO `tpshop_joke` VALUES ('2302', '我不怕告诉你们，我之所以每天早晚坚持跑五公里，两百个俯卧撑，两百个仰卧起坐，开启疯狂健身模式，就是为了跟前任证明一件事，不是所有有八块腹肌浑身肌肉的人，丁丁都大！');
INSERT INTO `tpshop_joke` VALUES ('2303', '邻居一萌萌小萝莉，爱吃辣条，可是又怕辣，每次吃都让她妈给她准备一块冰敷的毛巾。吃一口辣条，连忙伸长舌头，用冰毛巾敷一下嘴巴，嘴里嘟囔道：“妈呀，辣死啦！”如此循环，乐此不疲！哈哈～看来谁也阻挡不了一个小吃货的脚步啊！');
INSERT INTO `tpshop_joke` VALUES ('2304', '就刚刚，一女的在沃尔玛买东西的时候偷了一件50块钱的内衣，被监控拍到。和超市工作人员争吵了一番后超市报警，警察来了核实后把那女的带走了。要坐警车去派出所，那女的要开自己的车，警察也同意了，然后那女的就上了一张宝马车跟在警车后面走了！有钱人真的可以这么任性吗？真的没想通');
INSERT INTO `tpshop_joke` VALUES ('2305', '刚刚从负一楼上电梯，我到5楼，只见旁边那个人把所有楼层按了一遍，然后他一楼就下了……我好想问他:我和你有仇吗？');
INSERT INTO `tpshop_joke` VALUES ('2306', '撩男友第一式：他：“咱们出去玩吧，我想你了，你说去哪里玩啊？”我吼道：“玩什么玩，昨天刚见了面！吃饭呢！等会儿再说！”他：“哦。”失落的声音一下子就低了。两分钟后，我打电话给男友。我淡淡的说：“过来接我，刚订了两张电影票。”他：“好！马上过去！”语气惊讶又开心。');
INSERT INTO `tpshop_joke` VALUES ('2307', '前女友结婚请我去，婚礼上随了一万块的礼，新娘很开心，问为何会随这么多，我回答每次一块也不止这个数，旁边的新郎听到脸都绿');
INSERT INTO `tpshop_joke` VALUES ('2308', '本人在一家公司上班好好的，一个之前认识的公司经理，打电话说请我过去上班，谁知道等我辞职后出厂后，他来了句不招了，真是日了狗了，上一家工作丢了，这一家没进去，真是糗大了，打工的我容易吗？唉～～～');
INSERT INTO `tpshop_joke` VALUES ('2309', '小王:爸爸你以后能不能不打我啊老王:那哪能！祖传的手艺不能到我这失传啊，小时你爷爷就这么打我的你以为故事完了么小王:那隔壁李叔叔从不打自己孩子老王:他敢！');
INSERT INTO `tpshop_joke` VALUES ('2310', '冷嗖嗖的，今天悠哉悠哉的骑着我的小电驴在路上，看见前面一辆车的车门没有关紧，我加大马力追了上去，对车主喊着他的车门没关紧，车主感谢地看了我一眼，然后往路边开去。你以为，这就结束了吗？？刚转弯，他就被追尾了。见此，我默默的走了');
INSERT INTO `tpshop_joke` VALUES ('2311', '下班回到家，帮我们在家照看三岁儿子的岳母娘脸沉得能掉下水来。我悄悄的问媳妇啥状况，媳妇说：“昨晚和我们一起睡的儿子，午睡时一屁股 坐到他外婆腰上，上下摆动，嘴里念念有词，啊，老公，我不行了！”');
INSERT INTO `tpshop_joke` VALUES ('2312', '‍‍‍‍女：“你最欣赏我什么？” 男：“我最欣赏你滴头发。” 女：“为什么？” 男：“因为这样……就可以挡住你的脸了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2313', '‍‍‍‍因丈夫失踪，玛丽到警局报案不久。 她接到侦探的电话：“夫人，我们发现一具尸首，很可能是您丈夫的，能提供他更明显的特征以便我们进一步确认吗？” 听到消息玛丽先是尖叫一声，稍做思考后回答：“他说话结巴，喜欢眨眼，还有就是老放屁。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2314', '在一个漆黑的夜晚，一个灯淡淡的照在那银白色的桌子上，有个黑帮老大说：货、、、到了吗？另一个男子拿出一相包裹着白色粉状的东西，黑帮老大对自己的手下挥了挥手说：验货！说完那旁边的那个小弟就拿着一把长50cm的刀刺入了那白色包装袋里，然后就开始吸了起来，之后那男子就说：见货，拿钱 这时黑老大就怒了，叫旁边的小弟绑了他，黑老大说：你竟敢骂我贱货！ 这时他哭笑不得');
INSERT INTO `tpshop_joke` VALUES ('2315', '‍‍‍‍‍‍老婆刚出门又匆匆返家，我问：“不是去逛商场吗？咋又回来啦？” 老婆：“落了东西没拿！” 我：“啥东西？” 老婆：“票子啊！” 说完一把拽住我胳膊，说：“现在拿了，‍‍还愣着干啥？跟我走啊！”');
INSERT INTO `tpshop_joke` VALUES ('2316', '老婆上星期给的二十元零花钱用完啦！今天朋友聚会想买包体面点的烟的钱也没有。这时儿子和老婆说：妈今天学校要交五十块钱。媳妇二话不说拿钱给儿子。等老婆出门后儿子把钱给我说：这也算我借你的，以后等我结婚后不济的时候你也得帮我。看着儿子的背影感叹到亲儿子啊[');
INSERT INTO `tpshop_joke` VALUES ('2317', '‍‍‍‍晚上九点多了，烧烤摊还没出来。 “老板在哪呢？等你半天了。” “兄弟啊，我摆个烧烤摊，向来比较自由。可是自从你成了熟客后，我就成上班一族了。” 好吧，我是有点准时了！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2318', '‍‍‍‍两个猎人去打猎。 在湖边看到几只野鸭，一个猎人举枪去打，没想到打偏了，野鸭全飞走了。 另一个猎人急忙摘下帽子，去湖里舀水喝，开枪那个猎人奇怪地问：“你在做什么？” 喝湖水的那个猎人说：“野鸭飞走了，我只能喝点鸭汤了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2319', '“老婆，我在烤肉店门口被车撞了，腿都断了！” “哪家烤肉店？什么口味的？好吃吗？”');
INSERT INTO `tpshop_joke` VALUES ('2320', '‍‍‍‍今天跟闺密抱怨，大姨妈来了晚上睡觉会漏。 只听见闺密得意的说：“我的从来就不会漏。” 我：“为什么啊？” 闺密：“我来大姨妈了，睡觉都用我儿子的尿不湿。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2321', '今天和老婆逛街，路上有个老太太摆摊卖的面包糕点还有袜子拖鞋啥的。我和老婆走到那，老太太吆喝：面包五元一个，袜子十元四双，拖鞋八元一双，小伙子来双拖鞋尝尝吧！”我刚想说不了，话还没出口，我老婆说：不要，太硬了煮不烂！”');
INSERT INTO `tpshop_joke` VALUES ('2322', '眼睛边上被蚊子咬了，问班里的国民男神借风油精。男神二话没说，拿出风油精亲手帮我涂了上去。 我有一点不好意思：“干嘛……” 他边涂边说：“我怕你涂眼睛里，别动了。” 旁边，几个女生在起哄：“哎哟哟～这么甜蜜呀～” 男神霸气回道：“我帮我的小裳涂风油精要你们管吗！” 心化了……');
INSERT INTO `tpshop_joke` VALUES ('2323', '一位老奶奶推着老爷爷，老爷爷对老奶奶说：“宝贝儿，你真好 好温馨！我走上前去问老奶奶：“怎样保持这一辈子爱呢？ 老奶奶说：“以前他有外遇，一度想抛弃我。” 我：“那您是怎么做的？ 老奶奶：“这不，我把他的腿打断了！ 我接着问大爷：“大爷您都八十多了，还叫老伴儿宝贝儿，请问您是怎么做到的？ 大爷：“别提了，前几年就忘了她叫啥名了，我TM也不敢问呐，怕她弄死我');
INSERT INTO `tpshop_joke` VALUES ('2324', '一老大爷坐火车；买的是慢车车票，却上了快车；乘务员查票时查到了，就对老人家说：“老人家，你的票要补哦”可老人家眼一瞪，说：“上面的洞洞儿是你们剪的，咋会喊我补哦？”乘务员傻眼了，又解释道：“不是票坏了喊你补，你买的票是慢车票，这趟车是快车，你应该补快车车票；”老人大悟，说道：“哦！是这样啊；那你喊司机开慢点吧，反正我又不赶时间的；”乘务员晕了。');
INSERT INTO `tpshop_joke` VALUES ('2325', '今天去饭馆吃饭对面坐了一对小情侣吃个饭一直在那里扭扭捏捏打情骂俏看的我这个火呀一拍桌子走到他们跟前掏出50块假钱甩到桌子上说到我实在受不了你们俩了给你们50块钱请你们去别的地方吃那男的一看我用钱侮辱他他愤怒的就从兜里掏出100块扔到桌子上说我TM给你100块请你去别地吃我当时就气坏了 用愤怒的眼神瞪着他拳头纂的紧紧的然后拿起100块钱快速离开了饭馆');
INSERT INTO `tpshop_joke` VALUES ('2326', '“上天对单身狗最大的恶意嘲讽就是总让他看到成双成对的东西：一个人在家做蛋炒饭，打个鸡蛋都是双黄的…” “然后呢？” “愤怒的砸了蛋，我又看到了手上的筷子…” “然后呢？” “折了筷子我就去了洗手间嘘嘘…” “行了不用说了”医生收起了手上的病历 “你这个左蛋好好休养一下还是能保住的，为了保险起见你还是先去神经科看看脑子吧”');
INSERT INTO `tpshop_joke` VALUES ('2327', '一位驰骋多年嫖场的老嫖客的自诉： 现在的鸡真是一年不如一年。你看身材好的都去参加模特大赛了，声音好听的都去报名歌唱比赛了，脸蛋漂亮的都去选美了，演技好的都去当演员了，有社会地位的都嫁入豪门了，有钱的谁他妈还出来卖。所以现在只剩下没身材，声音不好听的，长的丑的，床戏很假的，没社会地位的穷女屌丝。 所以朋友们，当年嫖到一只好鸡请珍惜吧，那是稀有物种。');
INSERT INTO `tpshop_joke` VALUES ('2328', '　　‍‍‍‍我：“哥们，你喜欢什么样的女生？” 　　哥们：“你先说你喜欢什么样子的？” 　　我：“我喜欢身材好，长的好的。” 　　哥们：“我只有一个要求，就是躺在她腿上时，看不到她脸的那种！” 　　居然秒懂了！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2329', '在没钱的时候把勤舍得出去，钱就来了。一一这叫天道酬勤。在有钱的时候把钱舍得出去，人就来了。一一这叫财散人聚。当有人的时候把爱舍弃出去，事业就来了。一一这叫博爱领众。当事业成功后把智慧舍得出去，喜悦就来了。一一这叫德行天下。');
INSERT INTO `tpshop_joke` VALUES ('2330', '‍‍‍‍老师批评学渣：“你一看书就睡觉。” 学渣辩解称：“我一玩手机就精神，一打开书就犯困！” 老师笑一笑说：“这好办啊，以后每天你开着手机摄像头看书不就解决了？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2331', '“哥 为什么每次嫂子一打你 你就满地打滚？丢人不”\r\n“我那是在报复她”\r\n“是么，怎么报复的？”\r\n“我虽然打不过她 可衣服都是她洗呀”……');
INSERT INTO `tpshop_joke` VALUES ('2332', '在车站买了瓶“娃哈哈”营养快线，喝起来总感觉哪里不对。仔细一看，马拉个b，“娃啥啥”呀！\r\n在东莞车站买了瓶“娃哈哈”营养快线，喝起来总感觉哪里不对。仔细一看，马拉个逼，“娃啥啥”呀！');
INSERT INTO `tpshop_joke` VALUES ('2333', '今天我戴了个大口罩，朋友居然夸我帅，我想了想，又戴了个墨镜，朋友说你帅呆啦，以至于我又穿了一双增高鞋，这下朋友说，完美了，你把你所有的缺陷都挡住了。。');
INSERT INTO `tpshop_joke` VALUES ('2334', '媳妇儿怀孕了，在朋友圈发了如下内容“每次看老公眼圈黑黑的还给我一个温暖的笑把我搂在怀里，我都特心疼，为了宝宝，他更加拼命，白天工作，晚上回来给我做饭，陪我散步，给我买各种好吃的，但我始终担心宝宝的长相，因为老公实在太丑了……”网友神回复：是怕儿子不像老公吧');
INSERT INTO `tpshop_joke` VALUES ('2335', '老婆大银逛街，老公拎东西。老婆饿了，进了一家比较高级滴饭店。老公想跟进去呢，就被饭店门口保安拦住了。“对不起，推销滴不允许进！”老公：“俺不是推销滴，俺是找刚进去那女银滴。”保安：“对不起，送快递滴也不允许进……”');
INSERT INTO `tpshop_joke` VALUES ('2336', '我哥打电话说下午太倒霉了出门被人骑车撞了一下，本来想骂呢，抬头一看，是个妹子，然后起来问妹子有男朋友吗？妹子说没有。然后上去一下把她推到了，没有男朋友你特么敢撞我。 怪不得大哥你单身二十多年呢。。。我也是醉了');
INSERT INTO `tpshop_joke` VALUES ('2337', '我一哥们商学院MBA毕业，有天跟我一起在一小餐馆吃炒饭，每份炒饭配有一碗免费的菜汤。我哥们跟老板说：你送了碗免费菜汤，客人就不会点饮料了，如果取消免费菜汤，你的饮料就会大卖，这叫自己创造商机，懂吗？老板欣然采纳他的建议。 果然，过了两月，餐馆倒闭了，因为客人都跑对面的餐馆吃炒饭去了。');
INSERT INTO `tpshop_joke` VALUES ('2338', '朋友跟我哭诉，说因为太穷而经常失恋，我顿时对这个社会绝望了：同样是穷，凭什么他能有女朋友？');
INSERT INTO `tpshop_joke` VALUES ('2339', '中午单位打球，老总嫌出汗多，又没带球衣，就把上衣全脱了光膀子打。\r\n这时旁边一同事路过开玩笑说：哟，老总，穿着皮衣打球呢？\r\n我也准备跟着开一句玩笑：还是真皮的呢！谁TM知道嘴一抽，说成了“还是猪皮的呢！”\r\n然后就看见老总脸上的笑容慢慢的慢慢的凝固了......');
INSERT INTO `tpshop_joke` VALUES ('2340', '今天班长对我说：“班主任为王，导员为皇，讲师为妃，课代表为相，支书为将，班级为城，学霸为太子，院领导垂帘听政。” 我琢磨了半天，那班长是什么？班长必须是一位能同时接触到王、皇、妃、相、将、太子、城，还要应付垂帘听政的人物。 经过潜心研究，最后终于得出了结论：班长是太监！');
INSERT INTO `tpshop_joke` VALUES ('2341', '　　古龙小说中的一个故事分享给大家，天天开心哈。 　　一个人问大侠，你贵姓啊， 　　大侠说，我不敢说，怕你吃了我 　　那人就问，姓范？ 　　大侠摇头 　　那人问，姓蔡？ 　　大侠摇头 　　那人又问，姓杨？ 　　大侠还是摇头 　　那人又问，姓马？ 　　大侠还是摇头 　　那人不甘心又问，那姓啥啊？ 　　大侠说，姓史(屎)。 　　哈哈。');
INSERT INTO `tpshop_joke` VALUES ('2342', '今天上淘宝看中一款衣服然后进去看评论，看到一条差评：\"和我妈一人买了一件结果穿起来我妈比我还年轻，差评！\"店家回复：\"你长得老也是我的错？\"');
INSERT INTO `tpshop_joke` VALUES ('2343', '刚才在路边听到一男人大喊，“你搞我马子，踢我对象，现在还特么想打炮，有车了不起啊！”这时旁边的男人默默说了一句：哥，咱还能好好下棋了么？…');
INSERT INTO `tpshop_joke` VALUES ('2344', '　　我的零花钱就像大姨妈一样，一个月来一次，一个星期就没有了……');
INSERT INTO `tpshop_joke` VALUES ('2345', '一对姐妹，妹妹嫁给富二代，生活无忧但没有多少快乐。于是问姐姐，姐夫有缺点吗？姐回答有，而且多的像天上的星星。那姐夫有优点吗？姐回答有，但少的像天上太阳。妹妹不解的问，那为什么你总是那么幸福？姐回答，因为太阳出来就没有星星了。。。楼下肯定会说星星出来没太阳，星星出来我们都睡觉了！');
INSERT INTO `tpshop_joke` VALUES ('2346', '刚上大学时，每天早上都在别人还在睡觉的时候，起床化妆，晚上在别人睡着的时候，卸妆睡觉；就这样，我有一次起来晚了，还没来得及洗漱化妆，就被室友当男的打了出去...');
INSERT INTO `tpshop_joke` VALUES ('2347', '　　一同事，平时就特2。大家都喜欢和他一起玩。这一天上班，眉飞色舞的进了办公室，迫不及待地和大家炫耀刚买的新手机。这个嘚瑟别提了。第二天上班，蔫头耷脑的进了办公室，主任就问他：怎么了，看你这状态不对啊？2B说：昨天新买的手机，晚上没事下载了一个称重软件，下载成功安装完毕，就把手机放在地上，然后一只脚踏了上去……一个办公室的人都没有憋住笑。');
INSERT INTO `tpshop_joke` VALUES ('2348', '我特么就想问问幼儿园能不能多找几个男老师？儿子放学回家上厕所，居然蹲着嘘嘘，完事还特么拿纸擦擦！');
INSERT INTO `tpshop_joke` VALUES ('2349', '我跟我妹睡，此为背景，今早妹的男票来找她，好不容易把她拉得坐起来，没叫他去把内衣取过来，男票很忧伤的说\"我哪知道哪是你的啊，等下取到姐的怎么办，\"我默默地说了一句，\"你看着小的取\"');
INSERT INTO `tpshop_joke` VALUES ('2350', '小明问他妈妈：“为什么你总不告诉谁是我爸爸啊？我爸爸到底是谁啊？”妈妈回答说：“你爸爸做好事不留名，我也不知道他是谁………”');
INSERT INTO `tpshop_joke` VALUES ('2351', '跟老公吵架，我们一人一个卧室，我越想越生气，于是跑到大门口开了一下门又重重关上了，然后赶紧藏到柜子里，不一会老公出来了，一看我不在卧室抓起外套就跑出去了！哈哈');
INSERT INTO `tpshop_joke` VALUES ('2352', '朋友应聘上了顺丰快递的工作，我们出去庆祝。在路上，顺丰快递的人力资源部给她打来电话，要她填好资料并给他们寄过去。朋友说：“没问题，我一会就圆通快递寄过去”');
INSERT INTO `tpshop_joke` VALUES ('2353', '小时候看电视，特别羡慕那种点穴法～有一个比我大几岁的邻居说他会～楼主倾尽所有“家当”，拜师学艺～在他的指点，楼主日夜操练，一周后我去找他～他说现在可以在他身上试试啦，楼主随便一点，他果然站立不动，还嘴里央求楼主给他解开～ 楼主狠狠的给他屁股一脚，嘴里说着：把劳资给你的钱还回来，否则你就等死吧！');
INSERT INTO `tpshop_joke` VALUES ('2354', '今天看到，一个哲学问题，三观的形成？问题是:我是谁？我从哪里来？将要去哪里？脑袋里第一时间蹦出的答案是:贫僧唐三藏，从东土大唐而来，去往西天拜佛求经……');
INSERT INTO `tpshop_joke` VALUES ('2355', '　　一直蹭隔壁网的两家人最近一个月老是改用户名聊天，昨晚看到一个改成“老公已出差” 　　看到后 默默给她老公发了消息，半夜听到隔壁一阵阵的惨叫。。。。');
INSERT INTO `tpshop_joke` VALUES ('2356', '刚刚给儿子整理书包，发现试卷一枚语文3分，唉！我点了颗烟想了老大会，这是上的什么学，关键这3分我没看明白怎么给的。。。。人才啊！');
INSERT INTO `tpshop_joke` VALUES ('2357', '原创【带刺的玫瑰】 “听说你和女朋友分手了?” “是的。” “为什么呢?” “我把他放到一张铺满玫瑰的床上……” “那不是挺浪漫的吗?!” “可我忘把刺去掉了!”');
INSERT INTO `tpshop_joke` VALUES ('2358', '晚上打完球回来一身臭汗，回到宿舍，脱个干净，洗头膏挤在手里就往浴室去，一2B舍友从厕所出来瞅着我手里，蹦出一句:吆，这么晚了，您还带着孩子遛弯呢！');
INSERT INTO `tpshop_joke` VALUES ('2359', '突然发现大学的考试去考场的路上都是一波一波的走，每波人群里必有一个大神！要是单独走的不是孤傲的学霸要么就是绝望的学渣。');
INSERT INTO `tpshop_joke` VALUES ('2360', '“把你的花拿走，我不喜欢。” “你不喜欢我，还是不喜欢花？” “我不喜欢牛粪。”');
INSERT INTO `tpshop_joke` VALUES ('2361', '一哥们在网吧上网，电脑提示余额不足，就找了点日本成人教育片看。看了将近一个小时电脑还没有结帐关机，就去问网管怎么回事儿，网管说你旁边那哥们给你充的……');
INSERT INTO `tpshop_joke` VALUES ('2362', '‍‍‍‍说个战友的事，有次理发的时候，那二货把眉毛刮了个精光。 次日集合时连长看到了，把那二货拉到前面：“大家看看这是人是鬼。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2363', '自从班里班花把网名改为小龙女，其他同学纷纷改名叫杨过。只有我默默地改名叫伊志平。。。');
INSERT INTO `tpshop_joke` VALUES ('2364', '开车带俩女闺蜜上高速，服务区买了一袋熟玉米继续上路，楼主一根玉米吃了半截嫌碍事，一手握方向盘一手往后递过去让她俩拿一下，俩二货在车里蹶着屁股打闹，一辆天杀的货车突然变道，楼主一脚急刹，拿玉米的手被闺蜜一屁股狠狠坐了上去，只听嗷的一声低叫，回头一看，玉米隔着裙子进去了大半截，闺蜜一脸错愕酸爽……');
INSERT INTO `tpshop_joke` VALUES ('2365', '‍‍‍‍看完忠犬八公，女票哭得稀里哗啦，：“我以后也要养条那样专情的狗，说不定也会一直等待我回来。” 我悠悠的来了句：“前提是你要比它先挂掉。” 结果，我差点先挂掉。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2366', '如果我的钱能像我的肉一样对我不离不弃就好了，或者，我的肉能像我的钱一样说没了就没了也行。');
INSERT INTO `tpshop_joke` VALUES ('2367', '‍‍‍‍我：“纯纯，我去洗衣服吧？” 老婆：“没有要洗的衣服！” 我：“我去做饭好不好？” 老婆：“还没到晌午！” 我：“我给你按摩吧？” 老婆：“给老娘跪好！让你给小姑娘说自己单身！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2368', '‍‍‍‍我两哥们双胞胎，长得一摸一样。 结婚那天叫我去喝喜酒，到了发现两位嫂子也是双胞胎，几乎模样相同。 我说：“你们老婆都长得一个样，不怕上错一个么？” 结果他俩笑了笑：“怕什么，肥水不流外人田。” 我：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2369', '早上，老婆趴我身上撒娇：“明天就是六一儿童节了，老公，我要买个LV包包。” “买买买！” “我还要迪奥香水。” “买买买！” 没成想此举被儿子看见了，待老婆去厨房后，只见他犹豫地趴了上来：“那个如果可以的话，老公，我想买个奥特曼！”');
INSERT INTO `tpshop_joke` VALUES ('2370', '‍‍‍‍昨天晚上哥跟几个女同学在学跳舞，我有点笨学的慢，每次做动作都会偷看旁边妹子的动作。 跳几次过后，这妹子就感叹到，“你老是偷看我的动作，抄我抄的好厉害啊！” 我极速反应到，“操你操好厉害？” 瞬间空气凝固了，那妹子后来才反应过来。 气的脸都绿了，完了，我这下被冠名上了变态！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2371', '一天上课，一女生有事跟老师请假走了，一哥们也无缘无故地跟着走了，众人奇怪。中午回来后看见他的QQ状态改成：“今天丢人丢大了，上课时我昏昏欲睡，突然看见一同学拿包走人。我以为下课了，也拿包走人……”');
INSERT INTO `tpshop_joke` VALUES ('2372', '一小偷被警察抓住了！　　警察审问到：“叫什么名字。”　　小偷不赖烦的答道：“李谦达（你欠打）。”　　警察莫名：“你说什么”　　小偷大声重复道：“我说李谦达（你欠打）啊。”　　警察有些生气道：“你说我欠打。”　　小偷：“不是我欠打，是李谦达（你欠打）啊”　　说着，警察上去就是两耳光。　　说道：“我知道了，不是我欠打，是你欠打吧”　　被打后的小偷哭着连说：“对对对，就是李谦达。”　　警察：“你真的是欠打啊。”');
INSERT INTO `tpshop_joke` VALUES ('2373', '集市上，一个老头背着十几只小鸭子，一个年轻人喊道：老人家，老人家，你的鸭鸭跑了。　　老头很自然摸了一下自己的拉链，原来拉链没拉上，顺手将拉链拉起。　　嘀咕着：现在的年轻人真不像话，和我们这把年纪的人开玩笑。他没理会年轻人，回家后一数小鸭子，差四五只。');
INSERT INTO `tpshop_joke` VALUES ('2374', '儿子失恋把自己关在房间看书不出来。　　爸爸问：干吗把自己关在房间看书了？　　儿子说：书上说——书中自有颜如玉，我在找呢。　　爸爸说：傻孩子，书上也说——书中自有黄金屋。　　儿子说：我不要黄金屋，我要颜如玉。　　爸爸说：有了黄金屋就成高富帅，不要说颜如玉了，白富美都可以找到了。');
INSERT INTO `tpshop_joke` VALUES ('2375', '“你这个无头无脑的笨蛋！”　　“你看，那个人有头有脑也是笨蛋，我比他强多了！”');
INSERT INTO `tpshop_joke` VALUES ('2376', '曹操得了头痛症。华佗认为曹操头痛的病根在颅中，于是禀告曹操：要、要~切开脑。　　曹操听完大怒：老子头痛的要死，你还敢唱歌，来人，拖出去斩了。　　华佗，卒');
INSERT INTO `tpshop_joke` VALUES ('2377', '一个人骑了一辆破摩托车，排气管子发出了很大的噪声，坐在后面的朋友开玩笑说：你这破车声音这么大，都能把狼招来。　　刚说完话，就看见前面有个交警在向他们示意停车。');
INSERT INTO `tpshop_joke` VALUES ('2378', '公交车上，某男一手托举着，五指分开，似托一碗，举累了，在换手时小心翼翼，好像托着一个无形的球。　　大家都很纳闷到底他托的是什么，终于有人忍不住问：“哥们，你这是练什么绝世神功呢？”　　这哥们答：“老婆让我给她买胸罩，怕忘了大小……”');
INSERT INTO `tpshop_joke` VALUES ('2379', '月黑风高夜，大明独自在公路收费站值夜班，突一阵阴风！　　伴随低沉的发动机轰鸣，只见一辆黑色轿车缓缓停在收费口，大明抬头定睛一看！驾驶座没有人！！　　惊惶失措的大明赶忙抬起栏杆放行！黑色轿车缓缓驶过……　　后来呢？后来……车缓缓驶入了“开封府”。');
INSERT INTO `tpshop_joke` VALUES ('2380', '话说我们化学老师，马上结婚了。　　一天自习，那厮拿了个本子写了一节课，下课无聊，我去看他写的啥，满满几篇全是“我要结婚了哦耶……”');
INSERT INTO `tpshop_joke` VALUES ('2381', '昨天男朋友和我吵架了，最后分手了，他性子挺偏激的，我就准备悄悄的去找他，本来分隔两地。今天实在很想他就给他打电话，我说后天平安夜了，他说没人陪他过～～～我就告诉他我会送给他苹果，让他那天不要拒收。你们能想到俺圆圆的身姿穿着红红的衣服假扮苹果站在他面前的样子么？');
INSERT INTO `tpshop_joke` VALUES ('2382', '　　妈妈加班了，爸爸去房后的菜园摘菜，采回许多蘑菇炖了猪肉，看我喜欢吃。爸爸就没动筷。吃完我去书房学习。妈妈下班后，就听见妈妈问爸爸：“你们吃的啥？”爸爸说：“我在菜园里采的野生蘑菇，也不知有没有毒，全叫儿子一个人吃了，要是没事，我明天再去采些给儿子吃。”');
INSERT INTO `tpshop_joke` VALUES ('2383', '我认识一妹子，刚开始不是很喜欢，有次她喝醉了，我就带她到我家，吐了拖地，给她擦脸，拖鞋，折腾好几小时她才睡着。没想到我就这么的喜欢她了，后面对她各种好，各种照顾，各种体贴，我觉得她对我也不反感，于是就表白，被拒绝。她家里也挺富有，我家比不上，最近还是对她一样好，只是感觉没啥希望，我也不知道为啥，搞得最近也没心思上班，求大神们出招！');
INSERT INTO `tpshop_joke` VALUES ('2384', '　　问：“你说我长得好看吗？”答：“在好看与难看之间。”喜看来本人姿色中等啊！答：“不对，我是说好难看！”');
INSERT INTO `tpshop_joke` VALUES ('2385', '女儿告诉妈妈，因为妈妈反对她和男朋友恋爱，她的男朋友服安眠药自杀了。母亲一惊：“自杀啦？”女儿说：“还好，他吃错了药，没死。”母亲说：　“我早就说过，他这个人马马虎虎，大大咧咧，成不了大事。你看，连这点小事都搞错，怎么能托付终身呢？”不要指望女人认错，她们没有认错的习惯。错一千回，她们就有一千个理由为自己开脱。人命关天又怎样？正确的依然是她。');
INSERT INTO `tpshop_joke` VALUES ('2386', '刚才接了个电话说是保险公司的...本来想挂的..后来一想都是做销售的！挺不容易的..就陪那大哥聊了半小时。经过半个小时的探讨....我终于把他拿下了！买了我两瓶祛痘霜……');
INSERT INTO `tpshop_joke` VALUES ('2387', '在泰国的酒吧玩，一个性感妖娆的美女走到我跟前挑逗我说：“帅哥，想玩玩吗？” 我警觉地说：“我怎么知道你不是男人？” 美女说：“脱裤子验证一下就知道喽。” 我点点头。 “看！我不是男人吧？” 美女一把脱下我的裤子，然后指了指自己的私处说，“看到你的J j我并没有搭帐篷。”');
INSERT INTO `tpshop_joke` VALUES ('2388', '男人在年轻的时候都希望拥有一个花枝招展，外表光鲜亮丽的女人。但是随着岁月的沉淀，尤其是有了自己的一番事业和丰富的人生阅历，就会发现自己当初的想法是多么幼稚，因为一个哪够！');
INSERT INTO `tpshop_joke` VALUES ('2389', '今天朋友捡到一个白色小米4，后面贴着标签，上面写着：你好，如果你捡到了我的手机，请联系我，我的电话是183…………。拥有雷锋精神的我们按照上面的号连续打了两次都占线，真是奇怪！人与人之间的信任都去哪了？留个电话号还不认真！');
INSERT INTO `tpshop_joke` VALUES ('2390', '今天刚发生的，北京现在的温度大家都知道，看到一男的去骑电瓶车，带着帽子包着脸，刚骑不到两米，就被从饭店里跑出来一小伙上去就是一脚，还喊了句“叫你吗b偷我车”那摔倒的。。。骑车那男的躺在地上抬着头，和小伙对视了几秒钟，这时小伙连忙上前扶道：“爸，你来骑车怎么不吭一声”？事实就是如此。。。');
INSERT INTO `tpshop_joke` VALUES ('2391', '老婆连续减肥一个月，晚上问我：“老公，你看看我的脸是不是瘦成瓜子脸了？”老公：“你？你那是瓜子脸他妈妈的脸！”老婆：“什么意思？”老公：“向日葵的脸！”老婆：“……今晚跪键盘，打出来一百遍‘老婆我错了！’”');
INSERT INTO `tpshop_joke` VALUES ('2392', '一男碰瓷，倒地装晕，女车主将其暴打至奄奄一息， 警察要将女车主拘留，她大怒“他碰瓷还受法律保护不成？” 警察“他碰瓷不假，另案处理，你涉嫌故意杀人，懂吗？” 女车主“卧槽！你拿碰瓷专业户没辙，拿我见义勇为倒挺TM有手段！” 警察“女侠你脾气总那么火爆？” 女车主一甩留海“劳资大姨妈来了！” 警察愣了三秒，压低声音“你快走吧，剩下的事我来处理，” 女车主一愣“卧槽！你是不是爱上我了？” 警察“不是，哥敬你是条汉子…”');
INSERT INTO `tpshop_joke` VALUES ('2393', '我昨天去看医生了。——哦？医生怎么说？——医生说，你看够了没？！');
INSERT INTO `tpshop_joke` VALUES ('2394', '下班回家，在电梯里遇见美女邻居。她跟我打招呼说：“呦，去遛鸟啦？”我还一头雾水呢。到家后知道真相的我眼泪掉下来。原来我裤子拉链没拉。遛鸟～遛鸟～');
INSERT INTO `tpshop_joke` VALUES ('2395', '新出生的宝宝还插着心率监视图的线子，三岁的小侄子说：弟弟的电什么时候能充满？');
INSERT INTO `tpshop_joke` VALUES ('2396', '美女是别人的，别爱。老婆是自己的，别厌。人生是痛苦的，别迷。活着是短暂的，别慌。欲望是无穷的，别贪。身体是自己的，别懒。工作是大家的，别抢。钞票是消费的，别存。生活是丰富的，别烦。假期是休闲的，别忙。朋友是永远的，别忘');
INSERT INTO `tpshop_joke` VALUES ('2397', '今天在卫生间洗了个水蜜桃，想甩干上面的水，手一甩掉进了马桶里！还正好塞住了那个窟窿眼，太杯具了！更杯具的是比起“马桶不会堵住吧”和“这要怎么掏出来啊”更快浮现在我心头的想法是“洗洗还能吃吗”……');
INSERT INTO `tpshop_joke` VALUES ('2398', '‍‍‍‍我看着镜子里的自己，幽怨的对妈妈说：“你为什么要把我生的这么胖。” 妈妈慢慢的转身，一脸鄙视的看着我说：“关我屁事呀，我生你的时候只有四斤。” 我竟无言以对！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2399', '一个医生对一个青年患者说：“有两个护士，一个年轻漂亮，温柔，有耐心，每次给病人护理至少花上半个多小时，一个年老，长相一般，没耐心，几分钟就对付一个病人，你喜欢哪个护士给自己看病？”青年患者毫不犹豫地：“要温柔又有耐心的护士！”医生冲门外点点头：“小文，你进来吧，你可以慢慢地给这个患者扎针，要温柔点，扎错了也没关系，可以多试几次！他就喜欢你这种有耐心的护士！”');
INSERT INTO `tpshop_joke` VALUES ('2400', '‍‍‍‍二货老婆早上洗衣服的时候对我说：“老公，我手机掉水里了，给我买个新手机吧？” 我仔细看了看说：“这不还在衣服上搭着吗，没掉水里啊？” 二货老婆一看，立马抽了抽衣服，那手机果断掉肥皂水了。 尼吗，这败家娘们！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2401', '一对夫妻不小心出了车祸，丈夫没有什么大碍，妻子却受伤极重面临死亡，丈夫坐在妻子的床边握着她的手，妻子身体不断的抽搐着说：“你能...不能答应...我...一件事？”丈夫抬起头艰难的说：“不管任何事，我都无条件的答应你。”妻子说：“你...能...不能...不要笑得这...么开心。”');
INSERT INTO `tpshop_joke` VALUES ('2402', '听说老鼠最怕猫，我实验了下。悄悄的在一只老鼠的耳畔大声唱起：啊啊啊~啊啊啊~黑猫警长！老鼠果然被吓跑了。');
INSERT INTO `tpshop_joke` VALUES ('2403', '有些女人上了妆就像P过一样 有些女人卸了妆却像劈过一样！');
INSERT INTO `tpshop_joke` VALUES ('2404', '我：我玩的游戏满级了。 宿友：我晚上和女友开房。 我：我得到游戏最牛的神器了。 宿友：我晚上和女友开房。 我：有人出高价买我账号了。 宿友：我晚上和女友开房。 我：我艹。你过来看我不抽死你');
INSERT INTO `tpshop_joke` VALUES ('2405', '一天，坐公交车，好不容易等来了35路车，车上已经人满为患，lz急中生智，大喊一声：＂后面还有一辆空车！＂ 没想到这么老套的办法竟然显灵了，所有人听到我的喊声都向后面跑去，虽然很挤，但我终究还是上来了，正在庆幸自己聪明过人，车子刚刚启动之时，旁边超过去另外一辆35路，人人有座。');
INSERT INTO `tpshop_joke` VALUES ('2406', '一同学，想吃糖，到超市了，问我‘那个糖叫什么来着，喜马拉雅还是珠穆朗玛？ ‘那他妈叫阿尔卑斯’。');
INSERT INTO `tpshop_joke` VALUES ('2407', '早上吃早餐，隔壁桌坐了对情侣，听见老板喊：帅哥，你的牛肉面！估计没人应，于是又叫了几遍… 隔壁桌女的问那男的：你叫的不是牛肉面吗？是在叫你吧？ 那男的慢悠悠站起来：好久没人叫我帅哥了，多听几遍…我tm一口粥瞬间喷出！');
INSERT INTO `tpshop_joke` VALUES ('2408', '在网上同时买了衣服和腊肠，写评论时，写反了。 给腊肠这样评论:尺寸正好，很合适。 结果老板居然回了:合适就好，注意安全啊！');
INSERT INTO `tpshop_joke` VALUES ('2409', '老婆打电话给我：“老公，我在高架追尾了，撞上了一辆面包车，我陪他两百块钱他还不要，非要叫交警。” 只听车主大声提醒到：“美女，别忘告诉你老公，这辆面包车叫路虎!”');
INSERT INTO `tpshop_joke` VALUES ('2410', '第三天，语文老师提问小明“小明，你用可爱造句。” 小明说“可可，爱我吗？” 老师有点生气，“那浪漫呢？” “可可，你可真浪，慢一点行吗？” “小明你给我滚出去！”');
INSERT INTO `tpshop_joke` VALUES ('2411', '在餐馆吃自助餐，忽有个奇怪的联想，觉得很多人吃自助餐的过程仿佛人生之缩影：开始时饥肠辘辘，大鱼大肉，仿佛青年之血气方刚；渐渐不饿了，开始精挑细品，仿佛中年之沉稳老练；最后饱了，开始打嗝剔牙，仿佛老年之迟暮。正联想着，猛然惊觉邻桌的人不见了，两位服务生正在把他们留下的东西清理掉。。。');
INSERT INTO `tpshop_joke` VALUES ('2412', '如果一个好朋友离你而去，那可能是他的问题；如果大量好朋友一个个离你而去，你就要反思一下了，自己平时结交得都是些什么JB玩意儿？');
INSERT INTO `tpshop_joke` VALUES ('2413', '出来混，一定要讲诚信，说不杀你全家就不杀你全家，你看，我这么讲诚信，能不能借点钱给我？');
INSERT INTO `tpshop_joke` VALUES ('2414', '和老婆坐在路边的凳子上，一美女走过来，老公的眼睛被吸引了过去……老婆生气：“你看什么看？有胆你去摸摸，我给你一百块！”老公脸红了：“不敢，我不去……”老婆：“就这胆量啊，给你二百！”老公连连摇手：“不，不……”美女走过来：“大哥，来摸摸，咱俩一人一百。”');
INSERT INTO `tpshop_joke` VALUES ('2415', '老公：老婆，我想要，特别想要。老婆：不给。老公：亲爱的你就给我好不好，我都快憋死了。老婆：不给，说什么也不给。老公：这个月家务我全包了，给我吧！老婆：这还差不多，咯，十块钱拿去买吧，省点抽。');
INSERT INTO `tpshop_joke` VALUES ('2416', '小区里经常有BT偷女性内衣，这不一大早老婆就大喊着叫我：老公老公我晾在阳台的内衣被偷啦！我赶紧爬起来安慰她：没事没事，我昨晚偷了很多藏在衣柜里，都给你。');
INSERT INTO `tpshop_joke` VALUES ('2417', '我躺在一张床上，心想：“在这张床不知道睡过多少女人，虽然我对她们都不了解，现在这社会不就是这么回事嘛……”就在此时，一个护士的声音响起：“喂，还在磨蹭什么呢？你该出院了！别的病人还在等着床位呢！”');
INSERT INTO `tpshop_joke` VALUES ('2418', '今天坐公交车，看到一个男子猥亵一个妙龄少女长达15分钟，我终于忍不住走过去对他吼道：你有完没完？说好了每人五分钟的');
INSERT INTO `tpshop_joke` VALUES ('2419', '你给我一个微笑，我也会还你一个微笑，也不是善意的招呼，只是让你知道：老子笑起来比你好看。');
INSERT INTO `tpshop_joke` VALUES ('2420', '天冷了！如果不能给我拥抱，请给我买件外套，M号。如果不能给我外套，又没有拥抱，请给我钞票，我有卡号~~~');
INSERT INTO `tpshop_joke` VALUES ('2421', '记得上小学的时候，有一次数学老师生病了一天都没来给我们上课，兴奋的我跑回家把这好消息告诉了爸妈，结果爸妈听后直接把我一顿打，还拿着棍子让我必须去老师家看望一下老师，当时吓的我含着泪就跑去了。到那说的什么我已经记不清了，只知道从那以后老师特别关注我这差等生的学习，我也不知道为什么，直到后来别人也发现了这个情况，问她为什么？当时数学老师有些感动的说道:“因为这孩子人品不错，前段时间我生病。其他学生也有来看我的，但很多的都是装装样子，只有他一进来就哭着问我:“老师，你身体好点了吗？”那眼中的泪水绝对不是装出来的。。。。”');
INSERT INTO `tpshop_joke` VALUES ('2422', '跟媳妇去超市，因为闹点小别扭我就跑了，我还以为她会来找我，没想到她去服务台让播音找我，整个超市都回荡着“何大宝同学，你的妈妈在服务台等你，请速到服务台，不要跟陌生人说话。”。。。。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('2423', '和老婆乘电梯，最尴尬的是中途进来个大美女，清香扑鼻、亭亭玉立的站在我旁边。老婆目不转睛的瞪着我，而我目不斜视的抬头盯着楼层指示灯，好像那灯很好看似的。');
INSERT INTO `tpshop_joke` VALUES ('2424', '那次和媳妇吵架，我妈知道后，把我骂了个狗血喷头。我感到心里很憋屈，于是给丈母娘打电话，把事情的来龙去脉详细地说了一遍。然后，丈母娘又把我骂了一遍。');
INSERT INTO `tpshop_joke` VALUES ('2425', '我的妈妈是一个特别朴实的农村妇女，比较保守，我姐出嫁的那天穿了个抹胸的婚纱，老公正准备抱着她出闺房的时候，妈妈拿着个白色的外套说：给，必须给我披上，谁家结婚光着个膀子啊！！！');
INSERT INTO `tpshop_joke` VALUES ('2426', '同事说：如果全世界的人都死了，就剩我和刘亦菲就好了，旁边的主任说：只要黄瓜、茄子不灭绝，她就看不上你！');
INSERT INTO `tpshop_joke` VALUES ('2427', '跟男朋友去超市，因为闹点小别扭我就跑了，第一次，我还以为他会来找我，没想到他去服务台让播音找我，整个超市都回荡着“王＊＊同学，你的爸爸在服务台等你，请速到服务台，不要跟陌生人说话。”。。。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('2428', '儿子非常调皮，经常令我头痛不已。那天我对我妈感慨：现在小孩真不好管！我妈冷冷地说：二十多年前的小孩同样不好管！');
INSERT INTO `tpshop_joke` VALUES ('2429', '找了一个特傻的老公。每次吃完饭都用石头，剪刀，布决定谁输谁刷锅。每次都是我出剪刀，他出布。这种状况已经坚持3年了。');
INSERT INTO `tpshop_joke` VALUES ('2430', '听说初中班主任突然间神 志 不清，基本不认得人了。过年和同学一起去看望他，见面时，同学都纷纷向前问，老师还记得我吗，老师都只是摇摇头，看着同学都问了，我走向前准备问。老师突然来 神，指着我：我记得你，当初你抽烟被我发现，逃跑时推了我一把，头撞墙上了，我变这样可能就是你 造 的。');
INSERT INTO `tpshop_joke` VALUES ('2431', '‍‍天热了，中午我只穿着一条内裤在床上睡午觉。 正睡得香，老婆叫醒了我：“你脱完了，干脆来做事。” 我高兴地答应她：“来嘛。” “到卫生间把席子洗了。” lz顿时呆了……‍‍');
INSERT INTO `tpshop_joke` VALUES ('2432', '内裤再美～终究也只能装B');
INSERT INTO `tpshop_joke` VALUES ('2433', '‍‍‍‍‍‍音像商店来了一位顾客，他决定用自己的年终奖买一套音响。 服务员很热情的介绍打动了顾客，决定先体验体验。 一小时以后，顾客走出了音响店，兴奋地说：“原来夏威夷这么好！今年我就去！”‍‍‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2434', '都说孤枕难眠，昨晚又放一枕头，结果还是一夜没睡着，丫的，这不骗人的么');
INSERT INTO `tpshop_joke` VALUES ('2435', '两个人哪怕年龄、背景、三观、爱好、财力、智力都相近，只要吃不到一起去，一样没法做朋友。我口水淌到地下了，她却说好恶心，那就是分分钟绝交的节奏；能一起狂吃麻辣烫，大嚼锅包肉，猛撸羊肉串，怒吞煎饼果子，才是友谊的起点。');
INSERT INTO `tpshop_joke` VALUES ('2436', '和一个哥们去小饭馆里吃饭。 点了很多菜 。考虑到吃不完。于是打包。服务员帮我 们弄好后。那哥们拿起盘子猛舔。。一边 舔一边说。盘子里那么多油。我是不会有 让你们有制作地沟油的机会的。。。');
INSERT INTO `tpshop_joke` VALUES ('2437', '深夜无聊，我拿手机摇啊摇，摇了一会摇到一美女，还在我100米内，赶紧加了好友问她:“美女，大半夜不睡觉寂寞不？” 美女道:“给我倒杯水” 我说:“你发错了吧？” 然后隔墙老妈大喊:“臭小子，还不快点给我倒杯水！”');
INSERT INTO `tpshop_joke` VALUES ('2438', '建筑师为一个大富豪建造了一座陵墓，富豪问忙了两年的建筑师：“也许还缺点什么吧？”建筑师说：“现在就缺你了！”');
INSERT INTO `tpshop_joke` VALUES ('2439', '中国人，日本人，美国人去原始森林冒险。结果被土著人捉住，酋长说：‘’我可以放了你们，但你们必须被打50大板子，打时，我可以满足你们一个愿望。美国人先打，他说：‘’给我屁股上垫上10个垫子‘’，结果垫子很薄，被打昏了。该日本人了，他要40个垫子，结果一点也不疼，该中国人了，他笑嘻嘻的说：‘’把那个日本鬼子垫我屁股上。‘’');
INSERT INTO `tpshop_joke` VALUES ('2440', '今天早上去吃牛肉面，吃着吃着吃出一只蚊子。我问老板这是什么，老板说这是肉。。。。。然后我脑抽的说了一个：“哦”然后吃了下去。');
INSERT INTO `tpshop_joke` VALUES ('2441', '&nbsp; &nbsp; &nbsp;老婆：老公，今天的臭豆腐好吃吗？ &nbsp; &nbsp; &nbsp;老公：嗯，好吃 ，挺香的，只是闻着有点熟悉，你在那儿买的？老婆幽幽道：不是买的，我买了一块豆腐，用你穿过的袜子捂了一夜…');
INSERT INTO `tpshop_joke` VALUES ('2442', '、邻居家有喜事结婚。混进去吃一餐，刚到人家门口就被拦截了。守门的问道，邀请函。然后我就顺口说了句，什么！你知道我是谁吗？看到没。那个人穿裙子的是我妈！然后…保安真让我进去了。。。。');
INSERT INTO `tpshop_joke` VALUES ('2443', '有一群孕妇建了一个群，名为：育儿交流群。她们在里面只做一件事：骂自己的老公....');
INSERT INTO `tpshop_joke` VALUES ('2444', '今天我在空间发了条动态，有两个朋友一男一女、他们根本不认识，却在评论那里聊的很嗨皮，互相回复了二十多条，就在他们快要交换手机号码的时候我把说说删了……');
INSERT INTO `tpshop_joke` VALUES ('2445', '快递来送快递，拿完快递还迟迟不走，我们就这样一直对视着，表面上我很冷静：他不会是喜欢上我了吧？难道要跟我表白？我该怎么办？我还没想完，快递员说：笔还我吧。');
INSERT INTO `tpshop_joke` VALUES ('2446', '美国人:“爸我想打篮球”“去吧，儿子，你爸我喜欢乔丹，不过看你的身高，当中锋会不错。”欧洲人:“爸我想打篮球”“啊，篮球啊，本来我想让你踢足球的，不过你喜欢就去吧”中国人:“爸我想打篮球”“作业做完了么”“做完了，我想……”“做完了就不能复习吗，给老子去复习去。”');
INSERT INTO `tpshop_joke` VALUES ('2447', '农村娃都知道牛左边腰上那个窝要是鼓出来了就说明牛吃饱了。小时候放牛，为了让牛吃饱，老妈说牛没吃饱我就没饭吃。一天我看着大水牛鼓鼓的肚皮，赶着牛回家，边走边哼着歌儿。突然，水牛屁股一撅，尾巴一翘。不好！！！牛要拉屎（拉屎的话那个窝就会陷回去）。急得我都快哭了，拿出鞭子就抽，边抽边吼道：不能拉，不能拉，要拉回去拉，妈妈看完以后才能拉');
INSERT INTO `tpshop_joke` VALUES ('2448', '“假如有一天你在撒哈拉沙漠快要渴死了。再不喝马上就会死！有两杯水 一杯姨妈血 一杯尿 你会选那杯？一定要选的话 我选尿。想到你这么变态，有两杯水为什么不喝！”……');
INSERT INTO `tpshop_joke` VALUES ('2449', '两人在聊天，一女说如果你和男朋友分手了，却发现自己怀孕了，你会怎么办？另一女的说我会偷偷把孩子生下来，在他结婚的时候给他带去，然后跟他说：我养了这么久，该你了……');
INSERT INTO `tpshop_joke` VALUES ('2450', '都说夫妻间需要默契，确实是这样的，你看我邻居那两口子，他们说好今天早上，一个出差去机场，一个去公司上班，两人同时出门，一个小时后，两个人在离家20多公里外的宾馆相遇了，这就是默契！这不，昨天下午他俩又去民政局了。');
INSERT INTO `tpshop_joke` VALUES ('2451', '‍‍冠军运动员患重感冒。 医生给他量了体温说：“你高烧41度。” 运动员忙问：“世界纪录是多少？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2452', '‍‍我：“医生我的牙齿好痛，拔了有什么副作用没有？” 医生：“没有，只是…只是…” 我：“只是什么？” 医生：“只是你的体重可能会轻些。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2453', '今眯着眼想事。二货媳妇摸着我的脸自言自语的说真是一张迷倒众生的瓜子脸，心里一阵狂喜，等着接下的赞美，看我睁开眼二货媳妇说道别误会，西瓜子……我去西瓜子……');
INSERT INTO `tpshop_joke` VALUES ('2454', '今天发现小侄女在看蚂蚁，我对她说你要学蚂蚁精神不怕苦勤奋！她眼睛翻了翻说：得了吧！蚂蚁最贪玩了我去哪儿都能碰到它们集体旅游！');
INSERT INTO `tpshop_joke` VALUES ('2455', '‍‍前些天在空间里看一同事发的照片，以为他在那天结婚了。 第二天故意寒暄了一句，“你结婚咋不叫我呢？真是的！” 结果！结果他说：“不是结婚，拍的婚纱照而已。” 就在刚刚，我收到了他的请帖。‍‍');
INSERT INTO `tpshop_joke` VALUES ('2456', '一次，老师问一个二货：“读书破万卷，下笔如有神是什么意思？”二货答：“读一万本书，下笔像神仙一样。”全班石化……');
INSERT INTO `tpshop_joke` VALUES ('2457', '‍‍刚买了新车，老公爱不释手。 有天他喝了很多就，居然打开车门上了车，我拉住他：“喝了酒不准开车。” 老公：“我只想到车上坐坐。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2458', '上小学二年级的妹妹在 一边认真的写作业 我凑过去说我考考你吧，看看你变聪明了没有，于是我就对她说，你用“驾驭”造个句子，在一旁的爸爸说我先来，“第一次骑马，很难驾驭呢”在一旁的妹妹说“马儿马儿快快跑，驾驾……吁”然后， 就没然后了');
INSERT INTO `tpshop_joke` VALUES ('2459', '‍‍那天，老婆出差了，于是我去幼儿园接孩子。 在校门口等待的功夫。就听见一个先接到孩子家长跟女儿说：“别人打你的时候要是不疼，你就忍着，要是疼一定要打回去。” 这肯定是亲爹啊！要搁我，我也会这样想。‍‍');
INSERT INTO `tpshop_joke` VALUES ('2460', '　　最讨厌坐电梯一层一停的，你妹的到2楼也做电梯。 　　接着往上5楼门又开了，进来个大叔问，哦，这是往上啊！ 　　大哥你进来就是为了问个路吗？');
INSERT INTO `tpshop_joke` VALUES ('2461', '一天早晨出门在路上捡了一部iPhone4s（2010年当时四千多一部），犹豫了下，先去工作了。当时都没用过智能手机。心想失主会给我打电话的，结果中午真给我来了个电话。\r\n 各种威胁。说是能高科技锁定我。又是能把手机锁定让我用不了，我当时在吃中午饭。心里就火了，手机上就那几个按键，我都试了一遍总算把手机关了，我想你再牛逼不开机联不了网。我就看你怎么锁，要是这都能让你锁定我我也认了！\r\n 后来回到家，上网一查，知道苹果还真能定位锁机，就把手机卡拔了。在网上下了几个游戏当个玩具用了两年。\r\n 说心里话，当时他要是说两句好话，哪怕请我吃顿拉面，我都能给他，我当时也不会用。可是这人就喜欢装逼（当年还没这词）。多年后我才觉得装逼遭雷劈真是有道理！');
INSERT INTO `tpshop_joke` VALUES ('2462', '在一场女性特殊技能评选中，“徒手解内衣”仅仅排在第五位，第四是“洗澡时站着尿尿”，“穿高跟鞋健步如飞”和“上公共厕所扎马步”分列第三和第二位，榜首无悬念，被“逛街永远不累”占领。');
INSERT INTO `tpshop_joke` VALUES ('2463', '昨晚和喜欢的妹子一起值班，编个故事想探探她对我的态度：“昨天我梦见你了。”“ 梦见我什么？”“ 我梦见我在办公室向你求婚 哈哈。”“那我答应了吗？”“你刚要回答我，结果一个学生进来了，一下我就醒了。所以，现在可不可以说说，你愿意吗？”妹子脸红扭捏，刚要开口，这时一个学生推门而进：老师，，，，，？？？？');
INSERT INTO `tpshop_joke` VALUES ('2464', '一个屌丝，上学的时候天天戳女神的车胎，然后假意的跟女神邂逅。不久后他们关系越来越好，屌丝感觉有希望了。一天，屌丝鼓起勇气对女神说，我每天戳你车胎，就是为了能在路上与你相见，你能做我女朋友吗？女神眼含热泪说，明天你就知道了。第二天，屌丝在放学路上给几个混混打了个半死。');
INSERT INTO `tpshop_joke` VALUES ('2465', '女友在广州上学，前两天回来了，去她家玩。心血来潮中午给她家下厨做三个菜，那菜让我做的黢黑，好心的叔叔阿姨还是吃了，她也鼓励我还可以。没吃完，晚上把剩菜拌饭喂狗，不一会我那二货媳妇问他爸爸，狗吃了多少啊？岳父大人淡定的说：狗...可能今晚不饿...');
INSERT INTO `tpshop_joke` VALUES ('2466', '邻居住了个小夫妻，女的怀孕有几个月了，前几天住院，今天才回来。晚上室友过去跟他们说，恭喜啊！ 男的上去就啪啪啪给室友几耳光，室友当时就火了，你妈的不识好歹。 现在才知道，女的流产了。 室友跟我说这事，我说：你这几耳光一点都不冤！');
INSERT INTO `tpshop_joke` VALUES ('2467', '男孩的哥哥和他老师是好哥们儿。小男孩成绩不好，老师骂他，他站着挨骂。老师反而觉得不好意思了：“咳……虽然你学习不好，但是还是很乖的。”男孩摇摇头：“没办法，我哥说不能和老师顶嘴。老师一愣笑了：“你哥说得对。”男孩继续说：“嗯，他说老师你的嘴只有他能顶。”“叫他死过来我要杀了他！”');
INSERT INTO `tpshop_joke` VALUES ('2468', '富翁临终前把三个儿子叫到床边，给他们每人一支筷子让他们折，儿子们轻而易举地折断了。富翁又给他们每人一双筷子，儿子们稍稍用力就折断了。富翁再给他们一打筷子，儿子们轮番上阵费尽全力都无法折断。富翁问：“你们都明白了吗？”儿子们纷纷点头，齐声答道：“棒子靠组合！”富翁卒。');
INSERT INTO `tpshop_joke` VALUES ('2469', '公交车快到站了，一哥们拼命挤到门口，对着门喊到，嘛咪嘛咪吽。门居然开了，这哥们下车扬长而去，一车人凌乱了，司机哈哈大笑。。。。 司机师傅你也太配合了吧。');
INSERT INTO `tpshop_joke` VALUES ('2470', '今天在路上看到一男生向女友告白。女生说：“你如果能马上给到我彩虹，我就和你交往！”男生问：“那你想让它出现在哪里？” 女生伸出左手说：“在这里。” 紧接着让我惊掉下巴的一幕出现了：只见那男生抓起女生的左手按在地上，然后用力的踩红了．．');
INSERT INTO `tpshop_joke` VALUES ('2471', '1、“爸，冰箱里的鸡腿呢？”“你妈炸了。”我。。。2、晚饭后陪老妈逛街。。看到别人母子。老妈凑过来说：“你看那个妈妈好像也在遛她们家的单身狗。”3、我问我爸有多爱我？我爸说以前为了让你改正错误，特地请了半天假回来打你！4、昨天老妈告诉我，下班时去超市再给她买个苍蝇拍。我问：“家里不是有一个苍蝇拍吗，还买？”老妈说：“现在的苍蝇胆子也太大了，居然经常趴在苍蝇拍上玩。我一拿苍蝇拍它就飞跑了，一会儿见我放下苍蝇拍，它又飞回来趴在上面，真是气死人。你给我再买一个苍蝇拍，一个放在那里诱敌深入，一个拿在手里主动出击，看它厉害还是我厉害。”5、给儿子过生日。儿子吹灭了生日蜡烛，对我说：“爸，我能许个愿不？”“什么愿望”“明年生日能在蜡烛下面放个蛋糕吗？”“说出来就不灵了。”“。。。。”6、今天去看了大圣归来。 我旁边有个小孩儿问他妈妈 “这个不是动画片么？为什么有这么多大人来看？”他妈妈回答： “因为他们一直在等大圣归来啊，等啊等啊，就长大了。”');
INSERT INTO `tpshop_joke` VALUES ('2472', '儿子：妈妈，你是Woman，年纪大一点的女的是Woman！！！\r\n妈妈：年纪小的呢？\r\n儿子：年纪小的是Girl！\r\n“那我是Girl，我年轻，，我不要做Woman”\r\n“不行的，你已经有一点年纪大，还有两根白头发，你就是Woman”\r\n“两根不算”\r\n“那你有三根，我看错了，我真的看到你有三根”\r\n很想是Girl的妈妈抓狂…一定要是Girl！！\r\n最后，没办法…\r\n“妈妈，可是我觉得Woman是最漂亮的，真的最好看”\r\n“真的，那好吧，我是Woman”\r\n“嗯，那说好了，你是Woman，不可以变了！”');
INSERT INTO `tpshop_joke` VALUES ('2473', '最近在六 间房看直 播，飞仔 直播摸他徒弟 悦宝 的奶子，房间被封了，只准进2000个人。\r\n 六 间房的管理员太不人性化了，凡是进 飞仔 房间的都是冲 悦宝 那对大咪 咪去滴，咪 咪不让抖还咋活啊！');
INSERT INTO `tpshop_joke` VALUES ('2474', '昨天晚上宿友对我说:这么晚了还不睡？你知道嘛，睡前玩手机，影响性生活！当时我就一拖鞋扔过去了，我TM有性生活还玩手机？');
INSERT INTO `tpshop_joke` VALUES ('2475', '“莪”这个字，在我初中的时候，很多女生都喜欢用，打字聊天的年代“喜欢莪吗？”“你爱不爱莪？”等等……现在回想起来，我当时真的是太年轻了！');
INSERT INTO `tpshop_joke` VALUES ('2476', '小李去女神的空间闲逛，见一相册名叫「我的裸照」，密码提问：叫声妈就给你看。他猥琐地敲出「妈」字，居然解密了，打开一刹那，小李泪流满面，里面赫然一个大字：乖！');
INSERT INTO `tpshop_joke` VALUES ('2477', '丈夫：我要离婚，我们的婚姻一点儿也不公平。 妻子：你脑子有毛病吧，哪里不公平了？ 丈夫：结婚以后，我把工资，房子，车子所有的东西都给你了。 妻子：但是我也把做饭，扫地，买菜交给你了啊。。。');
INSERT INTO `tpshop_joke` VALUES ('2478', '在初中的时候，暗恋同桌的女生很久了，一直不敢表白，初三期末，在最后一天喝了口高粱壮壮胆，写了张表白字条给她，她看到后也写了一张给我，然后说了一句想清楚了再来找我。（字条上的字“氼卋婄卋挋蚮卌犿仴”）然后我凌乱了');
INSERT INTO `tpshop_joke` VALUES ('2479', '和前女友分开三年了，当初是她甩的我和别人好上了，现在频繁访问我空间，到底什么意思！');
INSERT INTO `tpshop_joke` VALUES ('2480', '我被媳妇赶出家门了，原因是老婆一瓶水（化妆品）用了几个月都没用完，今天我在卫生间往里面装水的时候被发现了。');
INSERT INTO `tpshop_joke` VALUES ('2481', '公交车上你男的踩我一脚，我这小暴脾气马上就上来了，向他投去凶悍的眼神，谁知他弱弱的来了句，有话好好说，我们抬头不见低头手机见，是不是！');
INSERT INTO `tpshop_joke` VALUES ('2482', '舍友昨天去见网友，我：怎么见网友怎么样了。舍友：感觉不错。我：哦？怎么样的女孩让你这么有好感啊。舍友：我喜欢她吞吞吐吐的样子。我：嗯？现在还有这么单纯的女孩？不是结巴吧？舍友：不是那样。我：...');
INSERT INTO `tpshop_joke` VALUES ('2483', '男：大师，为何我已经23岁了，家产才只有两个亿呢？大师默默地拿着打火机点燃了他的上衣。男：哦！大师，你是在提醒我要努力不怕火海的闯下去吗？大师大怒曰：不吹你会死。');
INSERT INTO `tpshop_joke` VALUES ('2484', '表弟小时候很调皮，经常和别的小朋友打架。又一次在在和小朋友打架时，被欺负的小朋友说：我家有条狗让我家狗来咬你。表弟说：你家有狗我家还有猫呢？对方又说妈谁没有，没有你从哪儿来的...');
INSERT INTO `tpshop_joke` VALUES ('2485', '女同事双十一败完家后整天在办公室说：不买了不买了，再买剁手！今天双十二又忍不住了，我说手剁了吗！同事奇葩的来一句：谁让我是千手观音呢！哎…');
INSERT INTO `tpshop_joke` VALUES ('2486', '甲：听说你被炒鱿鱼了，为啥？乙：哎！别提了。公司年会聚餐。经理给我倒了杯酒说：量小非君子，来！是男人就把这杯酒闷了。甲：你难道没喝么？乙：喝了，喝完我拿出一瓶敌敌畏，放他前面说：无毒不丈夫，是男人也把这瓶干了···甲：(╯﹏╰)···');
INSERT INTO `tpshop_joke` VALUES ('2487', '孩子上网不学习的，为了让孩子学习给孩子出题放电脑跟前，把电脑开机密码设成题的答案的，我也试试，女儿直接把题目撕碎了，我也忘了给她出的啥题目了，现在电脑开不了机了，真是不作死就不会死啊！');
INSERT INTO `tpshop_joke` VALUES ('2488', '‍‍以前在外地上班，就搞上一个不错的妞。 刚在一起的时候，我说：“你很像我老婆。” 看到她那高兴的样子，我心还想这女孩到底想什么啊？ 后来她看到我老婆的时候，默默的说一句：“还真像。” 再后，就没有了后来。‍‍');
INSERT INTO `tpshop_joke` VALUES ('2489', '魔鬼：上帝，我可以投胎吗？\r\n上帝：可以。\r\n魔鬼：我不想再做魔鬼，我想像天使那样全身洁白，还要有一对翼，但是我仍然想吸血。\r\n上帝：那好，你就投胎做姨妈巾吧。');
INSERT INTO `tpshop_joke` VALUES ('2490', '现在才意识到，谈恋爱无非就是两种结果：一是，各回各家，各找各妈；二是，你妈变我妈……');
INSERT INTO `tpshop_joke` VALUES ('2491', '本人性别男，悄悄欠佳一些阳刚之气，但也是男的！！有一次坐地铁，车厢人不是很多，正好肩膀有些痒，我就掀开外套随手抓了几下，没想到后边有个女的走过来：“是不是肩带掉了，我来帮你整吧”！我………艹');
INSERT INTO `tpshop_joke` VALUES ('2492', '当兵的时候发生过一件糗事，打靶的时候有一哥们装的空包弹没下，试靶子的时候走火了。前面举旗的一个“砰”地一声躺地上了，走火那哥们都快吓尿了。GC来了，被击倒的那哥们爬了起来摸了摸自己屁股，大吼:“差点被你爆了”！');
INSERT INTO `tpshop_joke` VALUES ('2493', '以后，如果你的男友不愿意陪你逛街，你就眨眨眼：“晚点去优衣库帮你看衬衫好不好？”');
INSERT INTO `tpshop_joke` VALUES ('2494', '“你有梦想吗？”“……”“你连一点梦想都没有，活着还有什么意义？”“我有女朋友……”');
INSERT INTO `tpshop_joke` VALUES ('2495', '有一天，乾隆和刘罗锅在金殿议事，刘墉突然想放屁，就给皇帝说：“连续看折子太累了，我想大声吟诗一首，缓解一下疲劳”。于是背起了骆宾王的咏鹅诗。趁机把屁给放了。不成想，刚背完诗。皇帝就问：“你刚才背的啥诗歌？我一句没听到，因为你的屁太响太臭了。');
INSERT INTO `tpshop_joke` VALUES ('2496', '今早上坐公交车，看一男一女在那吵吵。\r\n女的说：你懂个鸟啊！\r\n男的说：那玩意儿我应该比你懂的。\r\n女：那TM你给老娘说说那是什么味儿！\r\n男。。。');
INSERT INTO `tpshop_joke` VALUES ('2497', '　　lz男！和女朋友异地，昨天晚上跟女朋友那个干柴烈火…到最后那一刻的时候，突然意识到自己不能那个啊…对的，我尿床了…尿…床…了…');
INSERT INTO `tpshop_joke` VALUES ('2498', '‍‍‍‍本人当学徒那会儿，一旦有点风吹草动，师傅就会开始喊我的名字。 于是我远远地从工具房跑过来，他说：“给我拿个螺丝刀。” 然后我又跑回去拿工具，再跑过来。 反对多次后无果，终于有一天，我不胜其烦，每样系列的工具收集到一个箱子里。 再叫我的名字，我就抬箱子过去一扔：“给，要什么自己挑。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2499', '有一同事，男，这两天来了好几个快递。今天，又有一个快递送到我们公司，当时他在洗手间。前台喊他的名字：“××，你的快递！”我当时脑一抽，本来想说他又来快递啦，结果变成：“他又来那个了。”');
INSERT INTO `tpshop_joke` VALUES ('2500', '“你知道穷光蛋是什么意思么？”\r\n“就是穷的连裤头都买不起，只能光着蛋。”');
INSERT INTO `tpshop_joke` VALUES ('2501', '小偷潜进一户人家，翻找了半天也没有找到值钱的东西，只看到一个保险柜。 看到卧室只有一个女主人，于是把她叫起来问道：“快把密码告诉我！” “我是不会说的，就算糟蹋我也不会说的！” 小偷瞄看了一眼：“你想的美！”');
INSERT INTO `tpshop_joke` VALUES ('2502', '笑尿～今天发历史考试试卷，给清朝五大将军领地写人名，同桌就知道个伊犁将军，还写成了伊利，剩下填的，光明将军 蒙牛将军，三鹿将军，还尼玛有个旺仔将军是什么概念！');
INSERT INTO `tpshop_joke` VALUES ('2503', '听说结束一段友情最直接的方法就是借钱不还，不过这好像在我朋友身上没什么用。”\r\n“难不成你的朋友是土豪？！”\r\n“是不是土豪我不知道，我跟他借了三次，一毛钱都不肯借！看来他还是挺珍惜我们这段友情的啊！”');
INSERT INTO `tpshop_joke` VALUES ('2504', '‍‍小时候电视热播西游记，家长不让看。 我只能满怀愤懑的写作业，这时桌前飞来一只苍蝇，我心里一动，问到：““悟空，是你吗？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2505', '死了老公之后，寡妇在自家后院种了黄瓜，茄子，山药，还有玉米，村里的留守妇女没事就喜欢去寡妇家串门……');
INSERT INTO `tpshop_joke` VALUES ('2506', '小王拉稀一个多月了，不得不到医院求诊，看了化验报告大夫平静地说：“你没病，天太热，屎化了。”');
INSERT INTO `tpshop_joke` VALUES ('2507', '小时候别人老说我长得丑，后来初中有一群小混混嘲笑我，我再也受不了然后冲上去和他们撕打起来，然后我再也听不到他们嘲笑我了，因为我被打聋了。。。。');
INSERT INTO `tpshop_joke` VALUES ('2508', '爸爸正看电视，女儿写作业。突然爸爸对女儿说：宝宝，你数学不好，今天爸爸给你辅导辅导。 宝宝：好呀。 爸爸：你今年6岁，老爸37岁，40年之后爸爸比你大多少？ 宝宝疑惑地想了一会儿说道：40年之后我46岁，爸爸你已经死了。爸爸： 哈哈哈哈哈哈哈哈！真棒！我要给你报三十二个兴趣班。');
INSERT INTO `tpshop_joke` VALUES ('2509', '的人，首先你是哪国人？然后这些东西你都是听谁说的？在者这些国家你是否去过了解过他们的国情？为什么贬低祖国的yan论你会那么积极的响应？你对祖国了解多少？你对国际she会了解多少？所以请你们不要盲目发表极端yan论，这会让人觉得你是个傻屌。');
INSERT INTO `tpshop_joke` VALUES ('2510', '‍‍‍‍上学时，一天放学下楼梯人很多，走的快了不小心踩到前边一妹子。 正不知所措时那妹子扭过头来攸攸来一句：“怎么，你想上人么？” 这什么节奏？我是不是错过什么了？‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2511', '家里买了张新床，丈夫感慨：“这张床可够宽的，能睡三个人。”\r\n妻子一直想要孩子，便说：“只要你愿意，我没问题。”\r\n丈夫激动坏了，说：“只要你不吃醋，我更没问题啦。”');
INSERT INTO `tpshop_joke` VALUES ('2512', '你的冰肌玉骨，让我倍感满足。长夜漫漫，我在你身上不停地变换着姿势，享受着你带给我的快乐。感谢这个夏天，让我拥有了你，我亲爱的……凉席。');
INSERT INTO `tpshop_joke` VALUES ('2513', '最痛心的三角恋是什么 我喜欢假期 假期喜欢作业 作业喜欢我 ！！！');
INSERT INTO `tpshop_joke` VALUES ('2514', '今天LG惹我生气了，百般哄我，问我究竟要哪样。。我看时机差不多了，扔一盒套套他前面，说除非今晚全用完。。等我洗完澡出来，那货在天花板上布置了一堆气球，我说她还挺懂浪漫，一看套套盒空了。。空了。。');
INSERT INTO `tpshop_joke` VALUES ('2515', '‍‍‍‍有次去九寨沟旅游，住的宾馆没有水。 早上同住的女生霸占了厕所，我只好到处找厕所。 正好看见隔壁的房间门开着，应该是刚退房，于是果断进去上个大。 上着上着，突然听见房间里有说话的声音，是那间房的客人又回来了！ 急的我在厕所里不敢出去，关键是还没有水冲最后只有硬着头皮假装走错房间。 留下那对夫妇一脸错愕的表情逃走了！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2516', '那些说：\r\n穆斯林比国人善良。\r\n美帝人民比国人有素质。\r\n欧洲国家比国人重视大自然。\r\n某岛国比国人懂礼貌。\r\n的人，首先你是哪国人？然后这些东西你都是听谁说的？在者这些国家你是否去过了解过他们的国情？为什么贬低祖国的yan论你会那么积极的响应？你对祖国了解多少？你对国际she会了解多少？所以请你们不要盲目发表极端yan论，这会让人觉得你是个傻屌。');
INSERT INTO `tpshop_joke` VALUES ('2517', '我那个逗比舍友，昨天晚上喊着我一起发段子，他起的名字叫做:好名都被狗起了呜呜呜。有好多人都评论他，说楼主名字起的好，这货竟然洋洋得意的对我说，好多人都在夸他。。。我强忍着没告诉他真相。。。');
INSERT INTO `tpshop_joke` VALUES ('2518', '老师带学生去监狱接受教育，回来问大家有什么感想。一学生答：“现在的监狱跟解放前国民党监狱一样的”。老师很惊讶，问：“为什么？是环境很差吗？吃的不好吗？”学生说：“不是的，是里面关了好多G产D员。”');
INSERT INTO `tpshop_joke` VALUES ('2519', '晚上睡觉被蚊子咬醒了。老公枕边有一瓶驱蚊药。遂伏在老公身上寻找。\r\n老公迷迷糊糊的问：“怎么了？”\r\n答：“药。”\r\n老公轻拍了拍我的背：“快睡吧，大半夜的，要什么要？”');
INSERT INTO `tpshop_joke` VALUES ('2520', '高中那时住校。一天晚上，一室友学大人刮胡子，因为不懂，拿刮胡刀沾点水就往脸颊上刮，不小心，刮出一长道口子来，鲜血瞬间流满脸颊。把我吓坏了，赶紧陪他去校门口旁边一药店，买了仨止血贴都没止住，那货吓得在那哭开了，弄得买药那女的实在没辙，从包里拿出一姨妈巾（当时纯，真不知此为何物）来，贴他脸上了，血止住了！我们还特高兴地谢人家来着，然后大摇大摆回宿舍了……事后大悟，想起看门那老头看我们的眼神，一辈子也忘不了，忘不了。');
INSERT INTO `tpshop_joke` VALUES ('2521', '　　某君送千金去托儿所的路上告诉千金：“在那儿不要交不三不四的朋友省的惹麻烦··”千金告诉本君曰：“你放心粑粑，我没有这样的朋友，我的朋友都很二！”');
INSERT INTO `tpshop_joke` VALUES ('2522', '与前任女友偶遇街头，她已经结婚了，我们寒暄了两句，她的儿子突然问：这位叔叔是谁？这货神回复：他是差点让你有哥哥的人。');
INSERT INTO `tpshop_joke` VALUES ('2523', '在学校食堂吃盖饭，，选来选去选了一宫保鸡丁，，吃着吃着发现一鸡屁股，，就把它挑出来放在桌边，，，这时一同学走过来看见桌上的鸡屁股，说：嘿，哥们 ，赏菊呢！！');
INSERT INTO `tpshop_joke` VALUES ('2524', '【研究称：吸“二手屁”有利远离癌症】英国艾克赛特大学的科学家指出，正确剂量的硫化氢（屁的主要成分）能使细胞中的线粒体平稳运转，确保细胞可以继续和病菌战斗，有助于降低得癌症、糖尿病等疾病的风险！所以，下次当有人偷偷放了一个臭屁的时候，你所要做的就是优雅的深吸一口气。');
INSERT INTO `tpshop_joke` VALUES ('2525', '　　一次在某宝网上寻到一家新店，中意了一件商品 　　：“老板在吗。” 　　：“在的亲” 　　：“我要XXX商品，有货吗” 　　：“有的哦亲” 　　：“好的，我就要它了，包邮吗” 　　：“？，亲为什么要包油呢？” 　　：“防水啊，吼吼吼吼吼” 　　隔天，我收到了快递，，外面包了一层油。。 　　你妹啊，，真的是新手开店啊。，');
INSERT INTO `tpshop_joke` VALUES ('2526', '想起我十几岁的时候，有个奇葩朋友，很少和我们一般大的朋友一起玩，专和七八岁的小孩玩。我们特别不理解他，直到有一天，我们在一处偏僻的楼后看到他坐在一张破沙发上，搂着一个小女孩，前面一排小男孩跪着磕头喊皇上万岁。我们才知道他是有多享受生活。');
INSERT INTO `tpshop_joke` VALUES ('2527', '喝酒时扯淡，惊觉，乔布斯，完全就是《天龙八部》中乔峰！都是自小没见过亲生父母，被平凡养父母带大。少年成名，一跃登顶。然后在世人瞩目中，众叛亲离，含恨隐去…再然后，王者归来，傲凌绝顶。最终，盛年之时，颠峰乐章戛然而止，留给世界一片惊愕。他俩共用一个称谓：乔帮主！');
INSERT INTO `tpshop_joke` VALUES ('2528', '去朋友家玩，他上初中的儿子问我一道选择题，我看了半天，只大概知道A或者C有可能是正确的，但我确定B选项肯定不是，我就说答案应该在A和C之间，结果那熊孩子一下笔就填了个B。。。');
INSERT INTO `tpshop_joke` VALUES ('2529', '“周杰伦到周杰家被周杰轮。求下联！！”回复：“范玮琪到范伟家被范伟骑！！怒求横批！”');
INSERT INTO `tpshop_joke` VALUES ('2530', '今个陪女友看电影呢，后面有对情侣一直在说腻歪的话，女的问男的：我是你的什么呀?男的：啥?女的：我是你的优乐美!男的：哎呀!优乐美啥时候出桶装的了?');
INSERT INTO `tpshop_joke` VALUES ('2531', '和老婆闹矛盾，她闺蜜来劝架。老婆像闺蜜诉苦，说我不是男人。闺蜜：“别伤心了，你老公还不男人啊？比我家男人强多了！”老婆：“算了吧，至少你家男人比他温柔！”');
INSERT INTO `tpshop_joke` VALUES ('2532', '　　某男对某女说“就你这样没人要你，除非公狗要你，”“我嫁不嫁挨着你了，你更没人要，除非母狗要你”结果我说了一句“别吵了，狗咬狗”某男听了直接说“我非常赞同你这句话，她TMD就是一条母狗”他自己还没反应过来里说了这么一句话，结果班里的同学哄哄大笑说“母狗和公狗一对啊”');
INSERT INTO `tpshop_joke` VALUES ('2533', '问：男人纵欲过度会肾虚，辣么问题来了，女人纵欲过度会怎样？\r\n神：也是男人肾虚……');
INSERT INTO `tpshop_joke` VALUES ('2534', '问：男人纵欲过度会肾虚，辣么问题来了，女人纵欲过度会怎样？\r\n神：也是男人肾虚……');
INSERT INTO `tpshop_joke` VALUES ('2535', '宝宝趣事一：今晚宝宝在便便，那个臭气熏天啊！我们都说睿睿的便便臭死了！宝宝听后接口答：香！而他的一只手却捏住他自己的鼻子。哈哈！我们都被笑翻了！');
INSERT INTO `tpshop_joke` VALUES ('2536', '宝宝趣事二：今晚宝宝想拉开装药的柜子，他爷爷不让用脚堵住柜子任宝宝怎么使力气都开不了，宝宝立刻想到一个好办法，他走到对面的椅子旁拍拍椅子对他爷爷说：坐坐！他爷爷听到后走了过去问他：要爷爷坐是吧？这时宝宝不吭声马上跑到柜子边开始拉柜子！他爷爷顿时也识破了宝宝的诡计！真的没想到就17个多月的宝宝怎么就有那么多的心机！');
INSERT INTO `tpshop_joke` VALUES ('2537', '粑粑昨晚刚到家，对于平时和粑粑打电话视屏时总会大声叫＂粑粑＂的宝宝来说却表现得很反常，宝宝害羞得不敢叫粑粑，但是却又喜欢跟着粑粑，看他粑粑走去房间他也跟去但是一看到他粑粑转身看他时他又赶快假装专注的看自己玩手指，那个模样好玩极了！');
INSERT INTO `tpshop_joke` VALUES ('2538', '昨天晚上宝宝跟着粑粑进房间，那时房间里没开灯，宝宝进去就说：暗暗。粑粑问宝宝：那你怕不怕？宝宝说：不怕。粑粑再问一次说：你怕不怕？宝宝大声的回答：不怕。粑粑说：那好，你在房间呆着我先出去了。说完粑粑就走出房间，宝宝见势不妙赶快匆匆随粑粑跑出房间。这个胆小鬼！');
INSERT INTO `tpshop_joke` VALUES ('2539', '我一个朋友，有选择困难症。\r\n现在要考交警，我问他，路上那么多车，大小不一样，颜色不一样。你查车怎么查？\r\n结果第二天就说不考了，回来把我揍了.说是在家里想了一夜还没想好。');
INSERT INTO `tpshop_joke` VALUES ('2540', '公交车上，老头：让个座位！小伙：那么多空位你不坐？老头：这个位置风水好！小伙：行！我让！您就埋这儿吧！');
INSERT INTO `tpshop_joke` VALUES ('2541', '唐僧师徒来到了雷因寺，佛主：玄奘八十一难未满，还差一难为何来此？结果唐僧给了佛主一大嘴巴子，佛主大怒把唐僧关了起来还要处死唐僧。佛主：玄奘，你临死前有什么要说的么？唐僧：这算一难吗？');
INSERT INTO `tpshop_joke` VALUES ('2542', '第一次去未来岳父家，一进家门，未来岳父就满脸黑线，那个只比女朋友大几岁的后妈也目瞪口呆，女朋友在一边着急，只有我在心里默默说到，你抢我女友，我就睡你女儿');
INSERT INTO `tpshop_joke` VALUES ('2543', '刚遇见一个奇葩！风风火火冲进银行，狂按取号机，看着他着那长长一串编号纸转身，奔入远方的公厕。。。');
INSERT INTO `tpshop_joke` VALUES ('2544', '一个男孩儿，一个女孩儿。男孩儿的手牵着女孩儿的手，拉着她向前走着，可女孩儿还是一步一回头，看那五楼的一间灯火，和摇曳在窗口的妈妈的身影。最后，扭过头，义无反顾地和男孩儿私奔了。 他们打了辆出租，直奔火车站。到了车站，男孩儿正要掏钱付账，司机对着女孩儿说：“不用付钱了。你爸爸已经给过钱了。”');
INSERT INTO `tpshop_joke` VALUES ('2545', '大学同学去面试，头发梳成大人模样，穿上一身帅气西装，面试的是那公司小副总，我朋友白话半天，那副总说了一句让他终身难忘的话：比起前几个应聘的，你更像收购我们的。');
INSERT INTO `tpshop_joke` VALUES ('2546', '一位救生员向游客抗议：我以已经注意你三天了,汪先生你不能在游泳池小便. 　　　　　　　　　汪先生：每个人都在游泳池小便. 　　　　　　　　　救生员：没错!先生,但只有你站在跳板上小便......　');
INSERT INTO `tpshop_joke` VALUES ('2547', '记得上小学6年级的时候，女生还没有穿罩罩，坐我斜对面的妹子袖口很大，于是上课的时候没事一直在研究，突然感觉谁在看我，通过另一个袖口看见一张猥琐的笑脸，相视一笑，各忙各的');
INSERT INTO `tpshop_joke` VALUES ('2548', '姐夫欠钱，小姨子来要帐。姐夫很为难：“妹呀，我知道你挺紧的，不过这几天你姐也特紧，我这儿一时抽不出来。等日后稍微松点，我保证把你窟窿堵上。');
INSERT INTO `tpshop_joke` VALUES ('2549', '上次去幼儿园看到一个孩子的名字叫“吴所谓”，当时感觉这个名字起的太随意了，直到一次家长会，我发现这个孩子父亲叫“吴辽”- - 、孩子，我不怪你了');
INSERT INTO `tpshop_joke` VALUES ('2550', '“媛媛，表姐大晚上来没别的事！你姐夫今天发工资，结果他只给了我5000元钱！于是我就急了，大声抱怨道：我一个包还1万多呢！5000元钱够干啥的！”“姐夫说啥了？”“你姐夫留句：够干你妹的！然后摔门就走了，打电话也没接，于是我就上你这看看...”');
INSERT INTO `tpshop_joke` VALUES ('2551', '‍‍‍‍一天下楼去买瓶饮料，付钱的时候，老板说：“3块钱！” 我忙问老板：“老板，你没看见，瓶上不是有建议零售价2元吗？” 老板不懈的看了我一眼说：“我不接受他的建议！” 我竟无言以对，乖乖的付了3元钱！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2552', '“现在小学的应用题，是不是可以放过出水管放水管了？” 神回复：“必须放过，现在可以改成小明手机常亮玩游戏五分钟掉3%的电量，用充电宝三分钟充2%，请问小明一边玩游戏一边充电多久可以充满？”');
INSERT INTO `tpshop_joke` VALUES ('2553', '‍‍‍‍老王对朋友说：“我家里是分工合作，我管几件事，太太管几件事。” 朋友问：“那么你管什么？” 老王说：“我管孩子和家务。” 朋友问：“那么你太太管什么？” 老王说：“太太管我和钱。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2554', '一对情侣在公园散步。男友已经厌恶女友了，但又不知如何开口。终于鼓起勇气对女友说：“没有你，我真的不知道怎么活下去。”女友：“真的吗？”男友：“但是，明天我要去试试看。”');
INSERT INTO `tpshop_joke` VALUES ('2555', '‍‍‍‍弟：“哥，你说红色车一般都是女司机，对吧。” 哥：“对啊，遇到红色车别你，别怕，使劲别她。” 弟：“可是今天别一辆红色车，车上下来一群人把我打了！” 哥：“啊！你别的什么红色车？” 弟：“就这辆(消防车)。” 哥：“咋没把你打死！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2556', '‍‍‍‍“当你的生命只剩下一天，你会选择在哪里度过？” “在学校里。” “真是爱学习的孩子。” “不，因为学校让我度日如年！” “额，好吧。如果只剩最后五秒，你想做些什么？” “请听下一小题。” “……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2557', '　　胖妹子：你的双眼皮那割ID呀！ 　　我说：每割，天生的！ 　　我问：你的双下巴哪装的啊！ 　　胖妹子答：吃肉送的！');
INSERT INTO `tpshop_joke` VALUES ('2558', '从商场出来、看见旁边有辆宝马5系、一种亲切感油然而生、我忍不住走到车前拿起手机自拍了几张、这时一个时髦女郎走过来了、拿出车钥匙、轻蔑的看了我一眼、坐进了车里。 我连忙道歉：“不好意思、不好意思。”然后坐进了她旁边的兰博、顺便发了个朋友圈：今天竟然遇到三年前卖的二手车啦！好有缘哇！');
INSERT INTO `tpshop_joke` VALUES ('2559', '‍‍‍‍‍‍公司正在开会，开到一半有一个同事放了一个响屁。 老板没说什么，过了一会儿那个人又放了一个响屁，老板也没说什么。 又过了一会，那个人又放了一个响屁。 老板忍不住了，大声说到：“你有完没完” 那个同事说：“照你这样开会的速度，大概还有七八个屁就完了。”‍‍‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2560', '‍‍‍‍一天，老妈抱怨道：“别人家的狗窝都比你屋干净！” 我反驳道：“那是养狗的人爱干净，又不是狗自己收拾的！所以你养我就得帮我收拾，说明是你不爱干净而已！” 真被自己的机智吓了一跳，后来我挨了一顿胖揍，老实的把房间收拾干净了！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2561', '第一次感觉太难了，老是插不进去，明明对准了还是偏了，试了好几次都进不去，哎果断放弃，穿针线真不是男人该干的活。');
INSERT INTO `tpshop_joke` VALUES ('2562', '我一室友谈了女朋友，他让我们帮他们俩取个情侣网名。 大家想了很多，他都不满意。 我说：这样吧，你女朋友叫珊瑚，你叫复方。 他说：这是什么意思呢？ 我：你听说过一种药吗？叫复方草珊瑚含片。');
INSERT INTO `tpshop_joke` VALUES ('2563', '大家都在群聊，我也拉了个群，把一群互相完全不认识的人全拉在一起，群的名字叫 兴趣爱好 ！好多天了，群里溺的溺、退的退、一片死寂，只有我一个人发发东西！今天我把群名改成 性趣爱好 ，又把这些货再次拉进来。泥马！就没消停过，各种图片、小shipin，人也越加越多。果断退群');
INSERT INTO `tpshop_joke` VALUES ('2564', '姑娘们，你们要是以为抓住一个男人的心就要先抓住他的胃的话，我明确告诉你们，你们的手位置放高了！');
INSERT INTO `tpshop_joke` VALUES ('2565', '当年考试，监考老师指着我临座说作弊，临座是个妹子，右手拿笔，左手握拳，死活不承认，僵持了差不多二十分钟，最后威胁赶出考场她才松手，手中空空如也。考完试，妹子淡定地说：不把她气糊涂，她早检查地面了，其实纸条在我脚边。时隔多年，我依旧佩服妹子的临危不乱啊。。。');
INSERT INTO `tpshop_joke` VALUES ('2566', '昨晚做了一个梦，梦见奥巴马女儿像我求婚，我嫌她丑，拒绝了，心想这样会不会影响中美国际友谊，把我吓得！！！一身汗，还好只是个梦！   ');
INSERT INTO `tpshop_joke` VALUES ('2567', '‍‍同桌在玩手机，没调成静音模式。 突然来了个电话，他立马把手机扔给我。 同学们和老师的目光都投了过来，我立马站起来说：“老师我错了，上课玩手机是我的不对，您把手机没收吧！当场砸了也行！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2568', '‍‍晚上跟姐姐姐夫一起看电影。 买爆米花的时候服务员介绍情侣套餐，两杯可乐加爆米花。 姐夫在旁边直接问了句：“有三个人的情侣套餐嘛。” 服务员愣半天冒出了：“家庭套餐可以吗？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2569', '‍‍‍‍看见嫂子又和大哥发脾气了。 我上前把大哥拉到一旁劝道：“大哥，女人都是吃软不吃硬，你别老是跟嫂子怄气吵架啊！” 大哥痛苦的说道：“兄弟你不知道，这婚后的女人其实都是吃硬不吃软，她就是每次嫌我太软才吵架的！” 额！纯洁的我怎么听不懂呢？‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2570', '今天去买西瓜，出门前媳妇嘱咐买杯芒果汁回来，我怕忘了就一直念叨，西瓜芒果汁，西瓜芒果汁。。。到了冷饮店，我张口来了句，我要一杯西瓜味的芒果汁！做饮料那妹子的眼神。。。。不说了，找地缝');
INSERT INTO `tpshop_joke` VALUES ('2571', '今天老婆问我上班是为了兴趣爱好，还是远大理想！我这暴脾气一巴掌就扇过去“老子上班就是为了钱，不是什么理想，老子的理想就是不用上班！”');
INSERT INTO `tpshop_joke` VALUES ('2572', '和妈妈一起出门上班，我穿高跟鞋追不上穿平底鞋的她，我说“你跑那么快，我要是摔死了，你还得回头把我捡起来，更耽误你上班时间！”，妈妈说“你都摔死了，我肯定不上班了！保险公司赔我那么多钱，我还上什么班呢！你活着只有我为你花钱。”……我说“我是你充话费送的吧？”，她说“嘁！还用得着我花钱？你是我顺路捡的。”……');
INSERT INTO `tpshop_joke` VALUES ('2573', '‍‍‍‍‍‍八戒：“师傅这菜太咸了。” 大师兄：“呆子，有的吃就不错了。” 沙僧：“大师兄说的对。” 八戒：“但是这菜真的太咸了！” 师傅：“放一放再吃。” 八戒：“为啥？” 师傅：“时间可以冲淡一切！”‍‍‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2574', '‍‍‍‍有个会开玩笑的朋友问阿凡提。 “阿凡提，咱俩究竟谁是大傻瓜？” 阿凡提不假思索地拍拍自己的胸口说：“阿凡提是个大傻瓜。” “为什么？” “因为他没能看出来你是个大傻瓜。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2575', '初中时班主任带了孩子去我们学校，趁他不在时便让他孩子叫我二大爷，，，等再过一个月班主任又带孩子去了，孩子一见到我便高兴的叫了声二大爷，，，老师你先听我说，误会，这都是误会。');
INSERT INTO `tpshop_joke` VALUES ('2576', '今天晚自习下大雨 突然天上一道闪电划过 整橦教学楼都黑了 一学渣顺势将班上总电闸关了… 十五分钟后电修好了 全校只有我们还黑着…');
INSERT INTO `tpshop_joke` VALUES ('2577', '‍‍‍‍今天老爸煮饺子吃，我一开始吃的时候还以为是面片。 结果到后来准备吃完的时候才知道锅底还沉着一堆肉丸子！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2578', '感觉华语情歌是“你不爱我，我明白的，可我心情，好好低落…” 欧美流行乐是“你不爱我，你是傻么！爱那个碧池，你特么瞎么！” 朋克摇滚是“都尼玛不爱我！都尼玛不爱我！都尼玛不爱我！” 印度音乐则是“不爱就不爱吧，大家一起跳！起！来！阿开苦力猴亚猴奔！”');
INSERT INTO `tpshop_joke` VALUES ('2579', '‍‍‍‍和老婆异地。 今儿正上班着，媳妇儿突然打电话来说她和丈母娘在我单位门口。 我说：“媳妇儿，咱不过愚人节，别闹，我开会忙着了。” 就匆匆挂断了电话。 十分钟后，领导一脸贱笑的过来训我：“大城市呆了几天，老婆老妈都不认拉，快去保安室领人，你媳妇儿说要进来杀了你这个薄情寡义的人，拉都拉不住。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2580', '‍‍‍‍晚上有伙人要打我，我立马叫兄弟，谁知那伙人没找到我。 这时，我的兄弟们冒着寒风来了。 我说了句：“逗你们玩呢。” 结果，我躺在医院了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2581', '去吃拉面，问老板羊肉拉面多少钱一碗？老板答：十块。问：加一份羊肉多少钱？答：十块。我疑惑：羊肉拉面多少？答：十块啊不是说了吗。问：是啊我说加一份羊肉多少钱。答：十块啊！我越发疑惑，旁人都在偷笑，这时旁边一位美女好意解释到：羊肉拉面十块，加一份羊肉十块，一共20。我反应过来后一脸无辜：不好意思脑子抽抽了。这不是高潮，我刚说完，旁边一个大妈笑得拉面从鼻子里喷出来，鼻涕冲到她老公碗里……整个店里的人都笑疯了……');
INSERT INTO `tpshop_joke` VALUES ('2582', '记得上学时，学校门口小卖部，几个特别好的小哥们，凑到一起，搞几根香烟，大家一个一烟轮着吸，谁多吸一口好点说，你要脸吧！拿来，擦，想起来，都笑。有同感地，赞一个');
INSERT INTO `tpshop_joke` VALUES ('2583', '艺考的心电图\r\n平时的心跳:__/\\______/\\_______\r\n候考的心跳:__/\\___/\\_/\\__/\\_/\\_\r\n考试的心跳:_/\\_/\\_/\\_/\\_/\\_/\\_\r\n等成绩的心跳：/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\\r\n看到成绩之后的心跳:___________________');
INSERT INTO `tpshop_joke` VALUES ('2584', '今年雾霾严重，记者问一老大妈，空气污染严重，对于元宵节放烟花你怎么看啊？ 大妈说我能怎么看啊 腿脚不利索啦 就站在阳台上看看！');
INSERT INTO `tpshop_joke` VALUES ('2585', '美女，可以交个朋友吗。我有男朋友了。那你介意多交个男朋友吗，介意。那你介意我喜欢你吗。介意。那你介意我介意你介意我吗？介。。不。介。哎呀我操。去尼玛的');
INSERT INTO `tpshop_joke` VALUES ('2586', '餐厅里，服务生走到一个客人旁：＂先生，对不起，让您久等了。＂＂没有啦！你知道你们餐厅的地砖有367块吗？＂');
INSERT INTO `tpshop_joke` VALUES ('2587', '今天参加婚礼，男主持和音响师说：“来一首《兄弟干杯》”，就往台上走，音响师可能没找到，就问“《兄弟抱一下》，行吗？”，主持人一边点头一边说话了：“接下来，给大家带来一首《兄弟干一下》......全部的人都笑疯了...疯了...');
INSERT INTO `tpshop_joke` VALUES ('2588', '我们宿舍有个胖子，特喜欢放屁……有一次熄灯了大家都躺被窝，结果他默默的把被子掀开放了一个连续性的响屁。你以为这是Gc错了，接下来宿舍另一个人说了一句:哎呀，听口音不像本地人啊！！这还不是GC，Gc是胖子的老乡来了一句话让我们喷了…:嗯！还是原来的配方，还是原来的味道！');
INSERT INTO `tpshop_joke` VALUES ('2589', '昨晚十二点回家，路过一楼门口，听见屋里有一小屁孩哭闹不停，妈妈骗他说再哭外面的鬼就进来抓你了，我本着助人为乐的精神，应了一声“你妈没骗你，听话“，结果里面俩人都哭了……');
INSERT INTO `tpshop_joke` VALUES ('2590', '我正在网吧包房里上网，房里八个人，做我隔壁的一对情侣，大概十七八岁，在旁边肆无忌惮的看着岛国大片，完全无视周围，这还不算什么，GC是女孩指着显示器大声说:他妈的我最喜欢这个姿势，老汉推车……！我一脸茫然，瞟了一眼，现在的青少年这是怎么了？太不知害臊了吧！');
INSERT INTO `tpshop_joke` VALUES ('2591', '早上，洗完脸，我都会对着镜子说：“魔镜魔镜，世界上最好看的人是我吗？好，你不说话我就当你默认了哟。”美好的一天就这样开始了。');
INSERT INTO `tpshop_joke` VALUES ('2592', '告诉大家一个辨别真假钱的方法，用火烧，真钱烧完是灰色的灰，假钱烧完是黑色的灰，我刚试了一下，我们家的钱都是真的，好开心。你们也试试吧。');
INSERT INTO `tpshop_joke` VALUES ('2593', '我媳妇什么都好，就是太自私了，有好东西不懂得分享。我今天教育她：有那么好的老公别自己享受，让你姐妹们都玩玩啊！结果，她倒把我教育了一番，不说了，教育的脸疼。。。');
INSERT INTO `tpshop_joke` VALUES ('2594', '老婆：你觉得幸福的婚姻应该是什么样子的？老公：幸福的婚姻就是男人娶了像你一样的妻子，女人嫁了像我一样的丈夫。老婆想了想，又问道：那你觉得不幸的婚姻应该是什么样子的？老公：同上。');
INSERT INTO `tpshop_joke` VALUES ('2595', '老婆：“老公，你喜欢现在的我，还是刚认识那时侯的我。”我：“现在的你。”老婆：“那你那个时候为什么还要追我啊？”我：“饥不择食。”啪啪。。。玛淡，脸现在还红红的。。。');
INSERT INTO `tpshop_joke` VALUES ('2596', '老婆：老公你去哪了？老公：我在楼下。老婆：大早上六点你不睡觉跑楼下做什么？老公：你不知道刚才地震了，我拿起手机和存折，裤子都没穿就跑下来了，我看你睡那么香我就没喊你。老婆啪啪。。。');
INSERT INTO `tpshop_joke` VALUES ('2597', '老婆：老公，我感冒了。老公：怎么回事？老婆：不知道，流鼻涕了。老公：是不是昨天夜里被窝的风刮的太大了？只听啪啪啪的几声，老婆道：没正经！老公：轻点，扇耳光也有风。。。');
INSERT INTO `tpshop_joke` VALUES ('2598', '加班到很晚，开车送美女同事回家，在一个老旧的小区门口我把车停住：“就送你到这吧，我不进去了。”她凑过来倚在我耳边说道：“你开车送我进去吧，还能上去坐坐。”我果断地拒绝了。望着同事远去的背影，我心想：MD，当初考驾照要是好好练科二，现在也不用害怕倒不车出来了！');
INSERT INTO `tpshop_joke` VALUES ('2599', '女：你跟我谈恋爱，却又去找另一个女孩。男：其实我找了两个女孩。女：人家脚踩两只船，你TM脚踩一舰队。男：没别的意思，我只是想货比三家。女：哦，是吗？那我是什么货？男：你高雅，非贱货；你贤惠，非骚货。女：我问你，我究竟是什么货？男：你很贪吃，吃货。');
INSERT INTO `tpshop_joke` VALUES ('2600', '晚上跟女友打电话，说：“亲爱滴，我想你了。”女友：“说说，都想我什么了？”“想你变漂亮点。”“滚！！！”');
INSERT INTO `tpshop_joke` VALUES ('2601', '我上小学的时候，姐姐经常用灌了水的气球丢我，一到夏天，我全身老是水淋淋的，一回家就被母亲呵斥。后来我实在忍不下去了，便偷偷把每个气球上都扎了眼儿，终于逃脱了每日水灾。第二年的夏天，我妹妹诞生了。');
INSERT INTO `tpshop_joke` VALUES ('2602', '家里来客人了，儿子在旁边玩，客人想逗逗孩子说:小浩浩，你知道，你是怎么来的吗？？儿子大叫:知道知道，我妈说我是充移动话费送的，我爸爸说我是充联通话费送的，但是，他们说的都不对，我奶奶说，是她充电信话费送的！当时一屋子人瞬间爆笑不止！');
INSERT INTO `tpshop_joke` VALUES ('2603', '公司来了个帅哥，公司以前迷我的MM都迷他去了。\r\n他业务各方面也得到了领导的赏识，我嫉妒啊。\r\n于是一天我请他吃饭，吃到最后，我抓住他的手说：“其实我喜欢你很久了，我知道这种喜欢不对，你不会歧视同性恋吧？”他惊恐的跑了，后来几天我一直对他很好。\r\n再后来他辞职了，我发现我太TM机智了！');
INSERT INTO `tpshop_joke` VALUES ('2604', '　　当兵时在营部，星期天我和一四川籍战友打闹，我占了便宜就跑出去了，那货就拎根棒子躲在门后想偷袭我，你能想象他一棒子把营长给敲翻在地的画面吗？');
INSERT INTO `tpshop_joke` VALUES ('2605', '吃自助餐不小心剩了很多，一个五大三粗的服务员狠狠的说：“本店剩菜必须吃完，否则后果很严重！”于是我硬着脖子把剩菜全部咽了下去，撑得我直翻白眼，完了我忍不住问他：“如果不吃完会有什么严重的后果？”大汉说：“会给国家资源造成巨大的浪费。”');
INSERT INTO `tpshop_joke` VALUES ('2606', '我姓刘，单位都喊老刘。一天，有同事来找我，我在厨房做饭，那同事一进门听厨房噼里啪啦响，就问我媳妇儿“老刘忙呢？”，我媳妇儿又问“啥？”同事又说“老刘忙呢？”，，，媳妇儿半天没说话，末了大声道“谁是老流氓？”');
INSERT INTO `tpshop_joke` VALUES ('2607', '以前一个美女英语老师，上课突然走出去，大家正纳闷发生什么事的时候，突然教室里的扬声器屁啦帕拉屎屁大作啊#(狂汗)，那天她拉肚子去厕所衣服上的无线麦克风忘记关了，留着我们一教室的人听着醉生梦死的#(勉强)');
INSERT INTO `tpshop_joke` VALUES ('2608', '　　老姐结婚，按我们这边的习俗我作为小舅子要送我姐过门还要在新人家住一晚。晚上安排住处时作难了，因为他们家房间不多，后来姐夫说一句要不你晚上和我妹住一起吧，我一看他小妹，一个标准的大美女，顿时觉得我姐嫁了个好人！！！');
INSERT INTO `tpshop_joke` VALUES ('2609', '　　老姐结婚，按我们这边的习俗我作为小舅子要送我姐过门还要在新人家住一晚。晚上安排住处时作难了，因为他们家房间不多，后来姐夫说一句要不你晚上和我妹住一起吧，我一看他小妹，一个标准的大美女，顿时觉得我姐嫁了个好人！！！');
INSERT INTO `tpshop_joke` VALUES ('2610', '一个胆小紧张的证人正在接受律师的询问。律师厉声问道：“你是否结过婚？”“是的，我结过一次。”证人声音很小，还有些颤抖。“那么你和谁结婚了？”“一个女人”。律师有些发怒“废话，你当然是和一个女人结婚了，你听说过有谁会和一个男人结婚吗？”证人颤抖着说：“听说过，我姐姐”。');
INSERT INTO `tpshop_joke` VALUES ('2611', '上联：老婆不败家，爷们赚钱小三花！下联：老婆会败家，爷们赚钱年年发！ 横批：会花钱的娘们能发家。');
INSERT INTO `tpshop_joke` VALUES ('2612', '本人生活所迫 快过年了缺钱，现诚揽以下业务：贴对联20元，放鞭炮20元，挂灯笼20元、陪过年600元、陪拜年500元红包归你、陪吃年夜饭 200元，包汤圆、饺子50元，替人喝酒800元，特殊服务坚决不陪，三缺一配门500元，输赢不管，代写小学一 二 三年级的寒假作业800元（四年级以上的不会，杀鸡拔毛100元，砍柴火300元，麻烦大家帮我宣传一下！可以赚点钱');
INSERT INTO `tpshop_joke` VALUES ('2613', '‍‍大学是改变我们人生观点的地方，比如食堂。 大一的发现饭里有虫子，二话不说端着碗就去找食堂大妈理论； 大二的发现饭里有虫子，啥也不说挑出来继续吃饭； 大三的发现饭里有虫子，眼也不眨的连虫子一起吃下去了； 大四的发现饭里居然没有虫子，一拍桌子怒吼道：“没虫子这饭怎么吃？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2614', '‍‍睡觉有点冷，就朝丈夫的怀里钻。 我：“有没小鸟依人的赶脚啊？” 丈夫：“只有猪拱圈的赶脚。” 我：“……”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2615', '‍‍老师上课时，大讲特讲公益环境道德，然后举了很多事例。 之后问学生：“谁还能再举一些例子？” 一二货学生立刻举手大声说：“我们在公厕上大号，明明可以把用过的纸扔到水里冲走，为什么还要扔到旁边的垃圾桶？” 老师很诧异的说：“这也算是一项全民公益活动？！” 二货学生立即说：“给不带纸的人留条活路！” 老师：“哦。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2616', '本人幼师一枚，幼儿园里的熊孩子特别牛，今天有一个犯了大错。我叫他提包回家。谁知这货拿起包就扒住我的腿开始嚎：“老师，我爱你，是真的爱你，你别赶我走~~~” 顿时我笑了，这是二十几年来第一次赤裸裸的告白啊!');
INSERT INTO `tpshop_joke` VALUES ('2617', '‍‍我姐有个朋友，她孩子调皮不爱学习。 有一回又考了个倒数，被她摁在板凳上狠狠地打屁股。 这倒霉孩子趴在凳子上，一边抹眼泪一边说：“人啊！总要给自己留条后路啊！” 我姐的朋友听了，拿尺子的手顿时停在半空。 寻摸着：“这混账东西。难不成长大了还想着不要娘了。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2618', '我们大学门禁卡和饭卡是同一张卡，刚换的门禁很贱，自动读取饭卡余额，超过一百就说“土豪请进”，低于50就说“屌丝，请充值”，之前从没做过土豪的我也是醉了。。。');
INSERT INTO `tpshop_joke` VALUES ('2619', '由于厂里搬了另一个地方了，网络设施还没弄好，想想好几天没打网游了，就问了一个女同事，你知道这附近有网吧吗？女同事回我，说道:网吧我就不知道了，但附近有间宾馆有电脑的，我带你去吧！我当时就犯嘀咕了，玩个电脑还要去宾馆，我说，不去了。第二天她就辞工了，想想活该单身了二十几年。');
INSERT INTO `tpshop_joke` VALUES ('2620', '‍‍马上要期末考试了，说个女儿一年级时的趣事。 期中考试卷发回来了，99分。 我看了看卷子，问女儿：“7+3你不知道得几啊？” “我知道啊！得10。” “那你为什么写9啊？要不就得100分了。” 女儿委屈地看着我“你看，横线太短了，写10写不下。我就只好写个最大的9了。” 乖，怎能思维正常点吗？‍‍');
INSERT INTO `tpshop_joke` VALUES ('2621', '一个女同事问我说：猜个谜语，打一字。请问什么字合在一起很疼，分开了很爽？哥纠结了一天没猜出来，猜出来以后又蛋疼了一天。');
INSERT INTO `tpshop_joke` VALUES ('2622', '　　正在上小学的9岁小表弟一日突然问我；“表哥表哥，你说这个小鸟是怎么来的？”我为了逗他就说；“和你一样”。他突然大声说；“那小鸟就是我弟弟咯”，我问他；“你怎么知道的？”他说；老师说的。');
INSERT INTO `tpshop_joke` VALUES ('2623', '“酷是帅得严肃，傲是俊得孤独，靓是美得端正，骚是浪得妖邪，憨是傻得可爱，萌是蠢得认真。”“那我是…？”“你是丑得一逼。”');
INSERT INTO `tpshop_joke` VALUES ('2624', '老师：小明，你平时在家喜欢看电视吗？ 小明：喜欢。 老师：那有没有你印象深刻、很喜欢的台词啊，给老师分享一下？ 小明：有啊，有啊，就是那句：唉哟喂！大爷，楼上请！新来的姑娘可俊俏呢。。。 老师：你TM给我滚出去！');
INSERT INTO `tpshop_joke` VALUES ('2625', '　　甲：听说你找了个胖子？为什么？因为你也是个胖子吗？ 　　乙：因为我不想让我将来的孩子也胖。我想要个瘦孩子。 　　甲：这有什么关系？ 　　乙：既然负负得正，那胖胖得瘦呗！');
INSERT INTO `tpshop_joke` VALUES ('2626', '躺在沙发上不经意地唱了一句：轻轻的一个吻…脸上竟然挨了一巴掌！还被质问青青是谁？我倒是不着急回答这个问题，心里却暗自庆幸，多亏没唱：青青河边草');
INSERT INTO `tpshop_joke` VALUES ('2627', '一朋友，真正的美女。之前从不用自己的照片做手机桌面，有天瞄到她锁屏界面换成了自己的写真照，明艳动人。我问她怎么把自己换成了锁屏界面？她答之：“哪天我手机要是丢了，假如被一男人捡了，手机还回来的可能性要大很多。”我问为啥？她说，见到锁屏界面，男人肯定更想见见我本人！你捡手机会见她吗');
INSERT INTO `tpshop_joke` VALUES ('2628', '　　白雪公主逃出皇宫看见一座小木屋，对“小矮人”说 你们是我命中注定的七个小矮人啊！！！ 　　可是我们是葫芦娃嘞。他说 　　白雪公主懵了。');
INSERT INTO `tpshop_joke` VALUES ('2629', '有人问我：蚊子只吸了你一口血，你却要了它的命，这样做对吗？我的回答很简单：我跟蚊子商量过 能不能只吸血不会痒或者只吸肥肉。它说不行、 那就没得谈了');
INSERT INTO `tpshop_joke` VALUES ('2630', '二十年的太子，一天的皇上,十个月的奴才,一辈子的提款机，男人的一生;又有人说：二十年的公主,一天的皇后,十个月的宠妃，一辈子的保姆,女人的一生。我觉得应该是：二十年的追寻,一天的仪式,十个月的呵护，换来一辈子的相濡以沫。其实男人和女人都不容易。相扶相携同甘共苦，才是真正的夫妻！且行且珍惜');
INSERT INTO `tpshop_joke` VALUES ('2631', 'Lz男，最近菊花那里不舒服，怀疑是痔疮，拍张照片问做医生的舅舅，，，，，，今天公司客服mm让我拍几张产品照片看看，拍好后我把手机给她就出去了。一会儿回来她指着我手机上的菊花照片问我，你手机里怎么有这种东西，是你的吗，好恶心呃。我当时那个汗啊，急忙说不是我的，别人的，，，，妹子悄悄说，同志么，我懂，，，，半小时后办公室里的人看我的眼神都不一样了。我，，，，，，不是，我咋解释啊~');
INSERT INTO `tpshop_joke` VALUES ('2632', '女友回家半个月了！她闺蜜来我家拿东西！她刚要走，我叫住了她！雪儿，可不可以帮我个忙？什么事你说吧！你也知道我女友回家了，只有我一个男的在家，有些事情总是要解决的！啊！这……就帮我这一次吧！说完我就把她拉进了厕所！啊……不要啊……不行的……别叫了，你就帮帮我吧！这么多衣服都没洗，我都快没衣服穿了');
INSERT INTO `tpshop_joke` VALUES ('2633', '楼主今天在南麂岛玩。。。。突然暴雨了。。。。大家躲烧烤摊。突然听到一个巴掌声。。看见一个男的拿着单反及三角架什么的一系列装备。一个女的在骂。。。你tm拿单反不抱儿子过来！单反和你一个姓啊！。。那男的说。。老婆你又不是不知道我姓单（shan）。。一帮人在偷笑。。');
INSERT INTO `tpshop_joke` VALUES ('2634', '我一生意上的朋友，年过三十才成家，她老婆头一胎双胞胎闺女，时过两年，二胎又一双胞胎闺女，现在养四个孩子要想高品质，实在是压力山大，时间一常孩子刚出生的喜悦感就没了，有一次他下班回家，看见客厅他老婆孩子坐成一排在玩耍，老婆坐在中间。于是诗兴大发作了一首诗。三十年前闹逼荒，三十年后逼满堂。大逼小逼客厅坐，中间坐着老逼王。实等淫才呀！');
INSERT INTO `tpshop_joke` VALUES ('2635', '昨天跟一美女同事开玩笑说我亲你一下不碰你身体任何地方你信不，输了我请你吃饭，她说好啊，我果断上去抱住就猛亲了一下后说我输了，唉…不说了脸还疼呢');
INSERT INTO `tpshop_joke` VALUES ('2636', '一哥们走在街上，嘴里哼了一句“你是我的小呀小苹果”，一妹子骑车从旁边经过接了句“怎么爱你都不嫌多”，这哥们跟打了鸡血似的追了过去，一直把妹子追到墙角，妹子吓得把钱手机钱包信用卡夸夸都掏出来了说“这些都给你，大哥求你放过我吧”那哥们很淡定说了句“我就是告诉你下次唱歌自己起头！！');
INSERT INTO `tpshop_joke` VALUES ('2637', '你身边有没有这样一种 额 也不能说贱人 就是烦人的人 你俩干一样的工作TA总是跑过来问你干了多少，然后说：你干的那么快啊！我一天才干几十，，，巴拉巴拉一堆” 等到你自己去一看产量本 人家比你多好几十。 等到发工资时过来问你发了多少钱，要看你的工资条，一看两千零几十 然后说发的不少啊。你问TA的时候 TA就含含糊糊的说也是两千多点。你去一看工资表，人家两千九百多 。你们身边有没有这样的人？是不是觉得特烦人？');
INSERT INTO `tpshop_joke` VALUES ('2638', '一女的去药店问男医生道:“有卖后悔药嘛？”男医生答到:“有，有10块的，12的还有18的，你要哪种？”女的想想说要18的，说完接着男医生递来的毓婷~');
INSERT INTO `tpshop_joke` VALUES ('2639', '今天我出去买麻辣烫，结账时我看见给我找钱的大姐，中指像小孩那种没发育的，盯了半天我脑残的问一句，阿姨你中指那么短平常一个人在家怎么办。。。。。怎么办。。。。当时那大姐脸绿了。。我提着麻辣烫就跑了。。。钱都没要。。。');
INSERT INTO `tpshop_joke` VALUES ('2640', '刚刚公交车上我站着发呆，突然有颗白白的脑袋凑到我手边，我以为是萨摩耶呢就顺手摸了摸，结果是位蹲下来系鞋带的老太太的脑袋啊！被骂神经病了…');
INSERT INTO `tpshop_joke` VALUES ('2641', '一同事带5岁儿子到单位来玩，几个二逼同事就问了，你爸爸晚上睡觉压你妈妈了吗？同事儿子：压了，还把妈妈压的一个劲的叫呢！二逗同事又问：你爸爸除了压你妈妈还压过谁？同事儿子用手一指：还压过那个王阿姨，瞬间科室冷场，那个王阿姨脸色通红的跑了出去......看着未婚妻惊慌失措的逃离现场，那个二逼同事的瞬间脸变成了猪肝色。');
INSERT INTO `tpshop_joke` VALUES ('2642', '公园管理人员对小明说：“这池塘是禁止游泳的。” 小明说：“我没游泳，我是不小心掉进来的。” 工作人员说：“这样啊，掉进池塘是不禁止的。”');
INSERT INTO `tpshop_joke` VALUES ('2643', '说个真事，我们这交警队摊上大事了，一跑油罐运输的，超载一到我们这就被查，把那哥们查疯了，于是就想了一狠招，那哥们组织了六辆空油罐车经过这 ，被查，就上去说好话，那交警八成刚失恋， 连磅都没有过就开超载百分之三十的票车扣在交警队GC是第二天车主发现油没了，六罐油得赔多少钱啊！！！');
INSERT INTO `tpshop_joke` VALUES ('2644', '他在北京发来消息：“我明天去看你，来接我，好么？”\r\n她在南京，开心地回复：“恩啊”\r\n第二天，她在车站搜寻过往人群中他的身影，\r\n期待又焦急“你到底在哪儿？”\r\n“傻瓜，你不会真在车站吧，今天是愚人节哎，哈哈哈…”\r\n她顿住了，泪无声落下，失落、委屈…却被人从身后轻轻抱住，\r\n熟悉的声音：“傻瓜，就知道你会来，即使是愚人节，我又怎么舍得骗你…”\r\n女孩转过身，娇滴滴的说道：“干爹你好坏。”\r\n女孩接着说:“干爹，我怀孕了”\r\n“.许久，是吗，太好了。”\r\n“干爹，我想要这个孩子”\r\n“那当然了，我们真爱的结晶。干爹要让你过最好的生活\r\n这张卡是干爹的所有积蓄，密码，你生日”\r\n女孩紧紧抱住干爹\r\n次日，女孩痛苦，失落，拿着空卡，陷入悲痛\r\n“你怎么了？”\r\n“我怀孕了”\r\n“太好了，我们真爱的结晶。我要让你过最好的生活，这张卡是我的所有积蓄，密码，你生日”\r\n男孩紧紧抱住女孩，接着说“我们结婚吧”\r\n“嗯”\r\n男孩很快跟女孩结婚了。\r\n男孩带着女孩见父母。“老爸，我要结婚。”\r\n他老爸看了一眼女孩，女孩和他老爸大吃一惊，同时脱口而出。“干！！”\r\n男孩望着这一幕，百思不得其解。\r\n正当三人尴尬的发愣时，大门开了，一中年妇女进来了\r\n说道：儿子回来了，这是你朋友吧！\r\n儿子：”妈，这是我女朋友怀孕了，我要结婚！”\r\n”啊，可你们还是学生啊”\r\n男孩的爸爸低头抽着闷烟，一声不吭！\r\n妈妈仔细看着这个从来没有见过的准儿媳，突然紧紧抓住那女孩的手\r\n眼睛直视着女孩肩膀上的红胎记，问道：\r\n”孩子你是不是莲花乡的，你妈妈叫李兰”\r\n”嗯，你怎么知道？”\r\n中年妇女扭头痛哭，说：”孩儿她爸，你还记得十八年前\r\n为了生个儿子，咱把刚出生三个月的女儿送人了…');
INSERT INTO `tpshop_joke` VALUES ('2645', '一男生对一女生说：“你真漂亮啊。”\r\n女的对那个男的说：“你真酷啊。”\r\n路人听到了便问另一个路人：“他们在过情人节吗？”\r\n那人回道：“不，他们在过愚人节。”');
INSERT INTO `tpshop_joke` VALUES ('2646', '愚人节快到了，其实那天最蠢的事情莫过于你机智的掌握了各种防被整的技能\r\n时刻小心翼翼，可是最后发现根本没人想去愚弄你');
INSERT INTO `tpshop_joke` VALUES ('2647', '女友是个波霸，我开玩笑问她里面的不会都是硅胶吧？\r\n她点点头说是，看我脸都白了她大笑着安慰我说：“今天是愚人节我骗你的啦！\r\n其实那是用我自己脂肪做的，我以前是男人的时候比较胖。”');
INSERT INTO `tpshop_joke` VALUES ('2648', '老公：亲爱的，如果我出 轨了，你会原谅我吗？\r\n老婆：当然，孰能无过，知错能改就行。\r\n然后老公就主动交代了自己出 轨的事情，老婆听后当时就是一大嘴巴过去，\r\n老公捂着脸说到：你不是说会原谅我吗？老婆：你不知道今天是愚人节啊。\r\n接着老公就还了一大嘴巴过去：你TM知道今天是愚人节还打我。');
INSERT INTO `tpshop_joke` VALUES ('2649', '昨天亲眼看见个求婚场面，男人单膝跪地，郑重的问女人：“嫁给我吧。”\r\n女人感动的泪流满面：“我愿意，但是你要敢给我说这是今天的愚人节玩笑\r\n我非要把你家祖坟刨了给你当个玩笑。”\r\n男人满头大汗的说：“其实我是准备被你拒绝以后才准备说是玩笑的。”\r\n围观群众不仅感叹，好一个机智的小伙子。');
INSERT INTO `tpshop_joke` VALUES ('2650', '今天愚人节向女神表白！\r\n她说是我异父异母的亲妹妹！\r\n我激动的哭了！\r\n现在想想她是不是在骗我！');
INSERT INTO `tpshop_joke` VALUES ('2651', '课间我们怂恿一胖同学去勾搭班花，班花本在快乐地玩手机，他过来后班花竟放下了手机，抬头看着他。我们心理暗惊：有戏啊！然后班花说：你能往旁边移一些么？胖子表示不懂，班花低声说：同学，你太大条了，4G网络都被你挡得没信号了～～');
INSERT INTO `tpshop_joke` VALUES ('2652', '——待我长发及腰。。。\r\n——那你放屁就可以把头发吹起来了，哈哈。。。');
INSERT INTO `tpshop_joke` VALUES ('2653', '美术馆正在举办一位青年画家的个人画展，参观的人挺多。这位青年画家的女朋友和她妈妈也来捧场。\r\n在画作中流连着，妈妈看得非常仔细。在一张裸.体人像前，妈妈停住了。怎么看画中的女孩儿就是自己的女儿。妈妈忍不住，小声对女儿说：“你没有光着身子让她画吧？”\r\n“啊，没有没有。”女儿脸一红：“我哪能光着身子让他画呢。他是凭着记忆画的。”');
INSERT INTO `tpshop_joke` VALUES ('2654', '去年10月份的有一天，女神对我说：“下个月的11号，她一定要脱光。”我说：“妹子，你是不是傻掉了，大冷天的，你要脱光呀，最好这个月吧，天热！”');
INSERT INTO `tpshop_joke` VALUES ('2655', '话说我老板是个二货，一天办公室里老板问我：跟男朋友分手了就叫前任，分手了又和好叫什么？ 我：不知道！ 老板：叫连任！ 我：……');
INSERT INTO `tpshop_joke` VALUES ('2656', '吃过晚饭，没事可做，就和老婆啪啪一次，事毕，老婆幽怨的说：“老公，我希望下辈子我们还是夫妻，只不过我们要互换一下，我来做男的，你来做女的。好不好啊？”我一脸兴奋的说：“那当然好啊，你说的是真的嘛，宝贝！我就知道你是懂我的，是那么滴爱我！我很高兴啊！”老婆一撅嘴巴说道：“你理解错了，其实我就是想让你知道，那一次次2分钟时间是什么感觉。。。。。。”尼玛，埋汰我啊。。。。');
INSERT INTO `tpshop_joke` VALUES ('2657', '一行人自驾游，可是没有人个会开车。这是，有一个人自告奋勇是他会。于是他们出发，车子来的很稳，众人并没有发现什么异样。车子走到了盘山公路上，右侧是万丈深渊，这是，那人很得意的说：“你们看我这技术像不像是有驾照的”。车里突然一片死寂………………');
INSERT INTO `tpshop_joke` VALUES ('2658', '今天失恋了，一个人去喝闷酒。 刚刚坐下，就是个美妞过来说：“先生，你是一个人吗？” 我点点头。 然后她就把我旁边多余的碗筷收走了………');
INSERT INTO `tpshop_joke` VALUES ('2659', '碰到狗熊应该马上躺下装死，等它过来时一动也不要动。这肯定是狗熊造的谣。');
INSERT INTO `tpshop_joke` VALUES ('2660', '我独自走在路上,见一老头拿着一个手电筒，和我家的手电筒一模一样！！\r\n我以为那人是我爸便走上前去，刚准备张嘴，那人却先说话了：“住嘴！加上你今晚已经是第三个人叫我爹啦！！！！ ”');
INSERT INTO `tpshop_joke` VALUES ('2661', '小明：“小王怎么了？” 小丽：“被前面两个人打了！” 小明：“敢欺负我兄弟？我要报仇！” 小明：“喂！你们两个！” 壮汉A壮汉B：“嗯？谁在叫？” 小明：“你们两个，我早看他不顺眼了，goodjob！”');
INSERT INTO `tpshop_joke` VALUES ('2662', '记者采访张国立:您的作品很多，您自己最喜欢哪一个？ 张国立:（康熙微服私访记） 记者:您觉得自己哪个作品最失败呢？ 张国立:……张默！');
INSERT INTO `tpshop_joke` VALUES ('2663', '公交车上，我面前坐着一个三四岁的小男孩，边上是他妈妈。公交车每停一站都会报：“请给老弱病残让个座。”于是小男孩问妈妈：“妈妈，老弱病残的弱是什么?”妈妈想了下，说：“是弱智。”小男孩目光转了一圈，停在我身上。我对他微笑了一下，于是他站起来说：“叔叔，你坐这吧');
INSERT INTO `tpshop_joke` VALUES ('2664', '一朋友和他同事喝酒，结果喝大了两人打起来了，脸上都挂彩了。 第二天酒醒后悔了，朋友觉得心里过意不去，于是摆酒道歉，结果两人又喝大了，提起来昨天的事情，又打起来了。');
INSERT INTO `tpshop_joke` VALUES ('2665', '为什么现在女人都以高价出售自己，是买卖，还是结婚？结婚有这么难，你们每天说，你没工作，没房子，没车，没资格说女人。你们好，你们长的好看，有资本，而又有哪些口上说得，够吃够穿就行。其实还不是找个有钱的，有房的，所谓的富二代，这个现实的社会，早看穿你们，真正的女人是陪男人从坎坷走过来。');
INSERT INTO `tpshop_joke` VALUES ('2666', '女儿问我：“爸,人生是什么？” 暗骂现在小学一年级就教这么深奥的东西。无奈上网、查资料解释半个小时,终于感觉孩子应该能理解一些了。 女儿又一句：“可我同桌说她爸爸用人生(人参)是泡茶喝的……”');
INSERT INTO `tpshop_joke` VALUES ('2667', '某一个公交车站，有一男一女在等车。 男问女说：“你有男朋友吗？” 女说：“没有。” 男：“怎么不去找一个？” 女：“我老公不让我找！”');
INSERT INTO `tpshop_joke` VALUES ('2668', '昨天刚跟一少妇啪啪啪！她说她老公出差了没怎么快回来，所以我就留了一把钥匙，到了第二天傍晚的时候我想给他惊喜，偷偷的跑去她家，一开门听见了哼声，然后见一男一少妇看着我，我机智的回答:表姐我回来了，男的连忙塞了我口袋两千元，麻烦不要告诉你姐夫。我既然无言以对了！');
INSERT INTO `tpshop_joke` VALUES ('2669', '今天下了点小雨，路比较湿滑，lz骑着自行车去上班，突然车轮打滑，左边一个保时捷，右边一个老太太，我想，装着老太太陪点钱就好了，保时捷可赔不起，就装在了老太太身上，旁边围上来了很多人，这时保时捷车主下了看热闹，我刚想赔钱，谁知那个老太太拼命抓住车主，说：“他撞的，他撞的”');
INSERT INTO `tpshop_joke` VALUES ('2670', '高中时候，有一同学下午想出去上网。但是被门卫拦住了，不让走，于是在门卫室和门卫聊天。 各种聊，两个小时后这哥们说:“哎，时间也不早了，那我回去了，你慢慢忙！” 门卫说:“行，路上小心点！”然后就出去了！ 为了上个网，花了两个小时去铺垫！我顿时五体投地！');
INSERT INTO `tpshop_joke` VALUES ('2671', '这几天都在说：“地球都找到它的好哥哥了，我的妹子你在哪？”，我特么的想告诉你，你不发射信号，能看到距你1400亿光年的妹妹么???');
INSERT INTO `tpshop_joke` VALUES ('2672', '\"有一颗小白菜，味道清清淡淡，淹没在白菜堆中普普通通，直到有一天被路过的小白兔相中，它问白兔为什么是我，白兔说：我饿了，低头一眼就看到了你。我选择了你只因为你出现得刚刚好。\"[☀] [?] \"后来胡萝卜出现了，兔子抛弃了白菜，\'为什么？\'白菜问，兔子说\'胡萝卜最适合我，你只不过在她之前出现的一个错的人。我现在明白我想要什么了。我喜欢你但是不爱你，早晚都会离开你。\' 然后白菜心冷了，把心缩起来成了一颗卷心菜。\" [呵呵] [?]');
INSERT INTO `tpshop_joke` VALUES ('2673', '今天我和老婆一块看电视。正巧，电视上正在播放一新闻调查说：“70%的男人都想有婚外恋……” 我立即对老婆表白：“我绝对是属于那30%的男人，真的，否则，天打五雷劈！”这时，电视上继续说：“其他30%的男人已经有了婚外恋……”');
INSERT INTO `tpshop_joke` VALUES ('2674', '昨天帮老妈买米，我扛到5楼半，然后交给媳妇！媳妇艰难的拉到六楼、我妈开门一看，那瞬间就热泪满眶了。进门又是给她拧热毛巾，又是削水果的，把我丢一边数落！我也就点支烟，在旁边笑笑！');
INSERT INTO `tpshop_joke` VALUES ('2675', '不爱理你的人就别去打扰了，赔了笑脸还丢了尊严； 不合适的鞋就别去硬塞了，磨了自己的脚还落下血泡。 费尽心思讨好，却换来不需要；使劲对一个人好，却得到不重要。 不要把时间花费在一厢情愿上，打扰了别人，作践了自己。 其实，是你的走不掉，不是你的抓不牢。 永远要记住：不惜我者，绝不为我所藏；不爱我者，你不配在我心上！用饱满的精力，去爱珍惜你的人，去珍惜爱你的人');
INSERT INTO `tpshop_joke` VALUES ('2676', '一个朋友发了一条说说：你敢说出自己经常丢的3样东西吗？我先说，眼药水，唇膏，伞。 我回复道：脸，爸妈的脸，老师的脸。 结果其他人全是回复：给跪了！');
INSERT INTO `tpshop_joke` VALUES ('2677', '最佳男朋友 去买奶茶，店主小情侣俩忙的团团转。 女生突然转过身，小声的对男生说了一句：“我真的很累了。” 眼泪就掉下来了。男生没有一秒的迟疑，冲排队的人喊了一句：“不好意思，今天不卖了。”');
INSERT INTO `tpshop_joke` VALUES ('2678', '今天下午下班后给上幼儿园的儿子做饭，那兔崽子盛了一大碗饭结果只吃了几口就不吃了，我教育他说：“农民伯伯辛辛苦苦种的粮食和蔬菜，你剩下来，对得起农民伯伯吗？”结果他小手一挥说：“农民伯伯辛辛苦苦种的粮食和蔬菜，你为什么做的那么难吃！”我现在反省中。。。');
INSERT INTO `tpshop_joke` VALUES ('2679', '女朋友问我：“你们男的看女人第一眼是不是都看胸啊？” 　　答：“没有，第一眼先观察眼睛。” 　　女友：“哎哟，什么时候审美观变那么高尚了” 　　答：“没有，如果她眼睛不看我的话，第二眼再看胸。”');
INSERT INTO `tpshop_joke` VALUES ('2680', '今天在步行街看到两个小姑娘跪在地上，前面写着无非就是钱包丢了，没钱吃饭求给多少多少吃个包子什么的。于是乎我果断跑去买了10块馒头两瓶饮料递给那两姑娘：吃吧，不用谢我，。她们只好在众目睽睽下啃掉那些馒头。 我走了一圈后又在原地看到那两姑娘还在跪着，心想那两娃得多饿呀，于是乎我又跑去买了20块馒头。我做对了吧！');
INSERT INTO `tpshop_joke` VALUES ('2681', '一群恐怖分子冲进一个国际会议的会场，将参加会议的一百多名政客扣为人质。随后，恐怖分子向政府提出了他们的条件，并宣称如果不答应，他们将会每过一个小时释放一名政客。');
INSERT INTO `tpshop_joke` VALUES ('2682', '极左的时代，到处都在批判今不如昔论。批判会一直开到某生产大队，会场上无人发言，农民们以种田为本，不想理论问题。 大队长只好带头，琢磨半晌，终于想出批判今不如昔的理论，他 说：大家伙想想，金子，多少钱一厅?锡，多少钱一斤?说金不如锡不是混帐话吗? 大家点头称是。');
INSERT INTO `tpshop_joke` VALUES ('2683', '前东欧某国公安部门在审查一个申请移居国外的公民时问：你为甚么要移居西方呢？你在这里有稳定的工作，稳定的收入，有免费劳保，你还有什么可埋怨的呢？　　我没有什么可埋怨的。　　那你为什么要移居西方？？　　因为在那里我可以自由埋怨。');
INSERT INTO `tpshop_joke` VALUES ('2684', '浙江某市一领导在会议上外地客人介绍经验，说其市经济得到迅速的发展，是一靠警察，二靠妓女。众人不解，领导继续阐释：警察，就是改革开放的警察，妓女，就是百年不遇的妓女。众人更加愕然，后经看书面资料，才知道是一靠政策，二靠机遇。');
INSERT INTO `tpshop_joke` VALUES ('2685', '某考选部长王先生，数年前谣传将接任陈履安先生为某监察院长。　　王先生戏称，只有猴子元年才有可能（盖猴子当皇帝时，才有猴子元年，猴子不可能。故他也将没机会）。　　然而近日上层关爱的眼神眷顾，报派王先生为某监察院长之迹象甚大。　　难道，真有猴子元年。　　我可要去查查李先生的生肖了…。　　还是某报故意…。');
INSERT INTO `tpshop_joke` VALUES ('2686', '克格勃的一位干部去拜访自己的好友，那个好友是一位老师。见到他时，好友正在唉声叹气。于是干部问：什么是让你心烦？　　好友有气无力的说：唉，这个问题连你的部门也解决不了。今天我问学生彼德若夫：‘《叶普盖尼。奥涅金》是谁写的？’他居然说不是他写的。后来我又问了季里连科和艾若娃，他们也说不是他们写的。这怎么办？　　两天后，干部给老师打了电话，说：你放心，他们三个都招了。他们说《叶普盖尼。奥涅金》是他们写的。');
INSERT INTO `tpshop_joke` VALUES ('2687', '克林顿访英期间，与撒切尔夫人,杰弗里.豪等共进晚餐。为活跃气氛，撒切尔夫人问杰弗里.豪：你的父母有个孩子，既不是你的兄弟，也不是你的姐妹，他( 她 )是谁？豪大笑着答道：就是我，豪呀。克林顿感到很有趣，回到白宫后，问克里斯托弗到：你的父母有个孩子，既不是你的兄弟，也不是你的姐妹，是谁？克里斯托弗回答不出。克林顿得意地大笑到：是豪呀。');
INSERT INTO `tpshop_joke` VALUES ('2688', '苏联看德国因盛产啤酒而每年赚进大笔外汇，决定仿效，开始派人研究制造啤酒的技术。第一批啤酒制造出来后，苏联送了一些样品给德国鉴定品质。一个月后，德国回函给苏联：恭喜，贵国的马很健康！');
INSERT INTO `tpshop_joke` VALUES ('2689', '甲歌手在模仿某歌星时，总有几句唱不像，心里很苦闷。于是，请教乙歌手。乙歌手倒也爽气，实话实说：上台演唱中，估计下一句有困难时，不必惊慌，只要大喝一声：‘来，大家一起唱！’接着把话筒面向观众，这不，问题就解决了吗？甲歌手听后，茅塞顿开，欣喜万分。');
INSERT INTO `tpshop_joke` VALUES ('2690', '上周末我去做头发，洗头、按摩、剪发、烫头，一阵折腾，好几个小时，有些无聊，便和按摩的那个女孩聊起来，女孩问我：姐姐是做什么的？　　我回答说：我是做媒体的。　　那女孩一听我是做媒体的，很是兴奋，对我说：是吗？这么巧！我原来也是做媒体的。　　我看看女孩，有些不敢相信：你也是做媒体的？报纸还是杂志？　　那女孩听了我的话，很困惑地看着我说：就是美体啊！美丽身体的‘美体’啊……');
INSERT INTO `tpshop_joke` VALUES ('2691', '　　在火车上又遇印度友人，聊着聊着，最后聊到了吃饭。 　　我说：“筷子是最方便的工具。” 　　他说：“是手。” 　　“没办法，只好待会儿下火车请你吃饭了！” 　　我刚把话一说完，他立马紧张起来了！ 　　过了一会儿弱弱的说了句：“不是吃火锅吧！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2692', '今天老婆表妹结婚，我和老婆都去了，结婚5年了，今天才知道前女友就是老婆表妹，老婆还对我说，前女友结婚高兴点。原来就我最后一个人知道的。');
INSERT INTO `tpshop_joke` VALUES ('2693', '跟女友KF脱光衣服的时候女友让我一边嚼炫迈一边啪啪啪。然后我根本停不下来了');
INSERT INTO `tpshop_joke` VALUES ('2694', '‍‍‍‍晚上上厕所蚊子太多，掰一节蚊香拿着去了。 用两个手指夹着，老想往嘴里放。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2695', '‍‍‍‍王局长给刺客100万用于刺杀李厅长。 两个星期后，报纸上刊登李厅长自杀身亡。 王局长问刺客：“你是怎么办到的。” 刺客神秘的笑了笑说：“小事一桩，只要想办法引导他炒股就行啦！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2696', '男人只有穷一次，才知道哪个女人最爱你。 女人只有丑一次，才知道哪个男人不会离开你。 人只有落魄一次，才知道谁最最最在乎你。 陪伴，不是你有钱我才追随。 珍惜，不是你漂亮我才关注。 时间留下的，不是财富，不是美丽，是真诚。');
INSERT INTO `tpshop_joke` VALUES ('2697', '‍‍‍‍记得很多年前，追一个女孩，有一次约会去欢乐谷玩。 为了好好表现一番，买了两张通票，尽情欢乐！ 坐了一个海盗船后，差点嗓子没喊破。 下来直接腿软了，吐的一逼，然后就没有了然后！ 这恐高症真特么不是吹的！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2698', '我去网吧打游戏，半夜想睡觉，结果旁边那女的打游戏太兴奋了，还尖叫。我实在受不了就说，“小姐你能不能让我睡一下？”我话还没说完就挨了一巴掌，我说错了吗？');
INSERT INTO `tpshop_joke` VALUES ('2699', '我儿子放学回来，（小学四年级）我让他给我做点事他不干跑去玩了！过会他来伸手要钱买吃的！我生气的说，那么懒吃狗屎去。他把手伸着不动:妈妈给钱啊！我大声叫不是让你吃狗屎吗。他慢悠悠的说给钱买狗啊，没狗哪有狗屎吃啊。这兔崽子……');
INSERT INTO `tpshop_joke` VALUES ('2700', '‍‍‍‍以前我带着两徒弟去吃夜宵，点了三个拌米粉，三个土鸡炖罐汤。 过了两分钟老板问我：“你们三个土鸡哦？” 我立马站起来：“老板你全家都是土鸡！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2701', '现在小明三年级。 小明：我懂的东西够多了，没必要继续读书。 老师：真的？你只读到三年级，打算做什么？ 小明：教二年级。 老师：滚出去...');
INSERT INTO `tpshop_joke` VALUES ('2702', '我妈和我爸吵架，我妈炖的排骨，冲着我爸喊到：我喂狗都不给你吃。 此时我正啃着排骨，瞬间画面定格，我转头问我妈：妈，我是亲生的么！');
INSERT INTO `tpshop_joke` VALUES ('2703', '我一朋友特会过，什么东西都舍不得扔的那种。 有一年冬天去他家玩，见他正在用冷水洗澡，我说：哥你咋啦？天这么冷怎么用凉水洗澡啊？ 结果这货说了一句我一辈子也不会忘的话：家里还有两盒感冒药，再不吃就过期了。');
INSERT INTO `tpshop_joke` VALUES ('2704', '今天去买水果看到柿子挺好的（硬的）不知道甜不甜。 老板娘说：我给你削一个你尝尝。 然后拿个又红又大的，我正感慨老板娘好大方，她削下一小块给我，自己抱着剩下的欢乐地啃了起来。 老板一脸黑线，咆哮到：“你个败家娘们一会看不住都不行，今天就吃四个了。”');
INSERT INTO `tpshop_joke` VALUES ('2705', '一天实在是憋不住，怒吼这对媳妇说：以后你不能穿吊带。 媳妇迷惑的问：为什么。 你穿吊带就跟大老爷们穿背心一样…');
INSERT INTO `tpshop_joke` VALUES ('2706', '一个好消息，一个坏消息。 好消息是，我收养的喵星人已经学会在固定的地方拉屎了； 坏消息是，把沙发上的屎弄干净挺麻烦的。');
INSERT INTO `tpshop_joke` VALUES ('2707', '如今车子，房子可以分期付款，手机可以分期付款，就连老马哥的英雄武器也可以分期付款了，但是我现在最关心的是媳妇什么时候也可以分期付款？？');
INSERT INTO `tpshop_joke` VALUES ('2708', '结婚的时候问：老公，如果有个美女脱光了在床上等你你会被诱惑吗？ 我以为老公会说：不会的，你放心，我只爱你。 没想这二货说了一句：还有这样的好事？');
INSERT INTO `tpshop_joke` VALUES ('2709', '女朋友比我高，看电影得时候都是我靠她肩上。 七夕那天她就很不乐意了，问我：为什么那么矮，别人都以为我带儿子来看电影呢。 我回了句：妈，我们回去睡觉吧，我好困了！ 脸到现在都还疼呢，当时要是说有地方长就不会被揍了。');
INSERT INTO `tpshop_joke` VALUES ('2710', '天热，公交大巴换空调车了，票价由原来的一块调为两块，大妈上车投了一块，司机：两块啊。 大妈点头：嗯，凉快。 司机说：投两块! 大妈笑曰：不光头凉快，浑身都凉快。 说完大妈径直往后头走… 司机急了大喊：钱投两块！ 大妈：后头人少更凉快......');
INSERT INTO `tpshop_joke` VALUES ('2711', '用输入法打“cy”打出“初音”是歌迷， “次元”的是漫迷，打出“成语”的是学霸，打出“产业”的是土豪，打出“穿越”的是影迷，打出“成员”的是基佬，打出“吃药”的已经放弃治疗！测测你是什么？（我的是创意，么么哒~）');
INSERT INTO `tpshop_joke` VALUES ('2712', '一妹子去男友家里和男友一起看电视，突然想体验一把偶像剧里女主睡着后男主温柔的给盖上毯子深情亲吻女主额头浪漫的感觉。于是就开始装睡，过了一会男友果然靠了过来，用手在妹子眼前晃了晃，看妹子没反应，然后只听到噗噗噗放了一大串响屁。');
INSERT INTO `tpshop_joke` VALUES ('2713', '单位新来的小姑娘非常马虎，不小心把重要资料和废纸一起放入碎纸机摧毁了，吓得蹲在地上大哭。路过的经理听见了关切地问：怎么了？小姑娘抬起头望着他哽咽地说：资料也碎了…经理一愣，随即接着唱到：安…安静的睡了？在我心里面，宁静的夏天？');
INSERT INTO `tpshop_joke` VALUES ('2714', '昨天在一个高档会所门口停车，看见一个车位有辆迈巴赫要停进去，我上前敲了敲他玻璃，甩出去一百块钱对他说：“这个车位我看中了，你去别的地方停！”　　丫冲我脸上甩来十几张一百的叫我滚一边儿去。　　后来我又如法炮制。。。MD，一晚上赚了两万多。不说了，我准备辞职甩豪车去了！！');
INSERT INTO `tpshop_joke` VALUES ('2715', '男子逛街，看见一美女包被小偷抢走，男子拼了命的去追！连追了好几条街，终于把小偷抓住！　　小偷趴在地上说：“哥！那女的是你女朋友呀！这么玩命！”　　男子也趴在地上说：“有点职业道德行不！我TM都跟一天了，你TM上来就抢呀！”');
INSERT INTO `tpshop_joke` VALUES ('2716', '弟弟处了女朋友，拿照片来家炫耀：“你们看，她漂亮吧？”老爸看了一眼说：“丑死了。”弟弟立马不干了回敬道：“你老婆黑不溜秋的，你不是还娶了？”老妈在旁边面露不悦。');
INSERT INTO `tpshop_joke` VALUES ('2717', '屌丝：今天跟女神表白了，她很高兴，说要请我吃东西，可我权衡了一下，还是婉言拒绝了！　　朋友：你这是表白成功了，为什么还拒绝她？　　屌丝：她说，去吃屎吧！');
INSERT INTO `tpshop_joke` VALUES ('2718', '在等地铁，一个老头跟我搭讪聊起了加班费，我说：“加班费大家都是三倍的吧？” 老头说：“不不不，我的加班费是平时的5-8倍。” “什么工作这么牛B？” “乞丐。”');
INSERT INTO `tpshop_joke` VALUES ('2719', '上学那些事 （9）寒假作业其实就是你写一个月，老师写一个阅。 （10）小学上课费嘴，初中上课费笔，高中上课费脑，大学上课费流量。 （11）上学的时候总想玩电脑，放假了只能对着电脑发呆…… （12）我有一个梦想。一张试卷只有5个填空题，学校_科目_班级_姓名_学号_。每空20分。');
INSERT INTO `tpshop_joke` VALUES ('2720', '‍‍门诊有个六岁宝宝挂针，又来了一个四岁的。 六岁的就跟那个四岁的说：“小不点，我们都是男子汉，扎针可不能哭。” 然后，就默默叨叨开始嘟囔。 针扎完了，四岁的宝宝说话了：“你闭嘴吧！别说话了。” 现在的孩子，唠嗑真硬。‍‍');
INSERT INTO `tpshop_joke` VALUES ('2721', '在地铁上，我给一大妈让了座，大妈高兴地和我攀谈，问：孩子多大了？我：26。大妈羡慕地说：你长得真年轻，看起来也就30出头，孩子都26岁了。');
INSERT INTO `tpshop_joke` VALUES ('2722', '语文考试作文题目引起了热议。作文题：一条鱼逆流而上，他以精湛的游技，冲过浅滩，划过激流，绕过层层渔网，躲过水鸟的追逐。他不停的游，最后穿过了山涧，游上了高原。然而还没来得及发出一声欢呼，很快就被冻僵了。于是出现了下面图片里的文体。。。。看到淘宝体时我笑了。');
INSERT INTO `tpshop_joke` VALUES ('2723', '我们是不是也能这般幸运，遇见属于自己的这么一个人。也许像大雄一样不够聪明，不完美，不那么受欢迎，单纯得有些傻气，但是，抛却属于你的所有光环，所有附加值，所有互利关系，他不会放弃你，他会等你，他依旧那么爱你。会不会有一天，我们会等到自己的大雄。他说他不要口袋，他说他只爱你。');
INSERT INTO `tpshop_joke` VALUES ('2724', '装傻的最高境界在于：拿得起、放得下、看得开、不动气、知足、感恩、善解、包容。潇洒人生六得：来得、做得、玩得、舍得、放得、走得。活到老，学到老，动到老，玩到老，乐到老，永不老。');
INSERT INTO `tpshop_joke` VALUES ('2725', '黄忠六十才跟了刘备走，德川家康七十打天下，姜子牙八十为丞相，佘太君百岁挂帅，孙悟空五百岁西天取经，白素贞一千多岁下山谈恋爱。年轻人，你说你急什么急！');
INSERT INTO `tpshop_joke` VALUES ('2726', '【厨师不吃的四样菜】一知名厨师说，他去饭店很少点四道菜：杭椒牛柳、水煮鱼、水晶虾仁、蒜香排骨。因为这些菜用添加剂的可能性都很高。在外面买熟食时，他也很少吃灌肠、培根等。“大家往往以为培根是腌肉，其实根本没经过腌这道工序，是因为加了硝酸钠，才有了烟熏的味道。”');
INSERT INTO `tpshop_joke` VALUES ('2727', '如果有一个身材火辣的女人脱光衣服站在一个男人面前，女人狂亲那个男人，有哪个男人真的会抵得住诱惑，讲出一句：“对不起，我已经有老婆了，请你穿好衣服，不要贬低自己。”【如果你真的是好男人，勇敢转发！如果你喜欢这样的男人，勇敢转发！】');
INSERT INTO `tpshop_joke` VALUES ('2728', '拉登在临死时说了一句话：“想要我的财宝吗？想要的话，那就去找吧，我的财宝都在哪里，美国！”于是越来越多的恐怖分子奔向美国，世界迎来了大恐怖时代!');
INSERT INTO `tpshop_joke` VALUES ('2729', '《速度与激情6》四大硬汉男主角！！~范·迪塞尔+保罗·沃克+杰森·斯坦森+巨石强森。。。。腐女们别流鼻血哦~~~2013年5月24日上映！！！');
INSERT INTO `tpshop_joke` VALUES ('2730', '据德国杂志《男人车》公开的一份调查结果，驾驶保时捷的男人比驾驶其他车型的男人更容易发生婚外情，紧随其后的是宝马车主，承认有婚外情的人占46%。在女士中，驾驶“奥迪”的最不可靠，41%的奥迪女主人承认有婚外情，奥迪A6是全球车震率最高的一款车，几乎每一辆奥迪A6里都曾经发生过车震事件。');
INSERT INTO `tpshop_joke` VALUES ('2731', '一夫妻去买床，男^看上一张床，就问老板，老板你这里的床动一动会不会有支嘎支嘎的声音。老板看了一看小伙说到，放心吧累死你都不会有支嘎支嘎的声音，只看旁边他老婆的脸红得。');
INSERT INTO `tpshop_joke` VALUES ('2732', '‍‍‍‍大学那会寝室一哥们超懒无比，衣服堆一个月不洗那种。 周末寝室偷偷弄火锅，大家都去买材料，这哥们就提了点蘑菇过来，味道还不错。 就问他：“哪买的？” 他说：“内裤放盆里两月了，看长了蘑菇就摘了下来。” 不说了，我们几个室友今年清明又准备去看他了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2733', '‍‍‍‍今天去我姐家，看见老姐在打孩子。 我：“打孩子干嘛啊，孩子那么小，打他干嘛。” 姐：“他骗老师，不去上课。” 我：“那也不能打啊，你要教育他。” 姐：“他跟老师说他舅舅死了，他要请假回来看他舅舅最后一眼。” 我：“姐你歇会，我来。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2734', '今天早上走路去上学的时候，经过一个小斜坡，在斜坡上遇到一个狗狗，这货斜斜的瞄了我一眼，然后坐下翘起后腿挠痒痒，结果这货没坐稳滚下去了，下去了！丫的爬起来后就对我狂叫啊！尼玛，关我啥事儿。');
INSERT INTO `tpshop_joke` VALUES ('2735', '‍‍‍‍女神：“我做你女朋友吧！” 屌丝：“哇真的么？” 女神：“嗯！” 屌丝：“前段时间不是有个高富帅追你么？” 女神：“我们只是普通朋友啦！” 屌丝：“可是你喜欢我什么呢，我又不帅又没钱！” 女神：“你不是喜欢孩子嘛！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2736', '‍‍‍‍周末又去相亲了，朋友忙问：“这次成功没！” 我说：“三分之二成功了！” 朋友问：“怎么讲？” 我一脸无奈的说：“我同意了，介绍人同意了，对方没同意！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2737', '本人特喜欢吃臭豆腐，以此为背景。一次在路边看到卖臭豆腐的，于是乎便买了些。给了钱，就在这时候，城管来了，只听到老板说了声，小伙子请随我来。我想着，钱都给过了，不能不要吧。我就追了上去，跑了几里路，直到看不到城管了，他才停下。事后对我说，你是我见过最有毅力的吃货。。。。。。。.');
INSERT INTO `tpshop_joke` VALUES ('2738', '‍‍‍‍老师：“小可你能想像老师们二十年后相聚的情景吗？” 小可：“可以唱出来吗？” 老师：“可以。” 小可：“二十年后，你们来相会，谁都不会记住谁，全部送到火葬场。你一堆，他一堆，运到农场做化肥。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2739', '为什么每次和铁哥们儿去吃自助餐，当我们走的时候，迎宾小姐对我们说的都是：您慢走！而对其他客人说的却是欢迎您下次光临呢？？？');
INSERT INTO `tpshop_joke` VALUES ('2740', '‍‍‍‍“姐夫，我姐出去了？” “恩，她一会儿就回来了，你等会。” “哎呀！我想死你了。” “别闹别闹，你姐都要回来了。” “你放心吧！我叫我老公带我姐开房去了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2741', '‍‍‍‍知道为什么文科生和理科生不好恋爱吗？ 因为如果文科生问：“叶的离去，是风的追求还是树的不挽留？” 理科生会回答：“是脱落酸。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2742', '‍‍舍友们在宿舍讨论，你说：“咱们三八妇女节也不休息，五四青年节也不休息，六一儿童节也不休息，咱们到底算什么呀！” 然后另外一个室友幽幽的说道：“咱们清明不是放假了么？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2743', '　　今天早上我班有一丑女在班里吃辣条，那个味道弥漫了整个教室，老师进来对我们班的同学说:你们宿舍昨天是不是停水了，怎么全班都不洗脚！');
INSERT INTO `tpshop_joke` VALUES ('2744', '“有次去东北旅游，逛街的时候把随身带的萧给弄丢了。” “然后呢？” “然后我就到处瞅瞅，到处找，你还别说，东北老大爷就是热情，上来就问我：你瞅啥，找萧啊！ 啊……然后就没然后了');
INSERT INTO `tpshop_joke` VALUES ('2745', '‍‍‍‍有天我和老公带儿子出去玩。 儿子：“爸爸，我要买玩具。” 爸爸：“你出来没说，我没带钱。” 儿子：“那如果我说了呢？” 爸爸：“那我就不出来了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2746', '‍‍小学的时候，老师说让每人带一个鸡蛋做彩绘，第二天每人把做好的鸡蛋带到学校。 老师满意的看看了，说：“同学们，做的不错，老师要留起来，做个纪念。” 等长大了我才知道，妈蛋，原来那会儿他老婆坐月子。‍‍');
INSERT INTO `tpshop_joke` VALUES ('2747', '我问朋友狗怎么叫，朋友脱口而出：汪汪，我又问：小狗呢？朋友：呜呜，我：那藏獒呢？朋友难住了，深思熟虑之后，斩钉截铁道：汪汪，巴扎嘿。太有才了');
INSERT INTO `tpshop_joke` VALUES ('2748', '有个人抢劫被警察追，他突然看到前面有台小车停在那还是个女的在开，欣喜若狂的冲上后排拿出刀对着那个女的说快开车不然捅死你，这时坐在旁边的一个小伙子悠悠的说：阿姨咱们不要紧张，按我刚才说的，踩离合，挂一档，走……');
INSERT INTO `tpshop_joke` VALUES ('2749', '‍‍男女生亲热，被老师发现带到办公室。 老师问：“亚当和夏娃是在什么情况下发生爱爱行为的。” 男生说：“是在相爱的情况下。” 老师说：“错！是在没有人的时候！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2750', '司马光小时候在玩耍，一个小伙伴急匆匆跑过来说，有个小孩掉进水缸里了，你快去救他吧。司马光问，缸在哪里？小伙伴迟疑后，撅起了屁股……啪啪啪的声响和节奏太强，最终震碎了水缸，小孩得救了，后来，大家都很佩服司马光的聪明才智!');
INSERT INTO `tpshop_joke` VALUES ('2751', '说一真事：话说有一次看到女神的微信刚更新：我现在需要男人。一看到这我就心澎湃，这是上天在给我机会，追了几年终于到头了，立马又洗澡又打扮，屁颠屁颠的跑过去，她一看到我就说：来得刚好帮我把冰箱抬下去修一下，他一个人抬不动。我看了一下另外一男的，那眼神比我还失望。');
INSERT INTO `tpshop_joke` VALUES ('2752', '老公：“老婆，家务活分开做，钱也要分开管，好吗？”老婆：“好啊，不过要看怎么个分法。”老公：“十块钱以下的票票我管，五十块钱以上的票票你管。”老婆：“好，就这样定了。”这天，老公去银行取钱，对工作人员说：“取一千块，全部要五元的……”');
INSERT INTO `tpshop_joke` VALUES ('2753', '一个富翁在做腹部手术之前，特意请人做了一套纯金手术器械、并对医护人员说：“诸位，这些刀剪以后留给你们做纪念吧。”富翁想，这下他们肯定会把手术做好，并且不用担心把什么东西忘到自己肚子里了，手术进行到一半时、主刀医生大怒：“手术器械都去哪里了？，我手术还没做完！”');
INSERT INTO `tpshop_joke` VALUES ('2754', '公园里一对情侣在甜蜜约会，一会女的说：老公，我牙痛。男孩子转过头亲了她一口，问：还痛吗？女孩子说：不痛了耶。 过了一会，女孩子又说：老。。公。。。我脖子痛，。。 男孩子又亲了她脖子一下，问道：这回不痛了吧。 旁边一老太太，站着看了好一会，忍不住问道：小伙子你真神了，你能治痔疮不？');
INSERT INTO `tpshop_joke` VALUES ('2755', '　　课堂上讲师女的公认漂亮。然后走到我边上一个睡了两节课的同学边叫醒他，故作幽默的说，你都睡了我几节课了，要不要放你回宿舍睡啊！你以为这是高潮，高潮是只有我一个人秒懂，狂笑三秒，鸦雀无声啊！尼玛的，糗死了！');
INSERT INTO `tpshop_joke` VALUES ('2756', '我妈妈在客厅拖地，我在房间玩电脑，突然我流鼻血了，房间没纸 我就赶紧跑出客厅说：“妈，我流鼻血了”我妈居然来一句：“快拿手接住，我刚拖的地”。。。。');
INSERT INTO `tpshop_joke` VALUES ('2757', '黑夜，包公带展照一干人等去暗中调查案子，忽然找不到包公了，展昭说：“包大人，您快笑一笑啊！”公孙策说：“没用的，包大人刚吃了巧克力饼干.\"');
INSERT INTO `tpshop_joke` VALUES ('2758', '大明问小明如果有两个女人愿意跟你，一个人丑胸大一个人美没胸。你会选哪个？小明走到厕所里想了5分钟出来神清气爽选人丑的那个，你呢？哥。大明嘿嘿一笑那要看丰胸和整容哪个贵了。');
INSERT INTO `tpshop_joke` VALUES ('2759', '这辈子说过最后悔的话是什么？你觉得怎么好看就怎么剪吧');
INSERT INTO `tpshop_joke` VALUES ('2760', '禅师又指了！ 警察遇到禅师，问：“大师，我每天反恐维稳，一级战备，定点巡逻，没有休息，不能顾家，待遇又低，房子也是按揭，提拔晋级遥遥无期，您说我该怎么办？” 禅师指了指街边相貌平庸的妓女，民警顿悟：“您是说还有为了生存，出卖肉体和灵魂，每天强颜欢笑，内心流泪却依然坚持的人，让我好好珍惜现在的工作对吗？”禅师摇了摇头说：“我的意思是你能干就干，不干有的是人干！”');
INSERT INTO `tpshop_joke` VALUES ('2761', '晚上在酒吧，一位美女过来问我是一个人吗？我立马就火了，我不是一个人难道是一头驴啊？然后他就问我要电话，我果断拒绝了，我说你有毛病吧，好几千块钱的电话能给你？他说跟我要电话号码，我说我这号码都用好多年了也不能给你啊！然后他就走了。我发现他真是有病啊，就差抢了是不是？见面就喊我，要电话不给完了还要电话号码，我号码里面还有话费呢！气死我了！什么样的骗子都有！');
INSERT INTO `tpshop_joke` VALUES ('2762', '邻居家三口，爸爸问女儿狗狗怎么叫，女儿回答 汪 汪，爸爸又问猫呢，女儿 喵 喵，爸爸又问人怎么叫，女儿回答，啊 啊 啊 啊用力，妈妈当时脸都绿了.爸爸问了句！那爸爸不在时妈妈怎么叫，女儿回答。我不知道，每次爸爸不在家时，隔壁王叔叔都给我十块钱叫我出去玩了');
INSERT INTO `tpshop_joke` VALUES ('2763', '今天早上，在洗脸的时候，发现自己满嘴都是血，我当时特别害怕，心想: 难道我是中毒了吗？ 我赶紧叫我女朋友送我去医院。 只见我女朋友特别鄙视地对我说: “看你那孙子样，昨天跟你说我来大姨妈了，你还说不怕……”卧槽');
INSERT INTO `tpshop_joke` VALUES ('2764', '杭城某中学门卫，六十五岁的周大爷因屡次遭到校花骚扰，被迫提出辞职，称现在的年轻人真让人受不了。据说该校花没事时候总往门卫室跑，喜欢聊一些隐私问题，还有次装晕倒硬要周大爷人工呼吸。周大爷无奈提出辞职，校长再三挽留才留下继续工作。 为避免类似事件发生，校方请周大爷以后不要开保时捷来上班，周大爷也表示准备换成奔驰。');
INSERT INTO `tpshop_joke` VALUES ('2765', '以前那小树林里，住了两头牛，一只公牛，一只母牛。一天公牛要出差，愁！这母牛托付给谁呢？托付给猴子吧，太精。给老虎吧，不安全。思前想后，决定托付给大象，老实憨厚，还安全。公牛出差去了。回来后那天早上，大呼：牛B大了。');
INSERT INTO `tpshop_joke` VALUES ('2766', '两口子一起钓鱼，老婆半天也钓不到。 　　她问：老公，“沉鱼落雁”是什么意思? 　　老公：是说西施长得太美了，连鱼儿都害羞得躲到水底。 　　她：哦，怪不得我钓不到鱼。');
INSERT INTO `tpshop_joke` VALUES ('2767', '老妈一进门：“乖乖，来，到妈妈这来，给你好吃的。” 我一扭头：“老妈，说的人都起鸡皮疙瘩了，买的什么好吃的啊？” 老妈一把抱起贵宾狗：“咋的了？我跟你说话了吗？来乖乖吃东西不理她。” 我：“老妈，什么意思？难道在你心里，我都不如它吗？” 老妈：“当然了，它是贵宾狗，你是单身狗！你能跟它比？”');
INSERT INTO `tpshop_joke` VALUES ('2768', '刚刚听两同事对话，同事说：“我女朋友气消了。” 另一同事说：“又来借气筒的吧！”');
INSERT INTO `tpshop_joke` VALUES ('2769', '我是个坏学生，天天在后面玩手机。 有一天，我们班后面的一个女生玩手机被发现了，班主任准备过来收她手机。 于是她非常聪明的想到一个办法：把手机收在内内里用腿家住。 也是老师来的时候毛都没找到。 就在这时我打开手机悄悄的给她打了个电话。 杯具啊！振动模式。');
INSERT INTO `tpshop_joke` VALUES ('2770', '在街上逛街，看到夫妻吵架，男的想揍这个女的。 女的彪悍的说：“你打啊，打了跟你离婚！” 男的欣喜：“真的？” 女：“真的。” 啪！男的一个巴掌扇过去：“你说话要算数的哈！”');
INSERT INTO `tpshop_joke` VALUES ('2771', '有一次跟老乡去游泳，她是个性格细腻，好面子的人，还是第一次去游泳怕出丑，一路上问这问那的。进了游泳管，换上泳衣我们两个走进了泳池，她突然拉住了我：“怎么游泳池里还有男人？”　　我说：“是啊，游泳池是不分男女的。”　　她说：“不分男女，我的天啊，早知道我就不来了！”　　我说：“干嘛不来啊？你又不是没穿衣服，怕男人干嘛？再说没有男人你穿这么漂亮的泳衣给谁看啊？”　　听了我这话，她在游泳池边上走了几个来回才肯下去！');
INSERT INTO `tpshop_joke` VALUES ('2772', '旅行社说：一个iPhone手机可以游云南一圈，一辆小车可以游新马泰住海岸宾馆，一间套房首付可以游遍世界，到时候，你的世界观可能就变了。　　房产商说：到时候你两手空空一身黝黑回来，看到朋友有车有房有媳妇，拿着iPhone切水果，你的世界观就真的变了。');
INSERT INTO `tpshop_joke` VALUES ('2773', '小明和他的父母正在一家小饭店里吃饭。　　小明狼吞虎咽，把肚子吃得滚圆滚圆。但是他的父母却对他教育到：“明明，以后要记住，再好吃的东西也不能吃起来没够，别人会笑话你的，尤其是在公共场合。”　　小明羞愧的低下了头。过了一会儿。　　一位孕妇吃完饭后准备离开，正好路过小明他们一家坐的位置。　　淘气的小明拦住孕妇，说：“阿姨，你太没出息了，竟然把肚子吃成这样！”');
INSERT INTO `tpshop_joke` VALUES ('2774', '公交车上，一男见有一美女，他便想了一个主意主动和其搭讪。　　男上前对女说：“你好，我想我以前在哪见过你。”　　美女一愣，说：“咱们认识吗？”　　男说：“我想起来了，昨天晚上也是在这路公交车上……”　　美女突然狠狠地给了男一响亮的耳光，愤怒地说道：“你这人还算诚实，原来昨晚下车时摸了我胸一把的男人是你！”');
INSERT INTO `tpshop_joke` VALUES ('2775', '老婆生孩子，老公在产房陪着。　　孩子生出来，老公兴奋不已，高喊：“老婆，咱生了个儿子！”　　护士在旁边说：“看清楚再说，那是脐带！”');
INSERT INTO `tpshop_joke` VALUES ('2776', '顾客:理发多少钱? 理发师:10元。　　顾客:太贵!我的头发非常少呀。　　理发师:正因为如此，10元中只有3元是理发费，另外7元是头发搜寻费。');
INSERT INTO `tpshop_joke` VALUES ('2777', '向朋友推荐一部恐怖电影，然后一起观看，他总是不厌其烦的问你：出现在镜头上的人最后死了没有？　　尼玛，我全告诉你了还有毛意思啊！');
INSERT INTO `tpshop_joke` VALUES ('2778', '换了一个新手机，旧的给婆婆，来信息的声音是“a new message”每天都有天气预报，话费提醒等信息。　　每次听到这个声音，婆婆都很疑惑，有次问我，手机天天响“打电话没事”“打电话没事”咋回事？没事打电话干嘛》？　　我当时就笑晕了，a new message听成，打电话没事了。');
INSERT INTO `tpshop_joke` VALUES ('2779', '“伙计，你没事吧？”我问。　　“今天真是糟糕透了，”他呻 吟道，“我老板想杀了我，我失去了工作，也失去了家庭……我妻子要离婚，孩子们都不愿意跟我说话。”　　“怎么啦？”　　“我今天早上到老板那里，我是去送文件的，老板不在，但他的老婆在。我们喝了几杯酒，就在那里……突然，老板进来了。”　　“哎呀，真不幸，伙计，”我打了个寒噤，“那你们家又是怎么回事呢？”　　“哦，我是在我岳父开的公司干活的……”');
INSERT INTO `tpshop_joke` VALUES ('2780', '　　今天去小卖部买了包烟，直接转身走了，只听见后头的小哥追着喊“帅哥！你还没结账呢”我一听，理都没理，开着摩托走了，心里想着“帅哥，明显不是叫我。”');
INSERT INTO `tpshop_joke` VALUES ('2781', '小时候做过一种特无聊的事。　　某天突然突发奇想，拿起电话随便打了几个数字，居然通了！　　于是我拿起电话，等那边人接通后，听到对方“喂？”的声音，我果断挂掉。　　过了一会儿，又拿起电话，打给刚刚那苦逼的人，反复这样做……　　　　有谁中枪了?');
INSERT INTO `tpshop_joke` VALUES ('2782', '我的强迫症严重到什么程度了，尼玛！！我玩植物大战僵尸，种个向日葵都要让他左右摇的频率一样啊…不一样就挖了再种啊…');
INSERT INTO `tpshop_joke` VALUES ('2783', '我就纳闷了，我真抓狂了：女人不抽烟就不能有打火机吗？？啊？啊？买个回来点蚊香蝇香，就被我那些哥们、朋友误以为是自己的拿走………这都买五个了，刚刚又找不到了………打个电话问问吧，电话那头还都笑得那么放肆！我死会！');
INSERT INTO `tpshop_joke` VALUES ('2784', '‍‍‍‍有一逗逼室友，喜欢玩LOL。 有一次他在玩披甲龙龟（我们都叫乌龟的），我问他：“你玩的啥？” 他直接来了句：“我是王八。” 过了两秒钟才反应过来，而且他还姓王。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2785', '小时候我一次在家写作业，突然停电了，耶耶耶耶耶！终于不用写作业了，可以看电视了。还特二的去开电视，在黑暗的角落里我爸淡淡的说了说声：丑就够倒霉了，还这么傻！');
INSERT INTO `tpshop_joke` VALUES ('2786', '‍‍‍‍‍‍大考结束后，班主任拿着试卷在讲台上大声斥责：“你们看看，都是同一个老师教出来的，为什么有的同学考得很好，有的同学考得非常差劲，谁能告诉我原因？” 全班无语。 班主任更气了，拍桌子道：“你们一个都找不到原因吗？” 这时教室角落里传来一个声音：“因为监考老师不同。”‍‍‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2787', '‍‍‍‍朋友是个妻管严，每次看见他，大家都要笑话他几句。 但是这小子就是嘴硬，说自己在家里属于老虎型，大家都说没见过这么怂的老虎。 他若无其事的说到：“那是因为我老婆是武松……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2788', '【开心一刻】 公交车上，一流氓调戏一个带着孩子的少妇说：这孩子多俊呢，多像我，全车的人的目光集中在少妇…[白眼]这个时候少妇的一句话 让我有了拜她为师的想法…[憨笑] 少妇说：“能不像吗，都是一个妈生的。” 全车的人都笑了……[鼓掌]');
INSERT INTO `tpshop_joke` VALUES ('2789', '‍‍‍‍一天老师让同学们比喻她。 小红：“老师像蜡烛。” 老师：“不错。” 小明：“靠那得多长啊，30几年了，咋还没烧死你呢。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2790', '夏天蚊子特别多，晚上睡觉的时候，我忘了关纱窗，进来一些蚊子，在我身上咬了好几个包，别的包几天就下去了，可是有个包又红又大，一点也没有下去的意思。这下我害怕了，老婆崔促我上医院去看看，我不敢马虎，到了医院，找到医生，因为位置有些敏感，所以进去以后我扭扭捏捏地对医生说： “你好，能不能让其他人出去，我有个大红包”。医生听完立刻让其他病人出去了，然后关上门，神秘对我说：“以后这事小声点，拿出来吧。”');
INSERT INTO `tpshop_joke` VALUES ('2791', '之前有一首歌很红，记得名字是 如果我是DJ你会爱我吗？我和一哥们用打开手机并输入了歌名，结果没搜到，只出来这么几个字 如果我是电话你会打我吗 [吓]');
INSERT INTO `tpshop_joke` VALUES ('2792', '过年三天，本命年太背了。输了五千多不说，今天去晚去买几个大炮仗放一下，没想到两个狗男女在河边搞搞！炮仗刚响，女大叫，阿！着才叫打pao！我和我弟弟在黑夜凌乱！');
INSERT INTO `tpshop_joke` VALUES ('2793', '　　女神：一天女神问我平时是不是喜欢上网？ 　　我说：蛮喜欢的。。。怎么了， 你以喜欢？ 　　女神：我才开始上网的，，，你喜欢上网，那网上的事，那你应该比较了解吧。 　　我：还可以吧，基本了解。 　　女神：我听说有个女的在网上特出名，叫苍井空，她是干嘛的呀！？？ 　　我：我怎么会知道这个呀，我又没有看毛片......');
INSERT INTO `tpshop_joke` VALUES ('2794', '暑假看来几级关于陈纳德的纪录片，开学老师上课讲轻于鸿毛重于泰山是哪位名人，我当机立断说陈纳德！！至今忘不了同学鄙夷的眼神。');
INSERT INTO `tpshop_joke` VALUES ('2795', '单身太久了，好不容易找个女朋友， 陪她去买衣服，她进换衣间换衣服， 我一迷糊，以为自己一个人出来转呢， 就回去了，回去了.....');
INSERT INTO `tpshop_joke` VALUES ('2796', '初一那年，班里的同学都很调皮，尤其是班长。有一次，我们约好上课时在老师来时一齐喊：老佛爷吉祥。就在上课时喊了。老师愣住了，就在这会，有两个同学因为上厕所而迟到了，站在教室门口愣了两秒钟后。挽起袖子，单膝跪地，喊了一声：臣等救驾来迟，还望老佛爷恕罪。结果，自从这件事后，我们班就变成了好班级！！！');
INSERT INTO `tpshop_joke` VALUES ('2797', '今天班里来了位外国女老师。上课期间小明一直看着老师下半身。 老师：“你知道你这样很不礼貌吗？” 小明：“对不起，电影看多了，我以为下面会有字幕……”');
INSERT INTO `tpshop_joke` VALUES ('2798', '在茶楼看到的相亲，男的很帅 女：我叫小丽 男：我叫冯和 女：啊？ 男：今天真是个风和日丽的好日子啊！ 女：恩 然后他们楼着就走了。 我笑了，外面明明在下雨！');
INSERT INTO `tpshop_joke` VALUES ('2799', '应聘时，面试官问我从事过什么工作，我说我一直在做网络传媒，针对新闻热点等信息进行推送！被录取之后，其他面试者一脸敬畏地问我到底做过什么，我悄悄告诉他们：转发微博。');
INSERT INTO `tpshop_joke` VALUES ('2800', '农村的，小时候地道战看多了，在家门外的空地上挖坑，梦想啥时候能挖个地下空间，在里面各种嗨皮！辛苦了半个多月啊！终于挖了一米深，结果…结果农村高压线路改善，那天放学回家，我就看到一根大电线杆子竖在我挖了半个月的坑里面！当时我那幼小的心灵……真坑了');
INSERT INTO `tpshop_joke` VALUES ('2801', '上小学时，老师组织给灾区捐款，此为背景。跟娘亲要到1块钱，走到学校门囗碰到有人推着车子卖冰棍，楼主没忍住。。尼玛，上课后老师拿着记录表给读出来了，别人都是一块两块的捐，到我了，九毛！想想现在还脸红');
INSERT INTO `tpshop_joke` VALUES ('2802', '　　一女同事平时说话就爱嗲声嗲气，还叠音，比如吃饭饭穿衣衣照镜镜……割……。这几天她鼻子上长了几颗豆豆，昨天休息去医院看了，到五官科人医生问她哪里不舒服啊？这家伙可好又来了：俺鼻鼻疼……好么这医生刚开始还没反应过来，接着就来了一句：美女，你应该去看妇科。。。。');
INSERT INTO `tpshop_joke` VALUES ('2803', '跟老公吵架，我们一人一个卧室，我越想越生气，于是跑到大门口开了一下门又重重关上了，然后赶紧藏到柜子里，不一会老公出来了，一看我不在卧室抓起外套就跑出去了！哈哈');
INSERT INTO `tpshop_joke` VALUES ('2804', '朋友应聘上了顺丰快递的工作，我们出去庆祝。在路上，顺丰快递的人力资源部给她打来电话，要她填好资料并给他们寄过去。朋友说：“没问题，我一会就圆通快递寄过去”');
INSERT INTO `tpshop_joke` VALUES ('2805', '小时候看电视，特别羡慕那种点穴法～有一个比我大几岁的邻居说他会～楼主倾尽所有“家当”，拜师学艺～在他的指点，楼主日夜操练，一周后我去找他～他说现在可以在他身上试试啦，楼主随便一点，他果然站立不动，还嘴里央求楼主给他解开～ 楼主狠狠的给他屁股一脚，嘴里说着：把劳资给你的钱还回来，否则你就等死吧！');
INSERT INTO `tpshop_joke` VALUES ('2806', '今天看到，一个哲学问题，三观的形成？问题是:我是谁？我从哪里来？将要去哪里？脑袋里第一时间蹦出的答案是:贫僧唐三藏，从东土大唐而来，去往西天拜佛求经……');
INSERT INTO `tpshop_joke` VALUES ('2807', '　　一直蹭隔壁网的两家人最近一个月老是改用户名聊天，昨晚看到一个改成“老公已出差” 　　看到后 默默给她老公发了消息，半夜听到隔壁一阵阵的惨叫。。。。');
INSERT INTO `tpshop_joke` VALUES ('2808', '刚刚给儿子整理书包，发现试卷一枚语文3分，唉！我点了颗烟想了老大会，这是上的什么学，关键这3分我没看明白怎么给的。。。。人才啊！');
INSERT INTO `tpshop_joke` VALUES ('2809', '原创【带刺的玫瑰】 “听说你和女朋友分手了?” “是的。” “为什么呢?” “我把他放到一张铺满玫瑰的床上……” “那不是挺浪漫的吗?!” “可我忘把刺去掉了!”');
INSERT INTO `tpshop_joke` VALUES ('2810', '晚上打完球回来一身臭汗，回到宿舍，脱个干净，洗头膏挤在手里就往浴室去，一2B舍友从厕所出来瞅着我手里，蹦出一句:吆，这么晚了，您还带着孩子遛弯呢！');
INSERT INTO `tpshop_joke` VALUES ('2811', '刚才惹到宝宝了，人家嚷着不要我这个妈妈了，要换个妈妈。她爸爸听见了，贱贱地跑出来对宝宝说：“你说的是真的，我也觉得应该换个老婆。”宝儿嘟着脸走过去，拉着她爸爸低下身子，对着脸就是一巴掌，嘴里还喊着：“你敢换老婆试试看！”笑死我了');
INSERT INTO `tpshop_joke` VALUES ('2812', '我找你帮个忙你会答应吗？ 什么忙？ 只要你答应，我给你500块钱 好啊，什么忙？ 借我一千块钱让我应应急');
INSERT INTO `tpshop_joke` VALUES ('2813', '前几天出差，今儿个一回去，我就和老婆合力抓到一个小偷，SB一样的躲床底下。居然还把衣服全部脱掉，挡着外面。真TM逗。');
INSERT INTO `tpshop_joke` VALUES ('2814', '有一天，跟二货哥们逛街，前面有个妞，腿又长又直！胸也很大，身材超级棒！就商量着谁去要电话，然后二货输了，就屁颠屁颠的跑过去，他就他看了一眼就跑回来了，说了一句特别经典的话，”从后面看能让你精尽，从前面看能让你人亡！”');
INSERT INTO `tpshop_joke` VALUES ('2815', '‍‍‍‍一老光棍去寺庙请教禅师：“请高僧指点我怎么才能找到老婆呢？” 禅师仰天长叹：“唉，我要知道怎么找老婆还出来当什么和尚？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2816', '我觉得现在的人都好自私啊。 计划生育的影响，都是独生子女不懂得分享。 那你是独生子女吗？ 不是的，我还有个弟弟。 你弟弟多大了？？ 18厘米！！！');
INSERT INTO `tpshop_joke` VALUES ('2817', '‍‍‍‍一男子老婆极为彪悍，男子经常成为她的出气筒，苦不堪言。 这天老婆又拿男子发泄，百般欺凌，男子实在忍受不住了，跑进厨房抓菜刀冲了出来。 老婆见状双手叉腰，喝问：“你想干啥？砍我？” 男子嗫嚅道：“你、你再打我可、可就砍你老公啦。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2818', '父亲把“读万卷书，行万里路。”这句话贴在书房的墙上，激励三个儿子。大儿子受前半句影响后来成了文学家，二儿子受后半句影响后来成了四海为家的流浪汉，三儿子受整句话影响后来成了一名导游。');
INSERT INTO `tpshop_joke` VALUES ('2819', '去年回家坐车，和一个挺逗的阿姨坐一起，到了半夜了，我手机没电了，我就问阿姨几点了，她拿出手机点亮屏幕，照了一下手上的手表！！我去。');
INSERT INTO `tpshop_joke` VALUES ('2820', '‍‍‍‍小时候我希望老师表扬我。 学习不行得不到表扬，咱可以把自己的2元钱上缴，说是捡的，让老师表扬了一通。 我饿了一顿，回家妈妈让我用剩下的钱去买醋。 我说：“钱丢了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2821', '一屌丝没有女朋友，于是求助于大湿，“大湿我到现在还没女朋友，我该怎么办？” 大湿叹了口气，从背后拿出来一个充气娃娃。屌丝见状哀伤的说“大湿你的意思是说我这辈子只能跟充气娃娃过了是吗？” “尼玛，老子意思是我女朋友都是充气的，你还问我怎么办。”');
INSERT INTO `tpshop_joke` VALUES ('2822', '今天跟女朋友去吃饭，看见对面那桌女的吃剩下的她男朋友都吃了，然后我女朋友就说你看人家多浪漫，还说我不懂浪漫，当时我就不乐意了，你特么吃饭剩下过么？不抢我的吃就不错了。');
INSERT INTO `tpshop_joke` VALUES ('2823', '跟一二货哥们去献血，到一半时他对医生说：我的尽量输给男的，最好别输给女的！ 医生疑问道：为什么？输男输女不都是输吗？哥们一脸不情愿的说：输男的才长久，输女的尼玛大姨妈一来，输再多都是浪费。');
INSERT INTO `tpshop_joke` VALUES ('2824', '饭店里，一单身男子说：真是羡慕啊，我啥时候也能吃上情侣套餐啊！ 我安慰道：要是有钱，就能像我，一个人也买情侣套餐。');
INSERT INTO `tpshop_joke` VALUES ('2825', '昨天做公交车，前面一帅哥掉了十块钱，被紧跟其后的一个妹子捡到，然后他俩居然聊上了。\r\n　　我寻思：“妈呀，新技能啊。”\r\n　　然后我也学那帅哥，我掉二十，尼玛，钱丢了，丢了…');
INSERT INTO `tpshop_joke` VALUES ('2826', '快递公司打电话给我叫我拿快递，我兴奋了一阵，因为我收件人的姓名是随意复制的三个生僻字，就是要看快递小哥念不出我名字时窘迫的样子。\r\n　　不过当我看到一个老谋深算的大叔坐在卡车里，怀里死死抱着快件一脸轻蔑的看着我，问我叫什么名字，喊对了给货时，我感到了这个世界的恶意，我还是太年轻了。');
INSERT INTO `tpshop_joke` VALUES ('2827', '一哥们在电线杆上看见一重金求子广告，非常兴奋，整整80万啊，而且那女的长的跟天仙似的。激动之余慌忙掏出手机拨打电话，打通后对方是一女的哇哇搁那儿哭，说家庭不幸福，很有钱但一直没有孩子，而且很有可能是老公的原因，现在就想要一儿子，只要有了儿子立马给他打80万等。哥们听到这立马对着电话大喊了一声：妈！');
INSERT INTO `tpshop_joke` VALUES ('2828', '有一女性朋友，体重140斤，有一天出门时找不到手机就用老爸的电话拨打自个儿的电话，这时老爸的电话华丽丽的显示三个大字～大肥妞。');
INSERT INTO `tpshop_joke` VALUES ('2829', '晚上躺床上问媳妇，男人到底是应该以事业为重还是家庭为重，媳妇沉默，然后我就想起了那句经典台词，我搬着砖就不能抱你，抱着你就没有手去搬砖了，然后媳妇就来了句“你可以背着我去搬砖”～');
INSERT INTO `tpshop_joke` VALUES ('2830', '刚刚在路口等车，一个电瓶车要搭我，我说你这个车不安全，他向我充分的展示了他刹车的高性能，然后各种忽悠，我就是不上，他无奈的走了，然后我打了个出租，一分钟后，我坐的出租把他撞了。果然不安全！');
INSERT INTO `tpshop_joke` VALUES ('2831', '核桃说：“我像大脑！” 西兰花说：“我像小树！” 蘑菇说：“我像雨伞！” 香蕉、黄瓜、茄子、甘蔗、苦瓜、萝卜说：“你们三滚出去！”');
INSERT INTO `tpshop_joke` VALUES ('2832', '一男的找女的搭讪。 “你有男朋友吗？” “没有。” “为什么不找个呢？看我合适不？” “不合适。” “你都没试你怎么知道不合适呢？” “我老公会打死我的。”');
INSERT INTO `tpshop_joke` VALUES ('2833', '　　在公司呆了两年，一直没发展，终于鼓起勇气给老板写辞职信。 　　写交接单时老板各种劝，我就是不肯留了。 　　最后老板说我的离职原因写“个人原因”不合适，一定要问清楚真正的原因。 　　我想了想说，工资太低了。 　　这时老板把人事经理叫过来：“给他批了。” 　　我……');
INSERT INTO `tpshop_joke` VALUES ('2834', '农村人教导孩子的三句话，：1. 孩子，爸妈没本事，你要靠自己；2. 孩子，做事先做人， 一定不能做伤害别人的事；3. 孩子，撒开手闯吧，实在不行， 回家种地还有饭吃。 城里人叮嘱孩子三句话：1. 宝贝，好好学习就行， 其他的事由爸妈来办；2. 记住不能吃亏， 即使打架把别人打坏了， 爸妈花钱给他治，也不能受欺负；3. 我告诉你多少遍了， 再不好好学习， 考不上大学，长大没饭吃。');
INSERT INTO `tpshop_joke` VALUES ('2835', '早些年，我们兄弟俩人约一女开房打牌，玩跑得快，最后一名脱一件衣服，如果没衣服了就做二十俯卧撑。从十一点打到凌晨三点，我兄弟俩衣服都脱光了，女的只脱了件外套，最后困的不行了，那妹子说了句：睡觉吧，不打了，我脱给你们看还不行吗？');
INSERT INTO `tpshop_joke` VALUES ('2836', '一男的喜欢到处沾花惹草。 一天碰到一美女，就搭讪：“美女，你好面熟啊，你做什么工作的？” 美女：“我做的服务行业。” 男：“噢，我记得，你是XX饭店的经理，我在你工作的地方见过你的。” 女：“不是，我是在殡仪馆给死人化妆的，你确定见过我？”');
INSERT INTO `tpshop_joke` VALUES ('2837', '“妈妈妈妈，我和你越来越像了！” “哪里像？” “你走路时胸在动，我走路胸也动了！” “儿子，你该减肥了！”');
INSERT INTO `tpshop_joke` VALUES ('2838', '昨天老婆跟她闺蜜饭后聊天，我在看电视。 闺蜜：你儿子好好玩啊。。。 老婆：那你带回家玩几天。 闺蜜：算了，我还是自己生一个吧。 老婆：那把我老公带回家玩几天吧。。。。 老婆真的是爱我啊！');
INSERT INTO `tpshop_joke` VALUES ('2839', '老婆：“亲爱滴，香奈儿最近出了一款包包儿。” 老公：“亲爱滴，咱没钱！” 老婆：“亲爱滴，珠宝店最近新出了一款项链。” 老公：“亲爱滴，咱没钱！” 老婆：“亲爱滴，情趣用品店最近新出了一款娃娃。” 老公：“亲爱滴，咱……有钱……” 老婆：“俺还没说完，男式滴！” 老公：“俺也没说完，才怪呢！”');
INSERT INTO `tpshop_joke` VALUES ('2840', '结婚一年总结的经验，当老婆问你，买这件还是买那件的时候。你正确的做法是去付款。因为这根本不是选择题。更不是疑问句。而是祈使句。');
INSERT INTO `tpshop_joke` VALUES ('2841', '4 某贫困村发补助款，一老汉问：这是什么 钱？村长说：是一次xing生活补助。老汉惊喜， 领款而去。次日，老汉弯腰扶墙来找村长，今 天我要三次姓生活补助。');
INSERT INTO `tpshop_joke` VALUES ('2842', '‍‍老婆：“亲爱滴，你昨晚梦游了。” 老公：“俺梦游干啥了啊？” 老婆：“你爬起来照酷狗滴狗脸上来了一耳光。你梦见啥了？” 老公：“呃，俺梦见俺变成一条狗，还做了酷狗滴老公，俺终于大展雄风，想甩老婆耳光就甩……” 老婆：“……”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2843', '　　一天和老板聊天老板说：“你大学学什么专业的？” 　　我：“心理学” 　　老板盯着我说：“那你猜一下我晚上回去什么地方吃饭？”');
INSERT INTO `tpshop_joke` VALUES ('2844', '！昨天跟一哥们说，最近过得很苦逼！想离婚了！他顿时兴趣勃勃的说！同意！灰长同意！我说现在回去就跟老婆说！可一路上怎么也没想出怎么跟老婆讲！到家老婆在做饭！想想还是算了！不说好了！可到了九点十几分！老婆手机上有一条信息！内容如下！今天我听你老公说！你离婚了！亲爱滴…我们终于可以在一起了！老婆手机在我手里！看到这……段友们！我该怎么办？');
INSERT INTO `tpshop_joke` VALUES ('2845', '我有一个同事，前几天报考驾照去体检，我俩一起进去的，我为一个，完事后，到我同事红绿图，看了半天，医生问这是什么？我就小声的告诉他是牛，告诉了他好几遍。一声说，别提醒他。结果我同事说，不用你提醒我，我已经看的很清楚了，然后斩钉截铁的告诉医生:这是兔子。我都憋的不行了！医生来了一句:你这同事你也不拦着点？我～～～～～');
INSERT INTO `tpshop_joke` VALUES ('2846', '‍‍嘚瑟男：“为什么别人老是误会我是土豪？” 奇葩女：“大哥，你那么丑，还带个漂亮女朋友，别人能不误会不？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2847', '据说，满足其中四条的就算是土豪了: 1.喝酸奶没有舔过盖儿； 2.吃薯片不舔手指； 3.吃泡面不喝汤； 4.喝星巴克不自拍； 5.吃益达敢三粒一起嚼 6.去超市要购物袋； 7.上厕所纸不折； 8.洗发水用没了也不兑水晃！');
INSERT INTO `tpshop_joke` VALUES ('2848', '小明：李大爷听说你新娶了个漂亮的媳妇。 李大爷：对啊，哈哈，年轻就是好 小明：你这一把年纪、吃的消吗？ 李大爷：这不简单，跟玩麻将似的，少吃，多摸，拼命碰，不放炮！');
INSERT INTO `tpshop_joke` VALUES ('2849', '老师：“小明，你喜欢去游泳不？” 小明：“不喜欢！” 老师：“为什么？” 小明：“那些游泳的阿姨们穿的比基尼太浪费布料了，让俺看得生气！”');
INSERT INTO `tpshop_joke` VALUES ('2850', 'NO.9.数学老师骂人：我放你妈七十二个平方的拐弯螺丝屁 NO.10.换座位时老师：大家听好了，换座位——男生站左边，女生站右边，剩下的不要动…………于是最后就只有他没动……');
INSERT INTO `tpshop_joke` VALUES ('2851', '有一天，我戴了个戒指去上学。此为背景。上课的时候我睡着了，梦到前男友在抢我的戒指，我啪的一个巴掌扇过去。突然就醒了，觉得教室里气氛有点不太对，一抬头，看见老师一脸惊悚的看着我，手里拿的，是刚从我手上撸下来的戒指。以后我就出名了。');
INSERT INTO `tpshop_joke` VALUES ('2852', '‍‍‍‍接7岁表妹放学，看见一拉猪的车。 突然想起表妹是属猪的，就指着那车猪开玩笑说：“看，那是你哥！” 说完我就感觉不对了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2853', '我今天问女神为什么不接受我，他唱起了张杰的《这就是矮》');
INSERT INTO `tpshop_joke` VALUES ('2854', '‍‍‍‍本人交警一枚，刚刚接个报警电话。 当事人称：“在某某收费站撞了头猪，猪没事，我脚擦伤了。” 我是不是该告诉他：“这种事，你和猪商量商量自行解决就行？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2855', '‍‍‍‍昨晚和朋友喝醉了，到朋友家小解，朋友的小儿子也跟过来了。 他看着我下面：“有毛毛，有毛毛。” 我笑着问他：“你有毛毛没？” 他得瑟的大声说：“我妈妈下面也有毛毛。” 当时我纳闷了你是怎么知道的？‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2856', '天生妙物腿间居，雅称玉户俗称逼。茸茸美髯红唇隐，幽幽秘洞甘露滴。无牙偏爱吃硬肉，嘴小却喜吞大鸡。最是令人销魂处，亦收亦缩亦吮吸。');
INSERT INTO `tpshop_joke` VALUES ('2857', '一哥们女朋友出.轨跟他分手了，三个月后女的挺.着肚子回来了，哭诉那男的是玩.弄她，搞.大了肚子就不见了。哥们不忍心，说：回来了就留下吧，忘记过去我依然还爱你。女的很感动，打算去打.胎，哥们感伤地说：不是孩子的错，既然都这样就生下来吧。然后…………然后六个月后我哥们不见了！');
INSERT INTO `tpshop_joke` VALUES ('2858', '这三天全市质检考试 语文的阅读题真的好难 是龙应台的《火警》 听教室后排的同学说连百度都找不到 阅读答案 我想我的答案一定很多错吧 可真是苦了全市的莘莘学子 再说作文把 半命题作文 只要————还在 我觉得这篇作文还好写 可是我太高估了自己的智商 考完才反应过来我吧记叙文写成了散文 只要飞翔的翅膀还在 可是题材只有议论文和记叙文 真是人傻意外多 说说政治吧 真的是的 都怪我平时不关注时事 连推进依法治国的总任务都不知道 我真的是该死 化学这次是真的败了 满分100我才得了29分 这三天全市质检考试 语文的阅读题真的好难 是龙应台的《火警》 听教室后排的同学说连百度都找不到 阅读答案 我想我的答案一定很多错吧 可真是苦了全市的莘莘学子 再说作文把 半命题作文 只要————还在 我觉得这篇作文还好写 可是我太高估了自己的智商 考完才反应过来我吧记叙文写成了散文 只要飞翔的翅膀还在 可是题材只有议论文和记叙文 真是人傻意外多 说说政治吧 真的是的 都怪我平时不关注时事 连推进依法治国的总任务都不知道 我真的是该死 化学这次是真的败了 满分100我才得了29分 我对不起国家 对不起党 对不起人民 对不起家长 化学这么差 物理我想也好不到拿去 希望自己能在明天的数学英语发挥好 争取两科130+ 靠自己了 加油！我是要当国家栋梁的人才 不要放弃 我可以的！ 化学这么差 物理我想也好不到拿去 希望自己能在明天的数学英语发挥好 争取两科130+ 靠自己了 在捧腹为自己打打气 ！加油！我是要当国家栋梁的人才 不要放弃 我可以的！');
INSERT INTO `tpshop_joke` VALUES ('2859', '‍‍‍‍大学室友排行第八。 有次我问：“8，你使劲往墙上靠干嘛？” 他：“哦，我只是想装个B”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2860', '宋美玲侧卧，打4个历史人物名字！！让我过吧，答案会让冷友们非常嗨皮的………');
INSERT INTO `tpshop_joke` VALUES ('2861', '老公又开始说我胖了，说个没停，我一生气就打了他一巴掌。老公：“太过分了吧！”我：“我过分！还不是你先说我胖，我才打你的。”老公：“明明是你先胖的！”');
INSERT INTO `tpshop_joke` VALUES ('2862', '夫妻准备亲热，行动前老婆说要跟老公打赌，谁输就包一年的家务，老公同意了。老婆：我们打赌你能坚持多长时间。老公暗自在心里估计了一下时间，然后说：我能坚持一柱香的时间。老婆许可，于是她开始点燃一盘蚊香。老公。。。');
INSERT INTO `tpshop_joke` VALUES ('2863', '小明考试从来不写试卷，因为知道写了也不会及格，不如不写。这样老师也不用改我的试卷，直接打0分。试卷发下来再抄别人90多分的，自己再在0前面加一个9，回家理直气壮给老妈看！');
INSERT INTO `tpshop_joke` VALUES ('2864', '高中有次过生日，家里给煮了鸡蛋让带着，我拿着顺手就揣在裤兜里，结果到了学校上厕所的时候，鸡蛋啪嚓一声，掉粪坑里了。。。只听后面有个货大叫：卧槽！你下蛋了！');
INSERT INTO `tpshop_joke` VALUES ('2865', '有次半夜，拿起手机玩，男神发来信息：做我女朋友好吗？卧槽! 我是那种人？我立马回：你TM滚，我下铺那么喜欢你你竟然这样对她！然后我直接把他号码拉黑，把手机还给了上铺。睡了！');
INSERT INTO `tpshop_joke` VALUES ('2866', '那天我问室友“想吃臭豆腐却钱不够怎么办？”那二货随口说道“买块豆腐去厕所吃”。。');
INSERT INTO `tpshop_joke` VALUES ('2867', '一个乡巴佬去五金店买锯子。他说：给我一个一个小时能锯掉10棵树的锯子。老板给他看了一下一个最好的电锯的模型。乡巴佬看了一眼就看上了，买了下来。过了一天，乡巴佬气愤的找到老板说：这是什么垃圾锯子，我用了一天时间才锯了一棵树，哼!老板拿过电锯，打开开关，看出了什么问题。乡巴佬疑惑的说道：诶，这是什么声音？？？');
INSERT INTO `tpshop_joke` VALUES ('2868', '老公接过我给的两张五块零花钱，高兴的准备下楼溜达，刚穿好鞋走了两步！我就连忙喊道：“回来！”老公疑惑的问道：“怎么了老婆？”我淡定的说道：“别装！拿出来！”老公竖起大手指说道：“老婆，你真厉害，我就走了两步你就听出来了...”说完，老公把鞋脱掉，从鞋里倒出七枚硬币；我一把抢过硬币淡定的说道：“刚才我给你那两张五块钱，你看看里面是不是夹了一张一块的...”');
INSERT INTO `tpshop_joke` VALUES ('2869', '老师：明天就高考了，千军万马过独木桥，大家要努力啊！小明：老师，桥下有水吗？老师：有，而且很深。小明：据《人与自热》里讲，马儿会游泳，掉下去，仍然可以扶着马儿过河啊.老师：滚！');
INSERT INTO `tpshop_joke` VALUES ('2870', '单位组织去旅游，行政小妹来核查身份信息。来我这里问我:“你是属老鼠的啊？” 我说是，但是把“老”字去掉。她愣了一下。几天后，单位全体人员都去旅游了，唯独我一人在加班，我去，还真把老子去掉了。');
INSERT INTO `tpshop_joke` VALUES ('2871', '约了几个死党在我家吃饭，吃完就无聊玩真心话大冒险啊！其中一个朋友输了，选择大冒险，楼主默默的去厨房拿了个洋葱出来交给她“来！咬一口……”看着这货辣的鼻涕眼泪直流，我们笑的前仰后翻。然后游戏继续，楼主输了，也选择大冒险，那货拿起她咬了一口的洋葱说“来！给老娘吃完……”');
INSERT INTO `tpshop_joke` VALUES ('2872', '‍‍一女孩去打针，问护士：“打哪里？” 护士说：“屁股啊！好的快！” 女孩：“哦，那你扎准点可以不？” 护士：“都扎了七八年啦！” 女孩又问：“会不会疼啊？” 护士：“不会！” 女孩沉默了。 护士拿着针一下扎了下去，女孩“嗷”的一声…… 欲哭的女孩说：“你不是说不疼吗？” 护士：“都扎了七八年啦！没一次不疼的！” 女孩：“……”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2873', '‍‍‍‍男：“我不是跟我的头像很像？” 女：“像个屁。” 男：“你才像个屁。” 女：“那你把我放了吧！” 男思考了下，答到：“不要，我要憋着。” 女：“不要憋着憋着憋成个嗝，从嘴里放出去了。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2874', '记得读初中的时候，和班上一妹子是同桌，有天上课，老师喊妹子起来回答问题，当时是夏天，妹子穿的裙子，当妹子站起来时我发现裙子夹在屁股里了，我怕后面同学看到不太好，悄悄的扯出来了，妹子用眼角看了我一下，狠狠的踢了我。我以为她不高兴，马上又把裙子给她顶进去。后来，后来就叫家长了。。。。。');
INSERT INTO `tpshop_joke` VALUES ('2875', '一日到做晚饭时间，老婆让我做饭，我说给你做个红烧鸡吧，老婆当时来了一句，把你的g掉，红烧也行，我听后瞬间无语');
INSERT INTO `tpshop_joke` VALUES ('2876', '‍‍‍‍老湿：“孩子的教育要从头开始。” 家长：“以前我就是从头开始，但一棍子就把他敲晕了，还是从屁股下手好点。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2877', '女：老公，假如有个很性感漂亮的女人勾引你，你是跑开还是去呢？ 男：我当然是跑……着去啦！ 女：吃我一脚！');
INSERT INTO `tpshop_joke` VALUES ('2878', '‍‍女：“你谈过女朋友吗？” 男：“谈过一个，她居然背着我跟别的男人约会。” 女：“她力气真大！” 男：“……”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2879', '‍‍空中小姐将一杯酒递给牧师，问：“这里距离地面多远？” 空中小姐说：“二万英尺。” 将酒还给空中小姐说：“我还是不喝了，这里距离我们总部太近了。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2880', '　　妹妹买了副隐形眼镜，怎么都带不上，老公给她掰眼皮，我给她戴， 　　关键是一个大男人都掰不住！眼镜片刚到眼前她就破死命地把眼睛闭上了！ 　　场面脑补，我妈还以为我俩杀人灭口呢！');
INSERT INTO `tpshop_joke` VALUES ('2881', '“这辈子不结婚了会带来什么困扰么？” “没人养老…” “各种闲言碎语…” “你这辈子随出去的份子钱一毛都回不来了”。 “结，明天纠结。”');
INSERT INTO `tpshop_joke` VALUES ('2882', '一大清早，我就赶到表妹家质问道：“媛媛，你姐夫昨天晚上没回来，你昨晚在哪！” 媛媛淡定的说道：“姐，你可别怀疑我，我一晚上都在车里，不信你可以看我的行车记录仪； 我连看带快进花了好几个小时才看完表妹昨夜的行车记录，然后淡定的说道：“媛媛，姐错怪你啦！这几个男人中，确实没有你姐夫...”');
INSERT INTO `tpshop_joke` VALUES ('2883', '昨晚下班回家，快到家了看到一美女，跟老婆挺像的，背影那个漂亮呀，于是没忍住上前拍了一下说：美女约不？ 那姑娘一回头看了我一眼呆了，说：姐夫，我正要去你家呢！！ 瞬间乱了。');
INSERT INTO `tpshop_joke` VALUES ('2884', '和女朋友一起逛街，路遇一肥胖大娘，跟女朋友撞衫了，女朋友各种不服气，还绕着她走来走去，准备气走她…… 转了几圈，我拉拉女朋友的衣服说:“咱们走吧，旁边的人都说你穿着没她好看……”');
INSERT INTO `tpshop_joke` VALUES ('2885', '有一女性朋友是标准的女汉子型，跟着我们吸烟喝酒都会了。 有一次银行卡忘带放老家，找她借钱用，她二话没说拿出几百给我之后说了句“拿去嫖”。 我愣了一下，又把钱还给了她，她也愣了一下。之后……秒懂点赞！');
INSERT INTO `tpshop_joke` VALUES ('2886', '中国最牛逼的事情：一、车“超载”了，车胎没爆，桥却压塌了。 二、出动万人，击毙一逃犯，数百人立功。花18亿建的大桥倒塌后，无一人负责。 三、据说在中国能安全过桥的只有云南米线了！ 四、每个倒塌的桥面上，必然趴着数不清的蛀虫。管你塌不塌，捞完走人。 五、过个车大桥坠毁；打个雷高铁追尾；下个雨满城泡水；开个会全国戒备；生个病债台高垒；读个书全家受累；眨个眼肉价飙飞；上个访有去无回；摆个地摊城管砸毁；炒个股票终生后悔,这是一个具有5000年的神奇国度。');
INSERT INTO `tpshop_joke` VALUES ('2887', '妹妹：姐姐，现在电视上经常报道小孩子阻止父母生二胎的事情，我得感谢姐姐当年不杀之恩。 姐姐：我靠，你的认识仅仅这样吗？为了你的出生，我天天扮成脑残，去医院开痴呆儿证明，还去父母单位表演疯癫的样子给他们领导看，我容易吗？');
INSERT INTO `tpshop_joke` VALUES ('2888', '我跟男友打赌，谁要是先理谁，谁就输了。于是1天……10天……100天……1年过去了。 今天在朋友圈发现他晒孩子满月的照片。');
INSERT INTO `tpshop_joke` VALUES ('2889', '老师：今天作文的题目是“假如我是董事长”，下面大家开始写： 小明坐在座位上一动不动。 “小明，你为什么不写？” “你见过哪个董事长自己写作文？去把我的秘书叫来”。 “滚出去”。');
INSERT INTO `tpshop_joke` VALUES ('2890', '女子看着整天出去喝的醉醺醺的老公，骂道：“要是让我再重返20岁，我特么绝对不选择嫁给你！” 女子老公迷糊的说道：“要是你能重返20岁，我特么保证滴酒不沾...”');
INSERT INTO `tpshop_joke` VALUES ('2891', '本尊历经千辛万苦，克服了困难重重，潜心修道18余载，最终窥的天道，结成了金丹！踏入了梦寐以求的金丹大道！为何你们却要毁我丹田，碎我金丹！”啪！旁边的小护士拍了我一下，“少贫嘴！不就是颗胆结石嘛！至于吗？人家二楼妇产科，有位少女天姿卓绝，才16岁就结成了元婴！还不是照样让我们给拿了！那啥隔壁精神病院有几个分神期呢，地下停尸房还有出窍境的，对面宾馆还有两个合体期的。诺，二楼又新来了个刚度过雷劫的大乘期高手。”');
INSERT INTO `tpshop_joke` VALUES ('2892', '&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;周四晚上十点多，我好说歹说，历经千辛万苦，总于在12点50几分把女票哄睡着了，趁女票熟睡之际，我迅速的打开电脑，选了好多东西，于是默默倒数10,9,8,7,6,5,4,当我快数到一的时候，女票不知道什么时候，手提着菜刀站在了我旁边，幽幽说道：你要敢数到一，我连你第三条腿都一起剁掉。那一刻，我哭了。');
INSERT INTO `tpshop_joke` VALUES ('2893', '和俩朋友一起吃早餐，我们两个坐一边，另一个坐对面。对话如下：今天早上有没有一柱擎天？我说有啊，早上五点多就醒了，难受啊，竖起来老高，被子都被顶起来了！这时对面那哥们儿说，起来了，我是起来了，然后看我们表情不对又加一句，我是五点多起来的...五点多...然后我们对视，无语，集体喷饭');
INSERT INTO `tpshop_joke` VALUES ('2894', '今天去医院体检要查大便,我看小护士挺漂亮,于是就想到逗她一下。跑门口买了一个烤红署,捏成泥放在了大便盒里,护士说太多了,喊处理一下,我说,不用麻烦了,说完用手拿着就吃,小护士连口罩都来不及摘,当场吐了三个。这时旁边的人拍了拍我的肩膀：兄弟你拿错了,这盒才是你的!');
INSERT INTO `tpshop_joke` VALUES ('2895', '本人学舞蹈的，最近在看教师资格证书。今天看书看的累了，自言自语说看段舞蹈视频休息一会，老妈听到来了句，你现在蹦都蹦不动了，肉坠的，肉坠的……');
INSERT INTO `tpshop_joke` VALUES ('2896', '班里新订了班服，后面印了“这个夏天，我们在一起”的字样。　　中午大家一起吃饭，就我寝室的俩哥们穿了班服…　　结果周围的人看着他们俩，各种侧目…~');
INSERT INTO `tpshop_joke` VALUES ('2897', '马上要五一，老板对新招来的售货员小王说：“今天很多柜台都搞促销活动，　　咱们也改一下价格。把那件1000元的西服打四折，会改吗？”　　小王说：“会，不就是把1000改成400嘛。”　　老板摆手说：“不对，学着点儿，　　是把原价1000元改成2500元，明白了吗？”');
INSERT INTO `tpshop_joke` VALUES ('2898', '高考，考数学天气凉爽不小心睡着了　　正做梦呢，监考呼唤我：同学醒醒，还有半小时交卷了，　　把你卷子晾晾，湿的没法装订．．．');
INSERT INTO `tpshop_joke` VALUES ('2899', '有一次室友下课发现有九个未接电话。　　于是他就回过去了，是快递员打的，　　快递员问：你上课都不玩手机的吗？　　二货答：我睡着了……');
INSERT INTO `tpshop_joke` VALUES ('2900', '一个年轻美貌的女子，　　问一个救火员：你为了救我出险，一定费了不少气力吧？ 　　　　救火员：可不是吗？我曾打退了3个救火员，　　他们都抢着来救你呢。');
INSERT INTO `tpshop_joke` VALUES ('2901', '跟前女友好上以后才知道她是公司里的公交车，经常和已婚男同事上床。于是愤而分手，分别成家。以上为背景。糗的是我现在也成为被她勾搭上床的已婚男同事之一');
INSERT INTO `tpshop_joke` VALUES ('2902', '‍‍‍‍一日，小明看见一位大爷倒在路旁，二话不说便上前扶起老人。 老人大喊：“是他撞得我，来人呀。” 小明说：“爷爷别喊了。” 老人：“叫爷爷也没用，赔钱。” 小明：“爷爷是我呀，奶奶把饭做好了，让我喊你回去吃饭。” 老人：“哦，那咱们走吧。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2903', '‍‍‍‍老婆：“老公，我给你买了个礼物。” 老公：“老婆真好，给我买了什么礼物？” 老婆：“看我给你买的搓衣板，之前那个给你跪坏了！我给你买个新的，高兴吗？” 老公哭丧着脸说：“高兴。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2904', '1.记得那年高考后，心知考得不怎么样，就对父亲如实说了考试成绩不理想等等。父亲无奈地说：“不行的话，就复读吧。”这时坐在一旁的爷爷生气地说：“考不上就考不上，服什么毒啊？！”\r\n2.“我用一下你的热点” “行啊 ” “你是不是没设密码？” “是哦 等下我设一下” “别！别设!”\r\n3.女神说她无聊，问我要不要一起吃饭，我心灰意冷的说：“算了吧，我知道你其实并不喜欢我。”女神立刻激动的反驳：“谁说的？”我心里一喜：“这么说你喜欢我？！”“不是，我就想知道是谁把这个秘密告诉你的。”\r\n4.“我有一把折断的剑，一条荆棘的路，一个看不清的未来，一场回不去的梦。”“说人话。”“伞坏了，鞋磨脚，雨大，不想起床。” \r\n5.“湘玉，你就把芙蓉留在店里打杂吧。”湘玉看着郭芙蓉那样，心想肯定是个特能吃的吃货，说道：“展堂，你给我一大嘴巴……”话未说完，展堂立马给了她一大嘴巴。\r\n6.老板不慎跌倒，撞破头，流了很多血，店员们都慌了，见到这一幕，我知道，机会来了，这次得好好表现。于是我扯开嗓子，朝店外大喊道：好消息！好消息！老板大出血！老板大出血！全场两折！全场两折！ \r\n7.“ 招普工，每月工资3000~5000元，要求:能吃苦！… ”我抱着试一试的心态进入了这家公司，已经一个月多了，被骗了！今天还是炒苦瓜！ \r\n8.今天一女生告诉我,生命科学院院长演讲一场,彻底改变了她。院长痛心疾首地说:我就不明白你们女生为什么喜欢敷面膜。不知道胶原蛋白大分子不能被皮肤吸收也就算了。那厚厚一层玩意,不就是在脸上抹了层培养基么?还一敷敷半小时,皮表各种菌各种虫高兴坏了,等你敷完都四世同堂了。\r\n9.一年前甩了我的前男友发短信说挺想我的，希望能见个面。宾馆里，他急着把我摁到床上，我说:死鬼，忙什么？先去洗个澡。他说好。我看他进去了，把他衣服抱着就离开了。\r\n今天的谜题是：你问别人什么问题是，老是回答“没有”？（脑筋急转弯）请关注捧腹微信公众号“捧腹笑话”，然后在该公众号的微信文本框输入“谜语0625”，即可获得参考答案！');
INSERT INTO `tpshop_joke` VALUES ('2905', '‍‍‍‍画家去给精神病院的天花板上绘一幅画。 过了一会一位精神病人走过来，说：“画家先生，你的那只画笔要使劲握住，可千万别掉下来。” 画家答：“我握着呢，没问题。” 病人：“好吧，那我就把梯子搬走了，借我用一下。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2906', '转:侄子不好好吃饭，嫂子怎么哄都不管用。我看见了，就哄侄子吃饭，说不吃饭就不帅了，嫂子很赞同，对我侄子说“你叔叔小时候就不好好吃饭，结果长成这样了……嫂子，我惹你了啊。');
INSERT INTO `tpshop_joke` VALUES ('2907', '‍‍‍‍今天去摆摊卖东西，饰品什么的。 有个胖妹子买了个盘发器，但是她说不太会弄。 我说：“你坐下我帮你盘一下吧，学学就会啦！” 然后，她就坐下了，再然后，就坐在地上了。 好吧，我就不让你赔我的板凳了，看你摔的那么惨，我也挺不好意思的。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2908', '有一家奇葩的公司，五点半下班，六点半才有公司班车，大家为了体面的坐大巴回家，都主动加班一小时；八点钟是丰盛的工作餐，想想回家还得自己做饭，再加一小时吧；十点钟后打车报销，累了一天谁还挤公交？再来俩小时！');
INSERT INTO `tpshop_joke` VALUES ('2909', '妻子挖苦：野猪可以活50年，家猪只能活5年；野狗能活20年，家犬只有8年寿命。生命在于运动嘛，谁叫你一天到晚像乌龟一样缩着不动。 丈夫反问：那么，亲爱的请问，乌龟可以活多少年？');
INSERT INTO `tpshop_joke` VALUES ('2910', '‍‍‍‍朋友30岁了没对象，别人拿了五张照片让他看看。 相中哪个给他介绍一下，他死死的盯着照片不说话。 人家问他：“倒底看上哪个说话呀？” 那二货说：“地上有五张百元大钞你捡哪个？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2911', '走到胡同里听见楼上有人喊我:“陛下……陛下!”\r\n我抬头应了一声:“干吗!”然后就被泼了一脸水。\r\n楼上泼水的女的说:“早喊你避一下嘛,活该!”');
INSERT INTO `tpshop_joke` VALUES ('2912', '今天去去超市买桶,回来做公交车没座位,于是把桶放在一个妹子的座位底下,以此为背景。10分钟后我到站了,要拿桶,给妹子说:把腿分开点,我要桶。尼玛,我的脸,现在还在痛。');
INSERT INTO `tpshop_joke` VALUES ('2913', '前天心情不好,手机设置了拦截任何人电话,被拦截的全部显示停机。其中有一个朋友找我有事,打电话给我,被拦截了。然后他给我交一百话费,再打,还是停机,又交一百,还是停机。后来给我发微信,第一句话就是你怎么欠费这么多?你是让我情何以堪啊,我要不要告诉他真相。');
INSERT INTO `tpshop_joke` VALUES ('2914', '穿着人字拖挤地铁,刚上去就被别人的高跟鞋踩到脚。踩我的女人连声给我道歉说对不起自己不是故意的要不你再踩回来吧!啧啧,真是搞笑,我一个大老爷们会在众目睽睽下穿着人字拖去踩你的脚?于是我从包里拿出一双高跟鞋。&#160;');
INSERT INTO `tpshop_joke` VALUES ('2915', '一哥们是个二货,骑电瓶车逆行,被警察逮住,要罚款50元。\r\n他立马就火了,大声呵斥警察:“不就是逆行吗,又不是汽车,还要罚我50,我要是请出我大爷来,你得倒找我50。”\r\n警察也怒了:“你把你大爷叫来,我看他有多牛,我就不信还让我给你50。”\r\n这货立马从兜里掏出一百元,指着毛爷爷头像说:“这就是我大爷,找我50￥!”');
INSERT INTO `tpshop_joke` VALUES ('2916', '一名男子走进酒吧,对他的朋友说:“有个女人现在就在我车里,我实在太累了,不然你去陪她玩一玩?我车里的灯坏了,她不会发现是你。”他朋友觉得这个主意很好,于是进了车开始翻云覆雨。几分钟后一个警察走过来,敲了敲车窗并用手电筒往车里照。警察:“你们在干嘛?”。男人:“没什么,这是我老婆。”警察:“不好意思,我不知道。”男人:“没关系。要不是你用电筒照,我也不知道。”');
INSERT INTO `tpshop_joke` VALUES ('2917', '老王出差回家,儿子小明说:“爸爸,我们家闹鬼!”老王大吃一惊,连忙问怎么了?小明说:每天晚上我都有听到妈妈叫喊着死鬼不要不要。吓得我好几天都不敢睡觉了!');
INSERT INTO `tpshop_joke` VALUES ('2918', '昨天吃晚饭,老婆吃完还想添饭,可又怕增肥,在纠结中。。。我一句话让她果断打开饭煲添饭:都已经是大象了,还在乎是瘦大象还是胖大象吗?');
INSERT INTO `tpshop_joke` VALUES ('2919', '女儿:“爸爸爸爸,狗熊长什么样儿啊?”\r\n我:“老婆,把你的皮草穿上,女儿又忘了。”\r\n老婆。。。');
INSERT INTO `tpshop_joke` VALUES ('2920', '我:老婆我在商场里给你买了个5000多的名牌包包。\r\n老婆高兴说:你又乱花钱!买那么贵的糟蹋钱。\r\n我:就知道你会这么说,我已经退过了。');
INSERT INTO `tpshop_joke` VALUES ('2921', '记得上高中那会和一个朋友去医务室，真事不割——看见进来一位打扮很酷的妹子，她走到医生跟前说，我手最近老痒，手指缝里还掉皮了。只见医生拿着她的小手掰开仔细看了看，头也没抬说，没啥大事，脚气。那女孩转身小跑走了，怪我没忍住眼泪都快笑出来了……');
INSERT INTO `tpshop_joke` VALUES ('2922', '‍‍‍‍结婚十年，我和老婆简单庆祝了一下。 晚上老婆搂着我含情脉脉地说：“老公，我觉得这十年来咱俩的心越贴越近了。” 我无奈地说：“是啊！你的胸越来越小，心不就近了么。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2923', '‍‍‍‍甲：“处男之身可以卖钱吗？” 乙：“你很缺钱吗？” 甲：“恩，我老婆怀孕了。” 乙：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2924', '　　一朋友从黑道借走80万，一直拖着不还，被砍掉一只耳朵，后来他花了5万做了个假耳朵。现在问题来了：他这是不是变相的卖器官？');
INSERT INTO `tpshop_joke` VALUES ('2925', '就刚才走路上突然觉得菊花处有点痒，当时路上人多不敢有动作。好不容易忍到肯德基门口见四处无人于是背朝肯德基用自己身体做掩护，手伸进裤子狠狠抓了两下。回头一看，橱窗里一妹纸手里抓着薯条蘸着酱正在犹豫要不要往嘴里送。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('2926', '‍‍‍‍‍‍晚上睡觉蚊帐没关好，进来几只蚊子，早上起来一看都吸一肚子血。 老婆让我拍死。 我说：“像我这么善良的人怎么能这么残忍呢？” 说完我蚊子都抓住，除掉它们的口器，翅膀和腿，然后就把它们放生了，我真的好善良！‍‍‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2927', '“我老公真是牛啊！”“知道了，知道了。” 大家不耐烦的对铁扇公主说道。');
INSERT INTO `tpshop_joke` VALUES ('2928', '茄子土豆青椒拦住了师徒四人的去路。悟空大喝一声：你们是什么妖怪？话音刚落，对面三人便责怪道：大胆，我们是地！三！仙！');
INSERT INTO `tpshop_joke` VALUES ('2929', '小奥特曼问道：“妈妈妈妈，为什么你每次做菜都要加那么多的盐？”奥特曼妈妈慈祥地摸了摸他的头：“因为我们是‘嫌淡超人’啊。”');
INSERT INTO `tpshop_joke` VALUES ('2930', '麻雀去足疗，问鹦鹉小姐：洗脚论次算？论只算？鹦鹉小姐说：废话，当然论次算了！麻雀冲门外叫：蜈蚣老弟进来吧，人家论次算的。');
INSERT INTO `tpshop_joke` VALUES ('2931', '一 二十年前一帮傻逼孩子吃干脆面集卡片总是集不齐，二十年后一群成年人支付宝集福卡也集不齐还乐此不疲。因为这帮傻逼孩子长大了.. .. ....~\\(≧▽≦)/~~\\(≧▽≦)/~\r\n今晚最搞笑的一句话：全国人民都上听了，单吊敬业福..\r\nTMD，马云暗杠了！\r\n上联：摇一摇摇到手酸只有一块五\r\n下联：咻一咻咻到没电还差敬业福\r\n横批：二马耍猴');
INSERT INTO `tpshop_joke` VALUES ('2932', '前几天，我家对面杂货铺的老板在店门口贴出一张纸，上书：“本店已易主。”可是，今天都还看见他在店里卖东西。我就奇怪地问：“老板，你不是已经把店卖给别人了吗？”店主说：“没有，是我结婚了。”');
INSERT INTO `tpshop_joke` VALUES ('2933', '小时候，红包是这样给的……别人给我个红包，我妈给别人小孩一个红包，回去后我妈又把别人给我的收走，她一点都没亏，我估计我妈给别人小孩的红包也被收了，我们就是过过手。');
INSERT INTO `tpshop_joke` VALUES ('2934', '最讨厌那些出租自己的人了，什么998马上带回家，白天可以萌萌哒，晚上可以啪啪啪！人穷也不能这样啊！在穷也不能穷志气！像我，对于那些租男友的妹子，我只想说\"我免费，真的免费。必要的时候还可以倒贴啊');
INSERT INTO `tpshop_joke` VALUES ('2935', '今天经理怒气冲冲的走进办公室，　　对一实习萌妹子一顿批评最后说了一句“你看怎么办吧？”　　那妹子弱弱的回答“只要不找家长就行。”　　整个办公室瞬间笑抽，　　连平常最严肃的经理都笑趴了那妹子还一脸无辜的看着人们！');
INSERT INTO `tpshop_joke` VALUES ('2936', '妈妈最喜欢吃甜点，那天回家扔给女儿一个快递，　　里面全是欧式小点心，收件人是妈妈。　　一问之下居然是女儿男朋友寄来给妈妈解馋的，　　女儿看到里面一张小贺卡，写着：“山大王，小的来给您送年货了！”');
INSERT INTO `tpshop_joke` VALUES ('2937', '今天同事在电梯看到一美女，　　上下打量了一番问她是来应聘的吗，　　女的白了他一眼，电梯门开了，他对刚要进电梯的老总比划着说这妞正点，　　前凸后翘，非常不错。　　忽然那女的来了一句“爸爸”。　　第二天同事就没来......');
INSERT INTO `tpshop_joke` VALUES ('2938', '有一天，一位其貌不扬的男士，带着一位十分美艳的小姐，来到一家爱马仕店。　　他为小姐选了一款价值565000元的包包。　　付款时，男士掏出支票本，十分潇洒地签了一张支票，店员有些为难。　　男士看穿了店员的心思，十分冷静地对店员说：“我感觉到，　　您担心这是一张空头支票，对吗？今天是周六，银行关门。　　我建议您把支票和包都留下。等到下周一支票兑现之后，再请你们把包送到这位小姐的府上。　　您看这样行不行？”　　店员放下心来，欣然地接受了这个建议，并且大方的承诺，　　送包的费用由该店承担，他本人将亲自把这件事情给办妥。　　星期一，店员拿着支票去银行入账，支票果真是张空头支票！　　愤怒的店员打电话给那位男士，男士对她说：“这没有什么要紧啊！你和我都没有损失。　　上星期六的晚上，我已經搞定那個女孩！哦，多谢您的合作！”');
INSERT INTO `tpshop_joke` VALUES ('2939', '虎子课堂上睡觉，等醒来时候发现全班在数数。　　“45！”“46！”“49！”　　虎子也激动地附和一句：“100！！”　　全场鸦雀无声，老师紧盯着虎子。　　后来才知道，语文老师让大家猜他的年龄。');
INSERT INTO `tpshop_joke` VALUES ('2940', '某男过两天要坐飞机回老家趟，　　今天就跟媳妇说：“要是我死了你就再找个吧，你还年轻，别为我守寡了。”　　媳妇含泪点点头：“你放心吧，我都找好了，就等你死了。”');
INSERT INTO `tpshop_joke` VALUES ('2941', '春天来了，劝君莫食三月鲫，万千鱼仔在腹中。劝君莫打三春鸟，子在巢中待母归。劝君莫食三春蛙，百千生命在腹中。爱向苍天，善行人间；爱是慈悲守护，喜舍难量。鞠躬感恩敬上。');
INSERT INTO `tpshop_joke` VALUES ('2942', '　　晚上跟朋友逛街，看见一个人烧纸， 　　于是我问说：今天不是愚人节吗，你烧纸干嘛。 　　他说：我逗鬼玩 　　我想说：大哥你真有胆量');
INSERT INTO `tpshop_joke` VALUES ('2943', '昨天问男友:“是不是其实你是富二代，一直在考验我啊？” 男友兴奋的说：“我也一直觉得我是富二代，是不是我爸在考验我啊！”');
INSERT INTO `tpshop_joke` VALUES ('2944', '\"有一个大坑，几百米深，你跳下去，有什么办法可以出来？” “我不需要别的，只要一根针，在脑袋上扎个洞，把脑袋里的水放出来，放满一坑我就可以游上来了。” “脑子里怎么会有那么多的水？” “脑子里没那么多的水，为什么要跳进那么深的坑里？');
INSERT INTO `tpshop_joke` VALUES ('2945', '告诉大家一个脸部美容的秘方，番茄蜂蜜美白：将番茄捣成番茄汁加入适量纯净水搅至糊状均匀涂于脸部像洗面奶一样的手法进行轻轻的按摩待约十五分钟洗去，每周1—2次。坚持数月你会惊奇的发现丑是无法用外力改变的。');
INSERT INTO `tpshop_joke` VALUES ('2946', '　　女：老婆，你看韩剧中的男主角发型很帅吧？怎么我就看不见这样的男的呢？ 　　男：胡扯。大街上不都是的吗？只不过当你看到时，你会觉得要么是灰主流，要么就是理发店的发型师！ 　　女：哇~~~老公你说的太对了！');
INSERT INTO `tpshop_joke` VALUES ('2947', '　　一个偏远的农村，村干部召集村里的全体妇女开会宣传计划生育政策。眼看一个多小时了，会还是没完没了。一会儿，一个中年妇女站起来向门口走去。“张大嫂，会还没完呢，你干啥去？”“我家里有孩子，我得回去看看了。”张大嫂走了。会议依然继续着。过了一会儿，又站起来一个年轻的少妇，也向门口走去。“小于啊，你要去哪儿啊？你家还没有孩子呢？”小于头都没回：“我要是老坐在这里开会，我永远也不会有孩子。”');
INSERT INTO `tpshop_joke` VALUES ('2948', '学生翻墙进学校被校长抓住。 校长：为什么不走校门？ 学生指着衣服：美特斯邦威，不走寻常路！ 校长：这么高的墙，你是怎么翻过来的？ 学生拍拍裤子：李宁，一切皆有可能！ 校长：翻墙的滋味怎么样？ 学生指着鞋子：特步，飞一般的感觉！ 次日，学生走正门进来。 校长：今天怎么不翻墙了？ 学生指着鞋子：安踏，我选择我喜欢！ 校长：为什么没穿校服？ 学生提了提裤子：森马，穿什么就什么。 校长：不怕我不让你进学校吗？ 学生拍拍身上的衣服：贵人鸟，无人可挡。 校长大怒：我要记你一大过！ 学生不满：为什么？ 校长冷笑：动感地带，我的地盘我做主！');
INSERT INTO `tpshop_joke` VALUES ('2949', '为了下定决心戒撸，把电脑里的你懂的文件夹改名为“双击遭雷劈”，一个多月了 至今没有双击过，每次都是看着文件名默默地把鼠标移上去-右键-打开....#');
INSERT INTO `tpshop_joke` VALUES ('2950', '　　作为一名宽带维修技术人员，朋友要经常到机房去检查。这个小区的机房在地下室，平时很少有人去的。忙中出错，朋友不小心把自己反锁在机房里了。手机连信号都没有，这才真是叫天天不应，叫地地不语啊。');
INSERT INTO `tpshop_joke` VALUES ('2951', '女友老爹爱玩儿古玩，第一次去她家，老头指着个木头盒子对我炫耀：\"小伙子看见没，这个盒儿两万多。\" \"哟叔叔，这么贵！为什……\" 还没等我问完，老头打开盒子拿出两万块钱给我看。');
INSERT INTO `tpshop_joke` VALUES ('2952', '‍‍就在超市门口，因为钱花多花少的原因，我和老婆大吵一架。 老婆负气开车走了，只剩下我和3岁儿子在风中凌乱。 于是我蹲下身来，问儿子说：“儿子，你妈开车走了，不要你了，你想她吗？我们该怎样办呢？” 小家伙听完我的话，立刻抱我的大腿大哭说：“爸，那我们家的车咋办？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2953', '四个剑客一起比试剑术，第一个剑客把一只苍蝇劈成了两半，第二个劈成了4半，第三个剑客劈成了8半，人们都以为胜负已分时，第四个剑客站了出来，一刀挥出，苍蝇却飞走了，大家哄笑，这是剑客说话了：你们仔细看看，他不能做爸爸了！');
INSERT INTO `tpshop_joke` VALUES ('2954', '‍‍高中是学霸，经常有人问问题。 有个波霸妹子，趴我桌子旁边，跟我探讨姿势。 当时哥就看到了，好大好白。 可惜了年少的我居然害羞的扭头盯着试卷，一直没敢再看。 我同桌后来告诉我，他看的好爽。‍‍');
INSERT INTO `tpshop_joke` VALUES ('2955', '有很多女生想找一个世界上最爱自己的男人。可是我想对她们说“世界上最爱你的男人已经娶了你妈了！还是算了吧。。”');
INSERT INTO `tpshop_joke` VALUES ('2956', '一天坐长途货车，口渴。便拿起车上的矿泉水猛喝了一口。妈的，怎么这么难喝，和尿一个味道。');
INSERT INTO `tpshop_joke` VALUES ('2957', '‍‍我跟老婆搬新家，找了个算命师傅来看风水。 那算命师傅一来就左邻右舍的乱窜。 不一会儿回到屋子里，面色凝重地跟我说：“风水不好。” 不解问之，答曰：“隔壁姓王。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2958', '‍‍老婆：“老公，你年纪不大，怎么额头那么多皱纹。” 老公：“有好多啊？” 老婆：“戴帽子跟拧螺丝似的。‍‍”');
INSERT INTO `tpshop_joke` VALUES ('2959', '“亲爱的，晚上你加班八点前能回来吗？” “不能...” “哦，那我就放心了...” 就这样，男朋友不到六点就回来了...');
INSERT INTO `tpshop_joke` VALUES ('2960', '‍‍一女同事说想买个坐垫，一男同事说送她一个。 女同事：“无事献殷勤，非奸即盗。” 男同事：“反正我不想盗。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('2961', '　　高中的时候我们班有个女老师，差不多更年期了吧，整天特别啰嗦那种，高一她刚进来我们班的时候说的第一句话是，男生都给我做后面去，不想看见你们，这是有多讨厌我们男生啊，后面一年她都没和我们班男生聊过天。。');
INSERT INTO `tpshop_joke` VALUES ('2962', '我发现很多老爸都有一个特异功能，就是看电视时一会儿睡着了，呼噜都震天响了，但你要想换台那是不可能的，因为他会在你换台的瞬间惊醒怒吼：“谁让你换台的，换回去！”......');
INSERT INTO `tpshop_joke` VALUES ('2963', '半夜睡不着， 爬起床来走到客厅抽支烟，发现一只蟑螂，于是跟它聊了很长时间，把我对生活的看法，对上司的不爽，生活的压力，压榨的发泄给它听 ，烟抽完了，于是我狠狠一脚踩死了它， 没办法，它知道的太多了...');
INSERT INTO `tpshop_joke` VALUES ('2964', '‍‍‍‍“妈！母亲节的礼物收到没？” “昨天就到了，我没在家，你爸签收的，下回你不用特意从网上给我买化妆品，我们家楼下的商店，就有卖这个牌子的！” “不对呀！我在网上给你买的是玉做的搓衣板……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2965', '‍‍‍‍老师：“你们最想要的？” 学生：“一次放6个月。” 老师：“最好是？” 学生：“一年来两次。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2966', '去相亲，女孩问我：\"你在公交上会让座吗？\"我笑着说：\"我不坐公交的，我有车。\"女孩又问：\"那你租的房子多少钱一个月？\"我才明白原来旁击侧敲我是不是有车有房，顿时有点生气了：怎么？我像一个租房子住的人？女孩顿时对我很热情，只是饭后我骑着自行车带她来到我住的桥洞下时她好像有点不高兴');
INSERT INTO `tpshop_joke` VALUES ('2967', '‍‍‍‍一天，甲乙丙三人一起去郊游，晚上睡在同一张睡袋里。 突然乙打了一个喷嚏，结果甲和丙脸上全是结晶。 两人一起说：“大哥，以后能不能提前说一声。” 半夜，乙大喊：“注意了！” 甲和丙赶紧躲进睡袋，结果乙放了一个屁。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2968', '‍‍‍‍“老公，我想买双凉鞋，热天没换的鞋了。” “好。” “什么都说好，完全是忽视人家。” “……” “老公，我想买个能烧水又能保温的水壶，你看这个好便宜。” “媳妇，我们不是有个烧水壶么，又不是经常烧水，没必要，浪费。” “不就是个烧水壶嘛，都不愿意人家买，点都不重视人家。” “……” 我说：“我到底该说好还是不好啊！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2969', '学校规定老师上课时不许接电话。一天，我们上生物课，老师电话响了。老师纠结地看了半天，问我们：“领导电话，接不？”我们回答：“必须接！”然后老师出去大喊一句：“老婆干啥啊？我上课呢！”');
INSERT INTO `tpshop_joke` VALUES ('2970', '‍‍‍‍早上老姐去买菜留我和五岁的外甥在家睡，外甥醒了问我：“我妈妈呢？” 我：“买菜去了！” 外甥：“去哪？” 我：“不知道。” 外甥：“到底去哪了？” 我：“我真不知道！” 外甥：“你天天上学上的啥呀！这都不知道，买菜肯定去菜市场啊！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2971', '昨天K歌以后去宵夜、一漂亮妹子问我，“知道两只蝴蝶里的哪句台词最经典不？”我答不知道，她说“穿过丛林去看小溪水”....');
INSERT INTO `tpshop_joke` VALUES ('2972', '今天，一朋友问我。“怎么样让一个人记住你，而且忘都忘不了？” 我淡淡的看了他一眼，扬起手就是一巴掌呼了过去。。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('2973', '‍‍‍‍问：“什么地方的儿女最孝顺？” 答：“朋友圈。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2974', '‍‍‍‍朋友问我：“如果你左边躺着美女，右边是基佬，你会选择对着谁？” 我：“废话，肯定是美女啦。” 朋友：“看不出来呀？！你还挺勇敢的嘛。” 我：“这也叫勇敢？” 朋友：“对呀，换成是我，就不敢把菊花对着基佬。” 我：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2975', '悟空和八戒为救唐僧，却被红孩儿一把三昧真火烧得落荒而逃，这件事史称“烽火戏猪猴”。');
INSERT INTO `tpshop_joke` VALUES ('2976', '‍‍‍‍吃完晚饭，儿子正在那里算数。 爸爸：“儿子爸爸靠靠你好吗？” 儿子：“好呀！” 爸爸：“8+16=多少呀？” 儿子就连忙数手指头和脚指头。 爸爸着急的说：“你用脑子行吧！” 儿子：“加上脑子也不够呀？”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2977', '‍‍‍‍一对夫妻正准备开饭，饭菜已摆桌上。 老公一看，没有喜欢吃的菜，便问：“老婆，今晚咱去外面吃呗？” 老婆：“可以啊！” 结果老公在家门口蹲着吃……‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2978', '今天很累，回家后躺床上玩手机，看视频，看了大概一个多小时，突然想起路由器没开……4G就是快，一点也不卡，不说了，我哭会儿……');
INSERT INTO `tpshop_joke` VALUES ('2979', '‍‍‍‍小呆毕业后在某食堂煎牛排，干了20年。 有一天他下岗了，他被迫流浪了，终于他在火葬场找到了工作。 第一天正准备焚化尸体时，他突然对尸体亲人家属问道：“是要五成熟，还是七成熟……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2980', '‍‍‍‍胖子：“有件让我很生气的事要告诉你。” 果果：“说吧！” 胖子：“今天早上，隔壁的邻居竟然说我是个肥猪。” 果果：“这真是太可恶了。” 胖子：“对呀！” 果果：“他应该说你是大象，这比较贴切。” 胖子：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('2981', '一性感美女摆着屁股从夜店的卫生间走向吧台。以一种撩人的姿态坐下。向酒保诱惑勾了勾手指。酒保连忙过来，美女问：你们经理在吗？酒保：不在，他出去了。美女就把手放在酒保的嘴里，酒保性奋的挨个吸允了十个手指。美女满意的看着酒保，对酒保说：告诉你们经理，卫生间没纸了！');
INSERT INTO `tpshop_joke` VALUES ('2982', '一直以来啊总有人批评80后90后如何思想堕落如何离经叛道 如何的扶不起来 还有人说他们是道德毁掉的一代人 是“垮掉的一代” 我特别的不理解 自顾自跳广场舞扰民的不是他们 不让座就拳脚相加的也不是他们 碰瓷耍赖的不是他们 炒高房价的不是他们 开黑心食品加工坊的不是他们 建小工厂乱排污的更不是他们 是谁自己道德缺失 反过来教育下一代人说这就是现实呢? 是谁自己跌倒了还要反讹把自己扶起来的年轻人这到底是谁扶不起来?  当然那些值得尊敬的长辈们给八零后九零后树立了榜样让他们不断的成长不忘自我反省 于是我们能看到身边大多的80、90后都能够遵守公序良俗 他们会主动的让座 会羞于插队 最坏的行为也就是在电梯里面按亮所有的楼层  如果要给绝大多数80后90后一个整体的印象 我可以这么说 他们是刚一就业就被延迟退休 他们是干20年都买不起房 结不起婚 却每天努力工作的一代人 他们是吃着各种黑心食品长大却每天茁壮成长的一代人 他们是顶着恶劣的就业环境 自然环境 顶着高高的房价 住在北上广出租屋里却没有怨天尤人 每天还在追 求自己理想追求自己想要生活的一代人 如果谁硬要说他们是“垮掉的一代人”那么是被现实');
INSERT INTO `tpshop_joke` VALUES ('2983', '　　早上坐公交车，不久上来一个美女， 　　我就盯着美女脸看，可能美女发觉我在看她， 　　白了我一眼，一脸高傲的样子说 　　：看什么看，没看过美女啊！ 　　我当时楞了下， 　　回道：我眼瞎！ 　　顿时车厢内安静下来。 　　美女那表情，大家体会。。。');
INSERT INTO `tpshop_joke` VALUES ('2984', '　　“我新买了辆君威，开着特费油。” “嗯，通用血统的车都这个德行。” “对，但那不是主要原因。” “那主要原因是什么啊？” “主要原因是我不认道儿。”');
INSERT INTO `tpshop_joke` VALUES ('2985', '逛街看中一条裤子，我对老妈说：“我想买这条裤子。”老妈立刻把钱包递给老爸摊开手说：“我没带钱包出来。”我把求助的眼光投向老爸，老爸点了点头，又把钱包递给老妈摊开手说：好巧，我也没带。');
INSERT INTO `tpshop_joke` VALUES ('2986', '转帖:一朋友姓刘，说按族谱下一代得叫刘星啥，我说你要生个女儿就叫刘星雨，多浪漫。 他说不行，我要个带把的，然后取个霸气的名字，刘星锤！！！');
INSERT INTO `tpshop_joke` VALUES ('2987', '　　惊呆了！排在我前面买单的情侣，抱一堆东西。男的跟女的说：我不反对买东西，但咱现在买回去的，你想想搬家时，都要一点点带走哦。女的估计也搬家搬怕了，转身把一些居家用的都放回去说：只留吃的吧，吃完袋子一扔就没。男的说：这些吃的吃完，长在身上要带一辈子哦。女的把篮子放下，他们直接走了……');
INSERT INTO `tpshop_joke` VALUES ('2988', '人永远不会珍惜三种人：一，轻易得到的；二，永远不会离开的；三，一直对你很好的。但是这三种人往往一离开，就永远不会再回来了。#婚礼#');
INSERT INTO `tpshop_joke` VALUES ('2989', '高中的时候暗恋隔壁班的一小美女，每天下了晚自习都会悄悄跟着她，确保她安全到家，希望以后她会知道我的良苦用心而感动。。。终于皇天不负苦心人，一个半月都不到，她报警把我抓了。');
INSERT INTO `tpshop_joke` VALUES ('2990', '一女子很苦恼的问着禅师： 大师，我长得这麽漂亮，每天出门都被一群男人死缠着告白，我又很难拒绝别人，我该怎麽办呢？禅师默默的从池塘里鞠起一瓢水，泼在女子的头上。 女子恍然大悟的说：我懂了，您是要我心静如水，对待世间万物都以清澈的心态面对……是吗？禅师答 : 没有这麽复杂…你只要...卸妆 就可以了…');
INSERT INTO `tpshop_joke` VALUES ('2991', '　　有一次，我和一位朋友去吃汉堡。他说带我去一家名叫“根莱”的汉堡店吃，这时，我傻傻地问了句：“根莱在哪？”他说：“跟我来！”不知为何，我哈哈大笑起来······');
INSERT INTO `tpshop_joke` VALUES ('2992', '有谁跟我一样认真的想过自己中了五百万，去掉税。然后买什么样的楼 ，什么牌子的车，怎样投资，并且还很纠结的。现在想想，我特么就是闲的。');
INSERT INTO `tpshop_joke` VALUES ('2993', '某日上课，一同学玩汤姆猫，我看的津津有味，但是被老班发现了，同学没关手机直接藏口袋，于是，神奇的事发生了......班主任骂一句，口袋那贱猫就学一句，那画面感简直醉了，全班笑个不停，，，感谢那位同学让我们没上一节课。^﹏^');
INSERT INTO `tpshop_joke` VALUES ('2994', '　　一直以为西点军校是给炊事兵和退伍军人学习蛋糕类西式面点技能的培训学校。。');
INSERT INTO `tpshop_joke` VALUES ('2995', '两人相约去钓鱼。不一会，有个人钓上来一把雨伞，又钓上来一件外套，还钓上来一支皮鞋。另一个人不安地说：“咱们还是别钓了，看来下面住着人呢。”');
INSERT INTO `tpshop_joke` VALUES ('2996', '老王家的狗很爱看电视。一天，狗狗正在看动物世界，老王家来客人了。老王见狗狗在电视前全无动静，气愤地过去关了电视，对狗说：妈的还看A片，没见来客人了吗！');
INSERT INTO `tpshop_joke` VALUES ('2997', '昨天老爹买了一瓶阿胶，自己用手机扫二维码，扫半天没扫出来，叫我来试试。我用苹果五扫了一下，成功。老爹问我：我怎么扫不出来？我开玩笑说你手机不行。今天早上突然发现老爹书桌上躺着一台苹果6！。。。老爹，我的手机也不行啊！');
INSERT INTO `tpshop_joke` VALUES ('2998', '1.老婆每天只给四块钱车费，还只准我穿她的内裤上班，嘴里还哼哼：“要是想乱来，看你怎么有脸脱裤子……\r\n2. 那些不相信男闺蜜的，你们错了，我和一美女就是好朋友，但对她没有任何不良想法，我还经常叫她老公出来喝酒，话说她老公真的很帅，想起就脸红。\r\n3．老板请了个风水先生，一会说办公室花盆位置不对，一会说饮水机位置不好。我正想吐槽这都是封建糟粕，老板就问他哪天正式上班最好，他掐指一算“过了大年，十六上班大吉大利”，老板欣然应许。瞬间感觉传统文化果然博大精深！\r\n4. 女友快一个月没来大姨妈了，总是以为自己有了，于是她遇事就说小心孩子，啥活都让我做，好吧！我都做了。昨天她大姨妈来了，对我说：它来了！而且带走了我们的孩子……\r\n5. 给女友打电话：“亲，发工资了晚上一起去嗨皮吧？”女友：“好啊！刚好今天大姨妈要来，你有口福了！” “对了，今晚要加班，过几天再约你。”“好吧，大姨妈从老家带来好多好吃的，我去给别人。”“逗你玩呢！今晚不加班……” “那个大姨妈也来了！”\r\n6. 情人节快来了，我已做好准备，一旦发现那些情侣吵架我就会在一旁等着，不是捡玫瑰花就是捡戒指，运气好的时候也许还能捡个男朋友或者老公，想想我就好激动。\r\n7. 最近养了只猫，天天粘我。 女朋友说我不关心她了。 我说：你要是能四脚朝天的打滚逗我开心，我也关心你。 女朋友幽幽的说道：我也没少对着你四脚朝天……\r\n8. 和5岁的小女儿玩捉迷藏的游戏，她藏我找，“爸爸你把脸转过去不许回头偷看！”“好的宝贝儿爸爸一定不偷看...藏好了吗宝贝儿？”“我藏好了！你来找我吧爸爸！”床底下传来了女儿调皮的声音……孩子就是单纯。\r\n9. 一对夫妻吵架。老公吵不过老婆，躺倒沙发上不说话，老婆还骂。老公直接来了句:“汪汪汪”。老婆直接一耳光上去。说:“你竟然敢骂我是狗日的!”\r\n10. 今天跟老婆照镜子，我问老婆，我一不帅，二不富，又长年出差陪不了你，你到底看中我哪里啊？老婆说，我呀就看中你们家邻里和睦，我怎么感觉哪里不对啊！');
INSERT INTO `tpshop_joke` VALUES ('2999', '刚刚跟一女网友聊天，她挺激动的，我：“怎么了？”她说：“刚买的5S差点丢厕所了，幸好监控室的保安看到了，跟我说了声。”我说：“有没有感谢人家妹纸？”她说：“什么妹纸？是一个大哥。”一个大哥？我在想要不要告诉她点什么');
INSERT INTO `tpshop_joke` VALUES ('3000', '转：‘’女朋友重要吗？‘’ ‘’ 不要‘’');
INSERT INTO `tpshop_joke` VALUES ('3001', '“喂！兄弟，你在哪呢？”\r\n“我在给亲戚打款呢。”\r\n“正好，先借点给我，你在哪家银行，我现在过去找你。”\r\n“我在老家上坟呢，你要来吗？\"\r\n“……”');
INSERT INTO `tpshop_joke` VALUES ('3002', '上街被相命先生叫住：“年青人，你今年鸿运当头，　　天降横财，老夫免费为你算卦，不准不要钱”，　　我正想离开，却被他凳子下的东西吸引住了，　　于是我跟他海聊了一会突然说，“快跑，我看见城管正从你后面跑过来”。　　老者拔腿而逃，我顺理成章的捡起他掉在凳子下的一百元，　　感叹道，老先生算命真准！');
INSERT INTO `tpshop_joke` VALUES ('3003', '回家刚进电梯。一位爸爸就带着一个两三岁的小萝莉进来了按了13层。　　电梯上升过程。有段时间没人说话，感觉有点尴尬。　　于是我率先打破气氛：这小姑娘的小眼睛可真大啊！　　说完就感觉那爸爸僵住了，感觉再也不会夸人了！。。');
INSERT INTO `tpshop_joke` VALUES ('3004', '路上走着一对情侣，穿着情侣装，是一模一样的款，　　手拉着手，结果谁知从后边走过来一个屌丝男居然和他们穿着一样的衣服，　　这时候男猪脚一把放开女猪脚的手“告诉我，怎么回事？”　　女的反过来给男的一个饼“我还想问你呢！”');
INSERT INTO `tpshop_joke` VALUES ('3005', '夜黑风高的夜晚，妈妈在玩QQ ，　　女儿一边贴面膜一边做瑜伽，　　气喘吁吁的问：“妈妈，我怎样才能成为女神？”　　妈妈淡淡的说：“美图秀秀”');
INSERT INTO `tpshop_joke` VALUES ('3006', '今天考试，一进考场就觉得“马鞍套在驴背上——对不上号”。　　慌慌张张找了半天原来是走错了考场。　　连忙赶到自己的考场，一看试卷真是“马缰绳栓羊头——路子不对”。　　没有办法啊，只有硬着头皮往下写，　　越写越发现是“马背上钉掌——离题太远”。　　终于考完试了，感觉到“马散宠头——自由自在”了。');
INSERT INTO `tpshop_joke` VALUES ('3007', '学校放暑假了，一妹纸和她姐姐坐车回家途中，　　窗外风景无限。妹妹看见车外是一片绿油油的菜地里有一只猪猪在欢快的吃草，　　瞬间想嘲笑她姐一把。于是乎！　　她指着那只猪对姐姐说“姐快看，你妹在那里吃草 ！”　　说完后，姐姐石化了。妹妹也瞬间黑线了。。。');
INSERT INTO `tpshop_joke` VALUES ('3008', '地铁站口，一女生狂抽一男子耳光，　　边打边骂：“我让你在外面玩女人，我让你在外面玩女人！”　　这男子被打急了，说：“那是我老婆！！！”');
INSERT INTO `tpshop_joke` VALUES ('3009', '小鸡问母鸡，人吃了我们的肉和蛋，为什么还不满足，　　竟不惜污染环境，疯狂追逐我们鸡的屁（GDP）呢？　　母鸡答：“因为肉和蛋只有营养价值，　　而我们鸡的屁味道虽不见得好闻，却具有新闻炒作价值，　　能给人精神上的满足、享受！”闻此言，小鸡顿感生而为鸡的自豪，　　更为鸡的屁神奇之魔力而骄傲！');
INSERT INTO `tpshop_joke` VALUES ('3010', 'A有一天想逗B一下，就问B：“你和禽兽赛跑，结果如何？” 　　（这是一道很贱的题目：赢了，比禽兽还禽兽；输了，　　禽兽不如；打平了，和禽兽没两样。）　　可是B回答：“你输了。”　　A很奇怪：“明明是你和禽兽赛跑，怎么我输了？”　　B回答：“因为我和禽兽赛跑，就是和你赛跑啊！”');
INSERT INTO `tpshop_joke` VALUES ('3011', '如果你选了三注双色球，让你女朋友帮你买，结果呢，她为了省两块钱给你买了两注，没买的那一注中了二等奖，大家说这种女朋友还能要不？');
INSERT INTO `tpshop_joke` VALUES ('3012', '离交卷还有十分钟时间监考老师对我们说还有十分钟要及格的同学抓紧时间了于是便走出去抽烟了好老师一生平安');
INSERT INTO `tpshop_joke` VALUES ('3013', '一天学校大扫除，小明拿着扫把在发呆，这时老师过来视察，小王指着小明说:“老师你看小明他没扫”，小明回过神来吼道:“谁说我没有，我哥都结婚两年了”');
INSERT INTO `tpshop_joke` VALUES ('3014', '一天村长找张三李四王二开会讨论殡葬改革，村长：“上面说了要现在土地稀缺，要进行殡葬改革，你们想想有什么好注意？”这个时候王二说了：“要让俺说吧这个改革吗？不就是变一变吗？我看不去把棺材那步省了，直接埋起来。”这是时候李四向前一步说“不去把人横着埋这要可以节省不少土地”只见张三悠悠的说“麻烦，直接把人埋一半，在留一半在上面不就行了”村长不解问为何？“这样不连墓碑都省了，一看不就知道是谁了吗？”村长李四王二：。。。。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('3015', '刚看了一期鉴宝节目，如果宝贝是假的那么一锤子砸碎。 有个河南的哥们拿了一个瓶子说是北宋官窑的，那帮专家一顿指指点点还用仪器鉴定，最后由一个老专家代表总结，上来第一句就是这个瓶子是仿的赝品，话音刚落主持人手起锤落瓶子碎了……老专家颤抖的继续说道，是明国高仿北宋的……');
INSERT INTO `tpshop_joke` VALUES ('3016', '好烦啊，今天房东要来收房租了，我又来大姨妈了。这下房租可咋办啊…………55555555');
INSERT INTO `tpshop_joke` VALUES ('3017', '售票员：你提的是什么东西？ 乘客：酒精. 售票员：NTMD装傻是不？！酒精还敢带上车 滚下去灬 乘客：口误灬灬这是乙醇. 售票员：TMD不早说 赶紧的上来 车就要开了');
INSERT INTO `tpshop_joke` VALUES ('3018', '男：昨天在街上突然内急，没带纸，附近又没商店，找了半天才找到一个ATM机，于是我就提了500块搞定， 女：土豪啊，我们做朋友吧！不过你这是侮辱人民币你造吗？ 男：可以啊！不过我取了5次，每次都取100，每次都让机器给我打印一张凭条，你以为呢？ 女：……');
INSERT INTO `tpshop_joke` VALUES ('3019', '刚才曾经追过的女神发微信说要3块钱买冰激凌，我直接红包5块，告诉她砍砍价买2吃，过了一会她回了我十块说开玩笑的不用我钱，我当时就有小情绪了，我说我的就是你的，就回了她20。后来她给我40。我就回80心里想这样也好能多多交流还能装逼，然后她又160，我就320。然后就被拉黑了。。。');
INSERT INTO `tpshop_joke` VALUES ('3020', '有一天，老小驴问老驴说：为什么人家奶牛吃的都是精饲料，我们却只能吃干草？老驴说人家奶牛是靠胸部工作的，我们只能靠两条破腿照不一样。');
INSERT INTO `tpshop_joke` VALUES ('3021', '真事，一次朋友把电脑打开清理灰尘，用水洗显卡可以理解，但是为什么要用微波炉烤！这货跟我说家里没有电吹风，就听那微波炉中噼里啪啦噼里啪啦。。。。。。');
INSERT INTO `tpshop_joke` VALUES ('3022', '‍‍‍‍本人身高174cm，体重63kg，体型偏瘦。 晚上洗完澡穿了衬衫和内内去刷牙。 女票：“好性感啊。” 我：“羡慕我的大长腿吧？” 女票：“你那明明是黑毛腿好不好？” 我：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3023', '‍‍‍‍昨晚看新闻，播报员说：“犯罪嫌疑人已经浮出水面了。” 我当时就叫道：“我靠，犯罪分子太特么机智了，居然藏在水里！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3024', '一到考试就觉得上帝为我关上了英语的窗，顺便带上了数学的门，塞上了物理的排水口，还堵死了化学的下水管道，就连生物的狗洞都给我拿水泥砌上了。。。');
INSERT INTO `tpshop_joke` VALUES ('3025', '一次和老公出去旅游开房，看电视正起劲，旁边房间一男一女正战况起劲！女的不停的大声呻吟中。。一开始我们两个还打算忽略，后来她越发起劲的喊，影响我们看电视情绪了。老公端着水问：怎么办? 于是，我深吸一口气。大声的喊了句：啊～干爹，你好厉害哦～～啊～～旁边房间顿时鸦雀无声。。老公喷了');
INSERT INTO `tpshop_joke` VALUES ('3026', '‍‍‍‍一哥们儿前段时间结婚了，等到我们闹洞房走了以后，那货竟然躺在床上睡着了。 第二天他媳妇回娘家了，哥们儿就去丈母娘家了。 他老婆就生气的说：“你个傻子，你就不知道你那个是干嘛用的，以后TM的别来找我。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3027', '‍‍以前在外地上班，就搞上一个不错的妞。 刚在一起的时候，我说：“你很像我老婆。” 看到她那高兴的样子，我心还想这女孩到底想什么啊？ 后来她看到我老婆的时候，默默的说一句：“还真像。” 再后，就没有了后来。‍‍');
INSERT INTO `tpshop_joke` VALUES ('3028', '现在才意识到，谈恋爱无非就是两种结果：一是，各回各家，各找各妈；二是，你妈变我妈……');
INSERT INTO `tpshop_joke` VALUES ('3029', '“你有梦想吗？”“……”“你连一点梦想都没有，活着还有什么意义？”“我有女朋友……”');
INSERT INTO `tpshop_joke` VALUES ('3030', '‍‍‍‍本人当学徒那会儿，一旦有点风吹草动，师傅就会开始喊我的名字。 于是我远远地从工具房跑过来，他说：“给我拿个螺丝刀。” 然后我又跑回去拿工具，再跑过来。 反对多次后无果，终于有一天，我不胜其烦，每样系列的工具收集到一个箱子里。 再叫我的名字，我就抬箱子过去一扔：“给，要什么自己挑。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3031', '我和邻居小张带着孩子去公园玩，我儿子两岁了，小张的孩子七个月。　　小张长得虽然不算美女，但是身材保持的很好，今年二十三岁，如果不抱着孩子真看不出已经是个孩子的妈妈。　　小张的孩子饿了想吃奶水，不停地咬她的衣服，小张把孩子递给我让我帮她抱一下，解自己的衣服。　　这时路过一个年轻的小伙子，看到小张在解衣服，眼珠子充满好奇都快瞪出来了，见小张解完衣服，从我手里抱过孩子去。　　恍然大悟般地收回了眼睛，若无其事地走了。');
INSERT INTO `tpshop_joke` VALUES ('3032', '两个年轻的小伙子，走过广场，看见两个盲人在唱歌乞讨，走出不远其中的一个小伙子，突然站住，对旁边的小伙子说：我这个人还是非常有同情心的，要不你去帮我献一下爱心。　　另一个小伙子不假思索地说：行！　　只见这个小伙子从兜里掏出了一个一分钱的硬币。　　另一个小伙子说：我丢不起这人！要不还是你自己去献这份爱心吧！');
INSERT INTO `tpshop_joke` VALUES ('3033', '外籍考察团在五星级酒店下塌，领导陪客人就餐完毕，准备离去，在门口等候车辆。车停在地下停车场，迟迟不见出来，于是打电话给司机：“怎么这么磨蹭，客人等得不耐烦了，你在哪？”　　司机压住心中怒火说：“我还在地下停车场找出口呢！”　　不知道是哪个缺德的保安把路标转了一个方向，害得司机在下面转了好几圈。　　司机见一大哥迎面走来，疑似工作人员，遂问：“大哥，出口在哪呀？”　　那位大哥哭笑不得：“我连车都找不到了，还出口呢？，我就想出口骂人！”');
INSERT INTO `tpshop_joke` VALUES ('3034', '两个人在酒吧喝酒，A说：“老婆怄气不跟我说话了。” 　　B：“你怎么得罪她了？”　　A：“她要去美容院，跟我要500块。” 　　B：“然后你没给？”　　A：“给了，我给了她整五千……”');
INSERT INTO `tpshop_joke` VALUES ('3035', '在家杀鸡，掐着一只它的脖子却不敢下刀，踌躇良久，那只鸡竟然被我掐死了！');
INSERT INTO `tpshop_joke` VALUES ('3036', '看一个介绍北极圈白猫头鹰的节目,一对夫妇养育五只幼仔,每只幼仔一天需要六只旅鼠。父亲负责捕食,往返数次从2.5公里外送来食物。　　我正感叹父亲真不容易，太帅了，讲解徐徐说道 “如果父亲感到负担过重，会失踪逃走。”');
INSERT INTO `tpshop_joke` VALUES ('3037', '“你为什么纹这两个汉字在身上？”　　“别提了！那天我去文身店。坐好之后突然闻到一股臭味，我使劲闻了闻，想找出来源。　　这时候文身师傅说你纹什么，我说好臭……”');
INSERT INTO `tpshop_joke` VALUES ('3038', '看着白球从洞口擦边而过，丁俊晖松了口气，拿起电话说道：“好险，刚刚白球差点进洞。”　　电话那头的朋友说：“小丁啊，看来你这高尔夫还得常练啊。”');
INSERT INTO `tpshop_joke` VALUES ('3039', '本人在酒吧上班，有几个客人喝多要找代驾，不巧没人，无奈LZ去代……　　　　行至孙中山像十字路口等红灯，见几个民工推着人力车，上面载满货物小跑过红灯，车上几人嘲笑似的高喊加油加油。　　LZ没有作声，行至一半路车上几人都已睡着，我故意给他车连闯了三个红灯。');
INSERT INTO `tpshop_joke` VALUES ('3040', '昨晚金鹰节，当开奖嘉宾撒贝宁上台的时候，和崔永元说起了“相声”。　　当撒贝宁说到，自己没有老婆时，全场嘘声一片，大喊章子怡。　　连崔永元也忍不住笑撒贝宁，强大的观众啊……');
INSERT INTO `tpshop_joke` VALUES ('3041', '有事去外地几天…媳妇和儿子非得要送我…火车来的时候心里有点忧伤，有点舍不得他俩…临走前儿子大声对我说“爸爸，路上慢点啊”…听完心里好欣慰“放心啦，过几天爸爸回来给你买好多玩具”…儿子点点头，又大吼“给我带回来个新妈妈啊”…');
INSERT INTO `tpshop_joke` VALUES ('3042', '‍‍‍‍在路上看到一小男孩和小女孩说：“我们来玩扮明星的游戏吧！” 那小女孩说：“好啊，那你把我当邓紫棋吧。” 说罢，小男孩就骑在了女孩身上。 小女孩火了，说：“我让你把我当邓紫棋，不是让你把我当凳子骑！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3043', '女神经常在来大姨妈的时候跟我去宾馆谈人生，望着电梯楼道众男羡慕的眼神，我感觉我又吃亏了。。。');
INSERT INTO `tpshop_joke` VALUES ('3044', '‍‍‍‍昨天晚上去网吧通宵，第二天早上早读迟到了。 班主任问我：“去哪了？” 我说：“在宿舍睡觉睡过头了。” 班主任问我：“你骗狗呢啊？” 我默默的低下了头说了句：“嗯。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3045', '‍‍‍‍前几天去面试，休息区等待时听见俩哥们在聊天。 一个问：“你家离公司只有几百米，你为什么每天还骑车？” 另一个反问：“自行车胎多少钱一条，皮鞋多少钱一双？单车内胎多少钱一条？” 那个又问他：“你既然这么省钱，午餐明明有十块的面条干嘛吃十一块的饺子？” 这个把眉毛一挑：“饺子上面经常会挂一两根面条，但你见过面条上能挂饺子吗？” 我当场跪了……‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3046', '‍‍‍‍某男生在食堂排队打饭，看见前面的一女生对打饭阿姨撒娇的说：“姨妈，我来那个了，给我打点热的。” 结果阿姨给女生打了好多好吃的。 轮到某男时撒娇的说：“姨妈，我来那个了，给我打点热的。” 结果阿姨说：“你一个男生那有那个？” 给他打的很少，某男生气的说：“为什么她可以，我就不可以？” 阿姨说：“因为你姨妈不认识你。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3047', '电视屏幕上出现举重塞颁奖仪式，一位运动员登上领奖台，高高举起奖杯。 爷爷问：“那个举杯子的是干什么的？”我回答他是举重冠军。爷爷笑着说：“就那么个小东西，我也能举起来。”');
INSERT INTO `tpshop_joke` VALUES ('3048', '‍‍‍‍老师：“小明，你说古代著名医生李时珍的著作是什么？” 小明：“老师，他有什么著作我不知道，但我知道他临死前说什么了！” 老师疑惑的问：“他说什么了？” 小明：“他说，尼玛这草有毒！” 老师：“滚粗！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3049', '一女的，不割，说自己买的电动车家里停不了，出自好心:你家门口可以停、实在不行你弄个狗帮你看着，结果二货过两天哭着说:\"我的车不见了、狗也没了。怎么回事？狗不叫吗？可能狗叫声太小了。你狗多大了？两个多月了、我…………！！！');
INSERT INTO `tpshop_joke` VALUES ('3050', '‍‍‍‍老师点小明起来回答问题。 “世界上最珍贵的马，是什么马？” 小明答：“奥巴马。” 老师又问：“那由黑色和白色组成的马是什么马？” 小明答：“二维码！” 小明的思维，无人能敌。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3051', '吃自助餐不小心剩了很多，一个五大三粗的服务员狠狠的说：“本店剩菜必须吃完，否则后果很严重！”于是我硬着脖子把剩菜全部咽了下去，撑得我直翻白眼，完了我忍不住问他：“如果不吃完会有什么严重的后果？”大汉说：“会给国家资源造成巨大的浪费。”');
INSERT INTO `tpshop_joke` VALUES ('3052', '发现没，瘦子考砸了也食不下咽，失恋了也食不下咽，找不到工作也食不下咽，愤怒的时候也食不下咽，悲伤的时候也食不下咽，寂寞的时候也食不下咽。而胖子，考砸了也暴饮暴食，失恋了也暴饮暴食，找不到工作也暴饮暴食，愤怒的时候也暴饮暴食，悲伤的时候也暴饮暴食，寂寞的时候也暴饮暴食。');
INSERT INTO `tpshop_joke` VALUES ('3053', '朋友们都叫我屌丝，呵呵。自从我说了我的工资后，他们就不叫我屌丝了。人的本性啊！现在他们都叫我穷屌丝。。。');
INSERT INTO `tpshop_joke` VALUES ('3054', '买了一袋橘子还没来得及吃，室友不 打招呼便剥开往嘴里放，我好心提醒 ：“橘子不要吃太多，容易上火！” 他 不以为然：“没事。” 说完伸手又要去 拿，全没有停下来的意思。“你特么还 没完了，是吗？” 失去耐心的我抄起 板凳朝他扔去，“老子跟你说了吃橘子 容易让人上火，容易让人上火，你偏不信，Cao！”');
INSERT INTO `tpshop_joke` VALUES ('3055', '女友没吃晚饭，买了块炸鸡，刚炸的，超级好吃的样子，出了门口没走几步就啃完了！然后，她又折了回去，一模一样的再买一块。 她还有点忧郁地对服务员说：哎呀今天好背，我刚刚买了一块还没吃就掉地上了。服务员说：刚刚在门口吃很大口那个不是你吗？');
INSERT INTO `tpshop_joke` VALUES ('3056', '昨天在公司吃饭，经理过来看了看我对面的妹子说：“吃这么多，都把公司吃穷了，我看你不用在这干了。 ”　　妹子蹭的站起来：“不就是看见你上班看Ａ片吗？至于吗？”');
INSERT INTO `tpshop_joke` VALUES ('3057', '是不是又被最有情怀没有之一的女教师辞职信“世界那么大，我想去看看”咔嚓一下给感动抽过去了。以后公司辞退你的时候，领导辞退信也这么写：“世界那么大，我想让你去看看。”青春！情怀！热泪盈眶！');
INSERT INTO `tpshop_joke` VALUES ('3058', '人在东京，出来吃点东西，服务员妹子特别萌，我反正不会日语，吭哧瘪肚跟她：第四万，普力斯……妹子看我一眼：你是不中果人哪？我一愣，说是啊……结果刚才超萌的妹子立刻变身：诶妈，妹有了，你点那个玩意儿妹有了！你整个别的吧！！…………？？？');
INSERT INTO `tpshop_joke` VALUES ('3059', '和媳妇冷战了几天了，实在是控制不住自己。于是发了一条消息给媳妇。：“你知道爱是什么吗？”媳妇：“是什么？”我：“是小狗止不住的亲你的脸，想方设法往你怀里钻。”媳妇：“哦，然后呢。”我：“即使你很久没有理这条小狗的时候。”紧接着接到了媳妇的电话，感动的稀里哗啦的。我一边对着她说着暖心的话，一边走向了卖套的超市。。。');
INSERT INTO `tpshop_joke` VALUES ('3060', '女朋友有洁癖，什么东西只要掉在马路上就嫌脏不要了。  今天我去接她下班，然后陪她一起逛街的时候，我拌倒了……');
INSERT INTO `tpshop_joke` VALUES ('3061', '　　给小外甥买的一盒摔炮，结果不知道他神马时候放电脑桌旁的凳子上，当时也没看急着玩电脑就一屁股坐上去啦，我去一缕青烟，一声闷响吓死我了！哪场面被我姐看到了那个笑的是一个灿烂。');
INSERT INTO `tpshop_joke` VALUES ('3062', '现在我来告诉大家，新鞋和旧鞋的区别在哪里！刚买了一双新鞋，朋友踩到我了：“卧槽，你特么踩到我鞋了”。当这双鞋穿成旧鞋的时候，朋友踩到我了：“卧槽，你麻痹踩到我脚了”！！！');
INSERT INTO `tpshop_joke` VALUES ('3063', '太久没有接吻，连吃个鸭舌都会感到温柔。太久没有牵手，拿个泡椒凤爪都会感到颤抖。');
INSERT INTO `tpshop_joke` VALUES ('3064', '逛淘宝不知道你们有没有这种感觉，就是想买裤子的时候看那些图片都会喜欢上配裤子的鞋子，想买外套会喜欢上图片上的衬衫，想买衬衫一定会看上配套的裤子');
INSERT INTO `tpshop_joke` VALUES ('3065', '　　刚刚……就刚刚……女朋友打电话给来说:“亲爱的，大过年的咱们分个手吧”.我顿时火冒三丈，愤努的大吼:你有病啊，和我散了，你他妈挺个大肚子喝西北风去。我是不是说的有点过分了，过了……马上去磕头认错。');
INSERT INTO `tpshop_joke` VALUES ('3066', '女人逛商场说的话和男人在床上说的话一样！男人：我只蹭蹭不进去！女人：我只逛逛不买！');
INSERT INTO `tpshop_joke` VALUES ('3067', '被别人删的时候腾讯不会告诉你，因为腾讯怕你伤心，你删人的时候腾讯问你确不确定，因为腾讯不想让你后悔');
INSERT INTO `tpshop_joke` VALUES ('3068', '每次大过年的，最怕小孩子捂着耳朵对你笑。特么我还真遇上了，那天走在乡村的 小路上，一熊孩子就特么捂着耳朵对着我笑。我以迅雷不及掩耳之势冲上前去把小孩子抱住。特么要死一起死...');
INSERT INTO `tpshop_joke` VALUES ('3069', '一个问新疆的朋友说，你们老是买买提的 这个买买提是谁？ 我们新疆的买买提就像你们说的小明。。。。。');
INSERT INTO `tpshop_joke` VALUES ('3070', '　　今天在舅妈家搓麻将，我爸嘴里吊支烟。邻居家小孩在玩划炮，屋里有个烤手的炉此为背景。突然画出一道弧线，炮落在了炉里普次一声所有人都想老鼠一样往屋外跑。哎！不说了去劝架去。');
INSERT INTO `tpshop_joke` VALUES ('3071', '一宿舍的同事在厕所抽烟，怕从后面往马桶里扔烟头会烫着pp，就从前面扔，结果烫的那个惨啊，连裤头都没办法穿，就找了两个创可贴打了个十字绷。今天下午一起去洗桑拿，旁边的一哥们跟他说：“兄弟洗澡够隆重的呀，连老二都打着蝴蝶结。”');
INSERT INTO `tpshop_joke` VALUES ('3072', '姐上初一时，班上很多女生来大姨妈了，姐还没有，听到一女生说现在不来大姨妈将来会流很多血，姐怕了，又不懂怎么才会来大姨妈，看到人家垫了卫生纸，自己回家也垫了张纸，以为这样就能来大姨妈了。哎，等了一天也没见血，卫生纸还老是滑落…');
INSERT INTO `tpshop_joke` VALUES ('3073', '我想当年白素贞身体不舒服就是因为有蛇精病吧！');
INSERT INTO `tpshop_joke` VALUES ('3074', '昨天朋友聚会，一个年龄差不多的新朋友席间问我“咱俩个谁大”，我说“这个，咱俩得掏出来比比”，他愣了一下，脸红着说“我说的是年龄”，我说“我说的是掏出来身份证比比年龄，一样啊，哈哈”。');
INSERT INTO `tpshop_joke` VALUES ('3075', '大龙觉得自己的名字太土了，于是到小明那里去改名字。大龙问小明：在我的名字后面加上一个什么字才能使它不土而且还很新颖呢？小明说：我能加一个字，使你的名字变得生龙活虎起来。大龙激动的问：什么字？小明不假思索地说了一句：大龙虾');
INSERT INTO `tpshop_joke` VALUES ('3076', '我娘又跟我谈人生，说以后可以找个军人男票，因为军人体质好。我：“军人多基佬。”娘亲：“嗯？基佬是什么？”我：“就是同性恋。”娘亲：“哦～怪不得说军队是个大‘基地’。”……娘亲啊?！！！');
INSERT INTO `tpshop_joke` VALUES ('3077', '“你的脑子被驴踢了么？” “那你不知道下脚轻点啊” “……”');
INSERT INTO `tpshop_joke` VALUES ('3078', '“医生，傲娇怎么治？” “哼，我才不告诉你。” “……”');
INSERT INTO `tpshop_joke` VALUES ('3079', '小学时捡过一次铅笔上交被老师表扬，从此走上不归路，经常去找东西上交，还有好几次拿自己的东西交换表扬。最奇特的一次是上课故意晃桌子，把同桌的笔晃掉，抢先一步捡起来，就去讲台上交。同桌也不示弱，死死拖住我，我就一步步把他拖到讲台对老师说:老师，我捡到一支笔！同桌拖着我哭着说:我的……');
INSERT INTO `tpshop_joke` VALUES ('3080', '那天和朋友一起去医院做体检，办健康证。医生给我们一人一个试管和棉签，棉签是那种一个木棒上面有团棉花。让我们去厕所取点便便做化验，我说早上刚拉完的。怎么办。医生说：没事，去厕所用棉签插入菊花里会蹭到一些。结果插进去了，这时一朋友叫我。那声音满走廊都能听见，当时我一紧张，菊花一紧，啵，一拔出来，哎呀我去，棉花呢。');
INSERT INTO `tpshop_joke` VALUES ('3081', '今天我和我老婆给2个多月大的儿子洗澡！三岁的女儿跑过来非要帮忙。女儿帮儿子洗了一小会儿，突然问：妈妈，是不是小孩子那里都会长个小尾巴呀？我老婆笑着说：是呀。女儿又问：是不是弟弟像我一样长大了，小尾巴就没了？当时，我和我老婆笑喷了！');
INSERT INTO `tpshop_joke` VALUES ('3082', '问：男人纵欲过度会肾虚，女人纵欲过度会怎样？ 神回复：也是男人肾虚。。。');
INSERT INTO `tpshop_joke` VALUES ('3083', '‍‍一老农在繁华的市区被撞倒，立马有人来围观。 老农恍惚地坐起来：“这是哪儿啊？” 一人说：“地图5元一张。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('3084', '高三那年晚自习，我和同桌无聊，相约将自己喜欢的人姓名写在纸上递给对方。递完后我和他打开纸团，上面赫然写着的是我和他各自的姓名，我们望着对方，久久都没有说话。谁能想到，我俩都他吗这么不要脸写下了自己的名字呢？');
INSERT INTO `tpshop_joke` VALUES ('3085', '‍‍家有小正太，有一次他奶奶问他：“长大娶个什么样子的媳妇儿。” 他说：“要娶个又漂亮又美丽的。” 奶奶说：“长的漂亮的不安全，会被人拐走的。” 他想一想说：“怪不得妈妈没人拐。” 一大家子人呀！不说了让我哭会，老公眼泪都笑出来了。‍‍');
INSERT INTO `tpshop_joke` VALUES ('3086', '奉劝各位男同胞不要和聚在一起的妇女开玩笑，昨天在我们村口，一个男的给正在打麻将的四个妇女说，嫂子们，三八节了，来一个人亲一下算是三八节的礼物。说着就上去亲去了，结果四个妇女把他绑到路边树上，还脱光了衣服，关键是，光天化日之下还给他弄硬了。不要问我咋知道的，冻感冒了，难受呀。');
INSERT INTO `tpshop_joke` VALUES ('3087', '我和朋友上床了，我们只是朋友！我们认真了，难受死了！我们都怕彼此会爱上对方，因为如果我俩相爱在一起，会有太多阻碍，到最后不会有结果！！直到我们都想开了！我们约定，只要开了灯，我们就只是朋友！！！他会来我家，会来给我做好吃的菜，期间我们聊朋友聊的天！！！然后我们关了灯，拥抱，亲吻，纠缠！！一番过后再开了灯，继续聊朋友的话题！！！');
INSERT INTO `tpshop_joke` VALUES ('3088', '今年带女友回家……\r\n饭桌上，开始还挺斯文的,别说不喝酒就连菜都不好意思夹,老爸说:“闺女到这就像到自己家里一样，不要拘束，”从老爸句话过后,女友和老爸碰了一杯接着和小叔碰了一杯,接着两杯三杯 …………都喝的差不多了,女友和小叔掰腕子，和老爸唠家常，我拉女友别喝了，她竟然一把把我推到在地,然后对我爸说:“大爷,你儿子跟着我,你就放一百个心，我绝对不会让别人欺负他的”我………');
INSERT INTO `tpshop_joke` VALUES ('3089', '今天是三八妇女节，小紫在这里祝捧腹全体人员节日快乐，虽然有些人不是妇女，但是你们是三八啊。');
INSERT INTO `tpshop_joke` VALUES ('3090', '接上女朋友 急急忙忙吃完晚饭 准备杀进宾馆痛痛快快来一场 有说有笑的进了大厅 之前有打电话预约钟点房 大家也明白钟点房是干嘛的 电话预约时就觉得女客服声音很亲切 妈的 去了前台尼玛 老子之前的女朋友 还就她一个人值班 卧槽 这怎么说 怎么开口 我日 这他妈什么狗血剧情 我这特么是在演电影么');
INSERT INTO `tpshop_joke` VALUES ('3091', '我们初中时，有个数学老师，每次上课前都会给大家讲个超级冷的笑话。　　于是，我和几个哥们儿，决定等下一节数学课上老师讲冷笑话的时候故意放声大笑。　　然后，下节课时，数学老师上来就来一句说他爸去世了，结果...　　我大笑，全班鸦雀无声...');
INSERT INTO `tpshop_joke` VALUES ('3092', '往往当一个人说：“不是我吹牛”，这句话的时候，他就开始要吹牛了。 　　往往当一个人说：“不是我打击你”，这句话的时候，他就开始要打击你了。 　　往往当一个人说：“不是我批评你”这句话的时候，他就要开始批评你了。 　　往往卖东西的人说：“不是我骗你”这句话时，他就要开始骗你了。');
INSERT INTO `tpshop_joke` VALUES ('3093', '甲：给你讲个故事，从前有个傻子，别人问他什么他都说\"没有\"，　　比如问他你吃饭了吗？他说\"没有\"，你叫什么？他说\"没有\"。　　唉，对了，你听过这个故事么？　　乙：“没有”。');
INSERT INTO `tpshop_joke` VALUES ('3094', '又到毕业季，食堂门口有学姐卖书，标语写到：“一元一本，买书送学姐啦！”　　果断上去围观，一新生装模作样的挑了一本书，　　指着标语说：“送的学姐呢？”　　姑娘豪迈的答到：“我们后天的火车，到时候记得来送我们啊。”　　围观的人爆笑，该男面红耳赤低头而去。');
INSERT INTO `tpshop_joke` VALUES ('3095', '有个人去动物园要当动物管理员，　　园长对他说：“那好，我要考考你，你有没有办法让大象先摇摇头，　　再点点头，最后跳进游泳池呢？”　　那人说“这容易！”　　于是他走到大象面前说：“你认识我吗？”大象摇摇头。　　那人又问：“你脾气大吗？”大象点点头。　　那人这时拿起一个锥子，扎了大象屁股一下，大象疼的跳进了游泳。　　园长看到后说：“你太没同情心了，不能当动物管理员。”　　那人说：“再给我一次机会吧，我一定会温柔的。”　　圆长说：“好，还是那三个条件，不过这次你不能动手了。”　　那人答应了，走到大象面前说：“你脾气还大吗？”大象摇摇头。　　那人又问：“你现在认识我了吗？”大象点点头。　　那人再问：“你现在知道该怎么办了吧？”　　大象一听，转身跳进了游泳池。');
INSERT INTO `tpshop_joke` VALUES ('3096', '男孩感冒了不想吃饭，女孩说你吃不吃，男孩说不吃，　　女孩说你不吃我就不理你了，男孩说好嘛，我吃就是了。　　女孩感冒了不想吃饭，男孩说你吃不吃，女孩说不吃，　　男孩说你不吃我就不理你了，女孩说不理就不理，哼！　　男孩说，我错了，你就吃点嘛。');
INSERT INTO `tpshop_joke` VALUES ('3097', '昨天晚上中心广场出现了一群快闪族。　　老王问我，刚那群突然出现跳个舞又不见了的都是些什么人。　　我说这你就不懂了吧人家这叫快闪族，是一种行为艺术。　　老王说：“哦，既然是艺术，我的钱包等会他们偷完会还我不？”');
INSERT INTO `tpshop_joke` VALUES ('3098', '女儿问他爸爸说：“爸爸，爸爸，我是你的什么啊？”　　她以为爸爸怎么也会说‘你是我的小心肝啊之类’的，　　谁知他爸回过头幽怨的说：“你是我冲动的惩罚。”');
INSERT INTO `tpshop_joke` VALUES ('3099', '某日，姑姑带堂弟去吃肯德基。　　刚一坐下，弟弟就指着墙上禁止吸烟和带宠物入内的标语说： “姑姑，这里不让吸烟。”　　我姑姑想，这标语效果不错呀，妇孺皆知呀。 　　结果弟弟又来了句：“谁要是吸烟啊，就放狗咬他。”');
INSERT INTO `tpshop_joke` VALUES ('3100', '我家有侄子一枚，五岁，某天回家发现胳膊青了，　　于是乎他妈妈问怎么弄的，小家伙说小姑娘咬的，问为什么？　　他无比神气的说：“我亲她了呗。”　　瞬间全家笑爆了。');
INSERT INTO `tpshop_joke` VALUES ('3101', 'TMD今天吃完饭准备开车，居然看到一小孩在用石头划我的车，我这暴脾气正要发火。突然想起段子精神，于是微笑着走过去，“小朋友，画得真好，来叔叔给你10块钱，看到对面那辆宝马没有，那边会给你更多”。看着小朋友开心的向宝马车走去，我哼着歌，开着我的小长安潇洒的离开了！！！');
INSERT INTO `tpshop_joke` VALUES ('3102', '女:我感冒了。男:喝点热水就好了。 女:我来事了。男:喝点热水就好了。 女:我胃疼了。男:喝点热水就好了。 女:我们分手吧。男:我会难过。女:你特么喝点热水就好了！');
INSERT INTO `tpshop_joke` VALUES ('3103', '‍‍‍‍现在哥们在公交车，这是上来一对年情侣，大概20岁的样子。 可是就一个座了，那男的直接就坐上去了。 女：“大哥，让个座吧我是孕妇。” 男：“啊，你怀孕几个月了。” 女：“从我们在宾馆里出来到现在半小时了。” 我怀孕半小时了！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3104', '‍‍‍‍昨天晚上做梦老是有人拿棍捅我屁股，结果一个翻身一脚踢过去。 结果老公“嗷”的一声响彻夜空，给吓醒了。 看到老公躺在地上捂着老二，一脸痛苦的在地上滚来滚去。 不知道他这是怎么了！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3105', '晚上睡觉,老公睡在外面!打算去上厕所又怕把他弄醒就在他身上爬过去了,回来的时候又爬过来了!等白天他睡醒了,告诉我:媳妇儿我昨晚睡着的时候梦到两辆大卡车在我身上压过去! 大！卡！车！');
INSERT INTO `tpshop_joke` VALUES ('3106', '‍‍‍‍我今天上夜班，老板半夜让我去取点钱,总共需要两万二。 老板跟经理就讨论取多少。 老板说：“取两万五！” 经理说说：“取三万！” 在他们喋喋不休讨论的时候。 我弱弱的说了声：“自助取款每天只能取两万。” 然后，我就被骂了一顿。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3107', '‍‍‍‍睡醒后发现老公已经上班去了，我懒洋洋的伸了个懒腰。 从嘴里捏出来一根头发，心想这么短，肯定是老公掉的。 不对！老公没烫头呀？‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3108', '从前有个傻子叫老八，不认钱，别人拿一张一块的和一张五块的问他要哪个，他只挑一块的拿走，每次有人逗他这个傻子总是拿走那张一块的，不拿五块的，人们纷纷笑他太傻，拿他寻开心的人也越来越多。 有一天傻子的老爸忍不住问他：别人给你钱你怎么不挑大的拿呢！ 傻子说：要是挑大的票拿，以后谁还拿钱逗我！！。。。');
INSERT INTO `tpshop_joke` VALUES ('3109', '就在刚才，儿子在看中央少儿频道，然后播放那首熟悉的大风车吱呀吱悠悠地转，我一边喂儿子喝钙，一边不由自主地跟着唱起来：“Tm滴Tm滴真好看”我真不是故意的，谁相信我。。');
INSERT INTO `tpshop_joke` VALUES ('3110', '刚刚在网上看到的，李敖点评郭美美：年轻的姑娘炫富，无非就两种情况，要么睡她的人牛逼，要么睡他妈的人牛逼。郭小姐称：自己所有的钱都是通过炒股而来。广大网友很纳闷：究竟是哪支股能这么厉害呢？后来才明白，原来是屁股……');
INSERT INTO `tpshop_joke` VALUES ('3111', '一姐妹晚上下班回来对我说：“走吃宵夜去、福建料理！” 我一听，呵，“升级了”改吃料理了 就屁颠屁颠的跟着出去了、 到了一看，尼玛，“沙县小吃”');
INSERT INTO `tpshop_joke` VALUES ('3112', '说有一帮哥儿们去吸毒，正在幻觉很爽的时候，警察突然出现了，其中一哥儿们大字型爬在墙壁上，警察过去点了他一下，谁知回头说了一句话：哎呀妈呀，我都变成壁虎了你还能看我呢？');
INSERT INTO `tpshop_joke` VALUES ('3113', '女朋友有时候狠二，有一次吃完午饭回学校，校门口有个乞丐，她就走过去了，给了五块钱，一想想又从里面拿了出来，拿出了个绿箭放了进去，当时都笑蒙了。她还解释说，我要是有十块我肯定把这五块给他........');
INSERT INTO `tpshop_joke` VALUES ('3114', '　　智能手机在路上遇见了早期的大哥大，打了招呼：“哟~ 这不是古巨机么~”');
INSERT INTO `tpshop_joke` VALUES ('3115', '说过我自己的糗事。北京现在公交分段计价了，上下车必须刷卡，不然扣除全程票价。如果哪天看到一个文静的女纸疯狂的追车，对，没错，那个人就是我。尼玛，你能想象那个司机一脸黑线的样子么，因为我其实下车刷过卡了，精神恍惚以为没刷，车走了后拼命追了一段，还有一个志愿者老太太戴着扩音器帮我一块儿追，说师傅，我没刷卡呢，滴，居！然！没！有！扣！钱！！居！然！刷！过！了！满满的一车人，静静的目送我……想想也是醉了。');
INSERT INTO `tpshop_joke` VALUES ('3116', '刚去吃晚餐， 进了一家常去的饭店。 老板： 来碗牛肉面！ 不一会老板就端了过来，放我面前，…不是牛肉面吗？ 怎么变鱼粉了？ 心想无所谓反正煮好了， 正准备拿筷子， 尼玛…老板拿了块端碗的布过来，看着我说：烫死我了。 然后他就端给后面桌的了…');
INSERT INTO `tpshop_joke` VALUES ('3117', '　　古龙小说中的一个故事分享给大家，天天开心哈。 　　一个人问大侠，你贵姓啊， 　　大侠说，我不敢说，怕你吃了我 　　那人就问，姓范？ 　　大侠摇头 　　那人问，姓蔡？ 　　大侠摇头 　　那人又问，姓杨？ 　　大侠还是摇头 　　那人又问，姓马？ 　　大侠还是摇头 　　那人不甘心又问，那姓啥啊？ 　　大侠说，姓史(屎)。 　　哈哈。');
INSERT INTO `tpshop_joke` VALUES ('3118', '今天上淘宝看中一款衣服然后进去看评论，看到一条差评：\"和我妈一人买了一件结果穿起来我妈比我还年轻，差评！\"店家回复：\"你长得老也是我的错？\"');
INSERT INTO `tpshop_joke` VALUES ('3119', '刚才在路边听到一男人大喊，“你搞我马子，踢我对象，现在还特么想打炮，有车了不起啊！”这时旁边的男人默默说了一句：哥，咱还能好好下棋了么？…');
INSERT INTO `tpshop_joke` VALUES ('3120', '　　我的零花钱就像大姨妈一样，一个月来一次，一个星期就没有了……');
INSERT INTO `tpshop_joke` VALUES ('3121', '我觉得我变了……以前下雨，都死死护住书包。生怕淋着。现在呢，我去下雨了。抓起书包就顶在头上。');
INSERT INTO `tpshop_joke` VALUES ('3122', '‍‍‍‍我在网吧上网，外面下了大雨。 我的小伙伴说：“你爸来了。” 我就发现我爸拿着雨伞送来了，什么都没说就走了。 我和我的小伙伴惊呆了。‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3123', '老婆：亲爱的，你的年终奖呢？\r\n 老公：没有………\r\n 老婆：什么？没有？你敢不上交？\r\n 老公：不是，是老板没发年终奖……不过……\r\n 老婆：不过什么？\r\n 老公：给我们每个人发了一女人，可是不敢带回家呀！\r\n 老婆：女人在哪？\r\n 老公：公司呀，老板说公司女孩大把，想要的拿出真本事带一个回家！\r\n 老婆：老板，你今年给我那死鬼发了多少奖金？\r\n 老板：今年没有……\r\n 老婆：什么？你也帮我那死鬼瞒着？\r\n 老板：不是，是真的没有！今年效益不好……\r\n 老婆：鬼才信，效益不好，你还养那么多情人！\r\n 老公：哪有养呀？发不起年终奖就给他们每个人分一个了………');
INSERT INTO `tpshop_joke` VALUES ('3124', '老公平时都舍不得花钱，一天，老婆试探他：老公啊，后天就是你生日了，你准备怎么过？老公：哦，不就是一生日吗？有什么啊，怎么省钱就怎么过，明天就下一碗长寿面……老婆显得有点失望：老公，你说还有几个星期我也过生日了，你说我的生日怎么过？老公：老婆，你和我不一样，你的生日必须过好，怎么开心，怎么过……老婆一听很高兴：那你说，怎么样才是最开心的呢？老公：哦，很简单啊，省钱最开心……老婆：滚……');
INSERT INTO `tpshop_joke` VALUES ('3125', '一妹纸逛公园，一不小心踩了一脚狗屎，在公园草坪上急的直跺脚。这时，一青年男子见了后哈哈大笑起来。妹纸非常生气的说道：笑什么笑，有什么好笑的？人家踩了一脚狗屎，值得你这么幸灾乐祸的吗？男子指着自己的脚说道：我是笑我自己踩了那么一脚狗屎，不信你看。妹纸：你这人怎么踩了狗屎也这么高兴啊？男子：踩了狗屎是走狗屎运，咱俩同时都踩了狗屎，这说明咱俩有圆粪。妹纸：这明明是狗粪，怎么能叫缘分啊？男子：怎么不叫圆粪？那狗屎拉出来的一坨一坨的都是圆柱形的，屎就是粪，所以就叫圆粪。咱俩脚上都踩了粪，所以咱俩就有圆粪，难道我说的不对吗？妹纸：真尼妹恶心，滚……');
INSERT INTO `tpshop_joke` VALUES ('3126', '凤姐结婚后，喜欢偷偷查老公的手机。有一天神色诡秘地问老公：“老实交代，你的兄弟喜欢去的‘老地方’是哪里？”老公一脸坏笑：“我的‘兄弟’喜欢去的地方你能不知道？但是绝对不喜欢什么老的‘地方’！”');
INSERT INTO `tpshop_joke` VALUES ('3127', '有一个小偷村，这里的人把偷都当成了一种习惯，儿子：“老爸，咱家的牛不见了？”老爸：“没事，一会我就去偷一头回来！”儿子：“老爸，我们家的车也不见了？”老爸：“没关系，等一下我就去偷一辆回来！”儿子：“老爸，我妈呢？”老爸：“八成也是丢了，我得赶紧去偷回来一个！”');
INSERT INTO `tpshop_joke` VALUES ('3128', '嫦娥寂.寞难.耐，想来个浪漫一夜.情，于是找来了六个她看得上眼滴男人，没想到她提出滴一夜.情要求却被这六人都拒绝了。 第一人：“俺有河东狮，所以俺不愿！” 第二人：“俺有野.蛮女友，所以俺不愿！” 第三人：“俺有老公，所以俺不愿！” 第四人：“俺有娃娃，所以俺不愿！” 第五人：“俺有五姑娘，所以俺不愿！” 第六人：“俺啥也没有！” 嫦娥不满了：“既然你啥都没有，那你为啥不愿跟俺来个一夜.情？” 第六人：“正因为俺啥都没有啊！哦，对了，俺刚从大内总管滴职位上下岗！”');
INSERT INTO `tpshop_joke` VALUES ('3129', '物理课正在讲蒸汽机的原理。物理老师把一个在打瞌睡的同学叫醒，叫他站起来回答问题：“蒸汽机是靠什么来驱动的？”老师刚问完，只听见另一位同学放了一个超响的屁，老师很不高兴，对另一位同学说：“不要给他提示！”');
INSERT INTO `tpshop_joke` VALUES ('3130', '屌丝对女神说：听说你和你的男朋友分手了？女神：分手怎么了？跟你有关系吗？屌丝：哦，太好了，我有机会了……女神瞥了一眼屌丝：就算世界上只有你一个男人，我也不会给你机会的……屌丝：尼玛，你想什么呢，你都和他分手，我怎么没有机会？说着掏出电话打给女神男友：喂，亲爱的，我们去看电影吧……');
INSERT INTO `tpshop_joke` VALUES ('3131', '那天我们宿舍的全部在吃饭，只有一个兄弟在拉翔，我们吃的正津津有味。那吊毛来了一句：出来吧，皮卡丘！！！接着就听到噼里啪啦的声音。。。来来来，你出来，我们保证不打死你。');
INSERT INTO `tpshop_joke` VALUES ('3132', '一次老婆没空、让我给儿子开家长会，期间睡着了，这不是gc,更糗的是抬起头时教室里就剩我和班主任俩人了，，，那眼神，不敢直视啊，班主任看我醒来了，只说了一句话：怪不得你儿子也是一上课就睡着了。。。');
INSERT INTO `tpshop_joke` VALUES ('3133', '刚发生的事，本人超市经理一枚，男。超市的服务员都是女性。厕所是单人那种，今天在厕所一边大一边看段子正哈皮，有人拉门！（厕所有人门是拉不开的）。顿时一惊，本想说“有人”奈何平时在办公室习惯了！来了句“请进”，现在想死的心都有了，不敢下楼了！觉得整个超市的人都在笑我～～～');
INSERT INTO `tpshop_joke` VALUES ('3134', '刚才午休时间，同部门的同事们吃过饭坐在一起聊天。聊到晚上各种睡姿的问题。这时候部门女总监说自己晚上睡觉特不老实，很不文雅。旁边另一男同事马上补充道：是啊是啊，她睡觉喜欢抱着被子，把手脚都露出来，还踢被子。。。此刻，世界安静了，我们似乎都明白了什么。。');
INSERT INTO `tpshop_joke` VALUES ('3135', '开学不久，有一天傍晚女生楼下挤了好多人，音箱气球拉拉队，一看就是要举行火爆告白了。几个人正在地上点蜡烛，一哥们跑到前面喊：“XXX，我爱你！”结果楼上一妹子喊：“给老娘滚！”现场目瞪口呆，这哥们很尴尬地走到一个一脸黑线的帅哥前面说：“兄弟，不好意思，你们继续，我就是蹭个场……”');
INSERT INTO `tpshop_joke` VALUES ('3136', '哥今早来公司附近水果店买水果，旁边没车位，停在20多米远的地方，回来发现车后窗忘记关全开着，顿时大惊，急忙打开门查看丢了什么东西，少了个耳机，u盘和几包烟，却在满是脚印的后座下面捡到到苹果4黑色手机和一钱包，内装1500多元，顿时一加油门开溜，边想附近有没摄像头，小偷会不会报警呢!');
INSERT INTO `tpshop_joke` VALUES ('3137', '本人已婚男子一枚，刚才在办公室中一群同事闲聊。话题说到生二胎上，一女同事问另外个男同事：“不打算在生一个？”男同事：“不生了，太辛苦了！”我在旁边听到后，脑袋一抽就说：“生个孩子辛苦个啥，就个几分钟的事嘛？”。。于是，我就在全公司出名了！');
INSERT INTO `tpshop_joke` VALUES ('3138', '新买的车今天上牌，拍了个沪AV569G,越想越带感，这也太暴露我的性格了。');
INSERT INTO `tpshop_joke` VALUES ('3139', '有一天和朋友在一家餐厅吃饭，看到这样一段话：\r\n常和领导吃饭，升官是迟早的事！常和老板吃饭发财是迟早的事！\r\n常和美女吃饭上床是迟早的事！');
INSERT INTO `tpshop_joke` VALUES ('3140', '一男网友在QQ问问上提出一问题求高手解答！问题是，说最近恋爱了，想迎合老婆的网名，改个情侣名，老婆网名叫“ 红颜薄命”，求神人给我想个霸气名相配！紧跟着，两分钟不到有网友回答了“英雄气短”，更有人回答是“英年早逝”的！！！我艹。。。。');
INSERT INTO `tpshop_joke` VALUES ('3141', '姐夫太作总伤姐姐心，我一招让他吓破胆再也不敢再乱来文/徐霖讲述：温心姐夫是个开大车跑长途的，一年365天，他基本有200天在外面，姐姐一人在家，上有老要伺候，下有小要照顾，还得把心提在嗓子眼上担心姐夫的安危。要说姐夫这活，虽然辛苦，但也还算赚的，可姐姐实在不明白别人家同期开大车跑长途的，不是赚了个金山银山，但每家每户都不愁吃穿还住上了小洋房。可自家的日子并没有任何改观，相反姐姐拿不到姐夫半毛钱不说，还经常倒贴油钱给姐夫。这个疑问困扰了姐姐许多年，直到前久，一个跟姐夫一样开大车的人酒后吐真相，道出了原委，说姐夫这些年不是没有赚到钱，而是姐夫这么些全把找到的钱，花在外面一些女人身上了，说只要半路有女人来搭车，姐夫总会想法设法不管花多大本钱也要搞到手。姐姐听了越气越急，跟姐夫闹过几次，可没有什么用，姐夫点是表面认错，可却是顽固不改的那种类型。姐姐来找我说起这事，才说着就哭得不成样子。我替姐姐抹去泪，然后让姐姐不要着急，说我有办法制住他。这天，姐夫又要跑长途，我找了两个漂亮的妹子，说要搭顺风车回家，姐夫一看漂亮妹子，哪有不乐意之理，马上停下车让两个妹子上了车。上车不久到了一个乱葬岗前后，两个妹子实然说尿急，让姐夫停车。在姐夫停好车后，两个妹子就下了车，不过才走几脚其中一姑娘又返了回来，说太冷，问姐夫能不能把他的外衣脱下来给她披一披。说后还朝姐天抛了一个眉眼。姐夫看得心花怒放，马上脱下衣服递了过去，两个姑娘一去不复返，姐夫等呀等呀怎么也等不到，有些不放心，下了车去寻找。可是找呀找，怎么也找不到，就在他要失望而归时，却意外在一座坟上挂着他的衣服。呀，刚才搭车的莫非是鬼，姐夫吓得马上跑回了车。从那以后，姐夫再也不敢乱搞了，每月上缴的钱也多了起来。');
INSERT INTO `tpshop_joke` VALUES ('3142', '15～17的时候想长大了一定要娶个女神当老婆18～20的时候想找个身材好一点的，脸蛋也没那么大要求了21～23的时候只想找个女的，身材脸蛋都不介意了！25～30的时候想想其实找个男的也认了！！！！！35～40的时候我TM就只要找活的就行了，管TM什么种族');
INSERT INTO `tpshop_joke` VALUES ('3143', '师傅 我要是要跟你睡！为什么？？俗话说 要想学的会！就得陪师傅睡！你走开！不走话！ 我叫我老公了啊');
INSERT INTO `tpshop_joke` VALUES ('3144', '刚刚，就在刚刚，本人在公共卫生间蹲坑，有人敲门，一排敲遍了没人来，然后他就进了我旁边的那个……对不到三秒就听见:噗通噗通的声音，然后紧接着就是一句卧槽。因为那个便池堵塞了里边都是水……哈哈');
INSERT INTO `tpshop_joke` VALUES ('3145', '一哥们儿三十了还单身……一次喝酒问他喜欢什么样的，哥们儿一口干掉杯中酒长叹一声：大学时想找个孝顺对我好我喜欢漂亮的高的白的处女，刚上班时想找个对我好漂亮的处女，后来只想找个对我好的处女，现在我仍然坚持我的底线—处女…旁边一哥们儿幽幽道：这就是你脱下警服去当幼师的原因吗？……');
INSERT INTO `tpshop_joke` VALUES ('3146', '今天老婆穿的很性感，老婆问老公：老公，你看我性感吗？老公说；性感。老婆又问；那你觉得我有沟吗？老公瞧了瞧，说：有。老婆高兴的说：是吗？那在你眼里我的勾有多深？老公说：如果别人是用东非大裂谷来形容的话……老婆急忙道：那我是哪道裂谷？老公说：后院小狗刨的那道');
INSERT INTO `tpshop_joke` VALUES ('3147', '今一大早上班，同科室大哥说，“你看我儿子给我新买的腰带”！我说，这孩子真孝顺。孝顺个屁，他是让老子勒紧裤腰带，给他省钱买房结婚。我…………');
INSERT INTO `tpshop_joke` VALUES ('3148', '今天早上，二货闺蜜在给办公桌上自己的花浇水，我看了看办公桌上我盆栽里枯萎的植物，抬起头萌萌的说：“这么有爱心，顺便帮我也浇一下呗。”这货说：“好埃”下一秒，我已是满脸的水。。不说了。。这货还在急救室呢。。');
INSERT INTO `tpshop_joke` VALUES ('3149', '昨天和同事在工地聊天，说起这个工人违规怎么办的问题，机智如我来了一句：“睁一只眼瞎一只眼就行了”！我特么说完感觉怪怪的。。');
INSERT INTO `tpshop_joke` VALUES ('3150', '记得当年初中语文考试，考题中有朱自清的《背影》。同桌偷偷问我，第xx题那个“他”指谁啊？我正忙着做题，就随意地说，是朱自清他老头（客家话老头就是老爸之意）。结果那家伙就直接写“朱自清他老头”交了上去。老师评讲试卷的时候我同桌凭此一举成名！事后他特么还要揍我，说是我让他那么写的。');
INSERT INTO `tpshop_joke` VALUES ('3151', '告诫下一届的同学们。撕书的时候要选自己经常翻的那些书，而一些好久没翻的书最好先翻一翻。因为里面可能有藏着你忘了的钱。问我为什么知道？呵呵，哥撕的不是书，是特么的青春的痛！');
INSERT INTO `tpshop_joke` VALUES ('3152', '‍‍儿子和爸爸看电视，忽然男主角跪下向女主角求婚。 儿子很好奇的问爸爸：“爸爸，你向妈妈求婚的时候有没有跪下？” 爸爸：“没有。” 儿子：“为什么？” 爸爸：“你妈说以后跪的机会多的是，这次就免了。”‍‍');
INSERT INTO `tpshop_joke` VALUES ('3153', '‍‍‍‍一个朋友在日企上班，他一脸愁容的对我说：“前两天宣布九月三十号全国放假！你知道吗？” 我说：“知道啊！怎么了！” 这二货说：“呜呜，这让我如何皇军汇报呢！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3154', '这是个令人尴尬的年龄，谈爱己老，谈死太早。和年轻人一起谈经历太幼稚，和老年人一起谈人情世故不好，出去疯狂又怕吵。任性说你扮成熟，沉默说你装清高，时尚说你有点妖，朴素说你有点老。觉得上气不接下气了，刚想消极下，回头一看，上面有老，下还有小！');
INSERT INTO `tpshop_joke` VALUES ('3155', '高中那会儿学校属于封闭式的，lz每晚都跳墙出去玩，但保安从来抓不到，在跳墙界颇有威望。某日，刚爬到墙头，发现身上黏呼呼，臭熏熏，尼玛！墙上被抹上大粪了！！！这不是高.潮，等我跳下去之后，七八根手电筒啪全照到我脸上，至今不忘保安队长特别兴奋的说，终于抓到你了！！！第二天便成为名人了。真事。');
INSERT INTO `tpshop_joke` VALUES ('3156', 'lz妹子，再外打工，一直和表姐一起合租房，今晚她的男朋友过来了，到了晚上因为宾馆离的远，没办法，只能将就睡一夜，好人做到底，lz打地铺，半夜你们以为我睡着了，xxoo没关系，可你们玩姿势，床沿后进势踩着我的脚就是你们的不对了，那可别怪你们以为睡着了的我笑出声来了，，，，！');
INSERT INTO `tpshop_joke` VALUES ('3157', '跟朋友合租，昨天出去玩看见他女朋友劈腿跟我各种求就差没跪地上了让LZ别告诉朋友！刚刚一回来朋友就让我这个星期搬出去住，问了半天才知道那婊子说我偷看她换衣服现在说什么朋友也不信了！各种艹泥马，婊子就是婊子……');
INSERT INTO `tpshop_joke` VALUES ('3158', '就刚刚，我骑单车送女神回到她住房，她叫我去她那坐坐，今天没有人。我就随口说我单车没锁，就不去了。我是不是错过了什么？想不通');
INSERT INTO `tpshop_joke` VALUES ('3159', '本人药店营业员妹纸一枚，每天都会遇见一些奇葩，有次来一个四十多岁的大叔，指着前面的TT,问我是什么东西，我说避孕套，然后他问是男的用的女的用的，我忍，回答男的用的，他又问女的能用吗，本着职业精神，再忍，尼玛，他又问这个怎么用，你给我讲解讲解，本人实习生一枚，我再忍，里面有说明书自己看，末了店长实在看不下去，上来就说你想问什么，我给你讲，店长女汉子一个，末了那人瞪了店长一眼走了。尼玛，你是进来买药的还是耍流氓的');
INSERT INTO `tpshop_joke` VALUES ('3160', '小时候家里养有小鸡，一日戏耍，不小心踩死一只，被老妈猛揍，边揍边吼：“怎么踩死的，啊，怎么踩死的？”天真的我一边抹眼泪一边又抓了只小鸡过来，然后对我妈说：“就是这样踩死的。”然后“咔”一下把它踩死了。那顿揍真是惨绝人寰啊，说多了都是泪！！！');
INSERT INTO `tpshop_joke` VALUES ('3161', '世人大多以为，最美的一句情话是“我爱你”，但这句话终究是以“我”为主体的。私认为最美的一句在红楼里，纵观整部红楼，宝玉没有对黛玉说过一次“我爱你”，他只说过“你放心”。');
INSERT INTO `tpshop_joke` VALUES ('3162', '有个人因抽烟得病死了，来到天上见到上帝。他说：“上帝，我是那么虔诚地爱您，怎么我抽烟得病这种事，您一点都不给我提示呢？”上帝说：“我给了你多少次提示啊，你没发现你经常抽烟找不到打火机吗？”');
INSERT INTO `tpshop_joke` VALUES ('3163', '1.唐僧西天取经途中，遇一妖女。妖女曰：为何西行？僧曰：取经。妖曰：小女有经，何必西行。僧大喜：经在何处？妖抚私处：在此。僧上前细察曰：操，月经');
INSERT INTO `tpshop_joke` VALUES ('3164', '如果女人是一根香烟，那么我就是一根火柴，高富帅就是打火机。一根火柴只为一根香烟点燃，一个打火机却可以点燃一根又一根香烟。。');
INSERT INTO `tpshop_joke` VALUES ('3165', '　　一次，公司组织大扫除，公司的一妹子站住桌子上擦玻璃，但是下面的就擦不到。 　　这时刚好一个男同事小王路过，于是妹子大喊：“唉，小王过来帮我擦擦我下面！” 　　小王顿时就是一惊：“哪？” 　　这女同事大喊：“哎呀，就是擦我的下面啊！” 　　办公室鸦雀无声，然后哄堂大笑！哎呦，大家竟然都邪恶了，都是没节操的银啊！');
INSERT INTO `tpshop_joke` VALUES ('3166', '路遇一戴口罩女生、那身材……忍不住唱一句：掀起你的盖头来,让我来看看你的脸。女生挺大方摘下口罩。我倒不好意思了,偷瞅了一眼,就一眼我立马换歌：妹妹你大胆的往前走哇、往前走、莫回啊头……');
INSERT INTO `tpshop_joke` VALUES ('3167', 'TOP8 高中时同桌，他特别爱睡，一上课就睡觉，有一次我突然摇醒他告诉他快跑快跑着火了，当时他的反应我一辈子都忘不了，裤子一脱在全班人眼前尿了一泼尿在衣服上，披着衣服就他妈往外冲，还不停的说卧槽卧槽。。。');
INSERT INTO `tpshop_joke` VALUES ('3168', '今天和一妹子去宾馆，ooxx的时候突然觉得有什么不对。妹子见我停下来问怎么了，我反问现在几点了，妹子抬头看表答刚过十二点。“草老子昨儿首胜没拿！”');
INSERT INTO `tpshop_joke` VALUES ('3169', '：唧唧复唧唧，木兰开飞机，开的什么机？波音747！ 问女何所思，问女何所忆．女亦有所思，没钱买飞机．昨夜见军帖，要用轰炸机，飞机十二架，架架买不起．阿爷无大钱，木兰无金银，愿去买钢铁，从此造飞机． 东市买图纸，西市买螺丝，南市买玻璃，北市买铁皮．旦辞爷娘去，暮宿旧机库，不闻爷娘唤女声，但闻铁皮摩擦滋啦啦．旦辞机库去，暮至军营旁，不闻爷娘唤女声，但闻将军大呼哈哈哈． 万里开飞机，关山一下没．热气传机翼，日光照玻璃．将军被吓死，壮士魂已飞． 飞来撞天子，天子躺病床．策勋十二转，赏赐俩耳光．可汗问所欲，木兰不愿进牢房；愿开747,飞着回故乡. 爹娘闻女来,端起机关枪;阿姊闻妹来,当户举手枪;小弟闻姊来,举枪噌噌推上膛.开我机舱门,进我飞机舱，脱我战时袍,换上飞行装,多装手榴弹,对外架机枪.出门埋炸弹,亲友皆惊忙:离别十二年,不知木兰变态狂. 疯子脚蹬地,呆子眼紧闭,两人并排走,谁能说我不正常');
INSERT INTO `tpshop_joke` VALUES ('3170', '清明缅怀先烈，一小女孩突然问母亲到:“妈妈，什么是党员？”冥思如果是我怎么向孩子解释，是从我国国情？还是政策？趋势……只见那妈妈很淡然的说了一句:“角色游戏中一个级别满后，存储装备……向另一个角色晋级，是少先队员的最高级！具有超强战斗力”……瞬间涨知识了！ (๑ʘ̅ д ʘ̅๑)!!!');
INSERT INTO `tpshop_joke` VALUES ('3171', '男子想和女友亲密，女友曰：“不洗澡不行！”不过因为天冷，女友答应只洗洗“局部”就可以了。男子洗后，女友发现男子的嘴是湿的。女友极为娇羞道：“亲爱的，你好好懒，居然用嘴洗那里啊！”男子听完狂晕道：“偶就是刷了个牙啊！！！”');
INSERT INTO `tpshop_joke` VALUES ('3172', '‍‍‍‍‍‍外甥拿家里电话瞎拨，舅舅家。 舅舅：“干嘛呢？” 外甥：“打电话呢？你傻啊，看不出了啊？” 舅舅：“你找揍啊？别给我拨到美国去。” 外甥：“喂，美国吗？快来抓我舅舅，他要打人了。”‍‍‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3173', '老师：你这孩子咋这么气人！连这种简单的题都不会做！我快被你气死了，你知道猪是咋死的吗？ 熊孩子：猪是咋死的？是气死的吗？ 老师：你给我滚出去 ！');
INSERT INTO `tpshop_joke` VALUES ('3174', '‍‍‍‍讲一个自己的真事，一天老公准备去超市买东西，楼下是个广场，绕过广场才能坐车。 然后看见一个腿残疾的老头在那跪着要钱，正好有零钱就给了他一块。 过一会我们上车，听见后边有人喊等一下，回头一看是刚才的老头。 他的腿没事，郁闷啊，这不是高潮，高潮是司机说：“李老头，你家儿子没开宝马接你啊。” 并且我眼睁睁看着他没投币就上车了，免费乘车，和司机是熟人！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3175', '“军师，如今敌人势大，我方势力不断受损，如之奈何？”“这还不简单，主公来，跟我做，保护视力，预防近视，眼保健操开始！”');
INSERT INTO `tpshop_joke` VALUES ('3176', '王傲儿：我未来的儿子, 我允许你打耳洞, 允许你泡姑娘, 允许你把儿媳妇带回家, 允许你跟别人打架, 允许你顶撞老师, 但是儿媳妇必须长得漂亮, 打架必须赢, 成绩必须好, 自己的女人必须知道怎么去保护, 我亲爱的儿子, 你爸这么努力就是为了以后你睡了别的姑娘, 人家跟咱要五万, 我可以把十万扔她脸上告诉你, 儿子, 再睡她一次！');
INSERT INTO `tpshop_joke` VALUES ('3177', '‍‍‍‍今天在楼道里偶遇一个女神级别的妹子。 我只是看了她一眼，她竟然羞愧的低头含笑从我身边过去了！ 哎呦，什么个情况？哈 哈，我不就昨天剪了个头吗？ 看来比以前帅了很多吗！ 直到我看到从我身边过去的另一个带把的，妈蛋，我才知道我TM想多了！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3178', '‍‍‍‍‍‍某天，外公将手机忘放在桌上，出门，回家后发现手机不见了。 连忙问表弟：“xx，爷爷的手机在哪里啊？” 表弟满怀着笑脸对外公说：“爷爷，我看你手机很脏，帮你拿去洗了。” 然后将手机拿回给外公。 外公他老人家看着滴着水的手机没话说。‍‍‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3179', '那天早晨，媳妇叫我去买五个韭菜包子，我当时走太急，拍到队的时候，说成了买九个五菜包子，我真想拿根面条上吊，头撞豆腐上，拿薯片割脉');
INSERT INTO `tpshop_joke` VALUES ('3180', '‍‍‍‍好色男得绝症英年早逝，阎王发现他前世行善无数，答应赐他一心愿。 他求阎王：“来世让俺投生在美女最多的地方并且让所有美女都属于俺一个人吧！” 于是，好色男来世投胎成了女儿国的女王！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3181', '中午下班回家，我问炒的什么菜。我姐抢先说：“豆角炒肉。”我看了满盘子的豆角问：“肉呢？”老姐说：“在豆角里面，你拣有虫窟隆眼儿的吃。”女汉纸啊~~');
INSERT INTO `tpshop_joke` VALUES ('3182', '听爸爸说，在他刚结婚时候家里养了一黑狗，后来不知道为什么全县开始打狗，所有大狗都要弄死了，挨家挨户检查，到我家时候，我奶奶跟狗说，别出来，躲到屋子里面，别出声，要不我也救不了你，后来他真的一声没出， 躲过一劫，再后来，全村都是他的后代。。');
INSERT INTO `tpshop_joke` VALUES ('3183', '警拦住卡车司机，欲捞点油水。 无奈既不违章又无超载，而且证件齐全。 交警：“倒一下车，看看倒车灯亮不亮！” 司机上车，打火，挂倒档。 倒了几米后停车，熄火，下车，一气呵成。 对交警说：“没问题吧，灯没坏！” 交警：“灯是没坏，开车不系安全带，罚款100！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('3184', '有些事发生一次可能毁掉一生，我是卖保险的，我有个女同事，一次去一个比较偏僻的公司给投保人签发保单…坐电梯时，遇到三个混混青年，电梯故障，困了他们半个小时，女同事说到这里泣不成声：妈的，那三个贱人，只看了我一眼就围在一起打了半个钟头的斗地主……');
INSERT INTO `tpshop_joke` VALUES ('3185', '　　老婆：老公，你喜欢俺瘦还是胖？ 　　老公：俺喜欢你胖，所以老婆你千万别减肥啊！ 　　老公心说：特么你胖才好省时装钱啊！ 　　老婆：老公你真好，那去给俺买一百盒德芙吧！俺不用忌口啦！ 　　老公泪奔：德芙特么更贵啊……');
INSERT INTO `tpshop_joke` VALUES ('3186', '我实习护士一枚。昨天拿一个病人的小便去化验，不小心倒地上了，心虚地去厕所尿了回去，今天检查报告上写着白带异常，医生想了半天都没明白，一个男病人怎么会有这病呢？我有些脸红红的看着报告不敢吭声…');
INSERT INTO `tpshop_joke` VALUES ('3187', '有一次手机坏了，下午手机修好刚开机，一个陌生的号码就打进来了，一开口就是“你电话咋一直关机？！我都打了一个下午了！你干啥去了？一个小姑娘在外地上学都不知道随时保持电话顺畅吗？！你知道你爸妈会有多担心吗？”我弱弱的问了一句“你是谁啊....”那边中气十足地说，我是中通快递，北门取快递快点儿！');
INSERT INTO `tpshop_joke` VALUES ('3188', '　　早上上WC，掏纸的时候没注意10块钱掉在马桶里了。 　　我那就纠结啊，到底要不要捡呢？ 　　我室友看到了我的纠结，二话不说，抢过我的钱包，拿出一张100的扔了进去！ 　　然后说：哥们，你知道你为什么犹豫不决吗？那是因为你失去的还不够多！看什么看，赶紧捞吧！');
INSERT INTO `tpshop_joke` VALUES ('3189', '有个同事姓王，特别爱吃，也很会吃。有次单位几个人去他家吃凉面，吃过的都知道一般都要放蒜入味，老王手艺不错，大家都吃的很嗨，最后吃完的时候，老王抱怨说，你们吃的嗨，我可费老劲了，捣蒜没找到蒜臼，我一口一口给你们嚼碎的，现在嘴还发麻。。。。');
INSERT INTO `tpshop_joke` VALUES ('3190', '最近老婆常生气，生气后就特爱买包，我就说：你们女人为什么买那么多包，这是病，得治。老婆答：“包—治—百—病！”（是谁发明这个词的，你出来，我不打你）');
INSERT INTO `tpshop_joke` VALUES ('3191', '1、“爸，冰箱里的鸡腿呢？”“你妈炸了。”我。。。2、晚饭后陪老妈逛街。。看到别人母子。老妈凑过来说：“你看那个妈妈好像也在遛她们家的单身狗。”3、我问我爸有多爱我？我爸说以前为了让你改正错误，特地请了半天假回来打你！4、昨天老妈告诉我，下班时去超市再给她买个苍蝇拍。我问：“家里不是有一个苍蝇拍吗，还买？”老妈说：“现在的苍蝇胆子也太大了，居然经常趴在苍蝇拍上玩。我一拿苍蝇拍它就飞跑了，一会儿见我放下苍蝇拍，它又飞回来趴在上面，真是气死人。你给我再买一个苍蝇拍，一个放在那里诱敌深入，一个拿在手里主动出击，看它厉害还是我厉害。”5、给儿子过生日。儿子吹灭了生日蜡烛，对我说：“爸，我能许个愿不？”“什么愿望”“明年生日能在蜡烛下面放个蛋糕吗？”“说出来就不灵了。”“。。。。”6、今天去看了大圣归来。 我旁边有个小孩儿问他妈妈 “这个不是动画片么？为什么有这么多大人来看？”他妈妈回答： “因为他们一直在等大圣归来啊，等啊等啊，就长大了。”');
INSERT INTO `tpshop_joke` VALUES ('3192', '儿子：妈妈，你是Woman，年纪大一点的女的是Woman！！！\r\n妈妈：年纪小的呢？\r\n儿子：年纪小的是Girl！\r\n“那我是Girl，我年轻，，我不要做Woman”\r\n“不行的，你已经有一点年纪大，还有两根白头发，你就是Woman”\r\n“两根不算”\r\n“那你有三根，我看错了，我真的看到你有三根”\r\n很想是Girl的妈妈抓狂…一定要是Girl！！\r\n最后，没办法…\r\n“妈妈，可是我觉得Woman是最漂亮的，真的最好看”\r\n“真的，那好吧，我是Woman”\r\n“嗯，那说好了，你是Woman，不可以变了！”');
INSERT INTO `tpshop_joke` VALUES ('3193', '最近在六 间房看直 播，飞仔 直播摸他徒弟 悦宝 的奶子，房间被封了，只准进2000个人。\r\n 六 间房的管理员太不人性化了，凡是进 飞仔 房间的都是冲 悦宝 那对大咪 咪去滴，咪 咪不让抖还咋活啊！');
INSERT INTO `tpshop_joke` VALUES ('3194', '昨天晚上宿友对我说:这么晚了还不睡？你知道嘛，睡前玩手机，影响性生活！当时我就一拖鞋扔过去了，我TM有性生活还玩手机？');
INSERT INTO `tpshop_joke` VALUES ('3195', '“莪”这个字，在我初中的时候，很多女生都喜欢用，打字聊天的年代“喜欢莪吗？”“你爱不爱莪？”等等……现在回想起来，我当时真的是太年轻了！');
INSERT INTO `tpshop_joke` VALUES ('3196', '小李去女神的空间闲逛，见一相册名叫「我的裸照」，密码提问：叫声妈就给你看。他猥琐地敲出「妈」字，居然解密了，打开一刹那，小李泪流满面，里面赫然一个大字：乖！');
INSERT INTO `tpshop_joke` VALUES ('3197', '丈夫：我要离婚，我们的婚姻一点儿也不公平。 妻子：你脑子有毛病吧，哪里不公平了？ 丈夫：结婚以后，我把工资，房子，车子所有的东西都给你了。 妻子：但是我也把做饭，扫地，买菜交给你了啊。。。');
INSERT INTO `tpshop_joke` VALUES ('3198', '在初中的时候，暗恋同桌的女生很久了，一直不敢表白，初三期末，在最后一天喝了口高粱壮壮胆，写了张表白字条给她，她看到后也写了一张给我，然后说了一句想清楚了再来找我。（字条上的字“氼卋婄卋挋蚮卌犿仴”）然后我凌乱了');
INSERT INTO `tpshop_joke` VALUES ('3199', '和前女友分开三年了，当初是她甩的我和别人好上了，现在频繁访问我空间，到底什么意思！');
INSERT INTO `tpshop_joke` VALUES ('3200', '我被媳妇赶出家门了，原因是老婆一瓶水（化妆品）用了几个月都没用完，今天我在卫生间往里面装水的时候被发现了。');
INSERT INTO `tpshop_joke` VALUES ('3201', '　　一天问老妈要钱，“妈，给我50元好不好？”妈：“什么？40元？你要30元干什么？我没有20元，给你10元和你妹妹一起分。”');
INSERT INTO `tpshop_joke` VALUES ('3202', '屌丝上学的时候，有时食堂的饭实在不想吃了，就去学校外面的食品一条街去逛一逛，有烤鸭、烧鸡等各种好吃的，馋的直流口水，于是食欲大增，然后屌丝欣然回学校食堂买两个馒头半份菜。。。');
INSERT INTO `tpshop_joke` VALUES ('3203', '“朋友，如果你今天不开心，我告诉你：三十九年前的今天，苹果联合创始人韦恩把他那份 10%的股份以八百元卖给了乔布斯。今天这部分股权价值 580 亿美元。 如果还想不开，再告诉你，卖掉股票的韦恩还活着，而拥有巨大财富的乔布斯却走了 。 这就是生活，无所谓输赢。”?');
INSERT INTO `tpshop_joke` VALUES ('3204', '一男子借钱后还给债主一堆大便，自称视钱财如粪土。');
INSERT INTO `tpshop_joke` VALUES ('3205', '男：“火车票我买好了！” 女：“几张？” 男：“两张！” 女：“算你还有良心！” 男：“一张是去的，一张是回来的！” 女：“你妹，滚。。。”');
INSERT INTO `tpshop_joke` VALUES ('3206', '-　想起我练车的时候了，每次靠边停车总是把车停歪，每次教练都狂骂我一通。 越被骂我越紧张，又把车停歪了，转过头看看教练，教练一语不发，也看着我。 我怯怯的问：“我又把车停歪了是吧？” 教练满腔怒火地说：“哪能啊，是他娘的路修歪了！”');
INSERT INTO `tpshop_joke` VALUES ('3207', '今天上课时我说:老师你觉得世界上的事是不是绝对的。老师:世界上没有什么事是绝对的。我:可老师我觉得一件事是绝对的，老师:什么事。我:老师你绝对不是我女儿的。老师:滚出去，抄课本50遍。难道我说什么了吗？');
INSERT INTO `tpshop_joke` VALUES ('3208', '上初中的时候，我们教室在五楼，我是特老实巴交那种人，那时候班上有个比较厉害的人（就是班上的老大），老是欺负我，老是要我下楼去给他打水喝。终于有一天我爆发了，打水的时候给他加点尿或者口水什么的，从那天起，我每次都是主动去给他打水。。。。');
INSERT INTO `tpshop_joke` VALUES ('3209', '　　大学同学聚会，主要是富人在一起吹牛逼；而像我这种穷人，连聚会通知都收不到，因为手机欠费。大家都很装，有人在打电话谈生意，还有人在用笔记本交易股票，为了让我自己也显得工作很忙，我不停的给他们端茶倒水上菜。');
INSERT INTO `tpshop_joke` VALUES ('3210', '某天上课，老师说学习要从娃娃抓起，你们小时候肯定没努力。然后，我们班一个男生不乐意了。老师，我是从胎教就开始的。');
INSERT INTO `tpshop_joke` VALUES ('3211', '‍‍路上听到一个小女孩说：“你究竟爱我不！” 小男生：“我妈妈每天给我3元，我都给你2元5买零食吃，你说我爱你么？”‍‍');
INSERT INTO `tpshop_joke` VALUES ('3212', '刚出去遛哈士奇，前面有个男孩在淘裤袋找东西，结果他的钱包掉下来，人家还没来得及捡起来，我家二货百米冲刺的奔过去叼起钱包，扭头就往回跑，放在我的脚下，嘴吐舌头狂摆尾巴一脸求奖励的表情。男孩子呆了看着我……');
INSERT INTO `tpshop_joke` VALUES ('3213', '哥儿们出去喝酒，一会的功夫，他儿子用钢丝球把他爱车擦了个遍，为的是暑假一篇作文，帮爸爸干家务…… 现在这小子正鬼哭狼嚎呢！哈哈哈……');
INSERT INTO `tpshop_joke` VALUES ('3214', '‍‍老婆：“老公，现在晚上外面好危险呢！你能不能到我下班的时候来接我啊！” 老公：“没事，带瓶矿泉水保你一路平安无事。” 老婆：“什么意思呀？” 老公：“如果遇到危险洗把脸。” 老婆：“………”‍‍');
INSERT INTO `tpshop_joke` VALUES ('3215', '‍‍今天与一哥们出去吃饭！ 饭吃完了，哥们突然问我：“你知道什么是无能为力的感觉吗？” 我愣了一下，说：“不知道！” 哥们说：“就是当你吃完东西！牙缝里有东西！你的舌头知道在哪里！可是你的手怎么也拿不出来！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('3216', '单身就是好，可以喜欢这个女生的同时，又喜欢另外一个女生，反正没女朋友管着。');
INSERT INTO `tpshop_joke` VALUES ('3217', '我送领导的老婆回家，经过路口的时候看见交警，于是慌忙找安全带，不小心碰到领导的老婆的腿了，她问我:你在找什么？我一慌，说成了:安全套。她楞了一秒，说:我就喜欢你这样直接的，回家说! ～先让我冷静一下');
INSERT INTO `tpshop_joke` VALUES ('3218', '家里儿子晚上睡的晚，刚才媳妇怎么哄都不睡，结果一顿胖揍，老实的躺床上了，我心想进去看看，给盖下被子，他却睁开眼说：爸，你那老娘们该修理修理了，今天打的是我，下次就不一定是我了。。');
INSERT INTO `tpshop_joke` VALUES ('3219', '今天静静突然问我:亲爱的，你以后会喜欢上别的女孩吗？当时就想逗她开心说:未来的事我也说不准，有可能会喜欢上一个女孩，那女孩会亲切的称你为妈妈。突然我脸上响起啪的一声，静静便说:畜牲，你连咱儿媳妇都下的去手。 。。');
INSERT INTO `tpshop_joke` VALUES ('3220', '寝室一兄弟最近开始疯狂练习转笔，问他为什么，他双目无神，淡淡道：没有一鸡之长，总得有一技之长啊，把手指头练灵活点，不然以后怎么找老婆。唉～～。听完他的话，我陷入沉思，而后去买了一支口琴。');
INSERT INTO `tpshop_joke` VALUES ('3221', '一次去同事家玩,他家的熊孩子正在看西游记,然后那只熊孩子忽然跑过来问我,能不能叫他一声孙悟空,我说干嘛,他说你叫了你就知道了,我叫了,他说:“爷爷在此”');
INSERT INTO `tpshop_joke` VALUES ('3222', '‍‍‍‍有一次本姑娘被困在火场，心想要是又谁能把我就出去，我就嫁给谁。 后来真有个年轻的消防小伙子把我背出了火海。 于是我对他说：“谢谢你救了我，我愿意以身相许！” 谁知他说：“唉，我当时拼死把你救出来，还以为你是个煤气罐！” 我：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3223', '早上坐地铁，我旁边坐着一位秃顶的大叔。此时，一位妈妈带着3岁的孩子上了车，坐在我和大叔的对面。孩子盯着大叔看，然后兴奋的对他妈妈说“妈妈，这里有个兔子”，我正纳闷，只见妈妈看了一眼大叔，认真的纠正说“孩子，这是秃子，不是兔子，发第一声”，我瞄了下大叔的脸，一会白一会绿，心疼大叔啊');
INSERT INTO `tpshop_joke` VALUES ('3224', '‍‍‍‍晚上抱着老婆正睡的香，梦中和一哥们打架。 突然叶问附体，迅雷不及掩耳盗铃之势连出两拳，拳拳打脸。 结果一睁眼结结实实打老婆脸上了…… 结果你们懂的，半夜两点多开始，老婆报仇拳打脚踢，还掐肉……‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3225', 'A同学问B同学:什么是中国全能人才？B同学:北大青鸟学习电脑，山东蓝翔学习挖掘机，新东方学习烹饪，都毕业了，就可以用电脑指挥挖掘机炒菜了！');
INSERT INTO `tpshop_joke` VALUES ('3226', '‍‍‍‍有一天我出去遛狗，突然有一个黑衣男子出现，框框把我的狗给打死了！ 我说：“你疯了，有病吧！干嘛打死我的狗！” 然后那个男子说：“哼哼，没办法，有人出100万取你的狗命！” 我：“……”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3227', '‍‍‍‍一朋友对我说：“你知道我女盆友有多好吗？” 我：“你有女票了？” 他说：“恩，她还很善良！” 我：“哦，那恭喜了，她有多善良？” 谁料他说：“她知道我没钱养不起她，她一直没有出现！” 我：“………”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3228', '给老婆买了一条情趣内裤，中间开岔那种，老婆很喜欢，经常穿，洗完就在阳台上晾着， 一天下班回家，只见丈母娘来了，坐在阳台上正在缝情趣内裤的开岔， 我忙招呼她“妈，你啥时候来的？” 丈母娘白我一眼，没理我，低头边缝内裤边抹眼泪“俺闺女跟你没过上好日子，内裤破成这样还舍不得扔…”');
INSERT INTO `tpshop_joke` VALUES ('3229', '男生宿舍确实脏乱差，不洗袜子的不在少数，慢慢发现不洗袜子有如下几个境界，一是袜子穿硬了可以立起来，二是袜子扔出去可以黏墙上，最猛的是隔壁一个哥，放假前没洗袜子收假回来居然里面长了一个蘑菇。后来他在学校火了，被尊称无土栽培哥。');
INSERT INTO `tpshop_joke` VALUES ('3230', '儿子：“爸爸，什么叫做女汉子” 爸爸：“你觉得呢？” 儿子：“是那些骂人抽烟喝酒打架的女孩吗？” 爸爸：“不是，女汉子是那些自理能力强，会洗衣做饭，照顾人时温文尔雅，处理事时性格像男生，不斤斤计较，没那么多事的女孩。” 儿子：“那我说的那些叫什么呀？”爸爸：“孩子，那些叫傻B！”');
INSERT INTO `tpshop_joke` VALUES ('3231', '　　同学之间是最会闹笑话的。开玩笑也不分场合、轻重。依稀记得初中三年级的时候，一个晚自习，前桌的一个心仪男生突然转身对我说：“10年后我一定会回来娶你。”我一听，暗自窃喜，因为我已经暗恋他许久了、、但当众被人家这样表白，我还是脸爆红，尽管是我比较喜欢他，但没想到他会在大庭广众之下这么说、、、正在我心花怒放之时，没想到这家伙竟然接着说，“回来取你狗命、、哈哈”');
INSERT INTO `tpshop_joke` VALUES ('3232', '老公用笔记本看地道战， 我过来看见屏幕黑黑的 。我问:咋这么黑 ，啥也看不见 ，是不是黑屏了？老公回:在地道里黑，没光。只有声音，我俩就看着黑黑的屏幕， 听着声音 ，许久 我动了下鼠标 屏幕亮了…… 亮了…… 亮 ……了…… 醉了 我可是看这黑屏许久……许久呢……');
INSERT INTO `tpshop_joke` VALUES ('3233', '　　那天去市里乘坐公交！上车后发现没零钱！于是掏出10元钱，问司机下车再给可以吗？司机师傅说你把10元投里面吧！等下有人上车钱你收！收到八元为止！见此司机好说话，我也很感激。只注意回收自己的8元钱，结果没想到，最后钱没收够（只收了6元）不说，还他。妈。的。座过站了！现在想想真是不值啊、、、、');
INSERT INTO `tpshop_joke` VALUES ('3234', '　　冬天到了，手和嘴巴不抹点凡士林油膏会被冻裂。所以我经常在办公室边上网工作边擦唇膏。有一天不小心没拿住，正好那天天穿的是很宽松的背带裤，于是凡士林油膏掉进裤裆里了，正在我从前开门往外拿出凡士林油膏时。不巧的是被突然推门进来的老板看见了。于是老板很惊诧地看见我从裤裆部位拿出一支长度和粗细都很值得怀疑的小棒子。我当时很囧的愣在那里。、、、');
INSERT INTO `tpshop_joke` VALUES ('3235', '　　小王陪太太去商场，在化妆品专柜太太相中了一套国外化妆品，但觉得价格有点贵，正在犹豫不决时，售货员说了一句话，她马上就买下了。售货员说：“太太，你现在不买，过两年你先生就领别人来买了！”');
INSERT INTO `tpshop_joke` VALUES ('3236', '难过的不是生活有多苦，而是那个曾经说要陪自己一辈子的人，却早已在中途退了场，我怕的不是你对我有多冷漠，而是我在这边努力加油的时候，你却在那边偷偷往后退、');
INSERT INTO `tpshop_joke` VALUES ('3237', '正上课，美女英语老师突然丢下粉笔出去了，正议论为何她如此匆忙，接下来教室传来噼里啪啦屎屁大作的声音，原来英语老师忘记关别在腰间的麦，只留下我们在教室听着醉生梦死的 ……');
INSERT INTO `tpshop_joke` VALUES ('3238', '有次出差提前回家就没告诉老婆想给他个惊喜，轻轻的打开门看到老婆在和其他男人在床上鬼混，轻轻的退了出来，给她老爸老妈打电话“你女儿在家服毒了，快不行了！”然后点根烟，在门口等着，二十分钟后两辆大巴拉满了七大姑八大姨的气势汹汹的推开了门，然后，然后，看着就行了！');
INSERT INTO `tpshop_joke` VALUES ('3239', '　　一夫妻周末呆在家。老婆想老公陪自己出去逛街，老公死活呆在家里不去。老婆想方设法劝说：“要是我自己在逛街时，被人劫财了该怎么办呢？”老公听了偷笑，说：“还好你说劫财......没说劫色，不然我笑了！”老婆听了，说：“NTM的。。。”');
INSERT INTO `tpshop_joke` VALUES ('3240', '普通监考老师，开始兜两圈，中间兜两圈，结束兜两圈。文艺监考老师，考卷一发，45度角仰望窗外，感受生活的美好。2b监考老师从头兜到尾。你们觉得是不是这个理？？');
INSERT INTO `tpshop_joke` VALUES ('3241', '早上起来，老婆坐在床上一手拿着奶瓶给儿子喂奶，一手拿着手机看着。啪。手机掉下来砸到了儿子，儿子哇哇的哭起来。老公一看：老婆，我该怎么说你呢？你看你，给孩子喂奶还看手机，万一砸坏了孩子怎么办？你说你……老婆看到孩子哭得厉害，也很内疚：老公，我错了，以后我玩手机不喂奶了。从此以后，只要儿子哭了，老婆：老公，儿子哭了，给他喂奶啊。老公：你没看到我在有事吗，你给喂一下啊。老婆：尼玛，我不说了吗，玩手机不喂奶，你没看到我正在玩手机吗？');
INSERT INTO `tpshop_joke` VALUES ('3242', '一从没出过山的老汉到城里打工，有人提醒他：“到城里要学着点，这里和山里不一样。不如说山里的茅厕，在城里就成了‘打不流稀’！”　　汉子闻听，瞪大眼睛惊叹道：“乖乖，城里人就是厉害，‘打’都‘不流稀’。我以后可得注意点，不然被打拉稀了咋办？”');
INSERT INTO `tpshop_joke` VALUES ('3243', '学生甲：这次期末考试你考得如何？学生乙：班里一部分同学考得好，一部分同学考得差。而我呢，是两者兼而有之。学生甲：还行，既不好也不差，中游水平。学生乙：错。你应该把“不”去掉，我是考得好差！学生甲：……');
INSERT INTO `tpshop_joke` VALUES ('3244', '厨　师：真爱是一瓶深藏不露的芥末油，只要一打开瓶盖，你就会泪流满面。气象家：花前月下固然美好，但真正经得起考验的，往往是在恶气候条件下滋生的爱。医　生：真爱是高值血压，催人炫迈；真爱是春季流感，令人发癫；真爱是生老病死，逼人就范。史学家：远古的爱情以生育至上，近代红颜多薄命，而当今社会，真正的爱情不一定在夫妇关系中产生。古玩家：真爱是青花瓷，要谨防“碰瓷”，不然会碎成一地渣；真爱是老古董，要懂得珍惜，不然失去便难复得。数学家：真爱是几何体，就像肉包子，是线（馅）与面的完美组合；真爱是代数中的一一对应，一把钥匙只能开一把锁。');
INSERT INTO `tpshop_joke` VALUES ('3245', '二战的时候有数据统计说2.5万发子弹打死一个士兵，现在想一想，这子弹还真是不长眼睛！');
INSERT INTO `tpshop_joke` VALUES ('3246', '老人养了只猴，猴非常顽劣，老人想：怎么能让这猴老实呢？对了，古人有杀鸡儆猴先例！于是老人把家里养的鸡抓了一只在猴面前手起刀落，鸡脑袋掉了，猴儿吓的眼都不敢眨！老人想：怕了吧？嘿嘿.....老人出门回来，一推门傻眼了，猴儿正拿着菜刀挨个剁鸡脑袋玩儿.....');
INSERT INTO `tpshop_joke` VALUES ('3247', '唐僧师徒取经回来后，玉皇大帝为表示庆贺，特邀所有神仙齐聚凌霄宝殿聚会。在地府工作的阎王爷也受到了邀请。在聚会过程中，玉皇大帝给予了唐僧师徒丰厚的奖赏，并且给在场所有的神仙都各加阳寿一千年！宴会结束后，阎王爷闷闷不乐地回到地府。牛头马面见阎王爷很不高兴于是问道：“老爷，您为何不高兴啊，难道玉皇大帝责怪您了？”阎王爷：“没有，不光没有还给我加了一千年的阳寿。”牛头马面：“那您为什么不高兴啊？”阎王爷：“你他娘的真是牛脑袋！阳寿对我有个屁用！”');
INSERT INTO `tpshop_joke` VALUES ('3248', '一年级体育课上，老师：有哪位同学会做俯卧撑？出来示范下！ 话音刚落同学们争先恐后的示范。 老师：皮皮，你不会吗？皮皮：老师，他们示范的都不对！ 老师：哦？？那你出来给大家做个示范。 皮皮：老师，我需要一个女同学配合... 老师：你给我滚....');
INSERT INTO `tpshop_joke` VALUES ('3249', '新婚之夜，小明媳妇与小明约法三章。小明媳妇：“亲爱的，我们俩你说谁是天？”小明：“媳妇你是天。”小明媳妇：“算你正确，我是天，你是地，那天地间稀薄的空气是什么？”小明：“媳妇你说。”小明媳妇：“那些都是事，靠近天的都是天大的事。”小明：“那地面上的呢？”小明媳妇：“地呢本来就比天小，而且地面上的空气多污浊呀，和屁一样，虽说也是事，只能算屁大的事。”');
INSERT INTO `tpshop_joke` VALUES ('3250', '一个女人关心我吃没吃饭，是不代表她对我有意思？当然了，是谁对你有意思啊？我的女神啊！昨天我帮她拿包儿，她关切对我说：“这么小包儿都拿不动，你没吃饭啊？”卧槽……');
INSERT INTO `tpshop_joke` VALUES ('3251', '一个男生想向喜欢的女神表白，他先向亲戚借了个扩音喇叭，然后在女神宿舍楼下用蜡烛摆成心形。 这时，看热闹的人越来越多，男生觉得时机到了，他激动的拿出扩音喇叭想说话，喇叭发出响亮的声音：“收购旧彩电，旧冰箱，旧电脑，旧洗衣机，旧热水器...”');
INSERT INTO `tpshop_joke` VALUES ('3252', '一男的说“要不是因为孩子，我早就跟你离婚了”女的回答说“要不是因为孩子，我就不会和你结婚”');
INSERT INTO `tpshop_joke` VALUES ('3253', '‍‍‍‍前年父亲对我说：“到年纪了，找个好女孩结婚吧！” 我：“嗯，知道了！” 去年父亲对我说：“找个差不多的女孩结婚算了！” 我说：“嗯，好吧！” 今年父亲对我说：“随便找个女孩凑合结婚吧！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3254', '‍‍‍‍一天坐公交车看到一老太太没座就说：“老太太，来，您坐这吧！” 老太太刚坐下，看到有个小孩就把座让给小孩了。 旁边的人就又给老太太让座，老太太刚坐下说：“年轻人上班挺辛苦的，站了那么久来坐这吧。” 车上的人看到就纷纷给老太太让座，老太太好不容易坐下了。 松了一口气：“唉，一家人总算都有座了！”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3255', '‍‍‍‍昨天去吃酒菜多就没有吃饭，夜晚玩着玩着电脑肚子就饿了。 刚好老爸来我房间，我就问他：“肚子饿，家里有什么吃的？” 我爸讲：“你去药柜拿金嗓子，甜的。” 好老爸啊！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3256', '　　我问一个富豪是怎么成为富豪的，他对我说：我去搬砖挣了一块钱，用它买了两个馒头，一个卖一块，于是我赚了一块。我又用两块钱买了两个苹果，一个卖两块，于是我又赚了两块钱。我说：我好像懂了。富豪说：懂你妹啊，后来我伯父死了，我继承了他的遗产。');
INSERT INTO `tpshop_joke` VALUES ('3257', '一次，一群同事吃饭。一个领导先吃完，没有纸巾擦嘴。我看旁边一平时玩的好的女同事打开的包里有，直接抽了出来递过去，女同事尖叫一声，结果我才发现是卫生巾。被笑了好久。。。。好糗。。。。');
INSERT INTO `tpshop_joke` VALUES ('3258', '‍‍‍‍单位几个科员午饭后聊天。 一妹子给大家说：“我有一同学真是失败的妞。” 科长急问：“失身了，还是咋的了？” 妹子说：“她结婚不到半年的老公跑了。你们说这算不算失败？” 我说：“这有啥啊？这不算失败啊。” 妹子继续说：“你别打岔，我还没说完呢。关键是领他跑的小三哪，是一男人啊？” 尼玛，这也是奇文啊！‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3259', '‍‍‍‍买手机时正和柜台的售货妹妹砍价，旁边一个促销的妹妹说：“先生要不要幽会啊？” 我看了她一眼，是很漂亮不过这也太直接了吧。 我说：“三十岁前我不考虑谈女朋友。” 羞红了脸说：“先生，要不要优惠，我们有活动。”‍‍‍‍');
INSERT INTO `tpshop_joke` VALUES ('3260', '老公：老婆，我决定戒烟！如果你发现我抽一根烟，就惩罚我在沙发上睡三天 老婆：好的。 第二天，老婆发现老公抽了一根烟。三天后，老婆又发现老公在偷偷地抽烟。一个月后，怒不可遏的老婆冲向睡在沙发上的老公咆哮： 你特么给我滚起来！装什么犊子！我看你不是要特么戒烟，你特么这是要戒我呀！');
INSERT INTO `tpshop_joke` VALUES ('3261', '平安夜，一小伙子要跟个女孩表白，就在女孩工作的厂房门口摆了个心形蜡烛，吸引好多人看呢。摆了很久终于摆好了就等女孩出来了，结果女孩真的出来了，在她感动的跨进去的瞬间，裙子着火了……');
INSERT INTO `tpshop_joke` VALUES ('3262', '最近小赚一笔买了一部苹果三，妻子农村下来的，见到智能手机问：老公你这是什么手机怎么还有半拉苹果在上面，我答：这就是用一筐苹果换来的半拉苹果');
INSERT INTO `tpshop_joke` VALUES ('3263', '一个旅游团到宾馆休息，法国人问：‘有红酒吗？’日本人问：“有小姐吗？”印度人问：“有按摩吗？”美国人问：“有牛排吗？”中国人问：“有WIFI吗？”');
INSERT INTO `tpshop_joke` VALUES ('3264', '记得高中时候上课查迟到很严，有次上课迟到，被抓到，让填名字。\r\n因为处罚可严，就想着填损友的名字，刚拿起笔，突然发现上面已经\r\n有好几个损友的名字了……');
INSERT INTO `tpshop_joke` VALUES ('3265', '大妈要不要这么吊 最近广场上风靡小苹果舞曲，一大妈居然吃着苹果，跳着小苹果，手里拿着苹果手机，真可谓是苹果到家了，不要这么吊吧');
INSERT INTO `tpshop_joke` VALUES ('3266', '上班忽然肚子痛，跑去厕所，刚刚脱下裤子，刚蹦出一屁，发现手纸没带，于是只能穿上裤子跑出去拿纸了，然后隔壁坑一哥们，悠悠的来了一句：“这素质，真牛逼，放个屁还要来厕所放。。。');
INSERT INTO `tpshop_joke` VALUES ('3267', '小明复读高三，老师问小明，给大家说说你高考多少分啊？\r\n小明：“差600多分上清华。”\r\n老师：“滚、出、去！”');
INSERT INTO `tpshop_joke` VALUES ('3268', '尼玛是谁说抢走装钱的碗能治好乞丐的腿，等老子抱着装有几十块钱的破碗跑出几步回头看时，只见那货不紧不慢的从怀里掏出了对讲机……，不说了，正在谈赔偿的事呢');
INSERT INTO `tpshop_joke` VALUES ('3269', '小提琴家参加宴会，女主人邀请他演奏一曲，他婉拒：“太晚了，会影响邻居休息的。” 女主人激动地说：“那样就太好了，谁让他家的狗昨晚在我家窗下叫了一夜呢！”');
INSERT INTO `tpshop_joke` VALUES ('3270', '一个人很吝啬朋友来做客，他只用青菜招待，还假装的热情地说：“没有什么好招待的，没有什么菜。” 朋友说：“这些不是菜，难道还是肉？”');
INSERT INTO `tpshop_joke` VALUES ('3271', '一天上课，老师讲到，尼加拉瓜大瀑布是世界上最大的瀑布。同桌睡着了，被老师叫起来让他重复，只见他支支吾吾的道，你家那块大破布是世界上最大的破布。');
INSERT INTO `tpshop_joke` VALUES ('3272', '海关官员拦住一位旅客，并问他是否带有应报关物品。 “没有。”旅客答道。 “您肯定没有么？” “当然。” “那么你身后的这头耳朵上夹着面包片的大象是怎么回事？” “先生，我的三明治里夹什么东西你也管，多事！”');
INSERT INTO `tpshop_joke` VALUES ('3273', '今天开班会！辅导员点完名后问：“那几个没有来的是不是一个寝室的？”下面有人回答“他们是一个区的”。');
INSERT INTO `tpshop_joke` VALUES ('3274', '有一天老婆做鱼给我吃，老公看了看鱼就问老婆一个问题：“为什么鱼的嘴巴总是 啵～啵～啵～啵的”，老婆说：“你管那么多，吃不就完了吗”，当时老公也急了，说：“你这鱼没做熟 我还不能问问啦”。');
INSERT INTO `tpshop_joke` VALUES ('3275', '中门烧鲫鱼，打算先用油煎，下油烧开，突然脑抽想知道油温度够不够。对的，有的人猜到结果了，我随手就像试洗澡水水温一样，伸出中指、食指下去试了！不说了，包扎着，用小指打字也辛苦！');
INSERT INTO `tpshop_joke` VALUES ('3276', '领导要我陪他出差，叫我先在网上订酒店，我问他对酒店有什么要求吗？ 他说：位置没有什么要求，干净就行了，订一间不带沙发的大床房，厕所要有浴缸，最主要就是隔音一定要好！听完后我对自己担心起来。。。。。');
INSERT INTO `tpshop_joke` VALUES ('3277', '夏天快到了，又到了西瓜盛行的时候了。西瓜可真是个好东西啊，不但可以解渴驱暑，还可以用来发泄自己内心的压抑情绪。每当你心情很糟或者遇见令你不爽的人或事的时候，不要彷徨！！！不要迷茫！！！你只需要买一个西瓜，然后拿着刀一顿乱砍，如果还觉得不够痛快，你要记得边砍边喊：我杀瓜！！我杀瓜！！我杀瓜。。。 注意配合好 眼神和面部表情，一定要凶！！！一定要有气势！！！一定！！！！');
INSERT INTO `tpshop_joke` VALUES ('3278', '和媳妇在池塘边上看到一只青蛙，看了一会青蛙的腿突然动了一下，媳妇跟我说:老公快看，它的腿动了一下，估计是腿蹲麻了');
INSERT INTO `tpshop_joke` VALUES ('3279', '女生天天发她们怎样怎样不容易，让男生好好珍惜的心情。男生天天发他们怎样怎样辛苦，让女生好好珍惜的心情。有意思吗？只想说，爱情是一个人的事吗？看到那些就烦。');
INSERT INTO `tpshop_joke` VALUES ('3280', 'LZ男，做小饭馆的，与姐姐，姐夫合租一房子，由于工作原因没时间打扫房间，。今天晚上休息回到住处，见姐替我们打扫房间卫生甚是不感激，躺在床上口渴，突然想起我昨晚喝剩的半瓶啤酒在床下，(LZ有绝招喝半瓶，喝不完的灌在饮料瓶里拧紧瓶盖保存，不影响口感，不想浪费)，怎么不见了，垃圾桶也倒干净了，……姐弟弟该怎么像你解释，那真是啤酒，关键瓶子是果粒橙的。。');
INSERT INTO `tpshop_joke` VALUES ('3281', '白素贞被法海困在雷峰塔后，想尽一切办法要出去！终于有一日，白素贞想到一首歌，于是就大唱到:小秃子乖乖！把门开开，，，，，，');
INSERT INTO `tpshop_joke` VALUES ('3282', 'A:为啥我女朋友叫我楚云飞啊？B:没看过亮剑？A:没有，咋？B:因为钱伯钧说过他是出了名的快枪手。');
INSERT INTO `tpshop_joke` VALUES ('3283', '上中学那会，和一男生闹矛盾，约好放学后学校门口等着，我可不是惹事的人，也怕他召集社会人打我，放学后便带着班主任，教导处主任，政教处主任气势汹汹的往学校门口去，谁知道那逼孩子竟然报警，带着警察在学校门口等我。');
INSERT INTO `tpshop_joke` VALUES ('3284', '‍‍老婆：“我们来做个互相夸奖对方的游戏，你先来！” 我：“老婆你真漂亮！” 老婆：“你真有眼光！”‍‍');
INSERT INTO `tpshop_joke` VALUES ('3285', '‍‍那天我跟女朋友分手。 我：“亲爱的，不要离开我。” 女友：“你妈是不会同意我们在一起的。” 我：“我会坚持的！” 女友：“就算你妈同意，你爸也不会同意的。” 我：“我会说服他们的！” 女友：“就算你全家都同意，我也不会同意的！” 我：“……”');
INSERT INTO `tpshop_joke` VALUES ('3286', '转学后的第一次月考，考场的座位是根据成绩来排的，学霸和学霸坐在一起，学渣跟学渣坐在一起。由于我是新转来的，所以我坐在最渣的一个考场。而这个考场也刷新了我之前对考场的所有认识。在整个考试的过程中，我看到有人在吃茶叶蛋，有人在泡方便面，有人在下象棋，还有人在拿试卷折飞机。我偷偷掏出了书，开始作弊。然后身为作弊小王子的我，竟然被发现了。因为在这个考场里，作弊的我显得太出众了。然后我荣幸的被表扬了，监考老师竟然表扬了我的行为，说我有上进心。说我不但想到去写，还想到去抄。那一刻，我醉了，我长那么大，第一次遇到这么懂我的老师。我默默给他点了32个赞。');
INSERT INTO `tpshop_joke` VALUES ('3287', '跟男朋友去看北京遇上西雅图，剧中汤唯说：他是世界上最好的男人，他也许不会带我去吃法餐，坐游艇，但他愿意每天早上为我跑几条街去买我最爱吃的豆浆油条... 我马上转头向男友撒娇：人家每天早上也要吃豆浆油条... 谁知那二货不紧不慢的说：油条含着，豆浆马上就来！');
INSERT INTO `tpshop_joke` VALUES ('3288', '和女朋友去公园散步，她突然放了个闷屁（巨臭的那种），于是我看见介个二货迅速围着我转了一圈跑到我面前说:老公，你已经被我的屁包围了，快投降吧哈哈！我。。特么抽死你丫的');
INSERT INTO `tpshop_joke` VALUES ('3289', '初恋女友气势汹汹的对我说：让你混蛋儿子赶紧转学！别再纠缠你女儿了！我.....还能说什么啊，赶紧给儿子转学呗！');
INSERT INTO `tpshop_joke` VALUES ('3290', '小时候因为家里穷，能喝碗羊肉汤那时候已经算很奢侈的了，记得年轻的时候在家拿的馒头。去喝羊肉汤，加汤不要钱，花了一元钱买了碗羊肉汤，加了老板十碗汤………想想那时候老板都醉了！！！');
INSERT INTO `tpshop_joke` VALUES ('3291', '蚂蚁与蜈蚣结婚。\r\n新婚的第二天，蚂蚁朋友问其感觉如何。\r\n蚂蚁唉声叹气道：“别提了，我昨晚掰开一条腿不是，又掰开一条又不是，他妈的我掰了一夜的腿。”');
INSERT INTO `tpshop_joke` VALUES ('3292', '莎士比亚：扒还是不扒呢？这是个问题！\r\n王朔：扒！我是流氓我怕谁？！\r\n木子美：你能做多长时间你就扒多长时间！\r\n竹影晴瞳：我自己扒的目的不是让你做，而是让你看！\r\n布什：为避免你先下手，我有先扒你的权利！\r\n阿拉法特：以扒换和平，是巴勒斯坦建国的出路所在！\r\n本拉登：扒不过瘾，炸掉它算了！ 萨达姆：说我扒？那是美国佬的阴谋！\r\n安南：请和平扒下，战争是解决不了问题的！\r\n布莱尔：先扒，然后再找理由！\r\n陈水扁：这事儿不是我想扒就能扒的，得全民公投！\r\n克林顿：还是扒我的吧，你用嘴就行了，方便。');
INSERT INTO `tpshop_joke` VALUES ('3293', '1、泡：动词，指的是接触并接近的意思。\r\n2、妞：名词，指的是有才有貌有品的女孩子（年龄在18 岁以上）。\r\n甲方：我\r\n乙方：她\r\n甲乙双方就甲方在心里偷偷喜欢乙方并遵守该合同的有关事宜，本着互相尊重互相爱护的原则，经友好协商，达成如下协议：\r\n一、原则：\r\n禁止伤害、禁止不雅。\r\n二、甲方职责：\r\n1、甲方须学会积极的生活态度并掌握很多的生活技巧和常识，以便乙方随时可以通过甲方解决某些困难；\r\n2、甲方须学会很多（包括但不限于网络笑话，小人书笑话）可以在1分60秒钟内使乙方开心大笑的文雅笑话，以便乙方随时可以在甲方这里得到欢乐；');
INSERT INTO `tpshop_joke` VALUES ('3294', '下午下班高峰期的公交车上，一个颇为帅气的小伙子接起电话，就好像周围没人一样，大声的讲电话。\r\n一开始也没怎么，后来就开始了：“大哥，今天真来不了，最近我忙着呢，你不知道，我最近被个富婆在xx小区包了，天天得回去，不回去不行，没办法呀。。。。。。。。。那好好，恩有时间我给你们打电话，我请客，，，给哥几个赔不是，，哈哈，好那挂了。”\r\n等他打完电话一抬头，全车人都在看着他。');
INSERT INTO `tpshop_joke` VALUES ('3295', '昨天晚上我着就穿一内裤躺床上，边喝酸奶边上网，不小心酸奶滴\r\n到身上了，我正在擦，我爸突然进来了，看了我什么话也\r\n没说扭头又关门出去了。What a f**king day!\r\n我LP过生日，我很想让她过得开心，精心筹划了一整天，给她买了新\r\n鞋，去吃300多的西餐，在后海玩了一晚上，回到家打开QQ，看见她签\r\n名更新，写着：“无聊的一天，睡觉去” What a f**king day!\r\n今天我从我15岁的女儿电脑上搜出来一部黄片，藏在“未来的职业”文\r\n件夹里，未来的职业！What a f**king day!\r\n......');
INSERT INTO `tpshop_joke` VALUES ('3296', '古蒂家有一只冠军狗到处找狗打架都赢……无论是国内的……国外的……\r\n因此它很嚣张……向别的狗挑衅，向它们乱叫……\r\n一天古蒂牵着冠军狗在路上走着……\r\n看到劳尔牵着一条很大的狗，古蒂的冠军狗又便跑过去乱叫\r\n古蒂心想：如果我的冠军狗把劳尔的狗打败，那不是很威风吗？\r\n于是他对劳尔说：“让我的冠军狗和你家的狗打打怎么样？”\r\n劳尔：“这个……不好吧”\r\n古蒂：“没关系，如果它真的伤到你家的狗，我会制止的。”\r\n劳尔：“还是不好吧。”\r\n就在他们两个商量的时候，两只狗打了起来，结果冠军狗惨遭落败，败得极其狼狈……\r\n......');
INSERT INTO `tpshop_joke` VALUES ('3297', '福尔摩斯和他的助手一天晚上在山坡上搭起帐篷露营，睡到半夜，福尔摩斯推醒旁边的助手，指着 满天的繁星问道：“看到这么多星星你想到了什么？”\r\n助手沉思了半晌，说道：“天空真是无边无际，每颗星星都相当于一个太阳，而我们居住的地球在太 阳系里只是很小的一颗行星，我们人类又是显得多么渺小啊！ ”\r\n你这个笨蛋，我们的?帐篷被偷了！！！ ”福尔摩斯怒道。');
INSERT INTO `tpshop_joke` VALUES ('3298', '小孩把妓院养的鹦鹉偷回家，一进门鹦鹉便叫；搬家了！\r\n看见他妈妈又叫：老板也换了！\r\n看见他姐姐又叫：小姐也换了！看见他爸爸又叫；我cao，还是老客户！');
INSERT INTO `tpshop_joke` VALUES ('3299', '电话铃声响，小女孩接起电话听筒....\r\n男人：“喂，小娃儿，我是爸爸，妈妈在哪儿？”\r\n小女孩：“妈妈和陈叔叔在楼上的房间。”\r\n男人有点生气地说：“哪个陈叔叔？我们家不认识叫陈叔叔的人啊！ ”\r\n小女孩：“有啊，每次你上班后就来找他*的陈叔叔埃”\r\n过了不久，男人沉住气冷静地说：“小娃儿，我们来玩个游戏好不好。”\r\n小女孩兴奋地说：“好哇！ ”\r\n男人：“你先去楼上的房间，然后大声喊 \"爸爸回来啦！\"过后再来听电话。”\r\n小女孩照着做了，不久听到一阵惨叫，小女孩跟着听电话.....');
INSERT INTO `tpshop_joke` VALUES ('3300', '甲：哈哈哈，我看到一个笑话好好笑喔！\r\n乙：是什么啊？说来听听！\r\n甲：可是很黄。\r\n乙：那黄色的地方就跳过嘛！\r\n甲：跳过跳过跳过跳过跳过，跳过跳过，完毕！');

-- ----------------------------
-- Table structure for tpshop_manager
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_manager`;
CREATE TABLE `tpshop_manager` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '密码',
  `email` varchar(255) NOT NULL DEFAULT '' COMMENT '邮箱',
  `nickname` varchar(255) NOT NULL DEFAULT '' COMMENT '昵称',
  `last_login_time` int(11) unsigned DEFAULT NULL COMMENT '上次登录时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态：1可用 2禁用',
  `role_id` tinyint(3) NOT NULL DEFAULT '0' COMMENT '角色id',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_manager
-- ----------------------------
INSERT INTO `tpshop_manager` VALUES ('2', 'xiaoming', '32d1259692ac01f2df13c6439c655e66', 'x@qq.com', 'xx', null, '1', '0', '1618041950', '1618041950', null);
INSERT INTO `tpshop_manager` VALUES ('3', 'xiaohong', '8cd9cb66ed41bdf08f62fa7b4b0abdb7', 'xh@qq.com', 'xh', null, '1', '0', '1618042584', '1618042584', null);
INSERT INTO `tpshop_manager` VALUES ('5', 'admin1', '60e2b17cd4cece9599a85d0b4e6fae3b', '', '', null, '1', '0', '1618460115', '1618460115', null);
INSERT INTO `tpshop_manager` VALUES ('10', '111111', 'bc41d1d77386428bda125277771acd62', '', '', null, '1', '0', '1618468901', '1618468901', null);

-- ----------------------------
-- Table structure for tpshop_manager_role
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_manager_role`;
CREATE TABLE `tpshop_manager_role` (
  `mid` int(11) DEFAULT NULL,
  `rid` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='管理员角色关系表';

-- ----------------------------
-- Records of tpshop_manager_role
-- ----------------------------
INSERT INTO `tpshop_manager_role` VALUES ('2', '4');

-- ----------------------------
-- Table structure for tpshop_member
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_member`;
CREATE TABLE `tpshop_member` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` varchar(40) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `age` int(11) DEFAULT NULL,
  `avatar` varchar(120) DEFAULT NULL COMMENT '头像',
  `sex` varchar(3) NOT NULL DEFAULT '3',
  `email` varchar(50) NOT NULL COMMENT '邮箱',
  `phone` char(11) NOT NULL COMMENT '手机',
  `money` float(9,2) unsigned DEFAULT '0.00' COMMENT '账户余额',
  `create_time` int(10) unsigned DEFAULT NULL COMMENT '注册时间',
  `active` tinyint(1) unsigned DEFAULT '0' COMMENT '用户状态',
  `delete_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `refer_code` varchar(255) DEFAULT NULL,
  `refer_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_member
-- ----------------------------
INSERT INTO `tpshop_member` VALUES ('1', 'admin', 'admin', null, null, '3', 'admin@qq.com', '11225478789', '0.00', null, '0', null, null, null, '0');

-- ----------------------------
-- Table structure for tpshop_member_email_phone
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_member_email_phone`;
CREATE TABLE `tpshop_member_email_phone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `phone` char(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `expire_time` int(11) DEFAULT NULL,
  `login_time` int(11) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_member_email_phone
-- ----------------------------

-- ----------------------------
-- Table structure for tpshop_order
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_order`;
CREATE TABLE `tpshop_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_sn` varchar(255) NOT NULL DEFAULT '' COMMENT '订单编号',
  `order_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '订单金额',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '下单用户id',
  `consignee_name` varchar(255) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  `consignee_phone` varchar(255) NOT NULL DEFAULT '' COMMENT '收货人手机号',
  `consignee_address` varchar(255) NOT NULL DEFAULT '' COMMENT '收货人地址',
  `shipping_type` varchar(64) NOT NULL DEFAULT '' COMMENT '配送方式 yuantong圆通 shentong申通 yunda韵达 zhongtong中通 shunfeng顺丰',
  `pay_status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '支付状态 0未付款 1已付款',
  `pay_type` varchar(64) NOT NULL DEFAULT '' COMMENT '支付方式 card银联 wechat微信 alipay支付宝',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_order
-- ----------------------------

-- ----------------------------
-- Table structure for tpshop_order_goods
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_order_goods`;
CREATE TABLE `tpshop_order_goods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL DEFAULT '0' COMMENT '订单id',
  `goods_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品id',
  `goods_name` varchar(255) NOT NULL DEFAULT '' COMMENT '商品名称',
  `goods_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品单价',
  `goods_logo` varchar(255) NOT NULL DEFAULT '' COMMENT '商品logo图',
  `number` int(11) NOT NULL DEFAULT '0' COMMENT '购买数量',
  `goods_attr_ids` varchar(255) NOT NULL DEFAULT '' COMMENT '商品属性ids',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_order_goods
-- ----------------------------

-- ----------------------------
-- Table structure for tpshop_qq
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_qq`;
CREATE TABLE `tpshop_qq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `gender` enum('男','女') DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `figurl` varchar(255) DEFAULT NULL,
  `login_num` int(11) DEFAULT '1',
  `mid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `openid` (`openid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_qq
-- ----------------------------

-- ----------------------------
-- Table structure for tpshop_role
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_role`;
CREATE TABLE `tpshop_role` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) NOT NULL DEFAULT '' COMMENT '角色/用户组名称',
  `role_auth_ids` varchar(128) NOT NULL DEFAULT '' COMMENT '权限ids,1,2,5，权限表中的主键集合',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_role
-- ----------------------------
INSERT INTO `tpshop_role` VALUES ('4', '经理', '', '1617865439', '1617865439', null);
INSERT INTO `tpshop_role` VALUES ('5', '主管', '', '1617865449', '1617865449', null);
INSERT INTO `tpshop_role` VALUES ('6', '老手', '', '1617865457', '1617865457', null);
INSERT INTO `tpshop_role` VALUES ('7', '萌新', '', '1617865465', '1617865465', null);
INSERT INTO `tpshop_role` VALUES ('8', '客服', '', '1617865472', '1617865472', null);
INSERT INTO `tpshop_role` VALUES ('9', '111', '', '1618565835', '1618565835', null);

-- ----------------------------
-- Table structure for tpshop_role_auth
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_role_auth`;
CREATE TABLE `tpshop_role_auth` (
  `rid` int(11) DEFAULT NULL,
  `aid` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='角色权限表';

-- ----------------------------
-- Records of tpshop_role_auth
-- ----------------------------
INSERT INTO `tpshop_role_auth` VALUES ('4', '15');
INSERT INTO `tpshop_role_auth` VALUES ('4', '19');
INSERT INTO `tpshop_role_auth` VALUES ('4', '20');
INSERT INTO `tpshop_role_auth` VALUES ('4', '21');
INSERT INTO `tpshop_role_auth` VALUES ('9', '21');
INSERT INTO `tpshop_role_auth` VALUES ('9', '22');
INSERT INTO `tpshop_role_auth` VALUES ('9', '23');

-- ----------------------------
-- Table structure for tpshop_test
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_test`;
CREATE TABLE `tpshop_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_test
-- ----------------------------

-- ----------------------------
-- Table structure for tpshop_token
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_token`;
CREATE TABLE `tpshop_token` (
  `id` tinyint(4) DEFAULT NULL,
  `expire_time` int(11) DEFAULT NULL,
  `access_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_token
-- ----------------------------
INSERT INTO `tpshop_token` VALUES ('1', '1619346389', '44_cpDugfErbgR2WdYaqZsS3yu7wawZsnfDRkQXUvz9UZVszen0J70Us4r_vO50beRMXcYb-lsa8j3BihgBcgs-kQSVONP0cN13Qpy4_-WEOEVWitbJ6fWKBDlt92oZgf0DaKxSkaBDN8G2ooE1YWPjACAECV');

-- ----------------------------
-- Table structure for tpshop_user
-- ----------------------------
DROP TABLE IF EXISTS `tpshop_user`;
CREATE TABLE `tpshop_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '' COMMENT '用户名',
  `phone` varchar(255) NOT NULL DEFAULT '' COMMENT '手机号码',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '密码',
  `email` varchar(255) NOT NULL DEFAULT '' COMMENT '邮箱',
  `last_login_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次登录时间',
  `is_check` tinyint(2) NOT NULL DEFAULT '0' COMMENT '激活状态 0未激活 1已激活',
  `email_code` varchar(255) NOT NULL DEFAULT '' COMMENT '邮箱激活验证码',
  `openid` varchar(255) NOT NULL DEFAULT '' COMMENT '第三方帐号openid',
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `delete_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tpshop_user
-- ----------------------------

-- ----------------------------
-- Table structure for typecho_comments
-- ----------------------------
DROP TABLE IF EXISTS `typecho_comments`;
CREATE TABLE `typecho_comments` (
  `coid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned DEFAULT '0',
  `created` int(10) unsigned DEFAULT '0',
  `author` varchar(200) DEFAULT NULL,
  `authorId` int(10) unsigned DEFAULT '0',
  `ownerId` int(10) unsigned DEFAULT '0',
  `mail` varchar(200) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `ip` varchar(64) DEFAULT NULL,
  `agent` varchar(200) DEFAULT NULL,
  `text` text,
  `type` varchar(16) DEFAULT 'comment',
  `status` varchar(16) DEFAULT 'approved',
  `parent` int(10) unsigned DEFAULT '0',
  `likes` int(11) DEFAULT '0',
  PRIMARY KEY (`coid`),
  KEY `cid` (`cid`),
  KEY `created` (`created`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of typecho_comments
-- ----------------------------
INSERT INTO `typecho_comments` VALUES ('4', '20', '1573613822', 'phpmaster', '1', '1', '137647337@qq.com', 'http://www.typecho.org', '116.136.21.161', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', '此生无悔入华夏,来世还在种花家!', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('2', '14', '1573135311', 'phpmaster', '1', '1', '137647337@qq.com', 'http://www.typecho.org', '171.8.223.202', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', '说的真好啊', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('3', '41', '1573611040', 'phpmaster', '1', '1', '137647337@qq.com', 'http://www.typecho.org', '116.136.21.161', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', '今天又是元气满满的一天呐!', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('5', '20', '1573614112', '初心', '0', '1', '137647337@qq.com', 'http://www.zhangxuhui.com', '116.136.21.161', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', '此生无悔入华夏,来世还在种花家!', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('6', '3', '1575967521', '111', '0', '1', '124354345@163.com', null, '117.158.213.170', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', '大太阳', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('7', '46', '1576379536', '111', '0', '1', '124354345@163.com', null, '117.158.213.170', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 'nice', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('8', '41', '1577197534', 'phpmaster', '1', '1', '137647337@qq.com', 'http://www.typecho.org', '42.233.6.248', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '这短短的一生，我们最终都会失去。你不妨大胆一些，爱一个人，攀一座山，追一个梦!', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('17', '1', '1577523218', 'IT小白', '0', '1', '3036685776@qq.com', null, '223.91.194.245', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', '本该分析着数据的我，却在这看起了你的博客，文章很喜欢，歌也很喜欢', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('18', '69', '1577523744', 'IT小白', '0', '1', '3036685776@qq.com', null, '223.91.194.245', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', '银临还有一首意难平我可喜欢，不知道你有木有听过', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('10', '65', '1577247792', '忍冬', '0', '1', '3069193161@qq.com', null, '1.192.147.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '老师你这页面实现的太好看了吧.', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('11', '63', '1577247961', '忍冬', '0', '1', '3069193161@qq.com', null, '1.192.147.10', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '银临的不老梦也超级好听.', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('12', '63', '1577250181', 'phpmaster', '1', '1', '137647337@qq.com', 'http://www.typecho.org', '42.233.6.235', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '银临的所有歌曲我都喜欢，声音纯净，空灵。双笙，封茗囧菌等都超喜欢的！我在“关于”里有写到。QAQ', 'comment', 'approved', '11', '0');
INSERT INTO `typecho_comments` VALUES ('13', '42', '1577328127', 'IT小白', '0', '1', '3036685776@qq.com', null, '223.104.106.216', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', '老师，好喜欢你的页面，效果太棒了！！！', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('14', '42', '1577353456', 'phpmaster', '1', '1', '137647337@qq.com', 'http://www.typecho.org', '218.29.60.105', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', '谢谢！｡◕ᴗ◕｡QAQ', 'comment', 'approved', '13', '0');
INSERT INTO `typecho_comments` VALUES ('15', '65', '1577361791', 'phpmaster', '1', '1', '137647337@qq.com', 'http://www.typecho.org', '42.233.3.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '是吗？我也觉得挺好看的，嘿嘿！', 'comment', 'approved', '10', '0');
INSERT INTO `typecho_comments` VALUES ('16', '46', '1577362012', '初心', '0', '1', 'phpmaster@yeah.net', 'http://www.zhangxuhui.com', '42.233.3.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '汉服！中华民族的服饰！吾辈定当竭尽全力，至死不渝！', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('19', '69', '1577779965', 'phpmaster', '1', '1', '137647337@qq.com', 'http://www.typecho.org', '117.158.213.170', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '听了，还行', 'comment', 'approved', '18', '0');
INSERT INTO `typecho_comments` VALUES ('20', '41', '1578190867', 'phpmaster', '1', '1', '137647337@qq.com', 'http://www.typecho.org', '61.158.149.78', 'Mozilla/5.0 (Linux; U; Android 7.0; zh-cn; Redmi Note 4X Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/71.0.3578.141 Mobile Safari/537.36 XiaoMi/MiuiBrowser/11.4.14', '虽然一直关注着天气预报，但是对于第一场雪的到来仍然感到猝不及防，欣喜若狂！盼望着，盼望着大雪的不期而至！盼雪的心情就犹如女子盼望心爱的男子到来一样，怕他不来，又怕他乱来！', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('21', '46', '1578579705', 'idouz', '0', '1', 'idouz@qq.com', null, '39.159.88.168', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', 'wc,界面太秀了把', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('22', '46', '1578580707', '楼下小黑', '0', '1', '2370730662@qq.com', 'http://myags.cn', '103.135.80.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', '博客的特效真是做得特别棒,双击666', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('23', '46', '1579247262', '楼上小李', '0', '1', '18736621852@163.com', null, '117.158.213.170', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '你这个黑娃', 'comment', 'approved', '22', '0');
INSERT INTO `typecho_comments` VALUES ('24', '43', '1583037479', '文轩设计之路', '0', '1', '2797190385@qq.com', 'http://www.wenxuands.com', '182.88.247.96', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36', '哈喽博主，小站来交换友链啦\r\n名称：文轩设计之路\r\n链接：http://www.wenxuands.com \r\n描述：一个80后从事于网站前端设计和平面设计的站长之路。\r\n图标：https://www.wenxuands.com/favicon.ico\r\n联系QQ：2797190385\r\n', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('25', '43', '1583039048', 'phpmaster', '1', '1', '137647337@qq.com', 'http://www.typecho.org', '120.216.201.219', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', '好的，已经添加！', 'comment', 'approved', '24', '0');
INSERT INTO `typecho_comments` VALUES ('26', '46', '1583040378', '文轩设计之路', '0', '1', '2797190385@qq.com', 'http://www.wenxuands.com', '182.88.247.96', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36', '博主是做汉服方面的吗？', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('30', '43', '1585393642', 'Xz博客', '0', '1', '3535988284@qq.com', 'http://www.2020xz.cn', '112.97.245.159', 'Mozilla/5.0 (Linux; Android 9; V1936A Build/PKQ1.190806.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/68.0.3440.91 Mobile Safari/537.36', 'XZ博客\r\nlogo：http://www.2020xz.cn/usr/uploads/2020/03/750278960.jpg\r\n网址:http://www.2020xz.cn/', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('29', '99', '1585147550', '夏树', '0', '1', '908851835@qq.com', null, '117.172.2.149', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400', 'Python 全栈工程师必备面试题  原始地址  https://gitbook.cn/gitchat/activity/5e097b811c2b9f718522c91d', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('31', '42', '1587452026', '阿锦', '0', '1', '2197706835@qq.com', null, '175.0.56.254', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3756.400 QQBrowser/10.5.4039.400', '请问那篇关于汉服的，我可以转载到我的公众号吗？', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('32', '59', '1587522729', '阿锦', '0', '1', '2197706835@qq.com', null, '175.0.54.11', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3756.400 QQBrowser/10.5.4039.400', '请问可以转载到我的公众号吗？求回复', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('33', '59', '1587796797', 'phpmaster', '1', '1', '137647337@qq.com', 'http://www.typecho.org', '1.199.77.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', '可以啊，标明出处就行！', 'comment', 'approved', '32', '0');
INSERT INTO `typecho_comments` VALUES ('34', '59', '1587796812', 'phpmaster', '1', '1', '137647337@qq.com', 'http://www.typecho.org', '1.199.77.75', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', '欢迎转载', 'comment', 'approved', '33', '0');
INSERT INTO `typecho_comments` VALUES ('35', '41', '1591090266', 'phpmaster', '1', '1', '137647337@qq.com', 'http://www.typecho.org', '171.8.63.53', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', '没想到一场疫情波及面会这么广，还一直以为影响不到我，结果还是被影响到了！新的公司，新的工作，新的起点，新的征程，新的开始，新的希望。。。加油吧！干吧得！', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('36', '42', '1591242084', '柠檬水', '0', '1', '2950066875@qq.com', null, '171.8.63.53', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3756.400 QQBrowser/10.5.4043.400', '老师好可爱', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('37', '42', '1594181270', '凛凛凛', '0', '1', '2434808201@qq.com', null, '61.158.208.134', 'Mozilla/5.0 (Linux; U; Android 10; zh-cn; MIX 3 Build/QKQ1.190828.002) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/71.0.3578.141 Mobile Safari/537.36 XiaoMi/MiuiBrowser/12.4.14', '老师来个美少女万华镜一的资源', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('41', '46', '1601973747', '八字算命', '0', '1', '1107379747@qq.com', 'http://www.ok0514.com', '14.21.138.121', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '文章写得好，网站都很吊', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('40', '26', '1597839873', '一位人才', '0', '1', '3067827865@qq.com', null, '223.104.111.208', 'Mozilla/5.0 (Linux; U; Android 9; zh-CN; V1934A Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.108 Quark/4.2.3.139 Mobile Safari/537.36', '人生就像写代码，总是不经意间的小细节，造成了许多bug，人生总会有些许惊喜和意外，只要心态不炸，总会好起来的啦', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('42', '42', '1603785019', '假正经', '0', '1', '1337647308@qq.com', 'http://www.baidu.com', '117.158.213.170', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '老师您真是太有才了！', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('43', '42', '1603956880', '^ ^卡卡西', '0', '1', '255374192@qq.com', null, '183.240.33.243', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', '可以呀', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('44', '46', '1603956925', '^ ^卡卡西', '0', '1', '255374192@qq.com', null, '183.240.33.243', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36', 'nicenice', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('45', '42', '1605100499', '你猜', '0', '1', '137647337@qq.com', 'http://www.zzidc.com', '1.193.82.91', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', '不错不错，学习了', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('46', '46', '1605511190', 't', '0', '1', 't@t.com', 'http://t', '183.62.142.253', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '赞同', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('47', '61', '1606940553', '席小欢', '0', '1', 'internetyewu@163.com', 'http://wozhidaole.com.cn/tiaozhuan/?url=http://wozhidaole.com.cn/', '171.81.255.46', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36/8mqQhSuL-09', '看到你的网站，觉得很不错，希望能与你互相友情链接…\r\n我的网站:建站知道网-http://wozhidaole.com.cn/\r\n\r\n如果同意的话，回复后互相上链接！', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('48', '61', '1610526347', 'phpmaster', '1', '1', '137647337@qq.com', 'http://www.typecho.org', '171.15.158.32', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', '已经添加', 'comment', 'approved', '47', '0');
INSERT INTO `typecho_comments` VALUES ('49', '138', '1611799915', '45tgf', '0', '1', '123465@qq.com', null, '111.58.168.227', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '中通 申通 圆通 顺丰 百世快递单号查询、购买单号网www.kuaid100.cn', 'comment', 'spam', '0', '0');
INSERT INTO `typecho_comments` VALUES ('50', '137', '1611800896', '5tgv', '0', '1', '123465@qq.com', null, '111.58.168.227', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', '空包代发。快递单号选择www.kuaidzj.com', 'comment', 'spam', '0', '0');
INSERT INTO `typecho_comments` VALUES ('51', '46', '1611945249', '您的学生', '0', '1', '1136810522@qq.com', null, '42.239.47.234', 'Mozilla/5.0 (Linux; U; Android 10; zh-cn; MI 9 Build/QKQ1.190825.002) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.120 MQQBrowser/11.2 Mobile Safari/537.36 COVC/045517', '棒棒哒', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('52', '42', '1614670706', '无名', '0', '1', '14111860091@qq.com', null, '182.117.62.100', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 UBrowser/6.2.4098.3 Safari/537.36', '我想参考一下', 'comment', 'approved', '0', '0');
INSERT INTO `typecho_comments` VALUES ('53', '3', '1617164435', 'Mike Michaelson', '0', '1', 'see-email-in-message@monkeydigital.co', 'https://www.monkeydigital.co/product-category/special-offers/', '109.248.148.243', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36 Kinza/4.9.1', 'Hi there \r\n \r\nDo you want a quick boost in ranks and sales for your zhangxuhui.com website? \r\nHaving a high DA score, always helps \r\n \r\nGet your zhangxuhui.com to have a DA between 50 to 60 points in Moz with us today and rip the benefits of such a great feat. \r\n \r\nSee our offers here: \r\nhttps://www.monkeydigital.co/product/moz-da50-seo-plan/ \r\n \r\nNEW: \r\nhttps://www.monkeydigital.co/product/ahrefs-dr60/ \r\n \r\n \r\nthank you \r\nMike Michaelson\r\n \r\nsupport@monkeydigital.co \r\nMonkey Digital', 'comment', 'waiting', '0', '0');
INSERT INTO `typecho_comments` VALUES ('54', '41', '1617443841', 'test', '0', '1', 'justTest2020@qq.com', 'http://test', '114.100.105.114', 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 'test', 'comment', 'waiting', '0', '0');
INSERT INTO `typecho_comments` VALUES ('55', '2', '1617443848', 'test', '0', '1', 'justTest2020@qq.com', 'http://test', '114.100.105.114', 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 'test', 'comment', 'waiting', '0', '0');
INSERT INTO `typecho_comments` VALUES ('56', '43', '1617443854', 'test', '0', '1', 'justTest2020@qq.com', 'http://test', '114.100.105.114', 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 'test', 'comment', 'waiting', '0', '0');
INSERT INTO `typecho_comments` VALUES ('57', '61', '1617443923', 'test', '0', '1', 'justTest2020@qq.com', 'http://test', '114.100.105.114', 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36', 'test', 'comment', 'waiting', '0', '0');
INSERT INTO `typecho_comments` VALUES ('58', '3', '1617972030', 'Mike Kennett', '0', '1', 'no-replytob@gmail.com', 'https://no-site.com', '86.106.74.252', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', 'Greetings \r\n \r\nI have just took an in depth look on your  zhangxuhui.com for its Local SEO Trend and seen that your website could use an upgrade. \r\n \r\nWe will improve your Local Ranks organically and safely, using only whitehat methods, while providing Google maps and website offsite work at the same time. \r\n \r\nPlease check our services below, we offer SEO at cheap rates. \r\nhttps://speed-seo.net/product/local-seo-package/ \r\n \r\nStart improving your local visibility with us, today! \r\n \r\nregards \r\nMike Kennett\r\n \r\nSpeed SEO Digital Agency \r\nsupport@speed-seo.net', 'comment', 'waiting', '0', '0');
INSERT INTO `typecho_comments` VALUES ('59', '3', '1618360578', 'Mike Watson', '0', '1', 'no-reply@google.com', 'https://google.com', '37.120.143.52', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36', 'Howdy \r\n \r\nI have just analyzed  zhangxuhui.com for its SEO metrics and saw that your website could use an upgrade. \r\n \r\nWe will increase your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly reports and outstanding support. \r\n \r\nPlease check our pricelist here, we offer SEO at cheap rates. \r\nhttps://www.hilkom-digital.de/cheap-seo-packages/ \r\n \r\nStart increasing your sales and leads with us, today! \r\n \r\nregards \r\nMike Watson\r\n \r\nHilkom Digital Team \r\nsupport@hilkom-digital.de', 'comment', 'waiting', '0', '0');
INSERT INTO `typecho_comments` VALUES ('60', '42', '1618432682', 'PeterRag', '0', '1', 'desrast84@yandex.com', 'http://crack-getdata-excel-repair.shkolamd.ru/', '213.166.69.123', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.34 Safari/537.36 Edg/83.0.478.25', '<b>Download Repair My Excel: Easily fix corrupt or damaged</b> \r\nDownload locations for <strong>Repair My Excel</strong>, Downloads, Size: MB. <strong>Repair damaged or corrupt Excel</strong> files. \r\nPDF Repair 9.11.01 Free Download - Rebuild inaccessible <a href=http://crack-getdata-excel-repair.shkolamd.ru/>shkolamd.ru</a>. \r\n<a href=http://minecraft-hack-gamemode.shkolamd.ru/>Single Player Commands Mod for Minecraft 1.4.7 Download</a>. Download Livewire Professional Edition 1.20 / 1.30 Updater <a href=http://professional-edition-kamus-keygen-full.shkolamd.ru/>http://professional-edition-kamus-keygen-full.shkolamd.ru/</a>. Black Swan Game Fixes <a href=http://skies-games-patch-crimson.shkolamd.ru/>websites</a>. <a href=http://nokia-super-hack-bluetooth.shkolamd.ru/>Free Nokia Asha 200/201 Super Bluetooth Hack V1.08</a>. <a href=http://keygen-fileminimizer-suite.shkolamd.ru/>FILEminimizer Suite 8.0 Full + Crack</a>. DJ Times March 2020, Vol 30 No 3 by DJ Times Magazine <a href=http://heels-serato-cracked.shkolamd.ru/>learn more here</a>. VMware ESXi 5.0, Patch ESXi-5.0.0-20120704001-standard <a href=http://vmware-esxi-keygen.shkolamd.ru/><img src=\"https://i.ytimg.com/vi/lR0s1lu6c2c/hqdefault.jpg\"></a>. Proshow Gold 5.0 3280 Keygen - Iz glave 20. sezona <a href=http://keyboard-proshow-producer-full.shkolamd.ru/>shkolamd.ru</a>. Firefox666 - Members - Metin2 ES <a href=http://hack-firefox-yang-metin2.shkolamd.ru/>http://hack-firefox-yang-metin2.shkolamd.ru/</a>. <a href=http://activator-win7.shkolamd.ru/>ChemInform Abstract: Ir-Catalyzed Intermolecular</a>. <a href=http://intocartoon-serial-crack.shkolamd.ru/>IntoCartoon Professional Edition V3.1 Portable</a>. <a href=http://only-crack-fifa-sinhala.shkolamd.ru/>Fifa 08 Crack Only Download Sinhala - Bhatkal Journalist\'s</a>. <a href=http://hack-source-royal-counter-strike.shkolamd.ru/>GibbsCam Crack With License Key Download ...</a>. Xiuang! WWX.Net SWS: //frenchliterature.party/Romain <a href=http://multi-banjo-hack.shkolamd.ru/>shkolamd.ru</a>. Jailbreak iOS 4.2.1 on iPad using PwnageTool with Working <a href=http://patch-file-cydia.shkolamd.ru/><img src=\"https://i.ytimg.com/vi/hPu-1zUMmuc/hqdefault.jpg\"></a>. <a href=http://hack-windows-wall.shkolamd.ru/>HL1 Framework cracked by slientos1337</a>. Need some help with Photodex ProShow... - VideoHelp Forum <a href=http://cracker-producer-proshow.shkolamd.ru/>http://cracker-producer-proshow.shkolamd.ru/</a>. <a href=http://photoshop-crack-adobe-professional-noiseware.shkolamd.ru/>Adobe Photoshop Plugin - Noiseware Professional 4.2</a>. Cartoon Wars 3 Hack Gold & Crystals – Download Hacked Games <a href=http://cartoon-wars-hack.shkolamd.ru/>helpful site</a>. COD Ghosts Keygen: Counter strike 1.6 Aimbot + Wallhack <a href=http://hack-counter-wallhack-strike.shkolamd.ru/><img src=\"https://i.ytimg.com/vi/yYyjmQgrt0s/hqdefault.jpg\"></a>. <a href=http://dissidia-english-patch.shkolamd.ru/>Dissidia 012 duodecim  Final Fantasy Sony PSP Complete w</a>. Windows 7 genuine activation removewat 2.2.6.0 rar download <a href=http://removewat-windows-genuine-activation.shkolamd.ru/>special info</a>. Software Activation over the Internet <a href=http://manager-internet-01net-patch.shkolamd.ru/><img src=\"https://i.ytimg.com/vi/u4HrZTeLmKQ/hqdefault.jpg\"></a>. CS: GO Static Crosshair Plugin for CS 1.6 <a href=http://mais-patch.shkolamd.ru/><img src=\"https://i.ytimg.com/vi/u89lUvfxb5I/hqdefault.jpg\"></a>. Free Lame Mp3 Encoder X 2 2.0.2.20 download <a href=http://music-keygen-router.shkolamd.ru/><img src=\"https://i.ytimg.com/vi/rCHPv9R5GgU/hqdefault.jpg\"></a>. Battlefield 2: Special Forces Videos for PC <a href=http://forces-patch-battlefield-special.shkolamd.ru/><img src=\"https://i.ytimg.com/vi/OK57AVgBOt4/hqdefault.jpg\"></a>. Lotr Bfme2 Crack Serial Keygen <a href=http://crack-bfme2-lotr.shkolamd.ru/>see this website</a>. VoyForums: Advices and Vices <a href=http://patch-music-brood.shkolamd.ru/>shkolamd.ru</a>. Vcds Lite 1.0 Crack Loader Rar 128 - Sydney Heartburn Clinic <a href=http://vcds-cracked-lite.shkolamd.ru/>navigate to this website</a>. <a href=http://hack-2sharedcom.shkolamd.ru/>Total Commander 9.51 Final with Key</a>. Real Life Videos - Kettler World Tours <a href=http://version-presser.shkolamd.ru/>shkolamd.ru</a>. <a href=http://diskdigger-license.shkolamd.ru/>4K Video Downloader 4.13.4.3930 Crack And Keygen  2020 </a>. Vicks Filter Free Humidifier V4450 Cool Mist: Amazon.ca <a href=http://hack-v4450.shkolamd.ru/>shkolamd.ru</a>. Guitar Pro (free version) download for PC <a href=http://guitar-patch.shkolamd.ru/>see this</a>. Comfy Partition Recovery 3.0 With Crack  Latest  <a href=http://folder-activation-windows.shkolamd.ru/>http://folder-activation-windows.shkolamd.ru/</a>. <a href=http://hack-shaiya.shkolamd.ru/>Spectral-shaiya.software.informer.com. Spectral shaiya</a>. <a href=http://edge-crack-reloaded-mirrors.shkolamd.ru/>Gamenet: Mirrors Edge Slow Motion PhysX Fix</a>. <a href=http://full-game-zombies-plants-crack.shkolamd.ru/>Plants Vs Zombies 3 1 for Android - Free downloads and</a>. <a href=http://schedule-cracked-version-lifetime.shkolamd.ru/>AVG TuneUp 20.1 Build 1997 Activation Code - Crack Key 2020</a>. <a href=http://wifi-wireless-network-hacker-password.shkolamd.ru/>Omniksol WIFI Kit User Manual</a>. <a href=http://money-adder-payza-security.shkolamd.ru/>Freeware password generator warez serial crack</a>. Hacker Typer - Geek Prank Hacker Simulator Online <a href=http://online-games.shkolamd.ru/><img src=\"https://i.ytimg.com/vi/R9U9M47hMpA/hqdefault.jpg\"></a>. ProWritingAid - the best grammar checker, style editor <a href=http://character-able-ready-hacked-battle.shkolamd.ru/>http://character-able-ready-hacked-battle.shkolamd.ru/</a>. <a href=http://credits-rivals-client-urban-hack.shkolamd.ru/>Urban Rivals Credits And Client Hack V3.0 FreeDownload</a>. Shadow Warrior 2 GAME CHEATS (SKILL POINT HACK) <a href=http://points-games-hack.shkolamd.ru/>shkolamd.ru</a>. Top 100 Current Players - 2K Ratings <a href=http://patch-warren.shkolamd.ru/>full report</a>. The Elder Scrolls: Blades 1.0.2.763396 (Early Access <a href=http://master-garena-hack.shkolamd.ru/>official statement</a>. ', 'comment', 'waiting', '0', '0');

-- ----------------------------
-- Table structure for typecho_contents
-- ----------------------------
DROP TABLE IF EXISTS `typecho_contents`;
CREATE TABLE `typecho_contents` (
  `cid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `created` int(10) unsigned DEFAULT '0',
  `modified` int(10) unsigned DEFAULT '0',
  `text` longtext,
  `order` int(10) unsigned DEFAULT '0',
  `authorId` int(10) unsigned DEFAULT '0',
  `template` varchar(32) DEFAULT NULL,
  `type` varchar(16) DEFAULT 'post',
  `status` varchar(16) DEFAULT 'publish',
  `password` varchar(32) DEFAULT NULL,
  `commentsNum` int(10) unsigned DEFAULT '0',
  `allowComment` char(1) DEFAULT '0',
  `allowPing` char(1) DEFAULT '0',
  `allowFeed` char(1) DEFAULT '0',
  `parent` int(10) unsigned DEFAULT '0',
  `hit` int(10) unsigned DEFAULT '1',
  `views` int(10) DEFAULT '0',
  `viewsNum` int(10) DEFAULT '0',
  `agree` int(11) DEFAULT '0',
  PRIMARY KEY (`cid`),
  UNIQUE KEY `slug` (`slug`),
  KEY `created` (`created`)
) ENGINE=MyISAM AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of typecho_contents
-- ----------------------------
INSERT INTO `typecho_contents` VALUES ('1', '唯美情话(持续更新中...)', 'start', '1579421100', '1579421220', '<!--markdown-->![yusheng.jpg][1]\r\n[Meting]\r\n[Music server=\"netease\" id=\"1347110044\" type=\"song\"/]\r\n[/Meting]\r\n\r\n你可要想好，这一生，你只能遇见我一次，请三思！\r\n\r\n我寻了半生的春天,你一笑 便是了...\r\n\r\n我深深的喜欢着这个,有你的世界\r\n\r\n很愿意有幸成为彼此最重要的人\r\n\r\n我见过春日夏风秋叶冬雪，也踏遍南水北山东麓西岭~ 可这四季春秋 苍山泱水，都不及你冲我展眉一笑！\r\n\r\n你住的城市下雨了,很想问问你有没有带伞,可是我忍住了,因为我怕你说没带,而我又无能为力,就像我爱你却给不了你想要的陪伴!\r\n\r\n浮世万千，吾爱有三，日月与卿，日为朝，月为暮，卿为朝朝暮暮。\r\n\r\n好想变成雪啊,这样就可以落在姑娘的肩上了 “若是姑娘撑了伞呢? “那就落在姑娘的油纸伞上,静载一路的月光。 “若是姑娘将雪拂去 “那就任她拂去,能在她的手掌上停留一刻,便足矣\r\n\r\n你是信的开头诗的内容 童话的结尾 \r\n\r\n你是理所当然的奇迹 你是月色真美 \r\n\r\n你是圣诞老人送给我 好孩子的礼物 \r\n\r\n你是三千美丽世界里 我的一瓢水\r\n\r\n说好从今以后都牵着手,因为要走很远\r\n\r\n\r\n炊烟袅袅，我在门前等你。 夕阳西下， 我在山边等你。叶子黄了， 我在树下等你。月儿弯了， 我在十五等你。 细雨来了， 我在伞下等你。 流水冻了， 我在河畔等你。生命累了， 我在天堂等你。 我们老了， 那我就在来生等你。一生只够爱一个人!\r\n\r\n前世你叫我妖仙姐姐\r\n今生我叫你道士哥哥\r\n前世我叫你二货道士\r\n今生你叫我一声小蠢货\r\n前世你是东方月初\r\n今生我是涂山苏苏\r\n东方月初爱他的妖仙姐姐\r\n白月初爱他的小蠢货\r\n\r\n梵云飞的“对不起！下次不敢了！”王权富贵的“如果我们能活着出去的话，万水千山，你愿意陪我一起看吗？”东方月初的“我愿意啊，五十年前就愿意了。。。”北山妖帝的“那年我守住了城，却没守住我的爱人。”颜如玉的“只要你能幸福，我是谁，又有什么关系，记不记得住，又有什么关系啊！”<狐妖小红娘>\r\n\r\n有些人忙于生活、有些人甘于平凡、有些人还在寻找方向、有些人还没准备好，就长大了、有些人还没活明白，就老了。我呀，趁现在还年轻，我还要再奋斗一次。如果有机会，我想对年少的自己说：对不起，我没能成为你想变成的那个英雄——<陈翔六点半之铁头无敌>\r\n\r\n孩子，穷怎么了？穷也要挺起胸膛来，让别人看看，你不仅穷而且还矮。矮又如何？抬起你的头来，让他们知道，你不仅矮，而且还丑！丑不要紧，用你的言谈举止让其他人明白：你还是一个没有内涵的人。没有内涵也不要放弃，从现在开始学习。当你读了足够多的书的时候，你会发现：自己还笨。别看那些有钱人表面上很风光，其实，他们私底下更风光。如果你的脑海里出现了“放弃”两个字，一定要坚定地告诉自己：你都一无所有了，哪来东西让你放弃\r\n\r\n我是个俗气至顶的人，见山是山，见海是海，见花便是花。唯独见了你，云海开始翻涌，江潮开始澎湃，昆虫的小触须挠着全世界的痒。你无需开口，我和天地万物便通通奔向你。从前，我见山是大地峻冷的脊背，见水是星象的眼泪，见雪是世纪的白象，直到见了你，我踏过土地，捧起泉水，接过下落的雪花，万物都于我有了生灵，你无需开口，就让我从虚空下坠，落地开花。王小波《爱你就像爱生命》\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/10/1250944072.jpg', '0', '1', null, 'post', 'publish', null, '1', '1', '1', '1', '0', '1', '3175', '510', '0');
INSERT INTO `typecho_contents` VALUES ('3', '关于', 'about', '1570333200', '1575703582', '<!--markdown-->张旭辉,网名初心,非典型宅男,IT男,编程爱好者,擅长Python,Php,JavaScript等编程语言. 充满正能量,对生活充满憧憬,对未来充满幻想.一个不甘寂寞,努力奋斗,拼命折腾,希望自己可以在有生之年有所作为的人.相较同龄人而言,有着超乎常人的成熟,睿智,冷静.思想容易天马行空,不着边际.思维活跃,能言善辩.爱生活,爱幻想,多愁善感,容易感动.乐观,开朗,幽默,自信,积极向上,崇尚用正能量影响到身边的每一个人,拒绝抱怨,拒绝负面情绪.\r\n\r\n兴趣爱好:\r\n爱运动,爱健身:跑步,游泳,篮球,羽毛球,乒乓球,台球\r\n电影:烧脑电影 励志电影 喜剧电影,科幻电影 漫威迷 DC迷\r\n爱读书:心理学,哲学,推理学,都市小说,信奉书中自有黄金屋...的道理\r\n增长自己见识阅历最快的方法,读书,电影,旅行\r\n古风音乐爱好者:喜欢的歌手 双笙 封茗囧菌 熙影CRITTY 银临 玄觞 天时\r\n云之泣 小时姑娘 HITA 小爱的妈 梦璟saya 少年霜 晴小瑶 少司命\r\n董贞 青弄 许多葵 凌之轩 梨花少 刘珂矣 绯村柯北 ....等等 太多不一一列举\r\n网易云音乐歌单(持续更新中...):\r\n**http://music.163.com/#/playlist?id=163291171**\r\n\r\n\r\n----------\r\n\r\n\r\n[Meting]\r\n[Music server=\"netease\" id=\"163291171\" type=\"playlist\"/]\r\n[/Meting]\r\n\r\n汉服爱好者:华夏复兴 衣冠先行 始自衣冠，达于博远\r\n爱好二次元:动漫迷 B站粉 初音粉 colsplay\r\n痛车爱好者 伪音爱好者 声控\r\n\r\n个人信息:\r\nQQ:137647337\r\n微信:itzhangxuhui\r\n\r\n\r\n关于版权:\r\n旭辉博客的博文均基于创作共享的知识共享姓名標示-非商業性-相同方式分享 4.0 國際 (CC BY-NC-SA 4.0)发布。 所有转载的博文将会在文章底部版权模块中加注【转载:原文链接】超链接标识。转载本站博文时请务必以超链接形式标明源文出处，否则谢绝一切转载！若发现转载不留链或恶意修改本站原创版权，尤其是公司企业性质的组织或团体，本人将追究到底，必要时可能会采取极端手段！\r\n\r\n当然，若发现本站有任何侵犯您利益的内容，请及时邮件或留言联系，我会第一时间删除所有相关内容。', '0', '1', null, 'page', 'publish', null, '1', '1', '1', '1', '0', '1', '2179', '625', '0');
INSERT INTO `typecho_contents` VALUES ('42', '留言', 'message', '1573611182', '1573611182', '<!--markdown-->有什么想对我说的吗?', '0', '1', null, 'page', 'publish', null, '9', '1', '1', '1', '0', '1', '3121', '964', '0');
INSERT INTO `typecho_contents` VALUES ('4', 'yusheng.jpg', 'yusheng-jpg', '1570334092', '1570334092', 'a:5:{s:4:\"name\";s:11:\"yusheng.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/10/1250944072.jpg\";s:4:\"size\";i:15776;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '1', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('5', '中国式英语-神翻译', '5', '1570334880', '1570336487', '<!--markdown-->![chineseenglish.jpg][1]\r\n1. 爱TMD谁谁谁：love he mother’s who who who\r\n\r\n2. 白痴：White eat!\r\n\r\n3. 板门弄斧：play an ax before Luban\r\n\r\n4. 班长：class long\r\n\r\n5. 彼此彼此：you me you me\r\n\r\n6. 表妹：watch sister\r\n\r\n7. 别唬我：don’t tiger me\r\n\r\n8. 不管三七二十一：no care three seven two ten one\r\n\r\n9. 不入虎穴，焉得虎子：Blue who say，and whose\r\n\r\n10. 不三不四： no three no four\r\n\r\n11. 不要开黄腔：do not open yellow gun\r\n\r\n12. 车祸现场描述 ：one car come， one car go ,two car peng－peng, people die\r\n\r\n13. 呈现强烈的企图心：Demonstrate the strong attempt heart\r\n\r\n14. 吃白食：eat white food\r\n\r\n15. 春江水暖鸭先知：spring river water warm duck first know\r\n\r\n16. 大人不计小人过：Big people do not think of small people’s mistake\r\n\r\n17. 第一眼看到你，我就爱上你了：first eye see you, i shit love you\r\n\r\n18. 电源线：power line\r\n\r\n19. 放马过来，给你点颜色看看：release your horse and come, I’ll give you some color to see see\r\n\r\n20. 蜂拥而至：go out like bee\r\n\r\n21. 给你点颜色看看：I’ll show you some color\r\n\r\n22. 给你脸你不要脸，你丢脸，我翻脸：I give you face you don’t wanna face,you lose you face ,I turn my　face\r\n\r\n23. 恭喜发财：go high fuck try\r\n\r\n24. 狗娘养的：dog mother born\r\n\r\n25. 关公面前耍大刀：play a big knife before Guangong\r\n\r\n26. 好多人死了，你怎么不去死！：How many pople go to die, why do not you go to die\r\n\r\n27. 好好学习、天天向上：good good study, day day up\r\n\r\n28. 好久不见：long time no see （这个用法已经被当地的美国口语，尤其是被文化程度不高的群体所接受）\r\n\r\n29. 红颜知己：red face know me\r\n\r\n30. 加油：add oil\r\n\r\n31. 救人一命，胜造七级浮屠： save man one life, betther than building up 7-floor tower\r\n\r\n32. 开水：open water\r\n\r\n33. 看不看：see no see?\r\n\r\n34. 抗日游行：resist sun swim go\r\n\r\n35. 课间操：lesson between fuck\r\n\r\n36. 老表：old watch\r\n\r\n37. 龙生龙，凤生凤，老鼠的儿子会打洞 ：dragon born dragon, chicken born chicken, mouse’s son can make hole\r\n\r\n38. 马马虎虎：horse horse tiger tiger\r\n\r\n39. 没脸见人：have no face see person\r\n\r\n40. 美中不足：American Chinese not enough\r\n\r\n41. 面试：face try\r\n\r\n42. 明天谁起得早谁叫谁：tomorrow morning who get up early, who call who!\r\n\r\n43. 哪凉快哪呆着去where cool where you stay!\r\n\r\n44. 你爱我吗：you love I?\r\n\r\n45. 你不鸟我，我也不鸟你：you don’t bird me, so I don’t bird you\r\n\r\n46. 你给我等着：you give me wait\r\n\r\n47. 你给我滚出去：you gave me get out\r\n\r\n48. 你给我记住：you give me remember\r\n\r\n49. 你给我站住：you give me stop\r\n\r\n50. 你没看，我现在非常忙，一边玩去：You no see, I now very busy. One side play go\r\n\r\n51. 你妻子真漂亮/哪里哪里：your wife is beautiful/where where\r\n\r\n52. 你去不去？你不去我去！：You go no go? You no go I go!\r\n\r\n53. 你认为你是谁？：What do you think ,who are you ?\r\n\r\n54. 你TMD：you he mother’s\r\n\r\n55. 你问我，我去问谁：you ask me, me ask who\r\n\r\n56. 你丫要敢唬我，我他妈扇你！：IF you tiger me,I will mountain you!\r\n\r\n57. 你有两下子：you have two down son\r\n\r\n58. 你有种，我要给你点颜色瞧瞧，兄弟们，一起上：you have seed， I will give you some color to see see,brothers！ together up ！\r\n\r\n59. 你真有两下子：you really have tow down son\r\n\r\n60. 骑驴看唱本，走着瞧：riding a donkey reading play , go and see\r\n\r\n61.朝三暮四 :morning three night four\r\n\r\n62.滴水之恩,涌泉相报.you di da di da me,i hua la hua la you\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/10/3253787230.jpg', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '1526', '215', '0');
INSERT INTO `typecho_contents` VALUES ('6', 'chineseenglish.jpg', 'chineseenglish-jpg', '1570335068', '1570335068', 'a:5:{s:4:\"name\";s:18:\"chineseenglish.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/10/3253787230.jpg\";s:4:\"size\";i:19102;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '5', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('7', 'hunyin.jpg', 'hunyin-jpg', '1570335492', '1570335492', 'a:5:{s:4:\"name\";s:10:\"hunyin.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/10/2896293986.jpg\";s:4:\"size\";i:23611;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '8', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('8', '“孩子高考后就离婚”，我以为只是妻子的气话，原来她准备了十年', '8', '1570335480', '1570336467', '<!--markdown-->![hunyin.jpg][1]\r\n孩子高考后就离婚，他以为只是妻子的气话，原来她准备了十年！\r\n有些女人连离婚都是优雅的。这个故事朋友圈都刷了屏了。\r\n一个刚离婚的男人谈起自己前妻，他说，我们从民政局出来的时候，我是哭着的。她却是笑着的。她跟我说，她为了这一天，等了十年。儿子的十年寒窗苦读，就是她十年的卧薪尝胆。\r\n她跟他说过很多次，等孩子高考结束后就离婚。他以为是妻子的一时气话，却不想妻子说的是真的。并且从那时候开始，就已经为离婚做打算了。\r\n他想想妻子这几年的独立和温柔，突然发现那些他自以为是的和谐，不过是妻子已经不想跟他计较了。\r\n他以前特别讨厌妻子让他干家务活，后来也不知道从哪天开始，妻子就不让他干了。他当时还觉得很开心，女人都是这样的，不惯着她，她自己不照样就做家务活了。\r\n现在想想，那个时候妻子已经开始放弃她了。她不需要他了，于是就什么都自己干了。\r\n妻子大概有好几年，没有给他要过钱了。以前，孩子小，妻子不上班的时候，妻子每个月都会给他要钱。他记得那个时候，他会特别烦妻子给他要钱。自己一个月就挣那么点都花在家里了。每次妻子给他要钱的时候，他就会说：“你就不能少花点，客厅买什么空调啊，锁坏了就先凑合用。你天天带个孩子买那些衣服化妆品有什么用，你就不能心疼我一点吗？男人挣个钱容易吗？”\r\n等孩子上小学了之后，妻子就出去上班了，之后，她再也没给他要过一分钱。他们的钱从那时候开始就完全分开了。妻子想要买什么，就自己买。他自己想买什么也自己买。他不知道妻子一个月挣多少钱，妻子也不会管他一个月挣多少钱。\r\n他记得有一段时间，妻子突然花钱就特别多了。给自己买了很多衣服和好的化妆品。他当时还怪她：“你不能省着点钱花，以后孩子还要上学呢。”\r\n结果她妻子第一次回怼了他：“我自己挣钱自己花，不花你一分钱，你管不着我。孩子长大了，你出多少钱，我出多少，只比你多，不比你少。”\r\n他到现在才明白，那时候的妻子估计子在经济上就已经不需要他了。\r\n不，妻子的从那些年开始，独立不仅仅是经济，还有精神。她开始很少跟他吵架。他说什么，她就听着。不想听了，她就会躲到别的房间去。他那时候还觉得是妻子终于变得贤惠温柔了，却不想那是妻子连跟他吵架都觉得没必要了。\r\n他想想自己的那几年，真的是想做什么做什么，不管做什么，妻子都是不管的。有一次他一晚上没回家，妻子也没打一个电话。当时他还笑话那些被女人催的男人，觉得他们没本事，被妻子管的严。\r\n现在想想，人家的妻子至少还有爱，但是他的妻子早就不爱他了。他竟然还觉得骄傲，觉得这个女人终于清静了。\r\n他再深想，那些年妻子为孩子的事都不会跟他吵架。因为，她包揽了孩子的一切，而他也乐意什么都不管。他那时候还沾沾自喜过一件事，孩子上的所有补习班的钱都是妻子出的，他当时还沾沾自喜地想：“花吧，替我养孩子了，不用我花钱随便你报。”\r\n十年，妻子自己挣钱，自己带娃，自己料理一切家务。甚至是妻子的父母有什么事，妻子都未曾对自己开口求助过。他曾经为这些事沾沾自喜，觉得妻子就该是这样的。\r\n他甚至觉得，妻子给他找一点麻烦，他就会觉得妻子烦。他不想管妻子的任何事，只要妻子还给他提供服务，不给他要钱，妻子干什么他都不关心。\r\n还有一次妻子生病，妻子先给他打的电话。他清楚地记得，他当时的说法：“你娘家没人管你了，你自己不是有钱吗，找我干什么。”\r\n妻子什么都没说就挂了电话。后来，妻子好了，他心里有点愧疚，他以为妻子会哭闹，但是妻子却当什么都没发生过。他觉得妻子也不过如此，他真不管她，她也不能怎样。\r\n他从未想过，这样“安分”的妻子跟他离婚的时候，能那么坚决。\r\n那种坚决是她在漫长的婚姻中攒够的寒吗？\r\n就像妻子有一次说的：“你早就不是我的丈夫了，但是我儿子的父亲。”\r\n于是，她忍了十年，准备了十年，等到孩子高考了，孩子大了，她就彻底离开自己了。\r\n想想，他确实没有什么值得妻子留恋的。因为，在这个婚姻里，他自己都找不出来，自己曾经给予过妻子什么。唯有一个孩子，是他们唯一的牵扯。\r\n从民政局出来，他是哭的。因为，他不敢想未来，他以后要自己做饭洗衣服，要自己整理家务，要自己面对所有的事。他这辈子都喝不上妻子熬的那些营养汤了。\r\n而妻子是笑的，婚姻于她，没有任何好处。离婚后，妻子不过是少照顾一个脾气不好的人罢了。\r\n男人不要等到孩子长大后，才懂得珍惜妻子。\r\n孩子小时候，你觉得这个女人不会离开你。只要你不犯大错，这个女人为了孩子也会忍着你，容着你\r\n你仗着她生养孩子时候的不强大，嫌弃她，看不起她，未曾给过她一点关爱。\r\n你仗着她爱孩子的心，不想让孩子成长在一个不完整的家庭里，漠视她，冷暴力，甚至是只把她当成一个免费的保姆。\r\n却不想，她那时候不过是没有选择。当她积攒够了力量，当她足够强大，当孩子已经长大，她会毫不犹豫地离开你。\r\n余生，有你，太累。\r\n余生，没你，何其快活。\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/10/2896293986.jpg', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '1396', '191', '0');
INSERT INTO `typecho_contents` VALUES ('9', 'rain.jpg', 'rain-jpg', '1570335663', '1570335663', 'a:5:{s:4:\"name\";s:8:\"rain.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/10/1148446509.jpg\";s:4:\"size\";i:28244;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '10', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('10', '你说你喜欢雨，下雨天你却撑起了伞', '10', '1570335660', '1570336455', '<!--markdown-->![rain.jpg][1]\r\n\r\n英文版：\r\n\r\nYou say that you love rain,\r\n\r\nbut you open yourumbrella when it rains.You say that you love the sun,\r\n\r\nbut you finda shadow spot when the sun shines.You say that you love the wind,\r\n\r\nbut you closeyour windows when wind blows.This is why I am afraid, when you say thatyou love me too.\r\n\r\n普通版：\r\n\r\n你说你喜欢雨，但是下雨的时候，你却撑开了伞；\r\n\r\n你说你喜欢阳光，但当阳光播撒的时候，你却躲在阴凉之地；\r\n\r\n你说你喜欢风，但清风扑面的时候，你却关上了窗户。\r\n\r\n我害怕你对我的爱也是如此。\r\n\r\n文艺版：\r\n\r\n你说烟雨微芒，兰亭远望；后来轻揽婆娑，深遮霓裳。\r\n\r\n你说春光烂漫，绿袖红香；后来内掩西楼，静立卿旁。\r\n\r\n你说软风轻拂，醉卧思量；后来紧掩门窗，漫帐成殇。\r\n\r\n你说情丝柔肠，如何相忘；我却眼波微转，兀自成霜。\r\n\r\n诗经版：\r\n\r\n子言慕雨，启伞避之。\r\n\r\n子言好阳，寻荫拒之。\r\n\r\n子言喜风，阖户离之。\r\n\r\n子言偕老，吾所畏之。\r\n\r\n离骚版：\r\n\r\n君乐雨兮启伞枝，君乐昼兮林蔽日，\r\n\r\n君乐风兮栏帐起，君乐吾兮吾心噬。\r\n\r\n五言打油诗版：\r\n\r\n恋雨偏打伞，爱阳却遮凉。\r\n\r\n风来掩窗扉，叶公惊龙王。\r\n\r\n片言只语短，相思缱倦长。\r\n\r\n郎君说爱我，不敢细思量。\r\n\r\n七言绝句版：\r\n\r\n恋雨却怕绣衣湿，喜日偏向树下倚。\r\n\r\n欲风总把绮窗关，叫奴如何心付伊。\r\n\r\n词牌版：\r\n\r\n君曰喜雨凭栏望，启伞避雨茫；\r\n\r\n爱阳寻阴遮凉，悦风却闭门窗。\r\n\r\n恋佳人、惜花香、易成殇，\r\n\r\n春去残红，君离卿去最断肠。\r\n\r\n七律压轴版：\r\n\r\n江南三月雨微茫，罗伞叠烟湿幽香。\r\n\r\n夏日微醺正可人，却傍佳木趁荫凉。\r\n\r\n霜风清和更初霁，轻蹙蛾眉锁朱窗。\r\n\r\n怜卿一片相思意，犹恐流年拆鸳鸯。\r\n\r\n女汉子版：\r\n\r\n你有本事爱雨天，你有本事别打伞啊！\r\n\r\n你有本事爱阳光，你有本事别乘凉啊！\r\n\r\n你有本事爱吹风，你有本事别关窗啊！\r\n\r\n你有本事说爱我，你有本事别停止啊！\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/10/1148446509.jpg', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '1907', '258', '0');
INSERT INTO `typecho_contents` VALUES ('11', '什么时候可以出新版本-程序员鼓励师.jpg', '什么时候可以出新版本-程序员鼓励师-jpg', '1570335800', '1570335800', 'a:5:{s:4:\"name\";s:53:\"什么时候可以出新版本-程序员鼓励师.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/10/2173989492.jpg\";s:4:\"size\";i:171884;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '12', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('12', '史上最污技术解读，让你秒懂IT术语', '12', '1570335780', '1573616368', '<!--markdown-->![什么时候可以出新版本-程序员鼓励师.jpg][1]\r\n\r\n假设你是一位妹子，你的男友沉迷游戏经常不接电话无故宕机，所以当你们约好下午逛街以后你要时不时地打个电话询问，看看他是不是还能正常提供服务，这叫心跳检测。\r\n\r\n假设你是一位妹子，你想去逛街而你的男友A在打游戏不接电话，于是乎你把逛街的请求发给了替补男友B，从而保障服务不间断运行，这叫故障切换。\r\n\r\n假设你是一位妹子，你有很多需要男朋友完成的事情，于是乎你跟A逛街旅游吃饭不可描述，而B只能陪你逛街，不能拥有全部男朋友的权利，这叫主从配置 master-slave。\r\n\r\n假设你是一位妹子，你的需求太强烈以至于你的男友根本吃不消，于是乎你找了两个男朋友，一三五单号，二四六双号限行，从而减少一个男朋友所面临的压力，这叫负载均衡。\r\n\r\n假设你是一位妹子，并且有多个男朋友，配合心跳检测与故障切换和负载均衡将会达到极致的体验，这叫集群LVS。注意，当需求单机可以处理的情况下不建议启用集群，会造成大量资源闲置，提高维护成本。\r\n\r\n假设你是一位妹子，你的需求越来越高导致一个男朋友集群已经处理不了了，于是乎你又新增了另外几个，这叫多集群横向扩容，简称multi-cluster grid。\r\n\r\n假设你是一位妹子，你的男朋友身体瘦弱从而无法满足需求，于是乎你买了很多大补产品帮你男朋友升级，从而提高单机容量，这叫纵向扩容。切记，纵向扩容的成本会越来越高而效果越来越不明显。\r\n\r\n假设你是一位妹子，你跟男友经常出去游玩，情到深处想做点什么的时候却苦于没有tt，要去超市购买，于是乎你在你们经常去的地方都放置了tt，从而大幅度降低等待时间，这叫CDN。\r\n\r\n假设你是一位妹子，你的男朋友英俊潇洒风流倜傥财大气粗对你专一，于是乎你遭到了女性B的敌视，B会以朋友名义在周末请求你男朋友修电脑、修冰箱，占用男朋友大量时间，造成男朋友无法为你服务，这叫拒绝服务攻击，简称DOS。\r\n\r\n假设你是一位妹子，你因男朋友被一位女性敌视，但是你男朋友的处理能力十分强大，处理速度已经高于她的请求速度，于是她雇佣了一票女性来轮流麻烦你的男朋友，这叫分布式拒绝服务攻击，简称DDOS。\r\n\r\n假设你是一位妹子，你发现男朋友总是在处理一些无关紧要的其它请求，于是乎你给男朋友列了一个白名单，要求他只处理白名单内的请求，而拒绝其它身份不明的人的要求，这叫访问控制，也叫会话跟踪。\r\n\r\n假设你是一位妹子，你发现采取上述措施以后男朋友的处理请求并没有减少很多，于是你经过调查发现，有人伪造你的微信头像、昵称来向你的男朋友发起请求，这叫跨站点请求伪造，简称CSRF。\r\n\r\n假设你是一位妹子，你收到了一份快递，于是你要求男朋友给你取快递，当你拿到快递以后发现有人给你邮寄了一封通篇辱骂的信件，这叫跨站点脚本攻击，简称XSS。请注意，对方完全可以给你邮寄微型窃听器来窃听你的隐私。\r\n\r\n假设你是一位妹子，为了应对威胁，你要求你的男朋友对邮寄给你的邮件必须检查，这叫数据校验与过滤。\r\n\r\n假设你是一位妹子，你的男朋友太优秀而遭人窥视，于是乎他们研究了一下你的男朋友，稍微修改了一点点生产出一个男朋友B，与你的男朋友百分之99相似，这不叫剽窃，这叫逆向工程，比如男朋友外挂。\r\n\r\n假设你是一位妹子，你要求你的男朋友坚持十分钟，然后十五分钟继而二十分钟以测试你男朋友的极限在哪里，这叫压力测试。压力测试的目的是查看男朋友是否可以处理需求从而决定是否启用男朋友集群或提升男朋友处理能力，不要对线上运行的男朋友做压力测试，可能会造成宕机的后果，会血本无归的。\r\n\r\n假设你是一位妹子，为了保证你男朋友的正常运行，于是乎你每天查看他的微信微博等社交资料来寻找可能产生问题的线索，这叫数据分析。\r\n\r\n假设你是一位妹子，你的男朋友属于社交活跃选手，每天的微博知乎微信生产了大量信息，你发现自己的分析速度远远低于他生产的速度，于是乎你找来你的闺蜜一起分析，这叫并行计算。\r\n\r\n假设你是一位妹子，你的男朋友太能折腾处处留情产生了天量的待处理信息，你和你的闺蜜们已经累趴也没赶上他创造的速度，于是你付费在知乎上找了20个小伙伴帮你一起分析，这叫云计算。\r\n\r\n假设你是一位妹子，在使用云计算后获得了大量整理好的男朋友数据，这些数据如：\r\n\r\n地点       活跃时间段       活跃次数\r\n\r\n如家       xxxx            123次\r\n\r\n汉庭       xxxx             45次\r\n\r\n...\r\n\r\n这叫数据统计。\r\n\r\n假设你是一位妹子，你在得到男朋友经常出没的地点后，根据酒店、敏感时间段等信息确定男朋友应该是出轨了，这叫数据挖掘。\r\n\r\n假设你是一位妹子，在分析男友的数据后，得知他下午又要出去开房，于是乎你在他准备出门前给他发了个短信，问他有没有带tt，没有的话可以在我这里买，这叫精准推送，需要配合数据挖掘。\r\n\r\n假如你是一位妹子，你的男朋友总出去浪而各种出问题，于是乎你租了间屋子并准备好了所有需要的东西并告诉他，以后不用找酒店了，直接来我这屋子吧，什么都准备好了，这叫容器。\r\n\r\n假如你是一位妹子，你每天都要和男朋友打通一次接口，这叫采集数据。你的男朋友用来连接你和他的工具，叫做接口“机”，你采集到的数据叫做“流”数据。你一天24小时不停地采，这叫实时数据采集。你决定开发新的接口来和男朋友交流，这叫虚拟化。你决定从不同的男友身上采集数据，你就是大数据中心。有一天你决定生一个宝宝，这叫大数据应用。宝宝生下来不知道是谁的，这叫大数据脱敏。但是从宝宝外观来看，黑色皮肤金色头发，这叫数据融合跨域建模。你决定把这个宝宝拿来展览收点门票，这叫大数据变现。（来自知乎作者@彩色郁金香）\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/10/2173989492.jpg', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '1635', '230', '0');
INSERT INTO `typecho_contents` VALUES ('13', 'code.gif', 'code-gif', '1570336186', '1570336186', 'a:5:{s:4:\"name\";s:8:\"code.gif\";s:4:\"path\";s:35:\"/usr/uploads/2019/10/1179308819.gif\";s:4:\"size\";i:59939;s:4:\"type\";s:3:\"gif\";s:4:\"mime\";s:9:\"image/gif\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '14', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('14', '为什么你应该开始学习编程了？', '14', '1570336140', '1577544500', '<!--markdown-->![code.gif][1]\r\n有一家饭店的大厨，烧得一手好菜，经过口碑相传，客人从五湖四海闻名而来。然而这对饭店的老板来说，并不单纯是一个好消息。因为客人不是奔着饭店，而是奔着大厨的手艺来的。老板必须想办法留住这位大厨，否则他一旦被别人挖走，饭店的生意就会一落千丈了。然而即便老板不惜血本保证了大厨的忠诚度，风险也依然存在：\r\n\r\n大厨休息或请假的时候，菜品的口味就无法让顾客满意；\r\n大厨只有一个，如果想在多个地方开分店，那口味也就不能保证了；\r\n大厨再厉害，同时也只能炒一个菜，而顾客越来越多，输出总是供不应求；\r\n大厨年纪大了总是要退休的，如果收徒的话，怎么继续保证徒弟的忠诚度呢？\r\n有一天，老板突然悟到，决定菜品口味的是大厨烧菜的过程，而不是大厨本人。如果大厨愿意把自己每个菜的菜谱都写出来，那不就可以请别的厨师来操作了吗？虽然别人按照菜谱烧出来可能达不到原味的100分，但90分总是能达到的，而这样的差距是一般的食客无法分辨的。这样只要菜谱在，饭店菜品的质量就能得到保证，扩张店面、开分店什么的就都不成问题了。\r\n\r\n于是老板和大厨谈了一个晚上，说服了他用菜谱技术入股。几年后，饭店生意越来越好，开了无数家分店，老板赚了很多钱，大厨也得到丰厚的分成，不用再天天靠手艺吃饭了。\r\n\r\n什么是编程？\r\n所谓程序，就是为了实现一个需求而设计的逻辑流程。大厨的菜谱就是程序。正是因为有程序作为媒介，我们才可以分离设计者、调用者和执行者的角色。虽然程序的执行效果未必能让其设计者满意，但它已经可以脱离设计者的时间和空间局限而存在，可以被其他人执行、验证和改进。\r\n\r\n所谓编程，就是指编写程序。将烧菜的过程写成菜谱，本质上就是在编程。通过编程，我们更加透彻地理解并阐述了事物的本质，让曾经专属于某个人、某个团体、某个地方甚至某个时刻的东西，得以独立的存在和发展。从这个角度来讲，说编程是在创造生命也不为过。\r\n\r\n编程可以说是一种标准化的写作。标准化保证了服务和产品的质量，也使大规模复制和扩张成为可能。肯德基正是依赖其详尽到炸薯条的油温秒数的食品加工手册，才将分店开满世界各地的。\r\n\r\n我们每天的生活，都在和各种技术产品和服务打交道，比如导航、搜索引擎、聊天软件……你可曾想过，这些产品和服务背后的原理是什么？相信除了程序员群体之外，绝大多数人不会去想这些问题，因为：\r\n\r\n这跟我没有关系啊！我又不去干这行……\r\n应该只有专业人士才能搞懂吧，我可不行！\r\n能用就行了，想那么多干啥，多累呀！\r\n编程的本质就是：设计一个逻辑流程来实现指定的需求，使调用者无需了解实现细节即可达到目的。他们这么想并没有错，因为编程的本质就是：设计一个逻辑流程来实现指定的需求，使调用者无需了解实现细节即可达到目的。\r\n由于程序和编程的广义概念太过笼统，为不导致概念混淆，下文中提到的“程序”和“编程”，特指使用计算机编程语言编写，由机器来运行的程序。\r\n\r\n解放时间和注意力\r\n假设你已经在某岗位工作了一段时间，根据经验总结出来每天下班前有下面三件事需要做：\r\n\r\nA、在公司内网系统查询某业务当天的数据（约10分钟）\r\nB、整理成日报表并存档（约15分钟）\r\n\r\nC、把几个核心数据用电子邮件发送给领导（约5分钟）\r\n\r\n你可以在认真梳理过之后，将这个流程写下来，贴在办公桌前，或者记在脑子里。这自然比那些不知道流程的人要强得多，你可以确保每个步骤都不被遗漏地执行到（然而这并不能完全保证）。虽然你已经花了时间认真思考过，但每天这样枯燥乏味的流程都需要自己执行一遍……等等，这活我不可以找个秘书来干吗？\r\n\r\n当然可以，雇佣他人是一种用金钱换时间的解决方案。但是这同时又引入了许多新的问题：\r\n\r\n你需要花钱（废话）\r\n你需要把要做的事解释给秘书听，并确保他能听懂（沟通成本）\r\n秘书下班或休假时，这些事你还得自己做（有时间限制）\r\n你要为秘书犯的错误承担责任（质量得不到保证）\r\n每次秘书犯了错误，你都需要进行教育（培训成本）\r\n秘书会直接接触业务数据和信息，难保有泄密的可能（安全隐患）\r\n换一个秘书，以上的事情都得再来一遍……\r\n这不是把事情搞复杂了吗？人的成本太高，提供的服务又不可靠。而如果你会编程的话，也许你可以：\r\n\r\n编一个小程序来完成查数据、做报表、发邮件的流程，再配置一个定时任务每天自动执行；\r\n谨慎一点的话，可以让程序先发到自己的邮箱，检查没有问题后再转发给领导；\r\n必要的话，还可以让程序在运行出现意外时给你的邮箱或手机发报警通知；\r\n如果你愿意，甚至可以给内网办公系统直接加上邮件报表这个功能……\r\n假设你原本手工做完ABC流程需要30分钟，那么你的程序每执行一次，就为你节省了30分钟的时间。假如你写程序用了三个小时，那么一周就能收回成本，以后全部都是净赚的。如果程序运行的时间足够长，那么单次运行的均摊成本将趋近于零。可以看出，编程是一种用时间换时间的解决方案。\r\n\r\n当然，你需要将要做的流程写成机器能读懂的程序；如果需求发生变化，你需要对应地修改程序；如果运行时出了BUG，你需要调试修复……但更重要的是：程序不拿工资，不可能辞职，不需要休息，不会闹情绪，不可能犯错，只要你的流程正确，依赖的资源不出问题，它就可以7*24小时一直运转下去。\r\n\r\n通过编程，你可以将那些枯燥无味的重复性工作中的部分甚至全部，交由机器来接管，这样就可以将你的时间和注意力从具体的事务中解放出来，去做更有价值的事情。比如研究和优化工作流程，或者陪伴家人，又或者去读一本书……\r\n\r\n体验“开挂”的人生\r\n要知道，身体并不强壮的人类之所以能征服地球，就是因为我们会创造并使用工具来突破生理的局限，做到原本不可能做到的事情。在即将到来的全信息时代，编程将是创造工具，甚至使用工具的主要方法。我们天天都在使用的软件和APP，不管是文字处理，K歌软件还是搜索引擎，都已经和我们的生活融为一体，成为我们生命的延伸。\r\n\r\n然而大多数人都是在被动地等待别人来满足自己的需求。他们会使用通过朋友推荐、广告宣传等各种渠道推送给自己的软件，然后感慨一下：“哇，居然还可以这样！”他们只会使用已有软件提供的标准功能，而一些个性化的特殊需求就只能因为软件不支持放弃掉了。这就和穿衣服一样，大多数人只会买标准尺寸穿，如果这里紧那里松，这里长那里短，也就只能忍了。\r\n\r\n还有少部分人会主动考虑如何去满足自己的特殊需求。他们会想：“如果有XXX功能就好了……”他们会主动去寻找能满足自己需求的软件，研究软件的个性化配置，或者给软件的开发者提功能建议。同样，追求个性的人可能会找裁缝为自己量身订做或者修改衣服，使其尽可能地适合自己。\r\n\r\n只有极少数的人有能力自己去实现那些别人不能满足的需求。他们能在原有软件的基础上开发插件，对软件进行二次开发，甚至写出一个全新的软件。同样，追求完美的人可能会亲自设计衣服，并把一件衣服不停地改来改去，直到自己彻底满意为止。\r\n\r\n如果说学好英语能为你的世界打开一扇门，让你拥有更多的选择的话；那么学好编程就能让你有机会以“上帝视角”来认识和改造这个世界，并拥有几乎无限的可能性。因为在现实中的一切最终都会被信息化，而你可以通过编程来对信息做任何形式的加工和处理，只要你想得到，就能做得到。\r\n\r\n你想体验“开挂”的人生吗？那就赶紧开始学编程吧。\r\n\r\n培养深入的思维方式\r\n\r\n每个人都应该学习编程，因为它将教会你如何思考。——苹果创始人 乔布斯 [1]\r\n\r\n思考，是人之所以为人的行为，而编程是一种对人的思考进行再思考的行为。我们不需要把每件事情想清楚，就可以在现实社会中生存。对某些从事机械性操作的职业来说，甚至完全不需要进行思考。然而在编程时，我们只有在想清楚之后，才能把程序写出来。在编写正确、高效、优雅的程序的同时，我们也在塑造自己的大脑，让它能思考得更清楚、运转得更高效。\r\n\r\n编程要求我们客观地去思考事物的本质，将注意力放在事物本身，而不是事物与我们的关系上。当古代的妇女在河边洗脏衣服时，她可能在想：“河水好冷啊……这衣服颜色真漂亮……我家孩子为啥这么调皮……”而当我们在为洗衣机设计程序时，只会想：“哦，这有一堆脏衣服需要洗”。其实很多原本困扰你许久的问题，只要跳出“我”的范畴，进行“忘我”的思考，就变得特别简单和容易解决。\r\n\r\n编程是将人的想法“实体化”的过程，这要求我们进行更深入、更细致、更全面地思考。为了实现一个需求，你必须对其原理和运转流程了解得十分透彻，否则就无法用编程语言精确地描述出来让机器去执行。在实体化的过程中，想法的结构缺陷和逻辑漏洞会自然凸显出来，你总会发现存在没有考虑到的可能性，以及需要进一步思考的细节。\r\n\r\n编程要求我们能够对事物和流程进行拆分，并在不同的抽象层次上进行完整自洽的思考，这使我们有可能去解决那些规模无比庞大的问题。在实现一个稍具规模的需求时，我们不太可能同时考虑主体流程和操作细节，也不太可能同时从多个角度进行思考。经过合理拆分后的需求细粒度需求简单明了，实现难度大大降低的同时，还可以分配给多人来共同进行。在一个成熟的软件或互联网公司，上千名工程师一起开发同一款产品是很常见的，而你能想象这么多人一起去写一本书么？\r\n\r\n编程是不断解决问题的过程，也是不断完善解决问题的方法论的过程。一个优秀的程序员总是解决问题的高手。在编程的各个阶段（需求定义、方案设计、编码实现、调试纠错……）中，都将面临无穷无尽的问题。这个问题要不要解决？什么时候解决？其根源是什么？需要考虑哪些方面？如何做取舍？有哪些方案可供选择？选择的原则是什么？……解决问题的方法论展开来讲可以写一本书了。\r\n\r\n在未来更好地生存\r\n半个世纪以前，美国有70%的人口在农场工作；随着自动化耕种的大面积普及，现在只剩下不到1%。 ——凯文.凯利《必然》\r\n\r\n从登录月球到生产纳米机器人，我们已经通过设计并使用各种机器完成了人类原本不可能亲手做到的各种事情。迄今为止，人类从事的简单重复性的工作（如洗衣、耕种、制造等）已经几乎完全被机器接管，人类的工作方向已经转向对机器的研发和维护。而那些需要复杂知识和精密操作的工作（比如驾驶汽车、外科手术等）也正在被机器逐步接管。\r\n\r\n洗衣机解放了家庭主妇们的双手，全自动流水线则解雇了工厂里的大部分工人。只有在一些正享受人口红利的发展中国家（比如中国和印度），由于技术引入成本比人工成本高，目前体力劳动者还有一些生存的空间。但技术的成本会不可阻挡地快速持续下降，而人口红利的窗口期将快速消失，拐点很快就会到来。\r\n\r\n资本是具有意志的，且不为人性所改变。当产出的质量不变，而技术的成本显著低于人工成本时，几乎所有的体力劳动者都会失业。机器不知疲倦，不会抱怨，干得比人又快又好又省钱，人类怎么可能和机器竞争？\r\n\r\n与此同时，人工智能正在将逐渐接管人类的简单重复性思考活动（如寻路、翻译等），人类只需要下达命令、制定原则和做出选择即可。人工智能甚至已经进入了那些被人们认为是“人之所以为人”的领域：写作、编曲、绘画……\r\n\r\nGoogle的AlphaGo战胜李世石是一个里程碑式的事件，它证明了人工智能已经可以在人类最擅长的思考领域超越人类。现在最优秀的棋手都在向AI学习下棋，职业棋手和AI进行日常训练成了常态。纯机比纯人强，人机比纯机强，这早已是棋界的共识。\r\n\r\n当人工智能在某个思考领域的能力接近或超过人类（这在很多领域已经做到了 ）时，而其成本极其低廉（这是早晚的事）时，在资本意志的作用下，这个领域就会将不可逆地被人工智能迅速占领。我们今天已经习惯了使用计算器来取代大脑进行数字计算，在不远的将来，我们也会习惯将原本需要自己思考的许多问题交给由无数程序组成的人工智能来处理。在可以预见的未来，所有构建在经验和技能基础上的非创造性工作岗位都会消失，人类的工作方向会转变成对人工智能的研发和维护。\r\n\r\n人工智能全面普及的时代正在以光一样的速度向我们飞奔，可能下一秒就将我们远远地甩在身后，连车尾灯都看不到。届时，几乎所有的工作都将和人工智能密不可分。只有那些理解人工智能，能够很好地和人工智能合作，并帮助改进人工智能的人，才能在那个时代更好地生存下去。\r\n\r\n每个人都应该尽早开始学习编程，我的孩子起步太晚了，我觉得应该在教他们ABC和颜色的时候就开始。——美国第44任总统 奥巴马 [2]\r\n\r\n我知道你会问……\r\n可是我又不准备当程序员啊，有必要学编程吗？\r\n\r\n你可能会开车，还是个老司机，很会享受驾驶的乐趣，但你未必愿意去当一名出租车司机吧？同理，学习编程不一定非要做程序员，但却能使你拥有全新的视角、深入的思维方式和效率优化的思维，这都将成为你重要的软实力。在不久的将来，编程将会变成像英语、驾驶一样人人必备的技能。到那时，你希望自己是一名老司机，还是搭车族呢？\r\n\r\n“学这个有用吗？”其实是一个很可怕的想法。由于很多东西现在看起来并没有什么用，大多数人就放弃了学习，而只有少数人会抱着“学学看能有什么用”的念头去尝试。在之后的某一天，真正需要这项技能和知识时，那些选择放弃的人只能感慨“要是当时……就好了！”，而选择学习的人则会惊喜地发现“哇，原来还能用在这里！”……所谓的“惊喜”和“运气”其实就是这样一回事：在不知不觉间，已经提前做好了准备。\r\n\r\n那我能不能现在努力赚钱，然后雇一个专业的程序员呢？\r\n\r\n不错，你是可以找一个程序员来实现你的想法，但我们之前请秘书时遇到的诸多问题又会接踵而来。更重要的是，如果你不会编程，你可能连个靠谱沾边的想法都提不出来。就好比没有见过汽车的人，只会想着让别人为他造一辆更快的马车。只有在理解了某个事物的原理之后，这个事物的概念才能在你的脑中清晰起来，才能真正融入你的认知结构中。\r\n\r\n有了清晰的概念，你才能对其进行思考，判断它能够用来做什么，不能做什么。如果概念不清晰，你甚至都无法讲清楚自己的需求，更难和程序员进行沟通和合作。每一位程序员在面对“给我做一个淘宝”这样的需求时，都会崩溃的。\r\n\r\n我的英语很烂，能学会编程吗？\r\n\r\n英语不是学习编程的瓶颈，关键在于理解其概念和原理，以及改变思维方式。虽然几乎所有编程语言的关键字都是英语，但常用的关键字也就那么几个，热门语言的相关书籍也都有译版。如果你愿意的话，甚至可以用中文来给程序里的变量、函数和命名。正如只要你认得start / save / load / quit这几个单词，就能去玩英文游戏；只要会说sorry和how much，就能去国外旅游一样。\r\n\r\n后话\r\n曾经何时，想要建立一个网站服务，需要购买动辙数万元的专业服务器，支付昂贵的机房托管和带宽租赁费用，聘请专业的开发人才或团队来研发，再通过广告和运营活动去拉拢用户……门槛如此之高，使绝大多数人望而却步，只有企业才能负担得起。\r\n\r\n而现如今，云服务器甚至比家里的宽带都便宜了，各种开源技术唾手可得，各种开放平台提供了免费的用户和渠道，一个APP通过社交网络可能瞬间火遍全国……有了树莓派这样超便宜的卡片电脑，再加上现在各种家电都在向智能化发展，想通过编程在实现自己的一些小创意，真是不要太简单。\r\n\r\n我们正身处一个只要愿意思考，就能改变世界的时代。那么你是愿意去改变世界，还是等待被世界改变呢？\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/10/1179308819.gif', '0', '1', null, 'post', 'publish', null, '1', '1', '1', '1', '0', '1', '1988', '251', '0');
INSERT INTO `typecho_contents` VALUES ('15', 'jinxing.jpg', 'jinxing-jpg', '1570336367', '1570336367', 'a:5:{s:4:\"name\";s:11:\"jinxing.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/10/4046329916.jpg\";s:4:\"size\";i:20960;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '16', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('16', '金星一段话，看哭多少女人！', '16', '1570336380', '1570336414', '<!--markdown-->![jinxing.jpg][1]\r\n面对离婚，金星这样说：\r\n不管是老公，情人，还是追你的男人，\r\n在你需要的时候没有陪在你身边，马上可以换掉了。\r\n\r\n女人出不出轨，取决于她的男人。\r\n男人出不出轨，取决于有没有机会。\r\n不要去痛恨小三，正是小三证明了一场经不起考验的爱情。\r\n\r\n其实女人很敏锐，你爱不爱她，她一眼就能分辨出来，只不过有的装傻，有的自欺欺人，有的委屈求全，有的决定和你一起演。\r\n选爱人不需要太多标准，只要这三样：不骗你，不伤害你，陪着你。\r\n关于爱情，金姐说：“真正的爱情一定是双方的，它计较的不是我付出多少得到多少，而是我对自己妥协了多少。”\r\n\r\n女人要什么？在物质和一个傻傻对她好的男人面前，大部分女人 还是会选择对她好的男人。\r\n别说她走了是因为你没钱，当初是她在你没钱的时候选择了你。也许更多的时候多问问自己，你有没有给她足够的踏实感和安全感？\r\n\r\n很多时候，女人的离开都是因为没有足够的安全感，让她觉得不踏实，看不到希望！任何女人都有一颗想被保护的心，女强人不过是隐藏了脆弱的一面而已，请你好好呵护身边爱你的女人，不要让她独立到让人心疼。\r\n再强势的女人遇到喜欢的男人也会柔情似水。如果可以依靠，没有哪个女人愿意做女汉子。所以不要说女人强势，只是你驾驭不了而已。\r\n\r\n很多男人都说女人现实，爱慕虚荣，情愿坐在宝马上哭，也不坐在自行车上笑，其实好女人不是这样的，如果你真的对她好，真的爱她，她会愿意陪你白手起家经历风雨！很多时候，她离开的原因是——你骑一个烂自行车，还天天让她哭……\r\n\r\n金姐说：\r\n等我女儿长大了，我会告诉她！如果一个男人心疼你挤公交，埋怨你不按时吃饭，一直提醒你少喝酒伤身体，阴雨天嘱咐你下班回家注意安全，生病时发搞笑短信哄你...请不要理他！\r\n然后跟那个可以开车送你、生病陪你、吃饭带你、下班接你、跟你说 「什么破工作别干了！跟我回家！」的人在一起……\r\n\r\n如果一个人说喜欢你\r\n请等到他对你百般照顾时再相信。\r\n\r\n如果他答应带你去的地方\r\n等他订好机票再开心。\r\n\r\n如果他说要娶你\r\n等他买好戒指跪在你面前再感动。\r\n\r\n感情不是说说而已\r\n我们已经过了耳听爱情的年纪。\r\n\r\n现在的男人啊、都希望女人说话温柔点儿，对你体贴点儿，长得还要漂亮点儿，身材好一点儿，经济独立点儿，持家又得贤惠点儿，对你爹妈孝顺点儿，对兄弟朋友客气点儿，还得勤快点，床上还要主动点。\r\n\r\n我要问了，你要求这么多，是因为你的哪一点儿？你是高一点儿？帅一点儿？还是你银行卡里有七八个小数点儿？又或者是温柔稳重会心疼人点儿？！ 什么都没有，你就给姐老实一点儿！\r\n\r\n要么给我爱，要么给我钱，要么给我滚。\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/10/4046329916.jpg', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '1527', '207', '0');
INSERT INTO `typecho_contents` VALUES ('17', '手淫 • 早泄 • 性技巧', '17', '1570336560', '1572443703', '<!--markdown-->希望大家能够正视性,不要谈性色变!性跟吃饭,呼吸一样自然,正常.\r\n内心肮脏的人，看啥都是肮脏的，内心纯净的人，即便世界再黑暗，也总能看到希望的点。\r\n苏小妹说：\"心存牛屎，看人都如牛屎；心存如来，看人都是如来。\"\r\n转载此文是为了帮助更多的人,让更多的人走出误区,为了让更多的人受益.\r\n也不是每个人都有勇气去发表转载此文的,所以,请大家理解,支持,多谢.\r\n[转载] 科学松鼠会 http://songshuhui.net/archives/49764\r\n性既是人们最强烈的快感的源泉，也常常是痛苦的开端，许多烦恼皆源自男女两性角色演化所致的固有冲突。\r\n\r\n——贾里德·戴蒙德《性趣探密——人类性的进化》\r\n\r\n独坐书斋手作妻，此情不与外人知，若将左手换右手，便是休妻再娶妻，一撸一撸复一撸，浑身骚痒骨头迷，点点滴滴遗在地，子子孙孙都姓泥——《笑林广记》（这首湿是作者于2013-8-31补充的，好湿岂容错过，不惜穿越至两年前修改旧文章咯）\r\n\r\n[匿名] real @ 2007-5-25 0:51:31\r\n请教年轻的时候手淫过多，现在存在早泄的情况\r\n请问能自然恢复吗？\r\n食物上可以做哪些补充吗？谢谢 李医生！\r\n\r\n这是4年前牛博网还没被彻底阉割时，一个网友在我博客上的提问，我当时查了很多材料，写了长长的一篇，絮絮叨叨说了很多，获得了很多回应，有不少网友在评论区以自己的真实经验佐证了我这“鸿篇巨制”中的诸多观点，其实在我看来全文都是老生常谈了无新意，但我终究还是高估了中国性知识的普及程度，在松鼠会的论坛里，类似的问题仍经常引发一些非常莫名其妙的观点，屡次想参与讨论，又觉得一言难尽，不是一句两句话能够说的清楚，于是只好作罢。今年春节期间，我在新浪围脖上贴出了该文章的一些段落，发现有些网友的回应仍提示，这些本应该是常识的东西并没有得到很好的宣讲，那么好，在2011年的情人节，我们不妨再讲一次。\r\n\r\n首先，这个网友的提问本身就存在问题；其次，他所问的问题于他本人来说，还不一定是个医学意义上的疾病，很可能是多虑了而已。\r\n\r\n手淫\r\n因为所谓手淫过多，实在是个天大的误会，何为过多呢？既然存在过多那是否存在过少？事实上手淫无所谓过多或者过度，如果累的不行了，勃起都是个问题，手淫又如何发生的了？或者真的有人能连续手淫不成？不信邪的连续下试试，到最后恐怕只有射精的动作而不会真的有精液射出来了。如同吃饭似的，吃饱就是了，已经撑着了还会拼命吃么？或者是跑步，如果都累吐了累虚脱了，那还跑？因为健身性质的跑步把自己跑死的，因为吃饭把自己撑死的，这些人首先是脑袋出了问题。同样，纵欲过度是个被误传了千年的传说，中国古代那些傻瓜大臣们上书皇帝要注意龙体不要纵欲过度，好像皇帝每次临幸女人时，他们都在边上帮着数数似的。那些被个别笨蛋史学家认为是因为女人而坏了江山的情况，更合理的解释其实是玩物丧志，而不是这些女人真的能毁了皇帝的身体。至于那些拿很多皇帝寿命不长说事的，你们可曾调查过同时代普通百姓的平均寿命？\r\n\r\n但为什么会有这种误会呢？\r\n原来人类历史上不论中西一直有很多人主张限制性活动， 18世纪早期，一位医学思想界的权威、伟大的临床医生布尔哈夫（Hermann Boerhaave，1668—1738），在其《医学教程》一书中写道，“射精可以带来疲倦、虚弱、动作乏力、痉挛、消瘦、干渴，视觉模糊、痴呆以及其他类似的病痛……”他通过对性高潮之后困扰男女双方的疲倦状态进行了观察总结，认为性是一些疾病的致病因素，主张如果要保持健康，性交必须受到限制。在那些年代里男人经常被劝说不要太常行房，以免其“活力之源”枯竭而变得衰弱，更骇人听闻的观点认为伴随着性器官的使用而产生的神经震动，神经系统将遭受最深重的刺激，如不严格控制会导致精神失常。无独有偶，我国古代也有人提出“一滴精十滴血”谬论来奉劝男人珍惜精液减少性交。\r\n\r\n直到近代尤其是二次世界大战之后，随着人类对性本质的科学解读以及避孕用品的广泛使用，人们逐步认识到性对于男女双方而言都是一种健康、快乐活动，健康的肉体就是需要性交的。男性气概越来越等同于带有侵略性的人格特质，认为男性就应该有插入和性交的“生物性需求”，有“动物本能”的旺盛“性冲动”，男人需要“好色”，需要经常性交这一理念甚至反而成为了一种无形而强烈的压力。\r\n\r\n其实，性活动实在是个能者多劳量力而为的事，度全在自己的感觉，并没有一个适用于所有人的所谓的“度”。这正如我们每个人跑5000米，成绩都将有差异一样。有一本《中医房事养生与保健》的书中提到，男人超过60岁就应该禁欲。君不见某70多岁的老教授还因为潜规则了一个女学生而向公众忏悔么？（我的一个朋友认为，此公不是忏悔，乃显摆）成年之后，随着年龄的增长，男性的性欲必然会随之下降，可能确实有些人到了60岁就不再有性需要了，但是对于那些身体状况良好的男性老年人来说，如果他们能够通过合理合法的手段享受鱼水之欢，实在没有必要再给他们制造无端的恐慌。\r\n\r\n对性愉悦的追求，是每个人与生俱来的本能，如果能够不通过麻烦别人就享受这样的愉悦，并没有什么不好。事实上很多人一直到婚后仍然保持着手淫的好习惯。单纯以性快感来说，手淫带来的刺激远大于真实的性交。因为只有你才最了解你自己，当你濒临高潮的那一刻，无论经验多么丰富的性伴侣也无法在动作的配合上叫你 100%满意。所谓两人同心，那不过只是个太美的传说。对于存在性压抑的青年男女来说，不要再对手淫存在认识上的误解了，这本就是属于人类自我享乐的一部分。当然，这并非说手淫可以完全取代性交，毕竟性欲的构成分为两部分，一为释放欲二为接触欲，只能解除释放欲还只是个退而求其次的无奈选择。\r\n\r\n必须强调的是，手淫跟早泄没有直接关系。如果手淫得法且一直是在安静不受打扰的环境下进行的，不但不会引起早泄，相反到可以增加阴茎对性刺激的耐受力，使射精时间相对延长。确切一点说，是阈值的提高，关于阈值的含义，可以参考“笑点”来理解，如果你是一个读过很多很好笑话的人，那么那些幽默程度不怎么高的笑话你还会笑么？同理，如果是一根久经实弹演习考验的JJ，当它参与实战时，它会比连演习都没经历过的JJ更怯场的么？\r\n\r\n耐人寻味的是，关于手淫对身体有害的说法无论在东方西方都曾长期存在。这里面有一系列复杂的社会原因，我们且不去深入探讨，但最根本的原因还是生理学的不完善导致的，在今天，虽然科学并没有穷尽所有有关性的秘密。但有关性生理学方面的研究足以充分证明手淫是无害的。\r\n\r\n那么为什么时至今日，仍然有人会将自己的早泄归罪于之前的手淫呢？这也许与早泄这一定义本身的模糊性有关。\r\n早泄\r\n早泄(Premature　ejaculation.PE)是一种常见的男子性功能障碍，发病率在成年男性为35%～50%（在英国发病率据估计为 (14% ~ 41% ),中东约为12. 4%,东南亚约为30. 5%,东亚约为29. 1%……够乱的吧）。人们直到几十年前才开始把早泄看成一个需要治疗的问题，这是由于人类性活动的主要目的，已不再是生育需要而是变成双方的共同享乐，尤其是女性的性需求和性满足得到充分认可和肯定。需要强调的是早泄虽然属于男性性功能障碍的一种,但却并不一定影响男性的生理性满足,这点在文化落后的地区和个人体现较为明显,由于不顾及女性性感受,因此此类个体并不认为早泄属于疾病范畴,甚至不会引起双方的不满。换句话说，早泄的情况可能是自古以来就存在的，但其之所以能成为一个问题甚至被称作一种疾病，则全拜社会文明进步之所赐。\r\n\r\n从表面看, 早泄主要表现为性生活过程中，射精潜伏时间控制的异常,特征为勃起后进行插入阴道动作时，或插入后射精潜伏期时间过短,无法控制的很快发生射精。从当前条件下看,其影响因素包括自身因素与外界因素两部分。自身因素中,主要与自身生理解剖结构与功能状态,如神经、内分泌系统功能状态及个体心理状态关系紧密相关；外界因素中,各种环境因素、性伴侣的性吸引力及配合与否等皆有重大影响。自身因素和外界因素均对早泄的形成和维持以及发展有重大的影响，套用马克思主义哲学辩证法的说法，根据内因外因关系原理（有过考研备考经历的同学，看到这句都想吐了吧）：自身因素是早泄发生的基础,外界因素是早泄发展的助力。（如果以后硕士考试出现了这道题，记得李医生说过要用这个原理答题哦）\r\n\r\n令人尴尬的是，目前PE的定义较多，至今仍未有较明确的结论。但通常以男性的射精潜伏期或女性在性交中到达性高潮的频度来评价。美国精神病协会颁布的《精神疾病诊断和统计手册———第四版(DSM-TR-Ⅳ)》中PE诊断标准是:\r\n\r\n① 持续地或反复地在很小的性刺激下,在插入前、插入时或插入后不久就射精,比本人的愿望提前,应该考虑影响性兴奋持续时间的各种因素,如年龄、新的性伴侣、新的环境和近期性交频率;\r\n② 明显的痛苦和人际关系(伴侣之间)紧张;\r\n③ 这种PE情况不是由某种物质(如酒精、类罂粟碱类和其他药物)所引起。\r\n2004年美国泌尿外科协会(AUA)认为PE即指射精发生在个人期望之前,不管是插入前还是插入后,并导致对方或双方的苦恼。\r\n\r\n我国学者普遍认为男性在性交时失去控制射精的能力，阴茎插入阴道之前或刚插入即射精,或女性在性交中到达性潮的频度不足50%时即可定义为PE。由于男性射精潜伏期受禁欲时间长短的影响,女性性高潮的发生频度受身体状态、情感变化、周围环境等多种因素的影响,因此这一定义注定不是板上钉钉的事，尚有待进一步完善。\r\n\r\n一个尚待完善的定义，有那么容易就能扣到自己的脑袋上么？对照一下看看自己是不是真的属于早泄的范畴。不然岂非庸人自扰白白给自己增加那些无谓的心理压力呢。\r\n\r\n那么早泄的真正危害又在哪里呢？\r\n其危害的特殊性不在于影响患者本人性生活的生理满足，而主要通过引起患者配偶性满意度下降，而引起患者心理或生理上的不满足，从而造成性生活单位的性满意度整体下降。诊断与治疗早泄必须考虑到这一特点才能抓住问题的本质。严格说来,早泄不仅仅属于个体疾病,而应属于性生活单位内的失衡,是性生活不和谐的一种形态。某些早泄患者在更换性伴侣的情况下，其早泄状态可消失，从另一角度证明了这一论点。因此早泄的诊断和治疗必须以性伴侣双方的生理条件为基础,充分考虑到双方的满意度问题,而不应仅仅从时间或抽插次数等来简单定性早泄与否，同时治疗方面也应以最大限度恢复性生活单位内部的生理及心理平衡作为治疗的终点。\r\n\r\n早泄治疗的方案以提高男性刺激性伴侣的有效性刺激量(有效刺激强度×有效刺激时间)达到其高潮阈值为主要手段，同时应适当调整女方状态,并使双方合作能力提高，从而提高双方性关系的和谐程度，这也符合当前临床诊断治疗工作的实际。\r\n\r\n增加对女方的有效性刺激量并不仅仅在于简单延长性交的时间与抽插的次数，同样取决于性刺激的时机、方式、方法的得当与否。与男性的射精潜伏期在不同环境有变化一样，同一女性在不同的状态下达到高潮需要的刺激量和刺激方式并不相同，在适当的时机和方式刺激下，女性达到高潮所需的时间可以大大缩短。因此男性掌握合适的时机及适当的性刺激技巧，配合女性丰富的性想象均有助于使女性达到性高潮，从而达到治疗早泄引起的临床问题的目的。\r\n\r\n据此早泄的治疗应从以下三个方面入手：第一，指导男性学习双方性生理及心理常识，并通过各种实践或练习提高其性刺激技巧；第二，应用各种手段延长男性射精潜伏期，包括各种减轻刺激的手段，如避孕套、局部麻醉药物的应用等，以及动停技术、分散注意力等等；第三，双方配合，女性也需要改变自身性行为时的身心状态，尽量通过自身调节提高兴奋度，降低其高潮阈值。\r\n\r\n这三个方面中，前两者与男性的关系最为密切，医生通过理论教育使男性对双方性生理和性心理有所了解，有助于男性选择在合适的时机给予女方适当的性刺激方式以提高女性的兴奋性,同时通过采取各种药物及器械辅助,并进行必要的训练和练习,有助于打破男性原有的快速射精行为模式,延长射精潜伏期,为给女方进行充足有效的性刺激建立时间储备;第三方面则与女方关系密切,女性本身的生理、心理状态的调整,可以改变自身性兴奋的程度,女性通过充分的性幻想和自身辅助刺激等,均可提高在性生活中获得性高潮的几率。通过以上三个方面的努力,如无严重的器质性病变因素,这一性生活单位的和谐性应该能得到改善。\r\n\r\n性技巧\r\n从某种意义上说,早泄的治疗不是单纯服务于男性自身,而是以通过增加男性对女性的有效性刺激量为手段,通过改善女性的性感受达到性生活单位的性和谐为目的,因此其治疗不应以男性为单一治疗对象,而应以整个性生活单位为治疗对象,以协调其内部和谐为目的,而这正是当前临床诊断与治疗中的盲区所在,也是临床疗效提高的潜力所在。\r\n\r\n关于如何增加对女方的有效性刺激量，实际上就是有关男性性技巧的问题，如前所述，这其实不太可能有一个非常固定的程式化的东西，如同所有的技巧一样，这也有个熟能生巧的过程，没有谁是与生俱来的性爱高手，也不会有那样一个男人，他的经验和技巧可以适合每一个女人，每一个人都是不同的，每一对儿伴侣都是不同的，找到最适合你们双方的，您就是高手。\r\n\r\n很多男人纠结于时间的问题，其实控制射精时间的能力是可以逐渐培养和训练的。比如说长跑，慢慢地跑起来是什么感觉呢？总想停下来是吧，总觉得气不够了，要停下了痛快地喘气才舒服。可只要你想坚持，总还是能再跑的久一些，而且随着锻炼时间的延长，跑的时间也一定会相应的延长。在夫妻性生活的时候，也有类似的情况，插入之后一泄如注当然痛快，但要只图自己痛快那手淫多好呢。坚持，坚持再坚持……只要你相信自己还能再坚持，总还能延长的。但时间不是终极目的，终极目的是夫妻双方的愉悦。不要被黄书和A片误导，以为只有时间长才是高手。所谓高手，应该是能在最短的时间里将女人送上云端，然后收兵的那种。当然如果男方有足够的自信而女方又有良好的调节能力的话，享受到序惯高潮自然是最美妙的。但是，有一点必须要注意，不管此前女方经历了几次高潮，一旦她最后那次没到，你被踢下床的几率，跟第一次她就没到的几率是很接近的。所以，见好就收才是明智之举，尤其是那种本身就比较敏感的男性，坚持到对方已经丢盔卸甲就很不容易了，你就别以剩勇追穷寇了，当心人家一个反扑弄得你招架不住，你再醒悟穷寇莫追的古训就有点晚了。所谓夫妻，来日方长，漫漫人生，慢慢享受。\r\n\r\n对于有些病人来说，积极的药物治疗甚至是手术治疗可能是有帮助的。\r\n\r\n口服药物治疗包括抗抑郁药，α受体阻滞剂，西地那非(万艾可)等，有部分药物虽然有明确的效果，但其在治疗中的作用和详细的作用机制并未完全阐明。需要的话，得有医生处方。阴茎表面外用药治疗,虽不及其抗抑郁药治疗流行和广泛,但因起效快,方便、安全、价廉,而为医生和病人所接受。这类药挺多挺杂的，别自己到什么夫妻用品店去瞎买，要在医生的指导下应用。\r\n\r\n手术治疗的方法是阴茎背神经切断术，在阴茎背侧冠状沟处皮肤上做1～2cm 的横切口，深度达阴茎筋膜，剥离筋膜，暴露放射状分布的阴茎背神经，保留主干，切断80% 左右的神经。该方法在一些国家试用。其安全性和有效性有待于进一步研究。其理论依据近年来研究发现,阴茎感觉过敏或阴茎感觉神经兴奋性增高等器质性因素也是引起早泄的病因。临床上常把早泄分为原发性早泄和继发性早泄,许多研究结果表明,原发性早泄患者的阴茎背神经兴奋性,特别是阴茎头的感觉神经兴奋性比正常人高,以至于在性交时射精潜伏期与射精反射弧较短,射精刺激阈低,在性交时容易诱发过早射精。\r\n\r\n某男科专家的一篇论文里对该手术的效果是这样描述的：手术治疗后3周开始性生活, 128例患者中, 显效（:阴茎插入阴道内性交时间5 min以上）78例, 好转（:阴茎插入阴道内性交时间3~5 min）34例, 有效率为87. 5%; 无效（:性交时间无明显改善）16例; 性伴侣满意率83%。并发症为局部疼痛6例,阴茎麻木2例,切口感染4例,包皮手术后外形不佳3例。\r\n\r\n目前认为阴茎背神经切断术治疗原发性早泄的手术指证为:①年龄一般≤40岁;②阴茎勃起角度一定要超过90°;③排除心理素质不佳因素;④带安全套有效者;⑤局部表皮涂药有效者;⑥适度饮酒有效者;⑦口服抗抑郁药有效者。\r\n\r\n我个人觉得，既然这个手术的有效性和安全性还在验证中，那么如果不是之前所有的方法都无效，而本人又急于改善性生活，最好别做这个手术。等手术数量再大些，结论更可靠一些的时候再尝试不迟。那些愿意作为先行者为医学的发展进步贡献数据的，可以试试，只不过动手之前要想清楚，那些神经一旦切断就再也接不上了，有些并发症也许是无可挽回的。\r\n\r\n写到这里可能读者也已发现，通篇大部分在谈的都是男性的情况。没办法，现实即如此，男性在两性活动中承担了大部分主动的责任，但这并非说在这些问题上，女性就应该完全被动地承受，前面也已提到，在针对一个性单位的治疗过程中，女性的作用十分关键。\r\n\r\n回到最初的问题，其实有些女性也存在关于手淫方面的困惑，这里我只谈一点，对于男性来说，通过手淫应该以可自如的控制射精冲动时间为目标,这对日后会有好的影响，你未来的妻子将会因为你当年的努力受益匪浅。而女性手淫时，需要达到的目的应该恰好相反，毕竟两性间在从性活动开始到高潮的时间是有很明显的差异的,故女性手淫时,不必刻意延缓高潮的到来, 应该顺着自己的感觉,寻找最能使自己快速进入巅峰状态的手法。而且,在婚前自己总结的这些经验一定要同爱人分享,你不说,他是不会知道的,不管他自诩性经验多么丰富,毕竟这个世界上并没有两个完全相同的女人。如果女性都能通过这样的方式学会享受性高潮, 那么女性高潮获得障碍的比例必然会大大下降。\r\n\r\n[转载] 科学松鼠会 http://songshuhui.net/archives/49764', '0', '1', null, 'post', 'private', null, '0', '1', '1', '1', '0', '1', '1', '1', '0');
INSERT INTO `typecho_contents` VALUES ('18', '何时再见我汉家衣裳——漫漫汉服复兴路', '18', '1570336680', '1570336795', '<!--markdown-->![7.jpg][1]\r\n\r\n《左传·定公十年》有云：“中国有礼仪之大，故称夏；有服章之美，谓之华。”多少年来，我们一直以“礼仪之邦”、“衣冠上国”自居。然而时过境迁，如今的我们在现代化的潮流下得到了许多，也失去了许多。\r\n\r\n《左传·定公十年》有云：“中国有礼仪之大，故称夏；有服章之美，谓之华。”多少年来，我们一直以“礼仪之邦”、“衣冠上国”自居。然而时过境迁，如今的我们在现代化的潮流下得到了许多，也失去了许多。\r\n\r\n近年来，我国街头时常能见到一些身着汉服，闲庭信步的年青人，他们衣袂当风，佩环叮咚，引得路人纷纷侧目，而我，正是其中的一员。大多数人也许会疑惑：他们为什么穿着古装？更甚者，会有人用或嘲笑，或厌恶的语气对身边人说：“你瞧瞧，他们是不是日本/韩国人？穿着和服/韩服就敢上街！”每到这时，我不禁会感到一种深深的无力：汉服，是汉民族的服饰，不是汉朝的服饰，她从夏商而来，经历千年风雨，一直作为汉族的衣冠，直到清政府强令“剃发易服”，所以不能称作古装；和服和韩服，都是由汉服演变而成，可以说是是父与子的关系，但如今我们自己竟已不知了么！\r\n\r\n汉服的噩梦，始于清军入关。明崇祯十七年(公元1644年)清军入关，为加强对关内百姓的统治，颁发“剃发令”，后因汉人强烈不满与反抗，此令被迫中止。1645年清兵进军江南后，汉臣孙之獬受到其他汉大臣的排挤，恼羞成怒之下向摄政王多尔衮提出重新颁发“剃发令”。于是，公元1645年1月15日，多尔衮下令再次颁发“剃发令”，规定：“全国官民，京城内外限十日，直隶及各省地方以布文到日亦限十日，全部剃发。”而伴随“剃发令”一起执行的，正是致使汉服断代的罪魁祸首——“易服令”。此令中提出：“官民既已剃发，衣冠皆宜遵本朝之制。”汉族自古以来就十分重视衣冠服饰，《孝经》有言：“身体发肤，受之父母，不敢毁伤，孝之始也。”剃发已不能忍，何况又加易服？汉族的发式衣冠是华夏民族文化传统的象征，满清要把汉族人民的民族尊严和民族感情踩在脚下，这就遭到了汉族人民坚决的浴血反抗。于是，汉族人民为保护世代相承的汉家衣冠进行了激烈的抗争，但是遭到清朝统治者的残酷镇压，最惨烈莫过于1645年的“嘉定三屠”事件。轰轰烈烈的保护汉族发式衣冠的抗争持续了长达37年之久，最终，清朝统治者还是取得了胜利，汉族大部分生者都剃发结辫，脱下汉服，改着满族衣冠。坚决不愿意剃发易服者要么被杀，要么流亡海外或是遁入空门，带发修行。后来的历史表明，满族统治者的这一措施基本达到了预期效果。汉人逐渐淡忘本民族服饰，习惯了满族的发式和服装。至此，汉服走向了没落。\r\n\r\n我曾经看过一张中华五十六个民族的合影，其他五十五个民族的同胞们身着自己的民族服饰，五彩缤纷，绚丽夺目，只有汉族同胞穿着T恤和牛仔裤站在其中，显得格格不入。当时只感觉万分心痛，又随手翻了翻其他的照片，终于发现，现在网络上所有的合影中，汉族同胞身上穿的不是西式服装就是旗袍唐装，鲜见有汉服的身影。有人说复兴汉服是无稽之谈，我只想代所有汉族同胞问一句：“为什么汉族不能有自己的传统服装？”也有汉族同胞对汉服抱有十分排斥的态度，说汉服不能代表汉族，我亦想问：“难道唐装就能代表汉族多少年的精神风貌了吗？”搜狗百科上这么评价唐装：唐装较为广泛的理解是指西式裁剪的满族服饰，而不是唐朝的服装。其实唐装叫做“清装”更为合适，而“唐装”这一名词非常不合适。西式裁剪、满族服饰，没有一点和“汉”有联系，这真的是我们汉族的传统服装吗？\r\n\r\n随着中国国力的上升，一部分汉族人民族意识的觉醒，为了改变人们普遍认为唐装(即满式旗袍、马褂)是汉族传统服装的认识，正本清源，一些有识之士开始致力于恢复清朝之前汉族传统服饰，借此以复兴华夏传统文化。这就是汉服复兴运动的开始，此时的中华大地还在沉睡，而黎明已无声地到来。汉服复兴运动，不是所谓的复古，更不是大民族主义，她是一场由汉族人始，延伸至整个中华民族的文化救赎。\r\n\r\n西元2003年11月22日，癸未年十月廿九日，河南郑州一名普通的电力工人王乐天，身着由薄绒深衣和茧绸外衣组成的汉服走上了街头，穿街过巷。这一身汉服十分简陋，可以说并不合身，但是这是王乐天和他志同道合的朋友们一针一线亲手缝制的。一帮大男人组建了一个汉服工作室，查文献、找规制，他们甚至拿起了缝衣针，即便是刺破了手指，他们也乐在其中。很快，在旁人眼中身着“奇装异服”的王乐天就引起了路人的围观，还有不少不认识汉服的人说他是日本人，说这身衣服是和服。\r\n\r\n时至今日，我身着汉服出行，依然有人鄙夷地指着我对别人说：“看！日本人！”“玩Cosplay的吧？”我心中有的不只是愤怒，更多的是浓重的悲哀：我可怜的汉家衣裳，你如何落到了如此境地？由此，我便能够理解王乐天当时的心情，也更加佩服他能够坦然面对四面八方各异的目光，悠然地在郑州最繁华的街道上行走的勇气。大家后来评论他说：那天风很大，他的头发有些乱，但飘逸得充满汉韵古风。\r\n\r\n新加坡《联合早报》的记者张从兴无意中看到了网络上流传的王乐天的照片，并据此写了一篇报道，这篇报道也成为了第一篇报道汉服的文章，引起了国内外媒体的广泛关注。王乐天被后继的汉服复兴者尊称为“汉服复兴第一人”，他此举得到了许多人的支持和响应，并在全国掀起了汉服复兴的浪潮。2008年2月24日，《莫愁》杂志刊登了一篇报道王乐天事迹的文章，题为《隔了360年，他为汉服续上了传承的脉络》。中国五十六个民族，唯有汉族一直以来没有自己的民族服装，如今汉服的回归，使得中华传统文化在服饰方面得到了延续，这使我们在悲哀之余，多了几分对未来的希望。\r\n\r\n2004年，是汉服复兴最为关键的一年。从这一年开始，汉服复兴的势头越来越强劲。北京、上海等各大城市都出现了汉服的身影，参与其中的人们也由当初的十数人发展到今天的全国上下数万同袍，是的，同袍，“岂曰无衣？与子同袍”，汉服复兴者们相互之间如此称呼。低落多年的汉服，在大家的努力下呈现出蓬勃发展的态势。10月7日，《京华时报》以“汉服集会”为题报导了一次汉服活动，并附有活动照片。然而，当晚一些网站出现了一条被篡改的虚假报道，把标题改为“寿衣上街”。因为汉服的普遍特点为：交领、右衽、结带，此为生者服，一般我们认为死者所着汉服为左衽，这一点恰好被一些人利用，进行炒作，不仅伤害了汉服，伤害了致力汉服复兴的同袍，更是误导了大众，为本就曲折坎坷的汉服复兴之路平添波折。12月7日，报道涉及的汉服活动人员将篡改汉服成寿衣的某电子公司告上法庭，是为汉服诉讼第一案。这场官司受到了社会各界的广泛关注，最终原告胜诉，并使汉服从此为更多人所了解。\r\n\r\n2006年，中华人民共和国中央人民政府网站将汉族的代表图片从肚兜更正为汉服，新华网也随之更改了图片。此事代表了政府对汉服的正式认可。2007年，两会期间，中国政协委员叶宏明提议立“汉服”为“国服”；中国人大代表刘明华则建议，中国在授予博士、硕士、学士等学位时，应该穿着汉服式样的中国式学位服。这是汉服第一次进入中国两会议案。同年10月28日，百度贴吧汉服吧第一任吧主天风环佩(又名：溪山琴况)因心疾去世，享年三十岁，留下了案头未完成的华夏昏礼修改稿、中国式学位服第二稿和奥运礼服设计稿。弥留之际，他还留下如此遗言：“华夏复兴，天风魂牵梦绕，至死不忘育我民族，死后怎舍梦里衣冠。始于衣冠，再造华夏，同袍之责，我心之愿。华夏复兴，同胞幸福，天风叩祈苍天。”如此赤诚之心，令我闻之动容。\r\n\r\n前不久的一个雪夜，我和几位同袍相约参加洛阳一高中汉服社的成立仪式，撑着一把油纸伞在雪地中拍了一张照片，照片里，我面前是苍茫的雪色和无边的黑暗，四面狂风夹杂着大团大团的雪末，在镜头中留下了一道道白亮的光影。发给朋友看的时候，她说：“只有我觉得看上去很凄凉吗？”我心中不由一痛。是啊，还有那么多人不知道什么是汉服，我和我的同袍们现在的处境不正如同这风雪交加的夜晚么？\r\n\r\n“华夏复兴，衣冠先行；始自衣冠，达于博远。”如今这句口号还在一位位同袍之间流传，前辈们的不懈努力，如何不令我们敬佩？现在，我们是时候挑起复兴华夏的重任了，从寻回汉家衣冠始，进而扩大到生活、艺术、技能、礼仪、精神。我们所要复兴的，不仅是做起来不易，甚至连说起来都感觉厚重如斯。\r\n\r\n最可悲的事莫过于礼仪之邦丢了礼仪，衣冠上国失了衣冠。放眼望去，有哪个民族如我们一般有着如此悠久且不曾中断的历史？有如此博大且深邃的文化？我不是赞成固步自封、闭关自守，我只是害怕我们一心向上生长，却忘记了脚下的根基。“物有本末，事有终始。知其先后，则近道矣。”我们的民族就像一棵大树，只有根深蒂固，才能茁壮成长。这根当然不是西方文明，而是经过千年发展形成的华夏文明，在此基础之上，我们才能有更好的发展。\r\n\r\n何时再见我汉家衣裳？华夏复兴，此我等夙愿也。虽前路漫漫，吾往矣。秀倜再拜顿首。\r\n\r\n作者：Lisa_10b2\r\n链接：https://www.jianshu.com/p/3d9b61b48ad8\r\n來源：简书\r\n著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/10/653959737.jpg', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '2053', '292', '0');
INSERT INTO `typecho_contents` VALUES ('19', '7.jpg', '7-jpg', '1570336786', '1570336786', 'a:5:{s:4:\"name\";s:5:\"7.jpg\";s:4:\"path\";s:34:\"/usr/uploads/2019/10/653959737.jpg\";s:4:\"size\";i:718472;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '18', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('20', '有服章之美谓之华,有礼仪之大谓之夏', '20', '1570336800', '1573614866', '<!--markdown-->![hanfu.jpg][1]\r\n——从质朴的秦汉风格到超然的魏晋神韵，都渗透着华夏民族的理想追求；从开放的大唐情怀到含蓄的宋明格律，无不流露出华夏文化的哲学信念。\r\n\r\n[http://blog.sina.com.cn/s/blog_5fc9c4be0102e0tn.html]\r\n\r\n汉服： http://baike.baidu.com/view/4514.htm[Meting]\r\n[Music server=\"netease\" id=\"33891487\" type=\"song\"/]\r\n[/Meting]\r\n\r\n\r\n汉服，并不是指汉朝服装，而是指汉族人民的民族服装，具体是指夏商周到明朝末年，华夏民族（汉朝后又称汉民族）主体人群所穿着的主要服饰，是具有明显独特风格的一系列服饰的总体集合。例如春秋战国、汉朝、唐朝、宋朝、明朝的主要衣服款式都包括在汉服的范畴之类。\r\n\r\n汉族没有民族服装吗？\r\n\r\n可能很多朋友多发现了，每当五十六个民族齐聚一堂时，其他五十五个民族的兄弟姐妹都会着自己民族的节日盛装。可是，汉族人却常常不知道穿什么，大部分的时候只好穿西式礼服。近年来所谓的‘唐装’流行，很多人都以为这是我们的传统服饰，过节的时候就穿‘唐装’旗袍。然而，大多数的汉族人可能不知道，所谓的‘唐装’并不是汉族的传统服装，而是根据满族的旗装结合西式风格改变而来的，而旗袍同样是根据满族的女式旗装结合西式风格演变而来的。有人说：中山装是中国的国服，而其实它是孙中山根据日本学生装改革而来，也不能代表几千年的中国文化和印象。　　那么什么才是汉族的传统服饰呢？汉族人奉黄帝为祖先，《史记》记载黄帝始做衣裳，一直发展到夏商周，华夏人（汉朝后称汉人）的衣裳形势已基本固定为y领，上衣下裙（裤）或有连体的深衣，圆领长衣（也有H领，即我们平时所见的唐朝式样），虽每朝每代有一些各自的特色和流行的款式，但是其交领右衽、无扣系带的基本样式一直到明朝末年皆未有大的变化。\r\n\r\n汉服的特点和形制\r\n\r\n汉服的特征以交领右衽为主，兼有圆领、直领，无扣，系带，宽衣大袖，线条柔美流畅。从形质上看，主要有“衣裳”制（上衣下裳，裳在古代指下裙）、“深衣”制（上衣下裳连体）、“襦裙”制（襦，即短衣）“袴褶”制（上短衣，下长裤）等。衣裳，衽、无扣系带是延续千年的主要特点。\r\n\r\n为什么汉族近代以来没穿民族服装呢？汉服是自然消失的吗？\r\n\r\n汉服并非自然消失的。明朝末年清军入关以后，满族贵族为了同化汉族而强行推行满族服饰，强迫汉族人脱去华夏衣裳，史称“剃发易服”。之后汉民族的民族服装——华夏衣冠便在中华大地上销声匿迹了，只有佛教、道教、演员等遵从“十从十不从”而保留了衣冠。藏族、苗族等兄弟民族那还保留着相似的样式。 PS：“十从十不从”的内容是：“男从女不从，生从死不从，阳从阴不从，官从隶不从，儒从僧道不从，老从少不从，娼从优伶不从，仕宦从婚姻不从，国号从官号不从，役税从文字语言不从。”\r\n\r\n辛亥革命后，因为西方文化来势汹汹，满族的旗装开始西化，具有民族特色但其实是时装的旗袍，汉族的服装也没有机会大规模的回复。\r\n\r\n一直西化到如今，中国频繁与世界接轨，世界上很多地方都把目光放向中国，想来看看中国这个神秘的东方国度。但是，他们看到纯粹的中国了吗？没有。我们在结婚、庆典等各种大礼仪场合基本都穿西装礼服，最多也就穿穿所谓的“唐装”、旗袍。而在我们的邻国，日本人和韩国人结婚的时候都穿传统服装。日本的和服，韩国的朝鲜服，其实在根本上受汉服（汉文化）影响的，也是交领右衽，戴冠插簪。这些服装，西方国家一看就知道他们属于日本、韩国。但我们中国人却没有明确的外在形象。相反，现在很多年轻人结婚拍婚纱照的时候，穿上的确是和服、朝鲜服拍照。想到这些，难道你不想穿上我们汉族自己的民族服装吗？\r\n\r\n\r\n中华民族要崛起，就要以自己独特的文化以立于民族之林。而我们中华延续了千年的文明是没有哪个国家可以相比的。汉唐宋明的建筑不可能轻易复原，以前的礼制跟现在代生活也有很大差距，那么不少人在网上提出“华夏复兴，衣冠先行”的口号汉家儿郎孙异一曲《重回汉唐》，更是唱出汉服复兴者的心声：\r\n\r\n\r\n蒹葭苍苍 白露为霜 广袖飘飘 今在何方\r\n\r\n几经沧桑 几度彷徨 衣裾渺渺 终成绝响\r\n\r\n我愿重回汉唐 再奏角徵宫商 着我汉家衣裳 兴我礼仪之邦\r\n\r\n我愿重回汉唐 再谱盛世华章 何惧道阻且长 看我华夏儿郎\r\n\r\n所谓华夏，《左传》记载：有服章之美谓之华；有礼仪之大谓之夏。\r\n\r\n\r\n“礼仪之邦”和“服章之邦”本不在日本韩国，而在我们中国，这是事实。只是近三百年来因为历史的缘故我们丢失了自己的民族服装，不过现在想起来还算不晚。早在2003年，已有同袍自制汉服，穿上街向人们宣传这是汉族的民族服装、华夏古国的传统服装。此后，汉服复兴便悄然展开了。在汉服复兴同袍的努力下，如今汉服已受到很多中国人的认可。关于汉式学位服、穿汉服过传统节日、穿汉服结婚等讨论一直从网上延至平面媒体、电视媒体，也有人在实际生活中尝试和实践。2006年和2007年，武汉市连续两次举办了汉服成人仪式，500多名学生身着黑红两色汉服，按古礼得恩师亲手加冠，宣告成人，场面十分庄严肃穆，感动了很多热爱传统文化的中国人。2006年7月中国政府网和新华网也将介绍汉汉族的照片换成了着汉服的照片，标志着政府对“汉服”概念的认同。\r\n\r\n但是，汉服复兴要走的路还是很长，需要更多的同袍一起努力，华夏复兴，是我们每个炎黄子孙的责任！\r\n\r\n\r\n上图：一位在英国街头卖艺的身着汉服的中国女子，她一个人在异乡，干了多少老爷们都汗颜的事。在外国文化肆意侵入的时代，她努力的向世人展示被族人遗弃的最美好的传承——汉族民族服装，真的很令人佩服。汉服不光是古装，还是汉族族装，和其他少数民族服装一样。汉人在哪？\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n汉服民族、等级和礼仪三属性的消失\r\n汉服的社会功能三属性\r\n\r\n相传，华夏民族的服装是由黄帝发明的。《周易·系辞下》记载黄帝“垂衣裳而天下治”，孔子也说黄帝“始垂衣裳”。后人疏周易解释曰：“以前衣皮，其制短小，今衣丝麻布帛，所作衣裳，其制长大，故曰垂衣裳也。”\r\n\r\n在汉（华夏）民族的传说中，我们的老祖宗黄帝发明了许多东西，连大家司空见惯的衣服竟然也有劳他老人家亲自过问并命胡曹发明，可想而知，汉民族的衣裳自古就不仅仅是穿着与欣赏的物品，它承载了更多的意义。\r\n\r\n一般说来，服装有三大功能：实用功能，比如保暖、保护（如盔甲）；审美功能；社会功能。每个民族对这三大功能的重视程度各不相同。例如，生活在北极圈的因纽特人肯定更在乎实用功能；而生活在新几内亚的土著们，虽然当地天气炎热，根本不需要穿衣服，却依然打扮的花枝招展，特别是土著男人，这便是对审美功能的突出；而中国古人，对服装的社会功能，从黄帝开始便有了超乎他族的重视。\r\n\r\n汉族在上古自称华夏，据《尚书正义》记载，“冕服华章曰华，大国曰夏。”又《左传·定公十年》疏：“中国有礼仪之大，故称夏；有章服之美，谓之华。”华夏便是服饰华丽的大族，或者举止有礼、衣裳华美的民族。\r\n\r\n孔子曾感叹：“微管仲，吾其被发左衽矣！”意思说若不是管仲提倡“尊王攘夷”的观念，恐怕我们就要被迫像蛮夷那样披散头发，穿着左衽服装了。因为华夏族无论男女，都习惯蓄发束发，衣襟右衽，宽衣博带。而同时期的其他民族，大多断发披发，衣襟左衽，袒露身体或窄衣窄裤。\r\n\r\n服饰成为华夏族及汉族区别于其他民族的最显著的外在表现。\r\n\r\n所以人们才会把晋室南下江东建立东晋称为“衣冠南渡”；所以朱元璋才在推翻蒙元之后，专门命人因循唐宋旧制，恢复汉服。汉服成为汉族传统文化，乃至中原正朔的象征。这是汉服的民族属性。\r\n\r\n为何汉族会如此推崇自己的服装，当然，首先是因为它的华美，此乃服装的审美功能；其次，也更加重要的在于汉族服饰与礼仪的紧密关系，汉服可以说是汉礼的重要组成部分，正所谓“衣冠上国，礼仪之邦”。\r\n\r\n见什么人穿什么衣服，或者做什么事穿什么衣服，这是人类从古到今遵循的服饰礼仪。中国古人对此更是极为重视。堪称西周礼仪大全的《礼记》，就对当时的服饰礼仪作出了明确且具体的规定，并成为后世继承祖制的规□。\r\n\r\n华夏族如此自信自傲于本族的服装，本质是自信自傲于本族的礼仪规范。礼仪象征人类从蒙昧进化到文明，而服装正是礼仪的外化和体现。这是汉服的礼仪属性。\r\n\r\n礼仪本身不分档次，无论皇帝还是老百姓，都要祭祖；但礼仪的外化——服装却是分等级的。不同地位的人穿着不同的服装行同样的礼，有差别有统一。最明显的例子即是从唐以后，明黄专属皇帝，连太子都不可逾矩。\r\n\r\n这种森严的服装等级本身也是礼仪的一部分：以各个阶层的人严格遵守本阶层的规矩，体现了古人心中的规范的宇宙图景和严谨的世界运行规律。这是汉服的等级属性。\r\n\r\n汉服以汉礼为核心，以极具汉族民族特色的式样为表现，以严格的等级制度为重要特征，三位一体的构成自己的文化传统和社会属性。\r\n\r\n汉服是汉族文化的全息微缩版\r\n\r\n汉服这三大社会属性分别与文化审美、经济技术和政治法律严格对应。\r\n\r\n汉服的民族属性是汉服文化的最显著表现，更直接体现了汉族的审美情趣。汉服的宽大飘逸体现了汉族温和包容的民族性格；汉服的端正舒缓体现了汉族拘礼又从容的行为方式；汉服的严格蔽体体现了汉族对蒙昧野蛮的厌恶（以裸体以及紧身贴体为象征，比如明朝官员就认为在澳门登陆的葡萄牙人像猴子，因为当时的葡萄牙男人下身穿着白色紧身裤袜，并用填充物对生殖器作出夸大），对文明教化的推崇，等等。\r\n汉服的礼仪属性的强调则必须基于经济技术的发展，正所谓“仓禀足而知礼节”。若连温饱都未达到，哪有足够的原料用于复杂的服饰。中国古人对经济水平的描述就往往取自吃穿这两方面，例如缺衣少食、饥寒、温饱、衣食无忧、丰衣足食等等。而若没有一定的纺织、制衣技术，想生产不同档次的面料和服装也不太可能。中国古人最早发现并使用纤维之王——蚕丝，织出美丽的绸缎，并在此基础上逐渐形成高超的丝绸加工技术，如织锦、刺绣、缂丝等等。如此才能制造出令外邦人士艳羡的丝绸服饰。\r\n汉服的等级属性则与政治法律紧密挂钩。汉服的等级制度属于整个等级制度的一部分，有明文规定，有专门的审查监管制度，并由法律对破坏此制度的行为和主体作出惩罚，从而保证汉服等级制度得到严格的遵守与施行。\r\n\r\n可以说，一件古代的精美汉服就是汉族文化的一个全息微缩版本，从中可以伸发出汉族物质与精神文化的方方面面，就好像一根猴毛能变出一个孙悟空那样的全息效果。\r\n\r\n汉服三属性的消失\r\n\r\n汉服三属性保留完整的最后一个历史时期是明朝。虽然蒙元统治中原近100年，但蒙元政府对汉族文化并没有作出过多的干预，采取的是压制与隔离的统治方针。明朝建立以后，朱元璋下令因循唐宋，全面恢复汉礼，其中自然包括汉服。\r\n\r\n明朝以后，汉服逐渐消亡。请注意，汉服的消亡不是一时，而是一个长期的历史过程。在消亡过程中，汉服的社会功能三属性也是先后遭到破坏。\r\n\r\n汉服民族属性的消失\r\n\r\n最先消失的是汉服的民族属性，这也是汉服爱好者每每提及必痛心疾首的一段往事。发生在清朝。\r\n\r\n满族入关之初，实行满汉分立，未对汉族服饰文化作出过多的干涉。但等政治局势稍微稳定，立即开始对以汉服为核心的汉族服饰文化下手。首先是剃头，这就涉及到汉族最根本的身体文化和作为礼仪之基础的孝道——“身体发肤，受之父母，不敢毁伤，孝至始也。”\r\n\r\n剃头这种野蛮残暴的行为自然遭到汉族人士的强烈反对，于是便有了“留头不留发，留法不留头”的说法。汉族男人斗争过，但最终失败，于是放下发髻，剔去头上大部分头发，仅在后脑勺留一缕长发编成辫子。男子发髻的放下，使得依附于发髻的冠礼也随之灭亡，流传几千年的冠冕制度至此消失。\r\n\r\n其次，要求汉族改穿满衣，经过斗争，满汉达成协议，有了“十从十不从”的说法。其中最重要的是“男从女不从”，即汉族男子改穿满族的长袍马褂，汉族女子则保留传统的两截穿衣（即上衣下裳，同时也保留了三缕梳头的发饰传统）。汉族男子服饰从此中断，汉族女子服饰传统尚在延续，并在有清一代形成满汉合流。\r\n\r\n评心而论，汉族女服在清朝属于文化强势，影响了满族女装。但另一方面，汉族女服也不得不对政治地位高高在上的旗袍作出让步，或多或少发生了改变。\r\n\r\n第一，汉族女服中断了宋明以来向紧窄方向发展的趋势（汉族女服从先秦至唐是一个逐渐增宽的过程，在盛唐时期达到最宽；晚唐和五代时，已经趋瘦，两宋继续；明朝时，江南最为崇尚瘦窄，甚至出现收腰马甲，似乎有“省道”的雏形），而变得愈发宽大，为的是显示与长窄的满族旗袍有区别（后来旗袍反而受此影响，也愈发宽大）。\r\n\r\n第二，汉族女服开始大量使用纽扣、立领等元素。汉族老早便使用纽扣，不过很少出现在礼服上，而且纽扣一般也用在暗处不外露；属于立领的元宝领在明代也频频出现。但有清一代，新一轮的“胡服骑射”出现，纽扣和立领逐渐全面取代了汉服传统的博带与交领。纽扣的普及主要是功能上的，立领的普及则有更多的审美趣味。例如受中国影响极深的越南，其女性传统礼服奥黛可是说是长袖旗袍搭配宽松长裤。而立领显然不应出现在越南这种热带国家（没有一个热带国家的衣领是高领，长衫长裤倒可以解释为隔离热气和避免晒伤）\r\n\r\n第三，汉族女服的装饰从宋明的平净素雅转向奢靡纷繁，这既有装饰技术提高的结果，也有满族审美趣味的影响。\r\n\r\n经历清代，汉服的民族属性中的一半（男性汉服）消失殆尽，另一半（女性汉服）也偏离了原来的发展路线。\r\n\r\n清末民国，西风东渐，这仅存的一半民族属性又受到西方服饰文化和现代服饰潮流的强势干预，使得上衣下裙的汉族女服出现了收腰、窄袖。衣摆越来越高，甚至露出肚脐；袖子越来越短，纤纤玉臂显露一半。裙子以黑色百褶为基本样式，去除了之前复杂的装饰，显露出具有现代审美意味的简单大方。服饰文化的其他方面，例如鞋履、头饰等更是西风压倒东风，汉族女服呈现出前所未有的中西合璧、华夷共处的面貌。（旗袍不在讨论之列）\r\n\r\n但汉族女服的真正消失并非在民国，而是在上世纪50以后，经历了革命化和西方化的进一步冲击，终于烟消云散：地主和资产阶级的被打到，使得具有“封建色彩”的汉族女服的最后堡垒被攻破（富人肯定穿得比穷人讲究精致，并且齐全）；男女平等的过度提倡导致对男女差异的无视，使得女性服装向男性看齐，甚至直接穿着男装；一边倒和西化风潮使得苏联服装和西方服装大行其道。这些都迅速挤压了汉族女服的生存空间，直至消亡。\r\n\r\n翻翻《民族画报》，各位便会深有感触。《民族画报》创刊号封面是民族大团结的照片，其中汉族女性穿的是斜襟高领衫和马面裙+百褶裙，这是典型的民国末年北方汉族妇女的装扮；而之后的民族大团结中，汉族女性服装变成了布拉吉、黄军装、西式套装等等。\r\n\r\n\r\n\r\n至此，汉族服饰文化的民族属性基本消亡，或者直接说汉族服饰基本消亡。\r\n\r\n汉服礼仪属性的消失\r\n\r\n汉服的礼仪属性在有清一代并未灭亡，只是换了一个形式。从过去的以汉服为载体，转变为以满服为载体（专指男性）。例如官府的补子，以前都是绣在汉族传统的官服圆领衫上，清朝改绣在满族男子长袍上，而且命名沿用汉名：朝服、补服、蟒袍。\r\n\r\n但汉礼毕竟最早并一直以汉服为载体发生发展，所以更换载体之后，肯定会出现差别。例如汉族冠冕制度的消失，使得汉族男子的成人礼消失。而且满族礼仪也肯定会一定程度的影响汉礼。\r\n\r\n不过汉服的礼仪属性最初的大改始于民国。受西方的影响，国人对纷繁复杂的古代礼仪作出了大刀阔斧的删减，况且不少汉礼也随着它们清代的载体——满服的灭亡而走进历史的故纸堆。\r\n\r\n例如剪辫。若当时东方没有遭遇西方，而汉族终于推翻满族，我想剪完辫子之后应该重新蓄发，皇帝下令恢复宋明的冠冕旧制，或略加创新。然而当时东方遭遇了西方，于是辫子是剪掉了，男子的头发却再没长起来，西式短发成功取代了满族辫子，又一次以夷变夏，并且以现代文明的口号让大家自觉自愿的接受。\r\n\r\n1929年4月16日，国民政府颁布《服制条例》，这是中国最后一次以中央政府的名义颁布服饰制度条例。\r\n\r\n与前朝的服饰条例相比，《服制条例》只规定了简简单单的四种服装：男子礼服、女子礼服、男子制服、女子制服。服装种类的减少，象征礼仪的简化。礼服一般用于重大场合，制服则主要指公务员的工作服。至于其他场合（例如祭祀），则没有要求（因为国家级别的祭祀礼仪本身不复存在），对民间更没有涉及。\r\n\r\n需要指出的是，《服制条例》明确将长袍马褂和改良旗袍分别作为男女礼服，改良自日本学生服或东南亚工作服的中山装则成为男子制服，传统的汉族女服上衫下裙成为女子礼服的另外一个选择，女子制服则选用改良旗袍。\r\n新中国建立之初，不再像之前所有朝代那样颁布服制规定，更多的是一种默认的服饰礼仪。一方面，长袍马褂和上衫下裙由于有太多的所谓封建社会的陈腐气息而被抛弃（中式夹袄和长裤则依然保留，因为是属于贫苦大众的服饰），中山装与旗袍延续作为礼服或制服；另一方面，苏联的影响使得列宁装和布拉吉风靡中国。\r\n\r\n上世纪60年代至70年代末，由于左的思想加剧，旗袍因其资产阶级属性，布拉吉因其所谓的小布尔乔亚属性都被而被打入十八层地狱，列宁装因为苏联成了“修正主义国家”也逐渐淡出人们的视线。中山装稍加改动，以“毛式制服”的形象，在个人崇拜的作用下，成了最重要的男性礼服和制服；军装流行。这个时期还有重要的特征就是男女服饰、装扮差异不明显，强调男女平等过头，演变成男女无差别。\r\n\r\n80年代以后至今，欧美服饰文化和潮流再次袭来，其影响甚至比民国时期更加强大，几乎成为主导。中山装越来越少见，西装越来越普及；旗袍偶尔露真容，套装和晚礼服频频亮相。汉服早已不在，连中山装、旗袍这些有中国特色的现代服装也被束之高阁。中国人服装的西化以“现代”和“潮流”的名义成为主导话语。\r\n\r\n汉服等级属性的消失\r\n\r\n汉服的等级属性，随着整个等级制度的瓦解，自然走向了历史的尽头。王朝覆灭，民国建立，自由平等由西方传来并被广泛接受，森严等级成为斗争对象。\r\n\r\n汉服的等级属性消失是好事，但从世纪50年代以后，却出现反复。长袍马褂（当然，这不是汉服）和上衫下裙被认为是封建主义的服装，旗袍被认为是资产阶级的服装。这几种本来在民国已经普及至各阶层的服装就因为这莫须有的指责遭受厄运。\r\n\r\n同时，政治上森严的等级制度虽然消失，但经济上变动性较大的等级制度则取而代之。以西方haut couture为代表的高级定制也逐渐影响中国。在古代，就算你有钱也可能不允许穿某种颜色或款式的服装；在当下，只要你有钱也可以买一套云锦龙袍来穿。\r\n\r\n汉服的三属性，并非截然分开，先后消失；三者纠缠在一起，互相影响，互相牵连。\r\n\r\n皮之不存，毛将焉附？当汉服本身（民族属性）都消亡了，它的礼仪属性和等级属性自然无所依归；而等级属性的灭亡，也是礼仪属性减少的重要原因。\r\n\r\n随着中国国力的增强，民族自信的提高，旗袍、中山装也渐渐活跃；所谓的中国元素每年也频繁出现在外国的各大时装周中；而汉服复兴运动的兴起，更是让已经消亡的汉服有了重生的希望。\r\n\r\n但是，在中国，服装自古与政治、与思想理念紧密相关。所谓垂衣裳治天下，服饰制度从来由上而下的制定推广施行。民间的任何运动只能影响，而不能决定传统服装的回归。\r\n\r\n汉服，作为非正常消亡的中国传统文化之一种，也许会成为传统文化重新全面弘扬的先行者。\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/10/63314641.jpg', '0', '1', null, 'post', 'publish', null, '2', '1', '1', '1', '0', '1', '3746', '573', '0');
INSERT INTO `typecho_contents` VALUES ('21', 'hanfu.jpg', 'hanfu-jpg', '1570337010', '1570337010', 'a:5:{s:4:\"name\";s:9:\"hanfu.jpg\";s:4:\"path\";s:33:\"/usr/uploads/2019/10/63314641.jpg\";s:4:\"size\";i:41094;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '20', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('22', '114.jpg', '114-jpg', '1570337109', '1570337109', 'a:5:{s:4:\"name\";s:7:\"114.jpg\";s:4:\"path\";s:34:\"/usr/uploads/2019/10/557175415.jpg\";s:4:\"size\";i:402067;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '23', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('23', '痛车', '23', '1570337115', '1570337115', '<!--markdown-->![114.jpg][1]\r\n痛车，文化发源于日本，属于ACGN文化的一种，在日本，对ACGN文化有爱的人们为了彰显个性，将喜爱的动漫角色、动漫公司或者角色名字之类的字画贴（喷）在车上，以作装饰。“痛车”一词的一种说法是：这样装饰车子，就像是人类纹身一样，故而被称为“痛车”。在中国，痛车文化也渐渐融入人们的生活。\r\n\r\n含义\r\n在日本，对ACGN文化有爱的人们为了彰显个性，将喜爱的动漫角色或者动漫公司或者角色名字之类的字画贴（喷）在车上。以作装饰，被这样装饰过的车子，被称为痛车。痛车未必是汽车。自行车、机车、电动车、车模等都可以用来痛。\r\n自2000年之后，ACGN文化在日本开始流行，因此也开始有人在车子上动脑。且随着ACGN文化的对外输出，一起流传到台湾、菲律宾、美国及欧洲等地。\r\n摩托车号称“痛单车(Itansya)”\r\n痛单车\r\n痛单车\r\n自行车被叫做“痛响铃(Itatyari)”。\r\n所谓的“痛”，是在电视和网络等媒体广为流传的人造复合词，指不可挽回的惨状，或是本人非常满意，而他人看来脱离常规的状态。\r\n这些年，“痛车”激增，已经到了被媒体所关注的数目，在日本各地也召开过数百辆“痛车”云集的专题活动。\r\n\r\n来源\r\n关于痛车这个词的来源，有几种说法：\r\n1.日语中有一个词语叫「视线が痛い」，多用在某人做出非主流的事情时候周围人围观导致气氛尴尬的时候。痛车对于普通一般人来说，视线痛也是完全可以理解的，所以先有传统的日语这个词语开始演变成了痛车这个概念。\r\n2. 给人纹身会很痛，于是车主人就把“纹身”过的车称为痛车。\r\n3. 在日本，如果要在车上画图案之类的东西要交税，由于开痛车的很多是年轻人，额外的税收使他们很心痛，所以叫痛车。\r\n4. 因为读音问题：paint car(涂上颜色的车) => pain car(疼痛的车) => 痛车\r\n5. 入了围城的人基本上是不会继续动漫爱好了，能够把车变成痛车的人自然是单身了，唉，可是这样一来，有哪个MM会来搭一辆满身全是花花少女图案的便车呢？单身的动漫迷们一旦选择印上自己热爱的角色上去，客观上就失去了MM搭车的机会，心“痛”啊……\r\n6. 因为画上各种图案会对车子本身的形状等造成视觉上的偏差（和迷彩同理），这样会影响车子原本工业设计的美感，所以到底是保留原汁原味的设计还是贴上喜欢的动漫让人很头痛。\r\n7.亦有说法是日本在经济起飞之后开始出现大批进口车，从“意大利车”（イタリア车，Itaria sha）转成“Itasha”\r\n\r\n表现\r\n据说给自己的车加上插图这样的行为，是表现对卡通人物的敬爱之情的手段之一。不仅外部装潢，连车内部的座位装饰等都精雕细刻，为做一辆“痛车”而花数百万日元的也大有人在。可是，制作痛车关系到版权的问题便进入了不明了的区域，也有难以判断之处。之所以没有发生大问题的理由之一，说不定是由于粉丝们的宣传带来的经济效果，其影响力已是不容忽视的了。\r\n并且，“痛车”打算拓宽它的活动范围，甚至参加了日本国内最受欢迎的车赛“2009 SUPER GT系列(GT300级）。初音 GSR BMW Z4 GT3已经凭借良好的燃油经济性获得了不错的成绩。\r\n车身上大胆地画着大受欢迎的二次元人物。虽然日本的动画片作为超酷的pop流行文化，\r\n已经得到了全世界的认可，但是在赛车界会遇到怎样的面孔呢？GT从2009年3月开始举行，开幕战打得很漂亮，今后的动向也很值得期待。\r\n而2008年9月在枥木Twin Ring召开的“世界摩托车锦标赛日本最高奖(MotoGP)”，由于雅马哈主要赞助的菲亚特公司的请求，画有“鲁邦三世”的摩托车得以参赛，与菲亚特500的限定款式设计相同的摩托车也已经发售，“痛车”已不仅仅局限于日本。但在中国，痛汽车，尤其是大面积的，似乎还并不多见，倒是痛响铃更多见一些。\r\n\r\n制作\r\n早期的痛车是以彩绘方式直接绘于车身，随着彩色大图输出的技术日渐普遍\r\n，现今的痛车大多是将图案输出之后再黏贴于车身，较早期彩绘方式容易且方便撕下图案更换。不过贴纸缺陷在于寿命只有一到两年，不过一般情况下一年就想要更换样式和图案了。\r\n痛自行车则是在轮圈加上塑胶盖，俗称：“封闭轮”，也叫：“自行车风盘”，将ACG图案贴在上面；另外也有在车架钢管间的三角型区域装上贴有ACG图案的塑胶板，少数则直接在车架钢管上贴图案。\r\n\r\n痛车集会\r\n痛G festa\r\n“痛G festa”是由以出版汽车相关杂志、书籍闻名的艺文社主办，“festa”为意大利语中“祭典”的意思。人们通常称之为“痛G节”，因为这是痛车宅们的节日，活动第一届举办时间为2008年11月09日，所以说始于2008年，举办场地为东京的台场地区，至2013年9月22日已是举办了八届，是一个展示车主其\r\n设计能力，对作品的爱，装饰心得的交流等等等等的活动。会场门票为1000日元，（其中500日元可以在内场当购物券使用）[2] ，因为会场经常会贩售一些专场限定的周边，而且还有知名艺人光临现场，所以不仅是痛车迷，就连一般的动漫爱好者也积极支持，号称史上最具规模痛车展。\r\n德岛眉山痛车聚会\r\n眉山是德岛市的名山，被认为是该市的象征，是一座海拔280米的丘陵。而眉山召开的痛车大[3]\r\n会上的车多数来自德岛县，此外还有冈山县跟鸟取县来的车。而且，眉山山顶的会场上，展示车辆是禁止触摸的，不过可以任意拍摄。虽然参加的痛车数量并不算多，但质量还是非常不错的。\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/10/557175415.jpg', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '1687', '232', '0');
INSERT INTO `typecho_contents` VALUES ('24', '1067418-20180427162658896-1470827026.png', '1067418-20180427162658896-1470827026-png', '1570337288', '1570337288', 'a:5:{s:4:\"name\";s:40:\"1067418-20180427162658896-1470827026.png\";s:4:\"path\";s:34:\"/usr/uploads/2019/10/720973315.png\";s:4:\"size\";i:79238;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '25', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('25', '佛祖保佑 永无BUG', '25', '1570337280', '1616253663', '<!--markdown-->![1067418-20180427162658896-1470827026.png][1]\r\n写字楼里写字间，写字间里程序员；\r\n程序人员写程序，又拿程序换酒钱。\r\n酒醒只在网上坐，酒醉还来网下眠；\r\n酒醉酒醒日复日，网上网下年复年。\r\n但愿老死电脑间，不愿鞠躬老板前；\r\n奔驰宝马贵者趣，公交自行程序员。\r\n别人笑我忒疯癫，我笑自己命太贱；\r\n不见满街漂亮妹，哪个归得程序员？\r\n\r\n    /**\r\n     *                             _ooOoo_\r\n     *                            o8888888o\r\n     *                            88\" . \"88\r\n     *                            (| -_- |)\r\n     *                            O\\  =  /O\r\n     *                         ____/`---\'\\____\r\n     *                       .\'  \\\\|     |//  `.\r\n     *                      /  \\\\|||  :  |||//  \\\r\n     *                     /  _||||| -:- |||||-  \\\r\n     *                     |   | \\\\\\  -  /// |   |\r\n     *                     | \\_|  \'\'\\---/\'\'  |   |\r\n     *                     \\  .-\\__  `-`  ___/-. /\r\n     *                   ___`. .\'  /--.--\\  `. . __\r\n     *                .\"\" \'<  `.___\\_<|>_/___.\'  >\'\"\".\r\n     *               | | :  `- \\`.;`\\ _ /`;.`/ - ` : | |\r\n     *               \\  \\ `-.   \\_ __\\ /__ _/   .-` /  /\r\n     *          ======`-.____`-.___\\_____/___.-`____.-\'======\r\n     *                             `=---=\'\r\n     *          ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\r\n     *                     佛祖保佑        永无BUG\r\n     *            佛曰:\r\n     *                   写字楼里写字间，写字间里程序员；\r\n     *                   程序人员写程序，又拿程序换酒钱。\r\n     *                   酒醒只在网上坐，酒醉还来网下眠；\r\n     *                   酒醉酒醒日复日，网上网下年复年。\r\n     *                   但愿老死电脑间，不愿鞠躬老板前；\r\n     *                   奔驰宝马贵者趣，公交自行程序员。\r\n     *                   别人笑我忒疯癫，我笑自己命太贱；\r\n     *                   不见满街漂亮妹，哪个归得程序员？\r\n    */\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/10/720973315.png\r\n#### 神兽保佑 永无BUG\r\n\r\n    // ┏┓　　　┏┓\r\n    // ┏┛┻━━━┛┻┓\r\n    // ┃　　　　　　　┃ 　\r\n    // ┃　　　━　　　┃\r\n    // ┃　┳┛　┗┳　┃\r\n    // ┃　　　　　　　┃\r\n    // ┃　　　┻　　　┃\r\n    // ┃　　　　　　　┃\r\n    // ┗━┓　　　┏━┛\r\n    // ┃　　　┃ 神兽保佑　　　　　　　　\r\n    // ┃　　　┃ 代码无BUG！\r\n    // ┃　　　┗━━━┓\r\n    // ┃　　　　　　　┣┓\r\n    // ┃　　　　　　　┏┛\r\n    // ┗┓┓┏━┳┓┏┛\r\n    // ┃┫┫　┃┫┫\r\n    // ┗┻┛　┗┻┛\r\n\r\n#### 美女保佑 永无BUG\r\n\r\n    //\r\n    //                       .::::.\r\n    //                     .::::::::.\r\n    //                    :::::::::::\r\n    //                 ..:::::::::::\'\r\n    //              \'::::::::::::\'\r\n    //                .::::::::::\r\n    //           \'::::::::::::::..\r\n    //                ..::::::::::::.\r\n    //              ``::::::::::::::::\r\n    //               ::::``:::::::::\'        .:::.\r\n    //              ::::\'   \':::::\'       .::::::::.\r\n    //            .::::\'      ::::     .:::::::\'::::.\r\n    //           .:::\'       :::::  .:::::::::\' \':::::.\r\n    //          .::\'        :::::.:::::::::\'      \':::::.\r\n    //         .::\'         ::::::::::::::\'         ``::::.\r\n    //     ...:::           ::::::::::::\'              ``::.\r\n    //    ```` \':.          \':::::::::\'                  ::::..\r\n    //                       \'.:::::\'                    \':\'````..\r\n    //\r\n#### 已OK，无BUG \r\n\r\n    //                      d*##$.\r\n    // zP\"\"\"\"\"$e.           $\"    $o\r\n    //4$       \'$          $\"      $\r\n    //\'$        \'$        J$       $F\r\n    // \'b        $k       $>       $\r\n    //  $k        $r     J$       d$\r\n    //  \'$         $     $\"       $~\r\n    //   \'$        \"$   \'$E       $\r\n    //    $         $L   $\"      $F ...\r\n    //     $.       4B   $      $$$*\"\"\"*b\r\n    //     \'$        $.  $$     $$      $F\r\n    //      \"$       R$  $F     $\"      $\r\n    //       $k      ?$ u*     dF      .$\r\n    //       ^$.      $$\"     z$      u$$$$e\r\n    //        #$b             $E.dW@e$\"    ?$\r\n    //         #$           .o$$# d$$$$c    ?F\r\n    //          $      .d$$#\" . zo$>   #$r .uF\r\n    //          $L .u$*\"      $&$$$k   .$$d$$F\r\n    //           $$\"            \"\"^\"$$$P\"$P9$\r\n    //          JP              .o$$$$u:$P $$\r\n    //          $          ..ue$\"      \"\"  $\"\r\n    //         d$          $F              $\r\n    //         $$     ....udE             4B\r\n    //          #$    \"\"\"\"` $r            @$\r\n    //           ^$L        \'$            $F\r\n    //             RN        4N           $\r\n    //              *$b                  d$\r\n    //               $$k                 $F\r\n    //               $$b                $F\r\n    //                 $\"\"               $F\r\n    //                 \'$                $\r\n    //                  $L               $\r\n    //                  \'$               $\r\n    //                   $               $\r\n#### 七龙珠孙悟空\r\n\r\n    /*\r\n                               _\r\n                               \\\"-._ _.--\"~~\"--._\r\n                                \\   \"            ^.    ___\r\n                                /                  \\.-~_.-~\r\n                         .-----\'     /\\/\"\\ /~-._      /\r\n                        /  __      _/\\-.__\\L_.-/\\     \"-.\r\n                       /.-\"  \\    ( ` \\_o>\"<o_/  \\  .--._\\\r\n                      /\'      \\    \\:     \"     :/_/     \"`\r\n                              /  /\\ \"\\    ~    /~\"\r\n                              \\ I  \\/]\"-._ _.-\"[\r\n                           ___ \\|___/ ./    l   \\___   ___\r\n                      .--v~   \"v` ( `-.__   __.-\' ) ~v\"   ~v--.\r\n                   .-{   |     :   \\_    \"~\"    _/   :     |   }-.\r\n                  /   \\  |           ~-.,___,.-~           |  /   \\\r\n                 ]     \\ |                                 | /     [\r\n                 /\\     \\|     :                     :     |/     /\\\r\n                /  ^._  _K.___,^                     ^.___,K_  _.^  \\\r\n               /   /  \"~/  \"\\                           /\"  \\~\"  \\   \\\r\n              /   /    /     \\ _          :          _ /     \\    \\   \\\r\n            .^--./    /       Y___________l___________Y       \\    \\.--^.\r\n            [    \\   /        |        [/    ]        |        \\   /    ]\r\n            |     \"v\"         l________[____/]________j  -Row   }r\"     /\r\n            }------t          /                       \\       /`-.     /\r\n            |      |         Y                         Y     /    \"-._/\r\n            }-----v\'         |         :               |     7-.     /\r\n            |   |_|          |         l               |    / . \"-._/\r\n            l  .[_]          :          \\              :  r[]/_.  /\r\n             \\_____]                     \"--.             \"-.____/\r\n    33          \r\n            */\r\n    /*\r\n                                    MMMMM\r\n                                      MMMMMM\r\n                                        MMMMMMM\r\n                                         MMMMMMMM     .\r\n                                          MMMMMMMMM\r\n                                          HMMMMMMMMMM\r\n                                           MMMMMMMMMMMM  M\r\n                                           MMMMMMMMMMMMM  M\r\n                                            MMMMMMMMMMMMM  M\r\n                                            MMMMMMMMMMMMM:\r\n                                            oMMMMMMMMMMMMMM\r\n                  .MMMMMMMMMMMMMMo           MMMMMMMMMMMMMMM M\r\n            MMMMMMMMMMMMMMMMMMMMMMMMMMM      MMMMMMMMMMMMMMMM\r\n              MMMMMMMMMMMMMMMMMMMMMMMMMMMM.  oMMMMMMMMMMMMMMM.M\r\n                MMMMMMMMMMMMMMMMMMMMMMMMMMMM  MMMMMMMMMMMMMMMM\r\n                  MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\r\n                    oMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\r\n                      MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\r\n                        MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM:                     H\r\n                         MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM                  .         MMM\r\n                          MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM              M       MMMMMM\r\n                           .MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM          M   MMMMMMMMMM\r\n                    MM.      MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM       M MMMMMMMMMMMM\r\n                        MM    MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM    .MMMMMMMMMMMMMM\r\n                          MM  MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\r\n                            MM MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\r\n                   .MMMMMMMMM MMMMMMMMMMMMMMMMMMMMMMMM.MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\r\n                      HMMMMMMMMMMMMMMMMMMMMM.MMMMMMMMM.MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\r\n                         MMMMMMMMMMMMMMM MMM.oMMMMMMM..MMMMMMMMM:MMMMMMMMMMMMMMMMMMMMMMM\r\n                           MMMMMMMMMMMMMM MM..MMMMMMM...MMMMMMM. MMMMMMMMMMMMMMMMMMMMM\r\n                             MMMMMMMMMMMMMMM ..MMMMMM...MMMMMM ..MMMMMMMMMMMMMMMMMMM\r\n                              MMMMMMM:M.MMM.M.. MMMMM M..MMMMM...MMMMMMMMMMMMMMMMMM  MMM\r\n                                MMMM. .M..MM.M...MMMMMM..MMMMM.. MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM .\r\n                                 MMMM..M....M.....:MMM .MMMMMM..MMMMMMM...MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\r\n                                  MMM.M.. ...M......MM.MMMMM.......MHM.M  .MMMMMMMMMMMMMMMMMMMMMMMMM\r\n                             MMMMMMMM..MM. . MMM.....MMMMMM.M.....M ..MM..M MMMMMMMMMMMMMMMMMMM\r\n                                .MMMMMHMM. ..MMMM. MMM............o..... . .MMMMMMMMMMMMMMM\r\n                                   MMM. M... .........................M..:.MMMMMMMMMMMM\r\n                                     oMMM............ .................M.M.MMMMMMMMM\r\n                                        .....MM........................ . MMMMMM\r\n                                       M.....M.....................o.MM.MMMMMMMM.\r\n                                        M........................M.. ...MMMMMMMMMMMMMo\r\n                                          :....MMM..............MMM..oMMMMMMM\r\n                                           M...MMM.............MMMMMMM\r\n                                              .............:MMMMMMMM\r\n                                              M..... MMM.....M\r\n                                              M M.............\r\n                                              ................M\r\n                                           ooM.................MM  MoMMMMMoooM\r\n                                      MMoooM......................MoooooooH..oMM\r\n                                  MHooooMoM.....................MMooooooM........M\r\n                                oooooooMoooM......... o........MoooooooM............\r\n                                Mooooooooooo.......M.........Moooooooo:..............M\r\n                               MooMoooooooooM...M........:Mooooooooooo:..............M\r\n                              M..oooooooooooo .........Mooooooooooooooo..............M\r\n                             M...Mooo:oooooooo.M....ooooooooooooooooooo..M...........M\r\n                              ...oooooMoooooooM..Mooooooooooooo:oooooooM.M...........M.\r\n                             M...ooooooMoo:ooooMoooooooooooooHoooooooooH:M. ...........:\r\n                             M..MoooooooMoooooooooooooooooo:ooooooMooooMoM..............M\r\n                             M..ooooooooooMooooooooooooooHoooooooMooHooooM...............M\r\n                             ...ooooooooooooooooooo:MooooooooooooooMoMoooM................\r\n                            M...oooooooooooooooooooooooooooooooooooooMooMM................M\r\n                            ...MooooooooooooooooooooooooooooooooooooooooMo ................\r\n                            ...MooooooooooooooooooooooooooooooooooooooooM M................M\r\n                           M...ooooooooooooooooooooooooooooooooooooooooM   ................M\r\n                           ...MoooooooooooooooooooooooooooooooooooooooMM   .:...............\r\n                           .....MooooooooooooooooooooooooooooooooooooMoo       .............M\r\n                           M...... ooooooooooooooooooooooooooooooooooooM       M..............M\r\n                           M........MooooMMM MM MM  MMMMMMMMMooooooooM         M...............M\r\n                           .........HM     M:  MM :MMMMMM          M           M...............\r\n                          M..........M     M   MoM M                           M................M\r\n                          M.........:M  MoH  M M M MooooHoooMM.   M             M...............M\r\n                          M..........Moooo MMooM    oooooMooooooooM              M..............H\r\n                          M.........MooooM  Mooo  : ooooooMooooMoooM              M........ . .o.M\r\n                          H..  .....ooooo   oooo  M MooooooooooooooM               M... MMMMMMMMMMM\r\n                          MMMMMMMMMMooooM M oooo  .  ooooooMooooooooM              .MMMMMMMMMMMMMMM\r\n                          MMMMMMMMMMooooH : ooooH    oooooooooooooooo               MMMMMMMMMMMMMMM\r\n                          MMMMMMMMMMoooo    ooooM    Moooooooooooooooo              .MMMMMMMMMMMMMMM\r\n                          MMMMMMMMMMoooo    ooooM    MooooooooooooooooM              MMMMMMMMMMMMMMM\r\n                          MMMMMMMMMMoooM    ooooM     ooooooooooooooooo               MMMMMMMMMMM:M\r\n                          MMMMMMMMMMoooM   MooooM     oooooooooooMoooooo               MH...........\r\n                           . ......Mooo.   MooooM     oooooooooooooooooo              M............M\r\n                          M.M......oooo    MooooM     Moooooooooooooooooo:           .........M.....\r\n                          M.M.....Moooo    MooooM      ooooooooooooooooooM            .M............\r\n                          .......MooooH    MooooM      oooooooooMoooooooooo          M..o...M..o....M\r\n                          .o....HMooooM    MooooH      MooooooooMooooooooooM          .:M...M.......M\r\n                         M..M.....MoooM    :oooo:    .MooooooooHooMoooooooooM         M M... ..oM.M\r\n                          M...M.:.Mooo. MMMMooooo   oooooooooooMoooooooooooooM          ....M. M\r\n                           M:M..o.Moooooooooooooo MooooooooooooooMooooooooooooM          .Mo\r\n                                  MooooooooooooooMooooooooooooMoMoooooooooooooo\r\n                                  Mooooooooooooooo:ooooooooooooooooooooooooooooo\r\n                                  ooooooooooooooooMooooooooooMoooooooooooooooooo\r\n                                  ooooooooooooooooMoooooooooooMooooooooooooooooHo\r\n                                  ooMooooooooooooooMoooooooooooooooooooooooooooMoM\r\n                                 MooMoooooooooooooo.ooooooooooooooooooooooooooo:oM\r\n                                 MoooooooooooooooooooooooooooooooooooooooooooooooM\r\n                                 MoooMooooooooooooooMooooooooooooooooooooooooooooo.\r\n                                 MoooMooooooooooooooMoooooooooooooooooooooooooMooooM\r\n                                 MooooooooooooooooooMoooooooooooooooooooooooooMoooooM\r\n                                 MooooMoooooooooooooMoooooooooooooooooooooooooMoHooooM\r\n                                 ooooooMooooooooooooooooooooooooooooooooooooooooMoMoooM\r\n                                MooooooooooooooooooooMooooooooooooooooooooooooooMoooooH:\r\n                                MoooooooMooooooooooooMoooooooooooooooooooooooooooooHoooM\r\n                                MooooooooMoooooooooooMoooooooooooooooooooooooooMoooMooooM\r\n                                Moooooooooooooooooooooooooooooooooooooooooooooo.oooMooooo\r\n                                MoooooooooooooooooooooooooooooooooooooooooooooMoooooooooM\r\n                                 MooooooooooooooooooooMoooooooooooooooooooooooooooooooooM\r\n                                  MooooooooooooooooooooMHooooooooooooooooooooMoooo:ooooo\r\n                                   MMooooooooooooooooooMoMHoooooooooooooooooooooooMooooo\r\n                                    MMoooooooooooooooMMooo MMooooooooooooooooooooooooooM\r\n                                    MMMoooooooooooooMooooo  oooooooooooooooooooooMooooo\r\n                                    MooMMoooooooooMoooMMoM  ooooHooooooooooooooooMooooM\r\n                                    MooooMooooooMooooMoooM  MoooooMoooooooooooooMooooo\r\n                                    ooooooMMooooooooMooooM  MoooooooooMooooooooooooooM\r\n                                    HooooooMoooooooMooooM    HoooooooHooMooooooooooooo\r\n                                     oooMoooooooooHoooM         MoooooooooMoooooooooM\r\n                                      HooooooooooooHM             MooooooooMMoooooooM\r\n                                       MMMMMMMMMMMMMM                Moooooo:MooooHMM\r\n                                        MMMMMMM: ...                  MMMMMMMMMMMMMM\r\n                                       M............M                  MMMMMMMMM ....\r\n                                       M.MM..........                  M.............M\r\n                                    M ..............MM                 M..............\r\n                                 MMMMM............MMMM                 ..MMMMMMMM ....M\r\n                               MMMMMMMMMMMMMMMMMMMMMMMM               MMMMMMMMMMMMM...M\r\n                            .MMMMMMMMMMMMMMMMMMMMMMMMMM               MMMMMMMMMMMMMMMMMM\r\n                            MMMMMMMMMMMMMMMMMMMMMMMMM                MMMMMMMMMMMMMMMMMMM\r\n                            :MMMMMMMMMMMMMMMMMMH                     MMMMMMMMMMMMMMMMMMM\r\n                               By EBEN Jérôme                        MMMMMMMMMMMMMMMMMM\r\n                                                                     MMMMMMMMMMMMMMM\r\n                                                                      HMMMMMM\r\n             \r\n            */\r\n\r\n#### 皮卡丘\r\n\r\n    /*\r\n            quu..__\r\n             $$$b  `---.__\r\n              \"$$b        `--.                          ___.---uuudP\r\n               `$$b           `.__.------.__     __.---\'      $$$$\"              .\r\n                 \"$b          -\'            `-.-\'            $$$\"              .\'|\r\n                   \".                                       d$\"             _.\'  |\r\n                     `.   /                              ...\"             .\'     |\r\n                       `./                           ..::-\'            _.\'       |\r\n                        /                         .:::-\'            .-\'         .\'\r\n                       :                          ::\'\'\\          _.\'            |\r\n                      .\' .-.             .-.           `.      .\'               |\r\n                      : /\'$$|           .@\"$\\           `.   .\'              _.-\'\r\n                     .\'|$u$$|          |$$,$$|           |  <            _.-\'\r\n                     | `:$$:\'          :$$$$$:           `.  `.       .-\'\r\n                     :                  `\"--\'             |    `-.     \\\r\n                    :##.       ==             .###.       `.      `.    `\\\r\n                    |##:                      :###:        |        >     >\r\n                    |#\'     `..\'`..\'          `###\'        x:      /     /\r\n                     \\                                   xXX|     /    ./\r\n                      \\                                xXXX\'|    /   ./\r\n                      /`-.                                  `.  /   /\r\n                     :    `-  ...........,                   | /  .\'\r\n                     |         ``:::::::\'       .            |<    `.\r\n                     |             ```          |           x| \\ `.:``.\r\n                     |                         .\'    /\'   xXX|  `:`M`M\':.\r\n                     |    |                    ;    /:\' xXXX\'|  -\'MMMMM:\'\r\n                     `.  .\'                   :    /:\'       |-\'MMMM.-\'\r\n                      |  |                   .\'   /\'        .\'MMM.-\'\r\n                      `\'`\'                   :  ,\'          |MMM<\r\n                        |                     `\'            |tbap\\\r\n                         \\                                  :MM.-\'\r\n                          \\                 |              .\'\'\r\n                           \\.               `.            /\r\n                            /     .:::::::.. :           /\r\n                           |     .:::::::::::`.         /\r\n                           |   .:::------------\\       /\r\n                          /   .\'\'               >::\'  /\r\n                          `\',:                 :    .\'\r\n                                               `:.:\'\r\n    \r\n             \r\n            */\r\n\r\n#### 程序员\r\n\r\n    /*\r\n     * ┌───┐   ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┐\r\n     * │Esc│   │ F1│ F2│ F3│ F4│ │ F5│ F6│ F7│ F8│ │ F9│F10│F11│F12│ │P/S│S L│P/B│  ┌┐    ┌┐    ┌┐\r\n     * └───┘   └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┘  └┘    └┘    └┘\r\n     * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐ ┌───┬───┬───┐ ┌───┬───┬───┬───┐\r\n     * │~ `│! 1│@ 2│# 3│$ 4│% 5│^ 6│& 7│* 8│( 9│) 0│_ -│+ =│ BacSp │ │Ins│Hom│PUp│ │N L│ / │ * │ - │\r\n     * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤ ├───┼───┼───┤ ├───┼───┼───┼───┤\r\n     * │ Tab │ Q │ W │ E │ R │ T │ Y │ U │ I │ O │ P │{ [│} ]│ | \\ │ │Del│End│PDn│ │ 7 │ 8 │ 9 │   │\r\n     * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤ └───┴───┴───┘ ├───┼───┼───┤ + │\r\n     * │ Caps │ A │ S │ D │ F │ G │ H │ J │ K │ L │: ;│\" \'│ Enter  │               │ 4 │ 5 │ 6 │   │\r\n     * ├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────────┤     ┌───┐     ├───┼───┼───┼───┤\r\n     * │ Shift  │ Z │ X │ C │ V │ B │ N │ M │< ,│> .│? /│  Shift   │     │ ↑ │     │ 1 │ 2 │ 3 │   │\r\n     * ├─────┬──┴─┬─┴──┬┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤ ┌───┼───┼───┐ ├───┴───┼───┤ E││\r\n     * │ Ctrl│    │Alt │         Space         │ Alt│    │    │Ctrl│ │ ← │ ↓ │ → │ │   0   │ . │←─┘│\r\n     * └─────┴────┴────┴───────────────────────┴────┴────┴────┴────┘ └───┴───┴───┘ └───────┴───┴───┘\r\n     */\r\n\r\n', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '2298', '404', '0');
INSERT INTO `typecho_contents` VALUES ('26', '给学生的一点个人中肯的十条面试建议', '26', '1572347820', '1616253657', '<!--markdown-->给学生的一点个人中肯的十条面试建议\r\n![面试.jpg][1]\r\n1.首先大家要明确的是,在面试的过程中遇到不会的问题纯属正常现象.既然是面试题,肯定也是公司精挑细选出来的题目,每道题都会针对性的考察一些你对Php的理解和掌握,从基础语法知识,到逻辑,算法,框架,数据库,优化,服务器等等.所以,在面试的过程中,遇到任何一个问题,无论是笔试,还是面试,都应该深思熟虑,认真思考,认真对待,认真作答.要尽可能详细的阐述你的答案,要有理有据,可以适当举例来解释清楚答案,并且让面试官知道你知道.\r\n\r\n2.心态一定要摆正,遇到会的问题可以认真回答,遇到不会的问题,认真回忆下,之前是否讲过,或者说之前老师是否提到过...如果有印象,或者印象不深刻,可以适当说点自己的理解和了解的知识,不妨告诉对方,这些东西原理都知道,只是在开发的工程中是由别人来完成的,毕竟开发都是分模块开发的,所以,这个东西是别人负责的,你没做,也属于正常现象.\r\n\r\n3.对于实在不会的,也不要东拼西凑,建议老实作答,不会就是不会,没搞过就是没搞过,也不要欺骗,应付,毕竟坐在你对面的可是专业人员,你糊弄不了他的.\r\n所以,不妨给人家留下一个谦虚,诚实的好印象,如果对方觉得你技术不强,可以培养,那即使你不会不影响的.\r\n\r\n4.面试感觉不好,很多答不上来,不代表就没有offer了,要根据实际情况对待,\r\n比如:公司只是急需用人的时候,当迫切需要人的时候,他明知你技术一般,也是很有可能把你招进去,破格录取的,毕竟,你实力在差,相比很多压根不懂程序的人来说还是还强的,毕竟再简单的功能,再简单的需求,也得需要有\"人\"来干的.所以,只要心态好,态度端正,谦虚,礼貌,跟人留个好印象,都是有机会的.\r\n\r\n5.面试找工作跟谈恋爱差不多,一是靠运气,二是靠努力.比如,你现在谈的对象,你是在什么机缘巧合下认识ta的?如果你在北京上的大学,是不是很有可能认识ta?如果你在上海上的大学,是不是就认识了另一个ta?也许,无论你在哪上大学你都会谈恋爱的.但是,你所在的地方不同,位置不同,那你的人生轨迹也就不同,认识的人自然也就不同,不变的是你一定会有对象,变化的是你的对象会另有其人.\r\n所以,找工作也是,要不急不躁,没有面试机会的时候,要尽可能的多投简历(毕竟PH面试机会不是很多),面试机会越多,自己的概率就越大,其实想开了,找工作也就没有那么难了,毕竟再多公司招聘,对我而言,我只需要拿到一个offer就好.\r\n毕竟,弱水三千只取一瓢饮嘛!一方面在等待面试的机会,一方面去网上找寻面试题,善于利用百度,百度可以解决百分之九十的问题.毕竟,他们遇到的问题,你也会遇到,你遇到的问题,今后的他们也会遇到.所以,在等待面试机会的时候不要不做任何准备,要时刻补充自己,时刻准备好.\r\n\r\n6.面试结束后,回到地方,要认真回忆每一道面试题,然后整理成文档,然后去百度,去尝试努力解决每一个问题,这样随着面试机会的增多,遇到的面试题整理的越多,这样,越往后面面试,通过的概率就越大,毕竟经验积累越来越丰富,而且,常见的面试题也是千篇一律的.不但要总结,整理自己的每一道面试题,而且要多跟同学互动,整理别人的面试题,也许人家现在遇到的面试题,你下个公司也会遇到,提前做好准备,面试就会一帆风顺了.有条件的话,大家建个面试讨论群,共同整理维护所遇到的每个面试题,大家都把自己的问题贡献出来,资源共享...这样每个人都可以受益.\r\n\r\n7.面试过程中,时刻准备面试题的同时,要多复习之前的讲课知识,不能遗忘,毕竟随着面试时间的推移,时间长了总会忘的,为了保证大家更快就业,所以,讲课的知识点覆盖面比较广,但是大家就业后,入职后,可以选择性的朝一个方向再去深入研究,发展.\r\n\r\n8.从来不打无准备的仗,同样,面试的话,提前查看面试公司官网,了解公司到底是做什么产品的,公司在招聘什么职位,有可能用到哪方面的知识,那就赶紧补充或者了解自己在这方面的知识,面试的时候往你要求职的岗位上去靠拢,让对方觉得你跟岗位比较吻合,这样通过的概率就比较大了,而不是,去了之后连公司名称都不知道,公司是做什么的也不知道,你要面试的职位用到什么技术也不知道,一点准备都不做,能面试通过吗?换位思考下,如果你是面试官,你会怎么想?面试被问到问题的时候,不妨设身处地,换位思考,想一想,如果你是面试官,你想听到什么样的答案,投其所好(谈恋爱的时候,给你喜欢的人买ta喜欢的东西,聊她感兴趣的话题,同样投其所好,ta自然愿意和你聊天,和你沟通了,想方设法的去找ta也感兴趣的东西,这不就是共同语言吗?)去回答,让他满意了,offer自然有了...\r\n\r\n9.如果不幸面试失败那也没有关系,心态不要受到任何影响,难得表白被拒一次,此生就不在谈恋爱了吗?你被拒绝,不代表你能力不行,也许是因为公司觉得你不太适合他们公司,好多同学会说,公司很好,很想进去,真的很想很想,但是人家不要你,你能怎么办呢?难得你喜欢的人,就一定也要喜欢你吗?你喜欢ta是你的权力,ta不喜欢你也是ta的权力,你可以表白,ta可以拒绝,对吧?所以,即使面试失利,再接再厉下个面试依然全力对待,用心对待,总会有公司因为你的技术,你的实力,你的态度录取你...\r\n\r\n10.如果条件允许的话,建议面试的时候手机录音,面试结束后不妨自己听听录音,揣摩下面试官到底想听到怎样的答案,自己到底怎么回答才能让他满意,不断反思总结.去网上找答案,然后求职同学,老师,都是可以的.但是,一定要养成自己主动解决问题的能力,因为老师不可能一直帮你,老师也忙,不可能第一时间及时回复的,所以还要靠自己,因为很多时候你反应的问题,老师只能给你提供思路,想法,具体还得你自己来实现,毕竟,老师也没有你那边的源码,也没有你那边的环境,如果环境不一致也是没办法解决的.也许解决方案已经告诉你了,可是你在编写代码的过程中,某个函数名写错了,字母写错了...然后说效果出不来,老师也不可能逐行检查你的代码是否有语法,拼写等错误...你们面试的公司种类繁多,也许做的某个新兴技术,也是老师之前没有接触过的...所以,这个时候老师也只能根据以往的经验来给你相应的建议.\r\n\r\n最后:总结一点,心态一定要摆正,不急不躁,不卑不亢,我是想找一份工作,同样我也是为公司创造价值的,所以.不要觉得你是在求别人施舍给你一份工作.双方都是平等的,是双向选择的.此处不留爷,自有留爷处.认真对待每一次面试,每一个机会.找工作,谈薪资,都是一个双方互相博弈的过程...要充满自信,浑身散发自信,但是不要自大,自负...希望各位同学能够认真阅读...如果能够帮到大家,我也就心满意足了.\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/11/4026454810.jpg', '0', '1', null, 'post', 'publish', null, '1', '1', '1', '1', '0', '1', '2270', '462', '0');
INSERT INTO `typecho_contents` VALUES ('37', '胡汉中国的定型期——清帝国(作为汉人,你不可不知)', '37', '1572504540', '1616253833', '<!--markdown-->中国人有中国人的形象，即便是元帝国，占领全中国，让南人为第四等，也没有让中国人的形象完全终止，汉家衣冠、文化仍然在有南人与汉人的地方保留，让民间保留了华夏复兴的可能性。而清帝国为了占中国之地以达长远，一方面在不自觉中被汉化，一方面却对中国人民进行了空前的胡化，把一个还部分保留了古典中国的血性、牺牲、诚信的明代中国人，改造成为清帝国的奴民。\r\n\r\n\r\n清帝国的胡化政策分三大部分：屠杀、剃发易服、修篡书藉，最终以胡变夏。以便让中国人忘记中国人的形象，忘记中国人应有的风骨。这对当代中国人的精神发生了直接的影响。\r\n下面将简述清帝国的这三大方面的政策及后果。\r\n\r\n一 屠杀\r\n\r\n　　清帝国利用中国内乱，从而征服中国并屠杀敢于反抗的中国人，从基因上灭绝中国人有血性的部分。以下表格，列举部分残酷的屠杀，足可证明这样的灾难对一个民族的从肉体与心灵上的影响，会达到怎么样的程度。\r\n\r\n　　表—1. 清军部分屠杀列举\r\n\r\n\r\n\r\n\r\n\r\n　　扬州大屠杀\r\n\r\n　　1645年四月二十五日（5月20日），清军攻占扬州后，进行了为期十天的大肆屠杀，后来由城内僧人收殓的尸体就超过了80万具。见幸存者王秀楚所著《扬州十日记》。\r\n\r\n\r\n\r\n　　昆山大屠杀\r\n\r\n　　1645年，昆山县人民起兵反清。七月初六，清军屠城，士民死难者达数万人。\r\n\r\n\r\n\r\n\r\n\r\n　　嘉定大屠杀\r\n\r\n　　1645年，在清军攻破嘉定后，清军三次对城中平民进行大屠杀。\r\n\r\n\r\n\r\n\r\n\r\n　　江阴大屠杀\r\n\r\n　　1645年，清帝国颁布剃发令后，江阴人民举行了反清起义。清廷先后调动24万军队攻城，江阴人民，守城八十一天，击毙清三王十八将，清军死伤过十万。守城者除53人外，全部壮烈牺牲。\r\n\r\n\r\n\r\n\r\n\r\n　　嘉兴大屠杀\r\n\r\n　　1645年，闰六月二十六日，嘉兴民众揭竿而起，二十六日城陷，按当时人口来推，可能约500，000余人遇难。\r\n\r\n\r\n\r\n\r\n\r\n　　四川大屠杀\r\n\r\n　　1646年至康熙初期，清帝国开始侵入四川，在四川各地进行大屠杀。顺治四年（1647年）多尔衮、孝庄采取了彻底屠杀的办法作为报复，公开发布告示，宣称：全城尽屠，或屠男而留女。直到顺治十六年（1659年），清军攻陷渝城（重庆）后，才算平复四川。清初时以“湖广填四川”来解决四川人口的缺口。\r\n\r\n\r\n\r\n\r\n\r\n　　常熟大屠杀\r\n\r\n　　1645年，8月到9月，清军先后占领苏州和南直隶常熟之后的纵兵焚烧杀掠。在常熟大屠杀中被屠杀的百姓无法计算，沿河沿岸都是人头。\r\n\r\n\r\n\r\n\r\n\r\n　　厦门屠杀\r\n\r\n　　顺治四年（1647年）清军攻克福建厦门和同安县，然后屠城。\r\n\r\n\r\n\r\n\r\n\r\n　　南昌大屠杀\r\n\r\n　　顺治五年（1648年）清军包围南昌。次年三月间，南昌城陷，清军屠城。八旗军把从南昌掠来的妇女分给各营，昼夜不停的轮奸。这些女性“除所杀及道死、水死、自经死，而在营者亦十余万。食牛豕皆沸汤微集而已。饱食湿卧，自愿在营而死者，亦十七八。而先至之兵已各私载卤获连轲而下，所掠男女一并斤卖。其初有不愿死者，望城破或胜，庶几生还；至是知见掠转卖，长与乡里辞也，莫不悲号动天，奋身决赴。浮尸蔽江，天为厉霾。”这些事情记载在《江变纪略》里，此书是清帝国政府查禁的重点，在乾隆四十四年被明令销毁，靠着手抄本流传下来。\r\n\r\n\r\n\r\n\r\n\r\n　　湘潭屠城\r\n\r\n　　顺治六年（1649年）正月二十一日清军攻入湖南湘潭和沅州（今芷江），南明督师何腾蛟被俘。清郑亲王济尔哈朗下令在湘潭屠城，湘潭城中百姓几乎全被杀光，城中不满百人。沅州也遭到纵兵杀掠。\r\n\r\n\r\n\r\n\r\n\r\n　　大同屠杀\r\n\r\n　　1649年，清军在大同屠杀后，全城只剩下5个重案犯。清帝国派来的大同知府，上书顺治帝，称既然没有了苦主，就可以释放这5个人了。这份奏折，至今保存在第一历史档案馆。\r\n\r\n\r\n\r\n\r\n\r\n　　广州屠杀\r\n\r\n　　1650年，清军攻广州，制造了庚寅之劫，甲申更姓，七年讨殛。何辜生民，再遭六极。血溅天街，蝼蚁聚食。饥鸟啄肠，飞上城北。北风牛溲，堆积髑髅。或如宝塔，或如山邱。五行共尽，无智无愚，无贵无贱，同为一区。可喜屠广州，孑遗无留；逸出城者，挤之海中。死难10万至70万人。\r\n\r\n\r\n\r\n\r\n\r\n　　意大利籍耶酥会士卫匡国（Martin Martini，1614～1661）在《鞑靼战纪》中记述：“大屠杀从11月24日一直进行到12月5日。他们不论男女老幼一律残酷地杀死，他们不说别的，只说：‘杀!杀死这些反叛的蛮子!”\r\n\r\n\r\n\r\n\r\n\r\n　　潮州和南雄大屠杀\r\n\r\n　　1653年，清军占领广东的潮州和南雄，清军之后进行下令屠杀，“纵兵屠掠，遗骸十余万”，“癸巳，郡城破，横尸遍野……收遗骸十余万，作普同塔于葫芦山”。揭阳县观音堂海德和尚等收尸聚焚于西湖山，将骨灰葬在西湖南岩。福建同安县屠城死难5万余人，梵天寺主持释无疑收尸合葬于寺东北一里之地，建亭“无祠亭”，墓碑上则刻“万善同归所”。南雄县民也遭到大肆屠杀，“大清平、靖二藩克雄城，民尽屠戮，十存二三。家家燕子巢空林，伏尸如山莽充斥……死者无头生被掳，有头还与无头伍。血泚焦土掩红颜，孤孩尚探娘怀乳”。\r\n\r\n\r\n\r\n\r\n\r\n　　清帝国不仅仅进行以上的屠杀，在控制全国后，为了封锁郑成功，下达禁海令，对沿海人民大肆屠杀，不愿意迁走的斩杀无赦，并乘机掠夺妇女、财物。　另外还有因为“剃发令”的原因进行的分散屠杀：“去秋新令：不剃发者以违制论斩。令发后，吏诇不剃发者至军门，朝至朝斩，夕至夕斩。”\r\n\r\n\r\n\r\n\r\n\r\n　　明末时期中国人口在8000万到一亿五千万之间，而到了康熙五十年（1711年）中国人丁数仅2464万余。也就是说这个时期中国的人口大量消失，除了明末的农民起义战争外，多半是死于清帝国征服战争所致的战乱、饥荒等原因。\r\n![4.jpg][4]\r\n\r\n\r\n\r\n二 剃发易服\r\n\r\n　　努尔哈赤建后金称汗后，强迫被占领区汉人和投降的汉人必须剃发，剃或不剃，成为一种政治性标志。汉人只要剃头，就免死收降，否则砍头。后金军占领辽阳后，当地汉民成千上万不愿剃头为虏，自投鸭绿江而死。\r\n\r\n\r\n\r\n\r\n\r\n　　入关后，清帝国碍于汉人的抵抗，对剃发并不严格，但是，一旦当清军征服顺利时，便露出穷凶极恶的面目。在北京的多尔衮得知南京已经平定，加上汉奸孙之獬上表相劝，即改变初衷，让礼部在全国范围内下达“剃发令”。\r\n\r\n\r\n\r\n\r\n\r\n　　一朝天子一朝臣。以家族宗法为源的中国人，或许面对这种外侵，可以假以天道循环进行自我安慰，但如果要把几千年的汉家发式与衣冠变成“猪尾巴”、小辫与马褂，这就是在对最后仅剩的人格尊严进行侮辱。而且，在明朝人心目中，以这种“夷狄”形象活着，必定失礼，死后一定有愧于祖先。因此，原本已经降附的地区纷纷反抗，整个中国大地陷入血雨腥风之中。\r\n\r\n\r\n\r\n　　以后世而论，许多史家认为，清帝国这一举措极为多余，让许多本来已经平复的地区重新举义，给自己制造了许多不必要的麻烦。但实际上，对于清帝国而言，为了长治久安，摧毁被征服民族的尊严是极为重要的关键因素。“剃发易服”就是这样一个无比狠毒但却极其有效的方法，以屠杀立威，在“留发不留头”的清洗中杀戮了大量的有反抗精神的人员，彻底摧毁汉人的抵抗基因，为制造奴民打下重要的基础，进而巩固了对汉族人民的统治。\r\n\r\n\r\n\r\n\r\n\r\n\r\n　　后来的历史表明，清帝国统治者的这一措施基本达到了预期效果。——汉人逐渐淡忘本民族服饰，习惯了清帝国的发式和服装。到辛亥革命推翻清帝国，号召民众剪去辫子时，仍然有许多人不愿意剪，可见“留头不留发，留发不留头”的“剃发易服”政策对汉族影响极深。\r\n\r\n\r\n\r\n　　三 篡改书藉与文字狱\r\n\r\n　　清代乾隆年间开“四库馆”，征天下藏书，修成七部《四库全书》， 首先收缴全国的书籍，从采进的所有版本中，查出的大批“禁书”，皆即行销毁。其他已经选作底本的书，继续保存在翰林院。\r\n\r\n\r\n\r\n\r\n\r\n　　清帝国修四库全书，可以收藏观看的存书不过3475部、79070卷；严禁收藏的禁书却多达6766部、93556卷；这些所谓禁书，只不过有些许文字问题，或者有勇武血性的部分，或者有记录前代胡族暴行的，或者与清帝国修改的史书有矛盾的地方。丧心病狂到极点，就连《天工开物》等这样开启民智的科技书籍也不能幸免。\r\n\r\n\r\n\r\n清帝国修改中国历史的工作是其执政策略的一部分。这是华夏中国的政治传统上绝不可想象的事情，中国的历史从来鼓励人民记载信史。文天祥《正气歌》列述正气12例，开篇第一例便是：“在齐太史简，在晋董狐笔。”——说的是齐国崔杼造反，太史做记录：“崔杼弑其君。”崔杼一怒之下，把太史杀了。\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n太史的弟弟接着写，崔杼照杀不误，太史的二弟仍然接着写。远在外地的南史氏听说崔杼连杀两位史官，也毫不畏惧地拿着简册赶往都城，准备等太史二弟死后接着写，直到崔杼让步，才返回故里。而董狐也是晋国太史，也以直笔写史而名传后世。可见，信史的精神是华夏中国的灵魂之一。\r\n\r\n\r\n\r\n\r\n\r\n　　清帝国前期把“文字狱”与篡改典藉相结合，把所有思念前朝或者疑为影射清廷的书籍文字，一律加以销毁，并对作者处以极刑，甚至诛连九族。就这样，清帝国通过对思想上的控制，把专制主义上升到一个新的高度，成功统治了中国267年。然而，清帝国这样一个邪恶的政权，却能让清末的部分汉人力图振兴它，并把它当作中国。——这就是清帝国对历史、文化以及民族精神的大屠戮的效果。更会有今天“有些人做着奴才，却还以为自己是主子”（鲁迅语）的现实。\r\n\r\n清帝国统治者并没有完全消灭汉文化，因为真正的文化是无法消灭的，相反他们找出了传统文化中最堕落的、最毁民气的那一部分，让中国人彻底的忘记华夏中国，把这个黑暗腐朽的现实当作这就是中国的本身，让中国人的精神彻底堕落，让中国人民丧失责任感与纪律性，去掉中国人的自尊和廉耻，更把胡汉中国彻底定格直到今天。——今天，满眼的清帝在屏幕上晃动，而汉人并不以之自耻，欺诈的、做假的社会风气让老人、孩童无以幸免，也正是这267年奴化的结果。\r\n\r\n\r\n\r\n\r\n\r\n更为悲剧的是——清帝国成功的长久统治，让投机者有根据的认为：一个没有诚信的统治也是可以久远的。——这才是对人心最大损害，使后代更难建设出一个诚信的华夏中国来。\r\n', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '2695', '515', '0');
INSERT INTO `typecho_contents` VALUES ('39', 'O1CN01hulg8t2B2kKNbXGhU_!!2168468281 (1).jpg', 'O1CN01hulg8t2B2kKNbXGhU_-2168468281-1-jpg', '1573115753', '1573115753', 'a:5:{s:4:\"name\";s:44:\"O1CN01hulg8t2B2kKNbXGhU_!!2168468281 (1).jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/11/3271703008.jpg\";s:4:\"size\";i:718472;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '41', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('41', '动态', 'mylife', '1573610580', '1577239246', '<!--markdown-->', '0', '1', 'mylife.php', 'page', 'publish', null, '4', '1', '1', '1', '0', '1', '2520', '455', '0');
INSERT INTO `typecho_contents` VALUES ('2', '归档', '40', '1573129500', '1573610367', '<!--markdown-->', '0', '1', 'archives.php', 'page', 'publish', null, '0', '1', '1', '1', '0', '1', '1427', '512', '0');
INSERT INTO `typecho_contents` VALUES ('43', '友链', 'link', '1573611420', '1573612704', '<!--markdown-->欢迎申请友情链接，只要是正规站常更新即可\r\n申请首页链接需符合以下几点要求：\r\n1、本站优先招原创内容的博客或网站，站点主题内容是互联网资讯、技术、资源、设计等等；\r\n\r\n2、百度有正常收录，百度近期快照，不含有违法国家法律内容的合法网站。\r\n\r\n3、如果您的站点内容少之又少，且长期不更新，申请链接不予受理!\r\n\r\n留言后，麻烦站长们先在自己的站点添加上本站友链，我会尽快添加你的友链并通知你。\r\n\r\n本站名称：旭辉博客\r\n本站地址：https://www.phpmaster.cn', '0', '1', null, 'page', 'publish', null, '3', '1', '1', '1', '0', '1', '2502', '572', '0');
INSERT INTO `typecho_contents` VALUES ('44', '面试.jpg', '面试-jpg', '1573616402', '1573616402', 'a:5:{s:4:\"name\";s:10:\"面试.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/11/4026454810.jpg\";s:4:\"size\";i:13716;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '26', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('45', '38dbb6fd5266d016c248a477942bd40735fa351f.jpg', '38dbb6fd5266d016c248a477942bd40735fa351f-jpg', '1573710086', '1573710086', 'a:5:{s:4:\"name\";s:44:\"38dbb6fd5266d016c248a477942bd40735fa351f.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/11/3173200224.jpg\";s:4:\"size\";i:301239;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '46', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('46', '关于汉服 我有话说', '46', '1573710060', '1616253616', '<!--markdown-->\r\n][1]如果你不小心看到了这篇文章，那我很开心，这是你的幸运，也是我的幸运。毕竟，我的一些文字能被别人看到，我很欣慰！\r\n\r\n如果你不小心把文章认真看完，那我表示感谢，谢谢您肯花几分钟时间来看看到我写的东西，也许可以跟你有思想碰撞，产生共鸣。\r\n\r\n如果因为你看了此文，哪怕或多或少学到点东西，受到一点影响，或者意识到一些问题，获益匪浅的话，那我很欣慰，希望我的观点可以被接纳，被讨论。\r\n\r\n我是一个凡事都是认真思考，深思熟虑，最终才会下决定去做的人，并非一时头脑发热，一时冲动才去做的，比如汉服。 其实，想认认真真的写一些文字，表达一下自己的观点，这种想法由来已久，但一直迟迟并未动笔，主要原因还是因为我懒，懒的写，懒得计较， 懒得跟别人争论，争的面红耳赤，最后还是谁也没能说服谁。我觉得这样做会很无聊。但是，当我看到所谓的“汉服圈”戾气越来越来越重， 越来越乌烟瘴气的时候，我觉得，是时候该站出来了，发出自己的声音。也许我的观点你并不能接纳，也许我的观点你并不认同，但请理性对待 毕竟码字的时候我是认真的理性的，一字一句的边思考边写的。\r\n\r\n首先先介绍下我自己的情况吧！其实，大学的时候就已经在电视上看过关于汉服的介绍，但一直并未放在心上，也许是因为不了解吧，并没有感觉到 汉服与每个人息息相关。但是，自从接触到古风音乐之后，慢慢接触到汉服。直到从去年开始就汉服入坑了，其实早就可以入坑了，只是我这个 人性格的问题，一直在思考，在反思，在了解汉服，了解汉服的历史，等等一切跟汉服相关的东西，因为我觉得，在没有把一件事搞懂之前我不会轻易做决定。直到去年，开始决定入坑。入坑之后，开始对汉服着迷，痴迷，着魔。抖音上关注的全是穿汉服的小姐姐，因为真心觉得穿汉服的小姐姐真的是太美了，飘飘欲仙的样子。 尤其是刚玩抖音的时候还闹了个笑话，因为我痴迷与汉服，关注的全是汉服小姐姐，以至于我每次打开抖音里面全是汉服小姐姐。我突然感觉到 现在的汉服已经发展到尽人皆知的地步，全民皆汉服。后来才知道，原来抖音是根据用户习惯主动推送的，你喜欢什么就推送什么。那个时候真的是 天真的以为全民汉服。自从接触汉服之后，就想去了解汉服，认识汉服，当然让我涨了很多知识。不得不说，在宣传，推广汉服方面抖音做出了巨大的 贡献，如果不是抖音，也许汉服发展的也不会这么快。个人觉得，因为抖音，因为汉服商家才让汉服发展如此之迅猛，也就是在这短短的两年间，已经 发展到几乎尽人皆知的程度。虽然也有部分人感觉，穿汉服是哗众取宠，穿越来的，唱戏的，和服，等等声音也不绝于耳。但是，随着时间的 推移，越来越多的人开始了解它，接纳它，为此真的感到很欣慰。\r\n\r\n刚接触汉服的时候，把中国的古代历史扒出来看了一遍，从三皇五帝看到诸子百家看到各个朝代，看到五胡乱华，崖山之战，看到胡服骑射，看到今天。 其实，之前的我是不喜欢历史的，正是因为接触汉服，又爱上了历史。当时的心情是激动的，欢呼雀跃的，就好像一个孩子拿到了心爱的玩具兴奋的 不得了，恨不得想告诉全世界我得到心爱的东西了，也恨不得想告诉身边的每个人，作为一名汉人，民族自豪感荣誉感瞬间爆棚。感觉，此生无悔入 华夏，来世还在种花家。心情是激动的，激情澎湃的。身为中国人，身为汉人，骄傲，自豪。\r\n\r\n汉服本来是没有圈的，可是现在所谓的汉服圈却问题众多，矛盾众多，大家各成一派没有统一的标准，各种思想，各种问题，比如皇汉，比如制式， 比如搭配，比如山正，比如如何宣传发扬，等等问题众多。汉服还没有做到被普遍大众接受的时候，汉服同袍内部的各种撕逼就已经开始。 每天群内圈内甚至走在大街上就有同袍走上前来跟你撕逼，我真的是受够了这种情况。一直喊着“华夏复兴，衣冠先行，始于衣冠，达于博远”难道你 真的理解这16个字的意思吗？还是说你自己被有心之人当了枪使你都不知道？风向到底被谁控制，又被谁左右？你是否静下心想好好思考下，到底什么 是该我们坚守的，又有什么是该我们放弃的。总有人说，政府应该介入汉服的推广和宣传，有了政府的支持，肯定会很快推广下来的。但是，说这句话 的时候，你是否动脑子想过，政府不去阻止就应该是在支持了啊。当然，你有可能会说，废话，政府怎么会去阻止呢，毕竟我们都是汉人，汉服也是 老祖宗留下来的东西。但是，你想过没有，了解汉服，就意味着要扒开历史，扒开历史你就会看到中国历史朝代上唯有元，清不是汉人的天下，你就 会知道崖山之战，会了解崖山之后再无中华的说法，会了解蒙古当时屠杀了多少汉人，会了解清朝屠杀了多少汉人，会知道清朝的十从十不从，会了解 清朝的留发不留头。会对这两个民族深恶痛绝，会增加汉人之间的凝聚力，但是也会离间我们与少数民族的关系。现在的新中国是老一辈为我们打下的 江山，连他们都在喊着民族大团结，你一个人凭什么口口声声的抵制其他民族？国家都怕引起民族分裂的事情，小心翼翼的将中国汉服日改为中国华服日 就是怕被有心之人拿来做文章，说政府来分裂民族。所以，改成华服，即表示中华名族的服饰，在这里我觉得政府还是很机智的，‘华’，‘汉’一字之差 就把我们56个民族给团结起来了。至于历史问题，作为当代人就不要过于纠结了，毕竟现在是提倡民族团结的时候，如果一味的跟政府唱反调，颇为不妥！\r\n\r\n接下来，我将对时下流行的几个问题依次给出自己的观点，欢迎各位同袍批评指正！！！\r\n##### 1.关于山正\r\n相信很多人都经历，或者遭遇过这个问题 汉服山正的问题一直争论不休，争论山正没有意义。\r\n**汉服到底分不分山正？**\r\n汉服山正之争由来已久!原则上汉服是不分山正的,真要区分也是商家.\r\n要相信垄断,一家独大并不能让汉服更好的传承,推广,发展,要百家争鸣,百花齐放,有竞争才能更好的为人民服务.当然,我们还是要大力提倡大家买正,可以分山,但不要喷山.穿正的也不要有优越感,毕竟每个人的实际情况不同,经济能力不同,都是可以理解的.对于\r\n买山的萌新也不要去攻击,以免寒了一个袍子热爱汉服的心,每个袍子的心灵都值得我们用心呵护.作为一个成年人,要理智,要成熟.要有主见,不要轻易被别人左右要有自己的分析能力.只要穿上汉服,无论山正,大家都是同袍.我宁愿满大家都是穿山汉服的同袍,也不愿无人穿汉服出行.衣服只是载体,通过宣传汉服从而达到恢复汉文化,汉礼仪,汉传统的目的!很多人因为喜欢上了汉服,从而接触到了很多古典文化,琴棋书画,各种手工艺!\r\n首先，所谓山是没有固定标准的。看了一圈下来有说一模一样才算山的，有说借鉴样式就算山的，有说借鉴图样就算山的。拜托你们先给出个统一标准呀。\r\n第二，打击山寨你们要去打击店铺啊，知山买山在我国不违法。既然不违法你们抨击消费者的出发点在哪？ 以道德要求别人这本身就是不道德的行为。道德是用来律己的。\r\n第三，所谓正版店在面临山寨的时候毫无还手之力，你们有没有想过是正版店的所谓设计版权本身就有问题？靠消费者来维护店铺的权利？ 笑话，自己不去维权要靠别人？说版权得不到保护？申请不到版权那只能说明你们所谓的设计根本就不算设计好么。如果申请得到版权，直接走法律程序， 搞这么麻烦干嘛？如果你们申请不到版权，还不允许别人做，别人买，这就是垄断，还不是靠商业手段的垄断，还靠的是阻止别人买更实惠的商品 的卑劣下三滥手段。\r\n第四，很多人说怕所谓正版店被山寨店干掉就买不到好看的裙子了，更是无稽之谈。从来都不会有市场有利润而没有商人的情况发生好么？ 一个所谓正店没了，只要你们肯花钱，分分钟有另外的店来赚你们的钱。你们想供养某个设计师，没问题啊，你们花钱买就是了， 但是你们凭什么强迫别人一起多花钱？\r\n\r\n最后，消费者没违法，爱咋样咋样，你们站在道德的高地上不冷么？别说盗版软件盗版游戏盗版耐克，人家是真真有版权的，你们的有么？ 有的话请店铺直接走法律程序。没有的话，请不要拉着别人为你们自己心里yy出来的版权买单。 其实我很不明白汉服为什么要学lo圈去宣扬山正问题。我一直认为山正根本不应该现在被提出来，然后分成两帮打来打去。山寨怎么了？ 别说什么要支持原创卖家这是市场调节，逼着原创卖家降低价格，提升社会生产效率。而不是汉服价格一直居高不下，所有东西都要经历这个。 同一件T恤还有很多家制作呢，有料子差一点的，有料子好一点的。lo不是汉服.是爱好者才会去穿，而汉服却是中华千万年流传下来的文化遗产。 是应当被宣传，应当让世人认可的。很多萌新确实挺喜欢汉服的，想让汉服普及出去，但是首先自己就不敢去买。 为什么？怕挨骂。有多少人是这样的一个想法呢？我不知道。有多少人被“汉服警察”吓得不敢入圈呢？我不知道。 而且如果汉服想要从奇装异服的高台上下来，首先就要降低价格，不以暴利为手段。所以我觉得根治这些问题需要国家手段来建立基础。 制造一些大家都可以穿，都可以尝试的真正平价（不是相对平价）的汉服厂家或者慢慢经历市场调节，使价格降低，质量提升。\r\n山和正之间的差距是谁说了算的？店家？因为山会影响他的销量以及拉低行业价格？还有就是，为什么那么多关于汉服圈子的事情，都是风评不好的？什么汉服警察，看到人家穿得不对上去撕人家衣服？即便发生的是极少数的例子，但这样只能让别人去反感汉服，厌恶汉服。为什么汉服圈子里大多都是15-25岁的女孩子呢？因为她们大部分还不够成熟啊，容易被舆论风向影响啊，口口声声复兴汉服却做的是断送汉服发展之路，像极了某些明星的脑残粉。\r\n2.关于汉服为什么发展不起来\r\n相信很多网友经历过这样的情况，自己高高兴兴的买了一套汉服，想要去秀一下，结果遇到了别人的指责， 你这个不是正规的汉服，正规的汉服不是这样的，是什么什么样的。 很多人刚进入汉服圈，还是萌新，结果呢，迎接他们的不是欢迎，而是横挑鼻子竖挑眼的指责。\r\n一位网友就表示，自己曾经去实体店买衣服，看中了汉服，结果进去试衣服的时候，女服务员就问这位网友，是否了解汉服，这位网友不是很懂， 结果服务员直接翻了一个白眼，说出了不了解汉服就别试了之类的话。然后还和同事吐槽说现在什么人都敢来买汉服了， 不了解汉文化还来买汉服哪来的勇气（非常不屑的口气）？这位网友也是气的不行。偏偏这种情况，并不少见，甚至成了一种风气， 一种潜规则（优越感，嘲讽新人，排外）！ 正是这种优越感，毁掉了汉服，实际上我非常奇怪，为什么穿衣服都能穿出优越感。而且衣服哪来的什么正统之分？ 很多人穿衣服都只是看中了舒适度以及美观程度。如果真要说正统，那么什么算正统？ 是最原始的汉服算正统？还是改良后的算正统？或者是末期算正统？ 每个人看待一件事情的角度不同，认为具有优越感的人，都只是一群可怜虫罢了。 首先他们并不是真的想要发扬汉服，仅仅只是为了满足自己的虚荣，与众不同。 再一个，这些人属于被商家卖掉的人，不过他们是心甘情愿被卖掉的。 为什么这么说，汉服本身并不值钱，商家想要加价（商品与价值不对等，所谓不值钱）就必须要打其它主意。那么宣扬某某是正统，某某材料是正统， 也就不足为奇了。一件商品，只有大家不知道其价格的时候，它的利润才最大，一旦人人都了解，那么没什么利润。 而那些具有优越感的人，所需要的，恰恰是具有那种我知道，你不知道，我有，你没有的心态的人，他们需要高的价格来体现优越，与众不同。\r\n3.大量汉服老人退群退圈\r\n汉服圈内矛盾不断，导致喜欢汉服的人，汉服圈内的人，都在不断的流失，那些想要穿汉服的人，在左右观望了一阵后，也默默地选择了退出。 有这样一位网友，分享出了自己的亲身经历，表示已经退圈了，下面来带大家看一看。 我是最早一批研究和呼吁复兴汉服的，当初汉服吧的吧主溪山琴况，是一位极其热忱地致力于汉服复兴工作的谦谦君子， 我是亲历他因为殚精竭虑而心脏病突发去世的。后来为什么离开了汉服圈，因为什么东西一跟传统挂钩，就会引来一群僵尸企图把一些糟粕死灰复燃。 本来我们复兴汉服是因为它美丽，而且是我们的民族服装，扔掉太可惜了。可后来来了太多头脑僵化冥顽不灵的死板卫道士。 他们第一想借汉服恢复儒家封建等级压迫那一套。就是几千年来儒生的那种嘴脸，说白了就是顺我者生（谈不上昌），逆我者亡。而且充满了民族沙文主义，只尊汉族，对其他民族不能平等相待。 第二，他们头脑僵化，墨守陈规。我们认为汉服是活的，是随着时代发展的，所以每个朝代都有每个朝代不同的样式。那么发展到现在，应该结合现代人的生活习惯和审美发展出现代的样式。比如很多的仙侠剧里的服装就很美，但是又不完全符合古代形制。汉服必须百花齐放才能振兴。必须尊重市场规律，满足客户要求。 可他们不，必须是古代出现过的样式才叫“正宗”，而他们头脑里的这些样式只限于最表面的一些资料，就是说白了就是必须符合他们的要求的才叫正宗，否则就会群起而攻之。很多喜欢汉服的一些非专业人士，本来开开心心地穿了一套很漂亮的汉服，但是被他们攻击得体无完肤。意思人家数典忘祖，污染了汉服文化之类… 反正，就像是在力挺儒家的那些头脑死板就会人云亦云的人一样，汉服圈也一样存在这些人。就是只有祖宗说的才是对的，只有祖宗用过的才能用。不许人有自己的独立见解和创新。而且不懂得界限，总是以道德为由肆意干涉他人自己的事情。所以我当然滚了，这么乌烟瘴气的圈子我待不下去。我宁愿汉服不复兴，也不能让吃人的社会又回来。\r\n4.毁掉汉服的，恰恰是那些喊着复兴汉服的人，断送了汉服的未来\r\n这是一块没有人进入的市场，先进来的人能拿到一笔怎样的财富？穿着汉服拍着照的女孩子们不会知道的，永远都不知道她们一边省吃俭用还分期供养着商家，一边还免费的给他们打广告并沾沾自喜的时候，商家在偷笑着说这钱可真好赚。大学生兼职发传单都80-120一天呢，她们365*24全年无休，口口声声文艺复兴自愿用爱发热。\r\n别的少数民族都有自己的传统服饰，那身为汉族的为什么我们不去穿汉服？我们中国自古以来一直深受儒家思想影响，儒家里面有个叫董仲舒的人，他提出了罢黜百家独尊儒术的思想，这个高中学过的。其中有个思想是“兼容”，即对我好的我就学过来，这样才保证了儒学成为正统主流思想传承下来，因此我们中国人大多都是实用主义。 打个比方说：穿中山装比穿汉服好就穿中山装，穿普通衣服比穿中山装好就穿普通衣服。\r\n而其他少数民族以前是因为偏远封闭，现自改革开放以来，除非真的是深山老林里的山区，近一点的少数民族区域也很少有人穿民族服饰了，除非特殊节日以及景点工作人员。我认为衣服演化成现在这个样子并且被全世界的人接受，应该很好说明了这样是最舒适实用的。\r\n那些原教旨主义者，一点点不接受改良的，就别穿汉服了。因为真正意义上的汉服已经不存在了，汉服在朝代更改之间也一次次被改良。唐朝衣服以华丽为美，因为唐朝思想开放，宋朝衣服以保守朴素为主，因为思想主张不一样。元朝是成吉思汗的天下，他们都是外来的，穿的更不能叫“汉服”了！明朝的服饰是参考前朝几代服饰特点做出来的，应该算是“不伦不类”吧，难入正统之眼。\r\n哦，对了，我上面说的那些衣服都是官老爷以及土豪乡绅们才能穿的衣服，贫民们有件勉强遮体的衣服已经很满足了。还有如果复原出“真正的汉服”，就是古人穿在身上的那种。相信我，你们是不愿意去穿它的。所以，你们要复兴的“汉服”到底是哪个朝代的衣服？圈子里定下“汉服”的形式以及其他东西到底是以为什么来评判？\r\n5.关于汉服复兴\r\n第一，部分汉服运动的支持者试图篡改中国服饰的发展史混淆人们对于“传统服饰”“汉族服饰”“古装”的概念，一上来作为新概念便想一家独大让所有人都穿，将其他现行传统服饰都当作满清遗秽抵制污蔑，通过捧一踩一来想让汉服上位。\r\n\r\n第二，不适合现代生活且产业链不成熟，不是去迁就人们的需要而是要求人去迁就一件衣服，古代的服制在今天很多都已不再适用。而店家做不好维权就煽动购买者去当枪使，其出产的衣服也并非是物美价廉，价格虚高的现象较为明显。\r\n\r\n第三，因为圈子重叠，有些汉服穿着者的行为带着从lo圈娃圈等其他亚文化圈来的“小家子气”，像是山正的纠纷，形制的纠纷，这些本是为了维护自己的小圈子、维持自己独特性的行为，放在lo圈娃圈这样注定是亚文化的圈子里虽然会有人诟病，但是也是无可厚非的，但是汉服运动的目的难道不是想让汉服被大众所接受所穿着吗？这样的排外行为对于受众的扩大是很不利的。\r\n\r\n第四，汉服身上的政治色彩、民族主义色彩都是难以规避的，因为现代的汉服摹自古代中国的服饰，但中国是个多民族国家，中华文化自身融合了很多民族的特点，若是以此创建“汉服”这个新概念，有将中华文化归为汉族文化的嫌疑，且如今的汉服容易与一些封建糟粕挂钩，像是三从四德之类的。\r\n\r\n第五，中国古代服饰体系非常复杂，其礼制和配饰妆容都不可忽视，僭越只是其中一个小问题。如果像现在的汉服圈那样，没有一个认定的朝代，各个朝代都承认都穿，冬天穿明制袄裙，夏天穿唐制襦裙，要么是承担理论了解和搭配分类的超长时间和他人的所永远不能了解接受，要么就是自己成了“四不像”和“双标”的典范。\r\n第六，没有明确的分类，礼服当常服穿，就算是古代的皇帝也不会穿着一身金光闪闪的大龙袍一个人扒拉两口午饭然后跟后院儿里溜达散步，这不符合礼制也不方便，而且对于现代人来说还多了一个问题——钱，一身汉服再便宜也得大几百吧，贵的上千很容易，汉服爱好者又大多是年轻人，没啥存款，出门逛个街处处提心吊胆，大家看了只知道这套衣服多么“花瓶”。\r\n\r\n6.汉服早已不是衣服，被圈内人当成了虎皮\r\n我只能说分析一下，现在大部分穿汉服的心理大多都是既看不上洋文化又想特立独行，于是想到了用传统文化来当幌子，打着复兴文化的目的，形成一个个大大小小 的圈子。注意我说的是现在。这些人心理想法是，如果穿Lolita，和服等其他服装，会被家长老师路人指责为奇装异服，但是如果是汉服，自己就可以理直气壮的说 这是传统文化，就可以反驳他们了。\r\n至于什么“华夏复兴，衣冠先行，始于衣冠，达于博远”，这是个什么东西?别影响我美美哒，拍抖音就完事了。如果说之前的汉服爱好者们没有圈子的划分，默默为 汉服的复兴奉献，科普，为新人融入圈子尽力的话。现在的汉服爱好者们就根本不配被称之为“同袍”，普通人稍有不解就恶语相向，指责对方不懂传统文化，然后带 着优越感扬长而去，陌生人的好奇的眼光都被曲解为恶心，歧视。\r\n对于爱好者更是变本加厉，新人入坑永远都是以高高在上的姿态，一板一眼的诉说着“圈子”的规矩，对新人的文化意识无限放任，反而在山正上撕的你死我活。而这 个“圈子”里那些认真做科普，给萌新普及知识的人在“圈子”里几乎没有发言权，因为他们也没时间和所谓“大佬”争发言权。传承怎会是捧起古老的碎片，将先哲锁入 神坛。\r\n有的人虽然不知山买山，但是却知法犯法。你问我为什么复兴不起来，难道不会看看身边的人吗？我本以为撕山正，挂人喷人的人很少，起码在这个群体里不占大多 数，但是自从我加了几个不同地区的群和贴吧之后，我发现这不是少部分的问题，一个一千多人的大群，有超过500个潜水不说话，剩下的几乎都是撕山正，或者今天在xx地方被人看了，那些人是***（友善），偶尔有一两个人出来说公道话却总是那些老面孔。对，我把潜水的人算作是汉服圈里的好人。所以看看周围的人怎么样你就知道为什么复兴不起来了不是吗。小众文化就应该默默发展，而不是大张旗鼓打着传统文化的旗号，以为自己穿张皮就是代表华夏文化了，我只能说看看那些学古琴古筝琵琶二胡的人，再看看学书法泼墨中国画的，再看看学儒道墨兵法等百家思想的。您配吗？？？作为一种服饰，穿就完事了，如果我现在觉得好看我继续穿下去，如果我哪天不想穿了也只是因为审美变了。有的人把它当服饰，有的人把它当虎皮罢了。\r\n\r\n总结：\r\n我们都是因为汉服之美才入的坑，所以，各位同袍们就不要因为各种问题相互撕逼了，现在当务之急是大力宣传，推广，发展汉服，先让汉服日常起来 等到全民汉服的时候，再去讨论山正，再去讨论形制，再去讨论发扬汉服，恢复礼仪，恢复传统文化。 不能一味的说，穿汉服就要穿某个朝代某个时期的汉服，因为当时的社会不像如今有这么多行业所以，衣服形制，款式难免较少。 \r\n\r\n汉服也是分为礼服和常服的，只有在盛大节日庆典的时候人们才会穿礼服，平时都是以常服为主。所以，如果想要日常，还是以常服为主吧。 至于是穿春秋，汉制，唐制，明制，全凭个人爱好，自己喜欢就好，只要不危害到这个社会，喷子勿喷。假如没有元朝，清朝，汉服发展到今天肯定也有很大的改良，毕竟要迎合当今时代的发展，迎合各行各业的需要。所以，各位喷子也不要去喷什么 改良款，汉元素的问题了！当时的衣服，体现了身份，体现了阶级，所以，公子有公子的服装， 小姐有小姐的服装，农民有农民的服装，书生有书生的服装 服装就是身份的象征。恢复传统文化也是要迎合时代发展不是一味的全部恢复，要吸取精华，去其糟粕。\r\n我相信你要买个洗脸盆应该是不会考虑山正的问题了吧，毕竟那么多品牌的脸盆，谁为山，谁为正，我只要物美价廉结实耐用就好。你能说中国的航母就 没有借鉴国外的技术了吗？你能说中国的航母是山的，你敢说吗？汽车厂商也在相互借鉴呢！所以，与其天天花时间在这里讨论山正，还不如多花时间多读读历史。了解下中国朝代的发展史呢！相信很多人买的衣服都叫不上牌子吧！如果大家都去买品牌了，地摊货不就早完蛋了吗？那我们为什么还要在淘宝上买那么多不是品牌的衣服呢？还不是因为穷吗？相信很多萌新刚刚入坑，什么都不懂的情况下，积极性被你一打击反而丧失对汉服的兴趣了。正品品牌不是我们不想买，关键不是 大家都那么有钱呢！！！穷人们买的衣服大多都不是牌子，难道这些穷人买不起牌子就不能穿衣服了吗？不要动不动就把汉服上升到国家大义，民族大义，你能代表中国吗？谁又赋予了你的权力？\r\n遇到不懂的同袍的时候，我们要耐心讲解汉服-中华民族的传统服饰，汉人的传统服饰，老祖宗穿的衣服，只不过是因为朝代的发展导致断代了，所以不知道 也属于正常，切不可因为对方不了解就对对方冷嘲热讽，这样很粗鲁也很幼稚。 遇到“汉服警察”的时候对于出言不逊，不尊重人的同袍，要怼回去，毕竟他没有资格来教育你。。。我花的钱，穿什么衣服，我乐意就好，管你什么事，吃你家饭了，还是花你钱了？碍着你什么事了？\r\n\r\n所以,希望\"华夏复兴,衣冠先行,始于衣冠,达于博远\"不仅仅是一句口号,一句空话.要真正理解其中的含义!通过汉服的复兴从而带动,汉文化,汉礼仪,汉传统的复兴,而汉服也不能单单成为商家敛财的手段.汉服出行,一直都是备受争议的话题,有人欣赏,就有人排斥.要求同存异,相互理解.你有多喜欢,就有人有多讨厌.所以,对于喜欢汉服的人,要尊重,要支持,对于不喜欢汉服的人,要理解,要包容.现在阶段正是大力弘扬,宣传汉服的时候,切莫内斗,伤了和气.泱泱华夏,礼仪之邦,仁义满怀,大爱无疆!\r\n\r\n再次感谢能够坚持到现在把文章看完的人......谢谢你\r\n2019-11-14   初心\r\n\r\n\r\n  [1]: http://www.zhangxuhui.com/usr/uploads/2019/11/3173200224.jpg', '0', '1', null, 'post', 'publish', null, '10', '1', '1', '1', '0', '1', '6848', '1126', '0');
INSERT INTO `typecho_contents` VALUES ('59', 'CCTV第一次提到了汉服断代的原因！', '59', '1573743480', '1616253861', '<!--markdown-->\r\nCCTV法制频道\r\n第一次提到了汉服断代的原因\r\n历史告诉我们的\r\n是正视它 而不是回避\r\n\r\n大国之殇：汉服断代简史\r\n\r\n第一节 伴随着满清入关而来就是剃发易服令\r\n\r\n西元1644年(农历甲申年)，这是中国历史上“天崩地裂”的一年，这年3月，李自成北上攻取燕京，崇祯帝自缢殉国。李自成进北京，派人招抚驻扎在山海关的原明宁远总兵吴三桂。吴三桂经过考虑，决定归顺新朝，并回京朝见“新君”李自成，在回京途中，听闻家产被抄，爱妾被掳，顿改初衷，回师山海关，占领关城。李自成闻讯，决定征剿吴三桂，21日，双方激战山海关，22日晨，吴三桂情势危急，带随从冲出重围，至关外向驻扎在外，觊觎已久的清多尔衮部剃发称臣，归降清军，双方合兵。26日，李自成败退回北京，旋即西撤，清军入关，“定鼎燕京”。\r\n\r\n早在满清入关之前，辽东汉民早已深受剃头之荼。满洲在明代被称为“建州女真”“海西女真”等部，剃发是女真人的风俗习惯之一，这也是从其先世，女真金人那里沿袭下来的（历史上，剃发垂辫这一习俗，存在于多数北方游牧民族之中，中原汉人因之泛称其为“索虏”）。同时，北方游牧、渔猎民族多属马背民族，为征战、抢掠方便其间，其服饰多以紧身窄袖为特点，亦不戴冠，与中原华夏民族（汉族）的宽衣大袖、束发戴冠大不相同，17世纪初，随着满洲的兴起和扩张，这一特定的风俗习惯转而变成民族斗争之间征服与反征服、奴役与反奴役的政治问题。努尔哈赤于明万历四十四年（1616年）称后金汗，并攻掠明地，开始以剃发作为降服满洲的标志。万历四十六年，（1618年）后金袭取抚顺，“被掳军丁八百余人，又尽髡为夷。”（《剿奴议措》）。天启元年，（1621年）后金攻取辽沈，“驱辽民聚城北，奴家聚城南，谴三骑持赤帜，传令自髡剃不杀。”（《明熹宗实录》卷三）\r\n\r\n后金的剃发政策，引起汉人的强烈不满，以金洲、复洲、海洲、盖洲南四卫，镇江（丹东）等地最为激烈，“坚不受命，有剃头者，群击杀之。”（《三朝辽事实录》卷四）镇江（丹东）人民拒不剃头，还杀了后金派去的官吏，努尔哈赤闻讯，派兵进行残酷镇压，不剃发者悉被杀害，又抢掠妇女千余人，据朝鲜史料载“时奴贼既得辽阳，辽东八站军民不乐从胡者，多至江边…… 其后，贼大至，义民不肯剃头者，皆投鸭水（鸭绿江）以死。”（《李朝实录》光海君十三年五月）。\r\n\r\n明崇祯九年（清崇德元年1636年），皇太极称帝，改后金为清，继续推行剃发易服政策，明令公布“凡汉人官民男女，穿戴要全照满洲式样，男人不许穿大领大袖，女人不许梳头缠足。”（《清太宗实录稿本》卷十四）三年，（1638年）又下令：“有效他国（指明朝）衣冠，及令妇人束发裹足者，俱加重罪。”（《东华录》崇德三）。皇太极的目的，是防止女真人受到汉人风化的熏染，“服汉人衣冠，尽忘本国语言”，（《清太宗实录》卷三四崇德二年四月丁酉）危及满洲民族政权的长远存在，为此，皇太极反复告戒满洲贵族，应恪受满洲衣冠和善于骑射的风俗习惯云云，还多次下“上谕”强调这一点：\r\n\r\n“当熙宗及完颜亮时，尽废（金）太祖、太宗旧制，盘乐无度。世宗即位，恐子孙效法汉人，谕以无忘祖法，练习骑射。后世一不遵守，以讫于亡。我国娴骑射，以战则克，以攻则取。往者巴克什· 达海等屡劝朕易满洲衣服以从汉制。朕惟宽衣博鮹，必废骑射，当朕之身，岂有变更。恐后世子孙忘之，废骑射而效汉人，滋足虑焉。尔等谨识之。”（《清史稿太宗本纪二》十一月戊申）\r\n\r\n“昔金熙宗循汉俗，服汉衣冠，尽忘本国言语，（金）太祖、太宗之业遂衰。夫弓矢，我之长技，今不亲骑射，惟耽宴乐，则武备浸弛。朕每出猎，冀不忘骑射，勤练士卒。诸王贝勒务转相告诫，使后世无变祖宗之制。” （《清史稿 太宗本纪二》 夏四月\r\n\r\n皇太极认为，女真金朝的灭亡是因为改穿了汉人的宽衣大袖，继尔废弃骑射，从马上下来，“数世之后，皆成汉俗”。因此，为避免再度崛起的满洲民族被 “汉化”，其装束绝不能改变，这是保证弓马骑射的必需。否则，就会有“祖业衰歇，以迄于亡” 的危险。为此，皇太极还召集诸王、贝子，固山额真，“现身说法” “朕试为此喻，如我等于此，聚集宽衣大袖，左佩矢，右挟弓，忽遇硕翁科罗·巴鲁图（满语：鹫一般的勇士）劳萨（人名）挺身突入，我等能御之乎？若废骑射，宽袍大袖，待他人割肉而后食，于尚左手之人何异耶？” （《清太宗实录》卷三二崇德元年十一月癸丑）这是说，一旦满洲人放弃本民族的装束，换上汉人的宽衣大袖，必定会废弃骑射，继尔沦落到“待他人割肉而后食”的悲惨处境。这种看法未免浅薄，穿宽衣大袖未必就会废弃骑射，赵武灵王推行“胡服骑射”，赵国一样不免于亡，秦始皇穿戴着宽袍大袖的冕旒兖服，却最终统一天下。（“冠冕堂皇”这一成语也是从汉族皇帝的传统礼服，冕旒兖服来的。）但皇太极对他的想法坚信不疑，还以之“垂戒”后世，成为清王朝的基本“国策”。进一步的，满洲贵族不但恪尊自己民族的风俗习惯，还将其强加给被征服各地的汉族人民。在他们看来，只要汉人肯剃发易服，除去自己民族的传统服饰，就会断绝其复明之路，效忠满清统治者，作满清的顺民；而汉人和明廷官吏则把坚守自己的服饰发式，作为民族大义的表现。双方以之为冲突的焦点，进行殊死的搏斗。\r\n清军入关后，继续推行这个政策。崇祯十七年，（顺治元年1644年）四月二十二日，清军打败李自成进入山海关的第一天就下令剃头。五月初一日，清摄政王多尔衮率领清军过通州，知州迎降，多尔衮“谕令剃发”。初二进北京，次日多尔衮给兵部和原明朝官民分别发出命令，命兵部派人到各地招抚，要求“投诚官吏军民皆着剃发，衣冠悉遵本朝制度”。（《清世祖实录》卷五顺治元年五月庚寅）这是满清进入北京后正式下达剃发和易衣冠的法令。但是这一政策遭到汉族人民的强烈反对，在朝汉族官员遵令剃发者为数寥寥，不过孙之獬、李若琳等最无耻的几个人。不少官员观望不出，甚至护发南逃，畿辅地区的百姓甚至揭竿而起，连吴三桂也极言之。“（吴）至齐化门，居民出迎，见百姓皆剃发，垂泣曰：‘清人轻中国矣，前得高丽，亦欲剃发，丽人以死争之曰，我国衣冠相传数千年，若欲去发宁去头耳！清人亦止。我堂堂天朝，不如属国耶？我来迟，误尔等矣。’”（《謏闻续笔》）多尔衮见满洲贵族的统治还不稳固，自知操之过急，随即宣布收回成命，改口敷衍说：“予前因归顺之民无所分别，故令其剃发以别顺逆。今闻甚拂民愿，反非予以文教定民心之本心矣。自兹以后，天下臣民照旧束发，悉从其便。” （《清世祖实录》卷五顺治元年五月辛亥）所以清军入关后，剃发、易衣冠的政策只实行了一个月。\r\n\r\n然而，这一政策并未就此完结。当满清统治者认为天下大定之时，立刻以民族征服者的姿态，悍然下令全国男性官民一律剃发。\r\n\r\n清顺治二年（弘光元年1645年）六月十五日，清军攻占南京，多尔衮即遣使谕令多铎“各处文武军民尽令剃发，倘有不从，以军法从事”。十五日谕礼部道：“向来剃发之制，不即令画一，姑令自便者，欲俟天下大定始行此制耳。今中外一家，君犹父也，民犹子也；父子一体，岂可违异？若不画一，终属二心……自今布告之后，京城内外限旬日，直隶各省地方自部文到日亦限旬日，尽令剃发。遵依者为我国之民，迟疑者同逆命之寇，必置重罪；若规避惜发，巧辞争辩，决不轻贷。” （《清世祖实录》卷十七）这是对汉族民众的。同时要求地方官员严厉执行，更不许疏请维持束发旧制，否则“杀无赦。”这是一道严令，只能执行，不许违抗。满清统治者其实把辫子作为的“良民证”使用！多尔衮在顺治元年五月讲到剃发令时，就明它的功能是“以别顺逆”：“因归顺之民，无所分别，故令其剃发，以别顺逆”！\r\n\r\n当然，在这个过程中，一部分降清的龌龊汉官起了一定的恶劣作用，最无耻是，就是上面提到的孙之獬，孙之獬在明末清初官场上声名狼藉，一度名列魏忠贤逆党。清兵入京后，他为投靠新主子，极尽巴结阿谀之能。为了表示他效忠满清之诚。“于众人未剃发之前，即行剃发，举家男妇皆效满装。”（《清世祖实录》卷二十）并迅速上奏清廷，谄媚满洲统治者。据王家桢《研堂见闻杂记》云：“我朝之初入中国也，衣冠一仍汉制。……有山东进士孙之獬阴为计，首剃发迎降，以冀独得欢心。乃归满班，则满 人以其为汉人也，不受；归汉班，则汉以其为满饰也，不容。于是羞愤上疏，大略谓：‘陛下平定中国，万里鼎新，而衣冠束发之制，独存汉旧，此乃陛下从中国，非中国从陛下也。’于是削发令下。而中原之民无不人人思挺螳臂，拒蛙斗，处处蜂起，江南百万生灵，尽膏野草，皆之獬一言激之也。原其心，止起于贪慕富贵，一念无耻，遂酿荼毒无穷之祸。”1647年六月，山东谢迁领导的反清义军攻破淄川，擒获孙之獬，深恨其无耻，用锥子遍刺其身，插上头发，恨声不绝地骂道：“尔贪一官，编天下人之发，今我为汝种发！” 孙之獬自知众怒难犯，已无活理，破口大骂。义军将其口缝上，凌迟、肢解而死，还把他在城中的孙子、曾孙杀个精光！民愤之大，于此可见！\r\n\r\n是否由此就可以说，满清的剃发易服政策是因为孙之獬而来呢？不是！“剃发易服” 本来就是清王朝既定的“国策”，有没有人出面“奏请”，头发也是要剃的、衣冠也是要换的，只是推行的方式和策略而已。而且这一切，依据满清的统治实力而变化。清廷“定鼎燕京”之后，当时还没有占据全中国的野心，多尔衮下令剃发，有人认为这使得“南人闻风警畏，非一统之策也” 多尔衮说“何言一统？但得尺则尺，得寸则寸耳。”可见，对于能否一统天下，满洲贵族这时还没有把握，在等待局势变化。清军占领南京之后，满洲贵族才有统一天下的野心，这从“剃发、易服”令的推行就可以看出来。“定鼎燕京”之后下达的剃发令遭到汉人的激烈反对，连吴三桂也极力要求罢除剃发令。这个时候，清廷还担心自己立足未稳，害怕丧失人心，激成兵变、民变，所以暂时罢剃。多尔衮发一纸诏书敷衍说“予前因归顺之民，无所分别，故令其剃发，一别顺逆。今闻甚拂民愿……自此之后，天下臣民，照旧束发，悉从其便。”（上同）清军占领南京之后，认为统一天下已成必然之势，民族征服者的狰狞面目就露出来了！这回，多尔衮也不管汉人愿不愿意了，反正是人为鱼肉，我为刀俎“传旨叫官民尽皆剃头”“遵依者为我国之民，迟疑者同逆命之寇，必置重罪；若规避惜发，巧辞争辩，决不轻贷。”（上同）措辞非常严厉，结果自然是激起各地人民的强烈反抗，清廷的惟一手段，就是谴兵四处疯狂屠杀。清王朝的辫子、衣冠就此在血泊中固定下来。孙之獬的谄媚之举无疑符合满清贵族的征服、奴化策略，但它悖逆了广大汉族人民的文化传统和民族感情，因而招致了汉人的刻骨仇恨！而满清统治者在推行这一政策的野蛮、残暴、荒谬、***，在人类文明史上也是史无前例的！\r\n\r\n实际上，宽衣大袖、峨冠博带的服饰，加上蓄发、束发的传统，共同构成华夏民族延续上千年的独特风景线，也成为古典中国文明在外观上最重要的象征！可以看成是华夏文明的外在躯体！正因为如此，汉族人民的奋起反抗，不是为一家一族的封建皇权而战，不是为一派一系的学说而战，而是为捍卫华夏文明而战，为捍卫汉民族（华夏民族）的民族尊严而战斗！为保存汉族（华夏民族）之所以为汉族的存在而战斗！一位西方传教士目睹了这个过程，记载说“士兵和老百姓都拿起武器，为保卫他们的头发拼死斗争，比为皇帝和国家战斗得更英勇，不但把***人赶出了他们的城市，还把他们打到钱塘江，赶过了江北，杀死了很多***人。”（卫匡国《***战纪》）\r\n\r\n明儒顾炎武明确地把“亡国”和“亡天下”两个概念区别开来。他在《日知录》中说：“有亡国，有亡天下。亡国与亡天下奚辨？曰：易姓改号，谓之亡国；仁义充塞，而至于率兽食人。人将相食，谓之亡天下。……保国者，其君其臣，肉食者谋之；保天下者，匹夫之贱，与有责焉耳矣！”他认为，历史上的改朝换代，是“易姓改号，谓之亡国”，而满清那样“仁义充塞而至于率兽食人，人将相食。”“谓之亡天下”。他还说，“保国者，其君其臣肉食者谋之”，而“保天下者，匹夫之贱与有责焉”。这就是后人总结的“天下兴亡，匹夫有责”。换言之，历史上“易姓改号”的“亡国”就是“亡朝代”，“仁义充塞而至于率兽食人，人将相食”的 “亡天下”就是“亡国家”。著名历史学家顾诚先生在其《南明史》开篇章节中亦说：“在汉族官绅看来，大顺政权取代明朝只是‘易姓改号’，朱明王朝的挣扎图存是宗室、皇亲国戚、世袭勋臣之类‘肉食者’的事，同一般官绅士民没有多大关系；而满洲贵族的入主中原则是‘被发左衽’（剃头改制），是‘亡天下’了；天下兴亡，匹夫有责，都应当奋起反抗。”\r\n\r\n清廷颁行的“留发不留头，留头不留发”(注意：“留发不留头”不是百姓的口头阐，而是满清政权正式颁布的命令)，“剃发易服，不随本朝制度剃发易衣冠者，杀无赦”，“所过州县地方，有能削发投顺，开城纳款，即与爵禄，世守富贵。如有抗拒不遵，大兵一到，玉石俱焚，尽行屠戮”等法令，相当于以多尔衮为代表满洲贵族发布的“屠城令”，之后，有蓄发者立执而剃之，不服则斩，悬其头于剃头挑子所缚高竿之上示众。\r\n\r\n值得一提的是，满清统治者在颁布剃发令所要求的剃发标准，并非现在人们常常看到的剃半个头，而是将头颅四周的头发都剃掉，只留一顶如钱大，结辫下垂。在头顶留发一钱大，大于一钱要处死！那种清末才有的、现在不合事实地垄断了所有清装戏的阴阳头发式，放在当时也得死，因为满清规定：“剃发不如式者亦斩。”顺治四年，浒墅关民丁泉“周环仅剃少许，留顶甚大”，被地方官拿获，以“本犯即无奸宄之心，甘违同风之化，法无可贷”为由上奏，奉朱批：“着就彼处斩”，县官也以失察“从重议处，家长、地邻即应拟罪”。\r\n\r\n据秦世祯《抚浙檄草》载：“小顶辫发”即每个汉族男子，都被迫把以前“不敢毁伤”的头发屈辱地剃去，只留下铜钱大一点，梳成一根小辫，叫“金钱鼠尾”式。将四周头发全部剃去，仅留头顶中心的头发，其形状一如金钱，而中心部分的头发，则被结辫下垂，形如鼠尾，实在不堪入目。而 1647年，（南明永历元年）清军攻陷广州时的剃发令中竟还无耻的说：“金钱鼠尾，乃新朝之雅政；峨冠博带，实亡国之陋规。”福州遗民陳燕翼撰《思文大纪》写道：“时剃头令下，闾左无一免者。金钱鼠尾，几成遍地腥膻。”\r\n\r\n满清的剃发易服令也激起社会上层人士的强烈反对，顺治二年十月，孔子后人，原任陕西河西道孔闻謤奏言：“近奉剃头之例，四氏子孙又告庙遵旨剃发，以明归顺之诚，岂敢再有妄议。但念先圣为典礼之宗，颜、曾、孟三大贤并起而羽翼之。其定礼之大，莫要于冠服。……惟臣祖当年自为物身者无非斟酌古制所载章甫之冠，所衣缝掖之服，遂为万世不易之程，子孙世世守之。自汉、唐、宋、金、元以迄明时，三千年未有令之改者，诚以所守者是三代之遗规，不忍令其湮没也。即剃头之例，当时原未议及四氏子孙，自四家剃发后，章甫缝掖不变于三千年者未免至臣家今日而变，使天下虽知臣家之能尽忠，又惜臣家未能尽孝，恐于皇上崇儒重道之典有未备也。应否蓄发，以复本等衣冠，统惟圣裁。” （《清世祖实录》一[过滤词]五年十月初三日孔闻謤揭帖）孔闻謤搬出孔子这块大招牌，又引金、元二代为例，满以为可以抵挡一阵子，保住先世蓄发衣冠。不料却碰了个大钉子，得旨：“剃发严旨，违者无赦。孔闻謤疏求蓄发，已犯不赦之条，姑念圣裔免死。况孔子圣之时，似此违制，有玷伊祖时中之道。著革职永不叙用”。连孔子的面子也不给。这就揭穿了满清统治者所谓“尊孔崇儒”的真面目：“奉行儒术，崇饰观听。”不过是“南面之术，愚民之计”（章太炎《驳康有为论革命书》）罢了。对孔闻謤，算是比较客气，“姑念圣裔免死”，如果把他杀了，恐失天下士人之心，也不好摆“崇儒”的高姿态。对另外的人，就没这么客气了，而是“杀无赦”。\r\n\r\n这种“留头不留发、留发不留头”的极端残暴的民族压迫政策，使外国人也无比震惊：“至今为止，用如此残忍的手段迫使一个民族放弃自己的风俗习惯，世界上还没有类似的例子。”（祖甫江孝男《文化人类学入门》）。\r\n\r\n剃发易衣冠成了顺治年间社会矛盾的焦点。不仅激起了强烈新统区人民士绅的反抗，就连已然剃发的满清忠顺之臣、甚至满洲贵族中也对此政策提出了疑议。然而满清最高统治者推行剃发易服决心之大，态度之坚决，丝毫没有通融的余地。\r\n\r\n顺治十一年（永历八年 1654），自顺治元年冬降清后，一直受到清廷最高统治者多尔衮、福临的信任，官居吏部尚书、内院大学士的复社文人陈名夏，因私下议论“只须留头发、复衣冠，天下即太平矣！”而被处以绞刑。弹劾陈名夏的，是早在满清还没入关就已降清此刻已官居清廷内院大学士的汉臣宁完我，宁完我在奏疏中说：“臣思我国臣民之众，不敌明朝十分之一，而能统一天下者，以衣服便于骑射，士马精强故也。今名夏欲宽衣博带，变清为明，是计弱我国也”。（《清史列传》陈名夏）其实，苟且乞怜的陈名夏只是出于对主子的一片忠心，但他的私下议论却触犯了清廷的忌讳，拍马屁拍到蹄子上，所以被斩杀也在预料之中。\r\n\r\n满清统治者的全国统治确立之后，在满洲贵族内部，也有人就冠服问题提出疑义，要求考虑恢复华夏衣冠，但满清统治集团对内部的异议也坚决予肃整。满清入关之前，文才极高曾甚受皇太极信任的满大臣库尔缠因主张汉化终见恶于皇太极，被借故处死。同时，“衣服骑射，不可轻变”这也是皇太极早就告戒过满洲贵族们的 “祖训”。清廷在招降郑成功、郑经父子时，总提出以剃发、登陆为前提条件，郑方则坚持相反的意见，双方议论不决。1662年，郑成功病故，郑经嗣立，向清廷提出仿朝鲜旧例的臣服方式，“不登岸，不辫发易衣冠”，清廷不许。到1680年，清王朝在中国大陆的统治已经完全确立下来，“反清复明” 也成了一句空话，清、郑双方还在为臣服的方式、辫子衣冠的问题争执不下。最后，郑经提出，台湾全岛皆可剃发、换装，只有他一人不剃不换，清廷也不许。台 湾军民最终没能保住汉式衣冠，1683年，清军入台湾，消灭郑氏余部，满洲贵族剃掉最后一个汉族男人头发的鸿愿终于“大功告成”！\r\n\r\n在屠杀与抗争了三十七年之后，汉服最终从华夏的土地上消失。满服的旗袍、马褂，再加一条金钱鼠尾的辫子装束，就这么在刺刀、血泊中被固定下来。\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/11/2408243032.jpg', '0', '1', null, 'post', 'publish', null, '3', '1', '1', '1', '0', '1', '3828', '645', '0');
INSERT INTO `typecho_contents` VALUES ('54', '02.Product_00_AllProducts_MySQL.png', '02-Product_00_AllProducts_MySQL-png-1', '1573736554', '1573736554', 'a:5:{s:4:\"name\";s:35:\"02.Product_00_AllProducts_MySQL.png\";s:4:\"path\";s:35:\"/usr/uploads/2019/11/2406683442.png\";s:4:\"size\";i:11499;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '57', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('55', '2.jpg', '2-jpg-2', '1573736554', '1573736554', 'a:5:{s:4:\"name\";s:5:\"2.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/11/1233049756.jpg\";s:4:\"size\";i:145976;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '2', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '57', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('56', '3.jpg', '3-jpg-2', '1573736555', '1573736555', 'a:5:{s:4:\"name\";s:5:\"3.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/11/2291731337.jpg\";s:4:\"size\";i:246445;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '3', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '57', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('57', 'Navicat12破解教程，Navicat12.1.20亲测有效！！', '57', '1573736640', '1573736905', '<!--markdown-->![02.Product_00_AllProducts_MySQL.png][1]\r\n说来心累，在网上找了好多Navicat12的破解教程，奈何我有“最新版”强迫症！！！！非要用最新的12.1，所以，一众大神的激活教程都不管用，老的注册机版本为Navicat_Keygen_Patch_v3.4，也有3.7的，还是不管用。\r\n还有把补丁直接放到安装软件根目录下的，对于新版本，仍旧不好用，求人不如求己，经过辛苦摸索，现将可用的，方便的教程放出来，供大家交流学习。\r\n本教程为 Navicat12注册教程，支持最新版Navicat12.1.20 注册机为Navicat_Keygen_Patch_v4.8，默认支持12.1.20之前版本的破解。本教程仅供学习交流，请支持正版Navicat\r\n\r\n软件地址：\r\n链接：https://pan.baidu.com/s/1RxvOuKQtmnaqyilvbcgdbg\r\n提取码：4vvq\r\n\r\n![1.jpg][2]\r\n1、先安装navicat12 ，完成后打开软件，点击14天试用，关闭软件！！\r\n2、关闭杀毒软件（自行百度），解压Navicat_Keygen_Patch_v4.8_By_DFoX，右键管理员身份运行程序\r\n3、点击patch（只能patch一次！！切记，走过的坑），找到Navicat12的安装路径，找到主程序Navicat.exe或者navicat，选中，打开，会提示Cracked，则此步骤表明成功，继续\r\n\r\n4、打开Navicat12软件，点击弹出框的注册\r\n5、点击破解软件中间的“4.keygen/offline activation”的Generate ，稍等片刻，前面软件框生成的序列号会自动插入到Navicat12的注册界面，核对一下是否成功。\r\n\r\n![2.jpg][3]\r\n6、点击Navicat12注册页面的激活----手动激活，将生成的请求码复制到破解软件的request code 后面的大框内，\r\n\r\n![3.jpg][4]\r\n然后点击破解软件左下角的Generate，此时，激活码会自动插入到Navicat12注册页面的接活码区内，点击激活即可。（没有的话就手动复制粘贴）。\r\n\r\n7、如果出现“error decrypt code”，卸载安装的Navicat12 ，删除安装目录，重复第1步骤。\r\n\r\n有其他问题的，欢迎留言\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/11/2406683442.png\r\n  [2]: http://www.phpmaster.cn/usr/uploads/2019/11/422668645.jpg\r\n  [3]: http://www.phpmaster.cn/usr/uploads/2019/11/1233049756.jpg\r\n  [4]: http://www.phpmaster.cn/usr/uploads/2019/11/2291731337.jpg', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '3396', '585', '0');
INSERT INTO `typecho_contents` VALUES ('58', '1.jpg', '1-jpg-2', '1573736900', '1573736900', 'a:5:{s:4:\"name\";s:5:\"1.jpg\";s:4:\"path\";s:34:\"/usr/uploads/2019/11/422668645.jpg\";s:4:\"size\";i:106811;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '4', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '57', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('60', '微信图片_20191114225921.jpg', '微信图片_20191114225921-jpg', '1573743635', '1573743635', 'a:5:{s:4:\"name\";s:31:\"微信图片_20191114225921.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/11/2408243032.jpg\";s:4:\"size\";i:28478;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '59', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('63', '《牵丝戏》- 银临', '63', '1575703260', '1575703740', '<!--markdown-->![e61190ef76c6a7ef95e0b421f5faaf51f2de66f6.jpg][1]\r\n[Meting]\r\n[Music server=\"netease\" id=\"30352891\" type=\"song\"/]\r\n[/Meting]\r\n\r\n\r\n牵丝戏<br>作曲：银临<br>编/混：灰原穷<br>填词：Vagary<br>演唱：银临、Aki阿杰<br>海报：苏澈白<br><br>余少能视鬼，尝于雪夜野寺逢一提傀儡翁，鹤发褴褛，唯持一木偶制作极精，宛如娇女，绘珠泪盈睫，惹人见怜。<br>时云彤雪狂，二人比肩向火，翁自述曰：少时好观牵丝戏，耽于盘铃傀儡之技，既年长，其志愈坚，遂以此为业，以物象人自得其乐。奈何漂泊终生，居无所行无侣，所伴唯一傀儡木偶。<br>翁且言且泣，余温言释之，恳其奏盘铃乐，作牵丝傀儡戏，演剧于三尺红绵之上，度曲咿嘤，木偶顾盼神飞，虽妆绘悲容而婉媚绝伦。<br>曲终，翁抱持木偶，稍作欢容，俄顷恨怒，曰：平生落魄，皆傀儡误之，天寒，冬衣难置，一贫至此，不如焚，遂忿然投偶入火。吾止而未及，跌足叹惋。忽见火中木偶婉转而起，肃拜揖别，姿若生人，绘面泪痕宛然，一笑迸散，没于篝焰。<br>火至天明方熄。<br>翁顿悟，掩面嚎啕，曰：暖矣，孤矣。<br><br>[银临]<br>嘲笑谁恃美扬威 没了心如何相配<br>盘铃声清脆 帷幕间灯火幽微<br>我和你 最天生一对<br><br>没了你才算原罪 没了心才好相配<br>你褴褛我彩绘 并肩行过山与水<br>你憔悴 我替你明媚<br><br>是你吻开笔墨 染我眼角珠泪<br>演离合相遇悲喜为谁<br>他们迂回误会 我却只由你支配<br>问世间哪有更完美<br><br>[Aki]<br>兰花指捻红尘似水<br>三尺红台 万事入歌吹<br>唱别久悲不成悲 十分红处竟成灰<br>愿谁记得谁 最好的年岁<br><br>[银临]<br>你一牵我舞如飞 你一引我懂进退<br>苦乐都跟随 举手投足不违背<br>将谦卑 温柔成绝对<br><br>你错我不肯对 你懵懂我蒙昧<br>心火怎甘心扬汤止沸<br>你枯我不曾萎 你倦我也不敢累<br>用什么暖你一千岁<br><br>[Aki]<br>风雪依稀秋白发尾<br>灯火葳蕤 揉皱你眼眉<br>假如你舍一滴泪 假如老去我能陪<br>烟波里成灰 也去得完美\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/12/1699722914.jpg', '0', '1', null, 'post', 'publish', null, '2', '1', '1', '1', '0', '1', '3054', '433', '0');
INSERT INTO `typecho_contents` VALUES ('61', '汉族3000年的自卫反击史', '61', '1574387760', '1616253883', '<!--markdown-->\r\n从公元前21世纪的夏朝开始，汉族祖先华夏人，已经在中原建立农耕文明国家。到今天，整整4000多年了。\r\n\r\n在这4000年的历史里，汉族赢了3000多年，鞑虏赢了两次半。\r\n\r\n\r\n汉族赢的时候，大家安居乐业。\r\n鞑虏一旦赢了，国家政治制度倒退，民生倒退，科技倒退，文明倒退。民族压迫降临，动辄屠杀和强奸妇女。\r\n\r\n\r\n     没人喜欢鞑虏赢。但历史无情，鞑虏赢过两次半。\r\n这两次半的胜利，也不英勇，只是捡漏。\r\n\r\n\r\n       五胡乱华，是在三国内斗和西晋八王之乱后，汉族内耗濒临自灭，五胡捡漏，占了半壁江山。\r\n\r\n\r\n       蒙元灭宋，是在辽宋夏金大乱斗的长期消耗之下，一举荡平各势力，厉害了一次，给东西方带来了屠杀、瘟疫、种族压迫等等灾难，然后如历史中昙花一现，88年而亡。\r\n\r\n\r\n       清灭明，这个事儿不存在。明末农民起义遍地，李闯王进北京灭明，没满清什么事儿。\r\n\r\n\r\n      大明内耗灭亡后，清朝利用吴三桂和刘宗敏的私仇，招降了吴三桂，这才进了山海关。不是打的。满清进了山海关，收拾了各路农民起义，收拾了没有野战军的南明，这就捡了江山。还未远去的清朝，就是鞑虏的时代。\r\n\r\n\r\n       鞑虏为了统治汉族，抹杀汉族自尊，无限夸大史上两次半的胜利，把汉族3000多年文明骂的体无完肤。\r\n\r\n\r\n      清朝以后，汉人的国亡了，舆论的风向也变了。\r\n\r\n\r\n      提到汉族，只谈汉族的繁荣富裕，绝口不提“犯我强汉虽远必诛”，绝不提起“汉族3000多年的自卫反击史”。更不提汉族战败的原因是制度缺陷和内斗，只有对汉族战斗力的无限贬低。\r\n\r\n\r\n但你要懂得，繁荣富裕必须由强大军力来守护。\r\n\r\n\r\n在汉文明4000年的历史中，汉族祖先年年浴血奋战，驱除鞑虏。\r\n如果没有浴血奋战，根本保不住这富庶国家和文明成果。\r\n满清统治者，最怕的就是汉族“驱除鞑虏”。\r\n\r\n\r\n因为汉族靠着驱除鞑虏，在东方屹立了3000多年。\r\n试想，在汉族的3700多年存续中，要和胡人打多少战争。\r\n游牧民族年年南下，岁岁入侵，我们要打多少战争，才能存活至今。\r\n汉人活在今天，就是汉族战胜的最好证明。\r\n在残酷的“民族生存竞争”历史中，汉族只有战胜，才能保住自己的国和民。\r\n\r\n\r\n汉族的亡国期，汉族的生存也不是靠入侵者怜悯，而是异族惧怕“人口庞大的汉族在种族压迫下会爆发反击”，这是侵略者不敢尽屠汉族的唯一原因。\r\n\r\n\r\n鞑虏南下，汉族年年征战。\r\n鞑虏犯边，汉族北伐草原。\r\n汉族对游牧战争，论灭国级的大战，胜负也是20比3。\r\n汉族胜20，游牧胜3。\r\n汉族北伐，次数就更多了。\r\n有记录的也几十次，被删除篡改的数不胜数。\r\n毕竟满清统治者，哪由得“驱除鞑虏”这几个字随意出现。\r\n\r\n满清篡改出来的汉族历史，得按照满清利益写，所以这“驱除鞑虏”，也只是明初北伐写的详细一点。\r\n\r\n明初北伐，没被历史抹去，不容易啊。\r\n毕竟是汉蒙之间的灭国与复仇嘛。\r\n汉蒙仇恨得记录下来，为了满清统治安稳，汉蒙得斗啊。\r\n\r\n\r\n就这样，清代以来的历史典籍，成了维护满清利益的舆论引导读本。\r\n然而，不管怎么改，这朝代次序终归是改不了。\r\n\r\n\r\n这东方历史4000年，鞑虏赢了两次半。\r\n就这两次半，鞑子天天吹，年年吹。鼓吹的太久，就吹出了一种思潮：“游牧一定打败农耕，胡人一定打败汉人”。\r\n4000年赢了2次半，竟然吹成了“他一直在胜利”，多么不要脸。\r\n\r\n\r\n鞑虏就是不提被汉族打爆的事儿。\r\n那么我们帮他们回忆一下。\r\n我们先看看唐诗，是怎么描述唐军灭鞑虏的。我随便列举几句。\r\n\r\n\r\n月黑雁飞高，单于夜遁逃。\r\n欲将轻骑逐，大雪满弓刀。 \r\n愿将腰下剑，直为斩楼兰。\r\n\r\n黄沙百战穿金甲，不破楼兰终不还。 \r\n一身能擘两雕弧，虏骑千重只似无。\r\n前军夜战洮河北，已报生擒吐谷浑。\r\n但使龙城飞将在，不教胡马度阴山。\r\n\r\n\r\n少年十五二十时，步行夺得胡马骑。汉兵奋迅如霹雳，虏骑崩腾畏蒺藜。\r\n脱鞍暂入酒家垆，送君万里西击胡，功名只向马上取，真是英雄一丈夫。\r\n\r\n\r\n严风吹霜海草凋，筋干精坚胡马骄。汉家战士三十万，将军兼领霍骠姚。\r\n敌可摧，旄头灭，履胡之肠涉胡血。悬胡青天上，埋胡紫塞旁。胡人亡，汉道昌。\r\n\r\n\r\n河广传闻一苇过，胡危命在破竹中。\r\n祗残邺城不日得，独任朔方无限功。\r\n偏坐金鞍调白羽，纷纷射杀五单于。 \r\n胡雁哀鸣夜夜飞，胡儿眼泪双双落。\r\n\r\n\r\n大家看看。\r\n这些诗句，就是描写汉人自卫反击的场面，生动描写了胡人战败逃窜的悲惨模样。\r\n有的“黑汉写手”又要跳出来问了，说你这也就是唐朝的事儿，别的朝代呢？\r\n\r\n\r\n\r\n那么我们回顾一下，汉族自卫反击的3000多年，打了多少战争。\r\n\r\n\r\n秦汉时期；\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n1；公元前213年，蒙恬率军北击匈奴，进攻纵深达到今天的阴山河套以北，夺匈奴朝那（今宁夏固原市东南）、肤施（今陕西省榆林市南鱼河堡附近）等地。秦大胜匈奴后，置九原郡，治所在今天的包头。随后蒙恬在燕、赵、秦长城基础上，修筑了万里长城。\r\n\r\n\r\n2；公元前201年，韩王信在大同地区叛乱，并勾结匈奴企图攻打太原。汉高祖刘邦率32万大军迎击匈奴，先在铜辊（今山西沁县）告捷，后来又乘胜追击，直至楼烦（今山西宁武）一带，直追到大同平城，因冒进被围困于平城白登山。后周勃部连破匈奴攻占楼烦三城，匈奴狼狈而逃，解白登之围。这是汉族和匈奴第一次大规模战争，以汉族惨胜告终。（并非什么汉高祖战败被迫和亲的。和亲只是后来的政治策略）。\r\n\r\n\r\n3；汉武帝元朔二年，卫青进攻河套，大破匈奴，收复了被匈奴占领的河套地区。汉武帝在河套建朔方郡，并建朔方城，再次确认了中国对于河套地区的统治。\r\n\r\n\r\n4；汉武帝元狩二年，骠骑将军霍去病在河西走廊地区发动了两次河西战役，大破匈奴，一举占领匈奴最后的一块膏腴之地（河西走廊），使匈奴人哀叹：“亡我祁连山，使我六畜不蕃息，失我焉支山，使我妇女无颜色”。汉武帝在河西置张掖郡，并在今天的敦煌以西沙漠边缘建长城和嘉峪关、阳关。从此中国打开了中亚的大门。\r\n\r\n\r\n5；汉武帝元狩四年，卫青，霍去病两路并发，发动了征服匈奴的漠北战役。卫青进攻纵深达到今天蒙古的杭爱山。霍去病一路，觅得匈奴主力，大获全胜。封狼居胥，并一直追击匈奴残部到翰海（今天俄罗斯贝加尔湖）。\r\n\r\n6；汉武帝元鼎五年，汉伏波将军路博德在番禺大破南越，灭南越，置南海郡（今广州），交趾郡（今河内）等九郡，从此华南和越南北部纳入中国版图。\r\n\r\n7；汉武帝元封二年，汉楼船将军杨仆、左将军荀彘率水陆军两路进击卫氏朝鲜，前108年，卫氏朝鲜降。汉置辽东郡，玄菟郡（今朝鲜北部），乐郎郡（今平壤），真番郡（今韩国北部），临屯郡（今韩国东北）五郡，至此统治朝鲜半岛大部分地区。\r\n\r\n\r\n东汉时期；\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n1；东汉永元元年，窦宪、耿秉，分三路进击，大破北匈奴于稽落山。登燕然山（今外蒙杭爱山）刻石记功而还。\r\n\r\n2；东汉明帝永平十六年，至和帝永元六年，东汉司马班超率部，纵横西域，西域50余国均纳质归附，其子班勇继为西域长使，西域对汉朝长期处于臣服状态。确认了西域在历史上的中华属国地位，为西域纳入中国版图提供了历史依据。\r\n\r\n3；东汉献帝建安十二年，丞相曹操在白狼山（今辽西北部）歼灭乌桓主力，乌桓灭国。\r\n\r\n4；三国蜀后主建兴三年，汉丞相诸葛亮平定南中（今云南、贵州及四川西南部地区）。建永昌郡（今云南保山），从此云贵高原纳入中国版图。\r\n\r\n\r\n两晋南北朝时期；\r\n\r\n\r\n\r\n1；建武元年（317年）祖狄（祖逖）率部北伐，得到各地人民的响应，数年间收复黄河以南大片领土。因势力强盛，受到东晋朝廷的忌惮。太兴四年（321年），东晋朝廷命戴渊出镇合肥，以牵制祖逖。祖逖目睹朝内明争暗斗，国事日非，忧愤而死，部众被弟弟祖约接掌。\r\n\r\n注；北伐大业功败垂成。祖狄北伐的成败，说明汉族战力受政治制度制约，无法发挥。\r\n\r\n2；东晋永和六年（350年），冉闵灭亡后赵，建立冉魏政权。颁布杀胡令，面对异族屠杀，冉闵保卫了北方汉人的生存空间，强势反杀胡人，带领汉族绝地反击，拯救汉族全族，是汉族得以繁衍至今的功臣。\r\n\r\n3；永和十年（354年）二月，桓温率军北伐前秦。\r\n\r\n4；永和十二年（356年）同年八月，桓温击败羌族首领姚襄，收复洛阳。进入金墉城，拜谒先帝皇陵，并设置陵使修复皇陵。\r\n\r\n5；太和四年（369年）四月，桓温率步骑五万，与江州刺史桓冲、豫州刺史袁真一同北伐前燕。湖陆一战擒获燕将慕容忠。一直打到枋头，距前燕国都邺城只有二百多里，还收复了淮水以北的广大地区恢复了部分失地，打击了氐族、羌族、鲜卑族的统治者残暴统治，并以实际行动支持了北方各族人民的反压迫斗争，符合汉族人民的利益。\r\n\r\n6；太元八年（383年），苻坚率领着号称百万的大军南下，谢安，谢石、谢玄、谢琰和桓伊等人，指挥淝水之战，以8万东晋军击败八十万前秦军。淝水之战的胜利，说明由众多游牧民族联合而成的前秦军，游牧各族为自己利益，都想保留实力，少受损失，多分战果，不能团结一致，不敢死战。而汉军团结奋战，可以把异族联军各个击破。谢安因军功大，遭到当权者忌惮，被迫交权病逝。谢安的生平，再次说明汉军的战力，主要被统治层束缚，无法发挥。士族制度皇权世袭制度的落后性\r\n\r\n7；南梁普通七年（526年），元树、陈庆之出兵寿春。梁军共克52城，获75000人。\r\n\r\n8；大通元年（527年）十月，陈庆之与曹仲宗联合进攻北魏涡阳，大败北魏军。\r\n\r\n9；大通二年（528年），陈庆之率梁军从铚县出发，北魏济阴王元晖业率羽林军2万来援，陈庆之在考城（今河南民权东北）全歼2万敌军，俘元晖业，得粮草辎重7800余车。\r\n\r\n10；中大通元年（529年）四月陈庆之攻克荥阳，收缴荥阳的储备，牛马谷帛都不可胜计。陈庆之旋即进攻虎牢，尔朱世隆不敢战，弃城而逃，梁军俘魏东中郎将辛纂。\r\n\r\n11；中大通元年（529年）夏，魏孝庄帝元子攸为避陈庆之锋芒，被迫撤至长子。陈庆之率军先后攻克洛阳、大梁。因陈庆之军皆穿白袍，城中童谣曰：“名师大将莫自牢，千兵万马避白袍”。陈庆之又以七千之众，从铚县至洛阳，前后作战47次，攻城32座，皆克。中大通元年冬，魏军集结三十万兵力反扑，陈庆之因兵少，无法固守，带领手下人马向南梁撤退，在蒿高遇到了山洪爆发。正在渡河的梁军被洪水吞没。\r\n\r\n\r\n\r\n隋唐时期；\r\n\r\n\r\n\r\n1；唐高祖武德八年（622），李道宗在灵州击败西突厥。\r\n2、唐太宗贞观三年，李靖和李勣（徐世勣）率军灭东突厥，俘颉利可汗。建安北都护府。将漠北纳入中国版图。\r\n3；贞观八年（634），牛进达击败吐蕃松赞干布。\r\n4、贞观九年，李靖率军在柏海（今青海黄河源头的鄂陵湖和扎陵湖）大破吐谷浑，吐谷浑慕容顺率部归唐，吐谷浑成为唐朝属国。\r\n5；贞观十四年，侯君集灭高昌。\r\n6；贞观十八年，郭孝恪灭焉耆、疏勒、于阗等二十余国，重开丝路。\r\n7；贞观二十年，李道宗、薛万彻、张俭等、率军击溃薛延陀。贞观二十年六月，薛延陀酋长梯真达官率众归降李勣。七月，李勣、李道宗歼灭薛延陀余部，在郁督军山北（外蒙杭爱山）斩首五千多级，俘虏三万余口。灭薛延陀。\r\n8；贞观二十一年，王玄策摧毁天竺阿祖那的七万战象部队，生擒阿祖那。\r\n9；贞观二十一年，牛进达攻克高丽石城（庄河），后攻积利城，斩首两千余。\r\n10、唐高宗显庆二年（657），右屯卫将军苏定方率军进攻西突厥，在金牙山（今中亚塔什干）大破西突厥沙钵罗可汗。灭亡西突厥。唐在西突厥故地设置漾池、昆陵二都护府。唐疆域扩张到了伊犁河流域。\r\n11、唐高宗显庆五年，左卫中郎将苏定方引兵渡海，在熊津江口（今韩国南部）大败百济军，攻破百济都城俱拔城（今韩国全州）。国王扶余义慈被迫率众归降，百济遂灭。唐在百济置熊津等五都督府。\r\n12；唐高宗龙朔元年（661），薛仁贵灭铁勒，定漠北。\r\n13；唐高宗龙朔三年（663），刘仁轨在白江口之战，歼灭日本百济联军数万。此战奠定了此后一千余年间东北亚地区的政治格局。\r\n14；唐高宗总章元年（668），李勣（徐世勣）攻克平壤，灭高句丽。俘虏高丽王高藏。此战，唐朝共获一百七十六城，六十九万七千户。分其地置九个都督府，四十一州，一百县，设安东都护府管辖高句丽旧地。此后200多年的时间内，朝鲜一直属于中国版图。\r\n15；调露元年（679），突厥的阿史德温傅背叛唐朝。裴行俭率李思文、程务挺，唐休璟等统领三十多万人马，在黑山击溃突厥叛军。\r\n16；永隆二年（681年），程务挺在金牙山击败阿史那伏念。\r\n17；唐高宗永淳元年（682），薛仁贵云州败突厥。\r\n18；永淳元年（682年），王方翼在伊犁河击溃阿史那车薄。又在热海击溃突厥援军十万，杀敌七千，擒其首领300人，西域震服。\r\n19；永淳元年，吐蕃入侵河源军（治今青海西宁）。娄师德率军在白水涧（今青海湟源）迎战，八战八捷。\r\n20；武则天长寿元年（692），王孝杰率军进入西域后，击败吐蕃，接连收复安西四镇。\r\n21；久视元年（700），唐休璟在洪源谷击溃吐蕃。\r\n22；唐玄宗开元二年，薛讷、郭知运在武街驿（今甘肃临洮东）大败吐蕃军，后双方激战于长城堡（在今甘肃临洮境），唐军再次大败吐蕃，斩首一万七千余。\r\n23；开元三年，张孝嵩败吐蕃阿拉伯联军，灭拔汗那国。\r\n24；开元五年，郭知运在黄河九曲击败吐蕃。\r\n25；开元十七年，李祎在石堡城一役大破吐蕃。\r\n26；开元十七年，张守珪大破吐蕃。\r\n27；开元二十年（723），李祎在抱白山之战大破奚和契丹，俘虏敌酋，驱逐叛逆。\r\n28；开元二十一年，王忠嗣在新城大败吐蕃。\r\n29；开元二十二年，张守珪镇守幽州，灭契丹可突可汗。\r\n30；开元二十五年，崔希逸在河西（青海西）大败吐蕃。\r\n31；开元二十七年，盖嘉运灭中亚突骑施，把疆域扩展到了葱岭以西的河中地区（阿姆河和锡尔河流域）。\r\n32；天宝元年（742），王倕在新城大破吐蕃（今青海门源）。\r\n33；天宝元年，皇甫惟明在陇右（青海）大败吐蕃，阵斩吐蕃赞普之子琅支都。\r\n34；天宝五年，王忠嗣收复吐谷浑。\r\n35；天宝六年（747）高仙芝灭小勃律、慑服七十二国。\r\n36；天宝十一年（753），封常清灭吐蕃属国大勃律。\r\n37；唐代宗广德二年（764），郭子仪退回纥、吐蕃联军。在灵台西原（今甘肃泾川）击破吐蕃，斩首五万。\r\n38；唐德宗贞元五年（789），韦皋派遣王有道在台登北谷击溃吐蕃，斩杀二千人。\r\n39；唐武宗会昌二年（842），李德裕击溃回鹘，杀胡山灭乌介可汗。\r\n40；唐宣宗大中二年（848），张议潮率众组成归义军驱逐吐蕃收复瓜州，沙州等地。大中四年，张议潮率众收复西州。大中五年，收复敦煌、安西、伊州、西州，临夏、张掖、酒泉、兰州、鄯州、河州、岷州、廓州等十一州，派遣兄长张议潭携版图户籍入朝。咸通二年（861），张议潮收复凉州。彻底击溃吐蕃。\r\n41；唐懿宗咸通六年（866），高骈率军击溃南诏军20万，再度控制交趾。\r\n。。。。\r\n\r\n\r\n宋代；\r\n\r\n\r\n\r\n\r\n\r\n\r\n1；宋真宗咸平二年，檀渊之战（999）。辽国对宋发动入侵战争，宋真宗亲征。宋军禁军在檀渊城击毙辽国主将萧挞凛（萧兰达），杨延昭军从山西合围辽军，辽军陷入死境。书本上记载是宋真宗求和。实际情况谁知道呢，总之此战后，辽国120年没来入侵，两国交战，和平从来不是求和得来的，是打赢。但宋史篡改严重。书上篡改的好，汉族赢了就写汉族求和，异族赢了就写战果无数、汉人溃败被杀，打的汉族如何如何。。谁让异族掌控了书籍改编权呢，是吧。\r\n2；北宋元丰4年，五路伐夏之战（1081），李宪攻克兰州，兰州归入北宋版图。\r\n3；北宋元符2年，平夏城之战（1099），占领西夏天都山。\r\n4；北宋政和3年，横山之战（1114），西夏主力溃败，面临亡国。\r\n5；南宋绍兴3年，和尚原之战，仙人关之战（1134），吴阶吴璘等将在仙人关、大散关、杀金坪等地大败金军。\r\n\r\n6；南宋绍兴3年至绍兴9年，岳飞北伐（1134-1140），收复襄樊、郢州（今湖北钟祥）、随州（今湖北随县）、邓州（今河南邓县）、唐州（今河南唐河）、信阳等六郡之地。\r\n\r\n7；南宋绍兴9年，颍昌大捷。郾城大捷（1140）。\r\n8；南宋绍兴9年，朱仙镇大捷（1140），全线收复中原失地，包围开封。\r\n\r\n9；宋绍兴三十一年（1161）年冬,宋军在虞文允指挥下，于采石矶击溃金主完颜亮，取得大胜。\r\n\r\n10；开禧二年（1206年）四月，宋宁宗命殿帅郭倪招抚山东、京、洛地区。郭倪派毕再遇与统制陈孝庆等攻取泗州，金军大败，南宋收复泗州。\r\n\r\n11；开禧二年（1206年）十月，金将胡沙虎率领步、骑兵数万及战船五百多艘自清河口渡过淮河，停泊在楚州（今江苏淮安）、淮阴之间，包围楚州。毕再遇援助楚州，在竹镇大败金军。\r\n\r\n12；开禧二年十一月，金国万户完颜蒲辣都、千户泥庞古等率十万骑兵驻屯在成家桥、马鞍山，进兵围六合城六合一役，毕再遇等将率军迎战，大败金军。宋军缴获金军骡马一千五百三十一匹、鞍六百套以及众多的铠甲、旗帜。\r\n\r\n13；开禧二年十二月，毕再遇在楚州派将分道阻击金兵，金军大败，楚州解围，金军北退。\r\n\r\n注：嘉定四年（1211年）二月，毕再遇遭到言官弹劾，先“降一官”。继而再遭劾奏，于是被免去提举佑神观之职。毕再遇和岳飞一样，反击异族入侵，立下军功，继而遭到文官集团的迫害压制，制度缺陷是压制宋军战力的主要原因。\r\n\r\n14；嘉定十二年（1219年），金将完颜讹可率步骑二十万分两路攻枣阳，孟珙取它道偷袭金人，破18寨，斩首千余级，缴获大量军器。\r\n\r\n15；绍定五年（1232年），孟珙和刘全、雷去危等将，率军在襄阳附近全歼金国武仙军二十万。\r\n\r\n16；端平元年（1234年），宋蒙联合灭金，江海、孟拱、江万载、马义云等将率军攻入蔡州，斩杀金哀宗，孟拱得到金哀宗的尸体，和蒙古平分金国皇帝的仪仗器械和玉玺等物。金国灭亡。\r\n\r\n17；端平二年（1235年），蒙古在南宋的川蜀、荆襄发动了全面入侵。孟珙率军转战江陵、枝江，连破敌二十四座营寨，抢回被俘百姓两万多人，并将蒙军的渡江器具一并焚毁，蒙军败退。\r\n\r\n18；端平三年（1236年）十月，蒙古军中路猛攻蕲州（今湖北蕲春）。孟珙救援蕲州，蒙军败退。\r\n\r\n19；嘉熙元年（1237年），蒙古将领口温不花率军再度南侵，孟珙率军保卫黄州，击败渡江的蒙军。黄州保卫战一直打倒嘉熙二年春季，蒙军死伤十之七八，败退。\r\n\r\n20；嘉熙二年（1238年）十二月，孟珙等将策划收复襄樊之战。十二月，宋将张俊收复郢州（今湖北钟祥），贺顺收复荆门，刘全在冢头、樊城、郎神山三次击败蒙军。三年（1239年）初，收复信阳军。孟珙、江海各军进击樊城，投附蒙古的地方军阀刘廷美再次投宋，孟珙军收复樊城。四月，江海率宋军从荆门出发，沿途招集官民兵农。襄阳蒙将刘义向宋军投降。至此，宋军收复了整个荆襄地区。\r\n\r\n21；嘉熙三年（1239年）秋，蒙古大将塔海、秃雪率兵八十万入四川，攻破开州（今重庆开县），抵达万州（今重庆万县）长江北岸，并成功渡江，将宋军追击到川东重镇夔州（今重庆奉节）。孟珙率领万余湖北精兵来夔州助战。孟珙弟孟瑛以精兵5000驻松滋。孟璋率精兵2000驻守澧州，防施、黔两州的蒙古军队。孟璟、将刘义在清平（今湖北巴东）大败进攻施州的蒙军。孟璟于归州西大垭寨大败蒙军，蒙军丢盔弃甲后撤至夔州。这一站史称大垭寨之战。\r\n\r\n22；嘉熙四年（1240年）初，孟珙命令张英出随州，任义出信阳，焦进出襄阳，分路连续袭扰蒙军。同时派遣部将王坚偷袭顺阳，将蒙古军积聚的造船材料全部烧毁，又派遣部将张德、刘整分兵攻入蔡州，将敌人的物资仓库烧光。扼杀了蒙军的二次攻势，这次战役史称邓穰之战。\r\n\r\n23；南宋淳祐3年到开庆元年，钓鱼城战役（1243到1259），和入侵蒙军激战，保卫国土。\r\n\r\n24；淳佑四年（1244年）开始，孟珙利用窝阔台病死、蒙古陷入内乱的时机，又开始使用“打谷草”的策略。他多次派兵出动出击，攻打蒙古军在河南的要塞，焚毁敌人囤积的粮草，并屡获胜捷。孟珙的声名至此更加显赫，不少原先向蒙军投降的南宋将士纷纷来归。\r\n\r\n25；淳祐六年（1246年），原南宋镇北军将领、时任蒙古河南行省的范用吉背叛蒙古人，秘密向孟珙请求投降。孟珙急忙上书请求朝廷予以批准。宋理宗和众文臣害怕孟珙收复河南而功高震主，命孟珙告老致仕，孟珙辞官后抑郁而死。\r\n\r\n注；孟珙的郁愤而死，说明皇帝和文官、制度对武将的压制，导致宋军无法发挥战力，文官主和、皇帝猜疑导致的政策压制，武器粮草拖延供应，陷害或频繁调换将领导致“将不知兵、兵不知将”而形成军事调遣配合问题，是宋军复国的主要阻碍。宋朝廷和文官集团时常主降，出卖武将（害岳飞，害张浚，压制吴阶吴鳞，压制毕再遇北伐，压制陆游、辛弃疾等），一遇战争，武将和军民轻易形成“大势已去，朝廷又要卖国卖军”的思潮，是宋军将领和百姓容易投敌的形势基础，在此种形势下，入侵者只需招降，可以较容易的进行“以汉灭汉”。\r\n\r\n26；南宋咸淳8年到咸淳9年，襄樊之战，汉军死战6年，至死不屈，守将范天顺誓死不降，自缢身亡，城破后，牛富率百余勇士巷战，最后全部战死，牛富投火自尽。\r\n\r\n27；南宋祥兴2年（1279），崖山海战，宋军战败。陆秀夫背着少帝赵昺，投海自尽，许多忠臣追随其后，宋军10余万军民跳海自尽，誓死不降。这场战役反应出汉族誓死不屈的血性。也标志着华夏文明的第一次整体灭亡。\r\n\r\n\r\n明代；\r\n\r\n\r\n1；明洪武三年（1370），明太祖发布北伐文告，提出“驱逐胡虏，恢复中华，立纲陈纪，救济斯民”。从此开始对北元的进行长达30余年的八次军事讨伐，总称洪武北伐。洪武三年（1370）第一次北伐，徐达率主力攻到定西，大败扩廓帖木儿。扩廓帖木儿逃往和林(今蒙古乌兰巴托西南哈尔和林)。四月，元惠宗因痢疾死于应昌府。\r\n\r\n\r\n2；洪武五年（1372）第二次北伐。徐达中路军战败，敛军守塞（此处有疑点，主要是汉族历史被清代篡改严重，到底是一路防守一路主攻，还是战败被迫防守，语焉不详，很多地方说不通）。。。西路军冯胜出金兰（今甘肃榆中）趋甘肃，连战皆捷。颍川侯傅友德率骁骑5000败元将失刺罕于西凉（今甘肃武威）。进至永昌，再败元太尉朵儿只巴于忽剌罕口，获辎重牛马甚众。然后与冯胜主力会师，败元兵于扫林山（今甘肃酒泉北），斩首4000余级，擒其太尉锁纳儿加、平章管著等人。六月初三日，逼降元将上都驴，获吏民830余户。师抵亦集乃路(今内蒙古额济纳旗东南)，故元守将伯颜帖木儿举城降，继败元军于别笃山口(地址不详)，获元平章长加奴等270人及马驼牛羊十余万头。元岐王朵儿只班遁去。傅友德率兵追至瓜州（今甘肃安西）、沙州（今甘肃敦煌西北），又败元军，获金银印、马驼牛羊2万头。。。。东路军李文忠在口温(今内蒙古查干诺尔南)击退援军，获牛马辎重无算。在土剌河、阿鲁浑河(即鄂尔浑河，位于蒙古乌兰巴托西北)一带与元将蛮子哈刺章激战数日，元军败退，获人马以万计。\r\n\r\n3；洪武十三年（1380）第三次北伐，西平侯沐英率陕西明军进攻北元。在灵州（今宁夏灵武）发现敌踪，急行军七昼夜，突袭脱火赤所率蒙军，脱火赤未经抵抗即被俘虏。\r\n\r\n4；洪武十四年（1381）第四次北伐。颍川侯傅友德在灰山（今内蒙古宁城东南）大败元军，俘获元军人畜，在潢河（今西辽河）击溃元军，俘虏北元平章别里不花、太史文通等。西路军沐英攻取高州（今河北平泉境内）、嵩州（今内蒙古赤峰市西南）、全宁（今内蒙古翁牛特旗），俘虏北元知院李宣及其部众。\r\n\r\n5；洪武二十年（1387）第五次北伐、宋国公冯胜在庆州击败北元太尉纳哈出，活捉其平章果来，擒其子不兰奚，获人马而还。三月初一日，冯胜等将率师出松亭关，筑大宁（今内蒙古宁城）、宽河（今河北宽城）、会州（今河北平泉）、富峪（今河北平泉北）4城，驻兵大宁。五月二十一日，冯胜率大军直捣金山。纳哈出多次派使臣赴明军驻地，以献降为名，观明军虚实。在明军大军压境，步步进逼的情况下，纳哈出被迫投降。明得其军民24万余人，羊、马、驴、驼、辎重无数，最后肃清了元朝在辽东的势力。辽东从此成为明朝势力范围，后来成为奴儿干都司的一部分。\r\n\r\n6；洪武二十一年（1388）第六次北伐，在捕鱼儿海南岸，击杀北元太尉蛮子，击溃元军，脱古思帖木儿次子地保奴等64人、太子必里秃妃并公主等119人、吴王朵里只、代王达里麻、平章八兰等2994人全部被俘。蒙古军民77000人投降，俘获马47000匹、驼4800余头、牛羊10万余头、车3000余辆，以及北元宝玺、图书、金银印章等等，北元行政的根基被动摇。四月明军胜利班师。\r\n7；洪武二十三年（1390）第七次北伐，燕王朱棣在驻迤都（今蒙古苏赫巴托），击溃北元太尉乃儿不花，乃儿不花等被迫投降，燕王朱棣悉收其部落数万人、马驼牛羊数十万头而还。\r\n\r\n8；洪武二十九年（1396）第八次北伐。燕王朱棣在彻彻儿山大败北元军，索林帖木儿等数十人被俘虏。明军骑兵追击元军到兀良哈秃城，遇北元将领哈剌兀，再次大败北元军。\r\n注；（整个北伐过程，被有选择性的部分保留，可能是清代想利用此段历史，打压蒙族民族自尊心，或用次段历史，可以引发汉蒙民族仇恨，蒙元灭汉和洪武永乐北伐，是汉蒙两族轮番互杀的最好证据，汉蒙如果永久敌对下去，满族可以长期坐收渔利。）\r\n\r\n9；明永乐八年（1410），朱棣率明军在飞云山大战中击破五万元朝铁骑，蒙古本部的鞑靼向明朝称臣纳贡，随后明军直入擒狐山，在巨石上刻八字“翰海为镡，天山为锷”。\r\n\r\n10；永乐十年（1414），朱棣率明军在忽兰忽失温（今蒙古国温都尔汗西北）战瓦剌军，杀瓦剌王子10余人，瓦剌遣使谢罪。\r\n\r\n11；永乐十九年（1423），阿鲁台投明复叛，杀害明军都指挥王唤。朱棣率明军追杀其至阔滦海子北岸，阿鲁台遗弃牛马辎重，率部远遁。明军进击归附阿鲁台的兀良哈部，掳其牛羊10余万头。八月，胜利班师。\r\n\r\n12；永乐二十二年正月（1425），鞑靼阿鲁台部袭扰大同、开平，四月，朱棣征讨大军从京师出发，进抵隰宁(今内蒙古沽源南)，追杀阿鲁台到答兰纳木儿河，中途朱棣病逝，因此撤军。\r\n\r\n13：正德十二年。蒙古小王子率五万兵犯边。正德皇帝朱厚照在应州击败蒙古小王子，称应州大捷。此战后，蒙古岁岁犯边而不敢深入。\r\n\r\n14；嘉靖36年（1557）开始，戚继光对倭寇进行了岑港之战（1557）、台州之战（1561）、兴化之战（1563），仙游之战（1564），将进犯倭寇全部肃清。\r\n\r\n15；万历元年1573年蒙古小王子向明朝廷索要赏赐遭到拒绝，于是在喜峰口烧杀抢掠，被戚继光击退。\r\n\r\n16；明天启六年（1626）、后金天命十一年正月，努尔哈赤率13万后金军攻宁远（今辽宁兴城），明军守城，后金军伤亡17000人，努尔哈赤被炮弹炸成重伤，后伤势发作而死。此战史称宁远大捷。\r\n\r\n17；崇祯六年六月（1633），为迫使明朝开放对荷兰贸易，荷兰舰队突然袭击南澳、厦门，明军随即应战。九月，明水军郑芝龙（郑成功之父）以优势兵力，在福建金门料罗湾重创荷兰舰队及刘香海盗联军。崇祯十二年，明郑再败荷兰舰队，夺回东亚、东南亚全部制海权。\r\n\r\n18；南明永历十五年（1661），郑成功收复台湾，结束了荷兰在台湾的殖民统治，这是中华民国之前最后一次维护汉族利益的战争。\r\n\r\n\r\n\r\n\r\n      以上所有汉族的自卫战争，都被伪中华文人抹黑过，所有战果都被质疑是否存在。特别是应州大捷、宁远大捷等明代战役，被清代文人抹黑尤其严重，在历史记录上也篡改严重，甚至有说应州一战伤亡才十几人，在卖国文人眼里，打仗是儿戏啊？鞑子年年南下，年年和明军发生战争，万人级别的攻防战时有发生，蒙古小王子带军来袭，大费周章筹集几万大军，结果就为来一场小打小闹？伤亡几十个就撤了？瞎编都不符合逻辑啊。\r\n\r\n\r\n\r\n\r\n\r\n      自从华夏汉族立族以来，汉族和游牧民族的战争，每次都是汉军用血换来内地的和平。有些文人，享受着和平环境，却污蔑守护你的汉军？这种狗屁文人活着有啥用？有篡改历史的闲心，不如上战场试试，死在敌族手里，也算你死得其所，省的没事儿污蔑汉军战果。\r\n\r\n\r\n。。。。\r\n\r\n我们不知道汉族驱除鞑虏的记录被删改了多少。\r\n\r\n\r\n我们只知道，汉族繁衍存续了4000多年，如今，我们还在这里。\r\n自满清以来，满清统治者为了灭杀汉族“自信自强自卫”的民族性格，把汉族对外反击战的记录删改大半。\r\n\r\n\r\n满清在篡改汉族历史的过程中，把游牧部落描绘的聪明英勇，汉族则是昏君奸臣。\r\n大清四库版《二十四史》俨然一部抹黑汉族的小说，汉族是反面人物，是腐败的内讧者。游牧民族是正面人物。\r\n\r\n\r\n经过一通瞎编乱改，《二十四史》只剩下点小故事，国家兴亡的真正经验那是一点也没有了。\r\n\r\n\r\n有人拿清朝说事儿，说汉族战败，跪下求活。这里，我们必须再次强调，满清没有灭明，灭明的是李自成。满清入关，是吴三桂打开山海关，不是打的。满清占领天下，是以汉灭汉，是一种政府和人民极端敌对情况下，入侵者利用人民的一种手段，这种政治手段，跟战争没什么关系。就算满人得了江山，少量满军，也不敢杀尽天下汉人。人都不是傻子，不会伸头等死。满族越杀汉人，汉人起义会越多。\r\n\r\n汉人不是不能打，汉人很能打,汉打匈奴，唐打突厥就是证明。宋明的悲剧之所以发生并不是汉人不能打，而是统治者思想有病。假如把宋朝的统治者换成汉朝的统治者照样灭掉蒙古帝国；把明朝的统治者换成唐朝统治者照样灭掉满清、沙俄。\r\n\r\n在古代这五千年，从没有一个民族像汉族这样强大。不但作战能力强、而且人数众多、且具有经济、科技等绝对优势。若论综合实力汉族绝对排第一。所需要防范的就是宋明那样的傻逼领导。一头绵羊领导的狮群打不过一头狮子领导的羊群。\r\n\r\n汉人要活下去，最重要的事就是，汉族必须一直赢。\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/11/2745208658.png', '0', '1', null, 'post', 'publish', null, '2', '1', '1', '1', '0', '1', '2691', '462', '0');
INSERT INTO `typecho_contents` VALUES ('62', '微信截图_20191122095834.png', '微信截图_20191122095834-png', '1574387949', '1574387949', 'a:5:{s:4:\"name\";s:31:\"微信截图_20191122095834.png\";s:4:\"path\";s:35:\"/usr/uploads/2019/11/2745208658.png\";s:4:\"size\";i:589707;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '61', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('64', 'e61190ef76c6a7ef95e0b421f5faaf51f2de66f6.jpg', 'e61190ef76c6a7ef95e0b421f5faaf51f2de66f6-jpg', '1575703734', '1575703734', 'a:5:{s:4:\"name\";s:44:\"e61190ef76c6a7ef95e0b421f5faaf51f2de66f6.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/12/1699722914.jpg\";s:4:\"size\";i:111216;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '63', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('65', '微信公众号入门操作', '65', '1575987420', '1577197716', '<!--markdown-->![3146133028.jpg][1]   \r\n <?php\r\n    \r\n    namespace Home\\Controller;\r\n    \r\n    use Think\\Controller;\r\n    \r\n    class ApiController extends Controller\r\n    {\r\n        const TOKEN = \'phpmaster\';\r\n        // 微信公众号appid appsecret\r\n        const APPID = \"wx5d700b1fd7c6bb211\";\r\n        const APPSECRET = \"18fc068a13214bae1fc94255732f03022\";\r\n    \r\n        public function index()\r\n        {\r\n            if(isset($_GET[\'echostr\'])){\r\n                if($this->checkWechat()){\r\n                    echo $_GET[\'echostr\'];\r\n                }\r\n            }else{\r\n                 $this->responseMsg();\r\n            }\r\n    \r\n        }\r\n    \r\n        //响应\r\n        public function responseMsg()\r\n        {\r\n            //$str = $GLOBALS[\"HTTP_RAW_POST_DATA\"];// 7-\r\n            $postStr = file_get_contents(\"php://input\");//php 7+\r\n    //        file_put_contents(\"a.txt\", $postStr);\r\n            //将xml格式的字符串转成对象\r\n    //        libxml_disable_entity_loader(true);\r\n            $this->postObj = simplexml_load_string($postStr, \'SimpleXMLElement\', LIBXML_NOCDATA);\r\n            // 用户发送的数据类型\r\n            $MsgType = $this->postObj->MsgType;\r\n            switch ($MsgType) {\r\n                // 事件\r\n                case \'event\':\r\n                    $this->sendEvent();\r\n                    break;\r\n                // 文本消息\r\n                case \'text\':\r\n                    $Content = $this->postObj->Content;\r\n                    // 查询天气\r\n                    $this->sendText($Content);\r\n                    break;\r\n                // 文本消息\r\n                case \'voice\':\r\n                    $this->sendVoice($this->getUserFile(\'voice\', \'.\' . $this->postObj->Format));\r\n                    // 回复语音识别的内容\r\n    //                $this->sendText($this->postObj->Recognition);\r\n    \r\n                    break;\r\n                case \'image\':\r\n                    //  1.如果用户发送了图片，获取用户发送视频的media_id\r\n                    //  2.通过media_id 调用获取临时素材接口 把图片下载到本地\r\n                    //  3.通过调用上传临时素材接口 上传图片文件并返回mediaid\r\n    \r\n                    $this->sendImg($this->getUserFile(\'image\', \'.jpg\'));\r\n                    break;\r\n                case \'video\':\r\n                    //  1.如果用户发送了视频，获取用户发送视频的media_id\r\n                    //  2.通过media_id 调用获取临时素材接口 把视频下载到本地\r\n                    //  3.通过调用上传临时素材接口 上传视频文件并返回mediaid\r\n    \r\n                    $this->sendVideo($this->getUserFile(\'video\', \'.mp4\'));\r\n                    break;\r\n            }\r\n        }\r\n    \r\n        // 事件操作\r\n        public function sendEvent()\r\n        {\r\n            switch ($this->postObj->Event) {\r\n                // 订阅 关注\r\n                case \'subscribe\':\r\n                    $this->sendText(\'非常感谢您的关注，公众号功能还在持续开发完善中，敬请期待！有任何问题请点击帮助按钮！\');\r\n                    break;\r\n                // 取消订阅 取消关注\r\n                case \'unsubscribe\':\r\n                    break;\r\n                case \"CLICK\":\r\n                    // 点击了菜单\r\n                    $this->sendClick();\r\n                    break;\r\n            }\r\n        }\r\n    \r\n        public function sendClick()\r\n        {\r\n            switch ($this->postObj->EventKey) {\r\n                case \'joke\':\r\n                    break;\r\n                case \'girl\':\r\n                    $this->sendImg();\r\n                    break;\r\n                case \'video\':\r\n                    $this->sendVideo();\r\n                    break;\r\n                case \'help\':\r\n                    $Content = \"公众号功能持续开发完善中\\n\";\r\n                    $Content .= \"现有功能1.发文字，图片，语音，视频，原路返回（发的什么，回复什么）\\n\";\r\n    //                $Content .= \"公众号功能持续开发完善中\\n\";\r\n    //                $Content = \"公众号功能持续开发完善中\\n\";\r\n                    $this->sendText($Content);\r\n                    break;\r\n            }\r\n        }\r\n    \r\n        // 发送文本\r\n        public function sendText($content)\r\n        {\r\n            // 被动回复给用户消息\r\n            $str = \"<xml>\r\n                      <ToUserName><![CDATA[%s]]></ToUserName>\r\n                      <FromUserName><![CDATA[%s]]></FromUserName>\r\n                      <CreateTime>%s</CreateTime>\r\n                      <MsgType><![CDATA[text]]></MsgType>\r\n                      <Content><![CDATA[%s]]></Content>\r\n                   </xml>\";\r\n            $ToUserName = $this->postObj->ToUserName;\r\n            $FromUserName = $this->postObj->FromUserName;\r\n            $CreateTime = time();\r\n            $resultStr = sprintf($str, $FromUserName, $ToUserName, $CreateTime, $content);\r\n            echo $resultStr;\r\n        }\r\n    \r\n        // 发送语音\r\n        public function sendVoice($media_id)\r\n        {\r\n            // 被动回复给用户消息\r\n            $str = \"<xml>\r\n                      <ToUserName><![CDATA[%s]]></ToUserName>\r\n                      <FromUserName><![CDATA[%s]]></FromUserName>\r\n                      <CreateTime>%s</CreateTime>\r\n                      <MsgType><![CDATA[voice]]></MsgType>\r\n                      <Voice>\r\n                        <MediaId><![CDATA[%s]]></MediaId>\r\n                      </Voice>\r\n                    </xml>\";\r\n            $ToUserName = $this->postObj->ToUserName;\r\n            $FromUserName = $this->postObj->FromUserName;\r\n            $CreateTime = time();\r\n            $resultStr = sprintf($str, $FromUserName, $ToUserName, $CreateTime, $media_id);\r\n            echo $resultStr;\r\n        }\r\n    \r\n        public function sendImg($media_id)\r\n        {\r\n            $str = \"<xml>\r\n                      <ToUserName><![CDATA[%s]]></ToUserName>\r\n                      <FromUserName><![CDATA[%s]]></FromUserName>\r\n                      <CreateTime>%s</CreateTime>\r\n                      <MsgType><![CDATA[image]]></MsgType>\r\n                      <Image>\r\n                        <MediaId><![CDATA[%s]]></MediaId>\r\n                      </Image>\r\n                   </xml>\";\r\n            $ToUserName = $this->postObj->ToUserName;\r\n            $FromUserName = $this->postObj->FromUserName;\r\n            $CreateTime = time();\r\n            $resultStr = sprintf($str, $FromUserName, $ToUserName, $CreateTime, $media_id);\r\n            echo $resultStr;\r\n        }\r\n    \r\n        // 发送图文\r\n        public function sendNews()\r\n        {\r\n            $str = \"<xml>\r\n                      <ToUserName><![CDATA[%s]]></ToUserName>\r\n                      <FromUserName><![CDATA[%s]]></FromUserName>\r\n                      <CreateTime>%s</CreateTime>\r\n                      <MsgType><![CDATA[news]]></MsgType>\r\n                      <ArticleCount>%s</ArticleCount>\r\n                      <Articles>\r\n                        <item>\r\n                          <Title><![CDATA[%s]]></Title>\r\n                          <Description><![CDATA[%s]]></Description>\r\n                          <PicUrl><![CDATA[%s]]></PicUrl>\r\n                          <Url><![CDATA[%s]]></Url>\r\n                        </item>\r\n                          <item>\r\n                          <Title><![CDATA[%s]]></Title>\r\n                          <Description><![CDATA[%s]]></Description>\r\n                          <PicUrl><![CDATA[%s]]></PicUrl>\r\n                          <Url><![CDATA[%s]]></Url>\r\n                        </item>\r\n                      </Articles>\r\n                   </xml>\";\r\n            $ToUserName = $this->postObj->ToUserName;\r\n            $FromUserName = $this->postObj->FromUserName;\r\n            $CreateTime = time();\r\n            $count = 2;\r\n            // sprintf 格式化输出\r\n            $t1 = \"CCTV第一次提到了汉服断代的原因！\";\r\n            $d1 = \"CCTV法制频道第一次提到了汉服断代的原因历史告诉我们的是正视它 而不是回避大国之殇：汉服断代简史\";\r\n            $pic1 = \"http://www.phpmaster.cn/usr/uploads/2019/11/2408243032.jpg\";\r\n            $url1 = \"http://www.phpmaster.cn/index.php/archives/59/\";\r\n            $t2 = \"胡汉中国的定型期——清帝国(作为汉人,你不可不知)\";\r\n            $d2 = \"中国人有中国人的形象，即便是元帝国，占领全中国，让南人为第四等\";\r\n            $pic2 = \"http://www.phpmaster.cn/usr/uploads/2019/10/3343280148.jpg\";\r\n            $url2 = \"http://www.phpmaster.cn/index.php/archives/37/\";\r\n            $resultStr = sprintf($str, $FromUserName, $ToUserName, $CreateTime, $count, $t1, $d1, $pic1, $url1, $t2, $d2, $pic2, $url2);\r\n            echo $resultStr;\r\n        }\r\n    \r\n        //发送视频\r\n        public function sendVideo($media_id)\r\n        {\r\n            $str = \"<xml>\r\n              <ToUserName><![CDATA[%s]]></ToUserName>\r\n              <FromUserName><![CDATA[%s]]></FromUserName>\r\n              <CreateTime>%s</CreateTime>\r\n              <MsgType><![CDATA[video]]></MsgType>\r\n              <Video>\r\n                <MediaId><![CDATA[%s]]></MediaId>\r\n                <Title><![CDATA[%s]]></Title>\r\n                <Description><![CDATA[%s]]></Description>\r\n              </Video>\r\n            </xml>\r\n            \";\r\n            $ToUserName = $this->postObj->ToUserName;\r\n            // 用户\r\n            $FromUserName = $this->postObj->FromUserName;\r\n            // 用户发送的内容\r\n            // 公众号回复给用户的时间\r\n            $CreateTime = time();\r\n            $Title = \"视频\";\r\n            $Description = \"原路返回的视频\";\r\n            $resultStr = sprintf($str, $FromUserName, $ToUserName, $CreateTime, $media_id, $Title, $Description);\r\n            echo $resultStr;\r\n        }\r\n    \r\n        //   //  1.如果用户发送了视频，获取用户发送视频的media_id\r\n        public function getUserFile($type, $ext)\r\n        {\r\n            $access_token = $this->getAccessToken();\r\n            $MediaId = $this->postObj->MediaId;\r\n            $url = \"https://api.weixin.qq.com/cgi-bin/media/get?access_token=\" . $access_token . \"&media_id=\" . $MediaId;\r\n            $res = curl($url);\r\n            $filename = \"./\" . $this->postObj->MsgId . $ext;\r\n            //  2.通过media_id 调用获取临时素材接口 把视频下载到本地\r\n            // 保存视频\r\n            $this->saveWeiXinFile($filename, $res);\r\n    \r\n            $media_id = $this->getMediaId($type, $filename);\r\n            unlink(realpath($filename));\r\n            return $media_id;\r\n        }\r\n    \r\n        //  //  2.通过media_id 调用获取临时素材接口 把视频下载到本地\r\n        public function saveWeiXinFile($filename, $filecontent)\r\n        {\r\n            $local_file = fopen($filename, \'w\');\r\n            if (false !== $local_file) {\r\n                if (false !== fwrite($local_file, $filecontent)) {\r\n                    fclose($local_file);\r\n                }\r\n            }\r\n        }\r\n    \r\n        // 获取access_token\r\n        public function getAccessToken()\r\n        {\r\n            $arr = M(\'token\')->find(1);\r\n            if (time() > $arr[\'expire_time\']) {\r\n                // 过期了\r\n                $access_token = $this->createAccessToken();\r\n            } else {\r\n                $access_token = $arr[\'access_token\'];\r\n            }\r\n            echo $access_token;\r\n            return $access_token;\r\n        }\r\n    \r\n        // 生成access_token\r\n        public function createAccessToken()\r\n        {\r\n            $url = \"https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=\" . self::APPID . \"&secret=\" . self::APPSECRET;\r\n            $json_str = curl($url);\r\n            $arr = json_decode($json_str, true);\r\n            $access_token = $arr[\'access_token\'];\r\n            // 更新数据库\r\n            M(\'token\')->where([\'id\' => 1])->save([\'access_token\' => $access_token, \'expire_time\' => time() + 7000]);\r\n            return $access_token;\r\n        }\r\n    \r\n    \r\n        // 用户绑定 用户关注公众号后添加记录\r\n        public function bindNotice()\r\n        {\r\n            $access_token = $this->getAccessToken();\r\n            // 用户\r\n            $FromUserName = $this->postObj->FromUserName;\r\n    \r\n            $url = \"https://api.weixin.qq.com/cgi-bin/user/info?access_token=\" . $access_token . \"&openid=\" . \"$FromUserName\" . \"&lang=zh_CN\";\r\n            $json_str = curl($url);\r\n    //        $userInfo = json_decode($json_str,true);\r\n            // 用户信息存储数据表\r\n        }\r\n    \r\n        // 创建自定义菜单\r\n        public function createMenu()\r\n        {\r\n            $access_token = $this->getAccessToken();\r\n            $url = \"https://api.weixin.qq.com/cgi-bin/menu/create?access_token=\" . $access_token;\r\n            $menu[\'button\'][0][\'name\'] = \'汉服\';\r\n    //        $menu[\'button\'][0][\'sub_button\'][0]= [\'name\'=>\'看美女\',\'type\'=>\'click\',\'key\'=>\'girl\'];\r\n    //        $menu[\'button\'][0][\'sub_button\'][1]= [\'name\'=>\'看视频\',\'type\'=>\'click\',\'key\'=>\'video\'];\r\n    //        $menu[\'button\'][0][\'sub_button\'][2]= [\'name\'=>\'博客\',\'type\'=>\'view\',\'url\'=>\'http://www.phpmaster.cn\'];\r\n            $menu[\'button\'][0][\'sub_button\'][0] = [\'name\' => \'扫一扫\', \'type\' => \'scancode_push\', \'key\' => \'saoyisao\'];\r\n            $menu[\'button\'][0][\'sub_button\'][1] = [\'name\' => \'拍照\', \'type\' => \'pic_sysphoto\', \'key\' => \'photo\'];\r\n    \r\n            $menu[\'button\'][1][\'name\'] = \'帮助中心\';\r\n            $menu[\'button\'][1][\'type\'] = \'click\';\r\n            $menu[\'button\'][1][\'key\'] = \'help\';\r\n    \r\n            $menu[\'button\'][2][\'name\'] = \'我的博客\';\r\n            $menu[\'button\'][2][\'type\'] = \'view\';\r\n            $menu[\'button\'][2][\'url\'] = \'http://www.phpmaster.cn\';\r\n            //中文编码问题\r\n            $json = json_encode($menu, JSON_UNESCAPED_UNICODE);\r\n            $bool = curl($url, $json, 1);\r\n            var_dump($bool);\r\n        }\r\n    \r\n        // 调用临时素材接口\r\n        public function getMediaId($type, $filename)\r\n        {\r\n            $access_token = $this->getAccessToken();\r\n            $url = \"https://api.weixin.qq.com/cgi-bin/media/upload?access_token=\" . $access_token . \"&type=$type\";\r\n    \r\n            if (class_exists(\'\\CURLFile\')) {\r\n                $data = array(\'media\' => new \\CURLFile(realpath($filename)));\r\n            } else {\r\n                $data = array(\'media\' => \'@\' . realpath($filename));\r\n            }\r\n            $json_str = curl($url, $data, 1);\r\n            $obj = json_decode($json_str);\r\n            $media_id = $obj->media_id;\r\n            return $media_id;\r\n        }\r\n    \r\n        public function checkWechat()\r\n        {\r\n            //1.接收 由微信服务器转出的参数\r\n            $signature = $_GET[\"signature\"];\r\n            $timestamp = $_GET[\"timestamp\"];\r\n            $nonce = $_GET[\"nonce\"];\r\n            // 2.将接收的三个值放入数组\r\n            $tmpArr = [self::TOKEN, $timestamp, $nonce];\r\n            // 3.进行字典排序 从小到大 1.2.3. a.b.c\r\n            sort($tmpArr, SORT_STRING);\r\n            // 4.合并成一个字符串\r\n            $tmpArr = implode($tmpArr);\r\n            // 5.sha1加密\r\n            $tmpStr = sha1($tmpArr);\r\n            // 6.加密之后的结果 同 微信传递过来的 signature 比对\r\n            if ($tmpStr == $signature) {\r\n                return true;\r\n            } else {\r\n                return false;\r\n            }\r\n        }\r\n    \r\n    }\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/12/3608372666.jpg', '0', '1', null, 'post', 'publish', null, '2', '1', '1', '1', '0', '1', '4142', '433', '0');
INSERT INTO `typecho_contents` VALUES ('66', '手册', 'manual', '1576400220', '1583509301', '<!--markdown-->[CodeIgniter 用户指南][1] \r\n\r\n[ptyhon3中文手册][2]\r\n\r\n[flask框架][3]\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/wiki/ci\r\n  [2]: http://www.phpmaster.cn/wiki/python_online\r\n  [3]: http://www.phpmaster.cn/wiki/flask/index.html', '0', '1', null, 'page', 'hidden', null, '0', '1', '1', '1', '0', '1', '347', '75', '0');
INSERT INTO `typecho_contents` VALUES ('67', '3146133028.jpg', '3146133028-jpg', '1577197701', '1577197701', 'a:5:{s:4:\"name\";s:14:\"3146133028.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/12/3608372666.jpg\";s:4:\"size\";i:20236;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '65', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('68', 'timg.jpg', 'timg-jpg', '1577260095', '1577260095', 'a:5:{s:4:\"name\";s:8:\"timg.jpg\";s:4:\"path\";s:34:\"/usr/uploads/2019/12/844789042.jpg\";s:4:\"size\";i:34344;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '69', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('69', '《锦鲤抄》-云の泣、银临', '69', '1577260080', '1577260254', '<!--markdown-->![timg.jpg][1]\r\n[Meting]\r\n[Music server=\"netease\" id=\"28138493\" type=\"song\"/]\r\n[/Meting]\r\n\r\n文案：\r\n\r\n宁武皇仁光九年锦文轩刻本《异闻录》载：\r\n扶桑画师浅溪，居泰安，喜绘鲤。院前一方荷塘，锦鲤游曳，溪常与嬉戏。\r\n其时正武德之乱，潘镇割据，战事频仍，魑魅魍魉，肆逆于道。兵戈逼泰安，街邻皆逃亡，独溪不舍锦鲤，未去。\r\n是夜，院室倏火。有人入火护溪，言其本鲤中妖，欲取溪命，却生情愫，遂不忍为之。翌日天明，火势渐歇，人已不见。\r\n溪始觉如梦，奔塘边，但见池水干涸，莲叶皆枯，塘中鲤亦不知所踪。\r\n自始至终，未辨眉目，只记襟上层迭莲华，其色魅惑，似血着泪。\r\n后有青岩居士闻之，叹曰：魑祟动情，必作灰飞。犹蛾之投火耳，非愚，乃命数也。\r\n\r\n《锦鲤抄》\r\n\r\n作词/文案：慕清明\r\n作曲/和声：银临\r\n编曲/混音：灰原穷\r\n二胡：弹棉花的GG\r\n演唱：云の泣 银临\r\n海报：青海草原\r\n\r\n【银】\r\n蝉声陪伴着行云流浪\r\n回忆开始后安静遥望远方\r\n荒草覆没的古井枯塘\r\n匀散一缕过往\r\n【云】 \r\n晨曦惊扰了陌上新桑\r\n风卷起庭前落花穿过回廊\r\n浓墨追逐着情绪流淌\r\n染我素衣白裳\r\n\r\n【银】阳光微凉 【云】琴弦微凉 【合】风声疏狂 人间仓皇\r\n【银】呼吸微凉 【云】心事微凉 【合】流年匆忙 对错何妨\r\n\r\n【云】你在尘世中辗转了千百年\r\n却只让我看你最后一眼\r\n【银】火光描摹容颜燃尽了时间\r\n别留我一人 孑然一身 凋零在梦境里面\r\n\r\n(music)\r\n【银】\r\n萤火虫愿将夏夜遗忘\r\n如果终究要挥别这段时光\r\n裙袂不经意沾了荷香\r\n从此坠入尘网\r\n【云】\r\n屐齿轻踩着烛焰摇晃\r\n所有喧嚣沉默都描在画上\r\n从惊蛰一路走到霜降\r\n泪水凝成诗行\r\n\r\n【云】灯花微凉 【银】笔锋微凉 【合】难绘虚妄 难解惆怅\r\n【云】梦境微凉 【银】情节微凉 【合】迷离幻象 重叠忧伤\r\n\r\n【合】原来诀别是因为深藏眷恋\r\n你用轮回换我枕边月圆\r\n我愿记忆停止在枯瘦指尖\r\n随繁花褪色 尘埃散落 渐渐地渐渐搁浅\r\n\r\n\r\n【云】多年之后 我又梦到那天\r\n【银】画面遥远 恍惚细雨绵绵\r\n\r\n【合】如果来生太远寄不到诺言\r\n不如学着放下许多执念\r\n以这断句残篇向岁月吊唁\r\n老去的当年 水色天边 有谁将悲欢收殓\r\n\r\n【云】蝉声陪伴着行云流浪\r\n回忆的远方\r\n\r\n-终-\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/12/844789042.jpg', '0', '1', null, 'post', 'publish', null, '2', '1', '1', '1', '0', '1', '3095', '351', '0');
INSERT INTO `typecho_contents` VALUES ('70', '2878420530 (1).jpg', '2878420530-1-jpg', '1577352984', '1577352984', 'a:5:{s:4:\"name\";s:18:\"2878420530 (1).jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/12/2506123306.jpg\";s:4:\"size\";i:37613;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '71', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('71', 'python中使用xpath实现段子网-“段子来了”分类下所有页面的段子和图片', '71', '1577352960', '1577354682', '<!--markdown--> ![2878420530 (1).jpg][1]\r\n![4168172313.jpg][2]\r\n   \r\n\r\n    # 导入xpath模块 用于解析页面数据\r\n        from lxml import etree\r\n        # 导入系统模块 用于自动创建目录\r\n        import os\r\n        # 导入requests模块用于发送http请求\r\n        import requests\r\n        # 发起请求获取数据\r\n        def get_content(url):\r\n            # user-agent 设置头信息 模拟浏览器访问\r\n            headers = {\r\n                \"User-Agent\": \"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36\"\r\n            }\r\n            return requests.get(url=url, headers=headers)\r\n        # 总页码\r\n        url = \"http://duanziwang.com/category/%E6%AE%B5%E5%AD%90%E6%9D%A5%E4%BA%86/1\"\r\n        res = get_content(url=url)\r\n        etree2 = etree.HTML(res.text)\r\n        page = etree2.xpath(\'//span[@class=\"page-number\"]/text()\')[0]\r\n        p = int(page.split()[-2])\r\n        \r\n        # 循环所有的页面\r\n        for i in range(1,p+1):\r\n            print(\"正在保存第%s页数据。。。。\" % str(i))\r\n            # 每一页的url\r\n            url = \"http://duanziwang.com/category/%E6%AE%B5%E5%AD%90%E6%9D%A5%E4%BA%86/\"+str(i)\r\n            response = get_content(url)\r\n            etree1 = etree.HTML(response.text)\r\n            article_list = etree1.xpath(\"//article\")\r\n        \r\n            # 打开文件 以当前页面为文件名 把当前页面的所有数据保存到一个文件中\r\n            f = open(\"./img/%sduanzi.txt\"%str(i),\'w\',encoding=\"utf-8\")\r\n            # 循环遍历每一页的所有数据\r\n            for art in article_list:\r\n                # 打开一张图片 每次for循环执行一次 即可保存一张图片\r\n                # 标题\r\n                title = art.xpath(\"./div/h1/a/text()\")[0]\r\n                # 写入一个标题\r\n                f.write(title+\"\\n\\n\")\r\n                print(title+\"保存成功。。。\")\r\n                # 如果获取的数据中p标签下有图片的话\r\n                if len(art.xpath(\"./div[2]/p/img\")):\r\n                    # 把每一页的图片都保存到当页的目录中\r\n                    # 如果当页有图片的话，才去创建目录 用于保存当页的图片\r\n                    my_dir = \'./img/\' + str(i) + \"/\"  # ./img/1 ./img/2\r\n                    # 如果目录不存在的话则创建以当前页码为名称的目录\r\n                    if not os.path.isdir(my_dir):\r\n                        os.makedirs(my_dir)\r\n                    # 获取当前记录中的图片地址\r\n                    pic_url = art.xpath(\"./div[2]/p/img/@src\")[0]\r\n                    # 截取其中的图片名称\r\n                    pic_name = pic_url.split(\"/\")\r\n                    # 取出图片名称\r\n                    pic_name = pic_name[-1]\r\n                    # 图片新的存储路径为 目录+图片名称\r\n                    pic_name = my_dir + pic_name\r\n                    # 发送请求\r\n                    pic_response = get_content(pic_url)\r\n                    # 获取二进制数据\r\n                    pic_content = pic_response.content\r\n                    # 打开一张图片\r\n                    with open(pic_name, \'wb\') as f1:\r\n                        # 写入一张图片\r\n                        f1.write(pic_content)\r\n                        # 关闭图片资源\r\n                        f1.close()\r\n                        print(pic_name + \"下载完成。。。。\")\r\n            # 关闭文件\r\n            f.close()\r\n            print(\"第%s页数据保存完毕\"%str(i))\r\n        print(\"程序执行结束。。。\")\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/12/2506123306.jpg\r\n  [2]: http://www.phpmaster.cn/usr/uploads/2019/12/2138788833.jpg', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '2124', '301', '0');
INSERT INTO `typecho_contents` VALUES ('72', '4168172313.jpg', '4168172313-jpg', '1577353195', '1577353195', 'a:5:{s:4:\"name\";s:14:\"4168172313.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2019/12/2138788833.jpg\";s:4:\"size\";i:43257;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '2', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '71', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('73', 'xpath在爬虫中的使用流程', '73', '1577354100', '1577354172', '<!--markdown-->![4168172313.jpg][2]\r\n- 1.下载：pip install lxml\r\n- 2.导包：from lxml import etree\r\n- 3.创建etree对象进行指定数据的解析\r\n    - 本地：etree=etree.parse(\'本地文件路径\')\r\n           etree.xpath(\'xpath表达式\')\r\n    - 网络：etree=etree.HTML(\'网络请求到的页面数据\')\r\n           etree.xpath(\'xpath表达式\')\r\n\r\n常用的xpath表达式：\r\n1.属性定位：\r\n#找到class属性值为song的div标签\r\n//div[@class=\"song\"] \r\n2.层级&索引定位：\r\n#找到class属性值为tang的div的直系子标签ul下的第二个子标签li下的直系子标签a\r\n//div[@class=\"tang\"]/ul/li[2]/a\r\n3.逻辑运算：\r\n#找到href属性值为空且class属性值为du的a标签\r\n//a[@href=\"\" and @class=\"du\"]\r\n4.模糊匹配：\r\n //div[contains(@class, \"ng\")] //div[starts-with(@class, \"ta\")] 取文本：\r\n# /表示获取某个标签下的文本内容\r\n# //表示获取某个标签下的文本内容和所有子标签下的文本内容\r\n//div[@class=\"song\"]/p[1]/text()\r\n//div[@class=\"tang\"]//text()\r\n5.取属性：\r\n //div[@class=\"tang\"]//li[2]/a/@href\r\n\r\n- xpath插件：就可以直接将xpath表达式作用于浏览器的网页当中\r\n- 安装：更多工具-》扩展程序-》开启右上角的开发者模式-》xpath插件拖动到页面即可\r\n- 快捷键：\r\n    - 开启和关闭xpath插件：ctrl+shitf+x\r\n  [2]: http://www.phpmaster.cn/usr/uploads/2019/12/534488989.jpg', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '2093', '312', '0');
INSERT INTO `typecho_contents` VALUES ('74', '4168172313.jpg', '4168172313-jpg-1', '1577354159', '1577354159', 'a:5:{s:4:\"name\";s:14:\"4168172313.jpg\";s:4:\"path\";s:34:\"/usr/uploads/2019/12/534488989.jpg\";s:4:\"size\";i:43257;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '73', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('75', '微信截图_20191226180359.png', '微信截图_20191226180359-png', '1577354649', '1577354649', 'a:5:{s:4:\"name\";s:31:\"微信截图_20191226180359.png\";s:4:\"path\";s:35:\"/usr/uploads/2019/12/2865775890.png\";s:4:\"size\";i:5250;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '76', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('76', 'python中使用re正则实现段子网“一句话段子”分类下所有段子', '76', '1577354640', '1577354700', '<!--markdown-->  ![微信截图_20191226180359.png][1] \r\n\r\n    # 导入requests 模块\r\n    import requests\r\n    # re 正则模块\r\n    import re\r\n    import os\r\n    import datetime\r\n    # 1.创建当前日期为名称的目录\r\n    today=datetime.date.today()\r\n    my_dir = \'./data/\'+str(today)\r\n    # isdir 判断目录是否存在\r\n    # 创建目录前先判断目录是否存在，如果不存在则创建\r\n    if not os.path.isdir(my_dir):\r\n        os.makedirs(my_dir)\r\n    \r\n    # 2.根据url获取对应的页面数据\r\n    def get_content(url):\r\n        headers = {\r\n            \"User-Agent\": \"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36\"\r\n        }\r\n        return requests.get(url=url, headers=headers)\r\n    \r\n    # 3.从第一页取出一共多少页\r\n    url = \"http://duanziwang.com/category/%E4%B8%80%E5%8F%A5%E8%AF%9D%E6%AE%B5%E5%AD%90/\"\r\n    page_content = re.findall(\'<span class=\"page-number\">(.*)</span>\',get_content(url).text)\r\n    total_pages = int(page_content[0].split()[-2])\r\n    \r\n    filename = my_dir + \"/all.txt\"\r\n    f = open(filename, \'w\', encoding=\"utf-8\")\r\n    # 4.循环遍历每一页的数据\r\n    for i in range(1,total_pages):\r\n        url = \"http://duanziwang.com/category/%E4%B8%80%E5%8F%A5%E8%AF%9D%E6%AE%B5%E5%AD%90/\"+str(i)\r\n        page_text = get_content(url)\r\n        content = re.findall(\'<h1 class=\"post-title\"><a.*>(.*)</a></h1>\',page_text)\r\n        for k,v in enumerate(content):\r\n            f.write(str(k)+v+\"\\n\")\r\n            print(\"抓取:\"+v)\r\n    \r\n        print(\"第%d页数据抓取结束。。。\"%i)\r\n    print(\"所有页面数据已经完全获取\")\r\n    f.close()\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2019/12/2865775890.png', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '2365', '487', '0');
INSERT INTO `typecho_contents` VALUES ('77', '微信图片_20200310101933.jpg', '微信图片_20200310101933-jpg', '1583806799', '1583806799', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310101933.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/03/2442654498.jpg\";s:4:\"size\";i:28646;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('78', '2.jpg', '2-jpg-1', '1583806866', '1583806866', 'a:5:{s:4:\"name\";s:5:\"2.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/03/2619276164.jpg\";s:4:\"size\";i:53599;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '2', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('79', '1.jpg', '1-jpg-1', '1583806875', '1583806875', 'a:5:{s:4:\"name\";s:5:\"1.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/03/1658598671.jpg\";s:4:\"size\";i:29675;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '3', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('80', '微信图片_20200310102137.jpg', '微信图片_20200310102137-jpg', '1583806912', '1583806912', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310102137.jpg\";s:4:\"path\";s:34:\"/usr/uploads/2020/03/210019693.jpg\";s:4:\"size\";i:37085;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '4', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('81', '微信图片_20200310102258.jpg', '微信图片_20200310102258-jpg', '1583806995', '1583806995', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310102258.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/03/1858748908.jpg\";s:4:\"size\";i:26194;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '5', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('82', '微信图片_20200310102329.jpg', '微信图片_20200310102329-jpg', '1583807033', '1583807033', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310102329.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/03/1819524221.jpg\";s:4:\"size\";i:125383;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '6', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('83', '微信图片_20200310102333.jpg', '微信图片_20200310102333-jpg', '1583807041', '1583807041', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310102333.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/03/2349705336.jpg\";s:4:\"size\";i:230632;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '7', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('84', '微信图片_20200310102419.jpg', '微信图片_20200310102419-jpg', '1583807074', '1583807074', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310102419.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/03/3955001274.jpg\";s:4:\"size\";i:27953;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '8', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('85', '微信图片_20200310102458.jpg', '微信图片_20200310102458-jpg', '1583807113', '1583807113', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310102458.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/03/4278053179.jpg\";s:4:\"size\";i:25394;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '9', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('86', '微信图片_20200310102535.jpg', '微信图片_20200310102535-jpg', '1583807163', '1583807163', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310102535.jpg\";s:4:\"path\";s:34:\"/usr/uploads/2020/03/574170759.jpg\";s:4:\"size\";i:49691;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '10', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('87', '微信图片_20200310102540.jpg', '微信图片_20200310102540-jpg', '1583807173', '1583807173', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310102540.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/03/3342715521.jpg\";s:4:\"size\";i:31467;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '11', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('88', '微信图片_20200310102630.jpg', '微信图片_20200310102630-jpg', '1583807203', '1583807203', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310102630.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/03/4112029825.jpg\";s:4:\"size\";i:44795;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '12', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('89', '微信图片_20200310102714.jpg', '微信图片_20200310102714-jpg', '1583807247', '1583807247', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310102714.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/03/1636428084.jpg\";s:4:\"size\";i:24801;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '13', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('90', '微信图片_20200310102801.jpg', '微信图片_20200310102801-jpg', '1583807300', '1583807300', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310102801.jpg\";s:4:\"path\";s:34:\"/usr/uploads/2020/03/328456361.jpg\";s:4:\"size\";i:56180;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '14', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('91', '微信图片_20200310102805.jpg', '微信图片_20200310102805-jpg', '1583807308', '1583807308', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310102805.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/03/1714648924.jpg\";s:4:\"size\";i:37565;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '15', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('92', '微信图片_20200310102847.jpg', '微信图片_20200310102847-jpg', '1583807339', '1583807339', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310102847.jpg\";s:4:\"path\";s:34:\"/usr/uploads/2020/03/263975514.jpg\";s:4:\"size\";i:52243;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '16', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('93', '微信图片_20200310103005.jpg', '微信图片_20200310103005-jpg', '1583807417', '1583807417', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310103005.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/03/3897145235.jpg\";s:4:\"size\";i:5813;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '17', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('94', '微信图片_20200310103034.jpg', '微信图片_20200310103034-jpg', '1583807455', '1583807455', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310103034.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/03/2547991010.jpg\";s:4:\"size\";i:203309;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '18', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('95', '微信图片_20200310103039.jpg', '微信图片_20200310103039-jpg', '1583807464', '1583807464', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310103039.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/03/1818235374.jpg\";s:4:\"size\";i:182856;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '19', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('96', '微信图片_20200310103126.jpg', '微信图片_20200310103126-jpg', '1583807503', '1583807503', 'a:5:{s:4:\"name\";s:31:\"微信图片_20200310103126.jpg\";s:4:\"path\";s:34:\"/usr/uploads/2020/03/254908397.jpg\";s:4:\"size\";i:22740;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '20', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '97', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('97', '挑衣指南 | 不同身型该如何选汉元素，看这里就对了！', '97', '1583807520', '1583808019', '<!--markdown-->在试汉服的时候\r\n相信不少同袍会发现\r\n明明我和XX身高体重差不多\r\n为什么我穿就莫名显胖/不好看？\r\n其实，这都是体型“惹的祸”\r\n如果光看身高体重，不看具体的体型\r\n就容易出现这样的情况\r\n今天，就来聊聊\r\n不同身材的同袍\r\n怎么挑选汉服/汉元素服装？\r\n\r\n**沙漏型身材**\r\n胸部、腿部、腰部、臀部都凹凸有致，是亚洲女性较为常见的体型，虽然X型身材的女性臀部也不一定最大，腰身也不一定最明显，但所有的部位搭配起来，X型一定是最和谐的。\r\n![微信图片_20200310101933.jpg][2]\r\n这类身材的同袍身材优势非常明显，可以说是百无禁忌，相对来说齐腰款的汉服更适合她们。\r\n\r\n\r\n----------\r\n![2.jpg][3]\r\n![1.jpg][4]\r\n此外，她们也非常适合收腰的汉元素服装，再搭配真丝、缎面等材质，就能更凸显自己的女人味~\r\n![微信图片_20200310102137.jpg][5]\r\n**苹果型身材**\r\n体形圆润，胸、 腹、臀部比较丰满，手背较丰且小腿轮廓很好看。\r\n\r\n苹果形的身材即使体重不大，也容易显得臃肿，尤其是上半身。\r\n![微信图片_20200310102258.jpg][6]\r\n\r\n\r\n----------\r\n![微信图片_20200310102329.jpg][7]\r\n![微信图片_20200310102333.jpg][8]\r\n如果选择汉元素服装，可以穿能露出纤细手臂、小腿的高腰裙装；上衣则选择V领/圆领，拉长颈部线条。\r\n![微信图片_20200310102419.jpg][9]\r\n**H型身材**\r\n\r\nH型身材整体来看还是较为显瘦的，整个身体呈现直线型。\r\n\r\n![微信图片_20200310102458.jpg][10]\r\n\r\nH型的袍袍会更适合齐腰款、袄裙等，塑造出有腰的错觉，或者利用衣服的线条，让身材有曲线。\r\n![微信图片_20200310102535.jpg][11]\r\n![微信图片_20200310102540.jpg][12]\r\n\r\n汉元素服装的话，可以选择强调腰线、有曲线的款式，会显得身材更有线条~\r\n![微信图片_20200310102630.jpg][13]\r\n**梨型**\r\n比较鲜明的特征是肩比臀窄，有溜肩的可能，上身较瘦，胸部线条不太明显，腿部、腹部、腰部、臀部则较为丰满。\r\n![微信图片_20200310102714.jpg][14]\r\n梨型的袍袍上身比较纤细，下身偏胖，所以可以选择绣花、图案等较为精致、复杂的浅色上衣，吸引视线，下身则转移注意力，选择深色、简约的裙装，齐胸衫裙、明制袄裙这种能遮住下半身的都很适合她们。\r\n\r\n\r\n----------\r\n![微信图片_20200310102801.jpg][15]\r\n\r\n![微信图片_20200310102805.jpg][16]\r\n也可以选择长度过胯的汉元素上衣，搭配遮胯的长外套，这样就可以显得苗条啦~\r\n![微信图片_20200310102847.jpg][17]\r\n**草莓型身材**\r\n体形上宽下窄像倒三角形，肩宽、胸部丰满，臀部、腿部较廋，俗称运动员体形\r\n![微信图片_20200310103005.jpg][18]\r\n\r\n\r\n----------\r\n![微信图片_20200310103034.jpg][19]\r\n\r\n![微信图片_20200310103039.jpg][20]\r\n\r\n注意，面料要避免比较硬挺显扩的材质，这样会让肩部更显宽。\r\n\r\n穿汉元素服装的时候，应该避免一字肩、船领这种会让宽肩无处遁形的设计，上衣的肩膀处尽量简洁，下装则选择装饰性、设计性比较强的服饰。\r\n![微信图片_20200310103126.jpg][21]\r\n\r\n转载地址:https://mp.weixin.qq.com/s/FVbLZfzy5XfFropzBCh7rg\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2020/03/2420810691.jpg\r\n  [2]: http://www.phpmaster.cn/usr/uploads/2020/03/2442654498.jpg\r\n  [3]: http://www.phpmaster.cn/usr/uploads/2020/03/2619276164.jpg\r\n  [4]: http://www.phpmaster.cn/usr/uploads/2020/03/1658598671.jpg\r\n  [5]: http://www.phpmaster.cn/usr/uploads/2020/03/210019693.jpg\r\n  [6]: http://www.phpmaster.cn/usr/uploads/2020/03/1858748908.jpg\r\n  [7]: http://www.phpmaster.cn/usr/uploads/2020/03/1819524221.jpg\r\n  [8]: http://www.phpmaster.cn/usr/uploads/2020/03/2349705336.jpg\r\n  [9]: http://www.phpmaster.cn/usr/uploads/2020/03/3955001274.jpg\r\n  [10]: http://www.phpmaster.cn/usr/uploads/2020/03/4278053179.jpg\r\n  [11]: http://www.phpmaster.cn/usr/uploads/2020/03/574170759.jpg\r\n  [12]: http://www.phpmaster.cn/usr/uploads/2020/03/3342715521.jpg\r\n  [13]: http://www.phpmaster.cn/usr/uploads/2020/03/4112029825.jpg\r\n  [14]: http://www.phpmaster.cn/usr/uploads/2020/03/1636428084.jpg\r\n  [15]: http://www.phpmaster.cn/usr/uploads/2020/03/328456361.jpg\r\n  [16]: http://www.phpmaster.cn/usr/uploads/2020/03/1714648924.jpg\r\n  [17]: http://www.phpmaster.cn/usr/uploads/2020/03/263975514.jpg\r\n  [18]: http://www.phpmaster.cn/usr/uploads/2020/03/3897145235.jpg\r\n  [19]: http://www.phpmaster.cn/usr/uploads/2020/03/2547991010.jpg\r\n  [20]: http://www.phpmaster.cn/usr/uploads/2020/03/1818235374.jpg\r\n  [21]: http://www.phpmaster.cn/usr/uploads/2020/03/254908397.jpg', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '1930', '392', '0');
INSERT INTO `typecho_contents` VALUES ('99', 'Python 全栈工程师必备面试题 300 道（2020 版）', '99', '1583917440', '1596179701', '<!--markdown-->![QQ截图20200731151440.png][1]\r\n#### 1. Python 基础知识\r\n\r\n1.1 语言特征及编码规范\r\n1.1.1 Python 的解释器有哪些?\r\n1.1.2 列举至少 5 条 Python 3 和 Python 2 的区别?\r\n1.1.3 Python 中新式类和经典类的区别是什么?\r\n1.1.4 Python 之禅是什么，Python 中如何获取 Python 之禅?\r\n1.1.5 python中的DocStrings(解释文档)有什么作用?\r\n1.1.6 Python 3 中的类型注解有什么好处?如何使用?\r\n1.1.7 Python 语言中的命名规范有哪些?\r\n1.1.8 Python 中各种下划线的作用?\r\n1.1.9 单引号、双引号、三引号有什么区别?\r\n\r\n1.2 文件 I/O 操作\r\n1.2.1 Python 中打开文件有哪些模式?\r\n1.2.2 Python 中 read 、readline 和 readlines 的区别?\r\n1.2.3 大文件只需读取部分内容，或者避免读取时候内存不足的解决方法?\r\n1.2.4 什么是上下文?with 上下文管理器原理?\r\n1.2.5 什么是全缓冲、行缓冲和无缓冲?\r\n1.2.6 什么是序列化和反序列化?JSON 序列化时常用的四个函数是什么?\r\n1.2.7 JSON 中 dumps 转换数据时候如何保持中文编码?\r\n\r\n\r\n1.3 数据类型\r\n1.3.1 Python 中的可变和不可变数据类型是什么?\r\n1.3.2 is 和 == 有什么区别?\r\n1.3.3 Python 中的单词大小写转换和字母统计?\r\n1.3.4 字符串，列表，元组如何反转?反转函数 reverse 和 reversed 的区别?\r\n1.3.5 Python 中的字符串格式化的方法有哪些?f-string 格式化知道吗?\r\n1.3.6 含有多种符号的字符串分割方法?\r\n1.3.7 嵌套列表转换为列表，字符串转换为列表的方法\r\n1.3.8 列表合并的常用方法?\r\n1.3.9 列表如何去除重复的元素，还是保持之前的排序?\r\n1.3.10 列表数据如何筛选，筛选出符合要求的数据?\r\n1.3.11 字典中元素的如何排序?sorted 排序函数的使用详解?\r\n1.3.12 字典如何合并?字典解包是什么?\r\n1.3.13 字典推导式使用方法?字典推导式如何格式化 cookie 值?\r\n1.3.14 zip 打包函数的使用?元组或者列表中元素生成字典?\r\n1.3.15 字典的键可以是哪些类型的数据?\r\n1.3.16 变量的作用域是怎么决定的?\r\n\r\n\r\n1.4 常用内置函数\r\n1.4.1 如何统计一篇文章中出现频率最高的 5 个单词?\r\n1.4.2 map 映射函数按规律生成列表或集合?\r\n1.4.3 filter 过滤函数如何使用?如何过滤奇数偶数平方根数?\r\n1.4.4 sort 和 sorted 排序函数的用法区别?\r\n1.4.5 enumerate 为元素添加下标索引?\r\n1.4.6 lambda 匿名函数如何使用?\r\n1.4.7 type 和 help 函数有什么作用?\r\n\r\n#### 2. Python 高级语法\r\n\r\n\r\n2.1 类和元类\r\n2.1.1 类 class 和元类 metaclass 的有什么区别?\r\n2.1.2 类实例化时候，__init__ 和 __new__ 方法有什么作用?\r\n2.1.3 实例方法、类方法和静态方法有什么不同?\r\n2.1.4 类有哪些常用的魔法属性以及它们的作用是什么?\r\n2.1.5 类中的 property 属性有什么作用?\r\n2.1.6 描述一下抽象类和接口类的区别和联系?\r\n2.1.7 类中的私有化属性如何访问?\r\n2.1.8 类如何才能支持比较操作?\r\n2.1.9 hasattr()、getattr()、setattr()、delattr()分别有什么作用?\r\n\r\n\r\n2.2 高级用法(装饰器、闭包、迭代器、生成器)\r\n2.2.1 编写函数的四个原则是什么?\r\n2.2.2 函数调用参数的传递方式是值传递还是引用传递?\r\n2.2.3 Python 中 pass 语句的作用是什么?\r\n2.2.4 闭包函数的用途和注意事项?\r\n2.2.5 *args 和 **kwargs 的区别?\r\n2.2.6 位置参数、关键字参数、包裹位置参数、包裹关键字参数执行顺序及使用注意?\r\n2.2.7 如何进行参数拆包?\r\n2.2.8 装饰器函数有什么作用?装饰器函数和普通函数有什么区别?\r\n2.2.9 带固定参数和不定参数的装饰器有什么区别?\r\n2.2.10 描述一下一个装饰器的函数和多个装饰器的函数的执行步骤?\r\n2.2.11 知道通用装饰器和类装饰器吗?\r\n2.2.12 浅拷⻉和深拷⻉的区别?\r\n2.2.13 元组的拷⻉要注意什么?\r\n2.2.14 全局变量是否一定要使用 global 进行声明?\r\n2.2.15 可迭代对象和迭代器对象有什么区别?\r\n2.2.16 描述一下 for 循环执行的步骤?\r\n2.2.17 迭代器就是生成器，生成器一定是迭代器，这句话对吗?\r\n2.2.18 yield 关键字有什么好处?\r\n2.2.19 yield 和 return 关键字的关系和区别?\r\n2.2.20 简单描述一下 yield 生成器函数的执行步骤?\r\n2.2.21 生成器函数访问方式有哪几种?生成器函数中的 send() 有什么作用?\r\n2.2.22 Python 中递归的最大次数?\r\n2.2.23 递归函数停止的条件是什么?\r\n\r\n\r\n2.4 模块\r\n2.4.1 如何查看模块所在的位置?\r\n2.4.2 import 导入模块时候，搜索文件的路径顺序?\r\n2.4.3 多模块导入共享变量的问题?\r\n2.4.4 Python 常用内置模块有哪些?\r\n2.4.5 Python 中常⻅的异常有哪些?\r\n2.4.6 如何捕获异常?万能异常捕获是什么?\r\n2.4.7 Python 异常相关的关键字主要有哪些?\r\n2.4.8 异常的完整写法是什么?\r\n2.4.9 包中的 __init__.py 文件有什么作用?\r\n2.4.10 模块内部的 __name__ 有什么作用?\r\n\r\n\r\n2.5 面向对象\r\n2.5.1 面向过程和面向对象编程的区别?各自的优缺点和应用场景?\r\n2.5.2 面向对象设计的三大特征是什么?\r\n2.5.3 面向对象中有哪些常用概念?\r\n2.5.4 多继承函数有那几种书写方式?\r\n2.5.5 多继承函数执行的顺序(MRO)?\r\n2.5.6 面向对象的接口如何实现?\r\n\r\n\r\n2.6 设计模式\r\n2.6.1 什么是设计模式?\r\n2.6.2 面向对象中设计模式的六大原则是什么?\r\n2.6.3 列举几个常⻅的设计模式?\r\n2.6.4 Mixin 设计模式是什么?它的特点和优点?\r\n2.6.5 什么是单例模式?单例模式的作用?\r\n2.6.7 单例模式的应用场景有那些?\r\n\r\n\r\n2.7 内存管理\r\n2.7.1 Python 的内存管理机制是什么?\r\n2.7.2 Python 的内寸管理的优化方法?\r\n2.7.3 Python 中内存泄漏有哪几种?\r\n2.7.4 Python 中如何避免内存泄漏?\r\n2.7.5 内存溢出的原因有哪些?\r\n2.7.6 Python 退出时是否释放所有内存分配?\r\n\r\n\r\n#### 3. 系统编程\r\n\r\n3.1 多进程、多线程、协程、并行、并发、锁\r\n3.1.1 并发与并行的区别和联系?\r\n3.1.2 程序中的同步和异步与现实中一样吗?\r\n3.1.3 进程、线程、协程的区别和联系?\r\n3.1.4 多进程和多线程的区别?\r\n3.1.5 协程的优势是什么?\r\n3.1.6 多线程和多进程分别用于哪些场景?\r\n3.1.7 全局解释器锁(GIL)是什么?如何解决 GIL 问题?\r\n3.1.8 Python 中有哪些锁(LOCK)?它们分别有什么作用?\r\n3.1.9 Python 中如何实现多线程和多进程?\r\n3.1.10 守护线程和非守护线程是什么?\r\n3.1.11 多线程的执行顺序是什么样的?\r\n3.1.12 多线程非安全是什么意思?\r\n3.1.13 互斥锁是什么?有什么好处和坏处?\r\n3.1.14 什么是僵尸进程和孤儿进程?\r\n3.1.15 多线程和多进程如何实现通信?\r\n3.1.16 Python 3 中 multiprocessing.Queue() 和 queue.Queue() 的区别?\r\n3.1.17 如何使用多协程并发请求网⻚?\r\n3.1.18 简单描述一下 asyncio 模块实现异步的原理?\r\n\r\n#### 4. 网络编程\r\n\r\n\r\n4.1 TCP UDP HTTP SEO WSGI 等\r\n4.1.1 UDP 和 TCP 有什么区别以及各自的优缺点?\r\n4.1.2 IP 地址是什么?有哪几类?\r\n4.1.3 举例描述一下端口有什作用?\r\n4.1.4 不同电脑上的进程如何实现通信的?\r\n4.1.5 列举一下常用网络通信名词?\r\n4.1.6 描述一下请求一个网⻚的步骤(浏览器访问服务器的步骤)?\r\n4.1.7 HTTP 与 HTTPS 协议有什么区别?\r\n4.1.8 TCP 中的三次握手和四次挥手是什么?\r\n4.1.9 TCP 短连接和⻓连接的优缺点?各自的应用场景?\r\n4.1.10 TCP 第四次挥手为什么要等待 2MSL?\r\n4.1.11 HTTP 最常⻅的请求方法有哪些?\r\n4.1.12 GET 请求和 POST 请求有什么区别?\r\n4.1.13 cookie 和 session 的有什么区别?\r\n4.1.14 七层模型和五层模型是什么?\r\n4.1.15 HTTP 协议常⻅状态码及其含义?\r\n4.1.16 HTTP 报文基本结构?列举常用的头部信息?\r\n4.1.17 SEO 是什么?\r\n4.1.18 伪静态 URL、静态 URL 和动态 URL 的区别?\r\n4.1.19 浏览器镜头请求和动态请求过程的区别?\r\n4.1.20 WSGI 接口有什么好处?\r\n4.1.21 简单描述浏览器通过 WSGI 接口请求动态资源的过程?\r\n\r\n\r\n#### 5. 数据库\r\n5.1 MySQL\r\n5.1.1 NoSQL 和 SQL 数据库的比较?\r\n5.1.2 了解 MySQL 的事物吗?事物的四大特性是什么?\r\n5.1.3 关系型数据库的三范式是什么?\r\n5.1.4 关系型数据库的核心元素是什么?\r\n5.1.5 简单描述一下 Python 访问 MySQL 的步骤?\r\n5.1.6 写一个 Python 连接操作 MySQL 数据库实例?\r\n5.1.7 SQL 语句主要有哪些?分别有什么作用?\r\n5.1.8 MySQL 有哪些常用的字段约束?\r\n5.1.9 什么是视图?视图有什么作用?\r\n5.1.10 什么是索引?索引的优缺点是什么?\r\n5.1.11 NULL 是什么意思?它和空字符串一样吗?\r\n5.1.12 主键、外键和索引的区别?\r\n5.1.13 char 和 varchar 的区别?\r\n5.1.14 SQL 注入是什么?如何避免 SQL 注入?\r\n5.1.15 存储引擎 MyISAM 和 InnoDB 有什么区别?\r\n5.1.16 MySQL 中有哪些锁?\r\n5.1.17 三种删除操作 drop、truncate、delete 的区别?\r\n5.1.18 MySQL 中的存储过程是什么?有什么优点?\r\n5.1.19 MySQL 数据库的有哪些种类的索引?\r\n5.1.20 MySQL 的事务隔离级别?\r\n5.1.21 MySQL 中的锁如何进行优化?\r\n5.1.22 解释 MySQL 外连接、内连接与自连接的区别?\r\n5.1.23 如何进行 SQL 优化?\r\n5.1.24 什么是 MySQL 主从?主从同步有什么好处?\r\n5.1.25 MySQL 主从与 MongoDB 副本集有什么区别?\r\n5.1.26 MySQL 账户权限怎么分类的?\r\n5.1.27 如何使用 Python 面向对象操作 MySQL 数据库?\r\n\r\n\r\n5.2 Redis\r\n\r\n5.2.1  Redis 是什么?常⻅的应用场景?\r\n5.2.2  Redis 常⻅数据类型有哪些?各自有什么应用场景?\r\n5.2.3  非关系型数据库 Redis 和 MongoDB 数据库的结构有什么区别?\r\n5.2.4  Redis 和 MongoDB 数据库的键(key)和值(value)的区别?\r\n5.2.5  Redis 持久化机制是什么?有哪几种方式?\r\n5.2.6  Redis 的事务是什么?\r\n5.2.7  为什么要使用 Redis 作为缓存?\r\n5.2.8  Redis 和 Memcached 的区别?\r\n5.2.9  Redis 如何设置过期时间和删除过期数据?\r\n5.2.10 Redis 有哪几种数据淘汰策略?\r\n5.2.11 Redis 为什么是单线程的?\r\n5.2.12 单线程的 Redis 为什么这么快?\r\n5.2.13 缓存雪崩和缓存穿透是什么?如何预防解决?\r\n5.2.14 布隆过滤器是什么?\r\n5.2.15 简单描述一下什么是缓存预热、缓存更新和缓存降级?\r\n5.2.16 如何解决 Redis 的并发竞争 Key 的问题?\r\n5.2.17 写一个 Python 连接操作 Redis 数据库实例?\r\n5.2.18 什么是分布式锁?\r\n5.2.19 Python 如何实现一个 Redis 分布式锁?\r\n5.2.20 如何保证缓存与数据库双写时的数据一致性?\r\n5.2.21 集群是什么?Redis 有哪些集群方案?\r\n5.2.22 Redis 常⻅性能问题和解决方案?\r\n5.2.23 了解 Redis 的同步机制么?\r\n5.2.24 如果有大量的 key 需要设置同一时间过期，一般需要注意什么?\r\n5.2.25 如何使用 Redis 实现异步队列?\r\n5.2.26 列举一些常用的数据库可视化工具?\r\n\r\n\r\n5.3 MongoDB\r\n\r\n\r\n5.3.1 NoSQL 数据库主要分为哪几种?分别是什么?\r\n5.3.2 MongoDB 的主要特点及适用于哪些场合?\r\n5.3.3 MongoDB 中的文档有哪些特性?\r\n5.3.4 MongoDB 中的 key 命名要注意什么?\r\n5.3.5 MongoDB 数据库使用时要注意的问题?\r\n5.3.6 常用的查询条件操作符有哪些?\r\n5.3.7 MongoDB 常用的管理命令有哪些?\r\n5.3.8 MongoDB 为何使用 GridFS 来存储文件?\r\n5.3.9 如果一个分片(Shard)停止或很慢的时候，发起一个查询会怎样?\r\n5.3.10 分析器在 MongoDB 中的作用是什么?\r\n5.3.11 MongoDB 中的名字空间(namespace)是什么?\r\n5.3.12 更新操作会立刻 fsync 到磁盘吗?\r\n5.3.13 什么是 master 或 primary?什么是 secondary 或 slave?\r\n5.3.14 必须调用 getLastError 来确保写操作生效了么?\r\n5.3.15 MongoDB 副本集原理及同步过程?\r\n5.3.16 MongoDB 中的分片是什么意思?\r\n5.3.17 “ObjectID”有哪些部分组成?\r\n5.3.18 在 MongoDB 中什么是索引?\r\n5.3.19 什么是聚合?\r\n5.3.20 写一个 Python 连接操作 MongoDB 数据库实例?\r\n\r\n\r\n#### 6. 数据解析提取\r\n\r\n6.1 正则表达式\r\n\r\n6.1.1 match、search 和 findall 有什么区别?\r\n6.1.2 正则表达式的 ()、[]、{} 分别代表什么意思?\r\n6.1.3 正则表达式中的 .* 、 .+ 、 .*? 、 .+? 有什么区别?\r\n6.1.4 .*? 贪婪匹配的一种特殊情况?当 * 和 ? 中间有一个字符会怎么样?\r\n6.1.5 \\s 和 \\S 是什么意思?re.S 是什么意思?\r\n6.1.6 写一个表达式匹配座机或者手机号码?\r\n6.1.7 正则表达式检查 Python 中使用的变量名是否合法?\r\n6.1.8 正则表达式检查邮箱地址是否符合要求?\r\n6.1.9 如何使用分组匹配 HTML 中的标签元素?\r\n6.1.10 如何使用 re.sub 去掉“028-00112233 # 这是一个电话号码”# 和后面的注释内容?\r\n6.1.11 re.sub 替换如何支持函数调用?举例说明?\r\n6.1.12 如何只匹配中文字符?\r\n6.1.13 如何过滤评论中的表情?\r\n6.1.14 Python 中的反斜杠 \\ 如何使用正则表达式匹配?\r\n6.1.15 如何提取出下列网址中的域名?\r\n6.1.16 去掉 \'ab;cd%e\\tfg,,jklioha;hp,vrww\\tyz\' 中的符号，拼接为一个字符串?\r\n6.1.17 str.replace 和 re.sub 替换有什么区别?\r\n6.1.18 如何使用重命名分组修改日期格式?\r\n6.1.19 (?:x) a(?=x) a(?!=x) (?<=x)a (?<!x)a 有什么区别?\r\n\r\n\r\n6.2 XPath\r\n\r\n6.2.1  XML 是什么?XML 有什么用途?\r\n6.2.2  XML 和 HTML 之间有什么不同?\r\n6.2.3  描述一下 XML lxml XPath 之间有什么关系?\r\n6.2.4  介绍一下 XPath 的节点?\r\n6.2.5  XPath 中有哪些类型的运算符?\r\n\r\n6.2.6 XPath 中的 /// 、 ./ 、 ../ 、 .// 别有什么区别?\r\n6.2.7 XPath 中如何同时选取多个路径?\r\n6.2.8 XPath 中的 * 和 @* 分别表示什么含义?\r\n6.2.9 如何使用位置属性选取节点中的元素?\r\n\r\n6.2.10 XPath 中如何多条件查找?\r\n6.2.11 Scrapy 和 lxml 中的 XPath 用法有什么不同?\r\n6.2.12 用过哪些常用的 XPath 开发者工具?\r\n\r\n\r\n6.3 BeautifulSoup4\r\n\r\n6.3.1 BeautifulSoup4 是什么?有什么特点?\r\n6.3.2 三种解析工具:正则表达式 lxml BeautifulSoup4 各自有什么优缺点?\r\n6.3.3 etree.parse()、etree.HTML() 和 etree.tostring() 有什么区别?\r\n6.3.4 BeautifulSoup4 支持的解析器以及它们的优缺点?\r\n6.3.5 BeautifulSoup4 中的四大对象是什么?\r\n6.3.6 BeautifulSoup4 中如何格式化 HTML 代码?\r\n6.3.7 BeautifulSoup4 中 find 和 find_all 方法的区别?\r\n6.3.8 string、strings 和 stripped_strings 有什么区别?\r\n6.3.9 BeautifulSoup4 输出文档的编码格式是什么?\r\n\r\n\r\n#### 7. 网络爬虫\r\n\r\n7.1 网络爬虫是什么?它有什么特征?\r\n7.2 Python 中常用的爬虫模块和框架有哪些?它们有什么优缺点?\r\n7.3 搜索引擎中的 ROBOTS 协议是什么?\r\n7.4 urlib 和 requests 库请求网⻚有什么区别?\r\n7.5 网⻚中的 ASCII Unicode UTF-8 编码之间的关系?\r\n7.6 urllib 如何检测网⻚编码?\r\n7.7 urllib 中如何使用代理访问网⻚?\r\n7.8 如果遇到不信任的 SSL 证书，如何继续访问?\r\n7.9 如何提取和使用本地已有的 cookie 信息?\r\n7.10 requests 请求中出现乱码如何解决?\r\n7.11 requests 库中 response.text 和 response.content 的区别?\r\n7.12 实际开发中用过哪些框架?\r\n7.13 Scrapy 和 PySpider 框架主要有哪些区别?\r\n7.14 Scrapy 的主要部件及各自有什么功能?\r\n7.15 描述一下 Scrapy 爬取一个网站的工作流程?\r\n7.16 Scrapy 中的中间件有什么作用?\r\n7.17 Scrapy 项目中命名时候要注意什么?\r\n7.18 Scrapy 项目中的常用命令有哪些?\r\n7.19 scrapy.Request() 中的 meta 参数有什么作用?\r\n7.20 Python 中的协程阻塞问题如何解决?\r\n7.21 Scrapy 中常用的数据解析提取工具有哪些?\r\n7.22 描述一下 Scrapy 中数据提取的机制?\r\n7.23 Scrapy 是如何实现去重的?指纹去重是什么?\r\n7.24 Item Pipeline 有哪些应用?\r\n7.25 Scrapy 中常用的调试技术有哪些?\r\n7.26 Scrapy 中有哪些常⻅异常以及含义?\r\n\r\n7.27 Spider、CrawlSpider、XMLFeedSpider 和 RedisSpider 有什么区别?\r\n7.28 scrapy-redis 是什么?相比 Scrapy 有什么优点?\r\n7.29 使用 scrapy-redis 分布式爬虫，需要修改哪些常用的配置?\r\n7.30 常⻅的反爬虫措施有哪些?如何应对?\r\n\r\n7.31 BloomFitler 是什么?它的原理是什么?\r\n7.32 为什么会用到代理?代码展现如何使用代理?\r\n7.33 爬取的淘宝某个人的历史消费信息(登陆需要账号、密码、验证码)，你会如何操作?7.34 网站中的验证码是如何解决的?\r\n7.35 动态⻚面如何有效的抓取?\r\n7.36 如何使用 MondoDB 和 Flask 实现一个 IP 代理池?\r\n\r\n\r\n#### 8. 数据分析及可视化\r\n\r\n8.1 Python 数据分析通常使用的环境、工具和库都有哪些?库功能是什么?\r\n8.2 常用的数据可视化工具有哪些?各自有什么优点?\r\n8.3 数据分析的一般流程是什么?\r\n8.4 数据分析中常⻅的统计学概念有哪些?\r\n8.5 归一化方法有什么作用?\r\n8.6 常⻅数据分析方法论?\r\n8.7 如何理解欠拟合和过拟合?\r\n8.8 为什么说朴素⻉叶斯是“朴素”的?\r\n8.9 Matplotlib 绘图中如何显示中文?\r\n8.10 Matplotlib 中如何在一张图上面画多张图?\r\n8.11 使用直方图展示多部电影 3 天的票房情况?\r\n8.12 描述一下 NumPy array 对比 Python list 的优势?\r\n8.13 数据清洗有哪些方法?\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2020/07/3511580941.png', '0', '1', null, 'post', 'publish', null, '1', '1', '1', '1', '0', '1', '3576', '536', '0');
INSERT INTO `typecho_contents` VALUES ('100', 'b21bb051f8198618b8f0ae2b40ed2e738ad4e6ee.png', 'b21bb051f8198618b8f0ae2b40ed2e738ad4e6ee-png', '1587447518', '1587447518', 'a:5:{s:4:\"name\";s:44:\"b21bb051f8198618b8f0ae2b40ed2e738ad4e6ee.png\";s:4:\"path\";s:34:\"/usr/uploads/2020/04/608273096.png\";s:4:\"size\";i:291265;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '101', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('101', '通俗讲解 OSI 七层协议参考模型', '101', '1587447480', '1587447540', '<!--markdown-->![b21bb051f8198618b8f0ae2b40ed2e738ad4e6ee.png][1]\r\n\r\n        国际标准化组织 ISO 于 1983 年正式提出了开放式系统互联模型（通称 ISO/OSI）。将整个网络通信的功 能划分为 7 个层次\r\n        OSI参考模型将整个网络通信的功能划分为 7 个层次，这些层 就像我们吃的洋葱、卷心菜的一样：每一层都将其下面的层遮起来。 下一层次的细节被隐藏起来。如果你将洋葱皮剥开往里看，你一 定会流下许多眼泪，OSI模型也是如此，越往下看越难理解，只要你 不怕流泪、麻烦，不放弃你就会成功。 \r\n物理层：网卡，网线，集线器，中继器，调制解调器\r\n数据链路层：网桥，交换机\r\n网络层：路由器\r\n网关工作在第四层传输层及其以上\r\n集线器是物理层设备,采用广播的形式来传输信息。\r\n交换机就是用来进行报文交换的机器。多为链路层设备(二层交换机)，能够进行地址学习，采用存储转发的形式来交换报文.。\r\n路由器的一个作用是连通不同的网络，另一个作用是选择信息传送的线路。选择通畅快捷的近路，能大大提高通信速度，减轻网络系统通信负荷，节约网络系统资源，提高网络系统畅通率。 \r\n交换机和路由器的区别\r\n交换机拥有一条很高带宽的背部总线和内部交换矩阵。交换机的所有的端口都挂接在这条总线上，控制电路收到数据包以后，处理端口会查找内存中的地址对照表以确定目的MAC（网卡的硬件地址）的NIC（网卡）挂接在哪个端口上，通过内部交换矩阵迅速将数据包传送到目的端口，目的MAC若不存在则广播到所有的端口，接收端口回应后交换机会“学习”新的地址，并把它添加入内部MAC地址表中。 \r\n使用交换机也可以把网络“分段”，通过对照MAC地址表，交换机只允许必要的网络流量通过交换机。通过交换机的过滤和转发，可以有效的隔离广播风暴，减少误包和错包的出现，避免共享冲突。 \r\n交换机在同一时刻可进行多个端口对之间的数据传输。每一端口都可视为独立的网段，连接在其上的网络设备独自享有全部的带宽，无须同其他设备竞争使用。当节点A向节点D发送数据时，节点B可同时向节点C发送数据，而且这两个传输都享有网络的全部带宽，都有着自己的虚拟连接。假使这里使用的是10Mbps的以太网交换机，那么该交换机这时的总流通量就等于2×10Mbps＝20Mbps，而使用10Mbps的共享式HUB时，一个HUB的总流通量也不会超出10Mbps。 \r\n总之，交换机是一种基于MAC地址识别，能完成封装转发数据包功能的网络设备。交换机可以“学习”MAC地址，并把其存放在内部地址表中，通过在数据帧的始发者和目标接收者之间建立临时的交换路径，使数据帧直接由源地址到达目的地址。\r\n从过滤网络流量的角度来看，路由器的作用与交换机和网桥非常相似。但是与工作在网络物理层，从物理上划分网段的交换机不同，路由器使用专门的软件协议从逻辑上对整个网络进行划分。例如，一台支持IP协议的路由器可以把网络划分成多个子网段，只有指向特殊IP地址的网络流量才可以通过路由器。对于每一个接收到的数据包，路由器都会重新计算其校验值，并写入新的物理地址。因此，使用路由器转发和过滤数据的速度往往要比只查看数据包物理地址的交换机慢。但是，对于那些结构复杂的网络，使用路由器可以提高网络的整体效率。路由器的另外一个明显优势就是可以自动过滤网络广播。\r\n集线器与路由器在功能上有什么不同? \r\n首先说HUB,也就是集线器。它的作用可以简单的理解为将一些机器连接起来组成一个局域网。而交换机（又名交换式集线器）作用与集线器大体相同。但是两者在性能上有区别：集线器采用的式共享带宽的工作方式，而交换机是独享带宽。这样在机器很多或数据量很大时，两者将会有比较明显的。而路由器与以上两者有明显区别，它的作用在于连接不同的网段并且找到网络中数据传输最合适的路径。路由器是产生于交换机之后，就像交换机产生于集线器之后，所以路由器与交换机也有一定联系，不是完全独立的两种设备。路由器主要克服了交换机不能路由转发数据包的不足。 \r\n总的来说，路由器与交换机的主要区别体现在以下几个方面： \r\n（1）工作层次不同 \r\n最初的的交换机是工作在数据链路层，而路由器一开始就设计工作在网络层。由于交换机工作在数据链路层，所以它的工作原理比较简单，而路由器工作在网络层，可以得到更多的协议信息，路由器可以做出更加智能的转发决策。 \r\n（2）数据转发所依据的对象不同 \r\n交换机是利用物理地址或者说MAC地址来确定转发数据的目的地址。而路由器则是利用IP地址来确定数据转发的地址。IP地址是在软件中实现的，描述的是设备所在的网络。MAC地址通常是硬件自带的，由网卡生产商来分配的，而且已经固化到了网卡中去，一般来说是不可更改的。而IP地址则通常由网络管理员或系统自动分配。 \r\n（3）传统的交换机只能分割冲突域，不能分割广播域；而路由器可以分割广播域 \r\n由交换机连接的网段仍属于同一个广播域，广播数据包会在交换机连接的所有网段上传播，在某些情况下会导致通信拥挤和安全漏洞。连接到路由器上的网段会被分配成不同的广播域，广播数据不会穿过路由器。虽然第三层以上交换机具有VLAN功能，也可以分割广播域，但是各子广播域之间是不能通信交流的，它们之间的交流仍然需要路由器。 \r\n（4）路由器提供了防火墙的服务 \r\n路由器仅仅转发特定地址的数据包，不传送不支持路由协议的数据包传送和未知目标网络数据包的传送，从而可以防止广播风暴。\r\n        1、物理层 物理层是OSI 参考模型的最低层，且与物理传输介质相关联， 该层是实现其他层和通信介质之间的接口。是整个开放系统的基 础。它的主要任务就是描述为确定与传输媒体的接口的一些特性， 即机械特性、电气特性、功能特性以及规程特性，如规定了使用电 缆和接头的类型、传送信号的电压（水晶头、脉冲电压 5v）等，在这 一层数据还没有被组织，仅作为原始的位流或电气电压处理。 物理层的主要功能：（1）为数据端设备提供传送数据的通路， 无论花费多少电缆、光纤、双绞线，都把通信的两个数据终端（电脑、 路由器、交换机）设备连接起来，形成一条通路；（2）传输数据。一 是要保证数据能在其上正确通过，必须保证一方发出“1”时（你 好！），另一方接收到的也是“1”（你好！）而不是“0”（再见！）。二 是要提供足够的带宽，以减少信道上的拥塞。三传输数据的方式 串行或并行，半双工或全双工，同步或异步传输的需要；（3）完成物 理层的管理工作，如监视物理层所有连接，任何的连接断开都会立 即得到检测。 物理层相应设备包括网络传输介质（如同轴电缆、双绞线、光 缆、无线）和连接器等，以及保证物理通信的相关设备，如中继器、 共享式 HUB、放大设备等。\r\n         2、数据链路层 数据链路层是 OSI 参考模型的第 2 层，数据链路层将物理层 不可靠的传输媒体变成可靠的传输通路提供给网络层，它把从物 理层来的原始数据打包成帧。负责帧在计算机之间的无差错信息 传递。物理层的传输媒体及其连接是长期的，网线、光纤时刻和终 端设备（电脑、路由器）连接着。而数据链路连接是有生存期的，在 连接生存期内，收发两端可以进行不等的一次或多次数据通信。就 像打电话，电话拨通后就建立了一次电话连接（数据链路），通话完 毕要挂掉，通话结束了但电话线还连接着，每次打电话都要拨通号 建立连接，当然电脑的每次通信也都要经过建立通信联络和拆除 通信联络两过程。这种建立起来的数据收发关系就叫作数据链路。\r\n         3、网络层 网络层是 OSI 参考模型中最复杂、最重要的一层。网络层的 产生也是网络发展的结果，当数据终端增多时，它们之间需要中继 设备相连。此时会出现一台终端要求不只是与唯一的一台而是能 和多台终端通信的情况，这就产生了把任意两台数据终端设备的 数据链接起来的问题，也就是路由，进而实现两个端系统之间的数 据传送。工作在网络层的协议有 TCP/IP、IPX/SPX、AppleTalk 等。 网络层的主要提供以下功能：（1）路径选择与中继。物理层和数据链路层都只解决直接相连的两节点间的数据传输问题，网络 层则是 OSI 模型中第一个能实现不一定需要直接相连的节点或设 备间的数据传输问题的协议层；（2）拥塞控制、流量控制。当通信 子网中有太多的分组时，网络性能降低，这种情况就叫拥塞，是一 个全局性问题，涉及主机、路由器等很多因素。流量控制则与点到 点的通信量有关，主要解决快速发送方与慢速接收方的问题，是局 部问题，一般都是基于反馈进行控制的。网络中链路层、网络层、 传输层等都存在，其控制方法大体一致，目的是防止通信量过大造 成通信于网性能下降。 \r\n        在实现网络层功能时，需要解决的主要问题如下：\r\n         寻址：数据链路层中使用的物理地址（如MAC地址）仅解决网络内部的寻址问题。在不同子网之间通信时，为了识别和找到网络中的设备，每一子网中的设备都会被分配一个唯一的地址。由于各子网使用的物理技术可能不同，因此这个地址应当是逻辑地址（如IP地址）。\r\n         交换：规定不同的信息交换方式。常见的交换技术有：线路交换技术和存储转发技术，后者又包括报文交换技术和分组交换技术。\r\n         路由算法：当源节点和目的节点之间存在多条路径时，本层可以根据路由算法，通过网络为数据分组选择最佳路径，并将信息从最合适的路径由发送端传送到接收端。\r\n         连接服务：与数据链路层流量控制不同的是，前者控制的是网络相邻节点间的流量，后者控制的是从源节点到目的节点间的流量。其目的在于防止阻塞，并进行差错检测。\r\n        4、传输层 传输层是 OSI 参考模型的第 4 层中，介于应用层和网络层之 间的传输层是分层网络体系结构的重心部分。是惟一负责总体数 据传输和控制的一层。它的重要任务就是直接给运行在不同主机 上的应用程序提供通信服务，而网络层协议为不同主机提供逻辑 通信。 在 OSI 模型中传输层是负责数据通信的最高层，又是面向网 络通信的低三层和面向信息处理的高三层之间的中间层。因为网 络层不一定保证服务的可靠，用户也不能直接对通信子网加以控 制，因此在网络层之上加一层即传输层以改善传输质量。原因世 界上各种通信子网在性能上存在着很大差异。如电话交换网，分 组交换网，局域网等通信子网都可互连，但它们提供的吞吐量，传 输速率，数据延迟通信费用各不相同，传输层就承担了调节上述通 信子网的差异，使会话层感受不到。此外传输层还具备差错恢复， 流量控制等功能。传输层支持的协议有：TCP/IP 的传输控制协议 TCP、Novell的顺序包交换SPX及Microsoft NetBIOS/NetBEUI等。\r\n        主要功能如下：\r\n        传输连接管理：提供建立、维护和拆除传输连接的功能。传输层在网络层的基础上为高层提供“面向连接”和“面向无接连”的两种服务。\r\n        处理传输差错：提供可靠的“面向连接”和不太可靠的“面向无连接”的数据传输服务、差错控制和流量控制。在提供“面向连接”服务时，     通过这一层传输的数据将由目标设备确认，如果在指定的时间内未收到确认信息，数据将被重发。\r\n         监控服务质量。\r\n         5、会话层 会话层的主要功能是在两个节点间建立、维护和释放面向用 户的连接，对进行会话的两台机器间建立对话控制，管理会话如管 理哪边发送、何时发送、占用多长时间等，保证会话数据可靠传送。 会话层还提供了同步服务，例如你正在下载一个 100M 的文件，当 下载到 95M 时，网络断线了，这时是不需要重头再传的。 会话层需要决定使用全双工通信还是半双工通信。如果采用 全双工通信，则会话层在对话管理中做的工作就很少；如果采用半 双工通信，会话层则通过一个数据令牌来协调会话，保证每次只有 一个用户能够传输数据。当建立一个会话时，先让一个用户得到 令牌。只有获得令牌的用户才有权进行发送数据。\r\n        会话层的具体功能如下：\r\n        会话管理：允许用户在两个实体设备之间建立、维持和终止会话，并支持它们之间的数据交换。例如提供单方向会话或双向同时会话，并管理会话中的发送顺序，以及会话所占用时间的长短。\r\n         会话流量控制：提供会话流量控制和交叉会话功能。\r\n        寻址：使用远程地址建立会话连接。l\r\n        出错控制：从逻辑上讲会话层主要负责数据交换的建立、保持和终止，但实际的工作却是接收来自传输层的数据，并负责纠正错误。会话控制和远程过程调用均属于这一层的功能。但应注意，此层检查的错误不是通信介质的错误，而是磁盘空间、打印机缺纸等类型的高级错误。\r\n         6、表示层 表示层是处理所有与数据表示及传输有关的一层，为异种机 通信提供一种公共语言，为上层用户提供数据信息的语法表示变 换，屏蔽不同计算机在信息表示方面的差异，即用一种大家一致同 意的标准方法对数据编码。进行数据加密、数据压缩传输、字符集 转换等以便能进行互操作。这种类型的服务之所以需要，是因为 不同的计算机体系结构使用的数据表示法不同。\r\n表示层的具体功能如下：\r\n        数据格式处理：协商和建立数据交换的格式，解决各应用程序之间在数据格式表示上的差异。\r\n        数据的编码：处理字符集和数字的转换。例如由于用户程序中的数据类型（整型或实型、有符号或无符号等）、用户标识等都可以有不同的表示方式，因此，在设备之间需要具有在不同字符集或格式之间转换的功能。\r\n        压缩和解压缩：为了减少数据的传输量，这一层还负责数据的压缩与恢复。\r\n        数据的加密和解密：可以提高网络的安全性。\r\n        7、应用层 应用层是最终用户应用程序访问网络服务的地方，它负责识 别并证实通信双方的可用性，进行数据传输完整性控制，使网络应 用程序（如电子邮件、P2P文件共享、多用户网络游戏、网络浏览、目 录查询等）能够协同工作。应用层是 OSI 参考模型的最高层，它为 用户的应用进程访问 OSI 环境提供服务。应用层关心的主要是进 程之间的通信行为，因而对应用进程所进行的抽象只保留了应用 产程与应用进程间交互行为的有关部分。这种现象实际上是对应 用进程某种程度上的简化。\r\n        用户接口：应用层是用户与网络，以及应用程序与网络间的直接接口，使得用户能够与网络进行交互式联系。\r\n        实现各种服务：该层具有的各种应用程序可以完成和实现用户请求的各种服务。\r\n        8、结语 OSI 模型（应称为 OSI 理想化的模型）本身不是网络体系结构 的全部内容，这是因为它并未确切地描述用于各层的协议和服务， 它仅仅告诉我们每一层应该做什么。各层之间相互独立，某一层 只要了解下一层通过接口所提供的服务，而不需了解其实现细节。 它的灵活性好，若某一层的内容发生变化，只要接口关系不变，上 下层均不受影响，同时也便于程序的实现、调试和维护。不过它仅 仅是一种网络教学模型，到目前为止，OSI 模型还没有实现。\r\n        由于OSI是一个理想的模型，因此一般网络系统只涉及其中的几层，很少有系统能够具有所有的7层，并完全遵循它的规定。\r\n在7层模型中，每一层都提供一个特殊的网络功能。从网络功能的角度观察：下面4层（物理层、数据链路层、网络层和传输层）主要提供数据传输和交换功能，即以节点到节点之间的通信为主；第4层作为上下两部分的桥梁，是整个网络体系结构中最关键的部分；而上3层（会话层、表示层和应用层）则以提供用户与应用程序之间的信息和数据处理功能为主。简言之，下4层主要完成通信子网的功能，上3层主要完成资源子网的功能。\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2020/04/608273096.png', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '1799', '391', '0');
INSERT INTO `typecho_contents` VALUES ('102', 'bg2013042401.jpg', 'bg2013042401-jpg', '1591089687', '1591089687', 'a:5:{s:4:\"name\";s:16:\"bg2013042401.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/06/1107498796.jpg\";s:4:\"size\";i:20181;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '112', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('103', 'bg2013042402.png', 'bg2013042402-png', '1591089707', '1591089707', 'a:5:{s:4:\"name\";s:16:\"bg2013042402.png\";s:4:\"path\";s:35:\"/usr/uploads/2020/06/1207249356.png\";s:4:\"size\";i:6162;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '2', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '112', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('104', 'bg2013042403.jpg', 'bg2013042403-jpg', '1591089715', '1591089715', 'a:5:{s:4:\"name\";s:16:\"bg2013042403.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/06/3840644866.jpg\";s:4:\"size\";i:31507;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '3', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '112', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('105', 'bg2013042404.jpg', 'bg2013042404-jpg', '1591089725', '1591089725', 'a:5:{s:4:\"name\";s:16:\"bg2013042404.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/06/4070422856.jpg\";s:4:\"size\";i:79106;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '4', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '112', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('106', 'bg2013042405.jpg', 'bg2013042405-jpg', '1591089734', '1591089734', 'a:5:{s:4:\"name\";s:16:\"bg2013042405.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/06/1869771675.jpg\";s:4:\"size\";i:15421;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '5', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '112', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('107', 'bg2013042406.png', 'bg2013042406-png', '1591089749', '1591089749', 'a:5:{s:4:\"name\";s:16:\"bg2013042406.png\";s:4:\"path\";s:35:\"/usr/uploads/2020/06/1855669491.png\";s:4:\"size\";i:5516;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '6', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '112', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('108', 'bg2013042407.jpg', 'bg2013042407-jpg', '1591089757', '1591089757', 'a:5:{s:4:\"name\";s:16:\"bg2013042407.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/06/1169034069.jpg\";s:4:\"size\";i:14618;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '7', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '112', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('109', 'bg2013042408.jpg', 'bg2013042408-jpg', '1591089764', '1591089764', 'a:5:{s:4:\"name\";s:16:\"bg2013042408.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/06/1534665868.jpg\";s:4:\"size\";i:25084;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '8', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '112', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('110', 'bg2013042409.jpg', 'bg2013042409-jpg', '1591089774', '1591089774', 'a:5:{s:4:\"name\";s:16:\"bg2013042409.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/06/2856383427.jpg\";s:4:\"size\";i:44546;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '9', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '112', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('111', 'bg2013042410.jpg', 'bg2013042410-jpg', '1591089807', '1591089807', 'a:5:{s:4:\"name\";s:16:\"bg2013042410.jpg\";s:4:\"path\";s:34:\"/usr/uploads/2020/06/259790879.jpg\";s:4:\"size\";i:38645;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '10', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '112', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('112', '进程与线程的一个简单解释', '112', '1591089780', '1591089860', '<!--markdown-->进程（process）和线程（thread）是操作系统的基本概念，但是它们比较抽象，不容易掌握。\r\n\r\n最近，我读到一篇材料，发现有一个很好的类比，可以把它们解释地清晰易懂。\r\n1.\r\n![bg2013042401.jpg][1]\r\n计算机的核心是CPU，它承担了所有的计算任务。它就像一座工厂，时刻在运行。\r\n\r\n2.\r\n![bg2013042402.png][2]\r\n假定工厂的电力有限，一次只能供给一个车间使用。也就是说，一个车间开工的时候，其他车间都必须停工。背后的含义就是，单个CPU一次只能运行一个任务。\r\n\r\n3.\r\n![bg2013042403.jpg][3]\r\n进程就好比工厂的车间，它代表CPU所能处理的单个任务。任一时刻，CPU总是运行一个进程，其他进程处于非运行状态。\r\n\r\n4.\r\n![bg2013042404.jpg][4]\r\n一个车间里，可以有很多工人。他们协同完成一个任务。\r\n\r\n5.\r\n![bg2013042405.jpg][5]\r\n线程就好比车间里的工人。一个进程可以包括多个线程。\r\n\r\n6.\r\n![bg2013042406.png][6]\r\n车间的空间是工人们共享的，比如许多房间是每个工人都可以进出的。这象征一个进程的内存空间是共享的，每个线程都可以使用这些共享内存。\r\n\r\n7.\r\n![bg2013042407.jpg][7]\r\n可是，每间房间的大小不同，有些房间最多只能容纳一个人，比如厕所。里面有人的时候，其他人就不能进去了。这代表一个线程使用某些共享内存时，其他线程必须等它结束，才能使用这一块内存。\r\n\r\n8.\r\n![bg2013042408.jpg][8]\r\n一个防止他人进入的简单方法，就是门口加一把锁。先到的人锁上门，后到的人看到上锁，就在门口排队，等锁打开再进去。这就叫\"互斥锁\"（Mutual exclusion，缩写 Mutex），防止多个线程同时读写某一块内存区域。\r\n\r\n9.\r\n![bg2013042409.jpg][9]\r\n还有些房间，可以同时容纳n个人，比如厨房。也就是说，如果人数大于n，多出来的人只能在外面等着。这好比某些内存区域，只能供给固定数目的线程使用。\r\n\r\n10.\r\n![bg2013042410.jpg][10]\r\n这时的解决方法，就是在门口挂n把钥匙。进去的人就取一把钥匙，出来时再把钥匙挂回原处。后到的人发现钥匙架空了，就知道必须在门口排队等着了。这种做法叫做\"信号量\"（Semaphore），用来保证多个线程不会互相冲突。\r\n\r\n不难看出，mutex是semaphore的一种特殊情况（n=1时）。也就是说，完全可以用后者替代前者。但是，因为mutex较为简单，且效率高，所以在必须保证资源独占的情况下，还是采用这种设计。\r\n\r\n11.\r\n操作系统的设计，因此可以归结为三点：\r\n\r\n（1）以多进程形式，允许多个任务同时运行；\r\n\r\n（2）以多线程形式，允许单个任务分成不同的部分运行；\r\n\r\n（3）提供协调机制，一方面防止进程之间和线程之间产生冲突，另一方面允许进程之间和线程之间共享资源。\r\n\r\n（完）\r\n文章来源:[http://www.ruanyifeng.com/blog/2013/04/processes_and_threads.html][11]\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2020/06/1107498796.jpg\r\n  [2]: http://www.phpmaster.cn/usr/uploads/2020/06/1207249356.png\r\n  [3]: http://www.phpmaster.cn/usr/uploads/2020/06/3840644866.jpg\r\n  [4]: http://www.phpmaster.cn/usr/uploads/2020/06/4070422856.jpg\r\n  [5]: http://www.phpmaster.cn/usr/uploads/2020/06/1869771675.jpg\r\n  [6]: http://www.phpmaster.cn/usr/uploads/2020/06/1855669491.png\r\n  [7]: http://www.phpmaster.cn/usr/uploads/2020/06/1169034069.jpg\r\n  [8]: http://www.phpmaster.cn/usr/uploads/2020/06/1534665868.jpg\r\n  [9]: http://www.phpmaster.cn/usr/uploads/2020/06/2856383427.jpg\r\n  [10]: http://www.phpmaster.cn/usr/uploads/2020/06/259790879.jpg\r\n  [11]: http://www.ruanyifeng.com/blog/2013/04/processes_and_threads.html', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '1492', '365', '0');
INSERT INTO `typecho_contents` VALUES ('113', 'QQ截图20200731151440.png', 'QQ截图20200731151440-png', '1596179697', '1596179697', 'a:5:{s:4:\"name\";s:26:\"QQ截图20200731151440.png\";s:4:\"path\";s:35:\"/usr/uploads/2020/07/3511580941.png\";s:4:\"size\";i:247655;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '99', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('114', 'u=15033599,1742744923&amp;fm=26&amp;gp=0.jpg', 'u-15033599-1742744923-fm-26-gp-0-jpg', '1597580438', '1597580438', 'a:5:{s:4:\"name\";s:36:\"u=15033599,1742744923&fm=26&gp=0.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/08/2380465461.jpg\";s:4:\"size\";i:18384;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '115', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('115', '58到家数据库30条军规解读', '115', '1597580400', '1597715721', '<!--markdown-->![u=15033599,1742744923&fm=26&gp=0.jpg][1]\r\n军规适用场景：并发量大、数据量大的互联网业务\r\n\r\n军规：介绍内容\r\n\r\n解读：讲解原因，解读比军规更重要\r\n\r\n一、基础规范\r\n\r\n（1）必须使用InnoDB存储引擎\r\n\r\n解读：支持事务、行级锁、并发性能更好、CPU及内存缓存页优化使得资源利用率更高\r\n\r\n（2）必须使用UTF8字符集\r\n\r\n解读：万国码，无需转码，无乱码风险，节省空间\r\n\r\n（3）数据表、数据字段必须加入中文注释\r\n\r\n解读：N年后谁tm知道这个r1,r2,r3字段是干嘛的\r\n\r\n（4）禁止使用存储过程、视图、触发器、Event\r\n\r\n解读：高并发大数据的互联网业务，架构设计思路是“解放数据库CPU，将计算转移到服务层”，并发量大的情况下，这些功能很可能将数据库拖死，业务逻辑放到服务层具备更好的扩展性，能够轻易实现“增机器就加性能”。数据库擅长存储与索引，CPU计算还是上移吧\r\n\r\n（5）禁止存储大文件或者大照片\r\n\r\n解读：为何要让数据库做它不擅长的事情？大文件和照片存储在文件系统，数据库里存URI多好\r\n\r\n二、命名规范\r\n\r\n（6）只允许使用内网域名，而不是ip连接数据库\r\n\r\n（7）线上环境、开发环境、测试环境数据库内网域名遵循命名规范\r\n\r\n业务名称：xxx\r\n\r\n线上环境：dj.xxx.db\r\n\r\n开发环境：dj.xxx.rdb\r\n\r\n测试环境：dj.xxx.tdb\r\n\r\n从库在名称后加-s标识，备库在名称后加-ss标识\r\n\r\n线上从库：dj.xxx-s.db\r\n\r\n线上备库：dj.xxx-sss.db\r\n\r\n（8）库名、表名、字段名：小写，下划线风格，不超过32个字符，必须见名知意，禁止拼音英文混用\r\n\r\n（9）表名t_xxx，非唯一索引名idx_xxx，唯一索引名uniq_xxx\r\n\r\n三、表设计规范\r\n\r\n（10）单实例表数目必须小于500\r\n\r\n（11）单表列数目必须小于30\r\n\r\n（12）表必须有主键，例如自增主键\r\n\r\n解读：\r\n\r\na）主键递增，数据行写入可以提高插入性能，可以避免page分裂，减少表碎片提升空间和内存的使用\r\n\r\nb）主键要选择较短的数据类型， Innodb引擎普通索引都会保存主键的值，较短的数据类型可以有效的减少索引的磁盘空间，提高索引的缓存效率\r\n\r\nc） 无主键的表删除，在row模式的主从架构，会导致备库夯住\r\n\r\n（13）禁止使用外键，如果有外键完整性约束，需要应用程序控制\r\n\r\n解读：外键会导致表与表之间耦合，update与delete操作都会涉及相关联的表，十分影响sql 的性能，甚至会造成死锁。高并发情况下容易造成数据库性能，大数据高并发业务场景数据库使用以性能优先\r\n\r\n四、字段设计规范\r\n\r\n（14）必须把字段定义为NOT NULL并且提供默认值\r\n\r\n解读：\r\n\r\na）null的列使索引/索引统计/值比较都更加复杂，对MySQL来说更难优化\r\n\r\nb）null 这种类型MySQL内部需要进行特殊处理，增加数据库处理记录的复杂性；同等条件下，表中有较多空字段的时候，数据库的处理性能会降低很多\r\n\r\nc）null值需要更多的存储空，无论是表还是索引中每行中的null的列都需要额外的空间来标识\r\n\r\nd）对null 的处理时候，只能采用is null或is not null，而不能采用=、in、<、<>、!=、not in这些操作符号。如：where name!=’shenjian’，如果存在name为null值的记录，查询结果就不会包含name为null值的记录\r\n\r\n（15）禁止使用TEXT、BLOB类型\r\n\r\n解读：会浪费更多的磁盘和内存空间，非必要的大量的大字段查询会淘汰掉热数据，导致内存命中率急剧降低，影响数据库性能\r\n\r\n（16）禁止使用小数存储货币\r\n\r\n解读：使用整数吧，小数容易导致钱对不上\r\n\r\n（17）必须使用varchar(20)存储手机号\r\n\r\n解读：\r\n\r\na）涉及到区号或者国家代号，可能出现+-()\r\n\r\nb）手机号会去做数学运算么？\r\n\r\nc）varchar可以支持模糊查询，例如：like“138%”\r\n\r\n（18）禁止使用ENUM，可使用TINYINT代替\r\n\r\n解读：\r\n\r\na）增加新的ENUM值要做DDL操作\r\n\r\nb）ENUM的内部实际存储就是整数，你以为自己定义的是字符串？\r\n\r\n五、索引设计规范\r\n\r\n（19）单表索引建议控制在5个以内\r\n\r\n（20）单索引字段数不允许超过5个\r\n\r\n解读：字段超过5个时，实际已经起不到有效过滤数据的作用了\r\n\r\n（21）禁止在更新十分频繁、区分度不高的属性上建立索引\r\n\r\n解读：\r\n\r\na）更新会变更B+树，更新频繁的字段建立索引会大大降低数据库性能\r\n\r\nb）“性别”这种区分度不大的属性，建立索引是没有什么意义的，不能有效过滤数据，性能与全表扫描类似\r\n\r\n（22）建立组合索引，必须把区分度高的字段放在前面\r\n\r\n解读：能够更加有效的过滤数据\r\n\r\n六、SQL使用规范\r\n\r\n（23）禁止使用SELECT *，只获取必要的字段，需要显示说明列属性\r\n\r\n解读：\r\n\r\na）读取不需要的列会增加CPU、IO、NET消耗\r\n\r\nb）不能有效的利用覆盖索引\r\n\r\nc）使用SELECT *容易在增加或者删除字段后出现程序BUG\r\n\r\n（24）禁止使用INSERT INTO t_xxx VALUES(xxx)，必须显示指定插入的列属性\r\n\r\n解读：容易在增加或者删除字段后出现程序BUG\r\n（25）禁止使用属性隐式转换\r\n\r\n解读：SELECT uid FROM t_user WHERE phone=13812345678 会导致全表扫描，而不能命中phone索引，猜猜为什么？（这个线上问题不止出现过一次）\r\n\r\n（26）禁止在WHERE条件的属性上使用函数或者表达式\r\n\r\n解读：SELECT uid FROM t_user WHERE from_unixtime(day)>=\'2017-02-15\' 会导致全表扫描\r\n\r\n正确的写法是：SELECT uid FROM t_user WHERE day>= unix_timestamp(\'2017-02-15 00:00:00\')\r\n\r\n（27）禁止负向查询，以及%开头的模糊查询\r\n\r\n解读：\r\n\r\na）负向查询条件：NOT、!=、<>、!<、!>、NOT IN、NOT LIKE等，会导致全表扫描\r\n\r\nb）%开头的模糊查询，会导致全表扫描\r\n\r\n（28）禁止大表使用JOIN查询，禁止大表使用子查询\r\n\r\n解读：会产生临时表，消耗较多内存与CPU，极大影响数据库性能\r\n\r\n（29）禁止使用OR条件，必须改为IN查询\r\n\r\n解读：旧版本Mysql的OR查询是不能命中索引的，即使能命中索引，为何要让数据库耗费更多的CPU帮助实施查询优化呢？\r\n\r\n（30）应用程序必须捕获SQL异常，并有相应处理\r\n\r\n总结：大数据量高并发的互联网业务，极大影响数据库性能的都不让用，不让用哟。\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2020/08/2380465461.jpg', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '1113', '358', '0');
INSERT INTO `typecho_contents` VALUES ('116', 'mysql8.0绿色版安装~卸载', '116', '1597715700', '1597733548', '<!--markdown-->![u=865625540,69448143&fm=26&gp=0.jpg][1]\r\n第一步：去官网下载安装\r\n[https://dev.mysql.com/downloads/mysql/][2]\r\n也可以去百度云下载\r\n链接：[https://pan.baidu.com/s/1DFCzW1_AekY0xVlyxTUaiA][3] \r\n提取码：hjn0 \r\n第二步：先解压，然后在mysql目录下创建一个my.ini文件，\r\n更改my.ini文件里面的两行安装目录，\r\n第二行加上\\data，\r\nmy.ini文件不能多或少一个符号，\r\n\r\n  \r\n\r\n      [mysqld]\r\n    # 设置3306端口\r\n    port=3306\r\n    # 设置mysql的安装目录\r\n    basedir=E:\\mysql\r\n    # 设置mysql数据库的数据的存放目录\r\n    datadir=E:\\mysql\\data\r\n    # 允许最大连接数\r\n    max_connections=200\r\n    # 允许连接失败的次数。这是为了防止有人从该主机试图攻击数据库系统\r\n    max_connect_errors=10\r\n    # 服务端使用的字符集默认为UTF8\r\n    character-set-server=utf8\r\n    # 创建新表时将使用的默认存储引擎\r\n    default-storage-engine=INNODB\r\n    # 默认使用“mysql_native_password”插件认证\r\n    default_authentication_plugin=mysql_native_password\r\n    \r\n    [mysql]\r\n    # 设置mysql客户端默认字符集\r\n    default-character-set=utf8\r\n    [client]\r\n    # 设置mysql客户端连接服务端时默认使用的端口\r\n    port=3306\r\n    default-character-set=utf8\r\n\r\n第三步：path（环境变量里面）加上mysql路径（/bin）。\r\n\r\n\r\n第四步：进入命令指示符（cmd），\r\n输入mysqld --initialize-insecure --user=mysql，\r\n再输入mysqld -install，\r\n出现Service successfully installed.表示配置完成\r\n启动数据库net start mysql，也可以通过Windows服务启动mysql服务\r\n输入mysql -u root -p，不用输入密码直接回车\r\n出现mysql>表示配置完成\r\n输入alter user user() identified by \"密码\";\r\n输入net stop mysql关闭数据库\r\n\r\n第五步：通过命令来测试mysql客户端是否可以连接服务器\r\n\r\n    mysql -uroot -p\r\n    密码:****\r\n\r\nMysql绿色版卸载\r\n  1、关闭Mysql服务\r\n\r\n         关闭服务可以使用命令行,也可以在「任务管理器」的「服务」里进行关闭，也可以用命令行关闭\r\n\r\n  管理员身份运行命令行（cmd）: net stop mysql\r\n![2018091009352048.jpg][4]\r\n2、删除Mysql的注册表\r\n\r\n      （1）Win+R打开运行界面，在输入框中输入 regedit 进入系统注册表窗口\r\n![20180910094158200.png][5]\r\n（2）分别在以下目录中找到 MySQL 的注册表，鼠标右键直接删除MySQL目录中的 EventMessageFile 和 TypesSupported 两个文件就好了,如果对应的目录中没有,就不用删除了，也可以搜索注册表: 在系统注册表窗口选择「编辑」 — 选择「查找」 — 输入 「MySQL」进行查找,将找到的MySQL目录中的 EventMessageFile 和 TypesSupported 两个文件进行删除\r\n\r\n    HKEY_LOCAL_MACHINE/SYSTEM/ControlSet001/Services/Eventlog/Application/MySQL\r\n    HKEY_LOCAL_MACHINE/SYSTEM/ControlSet002/Services/Eventlog/Application/MySQL\r\n    HKEY_LOCAL_MACHINE/SYSTEM/CurrentControlSet/Services/Eventlog/Application/MySQL\r\n\r\n![20180910201015150.png][6]\r\n3、移除Mysql服务\r\n\r\n     （1）以管理员身份使用命令行(cmd)进入MySQL的 bin 目录下\r\n\r\n     （2）执行移除 MySQL服务的命令 : mysqld -remove\r\n\r\n     （3）当看到有Service successfully removed时，则表示移除Mysql服务成功\r\n![20180910141540317.jpg][7]\r\n 4、删除Mysql文件\r\n\r\n      将Mysql安装目录下的文件全部删除即可\r\n![20180910141050551.jpg][8]\r\n\r\nOK,至此,MySQL绿色版卸载工作全部完成.\r\n修改密码：\r\n\r\n1. 方法1\r\n\r\n    格式：mysql> set password for 用户名@localhost = password(\'新密码\'); \r\n\r\n    例子：mysql> set password for root@localhost = password(\'123\'); \r\n\r\n2. 方法2\r\n\r\n    格式：mysqladmin -u用户名 -p旧密码 password 新密码 \r\n\r\n    例子：mysqladmin -uroot -p123456 password 123 \r\n\r\n3. 方法3\r\n\r\n    use mysql;\r\n\r\n   update user set password=password(\'123\') where user=\'root\' and host=\'localhost\'; \r\n\r\n   flush privileges; \r\n  [1]: http://www.phpmaster.cn/usr/uploads/2020/08/3746198198.jpg\r\n  [2]: https://dev.mysql.com/downloads/mysql/\r\n  [3]: https://pan.baidu.com/s/1DFCzW1_AekY0xVlyxTUaiA\r\n  [4]: http://www.phpmaster.cn/usr/uploads/2020/08/2383583954.jpg\r\n  [5]: http://www.phpmaster.cn/usr/uploads/2020/08/523964794.png\r\n  [6]: http://www.phpmaster.cn/usr/uploads/2020/08/649314890.png\r\n  [7]: http://www.phpmaster.cn/usr/uploads/2020/08/2353060700.jpg\r\n  [8]: http://www.phpmaster.cn/usr/uploads/2020/08/2926529322.jpg', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '1647', '543', '0');
INSERT INTO `typecho_contents` VALUES ('117', 'u=865625540,69448143&amp;fm=26&amp;gp=0.jpg', 'u-865625540-69448143-fm-26-gp-0-jpg', '1597716154', '1597716154', 'a:5:{s:4:\"name\";s:35:\"u=865625540,69448143&fm=26&gp=0.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/08/3746198198.jpg\";s:4:\"size\";i:79651;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:9:\"image/png\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '116', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('118', '2018091009352048.jpg', '2018091009352048-jpg', '1597716257', '1597716257', 'a:5:{s:4:\"name\";s:20:\"2018091009352048.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/08/2383583954.jpg\";s:4:\"size\";i:29180;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '2', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '116', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('119', '20180910094158200.png', '20180910094158200-png', '1597716278', '1597716278', 'a:5:{s:4:\"name\";s:21:\"20180910094158200.png\";s:4:\"path\";s:34:\"/usr/uploads/2020/08/523964794.png\";s:4:\"size\";i:8922;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '3', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '116', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('120', '20180910201015150.png', '20180910201015150-png', '1597716345', '1597716345', 'a:5:{s:4:\"name\";s:21:\"20180910201015150.png\";s:4:\"path\";s:34:\"/usr/uploads/2020/08/649314890.png\";s:4:\"size\";i:22383;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '4', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '116', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('121', '20180910141540317.jpg', '20180910141540317-jpg', '1597716384', '1597716384', 'a:5:{s:4:\"name\";s:21:\"20180910141540317.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/08/2353060700.jpg\";s:4:\"size\";i:34111;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '5', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '116', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('122', '20180910141050551.jpg', '20180910141050551-jpg', '1597716416', '1597716416', 'a:5:{s:4:\"name\";s:21:\"20180910141050551.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/08/2926529322.jpg\";s:4:\"size\";i:63429;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '6', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '116', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('123', 'MySql 8.0.19 权限分配', '123', '1597761600', '1616253584', '<!--markdown-->\r\n问题一：You have an error in your SQL syntax ---语法问题\r\n问题二：You are not allowed to create a user with GRANT ---权限问题\r\n问题三：No database selected ---没选择数据库\r\n\r\n创建数据库分配权限演示\r\n问题一：You have an error in your SQL syntax —语法问题\r\nMySql8.0.19 版本分配权限这有了一些改变，不需要后面的identified by \'123456a\'了\r\n\r\n    mysql> grant all privileges on sonar_scan.* to \'sonar\'@\'%\' identified by \'123456\r\n    a\';\r\n    ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that\r\n    corresponds to your MySQL server version for the right syntax to use near \'ident\r\n    ified by \'123456a\'\' at line 1\r\n\r\n分配权限\r\n\r\n    mysql> grant all privileges on sonar_scan.* to \'sonar\'@\'%\';\r\n    Query OK, 0 rows affected (0.24 sec)\r\n\r\n问题二：You are not allowed to create a user with GRANT —权限问题\r\n分配权限，% 代表全域，如果要远程使用数据库的话必须分配这个权限。\r\n\r\n    mysql> grant all privileges on sonar_scan.* to \'sonar\'@\'%\';\r\n    Query OK, 0 rows affected (0.24 sec)\r\n\r\n如果分配了全域，这个时候再分配本地就会报错，其实 % 已经包含 localhost 了。\r\n\r\n    mysql> grant all privileges on sonar_scan.* to \'sonar\'@\'localhost\';\r\n    ERROR 1410 (42000): You are not allowed to create a user with GRANT\r\n\r\n如果硬要是分配本地的话，要执这么一句就好了。要换回全域，在把 host 改成 % 就好了。\r\n\r\n    mysql>  update user set host=\'localhost\' where user=\'sonar\';\r\n    Query OK, 1 row affected (0.53 sec)\r\n    Rows matched: 1  Changed: 1  Warnings: 0\r\n\r\n问题三：No database selected —没选择数据库\r\n用一句 use mysql 就能解决问题了。\r\n\r\n    mysql> update user set host=\'localhost\' where user=\'sonar\';\r\n    ERROR 1046 (3D000): No database selected\r\n    mysql> use mysql;\r\n    Database changed\r\n    mysql>  update user set host=\'localhost\' where user=\'sonar\';\r\n    Query OK, 1 row affected (0.53 sec)\r\n    Rows matched: 1  Changed: 1  Warnings: 0\r\n\r\n创建数据库分配权限演示\r\n创建数据库\r\n\r\n    mysql> create database sonar_scan default character set utf8 collate utf8_genera\r\n    l_ci;\r\n    Query OK, 1 row affected, 2 warnings (0.19 sec)\r\n\r\n创建用户\r\n\r\n    mysql> create user \'sonar\' identified by \'123456a\';\r\n    Query OK, 0 rows affected (0.11 sec)\r\n\r\n分配权限\r\n\r\n    mysql> grant all privileges on sonar_scan.* to \'sonar\'@\'%\';\r\n\r\n    Query OK, 0 rows affected (0.24 sec)\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2020/08/3598121884.jpg', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '1257', '400', '0');
INSERT INTO `typecho_contents` VALUES ('124', 'quanxian.jpg', 'quanxian-jpg', '1597761765', '1597761765', 'a:5:{s:4:\"name\";s:12:\"quanxian.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/08/3598121884.jpg\";s:4:\"size\";i:12161;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '123', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('125', '5db6a7b65eb33376.jpg', '5db6a7b65eb33376-jpg', '1597934792', '1597934792', 'a:5:{s:4:\"name\";s:20:\"5db6a7b65eb33376.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/08/3347035301.jpg\";s:4:\"size\";i:46432;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '126', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('126', 'PEP 8 — Python代码样式指南', '126', '1597934760', '1616253571', '<!--markdown-->\r\nPython 的代码风格由 PEP 8 描述。这个文档描述了 Python 编程风格的方方面面。在遵守这个文档的条件下，不同程序员编写的 Python 代码可以保持最大程度的相似风格。这样就易于阅读，易于在程序员之间交流。\r\n1. 命名风格\r\n\r\n总体原则，新编代码必须按下面命名风格进行，现有库的编码尽量保持风格。\r\n尽量以免单独使用小写字母\'l\'，大写字母\'O\'，以及大写字母\'I\'等容易混淆的字母。\r\n模块命名尽量短小，使用全部小写的方式，可以使用下划线。\r\n包命名尽量短小，使用全部小写的方式，不可以使用下划线。\r\n类的命名使用CapWords的方式，模块内部使用的类采用_CapWords的方式。\r\n异常命名使用CapWords+Error后缀的方式。\r\n全局变量尽量只在模块内有效，类似C语言中的static。实现方法有两种，一是__all__机制;二是前缀一个下划线。对于不会发生改变的全局变量，使用大写加下划线。\r\n函数命名使用全部小写的方式，可以使用下划线。\r\n常量命名使用全部大写的方式，可以使用下划线。\r\n使用 has 或 is 前缀命名布尔元素，如: is_connect = True; has_member = False\r\n用复数形式命名序列。如：members = [\'user_1\', \'user_2\']\r\n用显式名称命名字典，如：person_address = {\'user_1\':\'10 road WD\', \'user_2\' : \'20 street huafu\'}\r\n避免通用名称。诸如 list, dict, sequence 或者 element 这样的名称应该避免。又如os, sys 这种系统已经存在的名称应该避免。\r\n类的属性（方法和变量）命名使用全部小写的方式，可以使用下划线。\r\n对于基类而言，可以使用一个 Base 或者 Abstract 前缀。如BaseCookie、AbstractGroup\r\n内部使用的类、方法或变量前，需加前缀\'_\'表明此为内部使用的。虽然如此，但这只是程序员之间的约定而非语法规定，用于警告说明这是一个私有变量，外部类不要去访问它。但实际上，外部类还是可以访问到这个变量。import不会导入以下划线开头的对象。\r\n类的属性若与关键字名字冲突，后缀一下划线，尽量不要使用缩略等其他方式。\r\n双前导下划线用于命名class属性时，会触发名字重整；双前导和后置下划线存在于用户控制的名字空间的\"magic\"对象或属性。\r\n为避免与子类属性命名冲突，在类的一些属性前，前缀两条下划线。比如：类Foo中声明__a,访问时，只能通过Foo._Foo__a，避免歧义。如果子类也叫Foo，那就无能为力了。\r\n类的方法第一个参数必须是self，而静态方法第一个参数必须是cls。\r\n一般的方法、函数、变量需注意，如非必要，不要连用两个前导和后置的下线线。两个前导下划线会导致变量在解释期间被更名。两个前导下划线会导致函数被理解为特殊函数，比如操作符重载等。\r\n2 关于参数\r\n\r\n要用断言来实现静态类型检测。断言可以用于检查参数，但不应仅仅是进行静态类型检测。 Python 是动态类型语言，静态类型检测违背了其设计思想。断言应该用于避免函数不被毫无意义的调用。\r\n不要滥用 *args 和 **kwargs。*args 和 **kwargs 参数可能会破坏函数的健壮性。它们使签名变得模糊，而且代码常常开始在不应该的地方构建小的参数解析器\r\n3 代码编排\r\n\r\n缩进。优先使用4个空格的缩进（编辑器都可以完成此功能），其次可使用Tap，但坚决不能混合使用Tap和空格。\r\n每行最大长度79，换行可以使用反斜杠，最好使用圆括号。换行点要在操作符的后边敲回车。\r\n类和top-level函数定义之间空两行；类中的方法定义之间空一行；函数内逻辑无关段落之间空一行；其他地方尽量不要再空行。\r\n一个函数 : 不要超过 30 行代码, 即可显示在一个屏幕类，可以不使用垂直游标即可看到整个函数；一个类 : 不要超过 200 行代码，不要有超过 10 个方法；一个模块 不要超过 500 行。\r\n4. 文档编排\r\n\r\n模块内容的顺序：模块说明和docstring—import—globals&constants—其他定义。其中import部分，又按标准、三方和自己编写顺序依次排放，之间空一行。\r\n不要在一句import中多个库，比如import os, sys不推荐。\r\n如果采用from XX import XX引用库，可以省略‘module.’。若是可能出现命名冲突，这时就要采用import XX。\r\n5. 空格的使用\r\n\r\n总体原则，避免不必要的空格。\r\n各种右括号前不要加空格。\r\n函数的左括号前不要加空格。如Func(1)。\r\n序列的左括号前不要加空格。如list[2]。\r\n逗号、冒号、分号前不要加空格。\r\n操作符（=/+=/-+/==/</>/!=/<>/<=/>=/in/not in/is/is not/and/or/not)左右各加一个空格，不要为了对齐增加空格。如果操作符有优先级的区别，可考虑在低优先级的操作符两边添加空格。如：hypot2 = x*x + y*y;  c = (a+b) * (a-b)\r\n函数默认参数使用的赋值符左右省略空格。\r\n不要将多句语句写在同一行，尽管使用‘；’允许。\r\nif/for/while语句中，即使执行语句只有一句，也必须另起一行。\r\n6. 注释\r\n\r\n总体原则，错误的注释不如没有注释。所以当一段代码发生变化时，第一件事就是要修改注释！避免无谓的注释\r\n注释必须使用英文，最好是完整的句子，首字母大写，句后要有结束符，结束符后跟两个空格，开始下一句。如果是短语，可以省略结束符。\r\n行注释：在一句代码后加注释，但是这种方式尽量少使用。。比如：x = x + 1 # Increment\r\n块注释：在一段代码前增加的注释。在‘#’后加一空格。段落之间以只有‘#’的行间隔。比如：\r\n# Description : Module config.\r\n#\r\n# Input : None\r\n#\r\n# Output : None\r\n 7. 文档描述\r\n\r\n为所有的共有模块、函数、类、方法写docstrings；非共有的没有必要，但是可以写注释（在def的下一行）。\r\n如果docstring要换行，参考如下例子,详见PEP 257\r\n\"\"\"Return a foobang\r\n \r\nOptional plotz says to frobnicate the bizbaz first.\r\n \r\n\"\"\"\r\n8. 编码建议\r\n\r\n编码中考虑到其他python实现的效率等问题，比如运算符‘+’在CPython（Python）中效率很高，都是Jython中却非常低，所以应该采用.join()的方式。\r\n与None之类的单件比较，尽可能使用‘is’‘is not’，绝对不要使用‘==’，比如if x is not None 要优于if x。\r\n使用startswith() and endswith()代替切片进行序列前缀或后缀的检查。比如：建议使用if foo.startswith(\'bar\'): 而非if foo[:3] == \'bar\':\r\n使用isinstance()比较对象的类型。比如：建议使用if isinstance(obj, int): 而非if type(obj) is type(1):\r\n判断序列空或不空，有如下规则：建议使用if [not] seq: 而非if [not] len(seq)\r\n字符串不要以空格收尾。\r\n二进制数据判断使用 if boolvalue的方式。\r\n使用基于类的异常，每个模块或包都有自己的异常类，此异常类继承自Exception。错误型的异常类应添加\"Error\"后缀，非错误型的异常类无需添加。\r\n异常中不要使用裸露的except，except后跟具体的exceptions。\r\n异常中try的代码尽可能少。比如：\r\n\r\n    try:\r\n        value = collection[key]\r\n    except KeyError:\r\n        return key_not_found(key)\r\n    else:\r\n        return handle_value(value)\r\n\r\nWhat Doesn\'t Kill Me Makes Me Stronger\r\n\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2020/08/3347035301.jpg', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '1125', '387', '0');
INSERT INTO `typecho_contents` VALUES ('127', '路线', '127', '1597980540', '1597981275', '<!--markdown-->《汉服超模大赛》\r\n超详细路线图~你一定能找到！\r\n![1.png][1]\r\n![2.png][2]\r\n![3.png][3]\r\n![4.png][4]\r\n![5.png][5]\r\n![7.jpg][7]\r\n如若同袍还是不能找到位置\r\n请联系我们：\r\n如果电话占线请拨打下一个\r\n小可爱们：点击号码可以直接拨号哦！\r\n<a href=\"tel:17633835203\" class=\"call\">小七：17633835203</a>\r\n<a href=\"tel:13303758600\" class=\"call\">一一：13303758600</a>\r\n<a href=\"tel:18336461293\" class=\"call\">关山月：18336461293</a> \r\n<a href=\"tel:15333762115\" class=\"call\">芊月：15333762115</a>\r\n<a href=\"tel:18736126018\" class=\"call\">白衣卿相：18736126018</a>\r\n<a href=\"tel:18336316597\" class=\"call\">胖胖：18336316597</a>\r\n《郑州汉服社》</span>全体成员竭诚为您服务，为您的出行保驾护航！\r\n\r\n![6.jpg][6]\r\n\r\n  [1]: http://www.phpmaster.cn/usr/uploads/2020/08/2238094972.png\r\n  [2]: http://www.phpmaster.cn/usr/uploads/2020/08/1824901204.png\r\n  [3]: http://www.phpmaster.cn/usr/uploads/2020/08/2878712905.png\r\n  [4]: http://www.phpmaster.cn/usr/uploads/2020/08/591583115.png\r\n  [5]: http://www.phpmaster.cn/usr/uploads/2020/08/631377370.png\r\n  [6]: http://www.phpmaster.cn/usr/uploads/2020/08/369630929.jpg\r\n  [7]: http://www.phpmaster.cn/usr/uploads/2020/08/3664467797.jpg', '0', '1', null, 'page', 'hidden', null, '0', '1', '1', '1', '0', '1', '17', '0', '0');
INSERT INTO `typecho_contents` VALUES ('128', '1.png', '1-png', '1597981100', '1597981100', 'a:5:{s:4:\"name\";s:5:\"1.png\";s:4:\"path\";s:35:\"/usr/uploads/2020/08/2238094972.png\";s:4:\"size\";i:1185481;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '127', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('129', '2.png', '2-png', '1597981103', '1597981103', 'a:5:{s:4:\"name\";s:5:\"2.png\";s:4:\"path\";s:35:\"/usr/uploads/2020/08/1824901204.png\";s:4:\"size\";i:879174;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '2', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '127', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('130', '3.png', '3-png', '1597981104', '1597981104', 'a:5:{s:4:\"name\";s:5:\"3.png\";s:4:\"path\";s:35:\"/usr/uploads/2020/08/2878712905.png\";s:4:\"size\";i:344856;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '3', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '127', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('131', '4.png', '4-png', '1597981108', '1597981108', 'a:5:{s:4:\"name\";s:5:\"4.png\";s:4:\"path\";s:34:\"/usr/uploads/2020/08/591583115.png\";s:4:\"size\";i:1198234;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '4', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '127', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('132', '5.png', '5-png', '1597981113', '1597981113', 'a:5:{s:4:\"name\";s:5:\"5.png\";s:4:\"path\";s:34:\"/usr/uploads/2020/08/631377370.png\";s:4:\"size\";i:1225556;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '5', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '127', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('133', '6.jpg', '6-jpg-1', '1597981113', '1597981113', 'a:5:{s:4:\"name\";s:5:\"6.jpg\";s:4:\"path\";s:34:\"/usr/uploads/2020/08/369630929.jpg\";s:4:\"size\";i:11304;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '6', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '127', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('134', '7.jpg', '7-jpg-2', '1597981114', '1597981114', 'a:5:{s:4:\"name\";s:5:\"7.jpg\";s:4:\"path\";s:35:\"/usr/uploads/2020/08/3664467797.jpg\";s:4:\"size\";i:152527;s:4:\"type\";s:3:\"jpg\";s:4:\"mime\";s:10:\"image/jpeg\";}', '7', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '127', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('136', 'QQ截图20201201194233.png', 'QQ截图20201201194233-png', '1606822971', '1606822971', 'a:5:{s:4:\"name\";s:26:\"QQ截图20201201194233.png\";s:4:\"path\";s:35:\"/usr/uploads/2020/12/3817273585.png\";s:4:\"size\";i:14055;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '137', '1', '0', '0', '0');
INSERT INTO `typecho_contents` VALUES ('137', 'php实现万年历', '137', '1606822980', '1616253561', '<!--markdown-->\r\n\r\n   \r\n\r\n     <?php\r\n    header(\"content-type:text/html;charset=utf-8\");\r\n    $y = date(\"Y\");\r\n    $m = date(\"m\");\r\n    $day = date(\"d\");//1\r\n    $h = date(\"H\");\r\n    $min = date(\"i\");\r\n    $sec = date(\"s\");\r\n    // 判断是否接收到year,如果接收不到，默认当前年\r\n    $year = isset($_GET[\'year\'])?$_GET[\'year\']:date(\"Y\");\r\n    if(  (int)$year <1970 || (int)$year>=2038){\r\n        $year = date(\"Y\");\r\n    }\r\n    $month = isset($_GET[\'month\'])?$_GET[\'month\']:date(\"m\");\r\n    if((int)$month<1 || (int)$month>12){\r\n        $month = date(\"m\");\r\n    }\r\n    \r\n    // 1.这个月有几天？\r\n    $days = date(\"t\",mktime(0,0,0,$month,1,$year));\r\n    // 2. 每个月1号是周几？\r\n    $w = date(\"w\",mktime(0,0,0,$month,1,$year));\r\n    $premonth = $nextmonth = $month;\r\n    $preyear = $nextyear = $year;\r\n    if($month>1 && $month <12){ // 如果不是1月 也不是12 月\r\n        $premonth--;   // 上一月-1\r\n        $nextmonth++; // 下一月 +1\r\n    } // 如果当前是12月\r\n    else if($month == 12){\r\n        $nextmonth = 1;  // 下一月就是1月\r\n        $premonth--;    // 上一月就是当前月-1\r\n        $nextyear++;    // 下一年就是当前年+1\r\n    \r\n    }else if($month == 1){ // 如果当前是1月=\r\n        $premonth = 12; // 上一月就是12月\r\n        $nextmonth++;   // 下一月就是当前月+1\r\n        $preyear--;     // 上一年就是当年-1\r\n    }\r\n    ?>\r\n    <h3 align=\"center\">\r\n        <span><?php echo $y;?></span>-<span><?php echo $m;?></span>-<span><?php echo $day;?></span> <span><?php echo $h;?></span>:<span><?php echo $min;?></span>:<span><?php echo $sec;?></span>\r\n    </h3>\r\n    <?php\r\n    echo \"<table border=\'1\' width=\'400\' align=\'center\' cellpadding=\'0\' cellspacing=\'0\'>\";\r\n    \r\n    echo \"<caption>\r\n    <h3>\r\n    <a href=\'?month=$month&year=\".($year-1).\"\' >上一年</a>\r\n     <a href=\'?month=$premonth&year=$preyear\'>上一月</a> \r\n     {$year}年{$month}月\r\n    <a href=\'?month=$nextmonth&year=$nextyear\'>下一月</a>\r\n    <a href=\'?month=$month&year=\".($year+1).\"\'>下一年</a> \r\n    </h3>\r\n    </caption>\";\r\n    echo \"<tr>\r\n            <th>日</th>\r\n            <th>一</th>\r\n            <th>二</th>\r\n            <th>三</th>\r\n            <th>四</th>\r\n            <th>五</th>\r\n            <th>六</th>\r\n            </tr>\";\r\n    echo \"<tr>\";\r\n    // 输出空td\r\n    for($j=0;$j<$w;$j++){\r\n        echo \"<td></td>\";\r\n    }\r\n    for($i=1;$i<=$days;$i++,$j++){\r\n        if($j%7==0 && $j!=0){\r\n            echo \"</tr><tr>\";\r\n        }\r\n        if($i == $day && $month == $m && $year == $y){\r\n            echo \"<td bgcolor=\'red\' align=\'center\' valign=\'center\'>{$i}</td>\";\r\n        }else{\r\n            echo \"<td align=\'center\' valign=\'center\'>{$i}</td>\";\r\n        }\r\n    \r\n    }\r\n    echo \"</tr>\";\r\n    echo \"</table>\";\r\n    ?>\r\n    <script>\r\n        var tds = document.querySelectorAll(\'td\');\r\n        var l = tds.length;\r\n        for(var i=0;i<l;i++){\r\n            tds[i].onclick = function () {\r\n                // 所有的td恢复原状\r\n                for(var j=0;j<l;j++){\r\n                    tds[j].style.border = \"1px solid #000\";\r\n                }\r\n                this.style.border = \"1px solid #f00\";\r\n            }\r\n        }\r\n        function randColor(){\r\n            var r = Math.floor(Math.random()*256)\r\n            var g = Math.floor(Math.random()*256)\r\n            var b = Math.floor(Math.random()*256)\r\n            return \"rgb(\"+r+\",\"+g+\",\"+b+\")\"\r\n        }\r\n        var spans = document.querySelectorAll(\'span\');\r\n        var l = spans.length;\r\n        for(var i=0;i<l;i++){\r\n            spans[i].style.color = randColor();\r\n        }\r\n        // 动态时间\r\n        function show(){\r\n            var date = new Date();\r\n            var year = date.getFullYear();\r\n            var month = date.getMonth()+1;\r\n            var day = date.getDate();\r\n            day = day>9?day:\"0\"+day;\r\n            var hour = date.getHours();\r\n            var min  = date.getMinutes();\r\n            var sec = date.getSeconds();\r\n            spans[0].innerText = year;\r\n            spans[1].innerText = month;\r\n            spans[2].innerText = day;\r\n            spans[3].innerText = hour;\r\n            spans[4].innerText = min;\r\n            spans[5].innerText = sec;\r\n            // var randcolor = randColor();\r\n            for(var i=0;i<l;i++){\r\n                spans[i].style.color = randColor();\r\n            }\r\n        }\r\n        setInterval(show,1000);\r\n    </script>\r\n\r\n\r\n\r\n  [1]: http://www.zhangxuhui.com/usr/uploads/2020/12/3817273585.png', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '659', '341', '0');
INSERT INTO `typecho_contents` VALUES ('138', 'php常用小功能', '138', '1606901325', '1606901325', '<!--markdown-->    \r\n1.敏感词过滤\r\n\r\n    function badwordfun($str,$badword,$type=\"*\"){\r\n            return  strtr($str,array_combine($badword,array_fill(0,count($badword),$type)));\r\n        }', '0', '1', null, 'post', 'publish', null, '0', '1', '1', '1', '0', '1', '678', '357', '0');
INSERT INTO `typecho_contents` VALUES ('139', 'QQ截图20210319185844.png', 'QQ截图20210319185844-png', '1616251394', '1616251394', 'a:5:{s:4:\"name\";s:26:\"QQ截图20210319185844.png\";s:4:\"path\";s:35:\"/usr/uploads/2021/03/3593168272.png\";s:4:\"size\";i:2566;s:4:\"type\";s:3:\"png\";s:4:\"mime\";s:9:\"image/png\";}', '1', '1', null, 'attachment', 'publish', null, '0', '1', '0', '1', '0', '1', '0', '0', '0');

-- ----------------------------
-- Table structure for typecho_fields
-- ----------------------------
DROP TABLE IF EXISTS `typecho_fields`;
CREATE TABLE `typecho_fields` (
  `cid` int(10) unsigned NOT NULL,
  `name` varchar(200) NOT NULL,
  `type` varchar(8) DEFAULT 'str',
  `str_value` text,
  `int_value` int(10) DEFAULT '0',
  `float_value` float DEFAULT '0',
  PRIMARY KEY (`cid`,`name`),
  KEY `int_value` (`int_value`),
  KEY `float_value` (`float_value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of typecho_fields
-- ----------------------------
INSERT INTO `typecho_fields` VALUES ('14', 'thumb', 'str', 'http://www.phpmaster.cn/usr/uploads/2019/10/1179308819.gif', '0', '0');
INSERT INTO `typecho_fields` VALUES ('46', 'thumb', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('37', 'keywords', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('37', 'thumb', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('37', 'desc', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('46', 'desc', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('46', 'keywords', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('46', 'video', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('46', 'sharePic', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('137', 'sharePic', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('137', 'video', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('137', 'keywords', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('137', 'desc', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('137', 'thumb', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('126', 'thumb', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('126', 'desc', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('126', 'keywords', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('126', 'video', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('126', 'sharePic', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('123', 'thumb', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('123', 'desc', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('123', 'keywords', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('123', 'video', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('123', 'sharePic', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('37', 'video', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('37', 'sharePic', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('26', 'thumb', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('26', 'desc', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('26', 'keywords', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('26', 'video', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('26', 'sharePic', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('25', 'thumb', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('25', 'desc', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('25', 'keywords', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('25', 'video', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('25', 'sharePic', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('59', 'thumb', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('59', 'desc', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('59', 'keywords', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('59', 'video', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('59', 'sharePic', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('61', 'thumb', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('61', 'desc', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('61', 'keywords', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('61', 'video', 'str', '', '0', '0');
INSERT INTO `typecho_fields` VALUES ('61', 'sharePic', 'str', '', '0', '0');

-- ----------------------------
-- Table structure for typecho_links
-- ----------------------------
DROP TABLE IF EXISTS `typecho_links`;
CREATE TABLE `typecho_links` (
  `lid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'links表主键',
  `name` varchar(200) DEFAULT NULL COMMENT 'links名称',
  `url` varchar(200) DEFAULT NULL COMMENT 'links网址',
  `sort` varchar(200) DEFAULT NULL COMMENT 'links分类',
  `image` varchar(200) DEFAULT NULL COMMENT 'links图片',
  `description` varchar(200) DEFAULT NULL COMMENT 'links描述',
  `user` varchar(200) DEFAULT NULL COMMENT '自定义',
  `order` int(10) unsigned DEFAULT '0' COMMENT 'links排序',
  PRIMARY KEY (`lid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of typecho_links
-- ----------------------------
INSERT INTO `typecho_links` VALUES ('1', '萌卜兔\'s Blog', 'https://racns.com/', '博客', 'https://racns.com/usr/images/logo.png', null, null, '2');
INSERT INTO `typecho_links` VALUES ('2', '全民vip视频在线解析', 'http://qmaile.com/', null, null, null, null, '1');
INSERT INTO `typecho_links` VALUES ('6', '飞之梦工作室', 'https://www.feizhimeng.com', null, null, null, null, '4');
INSERT INTO `typecho_links` VALUES ('5', '文轩设计之路', 'https://www.wenxuands.com/', null, null, null, null, '3');
INSERT INTO `typecho_links` VALUES ('8', '植物常识网', 'http://wozhidaole.com.cn/', null, null, null, null, '5');

-- ----------------------------
-- Table structure for typecho_metas
-- ----------------------------
DROP TABLE IF EXISTS `typecho_metas`;
CREATE TABLE `typecho_metas` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `type` varchar(32) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `count` int(10) unsigned DEFAULT '0',
  `order` int(10) unsigned DEFAULT '0',
  `parent` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`mid`),
  KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of typecho_metas
-- ----------------------------
INSERT INTO `typecho_metas` VALUES ('1', '心情随笔', 'default', 'category', '只是一个默认分类', '7', '1', '0');
INSERT INTO `typecho_metas` VALUES ('23', 'msyql', 'msyql', 'tag', null, '1', '0', '0');
INSERT INTO `typecho_metas` VALUES ('22', 'mysql', 'mysql', 'tag', null, '3', '0', '0');
INSERT INTO `typecho_metas` VALUES ('21', 'Python', 'Python', 'category', null, '5', '6', '0');
INSERT INTO `typecho_metas` VALUES ('6', '汉服', 'hanfu', 'category', '汉服', '7', '3', '0');
INSERT INTO `typecho_metas` VALUES ('20', 'PHP', 'PHP', 'category', null, '4', '5', '0');
INSERT INTO `typecho_metas` VALUES ('26', '云之泣', '云之泣', 'tag', null, '1', '0', '0');
INSERT INTO `typecho_metas` VALUES ('19', '技术杂谈', 'jishu', 'category', null, '6', '4', '0');
INSERT INTO `typecho_metas` VALUES ('9', '情感', '情感', 'tag', null, '2', '0', '0');
INSERT INTO `typecho_metas` VALUES ('10', '编程', '编程', 'tag', null, '2', '0', '0');
INSERT INTO `typecho_metas` VALUES ('11', '婚姻', '婚姻', 'tag', null, '1', '0', '0');
INSERT INTO `typecho_metas` VALUES ('12', '搞笑', '搞笑', 'tag', null, '1', '0', '0');
INSERT INTO `typecho_metas` VALUES ('13', '唯美', '唯美', 'tag', null, '1', '0', '0');
INSERT INTO `typecho_metas` VALUES ('14', '话题', '话题', 'tag', null, '0', '0', '0');
INSERT INTO `typecho_metas` VALUES ('15', '面试', '面试', 'tag', null, '1', '0', '0');
INSERT INTO `typecho_metas` VALUES ('24', '古风', 'gufeng', 'category', '古风音乐汇总', '2', '7', '0');
INSERT INTO `typecho_metas` VALUES ('25', '银临', '银临', 'tag', null, '2', '0', '0');
INSERT INTO `typecho_metas` VALUES ('27', 'python', 'python', 'tag', null, '1', '0', '0');
INSERT INTO `typecho_metas` VALUES ('28', '爬虫', '爬虫', 'tag', null, '3', '0', '0');
INSERT INTO `typecho_metas` VALUES ('29', '汉元素', '汉元素', 'tag', null, '1', '0', '0');
INSERT INTO `typecho_metas` VALUES ('30', '汉服', '汉服', 'tag', null, '1', '0', '0');
INSERT INTO `typecho_metas` VALUES ('31', '面试题', '面试题', 'tag', null, '1', '0', '0');
INSERT INTO `typecho_metas` VALUES ('32', 'MySql', 'mysql', 'category', 'mysql相关', '3', '8', '0');
INSERT INTO `typecho_metas` VALUES ('33', '规范', '规范', 'tag', null, '2', '0', '0');
INSERT INTO `typecho_metas` VALUES ('34', '安装', '安装', 'tag', null, '1', '0', '0');
INSERT INTO `typecho_metas` VALUES ('35', '卸载', '卸载', 'tag', null, '1', '0', '0');
INSERT INTO `typecho_metas` VALUES ('36', '权限', '权限', 'tag', null, '1', '0', '0');
INSERT INTO `typecho_metas` VALUES ('37', '代码', '代码', 'tag', null, '1', '0', '0');
INSERT INTO `typecho_metas` VALUES ('38', '标准', '标准', 'tag', null, '1', '0', '0');

-- ----------------------------
-- Table structure for typecho_meting
-- ----------------------------
DROP TABLE IF EXISTS `typecho_meting`;
CREATE TABLE `typecho_meting` (
  `id` binary(40) NOT NULL,
  `value` text NOT NULL,
  `date` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of typecho_meting
-- ----------------------------

-- ----------------------------
-- Table structure for typecho_options
-- ----------------------------
DROP TABLE IF EXISTS `typecho_options`;
CREATE TABLE `typecho_options` (
  `name` varchar(32) NOT NULL,
  `user` int(10) unsigned NOT NULL DEFAULT '0',
  `value` text,
  PRIMARY KEY (`name`,`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of typecho_options
-- ----------------------------
INSERT INTO `typecho_options` VALUES ('theme', '0', 'package');
INSERT INTO `typecho_options` VALUES ('theme:DUX-for-Typecho-master', '0', 'a:23:{s:7:\"logoUrl\";N;s:8:\"logotext\";N;s:11:\"DnsPrefetch\";s:7:\"disable\";s:6:\"fatext\";N;s:6:\"tj_cid\";N;s:11:\"smallbanner\";N;s:6:\"Slider\";s:11:\"SliderFalse\";s:10:\"slidercode\";N;s:12:\"sidebarBlock\";a:4:{i:0;s:15:\"ShowRecentPosts\";i:1;s:12:\"ShowCategory\";i:2;s:18:\"ShowRecentComments\";i:3;s:8:\"ShowTags\";}s:9:\"sidebarAD\";N;s:10:\"sitebar_fu\";N;s:12:\"socialweixin\";N;s:11:\"socialweibo\";N;s:13:\"socialtwitter\";N;s:7:\"src_add\";N;s:7:\"cdn_add\";N;s:13:\"default_thumb\";s:0:\"\";s:10:\"authordesc\";N;s:11:\"useHighline\";s:7:\"disable\";s:6:\"footad\";N;s:6:\"flinks\";N;s:5:\"fcode\";N;s:15:\"GoogleAnalytics\";N;}');
INSERT INTO `typecho_options` VALUES ('timezone', '0', '28800');
INSERT INTO `typecho_options` VALUES ('lang', '0', null);
INSERT INTO `typecho_options` VALUES ('charset', '0', 'UTF-8');
INSERT INTO `typecho_options` VALUES ('contentType', '0', 'text/html');
INSERT INTO `typecho_options` VALUES ('gzip', '0', '0');
INSERT INTO `typecho_options` VALUES ('generator', '0', 'Typecho 1.1/17.10.30');
INSERT INTO `typecho_options` VALUES ('title', '0', '旭辉博客');
INSERT INTO `typecho_options` VALUES ('description', '0', '关注网络发展,记录生活点滴');
INSERT INTO `typecho_options` VALUES ('keywords', '0', '个人博客,个人网站,IT博客,技术博客,个人主页 -张旭辉个人博客,旭辉博客');
INSERT INTO `typecho_options` VALUES ('rewrite', '0', '0');
INSERT INTO `typecho_options` VALUES ('frontPage', '0', 'recent');
INSERT INTO `typecho_options` VALUES ('frontArchive', '0', '0');
INSERT INTO `typecho_options` VALUES ('commentsRequireMail', '0', '1');
INSERT INTO `typecho_options` VALUES ('commentsWhitelist', '0', '0');
INSERT INTO `typecho_options` VALUES ('commentsRequireURL', '0', '0');
INSERT INTO `typecho_options` VALUES ('commentsRequireModeration', '0', '1');
INSERT INTO `typecho_options` VALUES ('plugins', '0', 'a:2:{s:9:\"activated\";a:11:{s:6:\"Sticky\";a:1:{s:7:\"handles\";a:1:{s:26:\"Widget_Archive:indexHandle\";a:1:{i:0;a:2:{i:0;s:13:\"Sticky_Plugin\";i:1;s:6:\"sticky\";}}}}s:6:\"Meting\";a:1:{s:7:\"handles\";a:6:{s:34:\"Widget_Abstract_Contents:contentEx\";a:1:{i:0;a:2:{i:0;s:13:\"Meting_Plugin\";i:1;s:13:\"playerReplace\";}}s:34:\"Widget_Abstract_Contents:excerptEx\";a:1:{i:0;a:2:{i:0;s:13:\"Meting_Plugin\";i:1;s:13:\"playerReplace\";}}s:21:\"Widget_Archive:header\";a:1:{i:0;a:2:{i:0;s:13:\"Meting_Plugin\";i:1;s:6:\"header\";}}s:21:\"Widget_Archive:footer\";a:1:{i:0;a:2:{i:0;s:13:\"Meting_Plugin\";i:1;s:6:\"footer\";}}s:27:\"admin/write-post.php:bottom\";a:1:{i:0;a:2:{i:0;s:13:\"Meting_Plugin\";i:1;s:9:\"addButton\";}}s:27:\"admin/write-page.php:bottom\";a:1:{i:0;a:2:{i:0;s:13:\"Meting_Plugin\";i:1;s:9:\"addButton\";}}}}s:7:\"YoduBGM\";a:1:{s:7:\"handles\";a:2:{s:21:\"Widget_Archive:header\";a:1:{i:0;a:2:{i:0;s:14:\"YoduBGM_Plugin\";i:1;s:6:\"header\";}}s:21:\"Widget_Archive:footer\";a:1:{i:0;a:2:{i:0;s:14:\"YoduBGM_Plugin\";i:1;s:6:\"footer\";}}}}s:5:\"Views\";a:1:{s:7:\"handles\";a:1:{s:27:\"Widget_Archive:beforeRender\";a:1:{i:0;a:2:{i:0;s:12:\"Views_Plugin\";i:1;s:12:\"viewsCounter\";}}}}s:6:\"Uptime\";a:1:{s:7:\"handles\";a:1:{s:21:\"Widget_Archive:footer\";a:1:{i:0;a:2:{i:0;s:13:\"Uptime_Plugin\";i:1;s:6:\"footer\";}}}}s:5:\"Links\";a:1:{s:7:\"handles\";a:3:{s:34:\"Widget_Abstract_Contents:contentEx\";a:1:{i:0;a:2:{i:0;s:12:\"Links_Plugin\";i:1;s:5:\"parse\";}}s:34:\"Widget_Abstract_Contents:excerptEx\";a:1:{i:0;a:2:{i:0;s:12:\"Links_Plugin\";i:1;s:5:\"parse\";}}s:34:\"Widget_Abstract_Comments:contentEx\";a:1:{i:0;a:2:{i:0;s:12:\"Links_Plugin\";i:1;s:5:\"parse\";}}}}s:12:\"Comment2Mail\";a:1:{s:7:\"handles\";a:3:{s:29:\"Widget_Feedback:finishComment\";a:1:{i:0;a:2:{i:0;s:19:\"Comment2Mail_Plugin\";i:1;s:13:\"finishComment\";}}s:34:\"Widget_Comments_Edit:finishComment\";a:1:{i:0;a:2:{i:0;s:19:\"Comment2Mail_Plugin\";i:1;s:13:\"finishComment\";}}s:25:\"Widget_Comments_Edit:mark\";a:1:{i:0;a:2:{i:0;s:19:\"Comment2Mail_Plugin\";i:1;s:4:\"mark\";}}}}s:7:\"Avatars\";a:1:{s:7:\"handles\";a:5:{s:34:\"Widget_Abstract_Contents:contentEx\";a:1:{i:0;a:2:{i:0;s:14:\"Avatars_Plugin\";i:1;s:5:\"walls\";}}s:33:\"Widget_Abstract_Comments:gravatar\";a:1:{i:0;a:2:{i:0;s:14:\"Avatars_Plugin\";i:1;s:7:\"avatars\";}}s:21:\"Widget_Archive:header\";a:1:{i:0;a:2:{i:0;s:14:\"Avatars_Plugin\";i:1;s:5:\"avcss\";}}s:21:\"Widget_Archive:footer\";a:1:{i:0;a:2:{i:0;s:14:\"Avatars_Plugin\";i:1;s:4:\"avjs\";}}s:26:\"Widget_Archive:callAvatars\";a:1:{i:0;a:2:{i:0;s:14:\"Avatars_Plugin\";i:1;s:6:\"output\";}}}}s:9:\"PageViews\";a:1:{s:7:\"handles\";a:1:{s:27:\"Widget_Archive:beforeRender\";a:1:{i:0;a:2:{i:0;s:16:\"PageViews_Plugin\";i:1;s:20:\"statisticalPageViews\";}}}}s:9:\"SlideShow\";a:1:{s:7:\"handles\";a:3:{s:21:\"Widget_Archive:footer\";a:1:{i:0;a:2:{i:0;s:16:\"SlideShow_Plugin\";i:1;s:6:\"footer\";}}s:27:\"Widget_Archive:beforeRender\";a:1:{i:0;a:2:{i:0;s:16:\"SlideShow_Plugin\";i:1;s:11:\"viewCounter\";}}s:21:\"Widget_Archive:select\";a:1:{i:0;a:2:{i:0;s:16:\"SlideShow_Plugin\";i:1;s:12:\"selectHandle\";}}}}s:10:\"AliceStyle\";a:1:{s:7:\"handles\";a:3:{s:21:\"Widget_Archive:header\";a:1:{i:0;a:2:{i:0;s:17:\"AliceStyle_Plugin\";i:1;s:6:\"header\";}}s:21:\"Widget_Archive:footer\";a:1:{i:0;a:2:{i:0;s:17:\"AliceStyle_Plugin\";i:1;s:6:\"footer\";}}s:21:\"admin/menu.php:navBar\";a:1:{i:0;a:2:{i:0;s:17:\"AliceStyle_Plugin\";i:1;s:6:\"render\";}}}}}s:7:\"handles\";a:16:{s:34:\"Widget_Abstract_Contents:contentEx\";a:3:{i:0;a:2:{i:0;s:13:\"Meting_Plugin\";i:1;s:13:\"playerReplace\";}s:5:\"0.001\";a:2:{i:0;s:12:\"Links_Plugin\";i:1;s:5:\"parse\";}s:5:\"0.002\";a:2:{i:0;s:14:\"Avatars_Plugin\";i:1;s:5:\"walls\";}}s:34:\"Widget_Abstract_Contents:excerptEx\";a:2:{i:0;a:2:{i:0;s:13:\"Meting_Plugin\";i:1;s:13:\"playerReplace\";}s:5:\"0.001\";a:2:{i:0;s:12:\"Links_Plugin\";i:1;s:5:\"parse\";}}s:21:\"Widget_Archive:header\";a:4:{i:0;a:2:{i:0;s:13:\"Meting_Plugin\";i:1;s:6:\"header\";}s:5:\"0.001\";a:2:{i:0;s:17:\"AliceStyle_Plugin\";i:1;s:6:\"header\";}s:5:\"0.002\";a:2:{i:0;s:14:\"YoduBGM_Plugin\";i:1;s:6:\"header\";}s:5:\"0.004\";a:2:{i:0;s:14:\"Avatars_Plugin\";i:1;s:5:\"avcss\";}}s:21:\"Widget_Archive:footer\";a:6:{i:0;a:2:{i:0;s:13:\"Meting_Plugin\";i:1;s:6:\"footer\";}s:5:\"0.001\";a:2:{i:0;s:17:\"AliceStyle_Plugin\";i:1;s:6:\"footer\";}s:5:\"0.002\";a:2:{i:0;s:14:\"YoduBGM_Plugin\";i:1;s:6:\"footer\";}s:5:\"0.004\";a:2:{i:0;s:13:\"Uptime_Plugin\";i:1;s:6:\"footer\";}s:5:\"0.005\";a:2:{i:0;s:14:\"Avatars_Plugin\";i:1;s:4:\"avjs\";}s:5:\"0.006\";a:2:{i:0;s:16:\"SlideShow_Plugin\";i:1;s:6:\"footer\";}}s:27:\"admin/write-post.php:bottom\";a:1:{i:0;a:2:{i:0;s:13:\"Meting_Plugin\";i:1;s:9:\"addButton\";}}s:27:\"admin/write-page.php:bottom\";a:1:{i:0;a:2:{i:0;s:13:\"Meting_Plugin\";i:1;s:9:\"addButton\";}}s:26:\"Widget_Archive:indexHandle\";a:1:{i:0;a:2:{i:0;s:13:\"Sticky_Plugin\";i:1;s:6:\"sticky\";}}s:27:\"Widget_Archive:beforeRender\";a:3:{i:0;a:2:{i:0;s:16:\"PageViews_Plugin\";i:1;s:20:\"statisticalPageViews\";}s:5:\"0.001\";a:2:{i:0;s:12:\"Views_Plugin\";i:1;s:12:\"viewsCounter\";}s:5:\"0.002\";a:2:{i:0;s:16:\"SlideShow_Plugin\";i:1;s:11:\"viewCounter\";}}s:34:\"Widget_Abstract_Comments:contentEx\";a:1:{i:0;a:2:{i:0;s:12:\"Links_Plugin\";i:1;s:5:\"parse\";}}s:29:\"Widget_Feedback:finishComment\";a:1:{i:0;a:2:{i:0;s:19:\"Comment2Mail_Plugin\";i:1;s:13:\"finishComment\";}}s:34:\"Widget_Comments_Edit:finishComment\";a:1:{i:0;a:2:{i:0;s:19:\"Comment2Mail_Plugin\";i:1;s:13:\"finishComment\";}}s:25:\"Widget_Comments_Edit:mark\";a:1:{i:0;a:2:{i:0;s:19:\"Comment2Mail_Plugin\";i:1;s:4:\"mark\";}}s:33:\"Widget_Abstract_Comments:gravatar\";a:1:{i:0;a:2:{i:0;s:14:\"Avatars_Plugin\";i:1;s:7:\"avatars\";}}s:26:\"Widget_Archive:callAvatars\";a:1:{i:0;a:2:{i:0;s:14:\"Avatars_Plugin\";i:1;s:6:\"output\";}}s:21:\"Widget_Archive:select\";a:1:{i:0;a:2:{i:0;s:16:\"SlideShow_Plugin\";i:1;s:12:\"selectHandle\";}}s:21:\"admin/menu.php:navBar\";a:1:{i:0;a:2:{i:0;s:17:\"AliceStyle_Plugin\";i:1;s:6:\"render\";}}}}');
INSERT INTO `typecho_options` VALUES ('commentDateFormat', '0', 'F jS, Y \\a\\t h:i a');
INSERT INTO `typecho_options` VALUES ('siteUrl', '0', 'http://www.zhangxuhui.com');
INSERT INTO `typecho_options` VALUES ('defaultCategory', '0', '1');
INSERT INTO `typecho_options` VALUES ('allowRegister', '0', '0');
INSERT INTO `typecho_options` VALUES ('defaultAllowComment', '0', '1');
INSERT INTO `typecho_options` VALUES ('defaultAllowPing', '0', '1');
INSERT INTO `typecho_options` VALUES ('defaultAllowFeed', '0', '1');
INSERT INTO `typecho_options` VALUES ('pageSize', '0', '20');
INSERT INTO `typecho_options` VALUES ('postsListSize', '0', '10');
INSERT INTO `typecho_options` VALUES ('commentsListSize', '0', '10');
INSERT INTO `typecho_options` VALUES ('commentsHTMLTagAllowed', '0', null);
INSERT INTO `typecho_options` VALUES ('postDateFormat', '0', 'Y-m-d H:i:s');
INSERT INTO `typecho_options` VALUES ('feedFullText', '0', '0');
INSERT INTO `typecho_options` VALUES ('editorSize', '0', '350');
INSERT INTO `typecho_options` VALUES ('autoSave', '0', '0');
INSERT INTO `typecho_options` VALUES ('markdown', '0', '1');
INSERT INTO `typecho_options` VALUES ('xmlrpcMarkdown', '0', '0');
INSERT INTO `typecho_options` VALUES ('commentsMaxNestingLevels', '0', '5');
INSERT INTO `typecho_options` VALUES ('commentsPostTimeout', '0', '2592000');
INSERT INTO `typecho_options` VALUES ('commentsUrlNofollow', '0', '1');
INSERT INTO `typecho_options` VALUES ('commentsShowUrl', '0', '1');
INSERT INTO `typecho_options` VALUES ('commentsMarkdown', '0', '0');
INSERT INTO `typecho_options` VALUES ('commentsPageBreak', '0', '0');
INSERT INTO `typecho_options` VALUES ('commentsThreaded', '0', '1');
INSERT INTO `typecho_options` VALUES ('commentsPageSize', '0', '20');
INSERT INTO `typecho_options` VALUES ('commentsPageDisplay', '0', 'last');
INSERT INTO `typecho_options` VALUES ('commentsOrder', '0', 'ASC');
INSERT INTO `typecho_options` VALUES ('commentsCheckReferer', '0', '1');
INSERT INTO `typecho_options` VALUES ('commentsAutoClose', '0', '0');
INSERT INTO `typecho_options` VALUES ('commentsPostIntervalEnable', '0', '1');
INSERT INTO `typecho_options` VALUES ('commentsPostInterval', '0', '60');
INSERT INTO `typecho_options` VALUES ('commentsShowCommentOnly', '0', '0');
INSERT INTO `typecho_options` VALUES ('commentsAvatar', '0', '1');
INSERT INTO `typecho_options` VALUES ('commentsAvatarRating', '0', 'G');
INSERT INTO `typecho_options` VALUES ('commentsAntiSpam', '0', '1');
INSERT INTO `typecho_options` VALUES ('routingTable', '0', 'a:28:{i:0;a:27:{s:5:\"index\";a:6:{s:3:\"url\";s:1:\"/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:8:\"|^[/]?$|\";s:6:\"format\";s:1:\"/\";s:6:\"params\";a:0:{}}s:7:\"archive\";a:6:{s:3:\"url\";s:6:\"/blog/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:13:\"|^/blog[/]?$|\";s:6:\"format\";s:6:\"/blog/\";s:6:\"params\";a:0:{}}s:2:\"do\";a:6:{s:3:\"url\";s:22:\"/action/[action:alpha]\";s:6:\"widget\";s:9:\"Widget_Do\";s:6:\"action\";s:6:\"action\";s:4:\"regx\";s:32:\"|^/action/([_0-9a-zA-Z-]+)[/]?$|\";s:6:\"format\";s:10:\"/action/%s\";s:6:\"params\";a:1:{i:0;s:6:\"action\";}}s:4:\"post\";a:6:{s:3:\"url\";s:24:\"/archives/[cid:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:26:\"|^/archives/([0-9]+)[/]?$|\";s:6:\"format\";s:13:\"/archives/%s/\";s:6:\"params\";a:1:{i:0;s:3:\"cid\";}}s:10:\"attachment\";a:6:{s:3:\"url\";s:26:\"/attachment/[cid:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:28:\"|^/attachment/([0-9]+)[/]?$|\";s:6:\"format\";s:15:\"/attachment/%s/\";s:6:\"params\";a:1:{i:0;s:3:\"cid\";}}s:8:\"category\";a:6:{s:3:\"url\";s:17:\"/category/[slug]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:25:\"|^/category/([^/]+)[/]?$|\";s:6:\"format\";s:13:\"/category/%s/\";s:6:\"params\";a:1:{i:0;s:4:\"slug\";}}s:3:\"tag\";a:6:{s:3:\"url\";s:12:\"/tag/[slug]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:20:\"|^/tag/([^/]+)[/]?$|\";s:6:\"format\";s:8:\"/tag/%s/\";s:6:\"params\";a:1:{i:0;s:4:\"slug\";}}s:6:\"author\";a:6:{s:3:\"url\";s:22:\"/author/[uid:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:24:\"|^/author/([0-9]+)[/]?$|\";s:6:\"format\";s:11:\"/author/%s/\";s:6:\"params\";a:1:{i:0;s:3:\"uid\";}}s:6:\"search\";a:6:{s:3:\"url\";s:19:\"/search/[keywords]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:23:\"|^/search/([^/]+)[/]?$|\";s:6:\"format\";s:11:\"/search/%s/\";s:6:\"params\";a:1:{i:0;s:8:\"keywords\";}}s:10:\"index_page\";a:6:{s:3:\"url\";s:21:\"/page/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:22:\"|^/page/([0-9]+)[/]?$|\";s:6:\"format\";s:9:\"/page/%s/\";s:6:\"params\";a:1:{i:0;s:4:\"page\";}}s:12:\"archive_page\";a:6:{s:3:\"url\";s:26:\"/blog/page/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:27:\"|^/blog/page/([0-9]+)[/]?$|\";s:6:\"format\";s:14:\"/blog/page/%s/\";s:6:\"params\";a:1:{i:0;s:4:\"page\";}}s:13:\"category_page\";a:6:{s:3:\"url\";s:32:\"/category/[slug]/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:34:\"|^/category/([^/]+)/([0-9]+)[/]?$|\";s:6:\"format\";s:16:\"/category/%s/%s/\";s:6:\"params\";a:2:{i:0;s:4:\"slug\";i:1;s:4:\"page\";}}s:8:\"tag_page\";a:6:{s:3:\"url\";s:27:\"/tag/[slug]/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:29:\"|^/tag/([^/]+)/([0-9]+)[/]?$|\";s:6:\"format\";s:11:\"/tag/%s/%s/\";s:6:\"params\";a:2:{i:0;s:4:\"slug\";i:1;s:4:\"page\";}}s:11:\"author_page\";a:6:{s:3:\"url\";s:37:\"/author/[uid:digital]/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:33:\"|^/author/([0-9]+)/([0-9]+)[/]?$|\";s:6:\"format\";s:14:\"/author/%s/%s/\";s:6:\"params\";a:2:{i:0;s:3:\"uid\";i:1;s:4:\"page\";}}s:11:\"search_page\";a:6:{s:3:\"url\";s:34:\"/search/[keywords]/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:32:\"|^/search/([^/]+)/([0-9]+)[/]?$|\";s:6:\"format\";s:14:\"/search/%s/%s/\";s:6:\"params\";a:2:{i:0;s:8:\"keywords\";i:1;s:4:\"page\";}}s:12:\"archive_year\";a:6:{s:3:\"url\";s:18:\"/[year:digital:4]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:19:\"|^/([0-9]{4})[/]?$|\";s:6:\"format\";s:4:\"/%s/\";s:6:\"params\";a:1:{i:0;s:4:\"year\";}}s:13:\"archive_month\";a:6:{s:3:\"url\";s:36:\"/[year:digital:4]/[month:digital:2]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:30:\"|^/([0-9]{4})/([0-9]{2})[/]?$|\";s:6:\"format\";s:7:\"/%s/%s/\";s:6:\"params\";a:2:{i:0;s:4:\"year\";i:1;s:5:\"month\";}}s:11:\"archive_day\";a:6:{s:3:\"url\";s:52:\"/[year:digital:4]/[month:digital:2]/[day:digital:2]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:41:\"|^/([0-9]{4})/([0-9]{2})/([0-9]{2})[/]?$|\";s:6:\"format\";s:10:\"/%s/%s/%s/\";s:6:\"params\";a:3:{i:0;s:4:\"year\";i:1;s:5:\"month\";i:2;s:3:\"day\";}}s:17:\"archive_year_page\";a:6:{s:3:\"url\";s:38:\"/[year:digital:4]/page/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:33:\"|^/([0-9]{4})/page/([0-9]+)[/]?$|\";s:6:\"format\";s:12:\"/%s/page/%s/\";s:6:\"params\";a:2:{i:0;s:4:\"year\";i:1;s:4:\"page\";}}s:18:\"archive_month_page\";a:6:{s:3:\"url\";s:56:\"/[year:digital:4]/[month:digital:2]/page/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:44:\"|^/([0-9]{4})/([0-9]{2})/page/([0-9]+)[/]?$|\";s:6:\"format\";s:15:\"/%s/%s/page/%s/\";s:6:\"params\";a:3:{i:0;s:4:\"year\";i:1;s:5:\"month\";i:2;s:4:\"page\";}}s:16:\"archive_day_page\";a:6:{s:3:\"url\";s:72:\"/[year:digital:4]/[month:digital:2]/[day:digital:2]/page/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:55:\"|^/([0-9]{4})/([0-9]{2})/([0-9]{2})/page/([0-9]+)[/]?$|\";s:6:\"format\";s:18:\"/%s/%s/%s/page/%s/\";s:6:\"params\";a:4:{i:0;s:4:\"year\";i:1;s:5:\"month\";i:2;s:3:\"day\";i:3;s:4:\"page\";}}s:12:\"comment_page\";a:6:{s:3:\"url\";s:53:\"[permalink:string]/comment-page-[commentPage:digital]\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:36:\"|^(.+)/comment\\-page\\-([0-9]+)[/]?$|\";s:6:\"format\";s:18:\"%s/comment-page-%s\";s:6:\"params\";a:2:{i:0;s:9:\"permalink\";i:1;s:11:\"commentPage\";}}s:4:\"feed\";a:6:{s:3:\"url\";s:20:\"/feed[feed:string:0]\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:4:\"feed\";s:4:\"regx\";s:17:\"|^/feed(.*)[/]?$|\";s:6:\"format\";s:7:\"/feed%s\";s:6:\"params\";a:1:{i:0;s:4:\"feed\";}}s:8:\"feedback\";a:6:{s:3:\"url\";s:31:\"[permalink:string]/[type:alpha]\";s:6:\"widget\";s:15:\"Widget_Feedback\";s:6:\"action\";s:6:\"action\";s:4:\"regx\";s:29:\"|^(.+)/([_0-9a-zA-Z-]+)[/]?$|\";s:6:\"format\";s:5:\"%s/%s\";s:6:\"params\";a:2:{i:0;s:9:\"permalink\";i:1;s:4:\"type\";}}s:4:\"page\";a:6:{s:3:\"url\";s:12:\"/[slug].html\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";s:4:\"regx\";s:22:\"|^/([^/]+)\\.html[/]?$|\";s:6:\"format\";s:8:\"/%s.html\";s:6:\"params\";a:1:{i:0;s:4:\"slug\";}}s:17:\"comment2mail_test\";a:6:{s:3:\"url\";s:18:\"/comment2mail/test\";s:6:\"widget\";s:19:\"Comment2Mail_Action\";s:6:\"action\";s:6:\"action\";s:4:\"regx\";s:26:\"|^/comment2mail/test[/]?$|\";s:6:\"format\";s:18:\"/comment2mail/test\";s:6:\"params\";a:0:{}}s:16:\"route_AliceStyle\";a:6:{s:3:\"url\";s:11:\"/AliceStyle\";s:6:\"widget\";s:17:\"AliceStyle_Action\";s:6:\"action\";s:6:\"action\";s:4:\"regx\";s:19:\"|^/AliceStyle[/]?$|\";s:6:\"format\";s:11:\"/AliceStyle\";s:6:\"params\";a:0:{}}}s:5:\"index\";a:3:{s:3:\"url\";s:1:\"/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:7:\"archive\";a:3:{s:3:\"url\";s:6:\"/blog/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:2:\"do\";a:3:{s:3:\"url\";s:22:\"/action/[action:alpha]\";s:6:\"widget\";s:9:\"Widget_Do\";s:6:\"action\";s:6:\"action\";}s:4:\"post\";a:3:{s:3:\"url\";s:24:\"/archives/[cid:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:10:\"attachment\";a:3:{s:3:\"url\";s:26:\"/attachment/[cid:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:8:\"category\";a:3:{s:3:\"url\";s:17:\"/category/[slug]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:3:\"tag\";a:3:{s:3:\"url\";s:12:\"/tag/[slug]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:6:\"author\";a:3:{s:3:\"url\";s:22:\"/author/[uid:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:6:\"search\";a:3:{s:3:\"url\";s:19:\"/search/[keywords]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:10:\"index_page\";a:3:{s:3:\"url\";s:21:\"/page/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:12:\"archive_page\";a:3:{s:3:\"url\";s:26:\"/blog/page/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:13:\"category_page\";a:3:{s:3:\"url\";s:32:\"/category/[slug]/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:8:\"tag_page\";a:3:{s:3:\"url\";s:27:\"/tag/[slug]/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:11:\"author_page\";a:3:{s:3:\"url\";s:37:\"/author/[uid:digital]/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:11:\"search_page\";a:3:{s:3:\"url\";s:34:\"/search/[keywords]/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:12:\"archive_year\";a:3:{s:3:\"url\";s:18:\"/[year:digital:4]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:13:\"archive_month\";a:3:{s:3:\"url\";s:36:\"/[year:digital:4]/[month:digital:2]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:11:\"archive_day\";a:3:{s:3:\"url\";s:52:\"/[year:digital:4]/[month:digital:2]/[day:digital:2]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:17:\"archive_year_page\";a:3:{s:3:\"url\";s:38:\"/[year:digital:4]/page/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:18:\"archive_month_page\";a:3:{s:3:\"url\";s:56:\"/[year:digital:4]/[month:digital:2]/page/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:16:\"archive_day_page\";a:3:{s:3:\"url\";s:72:\"/[year:digital:4]/[month:digital:2]/[day:digital:2]/page/[page:digital]/\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:12:\"comment_page\";a:3:{s:3:\"url\";s:53:\"[permalink:string]/comment-page-[commentPage:digital]\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:4:\"feed\";a:3:{s:3:\"url\";s:20:\"/feed[feed:string:0]\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:4:\"feed\";}s:8:\"feedback\";a:3:{s:3:\"url\";s:31:\"[permalink:string]/[type:alpha]\";s:6:\"widget\";s:15:\"Widget_Feedback\";s:6:\"action\";s:6:\"action\";}s:4:\"page\";a:3:{s:3:\"url\";s:12:\"/[slug].html\";s:6:\"widget\";s:14:\"Widget_Archive\";s:6:\"action\";s:6:\"render\";}s:17:\"comment2mail_test\";a:3:{s:3:\"url\";s:18:\"/comment2mail/test\";s:6:\"widget\";s:19:\"Comment2Mail_Action\";s:6:\"action\";s:6:\"action\";}s:16:\"route_AliceStyle\";a:3:{s:3:\"url\";s:11:\"/AliceStyle\";s:6:\"widget\";s:17:\"AliceStyle_Action\";s:6:\"action\";s:6:\"action\";}}');
INSERT INTO `typecho_options` VALUES ('plugin:AliceStyle', '0', 'a:8:{s:7:\"code_js\";s:13:\"9-RiseLove.js\";s:4:\"type\";s:1:\"2\";s:9:\"ReturnTop\";s:1:\"2\";s:8:\"headtips\";s:1:\"1\";s:11:\"live2d_type\";s:1:\"1\";s:6:\"appkey\";s:0:\"\";s:9:\"mouseType\";s:9:\"fireworks\";s:4:\"snow\";s:1:\"1\";}');
INSERT INTO `typecho_options` VALUES ('actionTable', '0', 'a:3:{s:15:\"comment-to-mail\";s:20:\"CommentToMail_Action\";s:9:\"metingapi\";s:13:\"Meting_Action\";s:10:\"links-edit\";s:12:\"Links_Action\";}');
INSERT INTO `typecho_options` VALUES ('panelTable', '0', 'a:2:{s:5:\"child\";a:2:{i:1;a:1:{i:0;a:6:{i:0;s:18:\"评论邮件提醒\";i:1;s:27:\"评论邮件提醒控制台\";i:2;s:54:\"extending.php?panel=CommentToMail%2Fpage%2Fconsole.php\";i:3;s:13:\"administrator\";i:4;b:0;i:5;s:0:\"\";}}i:3;a:1:{i:0;a:6:{i:0;s:12:\"友情链接\";i:1;s:18:\"管理友情链接\";i:2;s:44:\"extending.php?panel=Links%2Fmanage-links.php\";i:3;s:13:\"administrator\";i:4;b:0;i:5;s:0:\"\";}}}s:4:\"file\";a:2:{i:0;s:34:\"CommentToMail%2Fpage%2Fconsole.php\";i:1;s:24:\"Links%2Fmanage-links.php\";}}');
INSERT INTO `typecho_options` VALUES ('attachmentTypes', '0', '@image@');
INSERT INTO `typecho_options` VALUES ('secret', '0', '#R3s(CYU!R&$ar#P3KrCbaycj8VCcq9%');
INSERT INTO `typecho_options` VALUES ('installed', '0', '1');
INSERT INTO `typecho_options` VALUES ('allowXmlRpc', '0', '2');
INSERT INTO `typecho_options` VALUES ('plugin:Comment2Mail', '0', 'a:8:{s:3:\"log\";N;s:8:\"STMPHost\";s:11:\"smtp.qq.com\";s:12:\"SMTPUserName\";s:16:\"137647337@qq.com\";s:12:\"SMTPPassword\";s:16:\"zwichndgtoqtcabh\";s:10:\"SMTPSecure\";s:0:\"\";s:8:\"SMTPPort\";s:2:\"25\";s:4:\"from\";s:16:\"137647337@qq.com\";s:8:\"fromName\";s:12:\"旭辉博客\";}');
INSERT INTO `typecho_options` VALUES ('theme:Typecho-Theme-Littlehands-', '0', 'a:14:{s:7:\"logoUrl\";s:68:\"https://cdn.v2ex.com/gravatar/8609e36d7684afdb105e3c724c10c7d0?s=100\";s:13:\"backgroundUrl\";s:82:\"http://www.phpmaster.cn/usr/themes/Typecho-Theme-Littlehands-master/background.jpg\";s:10:\"faviconUrl\";s:12:\"/favicon.ico\";s:8:\"gravatar\";s:24:\"//cdn.v2ex.com/gravatar/\";s:12:\"lazyload_img\";s:22:\"{themeUrl}/loading.gif\";s:5:\"baidu\";N;s:6:\"sticky\";N;s:6:\"reward\";N;s:7:\"QPlayer\";s:33:\"{\"isRandom\":true,\"isRotate\":true}\";s:8:\"advanced\";a:7:{i:0;s:4:\"pjax\";i:1;s:8:\"hitokoto\";i:2;s:8:\"lazyload\";i:3;s:5:\"emoji\";i:4;s:9:\"ShortCode\";i:5;s:9:\"Parsedown\";i:6;s:5:\"prism\";}s:5:\"emoji\";s:4283:\"<div class=\"emoji-package\"><div class=\"emoji-btn\"><span data-url=\"{themeUrl}/emoji/paopao/\">泡泡表情</span></div><div class=\"emoji-body\"><ul><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/1.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/2.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/3.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/4.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/5.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/6.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/7.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/8.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/9.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/10.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/11.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/12.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/13.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/14.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/15.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/16.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/17.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/18.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/19.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/20.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/21.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/22.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/23.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/24.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/25.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/26.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/27.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/28.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/29.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/30.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/31.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/32.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/33.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/34.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/35.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/36.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/37.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/38.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/39.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/40.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/41.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/42.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/43.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/44.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/45.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/46.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/47.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/48.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/49.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/50.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/51.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/52.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/53.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/54.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/55.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/56.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/57.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/58.png\"></li><li><img class=\"emoji\" data-src=\"{themeUrl}/emoji/paopao/59.png\"></li></ul></div></div>\";s:6:\"header\";N;s:6:\"footer\";s:306:\"Powered by <a href=\"http://www.typecho.org/\" target=\"_blank\">Typecho</a>, Theme by <a href=\"https://www.littlehands.site/\" target=\"_blank\">Littlehands</a>, {inf}<br/>\n© {Y} <a href=\"{siteUrl}\">{title}</a>. All right reserved. <a href=\"http://www.miitbeian.gov.cn/\" target=\"_blank\">京ICP备00000000号</a>\";s:12:\"sidebarBlock\";a:6:{i:0;s:15:\"ShowRecentPosts\";i:1;s:18:\"ShowRecentComments\";i:2;s:12:\"ShowCategory\";i:3;s:11:\"ShowArchive\";i:4;s:9:\"ShowOther\";i:5;s:7:\"Console\";}}');
INSERT INTO `typecho_options` VALUES ('theme:castle-Typecho-Theme-maste', '0', 'a:27:{s:4:\"lang\";s:82:\"/www/users/HA72442/WEB/usr/themes/castle-Typecho-Theme-master/languages/zh-CN.json\";s:10:\"themecolor\";s:4:\"pink\";s:11:\"accentcolor\";s:4:\"pink\";s:3:\"tcs\";s:1:\"0\";s:3:\"hls\";s:15:\"default.min.css\";s:5:\"cardt\";s:1:\"0\";s:3:\"ftt\";s:1:\"0\";s:13:\"themeResource\";s:5:\"local\";s:7:\"randimg\";s:13:\"api.ohmyga.cn\";s:12:\"gravatar_url\";s:21:\"cdn.v2ex.com/gravatar\";s:9:\"qqheadimg\";s:1:\"0\";s:4:\"post\";a:3:{i:0;s:5:\"share\";i:1;s:4:\"cate\";i:2;s:4:\"copy\";}s:4:\"page\";a:2:{i:0;s:5:\"share\";i:1;s:4:\"copy\";}s:7:\"comment\";a:3:{i:0;s:6:\"userUA\";i:1;s:4:\"ajax\";i:2;s:4:\"link\";}s:5:\"other\";a:4:{i:0;s:4:\"copy\";i:1;s:4:\"pjax\";i:2;s:5:\"gotop\";i:3;s:5:\"night\";}s:6:\"sticky\";N;s:7:\"headimg\";N;s:3:\"bgp\";s:0:\"\";s:3:\"bgs\";s:0:\"\";s:9:\"sidebarBG\";s:0:\"\";s:8:\"miibeian\";s:0:\"\";s:7:\"apipass\";s:32:\"aO60lPQ4VxTWfcRzl8BFZfPSnSHT3w73\";s:7:\"sidebar\";s:426:\"[{\n   \"name\":\"归档\",\n   \"link\":\"#\",\n   \"icon\":\"access_time\",\n   \"type\":\"1\"\n },{\n   \"name\":\"分类\",\n   \"link\":\"#\",\n   \"icon\":\"view_list\",\n   \"type\":\"2\"\n },{\n   \"name\":\"页面\",\n   \"link\":\"#\",\n   \"icon\":\"view_carousel\",\n   \"type\":\"3\"\n },{\n   \"type\":\"5\"\n },{\n   \"type\":\"6\"\n },{\n   \"type\":\"5\"\n },{\n   \"type\":\"7\",\n   \"tes\":\"1\"\n },{\n   \"type\":\"7\",\n   \"tes\":\"2\"\n },{\n   \"type\":\"7\",\n   \"tes\":\"3\"\n },{\n   \"type\":\"7\",\n   \"tes\":\"4\"\n}]\";s:6:\"social\";s:96:\"[{\n \"name\": \"email\",\n \"icon\": \"<i class=\\\"mdui-icon material-icons\\\">email</i>\",\n \"link\": \"#\"\n}]\";s:10:\"statistics\";s:0:\"\";s:10:\"pjaxRelaod\";s:0:\"\";s:15:\"advancedSetting\";s:353:\"{\n \"setting\": {\n  \"leave\": {\n   \"title\": \"悄悄藏好 (/ω＼) \",\n   \"icon\": \"\"\n  },\n  \"return\": {\n   \"title\": \"被找到惹 Σ(っ °Д °;)っ \",\n   \"icon\": \"\"\n  },\n  \"copyPrompt\": {\n   \"text\": \"转载请保留相应版权！\",\n   \"title\": \"警告\"\n  },\n  \"toTopText\": \"回到顶部~\"\n },\n \"resource\": {\n  \"url\": \"\"\n },\n \"randPic\": {\n  \"url\": \"\"\n }\n}\";}');
INSERT INTO `typecho_options` VALUES ('theme:molerose-typecho-themes-de', '0', 'a:20:{s:7:\"logoUrl\";N;s:9:\"IndexName\";N;s:8:\"BlogName\";N;s:7:\"BlogPic\";N;s:7:\"BlogAdd\";N;s:7:\"BlogJob\";N;s:10:\"Indexwords\";N;s:13:\"socialtwitter\";N;s:14:\"socialfacebook\";N;s:17:\"socialgooglepluse\";N;s:12:\"socialgithub\";N;s:11:\"socialemail\";N;s:8:\"socialqq\";N;s:11:\"socialweibo\";N;s:14:\"socialweiboUrl\";N;s:11:\"socialmusic\";N;s:5:\"about\";N;s:8:\"analysis\";N;s:7:\"favicon\";N;s:10:\"titleintro\";N;}');
INSERT INTO `typecho_options` VALUES ('theme:typecho-theme-NexTPisces-m', '0', 'a:7:{s:9:\"next_name\";s:0:\"\";s:13:\"next_gravatar\";s:0:\"\";s:9:\"next_tips\";s:36:\"一个高端大气上档次的网站\";s:8:\"next_cdn\";s:24:\"http://www.phpmaster.cn/\";s:13:\"sidebarFlinks\";s:4:\"hide\";s:10:\"sidebarNav\";a:4:{i:0;s:4:\"main\";i:1;s:7:\"archive\";i:2;s:4:\"tags\";i:3;s:6:\"search\";}s:13:\"sidebarOthers\";a:1:{i:0;s:8:\"ShowFeed\";}}');
INSERT INTO `typecho_options` VALUES ('plugin:Sticky', '0', 'a:2:{s:11:\"sticky_cids\";s:13:\"46 59  61 37 \";s:11:\"sticky_html\";s:63:\"<span style=\'color:red;font-weight:bold\'>[博主推荐] </span>\";}');
INSERT INTO `typecho_options` VALUES ('plugin:Meting', '0', 'a:12:{s:5:\"theme\";s:7:\"#ad7a86\";s:6:\"height\";s:5:\"340px\";s:8:\"autoplay\";s:5:\"false\";s:5:\"order\";s:4:\"list\";s:7:\"preload\";s:4:\"auto\";s:9:\"cachetype\";s:4:\"none\";s:9:\"cachehost\";s:9:\"127.0.0.1\";s:9:\"cacheport\";s:4:\"6379\";s:7:\"bitrate\";s:3:\"192\";s:3:\"api\";s:99:\"http://www.phpmaster.cn/index.php/action/metingapi?server=:server&type=:type&id=:id&auth=:auth&r=:r\";s:4:\"salt\";s:32:\"ZjXP57Q3pxqMVq5ccIwvu9BAwUEtIxtx\";s:6:\"cookie\";s:0:\"\";}');
INSERT INTO `typecho_options` VALUES ('plugin:YoduBGM', '0', 'a:3:{s:3:\"bof\";s:1:\"1\";s:3:\"sxj\";s:1:\"1\";s:9:\"musicList\";s:11:\"482301912\r\n\";}');
INSERT INTO `typecho_options` VALUES ('plugin:Uptime', '0', 'a:1:{s:10:\"start_time\";s:19:\"2019/10/05 00:00:00\";}');
INSERT INTO `typecho_options` VALUES ('plugin:PageViews', '0', '144974');
INSERT INTO `typecho_options` VALUES ('plugin:Avatars', '0', 'a:10:{s:5:\"wsize\";s:2:\"32\";s:8:\"wdefault\";s:0:\"\";s:10:\"listnumber\";s:2:\"10\";s:5:\"since\";s:2:\"30\";s:7:\"altword\";s:9:\"条评论\";s:7:\"tooltip\";N;s:8:\"tipstyle\";s:333:\"#tooltip {position:absolute;z-index:20;max-width:200px;background-color:#101010;text-align:left;padding:0 8px;border-radius:6px;-moz-border-radius:6px;-webkit-border-radius:6px;}\n#tooltip p {color:#FFF;font:12px \"Microsoft YaHei\",Tahoma,Arial,Sans-Serif;}\n#tooltip p em {display:block;margin-top:1px;color:#70B9ED;font-style:normal;}\";s:5:\"proxy\";s:0:\"\";s:5:\"cache\";N;s:7:\"comment\";N;}');
INSERT INTO `typecho_options` VALUES ('plugin:SlideShow', '0', 'a:5:{s:9:\"delFields\";s:1:\"0\";s:7:\"hotNums\";s:1:\"8\";s:6:\"sortBy\";s:1:\"0\";s:8:\"minViews\";s:1:\"0\";s:2:\"jq\";s:4:\"true\";}');
INSERT INTO `typecho_options` VALUES ('theme:echo-master', '0', 'a:1:{s:7:\"logoUrl\";N;}');
INSERT INTO `typecho_options` VALUES ('theme:package', '0', 'a:105:{s:4:\"JCDN\";s:2:\"on\";s:6:\"JCache\";s:3:\"off\";s:7:\"JDefend\";s:3:\"off\";s:8:\"JPrevent\";s:2:\"on\";s:12:\"JHeaderStyle\";s:7:\"default\";s:10:\"JNavMaxNum\";s:1:\"6\";s:12:\"JHorseStatus\";s:2:\"on\";s:13:\"JCensusStatus\";s:2:\"on\";s:15:\"JBarragerStatus\";s:2:\"on\";s:11:\"JSignStatus\";s:2:\"on\";s:15:\"JProgressStatus\";s:2:\"on\";s:11:\"JPageStatus\";s:4:\"ajax\";s:18:\"JContextMenuStatus\";s:3:\"off\";s:14:\"JDocumentTitle\";s:0:\"\";s:14:\"JCursorEffects\";s:14:\"cursor4.min.js\";s:7:\"JPlayer\";s:9:\"163291171\";s:11:\"JCursorType\";s:3:\"off\";s:14:\"JConsoleStatus\";s:3:\"off\";s:10:\"JCustomCSS\";s:0:\"\";s:13:\"JCustomScript\";s:0:\"\";s:14:\"JCustomHeadEnd\";s:0:\"\";s:16:\"JCustomBodyStart\";s:0:\"\";s:14:\"JCustomBodyEnd\";s:0:\"\";s:7:\"JLive2D\";s:82:\"https://cdn.jsdelivr.net/npm/live2d-widget-model-miku@1.0.5/assets/miku.model.json\";s:12:\"JPageLoading\";s:3:\"off\";s:14:\"JBackTopStatus\";s:2:\"on\";s:17:\"JGlobalThemeColor\";s:0:\"\";s:18:\"JGlobalThemeStatus\";s:2:\"on\";s:10:\"JCountTime\";s:2:\"on\";s:8:\"JBanQuan\";s:0:\"\";s:13:\"JBanQuanLinks\";s:0:\"\";s:10:\"JGravatars\";s:29:\"gravatar.helingqi.com/wavatar\";s:17:\"JHoverMusicStatus\";s:2:\"on\";s:11:\"JFishStatus\";s:6:\"bottom\";s:9:\"JLazyLoad\";s:0:\"\";s:4:\"Jmos\";s:0:\"\";s:8:\"JFavicon\";s:0:\"\";s:5:\"JLogo\";s:0:\"\";s:17:\"JDocumentCanvasBG\";s:3:\"off\";s:13:\"JDocumentPCBG\";s:0:\"\";s:14:\"JDocumentWAPBG\";s:0:\"\";s:15:\"JBaiDuPushToken\";s:0:\"\";s:12:\"JBreadStatus\";s:3:\"off\";s:19:\"JPostCountingStatus\";s:3:\"off\";s:10:\"JCodeColor\";s:3:\"off\";s:10:\"JTagStatus\";s:3:\"off\";s:14:\"JBanQuanStatus\";s:3:\"off\";s:14:\"JRelatedStatus\";s:3:\"off\";s:16:\"JDirectoryStatus\";s:3:\"off\";s:7:\"JAdmire\";s:0:\"\";s:11:\"JQQSharePic\";s:0:\"\";s:17:\"JIndexAsideStatus\";s:2:\"on\";s:16:\"JPostAsideStatus\";s:2:\"on\";s:11:\"JADContent1\";s:0:\"\";s:11:\"JADContent2\";s:0:\"\";s:12:\"JAsideCustom\";s:0:\"\";s:13:\"JAsideVisitor\";s:0:\"\";s:10:\"JWetherKey\";s:0:\"\";s:11:\"JWetherType\";s:5:\"white\";s:12:\"J3DTagStatus\";s:3:\"off\";s:17:\"JAsideReplyStatus\";s:2:\"on\";s:15:\"JAsideHotNumber\";s:2:\"10\";s:13:\"JAuthorStatus\";s:1:\"3\";s:13:\"JAuthorAvatar\";s:0:\"\";s:11:\"JAuthorLink\";s:0:\"\";s:6:\"JMotto\";s:0:\"\";s:9:\"JMottoAPI\";s:0:\"\";s:16:\"JCountDownStatus\";s:2:\"on\";s:8:\"JRanking\";s:36:\"网易24H新闻点击榜$t_en_dianji\";s:15:\"JCardBackground\";s:0:\"\";s:7:\"JClassA\";s:0:\"\";s:7:\"JClassB\";s:0:\"\";s:7:\"JClassC\";s:0:\"\";s:7:\"JClassD\";s:0:\"\";s:10:\"JMainColor\";s:0:\"\";s:13:\"JRoutineColor\";s:0:\"\";s:11:\"JMinorColor\";s:0:\"\";s:10:\"JSeatColor\";s:0:\"\";s:13:\"JSuccessColor\";s:0:\"\";s:13:\"JWarningColor\";s:0:\"\";s:12:\"JDangerColor\";s:0:\"\";s:10:\"JInfoColor\";s:0:\"\";s:9:\"JRadiusPC\";s:3:\"0px\";s:10:\"JRadiusWap\";s:3:\"0px\";s:11:\"JTextShadow\";s:0:\"\";s:10:\"JBoxShadow\";s:0:\"\";s:12:\"JPCAnimation\";s:3:\"off\";s:13:\"JWapAnimation\";s:3:\"off\";s:12:\"JSummaryMeta\";a:5:{i:0;s:6:\"author\";i:1;s:4:\"cate\";i:2;s:4:\"time\";i:3;s:4:\"read\";i:4;s:7:\"comment\";}s:12:\"JIndexSticky\";s:0:\"\";s:12:\"JIndexNotice\";s:0:\"\";s:8:\"JIndexAD\";s:0:\"\";s:15:\"JIndexHotStatus\";s:3:\"off\";s:14:\"JIndexCarousel\";s:175:\"https://puui.qpic.cn/media_img/lena/PICykqaoi_580_1680/0 || http://baidu.com || 百度一下\r\nhttps://puui.qpic.cn/tv/0/1223447268_1680580/0 || http://v.qq.com || 腾讯视频\";s:15:\"JIndexRecommend\";s:0:\"\";s:8:\"JFriends\";s:0:\"\";s:13:\"JVideoListAPI\";s:0:\"\";s:9:\"JAnalysis\";s:0:\"\";s:11:\"JDplayerAPI\";s:0:\"\";s:12:\"JShieldNames\";s:0:\"\";s:14:\"JProhibitWords\";s:0:\"\";s:15:\"JProhibitScript\";s:3:\"off\";s:13:\"JProhibitEmsp\";s:3:\"off\";s:16:\"JProhibitChinese\";s:3:\"off\";s:15:\"JDynamicComment\";s:3:\"off\";}');

-- ----------------------------
-- Table structure for typecho_relationships
-- ----------------------------
DROP TABLE IF EXISTS `typecho_relationships`;
CREATE TABLE `typecho_relationships` (
  `cid` int(10) unsigned NOT NULL,
  `mid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`cid`,`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of typecho_relationships
-- ----------------------------
INSERT INTO `typecho_relationships` VALUES ('1', '1');
INSERT INTO `typecho_relationships` VALUES ('1', '13');
INSERT INTO `typecho_relationships` VALUES ('5', '1');
INSERT INTO `typecho_relationships` VALUES ('5', '12');
INSERT INTO `typecho_relationships` VALUES ('8', '1');
INSERT INTO `typecho_relationships` VALUES ('8', '11');
INSERT INTO `typecho_relationships` VALUES ('10', '1');
INSERT INTO `typecho_relationships` VALUES ('10', '9');
INSERT INTO `typecho_relationships` VALUES ('12', '10');
INSERT INTO `typecho_relationships` VALUES ('12', '19');
INSERT INTO `typecho_relationships` VALUES ('14', '10');
INSERT INTO `typecho_relationships` VALUES ('14', '19');
INSERT INTO `typecho_relationships` VALUES ('16', '1');
INSERT INTO `typecho_relationships` VALUES ('16', '9');
INSERT INTO `typecho_relationships` VALUES ('17', '1');
INSERT INTO `typecho_relationships` VALUES ('17', '14');
INSERT INTO `typecho_relationships` VALUES ('18', '6');
INSERT INTO `typecho_relationships` VALUES ('20', '6');
INSERT INTO `typecho_relationships` VALUES ('23', '1');
INSERT INTO `typecho_relationships` VALUES ('25', '19');
INSERT INTO `typecho_relationships` VALUES ('26', '15');
INSERT INTO `typecho_relationships` VALUES ('26', '19');
INSERT INTO `typecho_relationships` VALUES ('37', '6');
INSERT INTO `typecho_relationships` VALUES ('46', '6');
INSERT INTO `typecho_relationships` VALUES ('57', '20');
INSERT INTO `typecho_relationships` VALUES ('57', '23');
INSERT INTO `typecho_relationships` VALUES ('59', '6');
INSERT INTO `typecho_relationships` VALUES ('61', '6');
INSERT INTO `typecho_relationships` VALUES ('63', '24');
INSERT INTO `typecho_relationships` VALUES ('63', '25');
INSERT INTO `typecho_relationships` VALUES ('65', '20');
INSERT INTO `typecho_relationships` VALUES ('69', '24');
INSERT INTO `typecho_relationships` VALUES ('69', '25');
INSERT INTO `typecho_relationships` VALUES ('69', '26');
INSERT INTO `typecho_relationships` VALUES ('71', '21');
INSERT INTO `typecho_relationships` VALUES ('71', '27');
INSERT INTO `typecho_relationships` VALUES ('71', '28');
INSERT INTO `typecho_relationships` VALUES ('73', '21');
INSERT INTO `typecho_relationships` VALUES ('73', '28');
INSERT INTO `typecho_relationships` VALUES ('76', '21');
INSERT INTO `typecho_relationships` VALUES ('76', '28');
INSERT INTO `typecho_relationships` VALUES ('97', '6');
INSERT INTO `typecho_relationships` VALUES ('97', '29');
INSERT INTO `typecho_relationships` VALUES ('97', '30');
INSERT INTO `typecho_relationships` VALUES ('99', '21');
INSERT INTO `typecho_relationships` VALUES ('99', '31');
INSERT INTO `typecho_relationships` VALUES ('101', '19');
INSERT INTO `typecho_relationships` VALUES ('112', '19');
INSERT INTO `typecho_relationships` VALUES ('115', '22');
INSERT INTO `typecho_relationships` VALUES ('115', '32');
INSERT INTO `typecho_relationships` VALUES ('115', '33');
INSERT INTO `typecho_relationships` VALUES ('116', '22');
INSERT INTO `typecho_relationships` VALUES ('116', '32');
INSERT INTO `typecho_relationships` VALUES ('116', '34');
INSERT INTO `typecho_relationships` VALUES ('116', '35');
INSERT INTO `typecho_relationships` VALUES ('123', '22');
INSERT INTO `typecho_relationships` VALUES ('123', '32');
INSERT INTO `typecho_relationships` VALUES ('123', '36');
INSERT INTO `typecho_relationships` VALUES ('126', '21');
INSERT INTO `typecho_relationships` VALUES ('126', '33');
INSERT INTO `typecho_relationships` VALUES ('126', '37');
INSERT INTO `typecho_relationships` VALUES ('126', '38');
INSERT INTO `typecho_relationships` VALUES ('137', '20');
INSERT INTO `typecho_relationships` VALUES ('138', '20');

-- ----------------------------
-- Table structure for typecho_users
-- ----------------------------
DROP TABLE IF EXISTS `typecho_users`;
CREATE TABLE `typecho_users` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `mail` varchar(200) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `screenName` varchar(32) DEFAULT NULL,
  `created` int(10) unsigned DEFAULT '0',
  `activated` int(10) unsigned DEFAULT '0',
  `logged` int(10) unsigned DEFAULT '0',
  `group` varchar(16) DEFAULT 'visitor',
  `authCode` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `mail` (`mail`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of typecho_users
-- ----------------------------
INSERT INTO `typecho_users` VALUES ('1', 'phpmaster', '$P$Bt0WqbYh4PccNHKkgoYqFKm0OWWt9m.', '137647337@qq.com', 'http://www.zhangxuhui.com', 'phpmaster', '1570331493', '1616314244', '1616252573', 'administrator', 'dc8df3b4529d0f6ea2d39b6a10bda3a0');
