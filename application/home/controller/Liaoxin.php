<?php

namespace app\home\controller;

use think\Controller;
use think\Request;

class Liaoxin extends Controller
{
    const APPID = "wx0e3591698bf7b673";
    const APPSECRET = "096d1102e453cae25c5ee868a658b4c2";
    private $obj;

    public function __construct(Request $request = null)
    {
        parent::__construct($request);

      // $postStr = $GLOBALS["HTTP_RAW_POST_DATA"];// 7-`
        // xml格式的字符串  接收到微信服务器给我发送的内容
        $postStr = file_get_contents("php://input");//php 7+`

        dump($postStr);
       // 转成xml格式的对象
        $this->obj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
    //        // 被动回复
        dump($this->obj);
        $this->response();

    }

    // 被动回复
    public function response(){
        // 接收推送事件的类型
        $MsgType = $this->obj->MsgType;
        switch ($MsgType){
            case "event":
                // 事件
                $this->event();
                break;
            case "text":
                // 用户只要给我发了文本，我就开始回复
                $this->receive();
                break;
            case  "image":
                // 用户发送了图片
                $content = "你给我发了图片";
                $this->receiveText($content);
                break;
            case  "voice":
                // 用户发送了语音
                $content = "你给我发了语音";
                $this->receiveText($content);
                break;
            case  "video":
                // 用户发送了视频
                $content = "你给我发了视频";
                $this->receiveText($content);
                break;
            default:
                // 用户发送了啥
                $content = "你给我发啥啊";
                $this->receiveText($content);
                break;

        }
    }
    public function event(){
        // 判断是关注还是取关
        $Event = $this->obj->Event;
        switch ($Event){
            case "subscribe":
                // 关注事件 回复文本
                $content = "欢迎大驾光临，有失远迎，失敬失敬，光临寒舍，蓬荜生辉！";
                $this->receiveText($content);
                // 把用户信息写入数据库
                $this->addFans();
                break;
            case "unsubscribe":
                // 取消关注事件
                // 执行软删除
                $id = Fans::where('openid',$this->obj->FromUserName)->value('id');
                Fans::destroy($id);
                break;
        }
    }
    // 回复用户
    public function receive(){
        // 接收用户的文本 匹配前两个字是否天气
        if(preg_match('/^天气(.*)/',$this->obj->Content,$arr)){
            // 获取剩余内容
            $content = $this->getWeather($arr[1]);
        }else{
            $content = "你给我发了文本";
        }
        // 用户发送了文本
        // 用户发送天气郑州 可以查询郑州的天气
        $this->receiveText($content);
    }
    // 回复文本消息
    public function receiveText($Content){
        // 微信公众号
        $FromUserName = $this->obj->ToUserName;
        // 用户的openid
        $ToUserName = $this->obj->FromUserName;
        $time = time();
        $MsgType = 'text';
        $xml = "<xml>
              <ToUserName><![CDATA[%s]]></ToUserName>
              <FromUserName><![CDATA[%s]]></FromUserName>
              <CreateTime>%s</CreateTime>
              <MsgType><![CDATA[%s]]></MsgType>
              <Content><![CDATA[%s]]></Content>
            </xml>";
        printf($xml,$ToUserName,$FromUserName,$time,$MsgType,$Content);
    }
    public function addFans(){
        // 用户的所有信息
        $info = $this->getUserInfo();
        $fans = new Fans();
        // 3.插入记录
        // 根据当前的openid 来查询数据库中是否存在
        // 如果不存在 写入数据库
        if(!Fans::withTrashed()->where("openid",$info->openid)->find()){
            // 第一次关注 num 默认为1
            $fans->allowField(true)->save($info);
        }else{
            // delete 字段 清空 就是重新关注了
            $fans->save(["delete_time"=>null],["openid"=>$this->obj->FromUserName]);
            Db::name("fans")->where(["openid"=>$this->obj->FromUserName])->setInc('num');
        }

    }

    // 生成access_token
    public function createAccessToken(){
        Session::init([
            'auto_start'     => true,
            'expire'=>7000
        ]);
        // 判断accessToken是否过期
        // 如果session 过期了
        if(!Session::has("accessToken")){
            // 1.根据appid，appsercet 来获取access_token
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".self::APPID."&secret=".self::APPSECRET;
            $json = json_decode(curl($url));
            Session::set("accessToken",$json->access_token);
        }
        return Session::get("accessToken");
    }
    // 获取用户数据
    public function getUserInfo(){
        // 1.根据appid，appsercet 来获取access_token
        $accessToken = $this->createAccessToken();
        // 2.根据用户的openid，和access_token 来获取用户的数据
        $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={$accessToken}&openid=".$this->obj->FromUserName."&lang=zh_CN";
        return json_decode(curl($url));

    }
    // 校验 数据的合法性
    public function checkSignature()
    {
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];
        $echostr = $_GET["echostr"];

        $token = 'zhengke';
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode( $tmpArr );
        $tmpStr = sha1( $tmpStr );

        if( $tmpStr == $signature ){
            return $echostr;
        }else{
            return false;
        }
    }

    // 查看支持的城市列表
    public function getWeather($city){
        /*    $url = "http://apis.juhe.cn/simpleWeather/cityList?key=87ab46fe13af92d19f2bf0442b2aebeb";
            file_put_contents("weather.json",curl($url));
            file_put_contents("weather.txt",curl($url));
            file_put_contents("weather.html",curl($url));*/
        $cityList = json_decode(file_get_contents("weather.json"),true);
        $flag = false;
        foreach ($cityList['result'] as $v){
            if($v['district'] == $city){
                $flag = true;
                break;
            }
        }
        // 城市查到了
        if($flag){
            return $this->getW($city);
        }else{
            return "城市不存在，无法查询";
        }

    }
    public function getW($city){
        $city = urlencode($city);
        $key = "87ab46fe13af92d19f2bf0442b2aebeb";
        $url = "http://apis.juhe.cn/simpleWeather/query?city={$city}&key={$key}";
        $obj = json_decode(curl($url));
        if($obj->error_code){
            return $obj->reason;
        }
        $result = $obj->result;
        return "今天是".$result->future[0]->date."城市:".$result->city."当前温度:".$result->realtime->temperature.
            "湿度".$result->realtime->humidity;
    }

//    public function checkSignature()
//    {
//        $echostr = $_GET["echostr"];
//        $signature = $_GET["signature"];
//        $timestamp = $_GET["timestamp"];
//        $nonce = $_GET["nonce"];
//
//        $token = 'lx666666';
//        $tmpArr = array($token, $timestamp, $nonce);
//        sort($tmpArr, SORT_STRING);
//        $tmpStr = implode( $tmpArr );
//        $tmpStr = sha1( $tmpStr );
//
//        if( $tmpStr == $signature ){
//            echo  $echostr;
//        }else{
//            return false;
//        }
//    }
}
