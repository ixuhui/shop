<?php

namespace app\Home\controller;

use think\Controller;
use think\Request;
use think\Db;

class Xcx extends Controller
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $goodsname= input("get.goodsname");
        $rows = Db::name("goods")->where("goods_name","like","%$goodsname%")->select();
        return json($rows);

    }
    // 分页的接口
    public function getGoodsList(){
        // 1.  0,3
        // 2.  3,3
        // 3.  6,3
        $pagesize = 3;
        $page = input("get.page",1);
//        $page = 1;
        $start = ($page-1)*$pagesize;

        $rows = Db::name("goods")->order("id")->limit("$start,$pagesize")->select();
        return json($rows);
    }
    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
