<?php

namespace app\home\controller;

use app\home\model\Member;
use think\Controller;
use think\Db;


class Email extends Controller
{
    //邮箱注册
    public function regActionEmail(){
        // 入库
        $username = input("post.username");
        $password = md5($username.input("post.password"));
        $email = input("post.email");
        $expire_time = time()+120;
        $hash = md5($username);
        // 入库
        $data = compact("username","email",'password',"expire_time","hash");
        DB::name("member_email_phone")->insert($data);
        $active_url = request()->domain().url("active",["hash"=>$hash],false);
        $now = date("Y-m-d H:i:s");
        $body = <<<Abc
                    <pre>
                尊敬的聚合用户：
                欢迎使用聚合数据提供的数据技术服务！
                请点击以下的链接验证您的邮箱，验证成功后就可以使用聚合提供的所有服务了。
                <a href='$active_url'>$active_url</a>(如果不能打开页面，请复制该地址到浏览器打开)
                聚合数据团队
                $now
                    </pre>
Abc;
        sendEmail($email,"账号激活",$body);
        $this->redirect("regSuccess",['actindex'=>base64_encode($email)]);
    }
    // 验证邮箱 完成注册
    public function regSuccess($actindex){
        $actindex = base64_decode($actindex);
        $data = compact("actindex");// ["actindex"=>$actindex]
        return view("",$data);
    }

    // 激活
    public function active(){
        // 接收hash
        $hash = request()->param("hash");
        // 根据hash 查询是否已经激活过
        $row = Db::name("member_email_phone")->where("hash",$hash)->field(["active","expire_time"])->find();
        $flag = 1;
        if($row)
        {
            if($row['active'] == 1){
                // 3.激活的 再次激活 直接提示激活成功
                $flag = 0;
            }else if($row['expire_time'] > time()){
                // 1. 正常激活
                Db::name("member_email_phone")->where("hash",$hash)->update(['active'=>1]);
                $flag = 0;
            }
        }
        // 2. 查不到，就激活失败
        return view("",compact("flag"));
    }

    // 找回密码 发送邮件
    // 137@qq.com  www.qq.com
    // 137@163.com www.163.com
    public function findPass(){
        // 发邮件
        $email = input("post.email");
        // 查询hash
        $hash = Db::name("member_email_phone")->where("email",$email)->value("hash");
        if($hash === null){
            // die;
        }
        $url = request()->domain().url("resetPwd",['hash'=>$hash],false);
        $body = <<<Abc
                    <pre>
                        尊敬的聚合用户：
                        您在03月31日向聚合服务中心申请通过邮箱找回密码，如若非本人操作请忽略本邮件！
                        请通过点击以下链接进行密码重置：
                        <a href="$url">$url</a>
                        (如果不能打开页面，请复制该地址到浏览器打开)
                    </pre>
Abc;
        sendEmail($email,"账号激活",$body);
        return view("",['email'=>$email]);
    }

    // 重置密码 从邮箱点击过来,修改的页面
    public function resetPwd($hash){
        // $hash
        // 直接修改密码
        return view("",['hash'=>$hash]);
    }
    //
    public function resetPwdDo($hash){
        $username = Db::name("member_email_phone")->where("hash",$hash)->value("username");
        // $hash
        $password = md5($username.input("post.password"));
        // 直接修改密码
        Db::name("member_email_phone")->where('hash',$hash)->update(['password'=>$password]);

        return $this->redirect("Index/login");
    }


}
