<?php

namespace app\home\controller;

use think\Controller;
use think\Db;
use think\Session;

class Qqlogin extends Controller
{
    //
    // 专门用于接收腾讯给我回调回来的数据 code
    public function callback(){
        // Step1：获取Authorization Code
        $code = input("get.code");
        //Step2：通过Authorization Code获取Access Token
        $appid = "101938823";
        $appkey = "9fe22ece9566ea6998d2dcc9a53024dd";
        $redirect_uri = urlencode(request()->domain().url("callbakck"));
        $url = "https://graph.qq.com/oauth2.0/token?grant_type=authorization_code&client_id=$appid&client_secret=$appkey&code=$code&redirect_uri=$redirect_uri&fmt=json";
        //发送请求
        $json_str = curl($url);
        $obj = json_decode($json_str);
        $access_token = $obj->access_token;

        // Step3 获取用户OpenID_OAuth2.0
        $url = "https://graph.qq.com/oauth2.0/me?access_token=$access_token&fmt=json";
        $json_str = curl($url);
        $obj = json_decode($json_str);
        $openid = $obj->openid;
        // Step4 通过openid 来获取用户的资料
        // 获取用户资料
        $url = "https://graph.qq.com/user/get_user_info?access_token=$access_token&oauth_consumer_key=$appid&openid=$openid";
        $json_str = curl($url);
        $qqinfo = json_decode($json_str);
        // QQ入库
        $this->qqlogin($openid,$qqinfo);
//        $da['s'] = 1;
//        $da['m'] = 'xiaomimng';
//        $json_str = json_encode($da);
//
//        $this->redirect("qqlogin",['openid'=>$openid,'qqinfo'=>$json_str]);
        // http://www.zhangxuhui.com/home/index/qqlogin/openid/1/qqinfo/];
        // http://www.zhangxuhui.com/home/index/qqlogin?openid=1&qqinfo=2
    }
    // qq登录成功后入库
    public function qqlogin($openid,$qqinfo){
        // 遍历
        // 1.根据openid查询qq表是否有此记录
        $mid = Db::name("qq")->where("openid",$openid)->value("mid");
        // 1.第一次用QQ来登录
        if($mid === null){
            $data['openid'] = $openid;
            $data['nickname'] = $qqinfo->nickname;
            $data['gender'] = $qqinfo->gender;
            $data['province'] = $qqinfo->province;
            $data['city'] = $qqinfo->city;
            $data['year'] = $qqinfo->year;
            $data['figurl'] = $qqinfo->figureurl;
            Db::name("qq")->insert($data);
            // 绑定
            // 跳转
            $this->redirect("bindAccount",['openid'=>$openid]);
        }elseif ($mid === 0){
            // 已经用QQ登录过，但是没有绑定，跳转到绑定页面，login_num + 1
            Db::name("qq")->where('openid',$openid)->setInc("login_num");
            $this->redirect("bindAccount",['openid'=>$openid]);
        }else{
            // 存sesion
            Session::set("mid",$mid);
            // 跳转到首页
            return $this->redirect("Index/index");
        }


    }
    // 如果用户没有绑定账号，则需要绑定
    public function bindAccount(){
        $openid = request()->param("openid");
        return view("",['openid'=>$openid]);
    }
    // 绑定处理
    public function bindAccountAction(){
        // 修改QQ表mid的字段，让这个字段的值是当前绑定的这个账号的id
        $where['username'] = input("post.username");
        $where['password'] = md5(input("post.username").input("post.password"));
        $mid = Db::name("member")->where($where)->value("id");
        if($mid === null){
            // 跳404
            header(url("index"));
        }
        $data['mid'] = $mid;

        Db::name("qq")->where(["openid"=>input("post.openid")])->update($data);
        // 登录成功
        Session::set("id",$mid);
        Session::set("member_name",$where['username']);
        // 跳转首页
        return $this->redirect("Index/index");
    }
}
