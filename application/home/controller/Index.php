<?php
namespace app\home\controller;

use app\home\model\Member;
use think\Controller;
use think\Cookie;
use think\Session;
use think\Validate;
use think\Db;
use myclass\Test;

use myclass\SMTP;
use myclass\PHPMailer;
// 引入第三方二维码类
use myclass\QRcode;
class Index extends Controller
{
    public function index()
    {
       /* $t = new Test();
        $t->t();*/
        // 从session获取用户id
        return view();
    }
    // 注册
    public function reg(){
        // 1.每个用户的专属推荐码
        $refer_id = 0;
        if(input('?get.refer_code')){
            $refer_code = input("get.refer_code");
            // 2.根据推荐码查询当前用户的id
            $member = new Member();
            $refer_id = $member->where("refer_code",$refer_code)->value("id");
        }
        return view("",['refer_id'=>$refer_id]);
    }
    //注册处理
    public function regAction(){
        // 跳转

        // 后台校验
        $rule = [
            'username'  => 'require|regex:[a-zA-Z_]\w{2,9}',
            'password'  => 'require|length:6,18',
            'email'  => 'require|email',
            'phone'  => 'require|length:11',
        ];
        $msg = [
            'username.require' => '用户必须',
            'username.regex'     => '用户名格式不正确',
            'password.require'     => 'require',
            'password.length'     => 'length',
            'email.require'     => '密码不正确',
            'email.email'     => 'email',
            'phone.require'     => 'require',
            'phone.length'     => 'length',
        ];
        $data = input();
        array_shift($data);
      /*  $validate = new Validate($rule,$msg);
        $result   = $validate->check($data);
        if(!$result){
           echo $validate->getError();
        }*/
        // 移除掉password2
        unset($data['password2']);
        unset($data['yzm']);
        $data['password'] = md5($data['username'].$data['password']);
        // 注册
        $member = Member::create($data);
        // 修改refer_code
        // 注册成功
        if($member->id){
            $id = $member->id;
            $refer_code = $id.uniqid($id);
            if($member->save(['refer_code'=>$refer_code],["id"=>$id])){
                // 注册成功
                return $this->success("恭喜您，注册成功","login");
            }else{
                // 删除此记录
                $member->delete();
                return $this->error("注册失败了,请重新注册");

            }

        }else{
            return $this->error("注册失败了,请重新注册");
        }


    }
    // 校验
    public function ajaxVerifyYzm(){
        $yzm = input("get.yzm");

        // $yzm 用户输入的验证码
       if(!captcha_check($yzm)){
            //验证失败
            $data['status'] = '1';
        }else{
            $data['status'] = '0';
        }
        return json($data);
    }
    // 登录页面
    public function login(){
        $redirect_uri = urlencode("http://www.zhangxuhui.com/home/Qqlogin/callback");
        $appid = "101938823";
        $url = "https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=$appid&redirect_uri=$redirect_uri&state=test";
        return view('',['url'=>$url]);
    }
    public function loginAction(){
        $username = input("post.username");
        $password = input("post.password");
        $where = [];
        if(preg_match('/^[a-zA-Z_]\w{2,9}$/',$username)){
            echo "您正在使用用户名登录";
            $where['username'] = $username;

        }
        if(preg_match('/^[a-zA-Z_0-9]+\@\w{2,9}\.(com|cn|net)$/',$username)){
            echo "您正在使用邮箱登录";
            $where['email'] = $username;

        }
        if(preg_match('/^[1]\d{10}$/',$username)){
            echo "手机号登录";
            $where['phone'] = $username;

        }
        $where['passowrd'] = md5($username.$password);
        Db::name("member")->where($where)->find();
        $this->error("登录失败");
    }
    // 找回密码
    public function forget(){
        return view();
    }


    public function regQrcode($code){
        return view("",['code'=>$code]);
    }

    public function regQrcodeAction($code){
        $data = input("post.");
        $username = $data['username'];
        $data['password'] = md5($data['username'].$data['password']);
        $id = Db::name("member")->insertGetId($data);
        // refer_code
        $refer_code = $id.uniqid($id);
        // 根据推荐人的refer_code 查询推荐人的id
        $refer_id = Db::name("member")->where("refer_code",$refer_code)->value("id");
        Db::name("member")->where("id",$id)->update(['refer_code'=>$refer_code,'refer_id'=>$refer_id]);
        // 生成自己的专属二维码
        $this->createQrcode($username,$refer_code);

        $this->success("注册成功",'index');
    }

    public function createQrcode($username,$code){
        $value = "http://10.50.8.41/tpshop/public/index.php/home/Index/regQrcode/code/$code"; //二维码内容
        $errorCorrectionLevel = 'L';//容错级别
        $matrixPointSize = 6;//生成图片大小
        QRcode::png($value, "./qrcode/$username.png", $errorCorrectionLevel, $matrixPointSize, 2);
    }

	public function goodsDetail($id){
		
		echo "我是商品的详情页--$id";
	}

}
