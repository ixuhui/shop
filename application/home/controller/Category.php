<?php

namespace app\home\controller;

use think\Controller;
use think\Db;
use think\Request;

class Category extends Controller
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $request = request();
        $map = [];
        $searchList = [];
        $brand_id = $request->param("brand_id");
        $oldprice = $request->param("price");
        // 根据品牌id查询商品信息
        if(isset($brand_id) && ($brand_id !=null)){
            $map['brand_id'] = $brand_id;
            $searchList['品牌'] = Db::name("brand")->where("id",$brand_id)->value("type_name");
        }
        // 根据价格查询
        if(isset($oldprice) && ($oldprice !=null)){
//            $map['price'] = $brand_id;
            $price = explode("-",$oldprice);
            $low = $price[0];
            $height = $price[1];
            $map['goods_price'] = ['between',[$low,$height]];
            $searchList['价格'] = $oldprice;
        }

        // 获取所有的品牌(实际上获取的是跟搜索结果相关的品牌)
        $brandList = Db::name("brand")->select();
        // 获取所有的分类
        $categoryList = Db::name("category")->select();
        // 获取所有的商品信息
        $goodsList = Db::name("goods")->where($map)->select();

        $data = compact("goodsList","brandList","categoryList","searchList");
        return view("",$data);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
