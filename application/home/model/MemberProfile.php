<?php

namespace app\home\model;

use think\Model;

class MemberProfile extends Model
{
    //一对一 反向使用
//belongsTo('关联模型名','外键名','关联表主键名',['模型别名定义'],'join类型');
    public function member(){
        return $this->belongsTo("Member","mid")->bind("username");
    }
}
