<?php

namespace app\home\model;

use think\Model;

class Member extends Model
{
    // 开始自动维护时间戳
//    protected $autoWriteTimestamp = true;
    // 1. 一对一 hasOne('关联模型名','外键名','主键名',['模型别名定义'],'join类型');
// 设置当前模型对应的完整数据表名称
//    protected $table = 'tpshop_member';
    // bind 绑定你想要显示的第二张表的字段
    // 方法名随意 （习惯性用第二张的模型名，表名)
    public function memberProfile(){
        return $this->hasOne("MemberProfile","mid")->bind("hobby,height");
    }
}
