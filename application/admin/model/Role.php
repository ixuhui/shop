<?php

namespace app\admin\model;

use think\Model;

class Role extends Model
{
    //
    protected $autoWriteTimestamp = true;
    public function test(){
        dump("test....");
    }

    // 多对多
   //belongsToMany('关联模型名','中间表名','外键名','当前模型关联键名',['模型别名定义']);
    public function auth(){
        return $this->belongsToMany('Auth','role_auth','aid','rid');
    }
}
