<?php
use think\Db;

function getTree($pid=0,$times=0){
    static $arr;
    // 1.获取顶级分类
    $rows = Db::name("auth a")->join("auth au","a.pid=au.id","LEFT")->field("a.*,au.auth_name auth_pname")->where("a.pid",$pid)->select();
    $times++;
    if($rows){
        // 2.遍历顶级分类
        foreach ($rows as $v){
            $v['auth_name'] = "|".str_repeat("--",$times).$v['auth_name'];
            $arr[] = $v;
            $pid = $v['id'];
            getTree($pid,$times); // getTree(16); getTree(18);
        }
    }

    return $arr;
}

function getArr($pid=0)
{
    // 获取所有的记录
    $rows = Db::name("auth")->where("delete_time is null")->select();
    if ($rows) {
        foreach ($rows as $v) {
            $arr[$v['id']] = $v;
        }
    }
    return $arr;

}
function generateTree($items)
    {
        $tree = array();
        foreach ($items as $item) {
            //判断是否有数组的索引==
            if (isset($items[$item['pid']])) {     //查找数组里面是否有该分类  如 isset($items[0])  isset($items[1])
                $items[$item['pid']]['son'][] = &$items[$item['id']]; //上面的内容变化,$tree里面的值就变化
            } else {
                $tree[] = &$items[$item['id']];   //把他的地址给了$tree
            }
        }
        return $tree;
}

/**
 * @param $list
 * @param int $pid
 * @return array
 * [
 * 0=>[id=>1,pid=0,name=aaaa]
 * 1=>[id=>2,pid=1,name=bbbb]
 * 2=>[id=>3,pid=1,name=ccc]
 * 3=>[id=>4,pid=2,name=dddd]
 * 4=>[id=>5,pid=3,name=eeee]
 * ]
 * $arr = [
 * [id=>1,pid=0,name=aaaa]
 *
 * ]
 * [id=>2,pid=1,name=bbbb]
 *
 *
 *
 */
function getChildren($list,$pid=0)
{
    $arr = array();
    foreach ($list as $value) {
        if ($value['pid'] == $pid) {
            $value['son'] = getChildren($list, $value['id']);// 1  2
            $arr[] = $value;
        }
    }
    return $arr;

}