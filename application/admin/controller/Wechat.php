<?php

namespace app\admin\controller;

use app\admin\model\Fans;
use think\Controller;
use think\Db;
use think\Request;
use think\Session;
class Wechat extends Controller
{

    const APPID = "wx5d700b1fd7c6bb21";
    const APPSECRET = "18fc068a13214bae1fc94255732f0302";
    //
    private $obj;

    // 被动回复
    public function response(){
        //
//      // $postStr = $GLOBALS["HTTP_RAW_POST_DATA"];// 7-`
//        // xml格式的字符串  接收到微信服务器给我发送的内容
        $postStr = file_get_contents("php://input");//php 7+`
        file_put_contents("aaaaa.txt",$postStr);
        if(!$postStr){
            return false;
        }
//////
////        // 转成xml格式的对象
        $this->obj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
        file_put_contents("a.txt",$postStr);

        // 接收推送事件的类型
        $MsgType = $this->obj->MsgType;
        switch ($MsgType){
            case "event":
                // 事件
                $this->event();
                break;
            case "text":
                // 用户只要给我发了文本，我就开始回复
                $this->receive();
                break;
            case  "image":
                // 用户发送了图片
                $content = "你给我发了图片";
                $this->receiveText($content);
                break;
            case  "voice":
                // 用户发送了语音
              /*  $content = "您说了:".$this->obj->Recognition;
                $this->receiveText($content);*/
                $this->receiveVoice();
                break;
            case  "video":
                // 用户发送了视频
                $content = "你给我发了视频";
                $this->receiveText($content);
                break;
            default:
                // 用户发送了啥
                $content = "你给我发啥啊";
                $this->receiveText($content);
                break;

        }



    }
    // 关注或取关事件
    public function event(){
        // 判断是关注还是取关
        $Event = $this->obj->Event;
        switch ($Event){
            case "subscribe":
                // 关注事件 回复文本
//                $this->receiveNews();
                // 把用户信息写入数据库
                $this->addFans($this->obj->FromUserName);
                $content = "欢迎大驾光临，有失远迎，失敬失敬，光临寒舍，蓬荜生辉！";
                $this->receiveText($content);

                break;
            case "unsubscribe":
                // 取消关注事件
                // 执行软删除
                $id = Fans::where('openid',$this->obj->FromUserName)->value('id');
                Fans::destroy($id);
                break;

            case "CLICK":
                $this->receiveClick();

        }
    }
    // 用户点击了
    public function receiveClick(){
        $EventKey = $this->obj->EventKey;
        switch ($EventKey){
            case "joke":
                // 点击了看笑话
                $this->receiveText($this->joke());
                break;
            case "love":
                // 点击了看笑话
                $this->receiveText("love");
                break;
            case "luck":
                // 点击了看笑话
                $this->receiveText("luck");
                break;

        }
    }
    // 回复语音
    public function receiveVoice(){
        // 接收用户的文本 匹配前两个字是否天气
        if(preg_match('/^天气(.*)/',$this->obj->Recognition,$arr)){
            file_put_contents("aaa.txt",rtrim($arr[1],"。"));
            // 获取剩余内容
            $content = $this->getWeather(rtrim($arr[1],"。"));
            $this->receiveText($content);
        }else if(preg_match('/^搜索(.*)/',$this->obj->Recognition,$arr)){
            // 执行搜索
            file_put_contents("aaa.txt",rtrim($arr[1],"。"));
            $this->searchGoods(rtrim($arr[1],"。"));
        }else{
            // 用户发送了文本
            // 用户发送天气郑州 可以查询郑州的天气
            $content = "您说了:".$this->obj->Recognition;
            $this->receiveText($content);
        }

    }

    // 回复用户
    public function receive(){
        // 接收用户的文本 匹配前两个字是否天气
        if(preg_match('/^天气(.*)/',$this->obj->Content,$arr)){
            // 获取剩余内容
            $content = $this->getWeather($arr[1]);
        }else if(preg_match('/^搜索(.*)/',$this->obj->Content,$arr)){
           $content = $arr[1];
           // 执行搜索
            $this->searchGoods($content);
        }else{
            // 用户发送了文本
            // 用户发送天气郑州 可以查询郑州的天气
//            $content = $this->joke();
//            $this->receiveText($content);
//            $this->receiveText($this->obj->FromUserName);
//            $this->receiveText("账号功能开发中......敬请期待");
            $this->receiveVideo();


        }

    }
    // 回复文本消息
    public function receiveText($Content){

        // 微信公众号
        $FromUserName = $this->obj->ToUserName;
        // 用户的openid
       $ToUserName = $this->obj->FromUserName;

        $time = time();
        $MsgType = 'text';
        $xml = "<xml>
              <ToUserName><![CDATA[%s]]></ToUserName>
              <FromUserName><![CDATA[%s]]></FromUserName>
              <CreateTime>%s</CreateTime>
              <MsgType><![CDATA[%s]]></MsgType>
              <Content><![CDATA[%s]]></Content>
            </xml>";
        printf($xml,$ToUserName,$FromUserName,$time,$MsgType,$Content);
    }

    // 回复图片
    public function receiveImg($media_id){

        // 微信公众号
        $FromUserName = $this->obj->ToUserName;
        // 用户的openid
        $ToUserName = $this->obj->FromUserName;
        $time = time();
        $xml = "<xml>
                  <ToUserName><![CDATA[%s]]></ToUserName>
                  <FromUserName><![CDATA[%s]]></FromUserName>
                  <CreateTime>%s</CreateTime>
                  <MsgType><![CDATA[image]]></MsgType>
                  <Image>
                    <MediaId><![CDATA[%s]]></MediaId>
                  </Image>
                </xml>
";
        $media_id = "7_HMU-EbT5YzrfwU2xoxn7PDEOONXeR9HvEhdhtjFhFoiHGlMfZ705M9kaBx7xyU";
        printf($xml,$ToUserName,$FromUserName,$time,$media_id);
    }

    // 回复视频
    public function receiveVideo(){

        // 微信公众号
        $FromUserName = $this->obj->ToUserName;
        // 用户的openid
        $ToUserName = $this->obj->FromUserName;
        $time = time();
        $xml = "<xml>
  <ToUserName><![CDATA[%s]]></ToUserName>
  <FromUserName><![CDATA[%s]]></FromUserName>
  <CreateTime>%s</CreateTime>
  <MsgType><![CDATA[video]]></MsgType>
  <Video>
    <MediaId><![CDATA[%s]]></MediaId>
    <Title><![CDATA[%s]]></Title>
    <Description><![CDATA[%s]]></Description>
  </Video>
</xml>
";
        $title = "汉服出行";
        $desc = "汉服一日游！";
        $media_id = "QHPST4cgsPuNlpXM1BoVF5FCMQ6TM8nBwTxKgWsuk1xUTA4eFXOaAVDTuODpCWES";
        printf($xml,$ToUserName,$FromUserName,$time,$media_id,$title,$desc);
    }
    // 搜索
    public function searchGoods($content){
        // 搜索
        $goodsList = Db::name("goods")->where("goods_name","like","%$content%")->select();
        if(!$goodsList){
           return  $this->receiveText("您手上的商品[$content]不存在，请更换词语重试~~~");
        }
        $rand = mt_rand(0,count($goodsList));
        $goods = $goodsList[$rand];
        $xml = "<xml>
                  <ToUserName><![CDATA[%s]]></ToUserName>
                  <FromUserName><![CDATA[%s]]></FromUserName>
                  <CreateTime>%s</CreateTime>
                  <MsgType><![CDATA[news]]></MsgType>
                    <ArticleCount>1</ArticleCount>
                    <Articles>
                         <item>
                          <Title><![CDATA[%s]]></Title>
                          <Description><![CDATA[%s]]></Description>
                          <PicUrl><![CDATA[%s]]></PicUrl>
                          <Url><![CDATA[%s]]></Url>
                          </item>
                </Articles>
                </xml>";
        // 微信公众号
        $FromUserName = $this->obj->ToUserName;
        // 用户的openid
        $ToUserName = $this->obj->FromUserName;
        $time = time();
        $request = Request::instance();
        $picUrl = $request->domain()."/public/uploads/".$goods['goods_logo'];
        $url = $request->domain().url("home/Index/goodsDetail",["id"=>$goods['id']]);
     /*   file_put_contents("0.txt",$picUrl);
        file_put_contents("1.txt",$url);
        die;*/
        printf($xml,$ToUserName,$FromUserName,$time,$goods['goods_name'],$goods['goods_introduce'],$picUrl,$url);
    }
    public function receiveNews1(){
        $goods = [
            ['id'=>1,'goodsname'=>"小米手机","url"=>"sdfdsf","picurl"=>"dfsdf"],
            ['id'=>2,'goodsname'=>"大米手机","url"=>"sdfdsf","picurl"=>"dfsdf"]
        ];
        $l = count($goods);
        $xml = "<xml>
                  <ToUserName><![CDATA[%s]]></ToUserName>
                  <FromUserName><![CDATA[%s]]></FromUserName>
                  <CreateTime>%s</CreateTime>
                  <MsgType><![CDATA[news]]></MsgType>";
        $xml .= "<ArticleCount>%s</ArticleCount>";
        $xml .= " <Articles>";
        foreach ($goods as $v){
            $xml .=" <item>
                      <Title><![CDATA[$v[goodsname]]></Title>
                      <Description><![CDATA[%s]]></Description>
                      <PicUrl><![CDATA[%s]]></PicUrl>
                      <Url><![CDATA[%s]]></Url>
                    </item>";
        }
        $xml .= " </Articles>";
        $xml .= "</xml>";
        // 微信公众号
        $FromUserName = $this->obj->ToUserName;
        // 用户的openid
        $ToUserName = $this->obj->FromUserName;
        $time = time();
        printf($xml,$ToUserName,$FromUserName,$time,$l);
    /*
        $title1 = "福利姬，欢迎点击，马上删除！！！";
        $description1 = "一个你不容错过的精彩大戏！";
        $picurl1 = "https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1232129009,3319713821&fm=26&gp=0.jpg";
        $url1 = "http://www.baidu.com";

        $title2 = "美女图片";
        $description2 = "是美女啊";
        $picurl2 = "https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3098554968,1951756784&fm=26&gp=0.jpg";
        $url2 = "http://www.qq.com";*/
//        printf($xml,$ToUserName,$FromUserName,$time,$title1,$description1,$picurl1,$url1,$title2,$description2,$picurl2,$url2);

    }

    // 用户信息写入数据库
    public function addFans($openid){
        // 用户的所有信息
        $info = $this->getUserInfo($openid);
        $fans = new Fans();
        // 3.插入记录
        // 根据当前的openid 来查询数据库中是否存在
        // 如果不存在 写入数据库
        if(!Fans::withTrashed()->where("openid",$info['openid'])->find()){
            // 第一次关注 num 默认为1
            $fans->allowField(true)->save($info);
        }else{
            // delete 字段 清空 就是重新关注了
            $fans->save(["delete_time"=>null],["openid"=>$openid]);
            Db::name("fans")->where(["openid"=>$openid])->setInc('num');
        }
    }

    // 生成access_token
    public function createAccessTokenOld(){
        Session::init([
            'auto_start'     => true,
            'expire'=>7000
        ]);
        // 判断accessToken是否过期
        // 如果session 过期了
        if(!Session::has("accessToken")){
            // 1.根据appid，appsercet 来获取access_token
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".self::APPID."&secret=".self::APPSECRET;
            $json = json_decode(curl($url));
            Session::set("accessToken",$json->access_token);
        }
        return Session::get("accessToken");
    }

    public function createAccessToken(){
        // 从数据库中取出token
        $row = Db::name("token")->find(1);
        if(time() > $row['expire_time']){
            //过期 重新请求 存数据库
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".self::APPID."&secret=".self::APPSECRET;
            $json = json_decode(curl($url));
            // 存数据库
            $data['expire_time'] = time() + 7000;
            $data['access_token'] = $json->access_token;
            Db::name("token")->where("id",1)->update($data);
            return $json->access_token;
        }

        return $row['access_token'];
    }

    // 获取用户数据getUserInfo
    public function getUserInfo($openid){
      // 1.根据appid，appsercet 来获取access_token
        $accessToken = $this->createAccessToken();
        // 2.根据用户的openid，和access_token 来获取用户的数据
        $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={$accessToken}&openid=".$openid."&lang=zh_CN";
        return json_decode(curl($url),true);

    }
    // 校验 数据的合法性
    public function checkSignature()
    {
        $this->response();
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];
        $echostr = $_GET["echostr"];

        $token = 'zhengke';
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode( $tmpArr );
        $tmpStr = sha1( $tmpStr );

        if( $tmpStr == $signature ){
            return $echostr;
        }else{
            return false;
        }
    }

    // 查看支持的城市列表
    public function getWeather($city){
        /*    $url = "http://apis.juhe.cn/simpleWeather/cityList?key=87ab46fe13af92d19f2bf0442b2aebeb";
            file_put_contents("weather.json",curl($url));
            file_put_contents("weather.txt",curl($url));
            file_put_contents("weather.html",curl($url));*/
        $cityList = json_decode(file_get_contents("weather.json"),true);
        $flag = false;
        foreach ($cityList['result'] as $v){
            if($v['district'] == $city){
                $flag = true;
                break;
            }
        }
        // 城市查到了
        if($flag){
            return $this->getW($city);
        }else{
            return "城市不存在，无法查询";
        }

    }
    public function getW($city){
        $city = urlencode($city);
        $key = "87ab46fe13af92d19f2bf0442b2aebeb";
        $url = "http://apis.juhe.cn/simpleWeather/query?city={$city}&key={$key}";
        $obj = json_decode(curl($url));
        if($obj->error_code){
            return $obj->reason;
        }
        $result = $obj->result;
        return "今天是".$result->future[0]->date."城市:".$result->city."当前温度:".$result->realtime->temperature.
            "湿度".$result->realtime->humidity;
    }


    public function joke(){
        $url = "http://v.juhe.cn/joke/randJoke.php?key=64ff75a4be4561fc2480784e5b6b520b";
        $obj = json_decode(curl($url));
        if($obj->error_code){
            $row = Db::query("select * from tpshop_joke order by rand() limit 1");
//           file_put_contents("aaaaaaaaaa.txt",$row['content']);
            return $row[0]['content'];
        }else{
            // joke表写数据库
            foreach ($obj->result as $v){
                Db::name("joke")->insert(['content'=>$v->content]);
            }
            return $obj->result[0]->content;
        }
    }

    // 查询自定义菜单
    public function getMenu(){
        $accessToken = $this->createAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=".$accessToken;
        return curl($url);
    }

    // 创建自定义菜单
    public function createMenu(){
        $accessToken = $this->createAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=$accessToken";
        $myurl = "http://www.zhangxuhui.com/public/index.php";
         $data['button'][0] = ['type'=>"click","name"=>"看笑话","key"=>"joke"];
         $data['button'][1]['name'] = "我的菜单";
         $data['button'][1]['sub_button'][0] = ["type"=>"scancode_push","name"=>"scancode_push","key"=>"sp"];
         $data['button'][1]['sub_button'][1] = ["type"=>"scancode_waitmsg","name"=>"scancode_waitmsg","key"=>"sw"];
         $data['button'][1]['sub_button'][2] = ["type"=>"pic_sysphoto","name"=>"pic_sysphoto","key"=>"ps"];
         $data['button'][1]['sub_button'][3] = ["type"=>"pic_photo_or_album","name"=>"pic_photo_or_album","key"=>"ppoa"];
         $data['button'][1]['sub_button'][4] = ["type"=>"location_select","name"=>"location_select","key"=>"ls"];
         $data['button'][2] = ['type'=>"view","name"=>"我的网站","url"=>"$myurl"];

         $params = json_encode($data,JSON_UNESCAPED_UNICODE);
         $obj = json_decode(curl($url,$params,1));
        if($obj->errcode){
            return "创建失败";
        }else{
            return "创建成功";
        }

    }
    // 删除自定义菜单
    public function delMenu(){
        $accessToken = $this->createAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=$accessToken";
        $obj = json_decode(curl($url));
        if($obj->errorcode){
//            $this->success();
        }

    }

    // 创建个性化菜单
    public function createStyleMenu(){
        $myurl = "http://www.baidu.com";
        $data['button'][0] = ['type'=>"click","name"=>"girlwatchjoke","key"=>"joke"];
        $data['button'][1]['name'] = "girlmymenu";
        $data['button'][1]['sub_button'][0] = ["type"=>"click","name"=>"查星座","key"=>"star"];
        $data['button'][1]['sub_button'][1] = ["type"=>"click","name"=>"查星座","key"=>"star"];
        $data['button'][1]['sub_button'][2] = ["type"=>"click","name"=>"查星座","key"=>"star"];
        $data['button'][2] = ['type'=>"view","name"=>"mywebsite","url"=>"$myurl"];

        $data['matchrule'] = ["sex"=>2];

        $params = json_encode($data,JSON_UNESCAPED_UNICODE);
        $accessToken = $this->createAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=$accessToken";
        dump(curl($url,$params,1));
    }

    // 删除个性化菜单
    public function delStyleMenu(){
        $accessToken = $this->createAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/menu/delconditional?access_token=$accessToken";
        dump(curl($url));
    }

    // 获取所有用户openid并根据openid获取用户信息并插入数据库
    public function getAllInSql(){
        // 根据openid获取用户基本信息
//        dump($this->getAllFans()); // 获取所有粉丝的openid
//        $fansList = [];
        foreach ($this->getAllFans() as $v){
           $this->addFans($v);
        }
    }
   // 获取公众号所有用户的列表
    public function getAllFans(){
        $accessToken = $this->createAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/user/get?access_token={$accessToken}&next_openid";
        $arr = json_decode(curl($url),true);
        return $arr['data']['openid'];

    }




}

?>


