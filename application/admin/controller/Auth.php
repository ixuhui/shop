<?php
namespace app\admin\controller;

use app\admin\model\Auth as AU;
use think\Controller;


// 权限管理
class Auth extends Common
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    // 权限列表
    public function index()
    {

        //获取所有权限列表
        /**
         * 系统管理
         *     权限管理
         *     角色管理
         * 商品管理
         *     商品列表
         *     商品添加
         */
        $authList = getTree();

        return view("",["authList"=>$authList]);
    }

    // 添加权限
    public function add(){
        // 获取所有顶级权限
        $authList = AU::all(['pid'=>0]);
        return view("",["authList"=>$authList]);
    }

    // 添加权限的处理
    public function addAction(){
        $data = input("post.");
        if(AU::create($data)){
            $this->success("权限添加成功","index");
        }else{
            $this->error("权限添加失败");
        }

    }


}
