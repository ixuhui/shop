<?php

namespace app\admin\controller;

use app\admin\model\Category;
use think\Controller;
use think\Db;

class Goods extends Common
{

    public function index()
    {
        // 1.查询商品列表
        $goodsList = Db::name("goods")->paginate(3);
        // 2.分配模板
        $data = compact("goodsList");
        return view("", $data);
        // 3.模板遍历
    }

    /** 根据商品id删除商品
     * @param $id 商品id
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */

    public function goodsDel($id)
    {
        return json(Db::name("goods")->delete($id));
    }

    // 商品新增
    public function goodsAdd()
    {
        // 获取商品分类
        $level1 = Db::name("category")->where("pid", 0)->select();
        // 获取商品品牌
        $brands = Db::name("brand")->select();

        $data = compact("level1", "brands");
        return view('', $data);
    }

    // 添加商品
    public function goodsAddAction()
    {

        $data = input("post.");
        // 商品上传图片
        $file = request()->file('goods_logo');
        // 移动到框架应用根目录/public/uploads/ 目录下
        $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
        if ($info) {
            // 成功上传后 获取上传信息
            // 输出 jpg
//            echo $info->getExtension();
            // 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
            $goods_logo =  str_replace("\\","/",$info->getSaveName());
            // 输出 42a79759f284b767dfcb2a0197904287.jpg
//            echo $info->getFilename();
        } else {
            // 上传失败获取错误信息
//            echo $file->getError();
            return $this->error("请重试~~~");
        }
        $attr1 = $data['attr1'];
        $goods_name = $data['goods_name'];
        $goods_price = $data['goods_price'];
        $goods_number = $data['goods_number'];
        $goods_introduce = $data['goods_introduce'];
        $cate_id = $data['level3'];
        $brand_id = $data['brand_id'];
        // 1.商品表写入数据
        $goodsdata = compact("goods_name", "goods_price", "goods_number", "goods_introduce", "cate_id", "brand_id","goods_logo");
        $goods_id = Db::name("goods")->insertGetId($goodsdata);
        // 2.商品规格属性表写入数据
        foreach ($attr1 as $k => $v) {
            foreach ($v as $v1) {
                Db::name("goods_attr")->insert(['goods_id' => $goods_id, "attr_id" => $v1, "apid" => $k]);
            }
        }
        $this->success("商品添加成功","index");
    }

    // 通过商品分类id获取属性
    public function getAttrByCateId($id)
    {
        // 1.根据分类id 找到 此分类下的所有属性 中间表(分类属性表)
        $id = 129;
        $rows = Db::name("cate_attr ca")->join("attribute a", "ca.attr_id = a.id")->where("cate_id", $id)->field("a.id,a.attr_values")->select();

        // 根据pid = 当前的id 查询values
        foreach ($rows as &$v) {
            $pid = $v['id'];
            $attr_rows = Db::name("attribute")->where("pid", $pid)->select();
            foreach ($attr_rows as $v1) {
                $v['info'][$v1['id']] = $v1['attr_values'];
            }
        }
        return json($rows);
    }

    // 商品分类 列表
    public function cateList()
    {
        $cateList = Db::name("category c")->join('category cc', 'c.pid=cc.id', 'LEFT')->field('c.id,c.pid,c.cate_name  ca,cc.cate_name cn')->select();
$cateList = getChildren($cateList);
      /*  dump($cateList);
        echo "AAAAAAAAAAAAAAAAAAA<hr>";
        $arr = [];
        foreach ($cateList as $v) {
            $arr[$v['id']] = $v;
        }
        dump($arr);
        echo "BBBBBBBBBBBBBBBBBBB<hr>";
        dump(generateTree($arr));
        die;*/
//        dump($cateList);
        $data = compact("cateList");
        return view('', $data);
    }

    // 商品分类添加
    public function cateAdd()
    {
        // 获取一级
        $level1 = Db::name("category")->where("pid", 0)->select();
        $data = compact("level1");
        return view('', $data);
    }

    // 商品分类添加
    public function cateAddAction()
    {
        $cate = new Category();
        $level1 = input("post.level1");
        $level2 = input("post.level2");
        $cate_name = input("post.cate_name");
        if ($level1 == 0 || $level2 == 0) {
            // 如果$level1 == 0添加的是1级
            // 如果$level2 == 0添加的是2级
            $pid = $level1;
        } else {
            // 如果添加的是三级
            $pid = $level2;
        }
        if ($cate->save(['pid' => $pid, 'cate_name' => $cate_name])) {
            $this->success("添加商品分类成功", "cateList");
        } else {
            $this->error("添加商品分类失败");
        }

    }

    // 根据分类id获取此id的分类
    public function getCateByCateId($id)
    {
        $data = Db::name("category")->where("pid", $id)->select();
        return json($data);
    }


    // 修改商品分类名称
    public function modifyCateName($cid, $cate_name)
    {
        if (Db::name("category")->where(['cate_name' => $cate_name])->value("id")) {
            // 如果查到了，说明，你要改的分类已经存在了
            $data['status'] = '1';
        } else {
            if (Db::name("category")->where(['id' => $cid])->update(['cate_name' => $cate_name])) {
                // 修改成功
                $data['status'] = '0';
            } else {
                $data['status'] = '2';
            }

        }
        return json($data);
    }


    // 商品分类编辑页面
    public function modifyCateNamePage($id, $cate_name)
    {
        // 关闭layout
        $this->view->engine->layout(false);
        return view("", ['id' => $id, 'cate_name' => $cate_name]);
    }


}
