<?php

namespace app\admin\controller;

use app\admin\model\Fans;
use think\Db;

class Message extends Common
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    // 发送消息粉丝列表
    public function index()
    {
        $fans = Fans::all();
        return view("",['fans'=>$fans]);
    }

    // 弹窗发送消息页面

    public function alertWindow(){
        $this->view->engine->layout(false);
        return view();
    }

    // 发送文本
    public function sendText($openid,$txt){
        // 客服发送消息的接口
        $wechat = new Wechat();
        $accessToken = $wechat->createAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=$accessToken";
        $str = <<<SSS
    {
            "touser":"$openid",
            "msgtype":"text",
             "text":
            {
                "content":"$txt"
            }
}
SSS;
        return curl($url,$str,1);
    }
    public function getAllOpenid(){
        $fans = new Fans();
        return $fans->column("openid");
    }
    // 根据openid群发接口
/*    public function sendTextAll(){
        $wechat = new Wechat();
        $accessToken = $wechat->createAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=$accessToken";
        $fans = $this->getAllOpenid();
        $text = [
            "touser"=>$fans,
            "msgtype"=>"text",
            "text"=>["content"=>"我正在测试群发功能"]
        ];
        dump(curl($url,json_encode($text),1));
    }*/
    // 群发
    public function sendTextAll(){
        // 客服发送消息的接口
        $wechat = new Wechat();
        $accessToken = $wechat->createAccessToken();
        $fans = $this->getAllOpenid();
        $url = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=$accessToken";
        foreach ($fans as $v){
            $str = <<<SSS
            {
            "touser":"$v",
            "msgtype":"text",
             "text":
            {
                "content":"测试群发"
            }
            }
SSS;
            curl($url,$str,1);
        }
    }
}
