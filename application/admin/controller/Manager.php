<?php

namespace app\admin\controller;

use think\Controller;
use app\Admin\model\Manager as MAN;
use think\Db;

class Manager extends Common
{
    // 管理员列表
    public function index(){
//        dump(input("get."));
        $map = [];

     /*   if(input("?get.username") && (input("get.username") != "")){
            $username = input("get.username");
            $map["usernmae"] = ["like","%$username%"];
        }
        if(input("?get.nickname") && (input("get.nickname") != "")){
            $username = input("get.nickname");
            $map["nickname"] = ["like","%nickname%"];
        }
        if(input("?get.email") && (input("get.email") != "")){
            $email = input("get.email");
            $map["email"] = ["like","%$email%"];
        }*/

        $username = input("get.username");
        $nickname = input("get.nickname");
        $email = input("get.email");
        $status = input("get.status");
        if($status !=null && $status != "0"){
            $map["status"] = ["=",$status];
        }

        $role_id = input("get.role_id");
        // 初次打开页面，role_id == null
        // 点击查找 role_id == ""
        if($role_id != null or  $role_id != ""){
            $map["rid"] = ["=",$role_id];
        }
        $map["username"] = ["like","%$username%"];
        $map["nickname"] = ["like","%$nickname%"];
        $map["email"] = ["like","%$email%"];

        $searchDate = input("get.searchDate");
        if($searchDate && $searchDate !=""){
            // 2020-04-04  - 2020-04-06
            // 1.截取 取出开始，结束时间
            // 2.将日期转成时间戳
            $arr = explode(" - ",$searchDate);
            $start = strtotime($arr[0]);
            $end = strtotime($arr[1]);
            $map['m.create_time'] = ["between",[$start,$end]];
        }

        $mid = 1;
        $man = new MAN();
//        $managerList = $man->searchOrShowList($mid,$map);
        $list_rows = 3;
        $query = compact("username","nickname","email","role_id","status","searchDate","list_rows");
        $managerList =  $man->searchOrShowList($mid,$map,$query);

        $roles = Db::name("role")->column("id,role_name");
        $data = compact("managerList",'roles');
        return view("",$data);
    }
    // 新增管理员
    public function add(){
        return view();
    }
    // 新增管理员处理
    public function addAction(){
       $data = input("post.");
       $data['password'] = md5($data['username'].$data['password']);
        // 根据管理员名称查询此管理员是否已经存在，如果存在则不添加
        $a = 1;
        $b = 1000000;
//        // byte -128,127    short  -65536 ,65535  int   -2147483647 , 2147483647  long
//        int age = 18;
        if(MAN::create($data)){
            $this->success("管理员添加成功",'index');
        }else{
            $this->error("管理员添加失败");
        }
    }
    // 分配角色页面
    public function setRole($id,$manager_name){
        // 根据当前的管理员查询他的所有角色
        $rids = Db::name("manager_role")->where(["mid"=>$id])->column("rid");
        // 查询所有的角色
        $roles= Db::name("role")->column("id,role_name");
        $data = compact("id","manager_name","roles","rids");
        return view("",$data);
    }

    // 分配角色处理
    public function setRoleAction(){
        $data = input("post.");
        $mid = $data['mid'];
        Db::name("manager_role")->where("mid",$mid)->delete();
        if(!array_key_exists("rid",$data)){
            return $this->success("角色分配成功","index");
        }
        foreach ($data['rid'] as $rid){
            Db::name("manager_role")->insert(['mid'=>$mid,'rid'=>$rid]);
        }
        return $this->success("角色分配成功","index");
    }
    public function addAjax(){
        dump(input("post."));

    }
}
