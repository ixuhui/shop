<?php
namespace app\admin\controller;
use app\admin\model\Category;
use app\admin\model\Goods;
use app\home\model\Member as ME;
use think\Db;
use app\admin\model\Role;
use app\admin\model\Auth;
class Index extends Common
{

    // 测试一对多 正向
    public function test1(){
        // 正向一条
//        $info3 = Category::with("goods")->find(129);
        $info3 = Category::with("goods")->select();
        return json($info3);
    }

    // 测试一对多 反向
    public function test2(){
        // 反向 一条
//        $info3 = Goods::with("category")->find(146);
        $info3 = Goods::with("category")->select();
        return json($info3);
    }

    public function test3(){
//        $info3 = Role::with("auth")->find(3);
//        $info3 = Role::with("auth")->select();
//        return json($info3);
        $info3 = Auth::with("role")->select();
        return json($info3);
    }


    public function index()
    {

        // 当前登录是的xiaoming 2
        return view("");
    }
}
