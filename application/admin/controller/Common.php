<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Request;
use think\Session;

class Common extends Controller
{
    //
    public function __construct(Request $request = null)
    {
        parent::__construct($request);
        // 1.从session中取出mid
        // 如果没有，直接跳转登录页面，如果有继续往下执行
//        $mid = Session::get("mid");
        $mid = 1;
        if($mid == 1){
            // 超级管理员
            $nickname = "超级管理员";
            $info = Db::name("auth")->select();
        }else{
            $request = request();
            // 获取当前访问的控制器名称，当前访问的操作名称
            $info = Db::name("manager m")->join("manager_role mr","m.id=mr.mid")->join("role r","mr.rid=r.id")
            ->join("role_auth ra","r.id=ra.rid")
            ->join("auth a","ra.aid=a.id")
            ->field("m.nickname,r.role_name,a.id,a.auth_name,a.pid,a.auth_c,a.auth_a,a.is_nav")
            ->where("mr.mid",$mid)->select();
            $ca = $request->controller()."/". $request->action();
            $allowList =["Index/index"];
            foreach ($info as $v){
                $allowList[] = $v['auth_c']."/".strtolower($v['auth_a']);
            }
            if(!in_array($ca,$allowList)){
               return $this->redirect("Index/index");
            }
        }
        $authInfo = [];
        foreach ($info as $v){
            $authInfo[$v['id']] = $v;
        }
        $authInfoList = generateTree($authInfo);
        $this->assign("authInfoList",$authInfoList);

    }
}
